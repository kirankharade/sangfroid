#include <iostream>
#include "MReader.h"
#include "Reader.h"
#include "MPointDataStream.h"
#include "IPointDataStream.h"
#include <msclr\marshal_cppstd.h>

using namespace GLiDE::NET;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Poise::IOManaged;
using namespace Poise::IO;
using namespace std;

MReader::MReader()
{

}

MReader::~MReader()
{
   this->!MReader();
}

MReader::!MReader()
{
}

MPointDataStream^ MReader::Load(System::String^ filename, System::String^ formatIdentifier)
{
   std::string file = msclr::interop::marshal_as<std::string>(filename);
   std::string format = msclr::interop::marshal_as<std::string>(formatIdentifier);
   auto reader = new Poise::IO::Reader();
   Poise::IO::IDataStream* ds = reader->Load(file.c_str(), format.c_str());
   Poise::IO::IPointDataStream* us = dynamic_cast<Poise::IO::IPointDataStream*>(ds);
   return gcnew MPointDataStream(us);
}
