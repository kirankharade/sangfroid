#include "MPointDataStream.h"
#include <msclr\marshal_cppstd.h>

using namespace GLiDE::NET;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Poise::IOManaged;
using namespace Poise::IO;

MPointDataStream::MPointDataStream(Poise::IO::IPointDataStream* unmanaged)
   :MDataStream(unmanaged)
{
   m_unmanaged = unmanaged;
}

MPointDataStream::~MPointDataStream()
{
}

MPointDataStream::!MPointDataStream()
{
}

Point3^ MPointDataStream::First()
{
   auto pt = ((IPointDataStream*) m_unmanaged)->First();
   return gcnew GML::Elements::NET::Point3(pt.x, pt.y, pt.z);
}

Point3^ MPointDataStream::Next()
{
   auto pt = ((IPointDataStream*)m_unmanaged)->Next();
   return gcnew GML::Elements::NET::Point3(pt.x, pt.y, pt.z);
}


