#include "MSTLDataStream.h"
#include <msclr\marshal_cppstd.h>

using namespace GLiDE::NET;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Poise::IOManaged;
using namespace Poise::IO;

MSTLDataStream::MSTLDataStream(Poise::IO::IDataStream* unmanaged)
   :MDataStream(unmanaged)
{
   m_unmanaged = unmanaged;
}

MSTLDataStream::~MSTLDataStream()
{
}

MSTLDataStream::!MSTLDataStream()
{
}

//Poise::PoiseManaged::MTriangleMesh^ MSTLDataStream::Mesh()
//{
//   auto mesh = ((STLDataStream*)m_unmanaged)->Mesh();
//   return gcnew Poise::PoiseManaged::MTriangleMesh(mesh);
//}
//

IDataStream* MSTLDataStream::Unmanaged()
{
   return m_unmanaged;
}


