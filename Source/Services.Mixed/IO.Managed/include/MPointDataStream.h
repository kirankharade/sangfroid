#pragma once

#define pointer_safety Pointer_safety
#include <memory>

#include "GML_Elements/GML_Elements_Code.h"
#include "MDataStream.h"
#include "IPointDataStream.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace IOManaged {

      public ref class MPointDataStream: public MDataStream
      {

      public :

         MPointDataStream(IO::IPointDataStream* unmanaged);

         virtual ~MPointDataStream();

         !MPointDataStream();

         property Point3^ Min
         {
            Point3^ get() 
            { 
               auto pt = ((IO::IPointDataStream*)m_unmanaged)->Min();
               return gcnew GML::Elements::NET::Point3(pt.x, pt.y, pt.z);
            }
         }

         property Point3^ Max
         {
            Point3^ get()
            {
               auto pt = ((IO::IPointDataStream*)m_unmanaged)->Max();
               return gcnew GML::Elements::NET::Point3(pt.x, pt.y, pt.z);
            }
         }

         property int NumPoints
         {
            int get() 
            { 
               return ((IO::IPointDataStream*)m_unmanaged)->PointCount();
            }
         }


         Point3^ First();

         Point3^ Next();

      };
   }
} // end namespace
