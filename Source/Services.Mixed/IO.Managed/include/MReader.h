#pragma once

#define pointer_safety Pointer_safety
#include <memory>

#include "GML_Elements/GML_Elements_Code.h"
#include "MPointDataStream.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace IOManaged {

      public ref class MReader
      {

      public :

         MReader();

         ~MReader();

         !MReader();

         MPointDataStream^ Load(System::String^ filename, System::String^ formatIdentifier);

      };
   }
} // end namespace
