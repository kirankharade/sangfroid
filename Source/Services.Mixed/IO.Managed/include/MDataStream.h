#pragma once

#define pointer_safety Pointer_safety
#include <memory>

#include "GML_Elements/GML_Elements_Code.h"
#include "IDataStream.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace IOManaged {

      public ref class MDataStream
      {
      protected:

         IO::IDataStream *m_unmanaged;

      public :

         MDataStream(IO::IDataStream* unmanaged)
            :m_unmanaged(unmanaged)
         {

         }

         virtual ~MDataStream()
         {
            this->!MDataStream();
         }

         !MDataStream()
         {
            if (m_unmanaged != nullptr)
            {
               delete m_unmanaged;
            }
         }


      };
   }
} // end namespace
