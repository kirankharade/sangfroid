#pragma once

#define pointer_safety Pointer_safety
#include <memory>
#include "ModelBuilderManaged_Namespace.h"
#include "GML_Elements/GML_Elements_Code.h"
#include "DebugRenderer.h"
#include "Point3D.h"
#include "Vector3D.h"
#include "MTriangulationData.h"
#include "MLineData.h"
#include "MPointData.h"
#include <vector>

using namespace GML::Elements::NET;
using namespace GLiDE::NET;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace mb = Renishaw::ModelBuilder;
namespace mbm = Renishaw::ModelBuilderManaged;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
MODELBUILDER_MANAGED_NAMESPACE_START
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public ref class MDebugRenderer
{
public:

   List<mbm::MTriangulationData^>^ m_meshes = gcnew List<mbm::MTriangulationData^>();
   List<mbm::MLineData^>^ m_lines = gcnew List<mbm::MLineData^>();
   List<mbm::MPointData^>^ m_points = gcnew List<mbm::MPointData^>();

   MDebugRenderer()
   {
      m_meshes = gcnew List<mbm::MTriangulationData^>();
      m_lines = gcnew List<mbm::MLineData^>();
      m_points = gcnew List<mbm::MPointData^>();
   }

   void populate()
   {
	  #ifdef _DEBUG
      if (mb::DebugRenderer::Dirty())
      {
         m_meshes = populateMeshes();
         m_lines = populateLines();
         m_points = populatePoints();

         mb::DebugRenderer::set_Dirty(false);
      }
	  #endif
   }

   List<mbm::MTriangulationData^>^ populateMeshes()
   {
      List<mbm::MTriangulationData^>^ meshes = gcnew List<mbm::MTriangulationData^>();

      #ifdef _DEBUG
      auto drMeshes = mb::DebugRenderer::Meshes();

      for (auto& mesh : drMeshes)
      {
         if (mesh->Vertices.size() < 3)
         {
            continue;
         }
         meshes->Add(gcnew MTriangulationData(mesh));
      }
	  #endif

      return meshes;
   }

   List<mbm::MLineData^>^ populateLines()
   {
      List<mbm::MLineData^>^ lines = gcnew List<mbm::MLineData^>();

      #ifdef _DEBUG
	  auto drLines = mb::DebugRenderer::Lines();
      for (auto line : drLines)
      {
         auto l = gcnew MLineData(line);
         lines->Add(l);
      }
      #endif

	  return lines;
   }

   List<mbm::MPointData^>^ populatePoints()
   {
      List<mbm::MPointData^>^ points = gcnew List<mbm::MPointData^>();

      #ifdef _DEBUG
	  auto drPoints = mb::DebugRenderer::Points();
      for (auto pt : drPoints)
      {
         auto p = gcnew MPointData(pt);
         points->Add(p);
      }
      #endif

	  return points;
   }
   
   property List<mbm::MTriangulationData^>^ Meshes
   {
      List<mbm::MTriangulationData^>^ get()
      {
         populate();
         return m_meshes;
      }
   }

   property List<mbm::MLineData^>^ Lines
   {
      List<mbm::MLineData^>^ get()
      {
         populate();
         return m_lines;
      }
   }

   property List<mbm::MPointData^>^ Points
   {
      List<mbm::MPointData^>^ get()
      {
         populate();
         return m_points;
      }
   }

   property bool Dirty
   {
      bool get()
      {
		#ifdef _DEBUG
		  return mb::DebugRenderer::Dirty();
		#else
		  return false;
		#endif
      }
   }

   property bool Enabled
   {
      #ifdef _DEBUG
	  bool get()
      {
		  return mb::DebugRenderer::Enabled();
	  }
      void set(bool value)
      {
         mb::DebugRenderer::Enable(value);
      }
      #else
	   bool get()
	   {
		   return false;
	   }
	   void set(bool value)
	   {
	   }
       #endif
   }

   static std::string ToStdString(System::String^ string)
   {
      using System::Runtime::InteropServices::Marshal;
      System::IntPtr pointer = Marshal::StringToHGlobalAnsi(string);
      char* charPointer = reinterpret_cast<char *>(pointer.ToPointer());
      std::string returnString(charPointer, string->Length);
      Marshal::FreeHGlobal(pointer);
      return returnString;
   }

   static System::String ^ ToSystemString(const std::string& string)
   {
      return gcnew System::String(string.c_str());
   }


   property System::String^ Tag
   {
	#ifdef _DEBUG
	  System::String^ get()
      {
         return ToSystemString(mb::DebugRenderer::Tag());
      }
      void set(System::String^ value)
      {
         mb::DebugRenderer::set_Tag(ToStdString(value));
      }
	#else
	   System::String^ get()
	   {
		   return gcnew System::String("");
	   }
	   void set(System::String^ value)
	   {
	   }
	#endif
   }

   void ClearAll()
   {
	#ifdef _DEBUG
	   mb::DebugRenderer::ClearAll();
	#endif

	  m_meshes->Clear();
      m_lines->Clear();
      m_points->Clear();
   }

   void HideAll()
   {
	#ifdef _DEBUG
	   mb::DebugRenderer::HideAll();
	#endif
   }

   void ShowAll()
   {
	#ifdef _DEBUG
	   mb::DebugRenderer::ShowAll();
	#endif
   }

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
MODELBUILDER_MANAGED_NAMESPACE_END
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


