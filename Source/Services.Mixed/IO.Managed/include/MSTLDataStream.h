#pragma once

#define pointer_safety Pointer_safety
#include <memory>

#include "GML_Elements/GML_Elements_Code.h"
#include "MDataStream.h"
#include "STLDataStream.h"
//#include "MTriangleMesh.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace IOManaged {

      public ref class MSTLDataStream: public MDataStream
      {

      public :

         MSTLDataStream(IO::IDataStream* unmanaged);

         virtual ~MSTLDataStream();

         !MSTLDataStream();

         //PoiseManaged::MTriangleMesh^ Mesh();

         IO::IDataStream* Unmanaged();

      };
   }
} // end namespace
