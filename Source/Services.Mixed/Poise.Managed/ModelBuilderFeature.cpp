﻿#define pointer_safety Pointer_safety
#include <memory>
#include "GML_ELements/GML_Elements_Code.h"
#include "ModelBuilderFeature.h"
#include "ModelBuilderTouchPoint.h"
#include <cassert>
#include <algorithm>
#include <string.h>
#include <iostream>
#include <vector>
#include <sstream>

/////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace GML::Elements;
using namespace Poise;
using namespace Poise::PoiseManaged;
namespace gml = GML::Elements;

/////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
int ModelBuilderFeature::s_featureCount = 0;
/////////////////////////////////////////////////////////////////////////////////////////////////////////

ModelBuilderFeature::ModelBuilderFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints, int index)
   : m_index(index)
{
   if(m_index == -1)
   {
      m_index = s_featureCount;
      s_featureCount++;
   }

   //std::vector<ModelBuilderTouchPoint> uniqueTouchPoints;
   //int tchPtCnt = (int) touchPoints.size();
   //for (int i = 0; i < tchPtCnt; i++)
   //{
   //   if (uniqueTouchPoints.end() != std::find(uniqueTouchPoints.begin(), uniqueTouchPoints.end(), touchPoints[i]))
   //   {
   //      uniqueTouchPoints.push_back(touchPoints[i]);
   //   }
   //}

   //m_touchPoints.clear();
   //m_touchPoints = uniqueTouchPoints;

   m_touchPoints = touchPoints;
}

ModelBuilderFeature::~ModelBuilderFeature()
{
}

std::vector<ModelBuilderTouchPoint> ModelBuilderFeature::GetTouchPoints() const
{
   return m_touchPoints;
}

int ModelBuilderFeature::Index() const
{
   return m_index;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

PlaneFeature::PlaneFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints, 
                                 const GML::Elements::CPoint3d &point, 
                                 const GML::Elements::CVector3d &normal,
                                 const int index)
                                 : ModelBuilderFeature(touchPoints, index)
                                 , m_point(point)
                                 , m_normal(normal)
{
}

EFeatureGeometryType PlaneFeature::GetType() const 
{
   return EFeatureGeometryType::Plane;
}

GML::Elements::CPoint3d PlaneFeature::GetPoint() const 
{
   return m_point;
}

GML::Elements::CVector3d PlaneFeature::GetNormal() const 
{
   return m_normal;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

CylinderFeature::CylinderFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints, 
                                 const GML::Elements::CPoint3d &point, 
                                 const GML::Elements::CVector3d &axis, 
                                 const double radius,
                                 const bool isBore,
                                 const int index)
                                 : ModelBuilderFeature(touchPoints, index)
     , m_point(point)
     , m_axis(axis)
     , m_radius(radius)
     , m_isBore(isBore)
{
}

EFeatureGeometryType CylinderFeature::GetType() const 
{
   return EFeatureGeometryType::Cylinder;
}

GML::Elements::CPoint3d CylinderFeature::GetPoint() const 
{
   return m_point;
}

GML::Elements::CVector3d CylinderFeature::GetAxis() const 
{
   return m_axis;
}

double CylinderFeature::GetRadius() const 
{
    return m_radius;
}

bool CylinderFeature::IsBore() const 
{
    return m_isBore;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

static GML::Elements::CPoint3d calculateConeApex(
   const GML::Elements::CPoint3d& coneBaseCentre,
   const double coneBaseRadius,
   const GML::Elements::CVector3d coneAxis,
   const double semiAngle)
{

   // Cone axis -> Apex to Base; use inverse of axis of calculation
   GML::Elements::CVector3d revAxis(coneAxis);
   revAxis = revAxis.Negate().GetNormalised();
   const double tanSemiAngle = std::tan(semiAngle);

   // Distance from cone base point to apex
   const double h = std::fabs(coneBaseRadius / tanSemiAngle);

   // Return apex from distance and axis
   return coneBaseCentre + (revAxis * h);
}

ConeFeature::ConeFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints, 
               const GML::Elements::CPoint3d baseCentre, 
                         const GML::Elements::CVector3d axis,
                         const double baseRadius,
                         const double semiAngle,
                         const bool isBore,
                         int index)
                         : ModelBuilderFeature(touchPoints, index)
      , m_baseCentre(baseCentre)
      , m_axis(axis.GetNormalised())
      , m_baseRadius(baseRadius)
      , m_semiAngle(semiAngle)
      , m_isBore(isBore)
      , m_apex(CPoint3d())
{
   m_apex = calculateConeApex(m_baseCentre, m_baseRadius, m_axis, m_semiAngle);
}

EFeatureGeometryType ConeFeature::GetType() const 
{
   return EFeatureGeometryType::Cone;
}

GML::Elements::CPoint3d ConeFeature::GetBaseCenter() const 
{
   return m_baseCentre;
}

GML::Elements::CPoint3d ConeFeature::GetApex() const
{
   return m_apex;
}

GML::Elements::CVector3d ConeFeature::GetAxis() const 
{
   return m_axis;
}

double ConeFeature::GetBaseRadius() const 
{
   return m_baseRadius;
}

double ConeFeature::GetSemiAngle() const 
{
   return m_semiAngle;
}

bool ConeFeature::IsBore() const 
{
    return m_isBore;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

CircleFeature::CircleFeature(
                  const std::vector<ModelBuilderTouchPoint>& touchPoints, 
                  const GML::Elements::CPoint3d& centre,
                  const GML::Elements::CVector3d& associatedPlaneNormal,
                  const double radius,
                  const bool isBore,
                  int index)
                  : ModelBuilderFeature(touchPoints, index)
                  , m_radius(radius)
                  , m_associatedPlaneNormal(associatedPlaneNormal)
                  , m_centre(centre)
                  , m_isBore(isBore)
{
}

EFeatureGeometryType CircleFeature::GetType() const
{
   return EFeatureGeometryType::Circle;
}

GML::Elements::CVector3d CircleFeature::GetAssociatedPlaneNormal() const
{
   return m_associatedPlaneNormal;
}

GML::Elements::CPoint3d CircleFeature::GetCentre() const
{
   return m_centre;
}

double CircleFeature::GetRadius() const
{
   return m_radius;
}

bool CircleFeature::IsBore() const
{
   return m_isBore;
}

