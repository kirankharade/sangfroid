#pragma once

#define pointer_safety Pointer_safety
#include <memory>
#include "MPointCloud.h"
#include "TriangleMesh.h"
#include "GML_Elements/GML_Elements_Code.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace PoiseManaged {

      public ref class MTriangleMesh
      {
      private:

         TriangleMesh* m_unmanaged;

      public :

         MTriangleMesh(TriangleMesh* unmanaged);

         ~MTriangleMesh();

         !MTriangleMesh();

         void Associate(MPointCloud^ cloud);

         MTriangleMesh^ RemoveBigTriangles(float thresholdArea, bool unifyVertices);

         List<MTriangleMesh^>^ DivideIntoFaceTriangulations(float angleDeg);

         void GenerateCloudByFaceSampling(System::String^ filename, int level, GML::Elements::CTransform4d& transform);
      };
   }
} // end namespace


