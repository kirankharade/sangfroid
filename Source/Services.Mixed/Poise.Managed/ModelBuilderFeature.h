#pragma once

#include "ModelBuilderTouchPoint.h"
#include <sstream>
#include <string.h>
#include <iostream>
#include <vector>

#pragma warning( disable : 4251)

/*-----------------------------------------------------------------------------------*/
using namespace std;
/*-----------------------------------------------------------------------------------*/
namespace Poise { namespace PoiseManaged {
/*-----------------------------------------------------------------------------------*/

enum class EFeatureGeometryType
{
   Plane = 1,
   Cylinder = 2,
   Cone = 3,
   Circle = 4,
   Sphere = 5,
   Unknown = 99999
};
/*-----------------------------------------------------------------------------------*/

class ModelBuilderFeature
{

protected:
   std::vector<ModelBuilderTouchPoint> m_touchPoints;
   int m_index;
   static int s_featureCount;

public:

   ModelBuilderFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints, int index = -1);
   virtual ~ModelBuilderFeature();

   std::vector<ModelBuilderTouchPoint> GetTouchPoints() const;
   virtual EFeatureGeometryType GetType() const = 0;
   int Index() const;

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class PlaneFeature : public ModelBuilderFeature
{

protected:
   GML::Elements::CPoint3d m_point;
   GML::Elements::CVector3d m_normal;

public:

   PlaneFeature(
      const std::vector<ModelBuilderTouchPoint>& touchPoints,
      const GML::Elements::CPoint3d& point,
      const GML::Elements::CVector3d& normal,
      int index = -1);

   EFeatureGeometryType GetType() const;
   GML::Elements::CPoint3d GetPoint() const;
   GML::Elements::CVector3d GetNormal() const;

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class CylinderFeature : public ModelBuilderFeature
{
protected:

   GML::Elements::CPoint3d m_point;
   GML::Elements::CVector3d m_axis;
   double m_radius;
   bool m_isBore;

public:

   CylinderFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints,
      const GML::Elements::CPoint3d& point,
      const GML::Elements::CVector3d& axis,
      const double radius,
      const bool isBore,
      int index = -1);

   EFeatureGeometryType GetType() const;
   GML::Elements::CPoint3d GetPoint() const;
   GML::Elements::CVector3d GetAxis() const;
   double GetRadius() const;

   bool IsBore() const;
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class CircleFeature : public ModelBuilderFeature
{

protected:
   GML::Elements::CVector3d m_associatedPlaneNormal;
   GML::Elements::CPoint3d m_centre;
   double m_radius;
   bool m_isBore;

public:

   CircleFeature(
      const std::vector<ModelBuilderTouchPoint>& touchPoints,
      const GML::Elements::CPoint3d& centre,
      const GML::Elements::CVector3d& associatedPlaneNormal,
      const double radius,
      const bool isBore,
      int index = -1);

   EFeatureGeometryType GetType() const;
   GML::Elements::CVector3d GetAssociatedPlaneNormal() const;

   //The centre should be a point on the associated plane.
   GML::Elements::CPoint3d GetCentre() const;
   double GetRadius() const;
   bool IsBore() const;

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class ConeFeature : public ModelBuilderFeature
{

protected:

   //base params
   GML::Elements::CPoint3d m_baseCentre;
   double m_baseRadius;
   GML::Elements::CVector3d m_axis;
   double m_semiAngle;
   bool m_isBore;

   //derived params
   GML::Elements::CPoint3d m_apex;

public:

   ConeFeature(const std::vector<ModelBuilderTouchPoint>& touchPoints,
      const GML::Elements::CPoint3d baseCentre,
      const GML::Elements::CVector3d axis,
      const double baseRadius,
      const double semiAngle,
      const bool isBore,
      int index = -1);

   EFeatureGeometryType GetType() const;
   GML::Elements::CPoint3d GetBaseCenter() const;
   GML::Elements::CPoint3d GetApex() const;
   GML::Elements::CVector3d GetAxis() const;
   double GetBaseRadius() const;
   double GetSemiAngle() const;
   bool IsBore() const;

};
/////////////////////////////////////////////////////////////////////////////////////////////////////////


   }
}

