#pragma once

#define pointer_safety Pointer_safety
#include <memory>
#include <string.h>
#include <iostream>

#include "ModelBuilderFeature.h"
#include "ModelBuilderTouchPoint.h"
#include "pugixml.hpp"

using namespace std;

/*-----------------------------------------------------------------------------------*/
namespace Poise { namespace PoiseManaged {
/*-----------------------------------------------------------------------------------*/

class ModelBuilderIO
{

private:

   std::string                   m_logFileName;
   string                        m_MoveToNewLine;
   string                        m_CurrentStateFileVersion;
   pugi::xml_document            m_CurrentDoc;
   int                           m_currentActionIndex;
   static ModelBuilderIO*        s_instance;

   ModelBuilderIO();

public:

   static ModelBuilderIO* Instance();

   std::string GetLogFilePath() const;

   void SetLogFilePath(std::string logFileLocation);

   void ClearFile();

   const char* SaveState(
      const std::vector<ModelBuilderFeature *> &features,
      const GML::Elements::CPoint3d &machineBoundsMin, 
      const GML::Elements::CPoint3d &machineBoundsMax);

   void WriteFeatureToFile(
      pugi::xml_document& document,
      const GML::Elements::CPoint3d &machineBoundsMin, 
      const GML::Elements::CPoint3d &machineBoundsMax, 
      ModelBuilderFeature*, int index = -1);

   void WriteFeatureToFile(
      const GML::Elements::CPoint3d &machineBoundsMin, 
      const GML::Elements::CPoint3d &machineBoundsMax, 
      ModelBuilderFeature*);

private:

   void initialize(pugi::xml_document& document);

   void SaveToFile(std::string filePath);

   void serializeXMLPlane(PlaneFeature* pd, pugi::xml_node featureNode);
   void serializeXMLCylinder(CylinderFeature* cd, pugi::xml_node featureNode);
   void serializeXMLCircle(CircleFeature* cd, pugi::xml_node featureNode);
   void serializeXMLCone(ConeFeature* coneDef, pugi::xml_node featureNode);
   void serializeXMLPoint(GML::Elements::CPoint3d pnt, pugi::xml_node pointNode);
   void serializeXMLVector(GML::Elements::CVector3d vec, pugi::xml_node vectorNode);
   void serializeXMLTouchPoints(const std::vector<ModelBuilderTouchPoint>& tps, pugi::xml_node node);

};
/*-----------------------------------------------------------------------------------*/
} }
