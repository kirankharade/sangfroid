﻿#pragma once

#include <iostream>
#include "PoiseManagedNamespace.h"
#include "PoiseExportsDefs.h"
#include "PointCloud.h"
#include "CloudVolume.h"
#include "PointBuffer.h"
#include "GML_Elements\GML_Elements_Code.h"

using namespace std;

POISE_NET_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

ref class PoiseManagedUtils
{
    public:

       static std::string ToStdString(System::String^ string)
       {
          using System::Runtime::InteropServices::Marshal;
          System::IntPtr pointer = Marshal::StringToHGlobalAnsi(string);
          char* charPointer = reinterpret_cast<char *>(pointer.ToPointer());
          std::string returnString(charPointer, string->Length);
          Marshal::FreeHGlobal(pointer);
          return returnString;
       }

       static System::String ^ ToSystemString(const std::string& string)
       {
          return gcnew System::String(string.c_str());
       }

};
/*-----------------------------------------------------------------------------------*/

POISE_NET_NAMESPACE_END

