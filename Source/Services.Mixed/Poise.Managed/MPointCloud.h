#pragma once

#define pointer_safety Pointer_safety
#include <memory>

#include "GML_Elements/GML_Elements_Code.h"
#include "PointCloud.h"
#include "TriangleMesh.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace PoiseManaged {

      public ref class MPointCloud
      {
      private:

         PointCloud* m_unmanaged;

      public:

         MPointCloud(PointCloud* unmanaged);

         ~MPointCloud();

         !MPointCloud();

         PointCloud* Unmanaged()
         {
            return m_unmanaged;
         }

      };
   }
} // end namespace

