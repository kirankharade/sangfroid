#include "MRunner.h"
#include "MPointCloud.h"
#include "IPointDataStream.h"
#include "PointCloud.h"
#include "Reader.h"
#include "TriangleMesh.h"
#include "STLDataStream.h"
#include "GmlCollection.h"
#include <msclr\marshal_cppstd.h>
#include "MathsFitting.h"
#include "ModelBuilderFeature.h"
#include "ModelBuilderTouchPoint.h"
#include "ModelBuilderIO.h"
#include "Reader.h"
#include "PoiseManagedUtils.h"
#include "MAssortedRenderer.h"
#include "../Poise/Curve.h"
#include "../Poise/PointBuffer.h"
#include "../Poise/NormalBuffer.h"
#include "../Poise/PoiseUtils.h"
#include "../Poise/dirent.h"
#include <fstream>
#include <vector>

using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Poise::PoiseManaged;
using namespace Poise;
using namespace Poise::IO;
using namespace GML::Elements;
using namespace std;
using System::Collections::Generic::List;
/*-----------------------------------------------------------------------------------*/

static std::string _resultsLocation = "E:/FeatureSeparation/TempResults/";
static std::string _currDir = "";

/*-----------------------------------------------------------------------------------*/

TriangleMesh* readMesh(Reader* reader, System::String^ triangulationFile)
{
   auto trFile = PoiseManagedUtils::ToStdString(triangulationFile);
   auto stlStream = reader->Load(trFile.c_str(), "STL");
   TriangleMesh* originalMesh = ((STLDataStream*)stlStream)->Mesh();
   return originalMesh;
}
/*-----------------------------------------------------------------------------------*/

PointCloud* readCloud(Reader* reader, System::String^ pointCloudFile)
{
   auto pcFile = PoiseManagedUtils::ToStdString(pointCloudFile);
   auto fpStream = (IPointDataStream*)reader->Load(pcFile.c_str(), "ASCII-cloud");

   auto min = fpStream->Min();
   auto max = fpStream->Max();
   auto numPoints = fpStream->PointCount();
   if (numPoints == 0)
   {
      return nullptr;
   }

   float* buf = new float[numPoints * 3];
   int count = 0;
   auto pt = (GML::Elements::CPoint3f) fpStream->First();
   buf[(3 * count) + 0] = pt.x;
   buf[(3 * count) + 1] = pt.y;
   buf[(3 * count) + 2] = pt.z;
   count++;
   while (count < numPoints)
   {
      pt = (GML::Elements::CPoint3f) fpStream->Next();
      buf[(3 * count) + 0] = pt.x;
      buf[(3 * count) + 1] = pt.y;
      buf[(3 * count) + 2] = pt.z;
      count++;
   }
   auto posBuffer = new PointBuffer(buf, numPoints);
   PointCloud* cloud = new PointCloud(posBuffer);
   return cloud;
}
/*-----------------------------------------------------------------------------------*/

MTriangleMesh^ MRunner::CloudToMesh(System::String^ pointCloudFile, bool generateMissingCurves)
{
   auto pcFile = PoiseManagedUtils::ToStdString(pointCloudFile);

   auto reader = new Reader();
   auto cloud = readCloud(reader, pointCloudFile);
   auto outSTLFile = pcFile + ".stl";

   auto unmanMesh = cloud->ToMesh(outSTLFile.c_str(), generateMissingCurves);
   return gcnew MTriangleMesh(unmanMesh);
}
/*-----------------------------------------------------------------------------------*/

List<MRenderPointSet^>^ MRunner::CloudToZCurves(System::String^ pointCloudFile)
{
	auto psets = gcnew List<MRenderPointSet^>();

	auto pcFile = PoiseManagedUtils::ToStdString(pointCloudFile);

	auto reader = new Reader();
	auto cloud = readCloud(reader, pointCloudFile);

	auto t = cloud->CloudToZCurves();
	auto zCurves = get<0>(t);
	auto zCurvesCount = get<1>(t);

	for(int i = 0; i < zCurvesCount; i++)
	{
		auto& pts = zCurves[i].Points();
		auto list = gcnew List<Point3>();
		for (auto& pt : pts)
		{
			auto p = Point3(pt.x, pt.y, pt.z);
			list->Add(p);
		}
		auto rps = gcnew MRenderPointSet(list);
		psets->Add(rps);
	}

	return psets;
}
/*-----------------------------------------------------------------------------------*/

List<MRenderPointSet^>^ MRunner::CloudToRCurves(System::String^ pointCloudFile)
{
	auto psets = gcnew List<MRenderPointSet^>();

	auto pcFile = PoiseManagedUtils::ToStdString(pointCloudFile);

	auto reader = new Reader();
	auto cloud = readCloud(reader, pointCloudFile);

	auto t = cloud->CloudToRCurves();
	auto zCurves = get<0>(t);
	auto zCurvesCount = get<1>(t);

	for (int i = 0; i < zCurvesCount; i++)
	{
		auto& pts = zCurves[i].Points();
		auto list = gcnew List<Point3>();
		for (auto& pt : pts)
		{
			auto p = Point3(pt.x, pt.y, pt.z);
			list->Add(p);
		}
		auto rps = gcnew MRenderPointSet(list);
		psets->Add(rps);
	}

	return psets;
}
/*-----------------------------------------------------------------------------------*/

void MRunner::SeparateAngularSectorRegions(System::String^ pointCloudFile, int divisions, double angleOffsets)
{
	auto pcFile = PoiseManagedUtils::ToStdString(pointCloudFile);
	auto reader = new Reader();
	auto cloud = readCloud(reader, pointCloudFile);
	cloud->SeparateAngularSectorRegions(divisions, angleOffsets);
}
/*-----------------------------------------------------------------------------------*/

void MRunner::Run(System::String^ pointCloudFile, System::String^ triangulationFile, System::String^ resultLocation)
{
   auto subdir = PoiseUtils::GetCurrentTimeString();
   _currDir = _resultsLocation + "Res_" + subdir;
   PoiseUtils::CreateFolder(_currDir);

   auto reader = new Reader();

   //////////////////////////////////////////////////////////////////////
   //Optional step
   //Read fitted mesh
   //auto mesh = readMesh(reader, triangulationFile);
   //auto refinedMesh = mesh->RemoveBigTriangles(1.0f, false);
   //refinedMesh->WriteToSTL("C:/refined_traingulation_xxx.stl");
   //////////////////////////////////////////////////////////////////////

   //Read the cloud
   auto cloud = readCloud(reader, pointCloudFile);
   auto boundsMin = cloud->Positions()->Min();
   auto boundsMax = cloud->Positions()->Max();

   //Read the fitted triangulation
   auto trFile = PoiseManagedUtils::ToStdString(triangulationFile);
   auto stlStream = reader->Load(trFile.c_str(), "STL");
   auto mesh = ((STLDataStream*)stlStream)->Mesh();

   //Associate cloud with the fitted mesh
   mesh->Associate(cloud);

   //Subdivide the triangulation (mesh) based on detected edges, Edge detection threshold = 12.0f degrees
   float edgeDetectionAngle = 12.0f;
   int outMeshCount = 0;
   auto outMeshes = mesh->DivideIntoFaceTriangulations(edgeDetectionAngle, outMeshCount);
   for (int im = 0; im < outMeshCount; im++)
   {
      auto str = _currDir + "/" + "Out_" + std::to_string(im) + ".stl";
      outMeshes[im]->WriteToSTL(str.c_str());
   }

   auto boundaryMesh = mesh->BoundaryTriangles();
   auto bmLoc = _currDir + "/" + "boundary_mesh.stl";
   boundaryMesh->WriteToSTL(bmLoc.c_str());

   //Corelate the cloud points with mesh triangles to subdivide cloud into separate regions
   int outCloudCount = 0;
   float nearnessThreshold = 0.02f;
   auto outClouds = mesh->SeparateCloudRegions(nearnessThreshold, outCloudCount);
   //for (int ic = 0; ic < outCloudCount; ic++)
   //{
   //   auto str = _currDir + "/" + "Out_" + std::to_string(ic) + ".xyz";
   //   outClouds[ic]->Save(str.c_str());
   //}

   //Fit features using metrology maths...
   std::vector<ModelBuilderFeature*> features;

   auto cloudsToConvert = min(outCloudCount, 12);

   for (int j = 0; j < cloudsToConvert; j++)
   {
      auto str = _currDir + "/" + "Out_" + std::to_string(j) + ".xyz";
      outClouds[j]->Save(str.c_str());

      //Generate a list of touch-points in the form of gmlCollections
      auto cloud = outClouds[j];
      auto posBuffer = cloud->Positions();
      auto nmlBuffer = cloud->Normals();
      auto numPoints = posBuffer->PointCount();

      std::vector<GML::Elements::CPoint3d> points;
      std::vector<GML::Elements::CVector3d> normals;
      std::vector<ModelBuilderTouchPoint> tps;
      std::vector<double> offsets;

      for (int ip = 0; ip < numPoints; ip++)
      {
         auto pt = posBuffer->PointAt(ip);
         auto nm = nmlBuffer->NormalAt(ip);
         points.push_back(GML::Elements::CPoint3d(pt.x, pt.y, pt.z));
         normals.push_back(GML::Elements::CVector3d(nm.i, nm.j, nm.k));
         offsets.push_back(0.0f);

         ModelBuilderTouchPoint tp(GML::Elements::CPoint3d(pt.x, pt.y, pt.z), GML::Elements::CVector3d(nm.i, nm.j, nm.k), 0.0);
         tps.push_back(tp);
      }

      if (tps.size() == 0 || points.size() == 0)
      {
         continue;
      }
      GmlCollection<CPoint3d> ptCollection = GmlCollection<CPoint3d>(points);
      GmlCollection<CVector3d> normalsCollection = GmlCollection<CVector3d>(normals);
      GmlCollection<double> offsetsCollection = GmlCollection<double>(offsets);

      auto feature = MathsFitting::FitSomething(tps, ptCollection, normalsCollection, offsetsCollection);
      features.push_back(feature);
   }

   auto bmin = CPoint3d((double)boundsMin.x, (double)boundsMin.y, (double)boundsMin.z);
   auto bmax = CPoint3d((double)boundsMax.x, (double)boundsMax.y, (double)boundsMax.z);

   //Dump the features into file.
   auto stream = ModelBuilderIO::Instance()->SaveState(features, bmin, bmax);
   std::string bufferStr(stream);
   int size = (int) bufferStr.size();

   auto mbfile = _currDir + "/fittedFeatures.xml";
   remove(mbfile.c_str());
   std::ofstream outfile(mbfile);
   outfile.write(stream, size);
   outfile.flush();
   outfile.close();

}
/*-----------------------------------------------------------------------------------*/

void MRunner::GenerateCloud(System::String^ triangulationFile, int level)
{
   auto reader = new Reader();
   auto trFile = PoiseManagedUtils::ToStdString(triangulationFile);
   auto stlStream = reader->Load(trFile.c_str(), "STL");
   auto mesh = ((STLDataStream*)stlStream)->Mesh();
   auto outfile = trFile + ".xyz";

   GML::Elements::CTransform4d transform;
   transform = transform.Identity();
   mesh->GenerateCloudByFaceSampling(outfile.c_str(), level, transform);

   auto outTransformedfile = trFile + "_transformed.xyz";
   transform = transform.CreateRotation(CVector3d(0, 0, 1), 0.25 * 3.142);
   mesh->GenerateCloudByFaceSampling(outTransformedfile.c_str(), level, transform);

}
/*-----------------------------------------------------------------------------------*/
