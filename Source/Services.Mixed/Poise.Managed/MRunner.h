#pragma once

#define pointer_safety Pointer_safety
#include <memory>

#include "GML_Elements/GML_Elements_Code.h"
#include "MTriangleMesh.h"
#include "MAssortedRenderer.h"

using System::String;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;
using namespace GML::Elements::NET;

namespace Poise {
   namespace PoiseManaged {

      public ref class MRunner
      {

      public:

         MRunner() {}

         ~MRunner() {}

         !MRunner() {}

         void Run(System::String^ pointCloudFile, System::String^ triangulationFile, System::String^ resultLocation);

         MTriangleMesh^ CloudToMesh(System::String^ pointCloudFile, bool generateMissingCurves);

		 List<MRenderPointSet^>^ CloudToZCurves(System::String^ pointCloudFile);

		 List<MRenderPointSet^>^ CloudToRCurves(System::String^ pointCloudFile);

		 void SeparateAngularSectorRegions(System::String^ pointCloudFile, int divisions, double angleOffsets);

		 void GenerateCloud(System::String^ triangulationFile, int level);

	  };
   }
} // end namespace


