#pragma once
#include "GML_ELements/GML_Elements_Code.h"
#include "ModelBuilderFeature.h"
#include "GmlCollection.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace Poise {
   namespace PoiseManaged {

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


class MathsFitting
{

public:
   static ModelBuilderFeature* FitSomething(
      const std::vector<ModelBuilderTouchPoint>& tps,
      GmlCollection<CPoint3d>& ptCollection, 
      GmlCollection<CVector3d>& normalsCollection, 
      GmlCollection<double>& offsetsCollection);

   static ModelBuilderFeature* FitPlane(
      const std::vector<ModelBuilderTouchPoint>& tps,
      GmlCollection<CPoint3d>& ptCollection,
      GmlCollection<CVector3d>& normalsCollection,
      GmlCollection<double>& offsetsCollection);

   static ModelBuilderFeature* FitCylinder(
      const std::vector<ModelBuilderTouchPoint>& tps,
      GmlCollection<CPoint3d>& ptCollection,
      GmlCollection<CVector3d>& normalsCollection,
      GmlCollection<double>& offsetsCollection);

   static ModelBuilderFeature* FitCone(
      const std::vector<ModelBuilderTouchPoint>& tps,
      GmlCollection<CPoint3d>& ptCollection,
      GmlCollection<CVector3d>& normalsCollection,
      GmlCollection<double>& offsetsCollection);

   //static ModelBuilderFeature* FitSphere(
   //   const std::vector<ModelBuilderTouchPoint>& tps,
   //   GmlCollection<CPoint3d>& ptCollection,
   //   GmlCollection<CVector3d>& normalsCollection,
   //   GmlCollection<double>& offsetsCollection);

   static std::vector<ModelBuilderTouchPoint> SampleTouchPoints(const std::vector<ModelBuilderTouchPoint>& tps, int maxTPLimit);

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

} }

