﻿#pragma once

#include "GML_Elements\GML_Elements_Code.h"
/*-----------------------------------------------------------------------------------*/

using namespace std;

/*-----------------------------------------------------------------------------------*/
namespace Poise { namespace PoiseManaged {
/*-----------------------------------------------------------------------------------*/


struct ModelBuilderTouchPoint
{
   GML::Elements::CPoint3d TipCentreLocation;
   GML::Elements::CVector3d SurfaceNormal;
   double TipRadius;
   GML::Elements::CVector3d StylusOrientation;

   ModelBuilderTouchPoint();

   ModelBuilderTouchPoint(const GML::Elements::CPoint3d &touch,
      const GML::Elements::CVector3d &surfNormal,
      double tipRadius);

   ModelBuilderTouchPoint(const GML::Elements::CPoint3d &touch,
      const GML::Elements::CVector3d &surfNormal,
      double tipRadius,
      const GML::Elements::CVector3d &stylusOrientation);

   bool operator==(const ModelBuilderTouchPoint& a);
};

//bool operator==(const ModelBuilderTouchPoint& a, const ModelBuilderTouchPoint &b);

/*-----------------------------------------------------------------------------------*/
}}