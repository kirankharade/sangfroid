#pragma once

#define pointer_safety Pointer_safety
#include <memory>
#include "PoiseManagedNamespace.h"
#include "GML_Elements/GML_Elements_Code.h"
#include "GML_Elements.NET\GML_Elements.NET.h"
#include "AssortedRenderer.h"
#include "PoiseManagedUtils.h"
#include <vector>

using namespace GML::Elements::NET;
using namespace GLiDE::NET;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using System::Collections::Generic::List;
using System::Collections::Generic::Dictionary;

/*-----------------------------------------------------------------------------------*/
POISE_NET_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/

public enum class ERenderableType
{
   Mesh,
   PointSet,
   Point,
   Line,
   Text,
   Unknown
};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderable
{
public:

   System::String^ Name;
   System::Drawing::Color Color;
   Renderable* m_renderEntity;
   Drawable^ _drawable;

public:

   property bool Visible
   {
      bool get()
      {
         return m_renderEntity->Visible;
      }
      void set(bool value)
      {
         m_renderEntity->Visible = value;
      }
   }

   virtual ERenderableType RenderableType()
   {
      return ERenderableType::Unknown;
   }

   virtual Drawable^ GetDrawable()
   {
      return _drawable;
   }

   static System::Drawing::Color GetColor(ERenderColor color)
   {
      if (color == ERenderColor::Black) return System::Drawing::Color::Black;
      if (color == ERenderColor::Blue) return System::Drawing::Color::Blue;
      if (color == ERenderColor::Cyan) return System::Drawing::Color::Cyan;
      if (color == ERenderColor::Green) return System::Drawing::Color::Green;
      if (color == ERenderColor::Red) return System::Drawing::Color::Red;
      if (color == ERenderColor::Orange) return System::Drawing::Color::Orange;
      if (color == ERenderColor::Violet) return System::Drawing::Color::Violet;
      if (color == ERenderColor::White) return System::Drawing::Color::White;
      if (color == ERenderColor::Yellow) return System::Drawing::Color::Yellow;

      return System::Drawing::Color::Black;
   }

   MRenderable(Renderable* renderEntity)
      : m_renderEntity(renderEntity),
      _drawable(nullptr)
   {
      if (nullptr != m_renderEntity)
      {
         Color = GetColor(m_renderEntity->Color);
         if (m_renderEntity->Name.size() > 0)
         {
            MRenderable::Name = gcnew System::String(m_renderEntity->Name.c_str());
         }
      }
   }

   void Release()
   {
      Renderable::Destroy(m_renderEntity);
   }

};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderLine : public MRenderable
{
public:

   Point3 Va;
   Point3 Vb;


public:

   MRenderLine(RenderLine* line)
      :MRenderable(line)
   {
      Va = Point3(line->pa.x, line->pa.y, line->pa.z);
      Vb = Point3(line->pb.x, line->pb.y, line->pb.z);
   }

   ERenderableType RenderableType() override
   {
      return ERenderableType::Line;
   }

};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderPoint : public MRenderable
{
public:

   Point3 Vertex;

public:

   MRenderPoint(RenderPoint* point)
      :MRenderable(point)
   {
      _drawable = nullptr;
      Vertex = Point3(point->p.x, point->p.y, point->p.z);

      auto points = gcnew System::Collections::Generic::List<Point3>();
      auto normals = gcnew System::Collections::Generic::List<Vector3>();
      auto colors = gcnew System::Collections::Generic::List<System::Drawing::Color>();
      points->Add(Vertex);
      colors->Add(Color);
      _drawable = gcnew PointSet(points, colors, normals);
   }

   ERenderableType RenderableType() override
   {
      return ERenderableType::Point;
   }

};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderText : public MRenderable
{
public:

   Point3 Vertex;
   System::String^ Text;

public:

   MRenderText(RenderText* txt)
      :MRenderable(txt)
   {
      Vertex = Point3(txt->p.x, txt->p.y, txt->p.z);
      Text = PoiseManagedUtils::ToSystemString(txt->Text);
      _drawable = gcnew Text2D(Text);
   }

   ERenderableType RenderableType() override
   {
      return ERenderableType::Text;
   }
};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderMesh : public MRenderable
{
public:

   List<Point3>^ Vertices;
   List<Vector3>^ Normals;
   List<int>^ Indices;

public:
   MRenderMesh(List<Point3>^ vs, List<Vector3>^ ns, List<int>^ is)
      :MRenderable(nullptr), Vertices(vs), Normals(ns), Indices(is)
   {
      _drawable = gcnew Triangulation(Vertices, Indices, Normals, nullptr, nullptr);
   }

   MRenderMesh(List<Point3>^ vs, List<Vector3>^ ns, List<int>^ is, System::Drawing::Color color)
      :MRenderable(nullptr), Vertices(vs), Normals(ns), Indices(is)
   {
      _drawable = gcnew Triangulation(Vertices, Indices, Normals, nullptr, nullptr);
      MRenderable::Color = color;
   }

   MRenderMesh(RenderMesh* mesh)
      :MRenderable(mesh)
   {

      Vertices = gcnew List<Point3>();
      Normals = gcnew List<Vector3>();
      Indices = gcnew List<int>();

      if (mesh->Indices.size() == 0 || mesh->Normals.size() == 0)
      {
         auto triangleCount = (int)(mesh->Vertices.size() / 3);
         auto index = 0;

         for (int t = 0; t < triangleCount; ++t)
         {
            auto va = mesh->Vertices[(t * 3) + 0];
            auto vb = mesh->Vertices[(t * 3) + 1];
            auto vc = mesh->Vertices[(t * 3) + 2];

            Vertices->Add(Point3(va.x, va.y, va.z));
            Vertices->Add(Point3(vb.x, vb.y, vb.z));
            Vertices->Add(Point3(vc.x, vc.y, vc.z));

            auto normal = NormalFrom3Points(va, vb, vc);
            Normals->Add(Vector3(normal.i, normal.j, normal.k));

            Indices->Add(index); index++;
            Indices->Add(index); index++;
            Indices->Add(index); index++;
         }
      }
      else
      {
         for (auto v : mesh->Vertices)
         {
            Vertices->Add(Point3(v.x, v.y, v.z));
         }
         for (auto i : mesh->Indices)
         {
            Indices->Add(i);
         }
         for (auto n : mesh->Normals)
         {
            Normals->Add(Vector3(n.i, n.j, n.k));
         }
      }
      _drawable = gcnew Triangulation(Vertices, Indices, Normals, nullptr, nullptr);
   }

   ERenderableType RenderableType() override
   {
      return ERenderableType::Mesh;
   }

   static GML::Elements::CVector3d NormalFrom3Points(
      const GML::Elements::CPoint3d& a,
      const GML::Elements::CPoint3d& b,
      const GML::Elements::CPoint3d& c)
   {
      auto normal = (b - a).Cross(c - b);
      normal.Normalise();
      return normal;
   }

};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderPointSet : public MRenderable
{
public:

   List<Point3>^ Vertices;
   bool RenderLines;

public:

   MRenderPointSet(List<Point3>^ vs)
      :MRenderable(nullptr), Vertices(vs), RenderLines(false)
   {
      _drawable = gcnew PointSet(Vertices, nullptr, nullptr);
   }

   MRenderPointSet(RenderPointSet* ps)
      :MRenderable(ps)
   {
      RenderLines = ps->RenderLines;
      Vertices = gcnew List<Point3>();
      for (auto v : ps->Vertices)
      {
         Vertices->Add(Point3(v.x, v.y, v.z));
      }

      _drawable = gcnew PointSet(Vertices, nullptr, nullptr);
   }

   ERenderableType RenderableType() override
   {
      return ERenderableType::PointSet;
   }

};
/*-----------------------------------------------------------------------------------*/

public ref class MRenderGroup
{
public:

   System::String^ Tag;
   RenderGroup* m_renderGroup;
   List<MRenderable^>^ m_renderables;

public:

   property bool Visible
   {
      bool get()
      {
         return m_renderGroup->IsVisible();
      }

      void set(bool value)
      {
         if (value)
            m_renderGroup->Show();
         else
            m_renderGroup->Hide();
      }
   }

   property List<MRenderable^>^ Renderables
   {
      List<MRenderable^>^ get()
      {
         return m_renderables;
      }
   }

   MRenderGroup(RenderGroup* grp)
      : m_renderGroup(grp)
   {
      Tag = PoiseManagedUtils::ToSystemString(grp->Tag);

      m_renderables = gcnew List<MRenderable^>();

      auto renderableCount = grp->Renderables.size();

      for (int i = 0; i < renderableCount; i++)
      {
         Renderable* r = grp->Renderables[i];
         RenderMesh* mr = dynamic_cast<RenderMesh*>(r);
         if (mr)
         {
            auto md = gcnew MRenderMesh(mr);
            m_renderables->Add(md);
            continue;
         }

         RenderPointSet* mps = dynamic_cast<RenderPointSet*>(r);
         if (mps)
         {
            auto md = gcnew MRenderPointSet(mps);
            m_renderables->Add(md);
            continue;
         }

         RenderLine* lr = dynamic_cast<RenderLine*>(r);
         if (lr)
         {
            auto md = gcnew MRenderLine(lr);
            m_renderables->Add(md);
         }

         RenderPoint* pr = dynamic_cast<RenderPoint*>(r);
         if (pr)
         {
            auto md = gcnew MRenderPoint(pr);
            m_renderables->Add(md);
         }

         RenderText* tr = dynamic_cast<RenderText*>(r);
         if (tr)
         {
            auto md = gcnew MRenderText(tr);
            m_renderables->Add(md);
         }
      }
   }

   void Release()
   {
      for (int i = 0; i < m_renderables->Count; i++)
      {
         auto r = m_renderables[i];
         Renderable::Destroy(r->m_renderEntity);
      }
      m_renderables->Clear();
   }

};
/*-----------------------------------------------------------------------------------*/

public ref class MAssortedRenderer
{
public:

   List<MRenderGroup^>^ m_groups;

   MAssortedRenderer()
   {
      m_groups = gcnew List<MRenderGroup^>();
   }

   ~MAssortedRenderer()
   {
      m_groups->Clear();
      delete m_groups;
   }

   void populate()
   {
      if (AssortedRenderer::Dirty())
      {
         m_groups = populateGroups();
         AssortedRenderer::set_Dirty(false);
      }
   }

   List<MRenderGroup^>^ populateGroups()
   {
      m_groups->Clear();
      auto mGroups = gcnew List<MRenderGroup^>();
      auto groups = AssortedRenderer::Groups();
      for (auto& grp : groups)
      {
         mGroups->Add(gcnew MRenderGroup(grp));
      }
      return mGroups;
   }

   property List<MRenderGroup^>^ Groups
   {
      List<MRenderGroup^>^ get()
      {
         populate();
         return m_groups;
      }
   }

   property bool Dirty
   {
      bool get()
      {
         return AssortedRenderer::Dirty();
      }
   }

   property bool Enabled
   {
      bool get()
      {
         return AssortedRenderer::Enabled();
      }
      void set(bool value)
      {
         AssortedRenderer::Enable(value);
      }
   }

   void ClearAll()
   {
      AssortedRenderer::ClearAll();
   }

   void HideAll()
   {
      AssortedRenderer::HideAll();
   }

   void ShowAll()
   {
      AssortedRenderer::ShowAll();
   }

};
/*-----------------------------------------------------------------------------------*/
POISE_NET_NAMESPACE_END
/*-----------------------------------------------------------------------------------*/

