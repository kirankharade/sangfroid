#include "MTriangleMesh.h"
#include "MPointCloud.h"
#include "PointCloud.h"
#include "TriangleMesh.h"
#include <msclr\marshal_cppstd.h>

using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Poise::PoiseManaged;
using namespace Poise;

MPointCloud::MPointCloud(Poise::PointCloud* unmanaged)
   :m_unmanaged(nullptr)
{
   m_unmanaged = unmanaged;
}

MPointCloud::~MPointCloud()
{
   this->!MPointCloud();
}

MPointCloud::!MPointCloud()
{
   if (m_unmanaged != nullptr)
   {
      delete m_unmanaged;
   }
}

//MTriangleMesh^ MTriangleMesh::RemoveBigTriangles(float thresholdArea)
//{
//   auto mesh = ((TriangleMesh*)m_unmanaged)->RemoveBigTriangles(thresholdArea);
//   return gcnew Poise::PoiseManaged::MTriangleMesh(mesh);
//}
//
//List<MTriangleMesh^>^ MTriangleMesh::DivideIntoFaceTriangulations(float angleDeg)
//{
//   int meshCount = 0;
//   auto meshList = ((TriangleMesh*)m_unmanaged)->DivideIntoFaceTriangulations(angleDeg, meshCount);
//   List<MTriangleMesh^>^ meshes = gcnew List<MTriangleMesh^>();
//
//   for (int i = 0; i < meshCount; i++)
//   {
//      MTriangleMesh^ m = gcnew MTriangleMesh(meshList[i]);
//      meshes->Add(m);
//   }
//   return meshes;
//}

