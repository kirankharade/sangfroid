﻿#include "ModelBuilderTouchPoint.h"

using namespace Poise;
using namespace Poise::PoiseManaged;

/*-----------------------------------------------------------------------------------*/

ModelBuilderTouchPoint::ModelBuilderTouchPoint(const GML::Elements::CPoint3d &touch,
                        const GML::Elements::CVector3d &surfNormal,
                        double tipRadius )
{
   TipCentreLocation = touch;
   SurfaceNormal = surfNormal.GetNormalised();
   TipRadius = tipRadius;

   // When not specified, stylus orientation is Null vector signifying missing data
   StylusOrientation = GML::Elements::CVector3d::Null();
}

ModelBuilderTouchPoint::ModelBuilderTouchPoint()
   :TipRadius(0.0)
{}


ModelBuilderTouchPoint::ModelBuilderTouchPoint(const GML::Elements::CPoint3d &touch,
   const GML::Elements::CVector3d &surfNormal,
                        double tipRadius,
                        const GML::Elements::CVector3d &stylusOrientation)
                        : ModelBuilderTouchPoint(touch, surfNormal, tipRadius)
{
   this->StylusOrientation = stylusOrientation;
}

bool ModelBuilderTouchPoint::operator == (const ModelBuilderTouchPoint &rhs)
{
   auto tolerance = 0.0001;
   return GML::Compare::IsEqual(this->TipCentreLocation, rhs.TipCentreLocation, tolerance) &&
          GML::Compare::IsEqual(this->SurfaceNormal, rhs.SurfaceNormal, tolerance) &&
          GML::Compare::IsEqual(this->TipRadius, rhs.TipRadius, tolerance);
}

/*-----------------------------------------------------------------------------------*/

