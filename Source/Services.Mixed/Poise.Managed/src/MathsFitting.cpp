#include "MathsFitting.h"
#include "../Dependencies/Metrology_Maths/include/Metrology_Maths.h"
#include "../Poise/GmlCollection.h"
#include "../Poise/MathUtils.h"
#include "ModelBuilderFeature.h"
#include "time.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace MetrologyMaths;

using System::Collections::Generic::List;
using System::Math;
using namespace Poise;
using namespace Poise::PoiseManaged;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static const int MaxTPLimit = 50;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ModelBuilderFeature* MathsFitting::FitSomething(
   const std::vector<ModelBuilderTouchPoint>& tps,
   GmlCollection<CPoint3d>& ptCollection,
   GmlCollection<CVector3d>& normalsCollection,
   GmlCollection<double>& offsetsCollection)
{
   auto feature = FitPlane(tps, ptCollection, normalsCollection, offsetsCollection);
   if (feature == nullptr)
   {
      feature = FitCylinder(tps, ptCollection, normalsCollection, offsetsCollection);
      if (feature == nullptr)
      {
         feature = FitCone(tps, ptCollection, normalsCollection, offsetsCollection);
         if (feature == nullptr)
         {
            return nullptr;
         }
      }
   }
   return feature;
}

// adjust Plane centre to be within the convex hull of the touch points
static void adjustPlaneCentre(const vector<CPoint3d> &touchPoints, CPlane *plane)
{
   // make sure that the plane centre point is within the convex hull of the provided points
   CPoint3d c = touchPoints[0];
   for (size_t i = 1; i < touchPoints.size(); ++i)
   {
      c += touchPoints[i];
   }
   c /= (double) touchPoints.size();

   // project the centre onto the fitted plane
   CPoint3f ctemp((float)c.x, (float)c.y, (float)c.z);
   CPoint3f origin((float)plane->origin.x, (float)plane->origin.y, (float)plane->origin.z);
   CVector3f normal((float)plane->normal.i, (float)plane->normal.j, (float)plane->normal.k);

   ctemp = MathUtils::Project(ctemp, origin, normal);
   plane->origin = CPoint3d(ctemp.x, ctemp.y, ctemp.z);

}

ModelBuilderFeature* MathsFitting::FitPlane(
   const std::vector<ModelBuilderTouchPoint>& tps,
   GmlCollection<CPoint3d>& ptCollection,
   GmlCollection<CVector3d>& normalsCollection,
   GmlCollection<double>& offsetsCollection)
{
   CStatistics stats;
   CPlane plane;

   bool result = CFitting::FitPlaneLeastSquare(ptCollection, offsetsCollection, normalsCollection, &plane, &stats);

   double variance = stats.GetVariance();
   double allowableVariance = 3e-2;
   if (!result || std::abs(variance) > allowableVariance)
   {
      return nullptr;
   }

   CPoint3d planePoint = plane.origin;
   CVector3d planeVector = plane.normal;

   auto sampledTps = tps;
   sampledTps = SampleTouchPoints(tps, MaxTPLimit);
   auto sampledTPCount = (int)sampledTps.size();

   PlaneFeature* feature = new PlaneFeature(sampledTps, planePoint, planeVector);

   return feature;
}

ModelBuilderFeature* MathsFitting::FitCylinder(
   const std::vector<ModelBuilderTouchPoint>& tps, 
   GmlCollection<CPoint3d>& ptCollection,
   GmlCollection<CVector3d>& normalsCollection,
   GmlCollection<double>& offsetsCollection)
{
   if (ptCollection.Count() < 6)
   {
      return nullptr;
   }

   CStatistics stats;
   CCylinder cylinder;
   bool result = CFitting::FitCylinderLeastSquare(ptCollection, offsetsCollection, normalsCollection, &cylinder, &stats);
         
   double allowableVariance = 0.1;
   double variance = stats.GetVariance();
   if (!result || std::abs(variance) > allowableVariance)
   {
      return nullptr;
   }

   CPoint3d axisPoint = cylinder.origin;
   CVector3d axisDirection = cylinder.axis;
   double radius = cylinder.radius;
   bool isBore = cylinder.is_bore;

   auto sampledTps = tps;
   sampledTps = SampleTouchPoints(tps, MaxTPLimit);
   auto sampledTPCount = (int) sampledTps.size();

   auto feature = new CylinderFeature(sampledTps, axisPoint, axisDirection, radius, isBore);

   return feature;
}

ModelBuilderFeature* MathsFitting::FitCone(
   const std::vector<ModelBuilderTouchPoint>& tps,
   GmlCollection<CPoint3d>& ptCollection,
   GmlCollection<CVector3d>& normalsCollection,
   GmlCollection<double>& offsetsCollection)
{
   if (ptCollection.Count() < 7)
   {
      return nullptr;
   }

   CStatistics stats;
   CCone cone;
   bool result = CFitting::FitConeLeastSquare(ptCollection, offsetsCollection, normalsCollection, &cone, &stats);

   double allowableVariance = 0.2;
   double variance = stats.GetVariance();
   if (!result || std::abs(variance) > allowableVariance)
   {
      return nullptr;
   }	

   auto baseCentre = cone.origin;
   auto coneAxis = cone.axis.GetNormalised();
   auto semiAngle = cone.half_angle;
   auto baseRadius = cone.radius;
   auto isBore = cone.is_bore;

   //Now cone fitting convention from MetrologyMaths is 
   //If semi-angle is positive, the axis direction is base-->apex
   //If semi-angle is negative, the axis direction is apex-->base

   //Albertson convention is that - the axis direction is from apex-->base
   //we will be changing axis direction accordingly...

   if (semiAngle > 0)
   {
      //base-->apex
      coneAxis = coneAxis.Negate();
   }
   //else,...keep as it is...
   semiAngle = fabs(semiAngle);


   auto sampledTps = tps;
   sampledTps = SampleTouchPoints(tps, MaxTPLimit);

   auto feature = new ConeFeature(sampledTps, baseCentre, coneAxis, baseRadius, semiAngle, isBore);

   return feature;
}

std::vector<ModelBuilderTouchPoint> MathsFitting::SampleTouchPoints(
   const std::vector<ModelBuilderTouchPoint>& tps, 
   int maxTPLimit)
{
   std::vector<ModelBuilderTouchPoint> sampledTouchPoints;
   auto tpCount = tps.size();
   srand(time(NULL));

   for (int i = 0; i < maxTPLimit; i++)
   {
      auto r = abs(std::rand());
      auto index = (r % tpCount) + 1;
      index = std::min(index, tpCount - 1);

      auto& tp = tps[index];
      sampledTouchPoints.push_back(tp);
   }

   return sampledTouchPoints;
}



//void FittedCone::Initialize()
//{
//   CPoint3d topCenter, baseCenter;
//
//   double radius_min = DBL_MAX;
//   double radius_max = -DBL_MAX;
//
//   CPoint3d base(BaseCenter.X, BaseCenter.Y, BaseCenter.Z);
//   CVector3d axis(Axis.I, Axis.J, Axis.K);
//   double coneHtAtCenter = abs(BaseRadius / std::tan(SemiAngle));
//
//   for (int i = 0; i < m_touches->Count; i++)
//   {
//      CPoint3d tp(m_touches[i].Touch.X, m_touches[i].Touch.Y, m_touches[i].Touch.Z);
//
//      CPoint3d ptOnAxisLine = MathExtensions::ProjectOnLine(base, axis, tp);
//      double radius = Distance::PointToPoint(tp, ptOnAxisLine);
//
//      if (radius < radius_min)
//      {
//         radius_min = radius;
//         topCenter = ptOnAxisLine;
//      }
//
//      if (radius > radius_max)
//      {
//         radius_max = radius;
//         baseCenter = ptOnAxisLine;
//      }
//   }
//
//   CVector3d baseToTopDir = (topCenter - baseCenter).GetNormalised();
//   axis = baseToTopDir.Dot(axis) > 0.0 ? axis : -axis;
//
//   double coneHeight = GML::Distance::PointToPoint(topCenter, baseCenter);
//   baseCenter = baseCenter - (axis * coneHeight * 0.1);
//   coneHeight += coneHeight * 0.1;
//
//   CPoint3d apex = base + (axis * coneHtAtCenter);
//
//   double topHeight = GML::Distance::PointToPoint(topCenter, apex);
//   double baseHeight = GML::Distance::PointToPoint(baseCenter, apex);
//
//   double topRadius = abs(topHeight * std::tan(SemiAngle));
//   double baseRadius = abs(baseHeight * std::tan(SemiAngle));
//         
//   MPoint3D point(baseCenter.x, baseCenter.y, baseCenter.z);
//   MVector3D vec(axis.i, axis.j, axis.k);
//
//   m_topRadius = topRadius;
//   m_height = coneHeight;
//}
//

//ModelBuilderFeature* MathsFitting::FitSphere(
//   const std::vector<ModelBuilderTouchPoint>& tps,
//   GmlCollection<CPoint3d>& ptCollection,
//   GmlCollection<CVector3d>& normalsCollection,
//   GmlCollection<double>& offsetsCollection)
//{
//   if (ptCollection.Count() < 7)
//   {
//      return nullptr;
//   }
//
//   CStatistics stats;
//   CSphere sphere;
//   bool result = CFitting::FitSphereLeastSquare(ptCollection, offsetsCollection, normalsCollection, &sphere, &stats);
//
//   double variance = stats.GetVariance();
//   if (!result || std::abs(variance) > 1e-5)
//   {
//      return nullptr;
//   }
//
//   CPoint3d centre = sphere.centre;
//   double radius = sphere.radius;
//   bool isBore = sphere.is_bore;
//
//   auto feature = new SphereFeature(tps, );
//
//   return feature;
//}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


