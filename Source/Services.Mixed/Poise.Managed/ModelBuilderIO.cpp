
#include "ModelBuilderIO.h"
#include "MathUtils.h"
#include "ModelBuilderFeature.h"
#include "pugixml.hpp"
#include "pugiconfig.hpp"

#include <sstream>
#include <assert.h>

namespace gml = GML::Elements;

/*-----------------------------------------------------------------------------------*/
using namespace Poise;
using namespace Poise::PoiseManaged;
/*-----------------------------------------------------------------------------------*/

ModelBuilderIO* ModelBuilderIO::s_instance = nullptr;
static std::string FILE_TYPE = "ModelBuilder Log";

/*-----------------------------------------------------------------------------------*/

ModelBuilderIO* ModelBuilderIO::Instance()
{
   if(nullptr == s_instance)
   {
      s_instance = new ModelBuilderIO();
   }
   return s_instance;
}

ModelBuilderIO::ModelBuilderIO()
{
   m_MoveToNewLine = "\n";
   m_CurrentStateFileVersion = "1.0";
   m_logFileName = "";
   m_currentActionIndex = 0;
   initialize(m_CurrentDoc);
}

void ModelBuilderIO::initialize(pugi::xml_document& document)
{
   pugi::xml_node mbNode = document.append_child("ModelBuilder");
   pugi::xml_node metadataNode = mbNode.append_child("Metadata");

   metadataNode.append_child("FileType").text() = FILE_TYPE.c_str();
   metadataNode.append_child("FileVersion").text() = "1.0";   

   mbNode.append_child("Features");
   mbNode.append_child("Session");
}

std::string ModelBuilderIO::GetLogFilePath() const
{
   return m_logFileName;
}

void ModelBuilderIO::SetLogFilePath(std::string logFileLocation)
{
   m_logFileName = logFileLocation;
}

const char* ModelBuilderIO::SaveState(
   const std::vector<ModelBuilderFeature *> &features,
   const GML::Elements::CPoint3d &machineBoundsMin, 
   const GML::Elements::CPoint3d &machineBoundsMax)
{
   pugi::xml_document stateDoc;
   stateDoc.reset();
   initialize(stateDoc);

   int i = 0;
   for (auto &feature : features)
   {
      WriteFeatureToFile(stateDoc, machineBoundsMin, machineBoundsMax, feature, i);
      i++;
   }

   ostringstream opStream;
   stateDoc.save(opStream);
   std::string reqdStr = opStream.str();
   char* state = new char[reqdStr.size() + 1];
   strcpy(state, reqdStr.c_str());

   return state;
}

void ModelBuilderIO::WriteFeatureToFile(
   const GML::Elements::CPoint3d &machineBoundsMin, 
   const GML::Elements::CPoint3d &machineBoundsMax, 
   ModelBuilderFeature* feature)
{     
   WriteFeatureToFile(m_CurrentDoc, machineBoundsMin, machineBoundsMax, feature);
}

void ModelBuilderIO::WriteFeatureToFile(
   pugi::xml_document& document,
   const GML::Elements::CPoint3d &machineBoundsMin,
   const GML::Elements::CPoint3d &machineBoundsMax,
   ModelBuilderFeature* feature, int index)
{
   pugi::xml_node metadataNode = document.child("ModelBuilder").child("Metadata");
   if (!metadataNode.child("MachineBounds"))
   {
      pugi::xml_node machineBoundsNode = metadataNode.append_child("MachineBounds");
      pugi::xml_node maxPointNode = machineBoundsNode.append_child("MaxPoint");
      pugi::xml_node minPointNode = machineBoundsNode.append_child("MinPoint");

      serializeXMLPoint(machineBoundsMax, maxPointNode);
      serializeXMLPoint(machineBoundsMin, minPointNode);
   }

   pugi::xml_node featureNode = document.child("ModelBuilder").child("Features").append_child("Feature");

   if(index != -1)
   {
      featureNode.append_child("Index").text() = index;
   }
   else
   {
      featureNode.append_child("Index").text() = feature->Index();
   }
   if (feature->GetType() == EFeatureGeometryType::Plane)
   {
      serializeXMLPlane((PlaneFeature*) feature, featureNode);
   }
   else if(feature->GetType() == EFeatureGeometryType::Cylinder)
   {
      serializeXMLCylinder((CylinderFeature*) feature, featureNode);
   }
   else if(feature->GetType() == EFeatureGeometryType::Circle)
   {
      serializeXMLCircle((CircleFeature*) feature, featureNode);
   }
   else if(feature->GetType() == EFeatureGeometryType::Cone)
   {
      serializeXMLCone((ConeFeature*) feature, featureNode);
   }
}

void ModelBuilderIO::SaveToFile(std::string filePath)
{
   m_CurrentDoc.save_file(filePath.c_str());
}

void ModelBuilderIO::ClearFile()
{
   m_CurrentDoc.reset();
   initialize(m_CurrentDoc);
   if (!m_logFileName.empty())
   {
      SaveToFile(m_logFileName);
   }
}

/////////////////////////////////////////////////////////////////////////////////////////////

void ModelBuilderIO::serializeXMLPlane(PlaneFeature* pd, pugi::xml_node featureNode)
{
   featureNode.append_child("Type").text() = "PlaneFace";
   
   pugi::xml_node planePointNode = featureNode.append_child("PlanePoint");
   serializeXMLPoint(pd->GetPoint(), planePointNode);

   pugi::xml_node normalNode = featureNode.append_child("Normal");
   serializeXMLVector(pd->GetNormal(), normalNode);

   auto pts = pd->GetTouchPoints();

   serializeXMLTouchPoints(pts, featureNode);
}

void ModelBuilderIO::serializeXMLPoint(GML::Elements::CPoint3d point, pugi::xml_node pointNode)
{
   pointNode.append_child("x");
   pointNode.child("x").text() = point.x;

   pointNode.append_child("y");
   pointNode.child("y").text() = point.y;

   pointNode.append_child("z");
   pointNode.child("z").text() = point.z;
}

void ModelBuilderIO::serializeXMLVector(GML::Elements::CVector3d vec, pugi::xml_node vectorNode)
{
   vectorNode.append_child("i");
   vectorNode.child("i").text() = vec.i;

   vectorNode.append_child("j");
   vectorNode.child("j").text() = vec.j;

   vectorNode.append_child("k");
   vectorNode.child("k").text() = vec.k;
}

void ModelBuilderIO::serializeXMLTouchPoints(const std::vector<ModelBuilderTouchPoint>& tps, pugi::xml_node node)
{
   pugi::xml_node tpsNode = node.append_child("TouchPoints");

   for (size_t i = 0; i < tps.size(); i++)
   {
      pugi::xml_node tpNode = tpsNode.append_child("TouchPoint");

      pugi::xml_node pointNode = tpNode.append_child("Point");
      serializeXMLPoint(tps[i].TipCentreLocation, pointNode);

      pugi::xml_node normalNode = tpNode.append_child("Normal");
      serializeXMLVector(tps[i].SurfaceNormal, normalNode);

      pugi::xml_node tipRadiusNode = tpNode.append_child("TipRadius");
      tipRadiusNode.text() = tps[i].TipRadius;

      pugi::xml_node stylusOrientationNode = tpNode.append_child("StylusOrientation");
      serializeXMLVector(tps[i].StylusOrientation, stylusOrientationNode);
   }
}

void ModelBuilderIO::serializeXMLCylinder(CylinderFeature* cd, pugi::xml_node featureNode)
{
   pugi::xml_node typeNode = featureNode.append_child("Type");
   typeNode.text() = "CylinderFace";

   pugi::xml_node cylinderPointNode = featureNode.append_child("CylinderPoint");
   serializeXMLPoint(cd->GetPoint(), cylinderPointNode);

   pugi::xml_node axisNode = featureNode.append_child("Axis");
   serializeXMLVector(cd->GetAxis(), axisNode);

   pugi::xml_node radiusNode = featureNode.append_child("Radius");
   radiusNode.text() = cd->GetRadius();

   auto isBoreNode = featureNode.append_child("IsBore");
   isBoreNode.text() = cd->IsBore();

   auto pts = cd->GetTouchPoints();

   serializeXMLTouchPoints(pts, featureNode);
}

void ModelBuilderIO::serializeXMLCircle(CircleFeature* cd, pugi::xml_node featureNode)
{
   pugi::xml_node typeNode = featureNode.append_child("Type");
   typeNode.text() = "Circle";

   pugi::xml_node centreNode = featureNode.append_child("CircleCentre");
   serializeXMLPoint(cd->GetCentre(), centreNode);

   pugi::xml_node radiusNode = featureNode.append_child("Radius");
   radiusNode.text() = cd->GetRadius();

   pugi::xml_node axisNode = featureNode.append_child("AssociatedPlaneNormal");
   serializeXMLVector(cd->GetAssociatedPlaneNormal(), axisNode);

   auto isBoreNode = featureNode.append_child("IsBore");
   isBoreNode.text() = cd->IsBore();

   auto pts = cd->GetTouchPoints();

   serializeXMLTouchPoints(pts, featureNode);
}

void ModelBuilderIO::serializeXMLCone(ConeFeature* coneDef, pugi::xml_node featureNode)
{
   pugi::xml_node typeNode = featureNode.append_child("Type");
   typeNode.text() = "ConeFace";

   auto baseCentreNode = featureNode.append_child("BaseCenter");
   serializeXMLPoint(coneDef->GetBaseCenter(), baseCentreNode);

   pugi::xml_node axisNode = featureNode.append_child("Axis");
   serializeXMLVector(coneDef->GetAxis(), axisNode);

   //pugi::xml_node topRadNode = featureNode.append_child("TopRadius");
   //topRadNode.text() = coneDef->GetTopRadius();

   auto baseRadNode = featureNode.append_child("BaseRadius");
   baseRadNode.text() = coneDef->GetBaseRadius();

   //pugi::xml_node htNode = featureNode.append_child("Height");
   //htNode.text() = coneDef->GetHeight();

   auto semiAngleNode = featureNode.append_child("SemiAngle");
   semiAngleNode.text() = coneDef->GetSemiAngle();

   auto isBoreNode = featureNode.append_child("IsBore");
   isBoreNode.text() = coneDef->IsBore();

   auto pts = coneDef->GetTouchPoints();

   serializeXMLTouchPoints(pts, featureNode);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
