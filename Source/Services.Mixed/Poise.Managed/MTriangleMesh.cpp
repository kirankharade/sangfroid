#include "MTriangleMesh.h"
#include "TriangleMesh.h"
#include <msclr\marshal_cppstd.h>

using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace Poise::PoiseManaged;
using namespace Poise;

MTriangleMesh::MTriangleMesh(Poise::TriangleMesh* unmanaged)
   :m_unmanaged(nullptr)
{
   m_unmanaged = unmanaged;
}

MTriangleMesh::~MTriangleMesh()
{
   this->!MTriangleMesh();
}

MTriangleMesh::!MTriangleMesh()
{
   if (m_unmanaged != nullptr)
   {
      delete m_unmanaged;
   }
}

MTriangleMesh^ MTriangleMesh::RemoveBigTriangles(float thresholdArea, bool unifyVertices)
{
   auto mesh = ((TriangleMesh*)m_unmanaged)->RemoveBigTriangles(thresholdArea, unifyVertices);
   return gcnew Poise::PoiseManaged::MTriangleMesh(mesh);
}

List<MTriangleMesh^>^ MTriangleMesh::DivideIntoFaceTriangulations(float angleDeg)
{
   int meshCount = 0;
   auto meshList = ((TriangleMesh*)m_unmanaged)->DivideIntoFaceTriangulations(angleDeg, meshCount);
   List<MTriangleMesh^>^ meshes = gcnew List<MTriangleMesh^>();
   for (int i = 0; i < meshCount; i++)
   {
      MTriangleMesh^ m = gcnew MTriangleMesh(meshList[i]);
      meshes->Add(m);
   }
   return meshes;
}

void MTriangleMesh::Associate(MPointCloud^ cloud)
{
   //auto unmanagedCloud = cloud->Unmanaged();
   //((TriangleMesh*)m_unmanaged)->Associate(unmanagedCloud);
}

static std::string ToStdString(System::String^ string)
{
   using System::Runtime::InteropServices::Marshal;
   System::IntPtr pointer = Marshal::StringToHGlobalAnsi(string);
   char* charPointer = reinterpret_cast<char *>(pointer.ToPointer());
   std::string returnString(charPointer, string->Length);
   Marshal::FreeHGlobal(pointer);
   return returnString;
}

void MTriangleMesh::GenerateCloudByFaceSampling(System::String^ filename, int level, GML::Elements::CTransform4d& transform)
{
      auto f = ToStdString(filename);                                                 
      ((TriangleMesh*)m_unmanaged)->GenerateCloudByFaceSampling(f.c_str(), level, transform);
}



