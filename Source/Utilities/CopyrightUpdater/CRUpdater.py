#!/usr/bin/env python3
# encoding: utf-8

import os
import sys
import string
import re
import datetime
import json
import shutil
import time

copyright = [
"/*-----------------------------------------------------------------------------------------* \n",
" * SANGFROID FRAMEWORK                                                                     * \n",
" * Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      * \n",
" * This source code and the information herein is a property of Kiran Amrut Kharade. It is * \n",
" * strictly forebidden to duplicate, reproduce or disseminate in part or full without a    * \n",
" * prior written permission of Kiran Amrut Kharade.                                        * \n",
" * @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      * \n",
" *-----------------------------------------------------------------------------------------*/\n"
"\n"
]

def Contains(str, sub):
    return (str.find(sub) != -1)

def IsHeader(f):
    if f.endswith(".h") or f.endswith(".H") or f.endswith(".inl"):
        return True
    return False

def IsCPP(f):
    if f.endswith(".cpp") or f.endswith(".CPP") or f.endswith(".C"):
        return True
    return False

def GetHeaders(location):
    if not os.path.exists(location):
        return None
    headers = []
    for root, subfolders, files in os.walk(location):
        for file in files:
            s = os.path.join(root, file)
            if IsHeader(s):
                headers.append(s)
                print(s)
    return headers

def GetSources(location):
    if not os.path.exists(location):
        return None
    sources = []
    for root, subfolders, files in os.walk(location):
        for file in files:
            s = os.path.join(root, file)
            if IsCPP(s):
                sources.append(s)
                print(s)
    return sources

def AddCopyrights(file):
    fp = open(file, mode='r')
    lines = fp.readlines()
    fp.close()
    lines = copyright + lines
    fp = open(file, mode='w')
    fp.writelines(lines)
    fp.close();

def UpdateCopyrights(location):
    headers = GetHeaders(location)
    for h in headers:
        AddCopyrights(h)

if __name__ == '__main__':
    UpdateCopyrights("../../.")

