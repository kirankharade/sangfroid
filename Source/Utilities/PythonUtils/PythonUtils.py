#!/usr/bin/env python3
# encoding: utf-8

import os
import sys
import string
import re
import datetime
import json
import shutil
import time

def RefinePointCloudCSVFile(infile, outfile):
    fpout = open(outfile, mode='w')
    count = 0
    with open(infile, mode='r') as fpin:
        for line in fpin:
            tokens = line.split(",")
            if len(tokens) == 5:
                newLine = tokens[0] + "    " + tokens[1] + "    " + tokens[2] + "\n"
                fpout.write(newLine)
                count += 1
                if (count%10000 == 0) :
                    print("Processed Lines = ", count)

    fpout.close()

if __name__ == '__main__':
    infile = "C:/Kiran/Pointcloud_processing/Data/Data 03/Meritor_28_03_2018_oben.csv"
    outfile = "C:/Kiran/Pointcloud_processing/Data/Data 03/Meritor_28_03_2018_oben.xyz"
    RefinePointCloudCSVFile(infile, outfile)
