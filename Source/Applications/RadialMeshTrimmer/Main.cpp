
#include "Point3f.h"
#include "Vector3f.h"
#include "Reader.h"
#include "IDataStream.h"
#include "STLDataStream.h"
#include "TriangleMesh.h"
#include "PointTriangle.h"
#include "cxxopts.hpp"
#include "GeoElementsUtils.h"
#include <string>

using namespace Sangfroid::IO;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;


//-------------------------------------------------------------------------------------

static std::tuple<std::string, std::string, double, double> ProcessArguments(int argc, const char *argv[]);
static TriangleMesh* LoadTriangulation(const std::string& inputFile);
static UltraVector<PointTriangle> Trim(TriangleMesh* mesh, const double& innerDiameter, const double& outerDiameter);

//-------------------------------------------------------------------------------------

int main(int argc, const char *argv[])
{
	std::tuple<std::string, std::string, double, double> t = ProcessArguments(argc, argv);
	std::string inputFile = std::get<0>(t);
	std::string outputFile = std::get<1>(t);
	double innerDiamter = std::get<2>(t);
	double outerDiamter = std::get<3>(t);

	//std::string inputFile = "C:/Kiran/Pointcloud_processing/Data/Data_03/Meritor_28_03_2018_oben.asc.stl";
	//std::string outputFile = "C:/Kiran/Pointcloud_processing/Data/Data_03/Meritor_28_03_2018_oben.trimmed.stl";
	//auto innerDiamter = 35.0;
	//auto outerDiamter = 125.0;

	std::replace(inputFile.begin(), inputFile.end(), '\\', '/');
	std::replace(outputFile.begin(), outputFile.end(), '\\', '/');

	cout << inputFile << endl;
	cout << outputFile << endl;
	cout << innerDiamter << endl;
	cout << outerDiamter << endl;

	auto mesh = LoadTriangulation(inputFile);
	auto trimmedMeshTriangles = Trim(mesh, innerDiamter, outerDiamter);
	GeoElementsUtils::WriteTriangleListToSTL(trimmedMeshTriangles, outputFile);
}
//-------------------------------------------------------------------------------------

static std::tuple<std::string, std::string, double, double> ProcessArguments(int argc, const char *argv[])
{
	std::string inputFile = "";
	std::string outputFile = "";
	double innerDiameter = 0;
	double outerDiameter = 10000;

	try
	{
		cxxopts::Options options(argv[0], "A radial mesh trimmer accepting inner and outer diameter as trim boundaries.");
		options
			.positional_help("[optional args]")
			.show_positional_help();

		options.add_options()
			("i,input", "Input file <Must be specified> : STL file format", cxxopts::value<std::string>())
			("o,output", "Output file <Optional> : STL file format", cxxopts::value<std::string>())
			("s,smaller_diameter", "Inner/smaller diameter <Must be specified>", cxxopts::value<double>())
			("l,larger_diameter", "Outer/larger diameter <Must be specified>", cxxopts::value<double>())
			("h,help", "Print help")
			;

		std::cout << std::endl;

		auto result = options.parse(argc, argv);

		if (result.count("help"))
		{
			std::cout << options.help({ "" }) << std::endl;
			exit(0);
		}

		if (result.count("input"))
		{
			inputFile = result["input"].as<std::string>();
			std::cout << "Input   = " << inputFile << std::endl << std::endl;
		}
		else
		{
			std::cout << "MeshTrimmer: Input file argument is missing. Exiting..." << std::endl;
			exit(1);
		}

		if (result.count("output"))
		{
			outputFile = result["output"].as<std::string>();
			std::cout << "Output  = " << outputFile << std::endl << std::endl;
		}
		else
		{
			outputFile = inputFile + ".stl";
			std::cout << "MeshTrimmer: Output file not given. Using " << outputFile << " as output file." << std::endl << std::endl;
		}

		if (result.count("smaller_diameter"))
		{
			innerDiameter = result["smaller_diameter"].as<double>();
			std::cout << "ID   = " << innerDiameter << std::endl << std::endl;
		}
		else
		{
			std::cout << "MeshTrimmer: Smaller/Inner diameter argument is missing. Exiting..." << std::endl;
			exit(1);
		}
		if (result.count("larger_diameter"))
		{
			outerDiameter = result["larger_diameter"].as<double>();
			std::cout << "OD   = " << outerDiameter << std::endl << std::endl;
		}
		else
		{
			std::cout << "MeshTrimmer: Larger/Outer diameter argument is missing. Exiting..." << std::endl;
			exit(1);
		}
	}
	catch (const cxxopts::OptionException& e)
	{
		std::cout << "error parsing options: " << e.what() << std::endl;
		exit(1);
	}

	return std::tuple<std::string, std::string, double, double>(inputFile, outputFile, innerDiameter, outerDiameter);
}
//-------------------------------------------------------------------------------------

static TriangleMesh* LoadTriangulation(const std::string& inputFile)
{
	auto reader = new Sangfroid::IO::Reader();
	auto fpStream = (IDataStream*)reader->Load(inputFile.c_str(), "STL");
	return ((STLDataStream*)fpStream)->Mesh();
}
//-------------------------------------------------------------------------------------

static double SqDistFromZAxis(const Point3f& pt)
{
	return (pt.x * pt.x) + (pt.y * pt.y);
}
//-------------------------------------------------------------------------------------

static void OneVertexOutsideBoundary(const PointTriangle& t, UltraVector<PointTriangle>& tList, double r, const double d[3], int i0, int i1, int i2)
{
	auto fa = (r - d[i0]) / (d[i1] - d[i0]);
	float x = t.Vertices[i0].x + (fa * (t.Vertices[i1].x - t.Vertices[i0].x));
	float y = t.Vertices[i0].y + (fa * (t.Vertices[i1].y - t.Vertices[i0].y));
	float z = t.Vertices[i0].z + (fa * (t.Vertices[i1].z - t.Vertices[i0].z));
	Point3f a(x, y, z);

	auto fb = (r - d[i0]) / (d[i2] - d[i0]);
	x = t.Vertices[i0].x + (fb * (t.Vertices[i2].x - t.Vertices[i0].x));
	y = t.Vertices[i0].y + (fb * (t.Vertices[i2].y - t.Vertices[i0].y));
	z = t.Vertices[i0].z + (fb * (t.Vertices[i2].z - t.Vertices[i0].z));
	Point3f b(x, y, z);

	tList.push_back(PointTriangle(a, t.Vertices[i1], b));
	tList.push_back(PointTriangle(b, t.Vertices[i1], t.Vertices[i2]));
}
//-------------------------------------------------------------------------------------

static void TwoVerticesOutsideBoundary(const PointTriangle& t, UltraVector<PointTriangle>& tList, double r, const double d[3], int i0, int i1, int i2)
{
	auto fa = (r - d[i0]) / (d[i1] - d[i0]);
	float x = t.Vertices[i0].x + (fa * (t.Vertices[i1].x - t.Vertices[i0].x));
	float y = t.Vertices[i0].y + (fa * (t.Vertices[i1].y - t.Vertices[i0].y));
	float z = t.Vertices[i0].z + (fa * (t.Vertices[i1].z - t.Vertices[i0].z));
	Point3f a(x, y, z);

	auto fb = (r - d[i0]) / (d[i2] - d[i0]);
	x = t.Vertices[i0].x + (fb * (t.Vertices[i2].x - t.Vertices[i0].x));
	y = t.Vertices[i0].y + (fb * (t.Vertices[i2].y - t.Vertices[i0].y));
	z = t.Vertices[i0].z + (fb * (t.Vertices[i2].z - t.Vertices[i0].z));
	Point3f b(x, y, z);

	tList.push_back(PointTriangle(t.Vertices[i0], a, b));
}
//-------------------------------------------------------------------------------------

static UltraVector<PointTriangle> Trim(TriangleMesh* mesh, const double& innerDiameter, const double& outerDiameter)
{
	UltraVector<PointTriangle> triangleList = mesh->ToTriangleList();
	UltraVector<PointTriangle> newList;

	//Squared radii
	auto ir = 0.25 * (innerDiameter * innerDiameter);
	auto or = 0.25 * (outerDiameter * outerDiameter);

	auto numTriangles = triangleList.size();
	float x, y, z;

	for (int i = 0; i < numTriangles; i++)
	{
		const auto t = triangleList[i];
		double d[3];
		d[0] = SqDistFromZAxis(t.Vertices[0]);
		d[1] = SqDistFromZAxis(t.Vertices[1]);
		d[2] = SqDistFromZAxis(t.Vertices[2]);

		int nearerThanInnerRadius = 0;
		int fartherThanOuterRadius = 0;
		bool irs[3] = { false, false, false };
		bool ors[3] = { false, false, false };

		const double tolerance = 0.0001;

		if (d[0] < ir)	{ nearerThanInnerRadius++; irs[0] = true; }
		if (d[1] < ir)	{ nearerThanInnerRadius++; irs[1] = true; }
		if (d[2] < ir)	{ nearerThanInnerRadius++; irs[2] = true; }

		if (d[0] > or) { fartherThanOuterRadius++;	ors[0] = true; }
		if (d[1] > or) { fartherThanOuterRadius++;	ors[1] = true; }
		if (d[2] > or) { fartherThanOuterRadius++;	ors[2] = true; }

		if (nearerThanInnerRadius == 3 || fartherThanOuterRadius == 3)
		{
			continue;
		}
		if (nearerThanInnerRadius == 0 && fartherThanOuterRadius == 0)
		{
			newList.push_back(t);
		}

		if (nearerThanInnerRadius == 1 && fartherThanOuterRadius == 0)
		{
			if (irs[0]) OneVertexOutsideBoundary(t, newList, ir, d, 0, 1, 2);
			if (irs[1]) OneVertexOutsideBoundary(t, newList, ir, d, 1, 2, 0);
			if (irs[2]) OneVertexOutsideBoundary(t, newList, ir, d, 2, 0, 1);
		}
		if (fartherThanOuterRadius == 1 && nearerThanInnerRadius == 0)
		{
			if (ors[0]) OneVertexOutsideBoundary(t, newList, or, d, 0, 1, 2);
			if (ors[1]) OneVertexOutsideBoundary(t, newList, or, d, 1, 2, 0);
			if (ors[2]) OneVertexOutsideBoundary(t, newList, or, d, 2, 0, 1);
		}
		if (nearerThanInnerRadius == 2 && fartherThanOuterRadius == 0)
		{
			if (!irs[0]) TwoVerticesOutsideBoundary(t, newList, ir, d, 0, 1, 2);
			if (!irs[1]) TwoVerticesOutsideBoundary(t, newList, ir, d, 1, 2, 0);
			if (!irs[2]) TwoVerticesOutsideBoundary(t, newList, ir, d, 2, 0, 1);
		}
		if (fartherThanOuterRadius == 2 && nearerThanInnerRadius == 0)
		{
			if (!ors[0]) TwoVerticesOutsideBoundary(t, newList, or , d, 0, 1, 2);
			if (!ors[1]) TwoVerticesOutsideBoundary(t, newList, or , d, 1, 2, 0);
			if (!ors[2]) TwoVerticesOutsideBoundary(t, newList, or , d, 2, 0, 1);
		}
	}
	return newList;
}
//-------------------------------------------------------------------------------------
