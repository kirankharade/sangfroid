
#include "Point3f.h"
#include "MeshGenerator.h"
#include "cxxopts.hpp"
#include "CommandArguments.h"
#include <string>
#include <thread>

//-------------------------------------------------------------------------------------

int main(int argc, const char *argv[])
{
	auto file = "C:/Kiran/Pointcloud_processing/Data/Phase-2/Data-02/grTestData.oka";
	MeshGenerator mg(file, "", 500);
	mg.LoadCloud(file);

	mg.DumpCloudToOSGT();

	//auto a = CommandArguments::Process(argc, argv);
	//std::this_thread::sleep_for(std::chrono::milliseconds(25000));
	//MeshGenerator mg(a._inputFile, a._outputFile, a._meshRadialDivisions);
	//mg.LoadCloud(a._inputFile);
	//mg.StructuredReconstruction();

	//std::cout << "Trying to trim the mesh..." << std::endl;
	//mg.SaveToSTL(a._outputFile);

	return 0;
}
//-------------------------------------------------------------------------------------
