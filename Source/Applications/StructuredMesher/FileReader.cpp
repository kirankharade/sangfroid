#include "FileReader.h"
#include "BaseIncludes.h"
#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "OKAData.h"

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;

//------------------------------------------------------------------------------------------------------

std::string ReplaceSubstring(std::string source, std::string search, std::string replace)
{
	while (source.find(search) != std::string::npos)
	{
		auto position = source.find(search);
		source.replace(source.begin() + position, source.begin() + position + search.size(), replace);
	}
	return source;
}

static inline bool StartsWith(std::string const & value, std::string const & starting)
{
	if (starting.size() > value.size()) return false;
	return std::equal(starting.begin(), starting.end(), value.begin());
}
//------------------------------------------------------------------------------------------------------

FileReader::FileReader()
{
}

FileReader::~FileReader()
{
}
//------------------------------------------------------------------------------------------------------

bool FileReader::ReadInPoints_csv(const std::string& filename, UltraVector<Sangfroid::Maths::Point3f>& cloudPoints)
{
	float bounds[6];
	bounds[0] = bounds[1] = bounds[2] = FLT_MAX;
	bounds[3] = bounds[4] = bounds[5] = -FLT_MAX;

	cloudPoints.clear();

	FILE *fp = fopen(filename.c_str(), "r");
	if (fp == NULL)
	{
		return false;
	}
	long currentPos = ftell(fp);
	fseek(fp, 0L, SEEK_END);
	long fileLen = ftell(fp);
	fseek(fp, currentPos, SEEK_SET);
	size_t cnt = 0;
	int ret;
	char buf[1024];

	float x = 0, y = 0, z = 0, nx, ny, nz, timestamp = 0, theta = 0;

	while (!feof(fp))
	{
		if (feof(fp))
		{
			break;
		}
		bool fgetOut = fgets(buf, 1024, fp);
		if (fgetOut == 0)
		{
			continue;
		}
		float q;
		//ret = sscanf(buf, "%f, %f, %f, %f, %f\n", &x, &y, &z, &timestamp, &theta);
		//if (ret != 5)
		//{
		//	ret = sscanf(buf, "%f, %f, %f, %f\n", &x, &y, &z, &timestamp);
		//	if (ret != 4)
		//	{
		ret = sscanf(buf, "%f, %f, %f\n", &x, &y, &z);
		if (ret != 3)
		{
			break;
		}
		//	}
		//}

		cloudPoints.push_back(Sangfroid::Maths::Point3f(x, y, z));

		if (bounds[0] > x) bounds[0] = x;
		if (bounds[1] > y) bounds[1] = y;
		if (bounds[2] > z) bounds[2] = z;
		if (bounds[3] < x) bounds[3] = x;
		if (bounds[4] < y) bounds[4] = y;
		if (bounds[5] < z) bounds[5] = z;
	}

	std::cout << "Total Point count = " << cloudPoints.size() << endl;

	return true;
}
//------------------------------------------------------------------------------------------------------

OKAData* FileReader::ReadInPoints_oka(const std::string& filename)
{
	float bounds[6];
	bounds[0] = bounds[1] = bounds[2] = FLT_MAX;
	bounds[3] = bounds[4] = bounds[5] = -FLT_MAX;

	OKAData* oka = new OKAData();

	FILE *fp = fopen(filename.c_str(), "r");
	if (fp == NULL)
	{
		return false;
	}
	long currentPos = ftell(fp);
	fseek(fp, 0L, SEEK_END);
	long fileLen = ftell(fp);
	fseek(fp, currentPos, SEEK_SET);
	size_t cnt = 0;
	int ret;
	char buf[1024];

	float C, Xs, Ys, Zs, Ci, Xi, Yi, Zi, Xsr, Ysr, Zsr, Cir, Xir, Yir, Zir, T_mm;
	long PointCount;
	int Index;
	float findex, radius, quality;

	float x = 0, y = 0, z = 0, nx, ny, nz, timestamp = 0, theta = 0;

	bool fgetOut = fgets(buf, 1024, fp);
	fgetOut = fgets(buf, 1024, fp);
	fgetOut = fgets(buf, 1024, fp);
	fgetOut = fgets(buf, 1024, fp);

	while (!StartsWith(buf, "Final number of points"))
	{
		OKACloudSection* section = new OKACloudSection();
		section->SectionHeader = new OKAHeader();

		ret = sscanf(buf, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %ld %d\n",
			&C, &Xs, &Ys, &Zs,
			&Ci, &Xi, &Yi, &Zi,
			&Xsr, &Ysr, &Zsr,
			&Cir, &Xir, &Yir, &Zir,
			&T_mm, &PointCount, &Index
		);

		section->SectionHeader->C = C;
		section->SectionHeader->Xs = Xs;
		section->SectionHeader->Ys = Ys;
		section->SectionHeader->Zs = Zs;
		section->SectionHeader->Ci = Ci;
		section->SectionHeader->Xi = Xi;
		section->SectionHeader->Yi = Yi;
		section->SectionHeader->Zi = Zi;
		section->SectionHeader->Xsr = Xsr;
		section->SectionHeader->Ysr = Ysr;
		section->SectionHeader->Zsr = Zsr;
		section->SectionHeader->Cir = Cir;
		section->SectionHeader->Xir = Xir;
		section->SectionHeader->Yir = Yir;
		section->SectionHeader->Zir = Zir;
		section->SectionHeader->T_mm = T_mm;
		section->SectionHeader->DataPoints = PointCount;
		section->SectionHeader->Index = Index;

		oka->CloudSections.push_back(section);

		fgetOut = fgets(buf, 1024, fp);
	}

	size_t sz;
	string str = buf;
	str = ReplaceSubstring(str, "Final number of points ", "");
	int pointCount = std::stoi(str, &sz);

	fgetOut = fgets(buf, 1024, fp);
	fgetOut = fgets(buf, 1024, fp);
	fgetOut = fgets(buf, 1024, fp);
	fgetOut = fgets(buf, 1024, fp);

	int currIndex = 0;
	UltraVector<Sangfroid::Maths::Point3f> cloudPoints;
	UltraVector<float> qualities;
	UltraVector<int> indices;
	UltraVector<float> pointRadiusList;
	int sectionCount = 0;

	while (!feof(fp))
	{
		if (feof(fp))
		{
			break;
		}
		bool fgetOut = fgets(buf, 1024, fp);
		if (fgetOut == 0)
		{
			continue;
		}
		float q;
		ret = sscanf(buf, "%f %f %f %f %f %f\n", &x, &y, &z, &quality, &findex, &radius);
		if (ret != 6)
		{
			break;
		}

		if (currIndex == 0)
		{
			currIndex = (int)findex;
		}
		else if (currIndex != (int)findex)
		{
			oka->CloudSections[sectionCount]->Points = cloudPoints;
			oka->CloudSections[sectionCount]->Qualities = qualities;
			oka->CloudSections[sectionCount]->Indices = indices;
			oka->CloudSections[sectionCount]->PointRadiusList = pointRadiusList;
			std::memcpy(&oka->CloudSections[sectionCount]->Bounds[0], &bounds[0], 6 * sizeof(float));

			cloudPoints = UltraVector<Sangfroid::Maths::Point3f>();
			qualities = UltraVector<float>();
			indices = UltraVector<int>();
			pointRadiusList = UltraVector<float>();
			bounds[0] = bounds[1] = bounds[2] = FLT_MAX;
			bounds[3] = bounds[4] = bounds[5] = -FLT_MAX;

			currIndex = (int)findex;
			sectionCount++;
		}

		cloudPoints.push_back(Sangfroid::Maths::Point3f(x, y, z));

		if (bounds[0] > x) bounds[0] = x;
		if (bounds[1] > y) bounds[1] = y;
		if (bounds[2] > z) bounds[2] = z;
		if (bounds[3] < x) bounds[3] = x;
		if (bounds[4] < y) bounds[4] = y;
		if (bounds[5] < z) bounds[5] = z;

		qualities.push_back(quality);
		indices.push_back((int) findex);
		pointRadiusList.push_back(radius);
	}

	oka->CloudSections[sectionCount]->Points = cloudPoints;
	oka->CloudSections[sectionCount]->Qualities = qualities;
	oka->CloudSections[sectionCount]->Indices = indices;
	oka->CloudSections[sectionCount]->PointRadiusList = pointRadiusList;
	std::memcpy(&oka->CloudSections[sectionCount]->Bounds[0], &bounds[0], 6 * sizeof(float));

	std::cout << "Total Point count = " << cloudPoints.size() << endl;

	return oka;
}
//------------------------------------------------------------------------------------------------------
