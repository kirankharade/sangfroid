#pragma once

#include "OKAHeader.h"
#include <vector>
#include "Containers/UltraVector.h"
#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;

//------------------------------------------------------------------------------------------------------

struct OKAData
{
	long FinalNumberOfPoints;
	std::vector<OKACloudSection*> CloudSections;

	OKAData()
	{
		CloudSections.clear();
		FinalNumberOfPoints = 0;
	}
};
//------------------------------------------------------------------------------------------------------
