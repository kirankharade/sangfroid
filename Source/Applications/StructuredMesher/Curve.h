#pragma once

#include <iostream>
#include "BaseIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include <vector>

namespace StructuredMesher
{

/*-----------------------------------------------------------------------------------*/

struct  Curve
{
   int ID;

   bool Merged;

public:

	Curve() { Merged = false; }

	Curve(const std::vector<Sangfroid::Maths::Point3f>& points);

	~Curve();

	const std::vector<Sangfroid::Maths::Point3f>& Points() const;

	void AddAtEnd(const Sangfroid::Maths::Point3f& pt);

	void AddAtStart(const Sangfroid::Maths::Point3f& pt);

	void AddAtEnd(const std::vector<Sangfroid::Maths::Point3f>& pts);

	void AddAtStart(const std::vector<Sangfroid::Maths::Point3f>& pts);

	Sangfroid::Maths::Point3f PointAt(const int& index) const;

	Sangfroid::Maths::Point3f Min() const;

	Sangfroid::Maths::Point3f Max() const;

	double AverageZ() const { return m_avgZ; }

	double AverageR() const { return m_avgR; }

	Sangfroid::Maths::Vector3f StartDir() const { return m_startDir; }

	Sangfroid::Maths::Vector3f EndDir() const { return m_endDir; }

	double StartAngle() const { return m_startAngle; }

	double EndAngle() const { return m_endAngle; }

protected:

	void calculateBounds();

protected:

	Sangfroid::Maths::Point3f					m_min;
	Sangfroid::Maths::Point3f					m_max;
	std::vector<Sangfroid::Maths::Point3f>	m_points;
	bool									m_calculatedBounds;

	double m_avgZ;
	double m_avgR;
	Sangfroid::Maths::Vector3f m_startDir;
	Sangfroid::Maths::Vector3f m_endDir;
	double m_startAngle;
	double m_endAngle;
};
/*-----------------------------------------------------------------------------------*/

typedef std::vector<Curve*> CurveList;

/*-----------------------------------------------------------------------------------*/

}

