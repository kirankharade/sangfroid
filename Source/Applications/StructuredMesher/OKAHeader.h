#pragma once

#include <string>
#include "Containers/UltraVector.h"
#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;

struct OKAHeader
{
	std::string Date;
	std::string Tag;

	float C;
	float Xs;
	float Ys;
	float Zs;
	float Ci;
	float Xi;
	float Yi;
	float Zi;
	float Xsr;
	float Ysr;
	float Zsr;
	float Cir;
	float Xir;
	float Yir;
	float Zir;
	float T_mm;
	long DataPoints;
	int Index;

	OKAHeader()
	{
		C  = 0;
		Xs = 0;
		Ys = 0;
		Zs = 0;

		Ci = 0;
		Xi = 0;
		Yi = 0;
		Zi = 0;

		Xsr = 0;
		Ysr = 0;
		Zsr = 0;

		Cir = 0;
		Xir = 0;
		Yir = 0;
		Zir = 0;

		T_mm = 0;
		DataPoints = 0;
		Index = 0;

	}
};

struct OKACloudSection
{
	OKAHeader* SectionHeader;
	UltraVector<Point3f> Points;
	UltraVector<float> Qualities;
	UltraVector<int> Indices;
	UltraVector<float> PointRadiusList;
	float Bounds[6];
};
