﻿#pragma once

#include <iostream>
#include <vector>
#include "Curve.h"

using namespace std;
using namespace StructuredMesher;
using namespace Sangfroid;
using namespace Sangfroid::Maths;

/*-----------------------------------------------------------------------------------*/

Curve::Curve(const std::vector<Sangfroid::Maths::Point3f>& points)
	:m_points(points),
	m_calculatedBounds(false)
{
	Merged = false;
	calculateBounds();

	if (m_points.size() > 0)
	{
		m_avgZ = 0;
		m_avgR = 0;
		for (auto p : m_points)
		{
			m_avgZ += p.z;
			m_avgR += sqrtf((p.x * p.x) + (p.y * p.y));
		}
		m_avgZ /= m_points.size();
		m_avgR /= m_points.size();
	}

	if (m_points.size() > 2)
	{
		auto lastIndex = m_points.size() - 1;
		m_startDir = m_points[1] - m_points[0];
		m_endDir = m_points[m_points.size() - 1] - m_points[m_points.size() - 2];
		m_startAngle = ToDegrees(atan2f(m_points[0].y, m_points[0].x));
		m_endAngle = ToDegrees(atan2f(m_points[lastIndex].y, m_points[lastIndex].x));
		if (m_startAngle < 0) m_startAngle += 360.0;
		if (m_endAngle < 0) m_endAngle += 360.0;
	}
}

Curve::~Curve() 
{
}

void Curve::AddAtEnd(const Sangfroid::Maths::Point3f& pt)
{
	m_points.push_back(pt);
}

void Curve::AddAtStart(const Sangfroid::Maths::Point3f& pt)
{
	m_points.insert(m_points.begin(), pt);
}

void Curve::AddAtEnd(const std::vector<Sangfroid::Maths::Point3f>& pts)
{
	m_points.insert(m_points.end(), pts.begin(), pts.end());
}

void Curve::AddAtStart(const std::vector<Sangfroid::Maths::Point3f>& pts)
{
	m_points.insert(m_points.begin(), pts.begin(), pts.end());
}

void Curve::calculateBounds()
{
	if (!m_calculatedBounds)
	{
		Sangfroid::Maths::Point3f min(FLT_MAX, FLT_MAX, FLT_MAX);
		Sangfroid::Maths::Point3f max(-FLT_MAX, -FLT_MAX, -FLT_MAX);

		for (int i = 0; i < m_points.size(); i++)
		{
			auto& pt = m_points[i];

			auto x = pt.x;
			auto y = pt.y;
			auto z = pt.z;

			if (x < min.x)	min.x = x;
			if (y < min.y)	min.y = y;
			if (z < min.z)	min.z = z;

			if (x >= max.x)	max.x = x;
			if (y >= max.y)	max.y = y;
			if (z >= max.z)	max.z = z;
		}

		m_min = min;
		m_max = max;

		m_calculatedBounds = true;
	}
}


const std::vector<Sangfroid::Maths::Point3f>& Curve::Points() const
{
   return m_points;
}

Sangfroid::Maths::Point3f Curve::PointAt(const int& index) const
{
   if (index >= m_points.size() || index < 0)
   {
      throw "Curve: Point-index out of range...";
   }
   return m_points[index];
}

Sangfroid::Maths::Point3f Curve::Min() const
{
   //calculateBounds();
   return m_min;
}

Sangfroid::Maths::Point3f Curve::Max() const
{
   //calculateBounds();
   return m_max;
}


/*-----------------------------------------------------------------------------------*/
