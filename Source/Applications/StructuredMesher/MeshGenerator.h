#pragma once

#include<iostream>

/*-----------------------------------------------------------------------------------*/

struct XVertex
{
	union
	{
		float p[3];
		struct
		{
			float x;
			float y;
			float z;
		};
	};

	XVertex()
	{
		p[0] = 0.0f;
		p[1] = 0.0f;
		p[2] = 0.0f;
	}

	XVertex(float ax, float ay, float az)
	{
		p[0] = ax;
		p[1] = ay;
		p[2] = az;
	}

	XVertex(float c[3])
	{
		p[0] = c[0];
		p[1] = c[1];
		p[2] = c[2];
	}

	void Normalize()
	{
		auto mag = sqrt((x*x) + (y*y) + (z*z));
		if (!(fabs(mag) < 0.00001f))
		{
			x = x / mag;
			y = y / mag;
			z = z / mag;
		}
	}

	float Magnitude() const
	{
		return sqrt((x*x) + (y*y) + (z*z));
	}
};

struct XTVertex
{
	XVertex v;
	XVertex n;
	float t; //Time-stamp
	XTVertex(float ax, float ay, float az, float at)
	{
		v.x = ax;
		v.y = ay;
		v.z = az;
		t = at;
	}
};
/*-----------------------------------------------------------------------------------*/

struct XTriangle
{
	XVertex Vertices[3];
	XVertex n;

	XTriangle()
	{
	}

	XTriangle(XVertex a, XVertex b, XVertex c)
	{
		Vertices[0] = XVertex(a.p);
		Vertices[1] = XVertex(b.p);
		Vertices[2] = XVertex(c.p);

		XVertex va(Vertices[1].x - Vertices[0].x, Vertices[1].y - Vertices[0].y, Vertices[1].z - Vertices[0].z);
		XVertex vb(Vertices[2].x - Vertices[0].x, Vertices[2].y - Vertices[0].y, Vertices[2].z - Vertices[0].z);

		n.x = (va.y * vb.z) - (va.z * vb.y);
		n.y = (va.z * vb.x) - (va.x * vb.z);
		n.z = (va.x * vb.y) - (va.y * vb.x);

		auto mag = sqrt((n.x*n.x) + (n.y*n.y) + (n.z*n.z));
		if (!(fabs(mag) < 0.00001f))
		{
			n.x = n.x / mag;
			n.y = n.y / mag;
			n.z = n.z / mag;
		}
	}

	XTriangle(XVertex a, XVertex b, XVertex c, XVertex nn)
	{
		Vertices[0] = XVertex(a.p);
		Vertices[1] = XVertex(b.p);
		Vertices[2] = XVertex(c.p);
		n = nn;
	}
};
/*-----------------------------------------------------------------------------------*/

class MeshGenerator
{

private:

	std::string _inputFilename;
	std::string _outputFilename;
	int _meshRadialDivisions;

public:

	MeshGenerator(const std::string& infile, const std::string& outfile, const int meshDivisions);

	~MeshGenerator();

	void LoadCloud(const std::string& filename);

	void DumpCloudToOSGT();

	void SaveToSTL(const std::string& outputFile = "");

	void StructuredReconstruction();

};

