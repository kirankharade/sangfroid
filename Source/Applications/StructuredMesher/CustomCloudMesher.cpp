﻿#include "CustomCloudMesher.h"
#include "BaseIncludes.h"
#include "Containers/UltraVector.h"
#include "MathUtils.h"
#include "Curve.h"
#include "Region.h"
#include "PointTriangle.h"
#include <map>
#include <algorithm>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include <map>
#include <tuple>

using namespace std;
using namespace Sangfroid;
using namespace StructuredMesher;
using namespace Sangfroid::Maths;


static const int REGION_COUNT = 10;
static const int PLANE_INTERSECTION_TOLERANCE = 0.00001;

#pragma warning(disable:4244 4996)

/*-----------------------------------------------------------------------------------*/

static UltraVector<PointTriangle> ConstructMesh(const std::vector<std::vector<Point3f*>>& intersectionGridCurves)
{
	UltraVector<PointTriangle> triangles;
	for (int ip = 0; ip < intersectionGridCurves.size() - 1; ip++)
	{
		auto ca = intersectionGridCurves[ip];
		auto cb = intersectionGridCurves[ip + 1];

		auto pointCount = ca.size();
		for (int j = 0; j < pointCount - 1; j++)
		{
			auto a = ca[j];
			auto b = ca[j + 1];
			auto c = cb[j + 1];
			if (a != nullptr && b != nullptr && c != nullptr)
			{
				PointTriangle t;
				t.Vertices[0] = *a;
				t.Vertices[1] = *b;
				t.Vertices[2] = *c;
				triangles.push_back(t);
			}

			a = ca[j];
			b = cb[j + 1];
			c = cb[j];
			if (a != nullptr && b != nullptr && c != nullptr)
			{
				PointTriangle t;
				t.Vertices[0] = *a;
				t.Vertices[1] = *b;
				t.Vertices[2] = *c;
				triangles.push_back(t);
			}
		}
	}
	return triangles;
}

static std::vector<std::vector<Point3f*>> GenerateMissingAreas(std::vector<std::vector<Point3f*>>& intersectionGridCurves)
{
	if (intersectionGridCurves.size() > 2)
	{
		for (int ip = 1; ip < intersectionGridCurves.size() - 1; ip++)
		{
			auto& cx = intersectionGridCurves[ip];
			auto& cp = intersectionGridCurves[ip - 1];
			auto& cn = intersectionGridCurves[ip + 1];

			auto pointCount = cx.size();
			for (int j = 0; j < pointCount; j++)
			{
				auto& a = cx[j];
				auto& b = cp[j];
				auto& c = cn[j];
				if (a == nullptr && b != nullptr && c != nullptr)
				{
					auto x = (b->x + c->x) / 2.0;
					auto y = (b->y + c->y) / 2.0;
					auto z = (b->z + c->z) / 2.0;

					a = new Point3f(x, y, z);
				}
			}
		}
	}
	return intersectionGridCurves;
}

static std::vector<std::vector<Point3f*>> FindPlaneCurveIntersections(const std::vector<Curve*>& curves, const std::vector<std::tuple<Point3f, Vector3f>>& planes)
{
	std::vector<std::vector<Point3f*>> intersectionGridCurves;
	bool segmentLiesInPlane = false;
	const double tol = PLANE_INTERSECTION_TOLERANCE;

	for (int i = 0; i < curves.size(); i++)
	{
		auto c = curves[i];
		auto points = c->Points();
		std::vector<Point3f*> intersections;

		for (int j = 0; j < planes.size(); j++)
		{
			auto t = planes[j];
			auto planePoint = std::get<0>(t);
			auto planeNormal = std::get<1>(t);
			Point3f* intersectionPoint = nullptr;

			for (int k = 0; k < points.size() - 1; k++)
			{
				auto a = points[k];
				auto b = points[k + 1];
				Point3f tempIntersection;
				auto intersecting = MathUtils::LineSegmentPlaneIntersection(a, b, planePoint, planeNormal,
					tempIntersection, segmentLiesInPlane, tol);

				if (intersecting)
				{
					intersectionPoint = new Point3f(tempIntersection);
					break;
				}
			}

			intersections.push_back(intersectionPoint);
		}
		intersectionGridCurves.push_back(intersections);
	}
	return intersectionGridCurves;
}

static std::vector<std::tuple<Point3f, Vector3f>> GetIntersectionPlanesForRegion(Region* region, const int meshDivisions)
{
	Point3f extremeStart(0, 0, 0);
	Point3f extremeEnd(0, 0, 0);

	double thetaStart = region->StartAngle();
	double thetaEnd = region->EndAngle();

	std::vector<std::tuple<Point3f, Vector3f>> planes;
	auto nSteps = (int) (meshDivisions / REGION_COUNT);
	auto step = (thetaEnd - thetaStart) / nSteps;
	auto r = 100.0;

	for (int i = 0; i <= nSteps; i++)
	{
		auto theta = thetaStart + (step * i);
		theta = ToRadians(theta);

		auto x = r * cos(theta);
		auto y = r * sin(theta);
		auto z = 0;

		Point3f pt(x, y, z);

		Vector3f va(0, 0, 1);
		Vector3f vb(x, y, z);

		auto dir = va.cross(vb);
		dir = dir.unitVec();

		planes.push_back(std::make_tuple(pt, dir));
	}

	return planes;
}

static bool IsCurveCCW(const Curve* c)
{
	auto thisPoints = c->Points();
	Point3f origin(0, 0, thisPoints[0].z);
	auto vec1 = (thisPoints[1] - thisPoints[0]).unitVec();
	auto vec2 = (thisPoints[0] - origin).unitVec();
	auto cross = vec2.cross(vec1);
	auto dot = Vector3f(0, 0, 1).dot(cross);
	if (dot > 0)
	{
		return true;
	}
	return false;
}

static bool IsAngleWithinRange(double start, double end, double value)
{
	auto ret = false;
	if (start > end)
	{
		if (value >= start || value <= end)
			return true;
	}
	else
	{
		if (value >= start && value <= end)
			return true;
	}
	return false;
}

static bool AreAnglesOverlapping(const Curve* c, const Curve* other)
{
	auto otherPoints = other->Points();
	auto thisPoints = c->Points();

	auto thisPointCount = thisPoints.size();
	auto otherPointCount = otherPoints.size();

	auto as = fmod(ToDegrees(atan2f(thisPoints[0].y, thisPoints[0].x)), 360.0);
	auto ae = fmod(ToDegrees(atan2f(thisPoints[thisPointCount - 1].y, thisPoints[thisPointCount - 1].x)), 360.0);
	auto bs = fmod(ToDegrees(atan2f(otherPoints[0].y, otherPoints[0].x)), 360.0);
	auto be = fmod(ToDegrees(atan2f(otherPoints[otherPointCount - 1].y, otherPoints[otherPointCount - 1].x)), 360.0);
	if (as < 0) as += 360.0;
	if (ae < 0) ae += 360.0;
	if (bs < 0) bs += 360.0;
	if (be < 0) be += 360.0;

	if (IsAngleWithinRange(as, ae, bs) || IsAngleWithinRange(bs, be, as))
		return true;

	return false;
}

static float AngleInDegrees(const Point3f& pt)
{
	auto angle = ToDegrees(atan2f(pt.y, pt.x));
	if (angle < 0) angle += 360.0;
	return angle;
}

static Region* GetRegion(std::vector<Region*> regions, Point3f& pt)
{
	auto numRegions = regions.size();
	auto angle = AngleInDegrees(pt);
	for (int i = 0; i < numRegions; i++)
	{
		auto region = regions[i];
		if (IsAngleWithinRange(region->StartAngle(), region->EndAngle(), angle))
		{
			return region;
		}
	}
	return nullptr;
}

static tuple<Point3f, Vector3f> GetIntersectionPlaneAtAngle(float theta)
{
	theta = ToRadians(theta);
	auto r = 100.0;

	auto x = r * cos(theta);
	auto y = r * sin(theta);
	auto z = 0;

	Point3f pt(x, y, z);

	Vector3f va(0, 0, 1);
	Vector3f vb(x, y, z);

	auto dir = va.cross(vb).unitVec();

	return std::make_tuple(pt, dir);
}

static std::vector<Region*> GenerateRegions(int numRegions)
{
	std::vector<Region*> regions;
	float tolerance = 0.000000;
	auto angleStep = 360.0 / numRegions;
	for (int i = 0; i < numRegions; i++)
	{
		float startAngle = (i * angleStep) - tolerance;
		float endAngle = ((i + 1) * angleStep) + tolerance;

		auto region = new Region(startAngle, endAngle);
		regions.push_back(region);
	}
	return regions;
}

static std::vector<Point3f> GetNextPoints(int currIndex, int numPoints, const UltraVector<Point3f>& points)
{
	auto totalPoints = points.size();
	std::vector<Point3f> outPoints;

	auto lastIndex = (currIndex + numPoints) < (totalPoints - 1) ? (currIndex + numPoints) : totalPoints - 1;
	for (int i = currIndex + 1; i <= lastIndex; i++)
	{
		outPoints.push_back(points[i]);
	}
	return outPoints;
}

static std::vector<Point3f> GetPreviousPoints(int currIndex, int numPoints, const UltraVector<Point3f>& points)
{
	auto totalPoints = points.size();
	std::vector<Point3f> outPoints;

	auto prevIndex = (currIndex - numPoints) > -1 ? (currIndex - numPoints) : 0;
	for (int i = currIndex - 1; i >= prevIndex; i--)
	{
		outPoints.insert(outPoints.begin(), points[i]);
	}
	return outPoints;
}

static void WriteTriangleListToSTL(const UltraVector<PointTriangle>& triangleList, const std::string& filename)
{
	try
	{
		FILE* pFile = std::fopen(filename.c_str(), "wb");
		if (!pFile)
		{
			throw "The STL file cannot be written.";
		}

		int triangleCount = triangleList.size();
		int vertexCount = 3 * triangleCount;
		int numIndices = 3 * triangleCount;

		//write 80 byte header
		fprintf(pFile, "%-80.80s", "STL");

		//write the facet count
		fwrite(&triangleCount, sizeof(int), 1, pFile);

		short spacer = 0;
		float va[3], vb[3], vc[3];
		float n[3];

		//write 50 byte blocks per triangle
		for (int i = 0; i < triangleCount; i++)
		{
			const PointTriangle& t = triangleList[i];

			va[0] = (double)(t.Vertices[0].x);
			va[1] = (double)(t.Vertices[0].y);
			va[2] = (double)(t.Vertices[0].z);

			vb[0] = (double)(t.Vertices[1].x);
			vb[1] = (double)(t.Vertices[1].y);
			vb[2] = (double)(t.Vertices[1].z);

			vc[0] = (double)(t.Vertices[2].x);
			vc[1] = (double)(t.Vertices[2].y);
			vc[2] = (double)(t.Vertices[2].z);

			Sangfroid::Maths::Vector3f v = (t.Vertices[1] - t.Vertices[0]).cross((t.Vertices[2] - t.Vertices[1]));
			Sangfroid::Maths::Vector3f nm = v.unitVec();
			n[0] = nm.i;
			n[1] = nm.j;
			n[2] = nm.k;

			//write normal (12 bytes)
			fwrite(&n[0], sizeof(float), 3, pFile);

			//write vertices (36 bytes)
			fwrite(&va[0], sizeof(float), 3, pFile);
			fwrite(&vb[0], sizeof(float), 3, pFile);
			fwrite(&vc[0], sizeof(float), 3, pFile);

			//write 2 byte spacer
			fwrite(&spacer, sizeof(short), 1, pFile);
		}

		std::fflush(pFile);
		std::fclose(pFile);
	}
	catch (...)
	{
		throw "Failed to write STL file.";
	}
}

/*-----------------------------------------------------------------------------------*/

//Mesh generation

vector<Region*> CustomCloudMesher::CloudToRegions(const UltraVector<Point3f>& points)
{
	std::vector<Region*> regions = GenerateRegions(REGION_COUNT);

	float tolerance = 0.0000001;

	//auto points = PoiseUtils::PointsFromBuffer(buffer);
	if (points.size() == 0)
	{
		return regions;
	}

	Point3f pt;
	Curve* currCurve = nullptr;
	Region* currRegion = nullptr;

	for (int i = 0; i < points.size(); i++)
	{
		pt = points[i];

		Region* region = GetRegion(regions, pt);

		if (region == nullptr)
		{
			continue;
		}

		if (region == currRegion)
		{
			currCurve->AddAtEnd(pt);
		}
		else
		{

			if (i == 0)
			{
				currCurve = new Curve();
				currRegion = region;
				currCurve->AddAtEnd(pt);
			}
			else
			{
				auto nextPoints = GetNextPoints(i, 10, points);
				currCurve->AddAtEnd(pt);
				currCurve->AddAtEnd(nextPoints);

				currRegion->AddCurve(currCurve);

				currCurve = new Curve();
				auto prevPoints = GetPreviousPoints(i, 10, points);
				currRegion = region;
				currCurve->AddAtStart(prevPoints);
				currCurve->AddAtEnd(pt);
			}
		}
	}
	return regions;
}

void CustomCloudMesher::Mesh(std::vector<Region*> regions, const char* stlFileName, const int meshDivisions)
{
	string outFile = stlFileName + string(".stl");
	UltraVector<PointTriangle> triangles;

	std::cout << "Starting mesh..." << endl;

	for (int i = 0; i < regions.size(); i++)
	{
		auto region = regions[i];
		auto curves = region->Curves();
		auto planes = GetIntersectionPlanesForRegion(region, meshDivisions);
		auto intersectionGridCurves = FindPlaneCurveIntersections(curves, planes);
		auto regionTriangles = ConstructMesh(intersectionGridCurves);

		for (int j = 0; j < regionTriangles.size(); j++)
		{
			triangles.push_back(regionTriangles[j]);
		}
	}

	//Store output
	WriteTriangleListToSTL(triangles, outFile);
}

/*-----------------------------------------------------------------------------------*/

//Region separation

void CustomCloudMesher::SeparateAngularSectorRegions(std::vector<Curve*> zCurves, int divisions, double angleOffsets)
{
	auto offset = angleOffsets;

	auto step = 360.0 / divisions;

	std::map<int, std::vector<Curve*>> xCurves;
	for (auto& c : zCurves)
	{
		auto amax = -DBL_MAX;
		auto amin = DBL_MAX;
		auto points = c->Points();
		for (int j = 0; j < points.size(); j++)
		{
			auto& p = points[j];
			auto ar = atan2f(p.y, p.x);
			ar = ToDegrees(ar);
			if (ar < 0)
				ar += 360.0;
			ar += offset;
			if (ar > amax) amax = ar;
			if (ar < amin) amin = ar;
		}

		auto simax = (int)floor(amax / step);
		auto simin = (int)floor(amin / step);

		if (simax == simin)
		{
			auto itr = xCurves.find(simax);
			if (itr != xCurves.end())
			{
				itr->second.push_back(c);
			}
			else
			{
				std::vector<Curve*> cs;
				cs.push_back(c);
				xCurves.insert(std::make_pair(simax, cs));
			}
		}
		else
		{
			auto dif = fabs(amax - amin);
			auto threshold = 360.0 - (2.0 * step);
			if (dif > threshold)
			{
				auto itr = xCurves.find(0);
				if (itr != xCurves.end())
				{
					itr->second.push_back(c);
				}
				else
				{
					std::vector<Curve*> cs;
					cs.push_back(c);
					xCurves.insert(std::make_pair(0, cs));
				}
			}
		}

	}

	for (auto itr = xCurves.begin(); itr != xCurves.end(); itr++)
	{
		auto index = itr->first;
		auto curves = itr->second;
		if (curves.size() == 0)
		{
			continue;
		}

		string filename = "C:/Temp/extractedRegion_" + to_string(index) + ".xyz";
		ofstream f(filename);
		for (int i = 0; i < curves.size(); i++)
		{
			auto points = curves[i]->Points();
			for (int j = 0; j < points.size(); j++)
			{
				auto& p = points[j];
				f << p.x << "    " << p.y << "    " << p.z << endl;
			}
		}
		f.close();
	}
}

/*-----------------------------------------------------------------------------------*/


