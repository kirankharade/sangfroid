#include "MeshGenerator.h"
#include "Containers/UltraVector.h"
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cctype>
#include "CustomCloudMesher.h"
#include "BaseIncludes.h"
#include "FileReader.h"
#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "OKAData.h"
#include <cerrno>
#include <cstring>
#include <sys/stat.h>
#include <chrono>
#include <ctime>
#include <fstream>
#include "OSGTWriter.h"

//------------------------------------------------------------------------------------------------------

using namespace std;
using namespace Sangfroid;
using namespace StructuredMesher;
using namespace Sangfroid::Maths;

//------------------------------------------------------------------------------------------------------

static UltraVector<XTriangle> _sTrimmedList;
static UltraVector<Sangfroid::Maths::Point3f> _cloudPoints;
static UltraVector<XTVertex> _normalUpdatedVertices;
static float _bounds[6];
static XVertex _centre;
static const double ANGLE_TOLERANCE = 0.0001;
static const double PROXIMITY_TOLERANCE = 0.00001;
static OKAData* _okaData = nullptr;

//------------------------------------------------------------------------------------------------------

static void SaveTriangleListAsSTL(const UltraVector<XTriangle>& triangleList, const std::string& filename)
{
	try
	{
		FILE* pFile = std::fopen(filename.c_str(), "wb");
		if (!pFile)
		{
			throw "The STL file cannot be written.";
		}

		int triangleCount = triangleList.size();
		int vertexCount = 3 * triangleCount;
		int numIndices = 3 * triangleCount;

		//write 80 byte header
		fprintf(pFile, "%-80.80s", "STL");

		//write the facet count
		fwrite(&triangleCount, sizeof(int), 1, pFile);

		short spacer = 0;
		float va[3], vb[3], vc[3];
		float n[3];

		//write 50 byte blocks per triangle
		for (int i = 0; i < triangleCount; i++)
		{
			const XTriangle& t = triangleList[i];

			va[0] = (double)(t.Vertices[0].x);
			va[1] = (double)(t.Vertices[0].y);
			va[2] = (double)(t.Vertices[0].z);

			vb[0] = (double)(t.Vertices[1].x);
			vb[1] = (double)(t.Vertices[1].y);
			vb[2] = (double)(t.Vertices[1].z);

			vc[0] = (double)(t.Vertices[2].x);
			vc[1] = (double)(t.Vertices[2].y);
			vc[2] = (double)(t.Vertices[2].z);

			n[0] = t.n.x;
			n[1] = t.n.y;
			n[2] = t.n.z;

			//write normal (12 bytes)
			fwrite(&n[0], sizeof(float), 3, pFile);

			fwrite(&va[0], sizeof(float), 3, pFile);
			fwrite(&vb[0], sizeof(float), 3, pFile);
			fwrite(&vc[0], sizeof(float), 3, pFile);

			//write 2 byte spacer
			fwrite(&spacer, sizeof(short), 1, pFile);
		}

		std::fflush(pFile);
		std::fclose(pFile);
	}
	catch (...)
	{
		throw "Failed to write STL file.";
	}
}

static inline bool EndsWith(std::string const & value, std::string const & ending)
{
	if (ending.size() > value.size()) return false;
	return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

static void OneVertexOutsideBoundary(const XTriangle& t, UltraVector<XTriangle>& tList, double r, const double d[3], int i0, int i1, int i2)
{
	auto fa = (r - d[i0]) / (d[i1] - d[i0]);
	float x = t.Vertices[i0].x + (fa * (t.Vertices[i1].x - t.Vertices[i0].x));
	float y = t.Vertices[i0].y + (fa * (t.Vertices[i1].y - t.Vertices[i0].y));
	float z = t.Vertices[i0].z + (fa * (t.Vertices[i1].z - t.Vertices[i0].z));
	XVertex a(x, y, z);

	auto fb = (r - d[i0]) / (d[i2] - d[i0]);
	x = t.Vertices[i0].x + (fb * (t.Vertices[i2].x - t.Vertices[i0].x));
	y = t.Vertices[i0].y + (fb * (t.Vertices[i2].y - t.Vertices[i0].y));
	z = t.Vertices[i0].z + (fb * (t.Vertices[i2].z - t.Vertices[i0].z));
	XVertex b(x, y, z);

	tList.push_back(XTriangle(a, t.Vertices[i1], b));
	tList.push_back(XTriangle(b, t.Vertices[i1], t.Vertices[i2]));
}

static void TwoVerticesOutsideBoundary(const XTriangle& t, UltraVector<XTriangle>& tList, double r, const double d[3], int i0, int i1, int i2)
{
	auto fa = (r - d[i0]) / (d[i1] - d[i0]);
	float x = t.Vertices[i0].x + (fa * (t.Vertices[i1].x - t.Vertices[i0].x));
	float y = t.Vertices[i0].y + (fa * (t.Vertices[i1].y - t.Vertices[i0].y));
	float z = t.Vertices[i0].z + (fa * (t.Vertices[i1].z - t.Vertices[i0].z));
	XVertex a(x, y, z);

	auto fb = (r - d[i0]) / (d[i2] - d[i0]);
	x = t.Vertices[i0].x + (fb * (t.Vertices[i2].x - t.Vertices[i0].x));
	y = t.Vertices[i0].y + (fb * (t.Vertices[i2].y - t.Vertices[i0].y));
	z = t.Vertices[i0].z + (fb * (t.Vertices[i2].z - t.Vertices[i0].z));
	XVertex b(x, y, z);

	tList.push_back(XTriangle(t.Vertices[i0], a, b));
}

static double Theta(const XVertex& v)
{
	auto theta = Sangfroid::Maths::ToDegrees(atan2f(v.y, v.x));
	if (theta < -ANGLE_TOLERANCE) theta += 360.0;
	return theta;
}

static XVertex Cross(const XVertex& va, const XVertex& vb)
{
	XVertex n;
	n.x = (va.y * vb.z) - (va.z * vb.y);
	n.y = (va.z * vb.x) - (va.x * vb.z);
	n.z = (va.x * vb.y) - (va.y * vb.x);

	auto mag = sqrt((n.x*n.x) + (n.y*n.y) + (n.z*n.z));
	if (!(fabs(mag) < 0.00001f))
	{
		n.x = n.x / mag;
		n.y = n.y / mag;
		n.z = n.z / mag;
	}
	return n;
}

static float Dot(const XVertex& va, const XVertex& vb)
{
	return (va.x * vb.x) + (va.y * vb.y) + (va.z + vb.z);
}

static XVertex CalculateNormalUsingZAxis(const XVertex& a, const XVertex& b, const XVertex& c)
{
	XVertex va(b.x - a.x, b.y - a.y, b.z - a.z);
	XVertex zAxis(0, 0, 1.0);
	XVertex vb(c.x - b.x, c.y - b.y, c.z - b.z);

	XVertex n1 = Cross(va, zAxis);
	XVertex n2 = Cross(vb, zAxis);

	XVertex n(n1.x + n2.x, n1.y + n2.y, n1.z + n2.z);
	auto mag = sqrt((n.x*n.x) + (n.y*n.y) + (n.z*n.z));
	if (!fabs(mag) < 0.00001f)
	{
		n.x = n.x / mag;
		n.y = n.y / mag;
		n.z = n.z / mag;
	}
	return n;
}

//------------------------------------------------------------------------------------------------------

MeshGenerator::MeshGenerator(const std::string& infile, const std::string& outfile, const int meshDivisions)
{
	_inputFilename = infile;
	_outputFilename = outfile;
	_meshRadialDivisions = meshDivisions;
}

MeshGenerator::~MeshGenerator()
{
}

void MeshGenerator::LoadCloud(const std::string& filename)
{
	FileReader* reader = new FileReader();

	_inputFilename = filename;

	std::cout << "Loading point cloud \"" << _inputFilename.c_str() << "\"" << endl;
	auto f = _inputFilename;
	
	std::transform(f.begin(), f.end(), f.begin(), ::tolower);
	if (EndsWith(f, ".csv"))
	{
		std::cout << "Loading CSV..." << endl;
		if (!reader->ReadInPoints_csv(_inputFilename, _cloudPoints))
		{
			throw "Problems in loading the point-cloud.";
		}
	}
	else if(EndsWith(f, ".oka"))
	{
		std::cout << "Loading CSV..." << endl;
		auto okaData = reader->ReadInPoints_oka(_inputFilename);
		if (nullptr == okaData)
		{
			throw "Problems in loading the point-cloud.";
		}
		_okaData = okaData;
	}
	else
	{
		throw "Unsupported point-cloud format.";
	}

	std::cout << "Finished loading." << endl;
	std::cout << "Cloud bounds      : (" << _bounds[0] << ", " << _bounds[1] << ", " << _bounds[2] << "), (" << _bounds[3] << "," << _bounds[4] << "," << _bounds[5] << ")" << endl;
	std::cout << "Total points read : " << _cloudPoints.size() << endl << endl;

	_centre = XVertex(0.5f * (_bounds[0] + _bounds[3]), 0.5f * (_bounds[1] + _bounds[4]), 0.5f * (_bounds[2] + _bounds[5]));
}

void MeshGenerator::DumpCloudToOSGT()
{
	OSGTWriter w(_okaData);
	w.Write_separated();
	w.Write_combined();
	w.Write_combined_sector(0.0, 90.0);
}

void MeshGenerator::StructuredReconstruction()
{
	//auto pBuffer = new PointBuffer(_csvCloudPoints, 
	//	Sangfroid::Maths::Point3f(_bounds[0], _bounds[1], _bounds[2]), 
	//	Sangfroid::Maths::Point3f(_bounds[3], _bounds[4], _bounds[5]));

	std::cout << "Separating cloud into regions..." << endl;
	auto regions = CustomCloudMesher::CloudToRegions(_cloudPoints);
	std::cout << "Regions generated..." << endl;

	std::cout << "Meshing started..." << endl;
	CustomCloudMesher::Mesh(regions, _outputFilename.c_str(), _meshRadialDivisions);
}

void MeshGenerator::SaveToSTL(const std::string& outputFile)
{
	std::cout << "Saving mesh as STL...";
	_outputFilename = outputFile;
	if (_outputFilename.length() == 0)
	{
		_outputFilename = _inputFilename + ".stl";
	}
	SaveTriangleListAsSTL(_sTrimmedList, _outputFilename);
	std::cout << "Done." << endl;
}

//------------------------------------------------------------------------------------------------------
