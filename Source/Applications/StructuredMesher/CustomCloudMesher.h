﻿#pragma once

#include <iostream>
#include "BaseIncludes.h"
#include "Region.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "Containers/UltraVector.h"
#include <vector>

namespace StructuredMesher
{

	/*-----------------------------------------------------------------------------------*/

	class CustomCloudMesher
	{
	public:

		static std::vector<Region*> CloudToRegions(const Sangfroid::UltraVector<Sangfroid::Maths::Point3f>& points);

		static void SeparateAngularSectorRegions(std::vector<Curve*> zCurves, int divisions, double angleOffsets);

		static void Mesh(std::vector<Region*> regions, const char* stlFileName, const int meshDivisions);

	};
	/*-----------------------------------------------------------------------------------*/

}
