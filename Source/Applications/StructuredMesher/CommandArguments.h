#pragma once

#include <string>
#include <iostream>
#include "cxxopts.hpp"

//-------------------------------------------------------------------------------------

struct CommandArguments
{
	std::string _inputFile;
	std::string _outputFile;
	int _meshRadialDivisions;

	CommandArguments()
	{
		_inputFile = "";
		_outputFile = "output_mesh";
		_meshRadialDivisions = 975;
	}

	static CommandArguments Process(int argc, const char *argv[])
	{
		CommandArguments a;

		try
		{
			cxxopts::Options options(argv[0], "A triangulation mesh generator for dense, unstructured point cloud");
			options
				.positional_help("[optional args]")
				.show_positional_help();

			options.add_options()
				("i,input", "Input file <Must be specified> : Accepted file formats are .asc", cxxopts::value<std::string>())
				("o,output", "Output file <Optional> : STL file format", cxxopts::value<std::string>())
				("d,divisions", "Radial divisions of the mesh (To change mesh density) <Optional>", cxxopts::value<int>())
				("h,help", "Print help")
				;

			std::cout << std::endl;

			auto result = options.parse(argc, argv);

			if (result.count("help"))
			{
				std::cout << options.help({ "" }) << std::endl;
				exit(0);
			}

			if (result.count("input"))
			{
				a._inputFile = result["input"].as<std::string>();
				std::cout << "Input   = " << a._inputFile << std::endl << std::endl;
			}
			else
			{
				std::cout << "XMesher: Input file argument is missing. Exiting..." << std::endl;
				exit(1);
			}

			if (result.count("output"))
			{
				a._outputFile = result["output"].as<std::string>();
				std::cout << "Output  = " << a._outputFile << std::endl << std::endl;
			}
			else
			{
				a._outputFile = a._inputFile + ".stl";
				std::cout << "XMesher: Output file not given. Using " << a._outputFile << " as output file." << std::endl << std::endl;
			}

			if (result.count("divisions"))
			{
				a._meshRadialDivisions = result["divisions"].as<int>();
				std::cout << "Mesh divisions = " << a._meshRadialDivisions << std::endl << std::endl;
			}
		}
		catch (const cxxopts::OptionException& e)
		{
			std::cout << "error parsing options: " << e.what() << std::endl;
			exit(1);
		}

		return a;
	}

};

//-------------------------------------------------------------------------------------

