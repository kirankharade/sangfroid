#pragma once
#include "Containers/UltraVector.h"
#include "BaseIncludes.h"
#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "OKAData.h"

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;

class FileReader
{
public:

	FileReader();

	~FileReader();

	bool ReadInPoints_csv(const std::string& filename, UltraVector<Sangfroid::Maths::Point3f>& cloudPoints);

	OKAData* ReadInPoints_oka(const std::string& filename);

};

