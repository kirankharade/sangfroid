#include "OSGTWriter.h"
#include "MeshGenerator.h"
#include "Containers/UltraVector.h"
#include <algorithm>
#include <iostream>
#include <cstring>
#include <cctype>
#include "CustomCloudMesher.h"
#include "BaseIncludes.h"
#include "FileReader.h"
#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "OKAData.h"
#include <cerrno>
#include <cstring>
#include <sys/stat.h>
#include <chrono>
#include <ctime>
#include <fstream>

using namespace std;
using namespace Sangfroid;
using namespace StructuredMesher;
using namespace Sangfroid::Maths;

//------------------------------------------------------------------------------------------------------
static string osgt_prefix =
"#Ascii Scene                    \n"
"#Version 78                     \n"
"#Generator OpenSceneGraph 2.9.17\n"
"                                \n"
"osg::Group {					 \n"
"	UniqueID 1					 \n"
"	Name \"Pointset.osg\"		 \n"
"	DataVariance STATIC		 \n"
"	Children 1 {				 \n"
"	osg::Geode {				 \n"
"		UniqueID 2				 \n"
"		DataVariance STATIC	 \n";

static string osgt_drawable_1 =
"		Drawables 1 {			 \n";

static string osgt_drawable_2 =
"		Drawables 2 {			 \n";

static string osgt_Single_Geometry =
"		osg::Geometry {			 \n"
"			UniqueID 3			 \n"
"			DataVariance STATIC \n"
"			PrimitiveSetList 1 { \n";

static string osgt_VertexArrayBlock =
"          }                                 \n"
"			VertexData {                     \n"
"			  Array TRUE ArrayID 1 Vec3fArray ";

static string osgt_DrawableEndX =
"            }										\n"
"            Indices FALSE							\n"
"            Binding BIND_OVERALL					\n"
"            Normalize 0							\n"
"		  }											\n"
"		  ColorData {								\n"
"			  Array TRUE ArrayID 3 Vec4fArray 1 {	\n";

static string osgt_DrawableEndY =
"		  }											\n"
"		  Indices FALSE								\n"
"			  Binding BIND_OVERALL					\n"
"			  Normalize 0							\n"
"		  }											\n"
"		}											\n";

static string osgt_suffix =
"	  }												\n"
"	}												\n"
"  }												\n"
"}													\n"
"													\n";

static std::vector<string> _colors =
{
	"              1 0 0 1 ",
	"              0 0 1 1 ",
	"              0 0 0 1 ",
	"              0 1 0 1 ",
	"              1 0 1 1 ",
	"              0 1 1 1 ",
	"              1 1 0 1 ",
	"              1 0.5 0 1 ",
	"              0.5 1 0 1 ",
	"              1 0 0.5 1 ",
};

//------------------------------------------------------------------------------------------------------

static std::string GetCurrentTimeString()
{
	auto now = std::chrono::system_clock::now();
	auto nowt = std::chrono::system_clock::to_time_t(now);
	auto t = gmtime(&nowt);
	auto str = std::to_string(t->tm_year) +
		std::to_string(t->tm_mon) +
		std::to_string(t->tm_mday) +
		std::to_string(t->tm_hour) +
		std::to_string(t->tm_min) +
		std::to_string(t->tm_sec);
	return str;
}
//------------------------------------------------------------------------------------------------------

static void WriteToOSGT_separated(std::string& file, OKACloudSection* section, bool asPoints)
{
	std::ofstream o;
	o.open(file, std::ofstream::out);
	o << osgt_prefix;
	o << osgt_drawable_1;
	o << osgt_Single_Geometry;

	if (asPoints)
	{
		o << "            DrawArrays GL_POINTS 0 " << section->Points.size() << endl;
	}
	else
	{
		o << "            DrawArrays GL_LINE_STRIP 0 " << section->Points.size() - 1 << endl;
	}
	auto pointCount = section->Points.size();
	o << osgt_VertexArrayBlock << pointCount << " {\n";
	for (size_t i = 0; i < pointCount; i++)
	{
		auto point = section->Points[i];
		o << "				" << point.x << "    " << point.y << "    " << point.z << "    " << endl;
	}
	o << osgt_DrawableEndX;
	if (asPoints)
	{
		o << "              1 1 1 1 " << endl;
	}
	else
	{
		o << "              1 0 0 1 " << endl;
	}
	o << osgt_DrawableEndY;
	o << osgt_suffix;

	o.close();
}
//------------------------------------------------------------------------------------------------------

OSGTWriter::OSGTWriter(OKAData * okaData)
{
	_okaData = okaData;
	_currTime = GetCurrentTimeString();
}

void OSGTWriter::Write_separated()
{
	auto str = "C:/Temp/PointClouds_osgt/cloud_scan_";
	auto prefix = "";

	for (size_t i = 0; i < _okaData->CloudSections.size(); i++)
	{
		auto section = _okaData->CloudSections[i];
		auto idx = std::to_string(i);
		auto fileA = str + idx + "_points_" + _currTime + ".osgt";
		auto fileB = str + idx + "_linseg_" + _currTime + ".osgt";

		WriteToOSGT_separated(fileA, section, true);
		WriteToOSGT_separated(fileB, section, false);
	}
}
//------------------------------------------------------------------------------------------------------

void OSGTWriter::Write_combined()
{
	auto drawableCount = _okaData->CloudSections.size();

	auto file = "C:/Temp/PointClouds_osgt/cloud_scan_combined_" + _currTime + ".osgt";
	std::ofstream o;
	o.open(file, std::ofstream::out);
	o << osgt_prefix; 
	o << "		Drawables " << drawableCount << " {" << endl;

	size_t colorIndex = 0;
	size_t uniqueId = 3;
	size_t arrayID = 1;

	for (size_t i = 0; i < _okaData->CloudSections.size(); i++)
	{
		if (colorIndex >= _colors.size())
		{
			colorIndex = 0;
		}
		auto section = _okaData->CloudSections[i];
		auto pointCount = section->Points.size();

		o << "		osg::Geometry { " << endl;
		o << "			UniqueID " << uniqueId << endl;
		o << "			DataVariance STATIC " << endl;
		o << "			PrimitiveSetList 1 { " << endl;
		o << "          DrawArrays GL_LINE_STRIP 0 " << (pointCount - 1) << endl;
		o << "          }" << endl;
		o << "			VertexData {" << endl;
		o << "			  Array TRUE ArrayID " << arrayID << " Vec3fArray " << pointCount << " {" << endl;
		arrayID++;

		for (size_t j = 0; j < pointCount; j++)
		{
			auto point = section->Points[j];
			o << "				" << point.x << "    " << point.y << "    " << point.z << "    " << endl;
		}
		o << "            }						" << endl;
		o << "            Indices FALSE			" << endl;
		o << "            Binding BIND_OVERALL	" << endl;
		o << "            Normalize 0			" << endl;
		o << "		  }							" << endl;
		o << "		  ColorData {				" << endl;
		o << "			  Array TRUE ArrayID " << arrayID << " Vec4fArray 1 {" << endl;
		arrayID++;

		o << _colors[colorIndex] << endl;
		o << osgt_DrawableEndY;
		colorIndex ++;
		uniqueId++;
	}
	o << osgt_suffix;
}
//------------------------------------------------------------------------------------------------------

static bool IsWithinSector(const Sangfroid::Maths::Point3f& point, 
	double startAngleDeg, double endAngleDeg)
{
	auto angle = ToDegrees(atan2f(point.y, point.x));
	if (angle < 0)	angle += 360.0;
	if (angle > 360.0) angle -= 360.0;

	return angle >= startAngleDeg && angle <= endAngleDeg;
}
//------------------------------------------------------------------------------------------------------

static bool HasProximity(const std::vector<Sangfroid::Maths::Point3f> currentSector, const Sangfroid::Maths::Point3f& point)
{
	auto currentSectorSize = currentSector.size();
	if (currentSectorSize == 0)
	{
		return true;
	}
	Point3f lastPoint = currentSector[currentSectorSize - 1];
	if (lastPoint.dist(point) < 3)
	{
		return true;
	}
	return false;
}
//------------------------------------------------------------------------------------------------------

std::vector<std::vector<Sangfroid::Maths::Point3f>> GetSectorCurves(
	OKACloudSection* section,
	double startAngleDeg, double endAngleDeg)
{
	auto pointCount = section->Points.size();

	bool sectorStarted = false;
	
	std::vector<std::vector<Sangfroid::Maths::Point3f>> sectors;

	std::vector<Sangfroid::Maths::Point3f> currentSector;

	for (size_t j = 0; j < pointCount; j++)
	{
		auto point = section->Points[j];
		if (IsWithinSector(point, startAngleDeg, endAngleDeg) && 
			HasProximity(currentSector, point))
		{
			currentSector.push_back(point);
		}
		else
		{
			if (currentSector.size() > 2)
			{
				sectors.push_back(currentSector);
				currentSector = std::vector<Sangfroid::Maths::Point3f>();
			}
		}
	}
	if (currentSector.size() > 2)
	{
		sectors.push_back(currentSector);
	}

	return sectors;
}
//------------------------------------------------------------------------------------------------------

void OSGTWriter::Write_combined_sector(double startAngleDeg, double endAngleDeg)
{
	auto drawableCount = _okaData->CloudSections.size();

	auto file = "C:/Temp/PointClouds_osgt/cloud_scan_combined_sector_" + _currTime + ".osgt";
	std::ofstream o;
	o.open(file, std::ofstream::out);
	o << osgt_prefix;
	o << "		Drawables " << drawableCount << " {" << endl;

	size_t colorIndex = 0;
	size_t uniqueId = 3;
	size_t arrayID = 1;

	for (size_t i = 0; i < _okaData->CloudSections.size(); i++)
	{
		if (colorIndex >= _colors.size())
		{
			colorIndex = 0;
		}
		auto section = _okaData->CloudSections[i];
		auto pointCount = section->Points.size();
		auto sectorCurves = GetSectorCurves(section, startAngleDeg, endAngleDeg);
		vector<Point3f> allPoints;

		o << "		osg::Geometry { " << endl;
		o << "			UniqueID " << uniqueId << endl;
		o << "			DataVariance STATIC " << endl;
		o << "			PrimitiveSetList " << sectorCurves.size() << " { " << endl;

		int currPointIndex = 0;
		for (auto k = 0; k < sectorCurves.size(); k++)
		{
			auto c = sectorCurves[k];
			auto pointsInCurve = c.size();
			if (pointsInCurve < 3)
			{
				continue;
			}
			allPoints.insert(allPoints.end(), c.begin(), c.end());

			o << "          DrawArrays GL_LINE_STRIP " << currPointIndex  << " " << (pointsInCurve - 1) << endl;
			currPointIndex += pointsInCurve;
		}

		o << "          }" << endl;
		o << "			VertexData {" << endl;
		o << "			  Array TRUE ArrayID " << arrayID << " Vec3fArray " << currPointIndex << " {" << endl;
		arrayID++;

		for (size_t j = 0; j < allPoints.size(); j++)
		{
			auto point = allPoints[j];
			o << "				" << point.x << "    " << point.y << "    " << point.z << "    " << endl;
		}
		o << "            }						" << endl;
		o << "            Indices FALSE			" << endl;
		o << "            Binding BIND_OVERALL	" << endl;
		o << "            Normalize 0			" << endl;
		o << "		  }							" << endl;
		o << "		  ColorData {				" << endl;
		o << "			  Array TRUE ArrayID " << arrayID << " Vec4fArray 1 {" << endl;
		arrayID++;

		o << _colors[colorIndex] << endl;
		o << osgt_DrawableEndY;
		colorIndex++;
		uniqueId++;
	}
	o << osgt_suffix;
}
//------------------------------------------------------------------------------------------------------

