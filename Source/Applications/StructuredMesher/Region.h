#pragma once
#include <iostream>
#include "BaseIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "Curve.h"
#include <vector>

namespace StructuredMesher
{
	class Region
	{
		int ID;

		std::vector<Curve*> _curves;
		float _startAngle;
		float _endAngle;

	public:

		Region(float startAngle, float endAngle) :_startAngle(startAngle), _endAngle(endAngle) {}

		Region() :_startAngle(0.0f), _endAngle(0.0f) {}

		~Region() {}

		void AddCurve(Curve* curve) { _curves.push_back(curve); }

		float StartAngle() const { return _startAngle; }

		float EndAngle() const { return _endAngle; }

		const std::vector<Curve*>& Curves() const { return _curves; }
	};

}
