#pragma once

#include "OKAData.h"

class OSGTWriter
{
	OKAData* _okaData;
	std::string _currTime;

public:
	OSGTWriter(OKAData* okaData);

	void Write_separated();
	void Write_combined();
	void Write_combined_sector(double startAngleDeg, double endAngleDeg);
};

