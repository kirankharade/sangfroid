﻿using Dazzle.Appearance;
using Dazzle.Controls.Tabs;
using System.Windows;
using System;
using System.Windows.Media;

namespace Sangfroid.Viewer.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            TheMenu.HeaderText = "Menu";
            TheMenu.HeaderIcon = "./Images/Menu_white.png";
        }

        private void LoadMainMenu()
        {
            var models_icon = "./Images/Models_white.png";
            var settingsIcon = "./Images/Settings_white.png";
            var operationsIcon = "./Images/Operations_white.png";
            var analysisIcon = "./Images/Analysis_white.png";
            var menuIconSize = 30;

            TheMenu.CollapsedWidth = 50;

            var modelExplorer = new ModelExplorer();
            modelExplorer.TheModelExplorer.BackgroundBrush = new SolidColorBrush(Color.FromRgb(35,40,42));
            modelExplorer.TheModelExplorer.BoundaryBrush = new SolidColorBrush(Colors.DarkGray);
            modelExplorer.TheModelExplorer.HeaderIcon = models_icon;
            modelExplorer.TheModelExplorer.HeaderText = "Model Explorer";
            modelExplorer.TheModelExplorer.Background = new SolidColorBrush(Color.FromRgb(35, 40, 42));

            TheMenu.AddMenuItem(new Dazzle.Controls.SelectableItem("Models", modelExplorer, models_icon, models_icon) { IconHeight = menuIconSize, IconWidth = menuIconSize });
            TheMenu.AddMenuItem(new Dazzle.Controls.SelectableItem("Operations", new TabberControl(), operationsIcon, operationsIcon) { IconHeight = menuIconSize, IconWidth = menuIconSize });
            TheMenu.AddMenuItem(new Dazzle.Controls.SelectableItem("Analysis", new TabberControl(), analysisIcon, analysisIcon) { IconHeight = menuIconSize, IconWidth = menuIconSize });
            TheMenu.AddMenuItem(new Dazzle.Controls.SelectableItem("Settings", new TabberControl(), settingsIcon, settingsIcon) { IconHeight = menuIconSize, IconWidth = menuIconSize });
            TheMenu.MenuSelectionChanged += OnMenuSelectionChanged;
        }

        private void OnMenuSelectionChanged(object sender, SelectableItemSelectionChangedEventArgs e)
        {
            InteractionArea.Content = e.Current.StoredContent;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AppearanceManager.CurrentSkin = SkinType.Default;
            AppearanceManager.SetAppearance(this);
            AppearanceManager.RegisterUI(this);
            AppearanceManager.CurrentLayout = LayoutType.WideScreen;
            LoadMainMenu();
        }
    }
}
