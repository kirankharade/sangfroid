
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "Line.h"
#include "Plane.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Line::Line()
    : m_point(Point3f())
    , m_direction(Vector3f(0.0f, 0.0f, 1.0f))
{
}
//---------------------------------------------------------------------------------------

Line::Line(const Point3f& point, const Vector3f& direction)
    : m_point(point)
    , m_direction(direction)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Line::Line(const Point3f& start, const Point3f& end)
    : m_point(start)
    , m_direction(end - start)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Line::Line(const Line& line)
{
    m_point = line.m_point;
    m_direction = line.m_direction;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Line::Line(const Ray& ray)
{
    m_point = ray.getPoint();
    m_direction = ray.getDirection();
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Line::Line(const LineSegment& lineSegment)
{
    m_point = lineSegment.getFirstPoint();
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Line::Line(const LineSegment2D& lineSegment)
{
    Point2f point = lineSegment.getFirstPoint();
    m_point = Point3f(point.x, point.y, 0);
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Line::~Line()
{
}
//---------------------------------------------------------------------------------------

void Line::set(const Point3f& point, const Vector3f& direction)
{
    m_point = point;
    m_direction = direction;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

void Line::set(const Point3f& start, const Point3f& end)
{
    m_point = start;
    m_direction = end - start;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Point3f Line::getPoint() const
{
    return m_point;
}
//---------------------------------------------------------------------------------------

Vector3f Line::getDirection() const
{
    return m_direction;
}
//---------------------------------------------------------------------------------------

bool Line::isIntersecting(const Plane* pPlane) const
{
    if (!pPlane)
    {
        return false;
    }
    Vector3f planeNormal = pPlane->getNormal();
    Vector3f lineDirection = m_direction;
    planeNormal.normalize();
    lineDirection.normalize();

    if (EQ_32(planeNormal.dot(lineDirection), 0.0f))
    {
        //Plane normal and line direction are perpendicular to each other,
        //which means the line is parallel to the plane.
        //Now check whether line lies in plane or not.
        if (EQ_32(pPlane->distFromPoint(m_point), 0.0f))
        {
            //Line lies in plane
            return true;
        }
        return false;
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool Line::isIntersecting(const Plane* pPlane, Point3f& intersectionPoint) const
{
    if (!pPlane)
    {
        return false;
    }
    Vector3f lineDirection = m_direction;
    lineDirection.normalize();
    Vector3f planeNormal = pPlane->getNormal();
    planeNormal.normalize();

    const AReal32 sampleDistance = 100.0f;
    Point3f anotherLinePt = m_point + (lineDirection * sampleDistance);
    Vector3f vecLine = anotherLinePt - m_point;

    AReal32 d1 = planeNormal.dot(vecLine);
    if (EQ_32(d1, 0.0f))
    {
        //Plane normal and line direction are perpendicular to each other,
        //which means the line is parallel to the plane.
        //Now check whether line lies in plane or not.
        if (EQ_32(pPlane->distFromPoint(m_point), 0.0f))
        {
            //Line lies in plane
            return true;
        }
        return false;
    }

    AReal32 d2 = planeNormal.dot((pPlane->getPoint() - m_point));
    AReal32 u = d2 / d1;
    intersectionPoint = m_point + (vecLine * u);

    return true;
}
//---------------------------------------------------------------------------------------

bool Line::passesThrough(const Point3f& point, const AReal32 tolerance) const
{
    if (point.dist(closestPoint(point)) <= tolerance)
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

Point3f Line::closestPoint(const Point3f& point) const
{
    if (m_direction.isNull())
    {
        return m_point;
    }
    //Get another point on line
    const AReal32 cDist = 100.0f;
    Point3f p2 = m_point + (m_direction * cDist);
    Vector3f v = p2 - m_point;
    //Do not check denominator for zero, already done above implicitly.
    AReal32 t = v.dot((point - m_point)) / v.dot(v);
    return Point3f(m_point + (v * t));
}
//---------------------------------------------------------------------------------------

AReal32 Line::distFromPoint(const Point3f& point) const
{
    Point3f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//operators

Line& Line::operator = (const Line& line)
{
    m_point = line.m_point;
    m_direction = line.m_direction;
    return *this;
}
//---------------------------------------------------------------------------------------

Line& Line::operator = (const Ray& ray)
{
    m_point = ray.getPoint();
    m_direction = ray.getDirection();
    m_direction.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

Line& Line::operator = (const LineSegment& lineSegment)
{
    m_point = lineSegment.getFirstPoint();
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

Line& Line::operator = (const LineSegment2D& lineSegment)
{
    Point2f point = lineSegment.getFirstPoint();
    m_point = Point3f(point.x, point.y, 0);
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

bool Line::operator == (const Line& line) const
{
    Vector3f dir = m_direction;
    Vector3f otherDir = line.m_direction;
    dir.normalize();
    otherDir.normalize();

    if (dir == otherDir)
    {
        if (m_point == line.m_point)
        {
            return true;
        }
        if (passesThrough(line.m_point))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Line::operator != (const Line& line) const
{
    if (!(*this == line))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Line::operator < (const Line& line) const
{
    if (m_point < line.m_point)
    {
        return true;
    }
    if (m_point > line.m_point)
    {
        return false;
    }
    if (m_direction < line.m_direction)
    {
        return true;
    }
    if (m_direction > line.m_direction)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------

