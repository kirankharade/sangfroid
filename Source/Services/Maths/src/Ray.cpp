
//---------------------------------------------------------------------------------------

#include "Ray.h"
#include "MathsSettings.h"
#include "Plane.h"
#include "Line.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Ray::Ray()
    : m_point(Point3f())
    , m_direction(Vector3f(0.0f, 0.0f, 1.0f))
{
}
//---------------------------------------------------------------------------------------

Ray::Ray(const Point3f& point, const Vector3f& direction)
    : m_point(point)
    , m_direction(direction)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Ray::Ray(const Point3f& firstPoint, const Point3f& secondPoint)
    : m_point(firstPoint)
    , m_direction(secondPoint - firstPoint)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Ray::Ray(const Ray& ray)
{
    m_point = ray.m_point;
    m_direction = ray.m_direction;
}
//---------------------------------------------------------------------------------------

Ray::~Ray()
{
}
//---------------------------------------------------------------------------------------

void Ray::set(const Point3f& point, const Vector3f& direction)
{
    m_point = point;
    m_direction = direction;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

void Ray::set(const Point3f& firstPoint, const Point3f& secondPoint)
{
    m_point = firstPoint;
    m_direction = secondPoint - firstPoint;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

Point3f Ray::getPoint() const
{
    return m_point;
}
//---------------------------------------------------------------------------------------

Vector3f Ray::getDirection() const
{
    return m_direction;
}
//---------------------------------------------------------------------------------------

bool Ray::isIntersecting(const Plane* pPlane) const
{
    Point3f intersectionPoint;
    return isIntersecting(pPlane, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool Ray::isIntersecting(const Plane* pPlane, Point3f& intersectionPoint) const
{
    Line line(m_point, m_direction);

    if (false == line.isIntersecting(pPlane, intersectionPoint))
    {
        return false;
    }

    const AReal32 dist = 100.0f;
    Point3f secondPoint = (m_point + (m_direction * dist));
    Vector3f v1 = secondPoint - m_point;
    Vector3f v2 = intersectionPoint - m_point;

    AReal32 u = v1.dot(v2) / v1.dot(v1);

    if (u >= (-TOLERANCE_32))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Ray::passesThrough(const Point3f& point, const AReal32 tolerance) const
{
    Line line(m_point, m_direction);

    if (line.passesThrough(point, tolerance))
    {
        const AReal32 dist = 100.0f;
        Point3f secondPoint = m_point + (m_direction * dist);

        Vector3f v1 = secondPoint - m_point;
        Vector3f v2 = point - m_point;

        AReal32 u = v1.dot(v2) / v1.dot(v1);

        if (u >= (-TOLERANCE_32))
        {
            return true;
        }

        //AReal32 xProjDist = secondPoint.x - m_point.x;
        //AReal32 yProjDist = secondPoint.y - m_point.y;
        //AReal32 zProjDist = secondPoint.z - m_point.z;
        //AReal32 u = 0;
        //if(!EQ_32(xProjDist, 0))
        //{
        //    u = (point.x - m_point.x) / xProjDist;
        //}
        //else if(!EQ_32(yProjDist, 0))
        //{
        //    u = (point.y - m_point.y) / yProjDist;
        //}
        //else if(!EQ_32(zProjDist, 0))
        //{
        //    u = (point.z - m_point.z) / zProjDist;
        //}
        //else
        //{
        //    return false;
        //}
        //if(u >= 0.0f)
        //{
        //    return true;
        //}
    }
    return false;
}
//---------------------------------------------------------------------------------------


Point3f Ray::closestPoint(const Point3f& point) const
{
    Line line(m_point, m_direction);
    Point3f pt = line.closestPoint(point);
    if (point.dist(pt) > point.dist(m_point))
    {
        return m_point;
    }
    return pt;
}
//---------------------------------------------------------------------------------------

AReal32 Ray::distFromPoint(const Point3f& point) const
{
    Point3f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//Assignment operator

Ray& Ray::operator = (const Ray& ray)
{
    m_point = ray.m_point;
    m_direction = ray.m_direction;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool Ray::operator == (const Ray& ray) const
{
    Vector3f dir = m_direction;
    Vector3f otherDir = ray.m_direction;
    dir.normalize();
    otherDir.normalize();

    if ((dir == otherDir) && (m_point == ray.m_point))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Ray::operator != (const Ray& ray) const
{
    if (!(*this == ray))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Ray::operator < (const Ray& ray) const
{
    if (m_point < ray.m_point)
    {
        return true;
    }
    if (m_point > ray.m_point)
    {
        return false;
    }
    if (m_direction < ray.m_direction)
    {
        return true;
    }
    if (m_direction > ray.m_direction)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------
