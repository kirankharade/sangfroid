
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "Point2f.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Point2f::Point2f()
{
    x = 0.0f;
    y = 0.0f;
}
//---------------------------------------------------------------------------------------

Point2f::Point2f(const AReal32 v[2])
{
    x = v[0];
    y = v[1];
}
//---------------------------------------------------------------------------------------

Point2f::Point2f(const AReal32& a, const AReal32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

Point2f::Point2f(const Point2f& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

Point2f::Point2f(const Point2i& point)
{
    x = (AReal32)point.x;
    y = (AReal32)point.y;
}
//---------------------------------------------------------------------------------------

Point2f::Point2f(const Point3f& point)
{
    x = (AReal32)point.x;
    y = (AReal32)point.y;
}
//---------------------------------------------------------------------------------------

void Point2f::set(const AReal32& a, const AReal32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

void Point2f::set(const Point2f& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

AReal32 Point2f::dist(const Point2f& point) const
{
    return sqrt((AReal32)(((x - point.x)*(x - point.x)) + ((y - point.y)*(y - point.y))));
}
//---------------------------------------------------------------------------------------

//operators

AReal32& Point2f::operator [] (const AIndex32& index)
{
    if (0 == index) return x;
    if (1 == index) return y;
    throw "Point2f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AReal32& Point2f::operator [] (const AIndex32& index) const
{
    if (0 == index) return x;
    if (1 == index) return y;
    throw "Point2f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

Vector3f Point2f::operator - (const Point2f& point) const
{
    return Vector3f(x - point.x, y - point.y, 0);
}
//---------------------------------------------------------------------------------------

Point2f Point2f::operator + (const Point2f& point) const
{
    return Point2f(x + point.x, y + point.y);
}
//---------------------------------------------------------------------------------------

Point2f Point2f::operator + (const Vector3f& vec) const
{
    return Point2f(x + vec.x, y + vec.y);
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator += (const Point2f& point)
{
    x += point.x;
    y += point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator = (const Point2f& point)
{
    x = point.x;
    y = point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator -= (const Vector3f& vec)
{
    x -= vec.x;
    y -= vec.y;
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator += (const Vector3f& vec)
{
    x += vec.x;
    y += vec.y;
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator = (const Point3f& point)
{
    x = point.x;
    y = point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator = (const Point2i& point)
{
    x = (AReal32)point.x;
    y = (AReal32)point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

bool Point2f::operator == (const Point2f& point) const
{
    if (EQ_32(x, point.x) && EQ_32(y, point.y))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Point2f::operator != (const Point2f& point) const
{
    if (!EQ_32(x, point.x) && !EQ_32(y, point.y))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Point2f::operator < (const Point2f& point) const
{
    if (EQ_32(x, point.x) && EQ_32(y, point.y))
    {
        return false;
    }
    if (x < point.x)      return true;
    else if (x > point.x) return false;
    else return (y < point.y);
}
//---------------------------------------------------------------------------------------

bool Point2f::operator > (const Point2f& point) const
{
    if (EQ_32(x, point.x) && EQ_32(y, point.y))
    {
        return false;
    }
    if (x > point.x)      return true;
    else if (x < point.x) return false;
    else return (y > point.y);
}
//---------------------------------------------------------------------------------------

Point2f Point2f::operator * (const AReal32& scalar) const
{
    return Point2f((AReal32)(x * scalar), (AReal32)(y * scalar));
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator *= (const AReal32& scalar)
{
    x = (AReal32)(x * scalar);
    y = (AReal32)(y * scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f Point2f::operator / (const AReal32& scalar) const
{
    return Point2f((AReal32)(x / scalar), (AReal32)(y / scalar));
}
//---------------------------------------------------------------------------------------

Point2f& Point2f::operator /= (const AReal32& scalar)
{
    x = (AReal32)(x / scalar);
    y = (AReal32)(y / scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

Point2f Point2f::operator - () const
{
    return Point2f(-x, -y);
}
//---------------------------------------------------------------------------------------

