
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "MathUtils.h"
#include "MathsMiscUtils.h"
#include "Point2f.h"
#include "Point3f.h"
#include "Ray.h"
#include "LineSegment.h"
#include "Plane.h"
#include "PointTriangle.h"
#include <cfloat>
#include <algorithm>
#include <tuple>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

AReal32 MathUtils::getCornerAngleDeg(const Point2f& pa, const Point2f& pb, const Point2f& pc)
{
    Vector3f v1 = pa - pb;
    Vector3f v2 = pc - pb;
    AReal32 angleDeg = v1.angleDeg(v2);

    Vector3f normal = (pb - pa).cross(pc - pb);
    normal.normalize();

    if (normal.z > 0.0f)
    {
        angleDeg = (AReal32)(360.0 - angleDeg);
    }

    return angleDeg;
}
//---------------------------------------------------------------------------------------

AReal32 MathUtils::getCornerAngleDeg(const Point3f& pa, const Point3f& pb, const Point3f& pc)
{
    Vector3f v1 = pa - pb;
    Vector3f v2 = pc - pb;
    AReal32 angleDeg = v1.angleDeg(v2);

    Vector3f normal = (pb - pa).cross(pc - pb);
    normal.normalize();

    if (normal.z > 0.0f)
    {
        angleDeg = (AReal32)(360.0 - angleDeg);
    }

    return angleDeg;
}
//---------------------------------------------------------------------------------------

void MathUtils::sortAscending(std::vector<Point2f>& points)
{
    AUInt32 size = points.size();

    for (AUInt32 i = 0; i < size; i++)
    {
        for (AUInt32 j = 0; j < size; j++)
        {
            if (points[i] > points[j])
            {
                Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void MathUtils::sortAscending(std::vector<Point3f>& points)
{
    AUInt32 size = points.size();

    for (AUInt32 i = 0; i < size; i++)
    {
        for (AUInt32 j = 0; j < size; j++)
        {
            if (points[i] > points[j])
            {
                Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void MathUtils::sortDescending(std::vector<Point2f>& points)
{
    AUInt32 size = points.size();

    for (AUInt32 i = 0; i < size; i++)
    {
        for (AUInt32 j = 0; j < size; j++)
        {
            if (points[i] < points[j])
            {
                Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void MathUtils::sortDescending(std::vector<Point3f>& points)
{
    AUInt32 size = points.size();

    for (AUInt32 i = 0; i < size; i++)
    {
        for (AUInt32 j = 0; j < size; j++)
        {
            if (points[i] < points[j])
            {
                Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

Point2f MathUtils::getCentroid(const std::vector<Point2f>& points)
{
    AUInt32 size = points.size();
    Point2f centroid = Point2f(0, 0);
    if (size)
    {
        for (AUInt32 i = 0; i < size; i++)
        {
            centroid += points[i];
        }
    }
    return centroid / (AReal32)size;
}
//---------------------------------------------------------------------------------------

Point3f MathUtils::getCentroid(const std::vector<Point3f>& points)
{
    AUInt32 size = points.size();
    Point3f centroid = Point3f(0, 0, 0);
    if (size)
    {
        for (AUInt32 i = 0; i < size; i++)
        {
            centroid += points[i];
        }
    }
    return centroid / (AReal32)size;
}
//---------------------------------------------------------------------------------------


SCoordinateExtents MathUtils::getExtents(const std::vector<Point2f>& points)
{
    AUInt32 count = points.size();

    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;

    for (AUInt32 i = 0; i < count; i++)
    {
        const Point2f& pt = points[i];
        if (pt.x > maxx) maxx = pt.x;
        if (pt.y > maxy) maxy = pt.y;

        if (pt.x < minx) minx = pt.x;
        if (pt.y < miny) miny = pt.y;
    }

    return SCoordinateExtents(Point3f(minx, miny, 0), Point3f(maxx, maxy, 0));
}

//---------------------------------------------------------------------------------------

SCoordinateExtents MathUtils::getExtents(const std::vector<Point3f>& points)
{
    AUInt32 count = points.size();

    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;
    AReal32 maxz = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;
    AReal32 minz = MAX_REAL32;

    for (AUInt32 i = 0; i < count; i++)
    {
        const Point3f& pt = points[i];
        if (pt.x > maxx) maxx = pt.x;
        if (pt.y > maxy) maxy = pt.y;
        if (pt.z > maxz) maxz = pt.z;

        if (pt.x < minx) minx = pt.x;
        if (pt.y < miny) miny = pt.y;
        if (pt.z < minz) minz = pt.z;
    }

    return SCoordinateExtents(Point3f(minx, miny, minz), Point3f(maxx, maxy, maxz));
}
//---------------------------------------------------------------------------------------

AReal32 MathUtils::GetMax(AReal32 a, AReal32 b, AReal32 c)
{
    AReal32 max = a;
    if (b > max)	max = b;
    if (c > max) max = c;
    return max;
}
//---------------------------------------------------------------------------------------

AReal32 MathUtils::GetMin(AReal32 a, AReal32 b, AReal32 c)
{
    AReal32 min = a;
    if (b < min)	min = b;
    if (c < min) min = c;
    return min;
}
//---------------------------------------------------------------------------------------

std::tuple<Point3f, AReal32> MathUtils::NearestPoint(const Point3f& p, const std::vector<Point3f>& points)
{
    float minDist = FLT_MAX;
    Point3f nearestPoint(p);
    auto numPoints = points.size();
    for (int i = 0; i < numPoints; i++)
    {
        auto d = p.dist(points[i]);
        if (d > 0 && d < minDist)
        {
            minDist = d;
            nearestPoint = points[i];
        }
    }
    return std::tuple<Point3f, AReal32>(nearestPoint, minDist);
}
/*-----------------------------------------------------------------------------------*/

Sangfroid::Maths::Point3f MathUtils::Project(
    const Sangfroid::Maths::Point3f &point,
    const Sangfroid::Maths::Point3f &planePt,
    const Sangfroid::Maths::Vector3f &planeNorm)
{
    auto planeNormal = planeNorm.unitVec();
    return point - (planeNormal * ((point - planePt).dot(planeNormal)));
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::Point3f MathUtils::ProjectOnLine(
    const Sangfroid::Maths::Point3f &linePt,
    const Sangfroid::Maths::Vector3f &lineDir,
    const Sangfroid::Maths::Point3f &ptToBeProjected)
{
    Sangfroid::Maths::Vector3f ptToLinePt_dir = ptToBeProjected - linePt;
    auto projectionLength = ptToLinePt_dir.dot(lineDir);
    return linePt + (lineDir * projectionLength);
}
//---------------------------------------------------------------------------------------

double MathUtils::GetProjectedDistFromLine(const Sangfroid::Maths::Point3f &linePt, const Sangfroid::Maths::Vector3f &lineDir,
    const Sangfroid::Maths::Point3f &fromPoint)
{
    Sangfroid::Maths::Point3f projectedPtOnLine = ProjectOnLine(linePt, lineDir, fromPoint);
    return fromPoint.dist(projectedPtOnLine);
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::Point3f MathUtils::ProjectOnLine(
    const Sangfroid::Maths::Point3f &p1,
    const Sangfroid::Maths::Point3f &p2,
    const Sangfroid::Maths::Point3f &p)
{
    const double ZERO_TOLERANCE = 0.0000000001;

    Sangfroid::Maths::Vector3f v1 = p2 - p1;

    auto denominator = v1.squareLength();
    if (fabs(denominator) < ZERO_TOLERANCE)
    {
        return p1;
    }

    Sangfroid::Maths::Vector3f v2 = p - p1;

    auto u = (v1.dot(v2)) / denominator;

    auto x = p1.x + (u * (p2.x - p1.x));
    auto y = p1.y + (u * (p2.y - p1.y));
    auto z = p1.z + (u * (p2.z - p1.z));

    return Sangfroid::Maths::Point3f(x, y, z);
}
//---------------------------------------------------------------------------------------

double MathUtils::Distance_PointToPlane(
    const Point3f& point,
    const Point3f& planePoint,
    const Vector3f& planeNormal)
{
    auto normal = planeNormal.unitVec();
    return abs((point - planePoint).dot(normal));
}
//---------------------------------------------------------------------------------------

double MathUtils::DistanceSigned_PointToPlane(
    const Point3f& point,
    const Point3f& planePoint,
    const Vector3f& planeNormal)
{
    auto normal = planeNormal.unitVec();
    return (double)(point - planePoint).dot(normal);
}
//---------------------------------------------------------------------------------------

double MathUtils::Distance_PointToLine(
    const Sangfroid::Maths::Point3f& lineStart,
    const Sangfroid::Maths::Vector3f& dir,
    const Sangfroid::Maths::Point3f& point)
{
    auto direction = dir.unitVec();
    auto lineEnd = lineStart + (direction * 100.0);
    auto projection = MathUtils::ProjectOnLine(lineStart, lineEnd, point);

    return point.dist(projection);
}
//---------------------------------------------------------------------------------------

float MathUtils::Distance_PointToTriangle(
    const Sangfroid::Maths::Point3f& p,
    const Sangfroid::Maths::Point3f& t0,
    const Sangfroid::Maths::Point3f& t1,
    const Sangfroid::Maths::Point3f& t2,
    const Sangfroid::Maths::Vector3f& normal)
{
    auto n = normal.unitVec();
    auto projPt = Project(p, t0, n);

    //Check here if the proj. point is inside triangle
    if (!InTriangle(projPt, t0, t1, t2))
    {
        return p.dist(projPt);
    }

    auto d0 = LineSegment(t0, t1).distFromPoint(p);
    auto d1 = LineSegment(t1, t2).distFromPoint(p);
    auto d2 = LineSegment(t2, t0).distFromPoint(p);

    auto d = std::min(std::min(d0, d1), d2);

    return d;
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::Point3f MathUtils::Centroid(
    const Sangfroid::Maths::Point3f &p0,
    const Sangfroid::Maths::Point3f &p1,
    const Sangfroid::Maths::Point3f &p2)
{
    auto x = (p0.x + p1.x + p2.x) / 3.0f;
    auto y = (p0.y + p1.y + p2.y) / 3.0f;
    auto z = (p0.z + p1.z + p2.z) / 3.0f;

    return Point3f(x, y, z);
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::Point3f MathUtils::Centroid(
    const std::vector<Sangfroid::Maths::Point3f> &points)
{
    auto x = 0.0f;
    auto y = 0.0f;
    auto z = 0.0f;

    for (auto& pt : points)
    {
        x += pt.x;
        y += pt.y;
        z += pt.z;
    }

    float numPoints = (float)points.size();

    x = x / numPoints;
    y = y / numPoints;
    z = z / numPoints;

    return Point3f(x, y, z);
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::Point3f MathUtils::MidPoint(
    const Sangfroid::Maths::Point3f &p0,
    const Sangfroid::Maths::Point3f &p1)
{
    auto x = (p0.x + p1.x) / 2.0f;
    auto y = (p0.y + p1.y) / 2.0f;
    auto z = (p0.z + p1.z) / 2.0f;

    return Point3f(x, y, z);
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::Vector3f MathUtils::NormalFrom3Points(
    const Sangfroid::Maths::Point3f& a,
    const Sangfroid::Maths::Point3f& b,
    const Sangfroid::Maths::Point3f& c)
{
    auto normal = (b - a).cross(c - b);
    normal.normalize();
    return normal;
}
//---------------------------------------------------------------------------------------

static bool areSame(const float& a, const float& b)
{
    return ((a <= b + TOLERANCE) && (a >= b - TOLERANCE));
}
//---------------------------------------------------------------------------------------

static bool areSame(const Vector3f& v1, const Vector3f& v2)
{
    return (areSame(v1.i, v2.i) && areSame(v1.j, v2.j) && areSame(v1.k, v2.k));
}
//---------------------------------------------------------------------------------------

static bool IsZero(float val, float tol)
{
    return -tol <= val && val <= tol;
}
//---------------------------------------------------------------------------------------

static bool IsZeroVector(const Vector3f& vec, float tol)
{
    return IsZero(vec.length(), tol);
}
//---------------------------------------------------------------------------------------

static float AreaNormalized(
    const Sangfroid::Maths::Point3f&a,
    const Sangfroid::Maths::Point3f&b,
    const Sangfroid::Maths::Point3f&c,
    float tol)
{
    Vector3f vec1 = b - a;
    if (IsZeroVector(vec1, tol)) return 0.0f;

    Vector3f vec2 = c - b;
    if (IsZeroVector(vec2, tol)) return 0.0f;


    auto d = vec1.unitVec().cross(vec2.unitVec()).length() * 0.5f;
    return d;
}
//---------------------------------------------------------------------------------------

static bool IsCollinear(
    const Sangfroid::Maths::Point3f&a,
    const Sangfroid::Maths::Point3f&b,
    const Sangfroid::Maths::Point3f&c,
    float tol)
{
    return IsZero(AreaNormalized(a, b, c, tol), tol);
}
//---------------------------------------------------------------------------------------

static bool isOnLineSegment(const Point3f& startPoint,
    const Point3f& endPoint,
    const Point3f& intersectionPoint,
    const double tolerance)
{
    bool intersected = false;

    if (IsCollinear(startPoint, endPoint, intersectionPoint, (float)tolerance))
    {
        double minX, minY, minZ;
        double maxX, maxY, maxZ;

        minX = MIN(startPoint.x, endPoint.x);
        maxX = MAX(startPoint.x, endPoint.x);

        minY = MIN(startPoint.y, endPoint.y);
        maxY = MAX(startPoint.y, endPoint.y);

        minZ = MIN(startPoint.z, endPoint.z);
        maxZ = MAX(startPoint.z, endPoint.z);

        if ((intersectionPoint.x >= minX - tolerance) && (intersectionPoint.x <= maxX + tolerance) &&
            (intersectionPoint.y >= minY - tolerance) && (intersectionPoint.y <= maxY + tolerance) &&
            (intersectionPoint.z >= minZ - tolerance) && (intersectionPoint.z <= maxZ + tolerance))
        {
            intersected = true;
        }
    }

    return intersected;
}
//---------------------------------------------------------------------------------------

bool MathUtils::InTriangle(
    const Sangfroid::Maths::Point3f& p,
    const Sangfroid::Maths::Point3f& a,
    const Sangfroid::Maths::Point3f& b,
    const Sangfroid::Maths::Point3f& c)
{
    if ((a == b) && (b == c))
    {
        return p == a;
    }

    //Colinearity check
    auto vecA = a - c;
    auto vecB = a - b;

    vecA.normalize();
    vecB.normalize();
    auto nvecB = vecB.reverse();

    if (areSame(vecA, vecB) || areSame(vecA, nvecB) || (a == c) || (a == b))
    {
        if (isOnLineSegment(a, b, p, TOLERANCE))
        {
            return true;
        }
        else
        {
            if (isOnLineSegment(a, c, p, TOLERANCE))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    bool inside = true;

    auto normal = (b - a).cross(c - a);
    auto tempVec = (b - a).cross(p - a);

    if (tempVec.dot(normal) < 0)
    {
        inside = false;
    }
    else
    {
        tempVec = (c - b).cross(p - b);
        if (tempVec.dot(normal) < 0)
        {
            inside = false;
        }
        else
        {
            tempVec = (a - c).cross(p - c);
            if (tempVec.dot(normal) < 0)
            {
                inside = false;
            }
        }
    }
    return inside;
}
/*-----------------------------------------------------------------------------------*/

bool MathUtils::LineSegmentPlaneIntersection(
    const Sangfroid::Maths::Point3f& startPoint,
    const Sangfroid::Maths::Point3f& endPoint,
    const Sangfroid::Maths::Point3f& planePoint,
    const Sangfroid::Maths::Vector3f& planeNormal,
    Sangfroid::Maths::Point3f& intersection,
    bool & segmentLiesInPlane,
    const float proximityTolerance,
	const float parametricTolerance)
{
    segmentLiesInPlane = false;

    auto u = endPoint - startPoint;
    auto w = startPoint - planePoint;
    auto normal = planeNormal.unitVec();

    float denominator = normal.dot(u);
    float numerator = -normal.dot(w);

    //Check if line segment is parallel to the plane
    if (fabs(denominator) < proximityTolerance)
    {
        //Yes, it is parallel
        if (EQT(numerator, 0.0f, proximityTolerance))
        {
            //Not only parallel, but also...
            //the line segment is in plane, since both numerator and 
            //denominator are in zeros. Eventhough the intersection 
            //is the whole segment, the start point is returned as 
            //the intersection point.
            intersection = startPoint;
            segmentLiesInPlane = true;
            return true;
        }
        return false;
    }

    //Not parallel
    float t = numerator / denominator;
    if (t < (0.0f - parametricTolerance) || t >(1.0f + parametricTolerance))
    {
        //Infinite line corresponding to the segment intersects, 
        //but intersection point is not within the segment. 
        //i.e. Not between start and end points
        return false;
    }

    intersection = startPoint + (u * t);
    return true;
}
//---------------------------------------------------------------------------------------
