
//---------------------------------------------------------------------------------------

#include "MathsSettings.h"

using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------
//static member initialization

ECoordSystemHandedness CSettings::s_eCoordSysOrientation = ECoordSystemRightHanded;

EScreenConventionPosYAxis CSettings::s_e2DYAxisConvention = EScreenConventionPosYAxisUpNegative;

//---------------------------------------------------------------------------------------

ECoordSystemHandedness& CSettings::CoordSysOrientation()
{
    return s_eCoordSysOrientation;
}
//---------------------------------------------------------------------------------------

EScreenConventionPosYAxis& CSettings::YAxis2DConvention()
{
    return s_e2DYAxisConvention;
}
//---------------------------------------------------------------------------------------
