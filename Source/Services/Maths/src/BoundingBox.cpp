
//---------------------------------------------------------------------------------------
#include "BoundingBox.h"
#include "MathsIncludes.h"
#include "AxisOrientedBox.h"
#include "Point3f.h"
#include <vector>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

BoundingBox::BoundingBox()
    :AxisOrientedBox()
{
    m_bIsInitialized = false;
}
//---------------------------------------------------------------------------------------

BoundingBox::BoundingBox(const Point3f& origin, const AReal32& width, const AReal32& height,
    const AReal32& depth)
    :AxisOrientedBox(origin, width, height, depth)
{
    m_bIsInitialized = true;
}
//---------------------------------------------------------------------------------------

BoundingBox::BoundingBox(const Point3f& minPoint, const Point3f& maxPoint)
    :AxisOrientedBox(minPoint, maxPoint)
{
    m_bIsInitialized = true;
}
//---------------------------------------------------------------------------------------

BoundingBox::BoundingBox(const std::vector<Point3f>& points)
    :AxisOrientedBox()
{
    if (points.size() > 0)
    {
        m_bIsInitialized = true;
    }
    else
    {
        m_bIsInitialized = false;
    }

    AUInt32 count = (AUInt32)points.size();

    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;
    AReal32 maxz = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;
    AReal32 minz = MAX_REAL32;

    for (AUInt32 i = 0; i < count; i++)
    {
        const Point3f& pt = points[i];
        if (pt.x > maxx) maxx = pt.x;
        if (pt.y > maxy) maxy = pt.y;
        if (pt.z > maxz) maxz = pt.z;

        if (pt.x < minx) minx = pt.x;
        if (pt.y < miny) miny = pt.y;
        if (pt.z < minz) minz = pt.z;
    }

    m_minPoint = Point3f(minx, miny, minz);
    m_maxPoint = Point3f(maxx, maxy, maxz);
}
//---------------------------------------------------------------------------------------

BoundingBox::BoundingBox(const Point3f* points, const AUInt32 numPoints)
    :AxisOrientedBox()
{
    try
    {
        if (numPoints > 0 && points != NULL)
        {
            m_bIsInitialized = true;
        }
        else
        {
            m_bIsInitialized = false;
        }

        AReal32 maxx = -MAX_REAL32;
        AReal32 maxy = -MAX_REAL32;
        AReal32 maxz = -MAX_REAL32;

        AReal32 minx = MAX_REAL32;
        AReal32 miny = MAX_REAL32;
        AReal32 minz = MAX_REAL32;

        for (AUInt64 i = 0; i < numPoints; i++)
        {
            const Point3f& pt = points[i];
            if (pt.x > maxx) maxx = pt.x;
            if (pt.y > maxy) maxy = pt.y;
            if (pt.z > maxz) maxz = pt.z;

            if (pt.x < minx) minx = pt.x;
            if (pt.y < miny) miny = pt.y;
            if (pt.z < minz) minz = pt.z;
        }

        m_minPoint = Point3f(minx, miny, minz);
        m_maxPoint = Point3f(maxx, maxy, maxz);
    }
    catch (...)
    {
        throw "Unexpected error in constructor BoundingBox::BoundingBox(const Point3f* , const AUInt32 ): Probable reason - Bounds crossed.";
    }
}
//---------------------------------------------------------------------------------------

BoundingBox::BoundingBox(const Point3f& origin, const AReal32& size)
    :AxisOrientedBox(origin, size)
{
    m_bIsInitialized = true;
}
//---------------------------------------------------------------------------------------

BoundingBox::BoundingBox(const BoundingBox& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
    m_bIsInitialized = box.m_bIsInitialized;
}
//---------------------------------------------------------------------------------------

BoundingBox::~BoundingBox()
{
}
//---------------------------------------------------------------------------------------

void BoundingBox::Add(const std::vector<Point3f>& points)
{
    if (points.size() == 0)
    {
        return;
    }
    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;
    AReal32 maxz = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;
    AReal32 minz = MAX_REAL32;

    if (m_bIsInitialized)
    {
        maxx = m_maxPoint.x;
        maxy = m_maxPoint.y;
        maxz = m_maxPoint.z;

        minx = m_minPoint.x;
        miny = m_minPoint.y;
        minz = m_minPoint.z;
    }

    AUInt32 count = points.size();

    for (AUInt32 i = 0; i < count; i++)
    {
        const Point3f& pt = points[i];
        if (pt.x > maxx) maxx = pt.x;
        if (pt.y > maxy) maxy = pt.y;
        if (pt.z > maxz) maxz = pt.z;

        if (pt.x < minx) minx = pt.x;
        if (pt.y < miny) miny = pt.y;
        if (pt.z < minz) minz = pt.z;
    }

    m_minPoint = Point3f(minx, miny, minz);
    m_maxPoint = Point3f(maxx, maxy, maxz);
    m_bIsInitialized = true;
}
//---------------------------------------------------------------------------------------

void BoundingBox::Add(const Point3f* points, const AUInt32 numPoints)
{
    try
    {
        if (numPoints <= 0 || points == NULL)
        {
            return;
        }

        AReal32 maxx = -MAX_REAL32;
        AReal32 maxy = -MAX_REAL32;
        AReal32 maxz = -MAX_REAL32;

        AReal32 minx = MAX_REAL32;
        AReal32 miny = MAX_REAL32;
        AReal32 minz = MAX_REAL32;

        if (m_bIsInitialized)
        {
            maxx = m_maxPoint.x;
            maxy = m_maxPoint.y;
            maxz = m_maxPoint.z;

            minx = m_minPoint.x;
            miny = m_minPoint.y;
            minz = m_minPoint.z;
        }

        for (AUInt64 i = 0; i < numPoints; i++)
        {
            const Point3f& pt = points[i];
            if (pt.x > maxx) maxx = pt.x;
            if (pt.y > maxy) maxy = pt.y;
            if (pt.z > maxz) maxz = pt.z;

            if (pt.x < minx) minx = pt.x;
            if (pt.y < miny) miny = pt.y;
            if (pt.z < minz) minz = pt.z;
        }

        m_minPoint = Point3f(minx, miny, minz);
        m_maxPoint = Point3f(maxx, maxy, maxz);
        m_bIsInitialized = true;
    }
    catch (...)
    {
        throw "Unexpected error in BoundingBox::Add. Probable reason - Bounds crossed.";
    }
}
//---------------------------------------------------------------------------------------

void BoundingBox::Add(const Point3f& pt)
{
    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;
    AReal32 maxz = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;
    AReal32 minz = MAX_REAL32;

    if (m_bIsInitialized)
    {
        maxx = m_maxPoint.x;
        maxy = m_maxPoint.y;
        maxz = m_maxPoint.z;

        minx = m_minPoint.x;
        miny = m_minPoint.y;
        minz = m_minPoint.z;
    }

    if (pt.x > maxx) maxx = pt.x;
    if (pt.y > maxy) maxy = pt.y;
    if (pt.z > maxz) maxz = pt.z;

    if (pt.x < minx) minx = pt.x;
    if (pt.y < miny) miny = pt.y;
    if (pt.z < minz) minz = pt.z;

    m_minPoint = Point3f(minx, miny, minz);
    m_maxPoint = Point3f(maxx, maxy, maxz);
    m_bIsInitialized = true;
}
//---------------------------------------------------------------------------------------

BoundingBox& BoundingBox::operator =(const BoundingBox& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
    m_bIsInitialized = box.m_bIsInitialized;
    return *this;
}
//---------------------------------------------------------------------------------------

BoundingBox BoundingBox::operator +(const BoundingBox& box)
{
    if (!m_bIsInitialized && box.m_bIsInitialized)
    {
        return box;
    }
    else if (m_bIsInitialized && !box.m_bIsInitialized)
    {
        return *this;
    }
    AReal32 xmax = m_maxPoint.x > box.m_maxPoint.x ? m_maxPoint.x : box.m_maxPoint.x;
    AReal32 ymax = m_maxPoint.y > box.m_maxPoint.y ? m_maxPoint.y : box.m_maxPoint.y;
    AReal32 zmax = m_maxPoint.z > box.m_maxPoint.z ? m_maxPoint.z : box.m_maxPoint.z;

    AReal32 xmin = m_minPoint.x < box.m_minPoint.x ? m_minPoint.x : box.m_minPoint.x;
    AReal32 ymin = m_minPoint.y < box.m_minPoint.y ? m_minPoint.y : box.m_minPoint.y;
    AReal32 zmin = m_minPoint.z < box.m_minPoint.z ? m_minPoint.z : box.m_minPoint.z;

    return BoundingBox(Point3f(xmin, ymin, zmin), Point3f(xmax, ymax, zmax));
}
//---------------------------------------------------------------------------------------

BoundingBox& BoundingBox::operator +=(const BoundingBox& box)
{
    if (!m_bIsInitialized && box.m_bIsInitialized)
    {
        m_minPoint = box.m_minPoint;
        m_maxPoint = box.m_maxPoint;
        m_bIsInitialized = true;
    }
    else if (m_bIsInitialized && !box.m_bIsInitialized)
    {
    }
    else
    {
        AReal32 xmax = m_maxPoint.x > box.m_maxPoint.x ? m_maxPoint.x : box.m_maxPoint.x;
        AReal32 ymax = m_maxPoint.y > box.m_maxPoint.y ? m_maxPoint.y : box.m_maxPoint.y;
        AReal32 zmax = m_maxPoint.z > box.m_maxPoint.z ? m_maxPoint.z : box.m_maxPoint.z;

        AReal32 xmin = m_minPoint.x < box.m_minPoint.x ? m_minPoint.x : box.m_minPoint.x;
        AReal32 ymin = m_minPoint.y < box.m_minPoint.y ? m_minPoint.y : box.m_minPoint.y;
        AReal32 zmin = m_minPoint.z < box.m_minPoint.z ? m_minPoint.z : box.m_minPoint.z;

        m_minPoint = Point3f(xmin, ymin, zmin);
        m_maxPoint = Point3f(xmax, ymax, zmax);
        m_bIsInitialized = true;
    }
    return *this;
}
//---------------------------------------------------------------------------------------

bool BoundingBox::operator ==(const BoundingBox& box) const
{
    if ((m_minPoint == box.m_minPoint) && (m_maxPoint == box.m_maxPoint) && m_bIsInitialized == box.m_bIsInitialized)
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool BoundingBox::operator !=(const BoundingBox& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

bool BoundingBox::operator <(const BoundingBox& box) const
{
    if (m_minPoint < box.m_minPoint)      return true;
    if (m_minPoint > box.m_minPoint)      return false;
    if (m_maxPoint < box.m_maxPoint)      return true;
    if (m_maxPoint > box.m_maxPoint)      return false;
    if (m_bIsInitialized == false && box.m_bIsInitialized == true)   return true;
    if (m_bIsInitialized == true && box.m_bIsInitialized == false)   return false;
    return false;
}
//---------------------------------------------------------------------------------------
