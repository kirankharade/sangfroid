
//---------------------------------------------------------------------------------------

#include "LineSegment.h"
#include "MathsSettings.h"
#include "Plane.h"
#include "Line.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

LineSegment::LineSegment()
{
    m_firstPoint = Point3f();
    m_secondPoint = Point3f();
}
//---------------------------------------------------------------------------------------

LineSegment::LineSegment(const Point3f& point, const Vector3f& direction, const AReal32& length)
    : m_firstPoint(point)
{
    Vector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

LineSegment::LineSegment(const Point3f& firstPoint, const Point3f& secondPoint)
    : m_firstPoint(firstPoint)
    , m_secondPoint(secondPoint)
{
}
//---------------------------------------------------------------------------------------

LineSegment::LineSegment(const LineSegment& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
}
//---------------------------------------------------------------------------------------

LineSegment::~LineSegment()
{
}
//---------------------------------------------------------------------------------------

void LineSegment::set(const Point3f& firstPoint, const Vector3f& direction, const AReal32& length)
{
    m_firstPoint = firstPoint;
    Vector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

void LineSegment::set(const Point3f& firstPoint, const Point3f& secondPoint)
{
    m_firstPoint = firstPoint;
    m_secondPoint = secondPoint;
}
//---------------------------------------------------------------------------------------

Point3f LineSegment::getFirstPoint() const
{
    return m_firstPoint;
}
//---------------------------------------------------------------------------------------

Point3f LineSegment::getSecondPoint() const
{
    return m_secondPoint;
}
//---------------------------------------------------------------------------------------

Vector3f LineSegment::getDirection() const
{
    Vector3f vec = m_secondPoint - m_firstPoint;
    vec.normalize();
    return vec;
}
//---------------------------------------------------------------------------------------

AReal32 LineSegment::getLength() const
{
    return (m_secondPoint - m_firstPoint).length();
}
//---------------------------------------------------------------------------------------

bool LineSegment::isIntersecting(const Plane* pPlane) const
{
    Point3f intersectionPoint;
    return isIntersecting(pPlane, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool LineSegment::isIntersecting(const Plane* pPlane, Point3f& intersectionPoint) const
{
    Line line(m_firstPoint, m_secondPoint);

    if (false == line.isIntersecting(pPlane, intersectionPoint))
    {
        return false;
    }

    Vector3f v1 = m_secondPoint - m_firstPoint;
    Vector3f v2 = intersectionPoint - m_firstPoint;

    AReal32 u = v1.dot(v2) / v1.dot(v1);

    if ((u >= (-TOLERANCE_32)) && (u <= (1.0f + TOLERANCE_32)))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool LineSegment::passesThrough(const Point3f& point, const AReal32 tolerance) const
{
    Line line(m_firstPoint, m_secondPoint);

    if (line.passesThrough(point, tolerance))
    {
        AReal32 d1 = (m_firstPoint - point).length();
        AReal32 d2 = (m_secondPoint - point).length();
        AReal32 d = (m_secondPoint - m_firstPoint).length();
        if (EQ_32(d1 + d2, d))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

Point3f LineSegment::closestPoint(const Point3f& point) const
{
    Line line(m_firstPoint, m_secondPoint);

    Point3f closestPt = line.closestPoint(point);
    if (point.dist(closestPt) > point.dist(m_firstPoint))
    {
        closestPt = m_firstPoint;
    }
    if (point.dist(closestPt) > point.dist(m_secondPoint))
    {
        closestPt = m_secondPoint;
    }
    return closestPt;
}
//---------------------------------------------------------------------------------------

AReal32 LineSegment::distFromPoint(const Point3f& point) const
{
    Point3f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//Assignment operator

LineSegment& LineSegment::operator = (const LineSegment& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool LineSegment::operator == (const LineSegment& ls) const
{
    if ((m_firstPoint == ls.getFirstPoint()) && (m_secondPoint == ls.getSecondPoint()))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool LineSegment::operator != (const LineSegment& lineSegment) const
{
    if (!(*this == lineSegment))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool LineSegment::operator < (const LineSegment& lineSegment) const
{
    Point3f thatFirstPoint = lineSegment.getFirstPoint();
    Point3f thatSecondPoint = lineSegment.getSecondPoint();

    if (m_firstPoint < thatFirstPoint)
    {
        return true;
    }
    if (m_firstPoint > thatFirstPoint)
    {
        return false;
    }
    if (m_secondPoint < thatSecondPoint)
    {
        return true;
    }
    if (m_secondPoint > thatSecondPoint)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------
