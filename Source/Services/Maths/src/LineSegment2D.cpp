
//---------------------------------------------------------------------------------------

#include "LineSegment2D.h"
#include "LineSegment.h"
#include "MathUtils.h"
#include "MathsSettings.h"
#include "Plane.h"
#include "Line.h"
#include "LineSegment.h"
#include "Ray.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

LineSegment2D::LineSegment2D()
{
    m_firstPoint = Point2f();
    m_secondPoint = Point2f();
}
//---------------------------------------------------------------------------------------

LineSegment2D::LineSegment2D(const Point2f& point, const Vector3f& direction, const AReal32& length)
    : m_firstPoint(point)
{
    Vector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

LineSegment2D::LineSegment2D(const Point2f& firstPoint, const Point2f& secondPoint)
    : m_firstPoint(firstPoint)
    , m_secondPoint(secondPoint)
{
}
//---------------------------------------------------------------------------------------

LineSegment2D::LineSegment2D(const LineSegment2D& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
}
//---------------------------------------------------------------------------------------

LineSegment2D::LineSegment2D(const LineSegment& lineSegment)
{
    m_firstPoint = lineSegment.getFirstPoint();
    m_secondPoint = lineSegment.getSecondPoint();
}
//---------------------------------------------------------------------------------------

LineSegment2D::~LineSegment2D()
{
}
//---------------------------------------------------------------------------------------

void LineSegment2D::set(const Point2f& firstPoint, const Vector3f& direction, const AReal32& length)
{
    m_firstPoint = firstPoint;
    Vector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

void LineSegment2D::set(const Point2f& firstPoint, const Point2f& secondPoint)
{
    m_firstPoint = firstPoint;
    m_secondPoint = secondPoint;
}
//---------------------------------------------------------------------------------------

Point2f LineSegment2D::getFirstPoint() const
{
    return m_firstPoint;
}
//---------------------------------------------------------------------------------------

Point2f LineSegment2D::getSecondPoint() const
{
    return m_secondPoint;
}
//---------------------------------------------------------------------------------------

Vector3f LineSegment2D::getDirection() const
{
    Vector3f vec = m_secondPoint - m_firstPoint;
    vec.normalize();
    return vec;
}
//---------------------------------------------------------------------------------------

AReal32 LineSegment2D::getLength() const
{
    return (m_secondPoint - m_firstPoint).length();
}
//---------------------------------------------------------------------------------------

//Reference: http://paulbourke.net/geometry/lineline2d/

LineSegment2D::IntersectionClassification LineSegment2D::classifyByIntersection(const LineSegment2D* lineSegment, Point2f& intersectionPoint) const
{
    if (!lineSegment)
    {
        return LineSegment2D::undefinedIntersection;
    }
    Point2f p1 = m_firstPoint;
    Point2f p2 = m_secondPoint;
    Point2f p3 = lineSegment->getFirstPoint();
    Point2f p4 = lineSegment->getSecondPoint();

    AReal32 u1 = ((p4.x - p3.x) * (p1.y - p3.y)) - ((p4.y - p3.y) * (p1.x - p3.x));
    AReal32 u2 = ((p2.x - p1.x) * (p1.y - p3.y)) - ((p2.y - p1.y) * (p1.x - p3.x));
    AReal32 d = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y));

    if (EQ_32(d, 0) && EQ_32(u1, 0))
    {
        return LineSegment2D::CoIncident;
    }
    if (EQ_32(d, 0))
    {
        return LineSegment2D::NonCoIncidentParallel;
    }

    u1 /= d;
    u2 /= d;

    if (GTEQ_32(u1, 0) || LTEQ_32(u1, 1.0f))
    {
        return LineSegment2D::IntersectingAtEnds;
    }

    if ((u1 >= (AReal32) 0.0 && u1 <= (AReal32) 1.0) &&
        (u2 >= (AReal32) 0.0 && u2 <= (AReal32) 1.0))
    {
        intersectionPoint = m_firstPoint + ((m_secondPoint - m_firstPoint) * u1);
        return LineSegment2D::Intersecting;
    }
    return LineSegment2D::NonIntersecting;
}
//---------------------------------------------------------------------------------------

LineSegment2D::IntersectionClassification LineSegment2D::classifyByIntersection(const LineSegment2D* lineSegment) const
{
    Point2f intersectionPoint;
    return classifyByIntersection(lineSegment, intersectionPoint);
}
//---------------------------------------------------------------------------------------

LineSegment2D::IntersectionClassification LineSegment2D::classifyByIntersection(const Line* line, Point2f& intersectionPoint) const
{
    if (!line)
    {
        return LineSegment2D::undefinedIntersection;
    }
    Point2f p1 = m_firstPoint;
    Point2f p2 = m_secondPoint;
    Point2f p3 = line->getPoint();
    Point2f p4 = p3 + (line->getDirection() * 100.0);

    AReal32 u1 = ((p4.x - p3.x) * (p1.y - p3.y)) - ((p4.y - p3.y) * (p1.x - p3.x));
    AReal32 d = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y));

    if (EQ_32(d, 0) && EQ_32(u1, 0))
    {
        return LineSegment2D::CoIncident;
    }
    if (EQ_32(d, 0))
    {
        return LineSegment2D::NonCoIncidentParallel;
    }

    u1 /= d;

    if (GTEQ_32(u1, 0) || LTEQ_32(u1, 1.0f))
    {
        return LineSegment2D::IntersectingAtEnds;
    }

    if ((u1 >= (AReal32) 0.0 && u1 <= (AReal32) 1.0))
    {
        intersectionPoint = m_firstPoint + ((m_secondPoint - m_firstPoint) * u1);
        return LineSegment2D::Intersecting;
    }
    return LineSegment2D::NonIntersecting;
}
//---------------------------------------------------------------------------------------

LineSegment2D::IntersectionClassification LineSegment2D::classifyByIntersection(const Line* line) const
{
    Point2f intersectionPoint;
    return classifyByIntersection(line, intersectionPoint);
}
//---------------------------------------------------------------------------------------

LineSegment2D::IntersectionClassification LineSegment2D::classifyByIntersection(const Ray* ray, Point2f& intersectionPoint) const
{
    if (!ray)
    {
        return LineSegment2D::undefinedIntersection;
    }
    Point2f p3 = ray->getPoint();
    Point2f p4 = p3 + (ray->getDirection() * 100000.0); //Create a large segment extending in one direction
    LineSegment2D ls(p3, p4);
    return classifyByIntersection(&ls, intersectionPoint);
}
//---------------------------------------------------------------------------------------

LineSegment2D::IntersectionClassification LineSegment2D::classifyByIntersection(const Ray* ray) const
{
    Point2f intersectionPoint;
    return classifyByIntersection(ray, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool LineSegment2D::passesThrough(const Point2f& point, const AReal32 tolerance) const
{
    Point3f firstPoint(m_firstPoint.x, m_firstPoint.y, 0);
    Point3f secondPoint(m_secondPoint.x, m_secondPoint.y, 0);
    Point3f pt(point.x, point.y, 0);

    LineSegment ls(firstPoint, secondPoint);

    return ls.passesThrough(pt, tolerance);
}
//---------------------------------------------------------------------------------------

Point2f LineSegment2D::closestPoint(const Point2f& point) const
{
    Point3f firstPoint(m_firstPoint.x, m_firstPoint.y, 0);
    Point3f secondPoint(m_secondPoint.x, m_secondPoint.y, 0);
    Point3f passedPoint(point.x, point.y, 0);

    LineSegment ls(firstPoint, secondPoint);

    Point3f pt = ls.closestPoint(passedPoint);

    return Point2f(pt);
}
//---------------------------------------------------------------------------------------

AReal32 LineSegment2D::distFromPoint(const Point2f& point) const
{
    Point2f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//Assignment operator

LineSegment2D& LineSegment2D::operator = (const LineSegment2D& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool LineSegment2D::operator == (const LineSegment2D& ls) const
{
    if ((m_firstPoint == ls.getFirstPoint()) && (m_secondPoint == ls.getSecondPoint()))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool LineSegment2D::operator != (const LineSegment2D& lineSegment) const
{
    if (!(*this == lineSegment))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool LineSegment2D::operator < (const LineSegment2D& lineSegment) const
{
    Point2f thatFirstPoint = lineSegment.getFirstPoint();
    Point2f thatSecondPoint = lineSegment.getSecondPoint();

    if (m_firstPoint < thatFirstPoint)
    {
        return true;
    }
    if (m_firstPoint > thatFirstPoint)
    {
        return false;
    }
    if (m_secondPoint < thatSecondPoint)
    {
        return true;
    }
    if (m_secondPoint > thatSecondPoint)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------
