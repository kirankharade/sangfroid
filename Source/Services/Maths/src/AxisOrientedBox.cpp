
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "AxisOrientedBox.h"
#include "Box.h"
#include "Line.h"
#include "LineSegment.h"
#include "Plane.h"
#include "PointTriangle.h"
#include "IntersectionInfo.h"
#include <vector>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

AxisOrientedBox::AxisOrientedBox()
    : m_minPoint(ORIGIN)
    , m_maxPoint(ORIGIN)
{
}
//---------------------------------------------------------------------------------------

AxisOrientedBox::AxisOrientedBox(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
    : m_minPoint(origin)
    , m_maxPoint(origin)
{
    m_maxPoint += Vector3f(width, height, depth);
}
//---------------------------------------------------------------------------------------

AxisOrientedBox::AxisOrientedBox(const Point3f& minPoint, const Point3f& maxPoint)
    : m_minPoint(minPoint)
    , m_maxPoint(maxPoint)
{
    validate();
}
//---------------------------------------------------------------------------------------

AxisOrientedBox::AxisOrientedBox(const Point3f& origin, const AReal32& size)
    : m_minPoint(origin)
    , m_maxPoint(origin)
{
    m_maxPoint = m_minPoint + Vector3f(size, size, size);
}
//---------------------------------------------------------------------------------------

AxisOrientedBox::AxisOrientedBox(const AxisOrientedBox& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
}
//---------------------------------------------------------------------------------------

AxisOrientedBox::~AxisOrientedBox()
{
}
//---------------------------------------------------------------------------------------

void AxisOrientedBox::set(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
{
    m_minPoint = origin;
    m_maxPoint = m_minPoint + Vector3f(width, height, depth);
}
//---------------------------------------------------------------------------------------

void AxisOrientedBox::set(const Point3f& origin, const AReal32& size)
{
    m_minPoint = origin;
    m_maxPoint = m_minPoint + Vector3f(size, size, size);
}
//---------------------------------------------------------------------------------------

void AxisOrientedBox::setMinCorner(const Point3f& minPoint)
{
    m_minPoint = minPoint;
    validate();
}
//---------------------------------------------------------------------------------------

void AxisOrientedBox::setMaxCorner(const Point3f& maxPoint)
{
    m_maxPoint = maxPoint;
    validate();
}
//---------------------------------------------------------------------------------------

Point3f AxisOrientedBox::getMinCorner() const
{
    return m_minPoint;
}
//---------------------------------------------------------------------------------------

Point3f AxisOrientedBox::getMaxCorner() const
{
    return m_maxPoint;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedBox::IsValid() const
{
    return !((m_minPoint == ORIGIN) && (m_maxPoint == ORIGIN));
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedBox::getWidth() const
{
    return (m_maxPoint.x - m_minPoint.x);
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedBox::getHeight() const
{
    return (m_maxPoint.y - m_minPoint.y);
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedBox::getDepth() const
{
    return (m_maxPoint.z - m_minPoint.z);
}
//---------------------------------------------------------------------------------------

Point3f AxisOrientedBox::getCentre() const
{
    return ((m_minPoint + m_maxPoint) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedBox::getVolume() const
{
    return ((m_maxPoint.x - m_minPoint.x) *
        (m_maxPoint.y - m_minPoint.y) *
        (m_maxPoint.z - m_minPoint.z));
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedBox::getSurfaceArea() const
{
    AReal32 w = m_maxPoint.x - m_minPoint.x;
    AReal32 h = m_maxPoint.y - m_minPoint.y;
    AReal32 d = m_maxPoint.z - m_minPoint.z;
    return (2.0f * ((w*h) + (h*d) + (d*w)));
}
//---------------------------------------------------------------------------------------

Point3f* AxisOrientedBox::getCorners() const
{
    Point3f* pCorners = new Point3f[8];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 z1 = m_minPoint.z;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;
    AReal32 z2 = m_maxPoint.z;

    pCorners[0] = Point3f(x1, y1, z1);
    pCorners[1] = Point3f(x1, y2, z1);
    pCorners[2] = Point3f(x1, y2, z2);
    pCorners[3] = Point3f(x1, y1, z2);
    pCorners[4] = Point3f(x2, y1, z1);
    pCorners[5] = Point3f(x2, y2, z1);
    pCorners[6] = Point3f(x2, y2, z2);
    pCorners[7] = Point3f(x2, y1, z2);

    return pCorners;
}
//---------------------------------------------------------------------------------------

LineSegment* AxisOrientedBox::getEdges() const
{
    LineSegment* pEdges = new LineSegment[12];

    Point3f pts[8];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 z1 = m_minPoint.z;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;
    AReal32 z2 = m_maxPoint.z;

    pts[0] = Point3f(x1, y1, z1);
    pts[1] = Point3f(x1, y2, z1);
    pts[2] = Point3f(x1, y2, z2);
    pts[3] = Point3f(x1, y1, z2);
    pts[4] = Point3f(x2, y1, z1);
    pts[5] = Point3f(x2, y2, z1);
    pts[6] = Point3f(x2, y2, z2);
    pts[7] = Point3f(x2, y1, z2);

    pEdges[0] = LineSegment(pts[0], pts[1]);
    pEdges[1] = LineSegment(pts[1], pts[2]);
    pEdges[2] = LineSegment(pts[2], pts[3]);
    pEdges[3] = LineSegment(pts[3], pts[0]);

    pEdges[4] = LineSegment(pts[4], pts[5]);
    pEdges[5] = LineSegment(pts[5], pts[6]);
    pEdges[6] = LineSegment(pts[6], pts[7]);
    pEdges[7] = LineSegment(pts[7], pts[4]);

    pEdges[8] = LineSegment(pts[0], pts[4]);
    pEdges[9] = LineSegment(pts[1], pts[5]);
    pEdges[10] = LineSegment(pts[2], pts[6]);
    pEdges[11] = LineSegment(pts[3], pts[7]);

    return pEdges;
}
//---------------------------------------------------------------------------------------

PointTriangle* AxisOrientedBox::getFaceTriangles() const
{
    Point3f c[8];

    //XY-plane through origin
    c[0] = m_minPoint;
    c[1] = m_minPoint + (X_AXIS * getWidth());
    c[2] = m_minPoint + (X_AXIS * getWidth()) + (Y_AXIS * getHeight());
    c[3] = m_minPoint + (Y_AXIS * getHeight());

    //Plane parallel to XY-plane through origin at offset
    Vector3f offset = Z_AXIS * getDepth();

    c[4] = c[0] + offset;
    c[5] = c[1] + offset;
    c[6] = c[2] + offset;
    c[7] = c[3] + offset;

    PointTriangle* tris = new PointTriangle[12];

    //Face 1 - 0, 1, 2, 3
    tris[0] = PointTriangle(c[0], c[1], c[2]);
    tris[1] = PointTriangle(c[2], c[3], c[0]);

    //Face 2 - 4, 5, 6, 7
    tris[2] = PointTriangle(c[6], c[5], c[4]);
    tris[3] = PointTriangle(c[4], c[7], c[6]);

    //Face 3 - 0, 1, 5, 4
    tris[4] = PointTriangle(c[5], c[1], c[0]);
    tris[5] = PointTriangle(c[0], c[4], c[5]);

    //Face 4 - 1, 2, 6, 5
    tris[6] = PointTriangle(c[6], c[2], c[1]);
    tris[7] = PointTriangle(c[1], c[5], c[6]);

    //Face 5 - 2, 3, 7, 6
    tris[8] = PointTriangle(c[7], c[3], c[2]);
    tris[9] = PointTriangle(c[2], c[6], c[7]);

    //Face 6 - 3, 0, 4, 7
    tris[10] = PointTriangle(c[4], c[0], c[3]);
    tris[11] = PointTriangle(c[3], c[7], c[4]);

    return tris;
}
//---------------------------------------------------------------------------------------

//! The normals of these planes pointing outwards of the box

Plane* Sangfroid::Maths::AxisOrientedBox::getPlanes() const
{
    Plane* planes = new Plane[6];

    planes[0] = Plane(m_minPoint, -X_AXIS);
    planes[1] = Plane(m_minPoint, -Y_AXIS);
    planes[2] = Plane(m_minPoint, -Z_AXIS);
    planes[3] = Plane(m_maxPoint, X_AXIS);
    planes[4] = Plane(m_maxPoint, Y_AXIS);
    planes[5] = Plane(m_maxPoint, Z_AXIS);

    return planes;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedBox::isIntersecting(const Plane* pPlane) const
{
    if (!pPlane)
    {
        return false;
    }
    LineSegment* pEdges = getEdges();
    if (!pEdges)
    {
        return false;
    }
    bool bStatus = false;
    for (AIndex16 i = 0; i < 12; i++)
    {
        bStatus = pEdges->isIntersecting(pPlane);
        if (bStatus)
        {
            break;
        }
    }
    SAFE_ARRAY_DELETE(pEdges);
    return bStatus;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* AxisOrientedBox::getIntersections(const Plane* pPlane) const
{
    if (!pPlane)
    {
        return NULL;
    }
    LineSegment* pEdges = getEdges();
    if (!pEdges)
    {
        return NULL;
    }

    std::vector<SPointIntersection> hits;
    Point3f hitPoint;

    for (AIndex16 i = 0; i < 12; i++)
    {
        if (pEdges->isIntersecting(pPlane, hitPoint))
        {
            SPointIntersection spi;
            spi.m_hitPrimitiveIndex = i;
            spi.m_intersectionPoint = hitPoint;
            hits.push_back(spi);
        }
    }
    SAFE_ARRAY_DELETE(pEdges);

    AUInt32 intersectionCount = (AUInt32)hits.size();
    if (intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedBox::isIntersecting(const Line* pLine) const
{
    if (!pLine)
    {
        return false;
    }
    Plane facePlanes[6]; //6 planes for 6 faces of the box
    getFacePlanes((Plane*&)facePlanes);

    Point3f intersectionPoint;

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pLine->isIntersecting(&facePlanes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, TOLERANCE_32))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* AxisOrientedBox::getIntersections(const Line* pLine) const
{
    if (!pLine)
    {
        return NULL;
    }
    Plane facePlanes[6]; //6 planes for 6 faces of the box
    getFacePlanes((Plane*&)facePlanes);

    Point3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pLine->isIntersecting(&facePlanes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32)hits.size();
    if (intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedBox::isInside(const Point3f& point, const AReal32 tolerance) const
{
    AReal32 x = point.x;
    AReal32 y = point.y;
    AReal32 z = point.z;

    if (GTEQT(x, m_minPoint.x, tolerance) && LTEQT(x, m_maxPoint.x, tolerance) &&
        GTEQT(y, m_minPoint.y, tolerance) && LTEQT(y, m_maxPoint.y, tolerance) &&
        GTEQT(z, m_minPoint.z, tolerance) && LTEQT(z, m_maxPoint.z, tolerance))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

Box* AxisOrientedBox::transform(const Matrix4f& matrix) const
{
    AReal32 width = getWidth();
    AReal32 height = getHeight();
    AReal32 depth = getDepth();
    Point3f boxOrigin = matrix.transform(m_minPoint);
    Vector3f xAxis = matrix.rotate(X_AXIS);
    Vector3f yAxis = matrix.rotate(Y_AXIS);

    return new Box(boxOrigin, xAxis, yAxis, width, height, depth);
}
//---------------------------------------------------------------------------------------

//Assignment operator

AxisOrientedBox& AxisOrientedBox::operator = (const AxisOrientedBox& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool AxisOrientedBox::operator == (const AxisOrientedBox& box) const
{
    if ((m_minPoint == box.m_minPoint) && (m_maxPoint == box.m_maxPoint))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedBox::operator != (const AxisOrientedBox& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

//Addition Operators

AxisOrientedBox AxisOrientedBox::operator + (const AxisOrientedBox& box) const
{
    if (m_minPoint == ORIGIN && m_maxPoint == ORIGIN)
    {
        //This is uninitialized box

        Point3f min = box.m_minPoint;
        Point3f max = box.m_maxPoint;

        return AxisOrientedBox(min, max);
    }

    AReal32 minX = m_minPoint.x;
    AReal32 minY = m_minPoint.y;
    AReal32 minZ = m_minPoint.z;

    AReal32 maxX = m_maxPoint.x;
    AReal32 maxY = m_maxPoint.y;
    AReal32 maxZ = m_maxPoint.z;

    if (minX > box.m_minPoint.x)    minX = box.m_minPoint.x;
    if (minY > box.m_minPoint.y)    minY = box.m_minPoint.y;
    if (minZ > box.m_minPoint.z)    minZ = box.m_minPoint.z;

    if (maxX < box.m_maxPoint.x)    maxX = box.m_maxPoint.x;
    if (maxY < box.m_maxPoint.y)    maxY = box.m_maxPoint.y;
    if (maxZ < box.m_maxPoint.z)    maxZ = box.m_maxPoint.z;

    Point3f min = Point3f(minX, minY, minZ);
    Point3f max = Point3f(maxX, maxY, maxZ);

    return AxisOrientedBox(min, max);
}
//---------------------------------------------------------------------------------------

AxisOrientedBox& AxisOrientedBox::operator += (const AxisOrientedBox& box)
{
    if (m_minPoint == ORIGIN && m_maxPoint == ORIGIN)
    {
        //This is uninitialized box
        m_minPoint = box.m_minPoint;
        m_maxPoint = box.m_maxPoint;
    }
    else
    {
        AReal32 minX = m_minPoint.x;
        AReal32 minY = m_minPoint.y;
        AReal32 minZ = m_minPoint.z;

        AReal32 maxX = m_maxPoint.x;
        AReal32 maxY = m_maxPoint.y;
        AReal32 maxZ = m_maxPoint.z;

        if (minX > box.m_minPoint.x)     minX = box.m_minPoint.x;
        if (minY > box.m_minPoint.y)     minY = box.m_minPoint.y;
        if (minZ > box.m_minPoint.z)     minZ = box.m_minPoint.z;

        if (maxX < box.m_maxPoint.x)     maxX = box.m_maxPoint.x;
        if (maxY < box.m_maxPoint.y)     maxY = box.m_maxPoint.y;
        if (maxZ < box.m_maxPoint.z)     maxZ = box.m_maxPoint.z;

        m_minPoint = Point3f(minX, minY, minZ);
        m_maxPoint = Point3f(maxX, maxY, maxZ);
    }

    return *this;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedBox::operator < (const AxisOrientedBox& box) const
{
    if (m_minPoint < box.m_minPoint)      return true;
    if (m_minPoint > box.m_minPoint)      return false;
    if (m_maxPoint < box.m_maxPoint)      return true;
    if (m_maxPoint > box.m_maxPoint)      return false;

    return false;
}
//---------------------------------------------------------------------------------------

void AxisOrientedBox::validate()
{
    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 z1 = m_minPoint.z;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;
    AReal32 z2 = m_maxPoint.z;

    if (x1 > x2)
    {
        m_minPoint.x = x2;
        m_maxPoint.x = x1;
    }
    if (y1 > y2)
    {
        m_minPoint.y = y2;
        m_maxPoint.y = y1;
    }
    if (z1 > z2)
    {
        m_minPoint.z = z2;
        m_maxPoint.z = z1;
    }
}
//---------------------------------------------------------------------------------------

void AxisOrientedBox::getFacePlanes(Plane*& facePlanes) const
{
    facePlanes[0] = Plane(m_minPoint, -X_AXIS);
    facePlanes[1] = Plane(m_minPoint, -Y_AXIS);
    facePlanes[2] = Plane(m_minPoint, -Z_AXIS);
    facePlanes[3] = Plane(m_maxPoint, X_AXIS);
    facePlanes[4] = Plane(m_maxPoint, Y_AXIS);
    facePlanes[5] = Plane(m_maxPoint, Z_AXIS);
}
//---------------------------------------------------------------------------------------

AxisOrientedBox AxisOrientedBox::CalculateBox(const Maths::Point3f* points, const AInt32& pointCount)
{
    AReal32 minX = FLT_MAX;
    AReal32 minY = FLT_MAX;
    AReal32 minZ = FLT_MAX;

    AReal32 maxX = -FLT_MAX;
    AReal32 maxY = -FLT_MAX;
    AReal32 maxZ = -FLT_MAX;

    for (AInt32 i = 0; i < pointCount; i++)
    {
        if (minX > points[i].x)     minX = points[i].x;
        if (minY > points[i].y)     minY = points[i].y;
        if (minZ > points[i].z)     minZ = points[i].z;

        if (maxX < points[i].x)     maxX = points[i].x;
        if (maxY < points[i].y)     maxY = points[i].y;
        if (maxZ < points[i].z)     maxZ = points[i].z;
    }

    return AxisOrientedBox(Point3f(minX, minY, minZ), Point3f(maxX, maxY, maxZ));
}
//---------------------------------------------------------------------------------------

AxisOrientedBox AxisOrientedBox::CalculateBox(const Maths::CPoint3fList points)
{
    AInt32 pointCount = points.size();

    AReal32 minX = FLT_MAX;
    AReal32 minY = FLT_MAX;
    AReal32 minZ = FLT_MAX;

    AReal32 maxX = -FLT_MAX;
    AReal32 maxY = -FLT_MAX;
    AReal32 maxZ = -FLT_MAX;

    for (AInt32 i = 0; i < pointCount; i++)
    {
        if (minX > points[i].x)     minX = points[i].x;
        if (minY > points[i].y)     minY = points[i].y;
        if (minZ > points[i].z)     minZ = points[i].z;

        if (maxX < points[i].x)     maxX = points[i].x;
        if (maxY < points[i].y)     maxY = points[i].y;
        if (maxZ < points[i].z)     maxZ = points[i].z;
    }

    return AxisOrientedBox(Point3f(minX, minY, minZ), Point3f(maxX, maxY, maxZ));
}
//---------------------------------------------------------------------------------------

