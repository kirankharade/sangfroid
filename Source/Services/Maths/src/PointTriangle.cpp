
//---------------------------------------------------------------------------------------

#include "PointTriangle.h"
#include "MathsSettings.h"
#include "Plane.h"
#include "LineSegment.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Sangfroid::Maths::PointTriangle::PointTriangle()
{
    Vertices[0] = Point3f();
    Vertices[1] = Point3f();
    Vertices[2] = Point3f();
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::PointTriangle::PointTriangle(const Point3f v[3])
{
    Vertices[0] = v[0];
    Vertices[1] = v[1];
    Vertices[2] = v[2];
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::PointTriangle::PointTriangle(const Point3f& a, const Point3f& b, const Point3f& c)
{
    Vertices[0] = a;
    Vertices[1] = b;
    Vertices[2] = c;
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::PointTriangle::PointTriangle(const PointTriangle& triangle)
{
    Vertices[0] = triangle.Vertices[0];
    Vertices[1] = triangle.Vertices[1];
    Vertices[2] = triangle.Vertices[2];
}
//---------------------------------------------------------------------------------------

void Sangfroid::Maths::PointTriangle::set(const Point3f& a, const Point3f& b, const Point3f& c)
{
    Vertices[0] = a;
    Vertices[1] = b;
    Vertices[2] = c;
}
//---------------------------------------------------------------------------------------

void Sangfroid::Maths::PointTriangle::set(const PointTriangle& triangle)
{
    Vertices[0] = triangle.Vertices[0];
    Vertices[1] = triangle.Vertices[1];
    Vertices[2] = triangle.Vertices[2];
}
//---------------------------------------------------------------------------------------

Vector3f Sangfroid::Maths::PointTriangle::normal() const
{
    Vector3f v = ((Vertices[1] - Vertices[0]).cross(Vertices[2] - Vertices[0]));
    v.normalize();
    return v;
}
//---------------------------------------------------------------------------------------

AReal32 Sangfroid::Maths::PointTriangle::area() const
{
    Vector3f v = ((Vertices[1] - Vertices[0]).cross(Vertices[2] - Vertices[0]));
    return (AReal32)(0.5 * v.magnitude());
}
//---------------------------------------------------------------------------------------

Point3f Sangfroid::Maths::PointTriangle::circumCenter() const
{
    return (Vertices[0] + Vertices[1] + Vertices[2]) / 3.0;
}
//---------------------------------------------------------------------------------------

void Sangfroid::Maths::PointTriangle::reverseOrientation()
{
    Point3f pt = Vertices[0];
    Vertices[0] = Vertices[2];
    Vertices[2] = pt;
}
//---------------------------------------------------------------------------------------

bool Sangfroid::Maths::PointTriangle::isInside(const Point3f& point) const
{
    Vector3f n = normal();
    Vector3f vtmp = (Vertices[1] - Vertices[0]).cross(point - Vertices[0]);
    AReal32 d = vtmp.dot(n);

    if (d < 0)
    {
        return false;
    }
    else
    {
        vtmp = (Vertices[2] - Vertices[1]).cross(point - Vertices[1]);
        d = vtmp.dot(n);
        if (d < 0)
        {
            return false;
        }
        else
        {
            vtmp = (Vertices[0] - Vertices[2]).cross(point - Vertices[2]);
            if (d < 0)
            {
                return false;
            }
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool Sangfroid::Maths::PointTriangle::isIntersecting(const LineSegment* lineSegment) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool Sangfroid::Maths::PointTriangle::isIntersecting(const Line* line) const
{
    return false;
}
//---------------------------------------------------------------------------------------

LineSegment Sangfroid::Maths::PointTriangle::edge(const AIndex32& i) const
{
    if (i > 2 || i < 0)
    {
        throw "Invalid index: CTriangle::edge []";
    }
    if (i == 0)
    {
        return LineSegment(Vertices[0], Vertices[1]);
    }
    if (i == 1)
    {
        return LineSegment(Vertices[1], Vertices[2]);
    }
    return LineSegment(Vertices[2], Vertices[0]);
}
//---------------------------------------------------------------------------------------

Point3f& Sangfroid::Maths::PointTriangle::operator [](const AIndex16& i)
{
    if (i > 2 || i < 0)
    {
        throw "Invalid index: CTriangle::operator []";
    }
    return Vertices[i];
}
//---------------------------------------------------------------------------------------

const Point3f& Sangfroid::Maths::PointTriangle::operator [](const AIndex32& i) const
{
    if (i > 2 || i < 0)
    {
        throw "Invalid index: CTriangle::operator []";
    }
    return Vertices[i];
}
//---------------------------------------------------------------------------------------

PointTriangle& Sangfroid::Maths::PointTriangle::operator =(const PointTriangle& triangle)
{
    Vertices[0] = triangle.Vertices[0];
    Vertices[1] = triangle.Vertices[1];
    Vertices[2] = triangle.Vertices[2];

    return *this;
}
//---------------------------------------------------------------------------------------

bool Sangfroid::Maths::PointTriangle::operator ==(const PointTriangle& triangle) const
{
    if ((Vertices[0] != triangle.Vertices[0]) && (Vertices[0] != triangle.Vertices[1]) && (Vertices[0] != triangle.Vertices[2]))
    {
        return false;
    }
    if ((Vertices[1] != triangle.Vertices[0]) && (Vertices[1] != triangle.Vertices[1]) && (Vertices[1] != triangle.Vertices[2]))
    {
        return false;
    }
    if ((Vertices[2] != triangle.Vertices[0]) && (Vertices[2] != triangle.Vertices[1]) && (Vertices[2] != triangle.Vertices[2]))
    {
        return false;
    }

    Vector3f thisNormal = normal();
    Vector3f thatNormal = triangle.normal();
    AReal32 dot = thisNormal.dot(thatNormal);
    if (!EQ_32(dot, 1.0f))
    {
        return false;
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool Sangfroid::Maths::PointTriangle::operator !=(const PointTriangle& triangle) const
{
    return !(*this == triangle);
}
//---------------------------------------------------------------------------------------

