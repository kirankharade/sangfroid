
//---------------------------------------------------------------------------------------

#include "ConvexHull2D.h"
#include "MathUtils.h"
#include "MathsSettings.h"
#include "PointTriangle.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

ConvexHull2D::ConvexHull2D()
{
    m_hullPoints.clear();
    m_inputPoints.clear();
    m_centroid = Point2f();
    m_extents = SCoordinateExtents();
}
//---------------------------------------------------------------------------------------

ConvexHull2D::ConvexHull2D(const std::vector<Point2f>& points)
{
    m_hullPoints.clear();
    m_inputPoints = points;
    generate();
}
//---------------------------------------------------------------------------------------

ConvexHull2D::ConvexHull2D(const ConvexHull2D& hull)
{
    m_hullPoints = hull.getHullPoints();
    m_inputPoints = hull.getInputPoints();
    m_centroid = hull.m_centroid;
    m_extents.minPoint = hull.m_extents.minPoint;
    m_extents.maxPoint = hull.m_extents.maxPoint;
}
//---------------------------------------------------------------------------------------

ConvexHull2D::~ConvexHull2D()
{
}
//---------------------------------------------------------------------------------------

void ConvexHull2D::addPoint(const Point2f& point)
{
    m_inputPoints.push_back(point);
    generate();
}
//---------------------------------------------------------------------------------------

void ConvexHull2D::addPoints(const std::vector<Point2f>& points)
{
    for (AUInt32 i = 0; i < points.size(); i++)
    {
        m_inputPoints.push_back(points[i]);
    }
    generate();
}
//---------------------------------------------------------------------------------------

const std::vector<Point2f>& ConvexHull2D::getInputPoints() const
{
    return m_inputPoints;
}
//---------------------------------------------------------------------------------------

const std::vector<Point2f>& ConvexHull2D::getHullPoints() const
{
    return m_hullPoints;
}
//---------------------------------------------------------------------------------------

bool ConvexHull2D::isPointOnHull(const Point2f& point) const
{
    for (AUInt32 i = 0; i < m_hullPoints.size(); i++)
    {
        if (m_hullPoints[i] == point)
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

const Point2f& ConvexHull2D::getCentroid() const
{
    return m_centroid;
}
//---------------------------------------------------------------------------------------

const SCoordinateExtents& ConvexHull2D::getExtents() const
{
    return m_extents;
}
//---------------------------------------------------------------------------------------

ConvexHull2D& ConvexHull2D::operator =(const ConvexHull2D& hull)
{
    m_hullPoints = hull.getHullPoints();
    m_inputPoints = hull.getInputPoints();
    return *this;
}
//---------------------------------------------------------------------------------------

//Reference:
//Graham Scan by Roland L. Graham (see docs/convex_hull_2D_RLGraham.pdf)
//Additional resource: http://marknelson.us/2007/08/22/convex/

void ConvexHull2D::generate()
{
    if (m_inputPoints.size() < 3)
    {
        return;
    }

    std::vector<Point2f> inputPoints;
    inputPoints = m_inputPoints;

    //Sort points in ascending x-coordinate direction
    MathUtils::sortAscending(inputPoints);

    AUInt32 count = inputPoints.size();
    Point2f frontPoint = inputPoints.front();
    Point2f backPoint = inputPoints.back();

    //Graham scan divides the input points into upper and lower hulls by drawing a line
    //joining first and last points of the sorted array.

    std::vector<Point2f> lower;
    std::vector<Point2f> upper;

    lower.push_back(frontPoint);

    //Classify the rest of the points by adding them to upper and lower point lists

    for (AUInt32 i = 1; i < (count - 1); i++)
    {
        Point2f point = inputPoints[i];
        AReal32 angleDeg = MathUtils::getCornerAngleDeg(frontPoint, point, backPoint);
        if (angleDeg < 180.0f)
        {
            lower.push_back(point);
        }
        else
        {
            upper.insert(upper.begin(), point);
        }
    }
    upper.insert(upper.begin(), backPoint);
    upper.push_back(frontPoint);
    lower.push_back(backPoint);

    generateHalfHull(lower);
    generateHalfHull(upper);

    //Merge 2 parts of the hull
    //Discard first and last point of the upper hull
    lower.insert(lower.end(), upper.begin() + 1, upper.end() - 1);

    m_hullPoints.clear();
    m_hullPoints = lower;

    m_centroid = MathUtils::getCentroid(m_hullPoints);
    m_extents = MathUtils::getExtents(m_hullPoints);
}
//---------------------------------------------------------------------------------------

void ConvexHull2D::generateHalfHull(std::vector<Point2f>& points)
{
    //First copy the input list
    std::vector<Point2f> outPoints;
    AUInt32 count = points.size();

    for (AUInt32 i = 0; i < count; i++)
    {
        Point2f point = points[i];
        outPoints.push_back(point);

        while (outPoints.size() > 2)
        {
            AUInt32 lastIndex = outPoints.size() - 1;

            //Check for duplicate points
            if (outPoints[lastIndex - 1] == outPoints[lastIndex])
            {
                outPoints.pop_back();
                break;
            }

            AReal32 angleDeg = MathUtils::getCornerAngleDeg(outPoints[lastIndex], outPoints[lastIndex - 1], outPoints[lastIndex - 2]);
            if (angleDeg > 180.0f)
            {
                //Remove the previous point as it is not on the hull...
                AUInt32 indexToBeRemoved = lastIndex - 1;
                outPoints.erase(outPoints.begin() + indexToBeRemoved);
            }
            else
            {
                break;
            }
        }
    }
    points.clear();
    points = outPoints;
}
//---------------------------------------------------------------------------------------




