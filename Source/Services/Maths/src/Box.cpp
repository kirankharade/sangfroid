
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "Box.h"
#include "Line.h"
#include "Ray.h"
#include "LineSegment.h"
#include "Plane.h"
#include "PointTriangle.h"
#include "IntersectionInfo.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------
const static AReal32 DEFAULT_SIZE = 10.0f;
//---------------------------------------------------------------------------------------

Box::Box()
    : m_origin(ORIGIN)
    , m_xAxis(X_AXIS)
    , m_yAxis(Y_AXIS)
    , m_width(DEFAULT_SIZE)
    , m_height(DEFAULT_SIZE)
    , m_depth(DEFAULT_SIZE)
{
    m_zAxis = Vector3f(0, 0, 1);
}
//---------------------------------------------------------------------------------------

Box::Box(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection,
    const AReal32& width, const AReal32& height, const AReal32& depth)
    : m_origin(origin)
    , m_xAxis(xDirection)
    , m_yAxis(yDirection)
    , m_width(width)
    , m_height(height)
    , m_depth(depth)
{
    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

Box::Box(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
    : m_origin(origin)
    , m_xAxis(X_AXIS)
    , m_yAxis(Y_AXIS)
    , m_width(width)
    , m_height(height)
    , m_depth(depth)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

Box::Box(const AReal32& width, const AReal32& height, const AReal32& depth)
    : m_origin(ORIGIN)
    , m_xAxis(X_AXIS)
    , m_yAxis(Y_AXIS)
    , m_width(width)
    , m_height(height)
    , m_depth(depth)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

Box::Box(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection, const AReal32& size)
    : m_origin(origin)
    , m_xAxis(xDirection)
    , m_yAxis(yDirection)
    , m_width(size)
    , m_height(size)
    , m_depth(size)
{
    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

Box::Box(const Point3f& origin, const AReal32& size)
    : m_origin(origin)
    , m_xAxis(X_AXIS)
    , m_yAxis(Y_AXIS)
    , m_width(size)
    , m_height(size)
    , m_depth(size)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

Box::Box(const AReal32& size)
    : m_origin(ORIGIN)
    , m_xAxis(X_AXIS)
    , m_yAxis(Y_AXIS)
    , m_width(size)
    , m_height(size)
    , m_depth(size)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

Box::Box(const Box& box)
{
    m_origin = box.m_origin;
    m_xAxis = box.m_xAxis;
    m_yAxis = box.m_yAxis;
    m_width = box.m_width;
    m_height = box.m_height;
    m_depth = box.m_depth;
    m_zAxis = box.m_zAxis;
}
//---------------------------------------------------------------------------------------

Box::~Box()
{
}
//---------------------------------------------------------------------------------------

void Box::set(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection,
    const AReal32& width, const AReal32& height, const AReal32& depth)
{
    m_origin = origin;
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_width = width;
    m_height = height;
    m_depth = depth;

    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void Box::set(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection, const AReal32& size)
{
    m_origin = origin;
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_width = size;
    m_height = size;
    m_depth = size;

    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void Box::setOrigin(const Point3f& origin)
{
    m_origin = origin;
}
//---------------------------------------------------------------------------------------

void Box::setDirections(const Vector3f& xDirection, const Vector3f& yDirection)
{
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void Box::setXDirection(const Vector3f& xDirection)
{
    m_xAxis = xDirection;
    m_xAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void Box::setYDirection(const Vector3f& yDirection)
{
    m_yAxis = yDirection;
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void Box::setSize(const AReal32& size)
{
    m_width = size;
    m_height = size;
    m_depth = size;
}
//---------------------------------------------------------------------------------------

void Box::setWidth(const AReal32& width)
{
    m_width = width;
}
//---------------------------------------------------------------------------------------

void Box::setHeight(const AReal32& height)
{
    m_height = height;
}
//---------------------------------------------------------------------------------------

void Box::setDepth(const AReal32& depth)
{
    m_depth = depth;
}
//---------------------------------------------------------------------------------------

Point3f Box::getOrigin() const
{
    return m_origin;
}
//---------------------------------------------------------------------------------------

Vector3f Box::getXDirection() const
{
    return m_xAxis;
}
//---------------------------------------------------------------------------------------

Vector3f Box::getYDirection() const
{
    return m_yAxis;
}
//---------------------------------------------------------------------------------------

AReal32 Box::getWidth() const
{
    return m_width;
}
//---------------------------------------------------------------------------------------

AReal32 Box::getHeight() const
{
    return m_height;
}
//---------------------------------------------------------------------------------------

AReal32 Box::getDepth() const
{
    return m_depth;
}
//---------------------------------------------------------------------------------------

Point3f Box::getCentre() const
{
    Point3f oppCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    return ((m_origin + oppCorner) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 Box::getVolume() const
{
    return (m_width * m_height * m_depth);
}
//---------------------------------------------------------------------------------------

AReal32 Box::getSurfaceArea() const
{
    return (2.0f * ((m_width * m_height) + (m_height * m_depth) + (m_width * m_depth)));
}
//---------------------------------------------------------------------------------------

Point3f* Box::getCorners() const
{
    Point3f* corners = new Point3f[8];

    //XY-plane through origin
    corners[0] = m_origin;
    corners[1] = m_origin + (m_xAxis * m_width);
    corners[2] = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height);
    corners[3] = m_origin + (m_yAxis * m_height);

    //Plane parallel to XY-plane through origin at offset
    Vector3f offset = m_zAxis * m_depth;

    corners[4] = corners[0] + offset;
    corners[5] = corners[1] + offset;
    corners[6] = corners[2] + offset;
    corners[7] = corners[3] + offset;

    return corners;
}
//---------------------------------------------------------------------------------------

LineSegment* Box::getEdges() const
{
    Point3f c[8];

    //XY-plane through origin
    c[0] = m_origin;
    c[1] = m_origin + (m_xAxis * m_width);
    c[2] = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height);
    c[3] = m_origin + (m_yAxis * m_height);

    //Plane parallel to XY-plane through origin at offset
    Vector3f offset = m_zAxis * m_depth;

    c[4] = c[0] + offset;
    c[5] = c[1] + offset;
    c[6] = c[2] + offset;
    c[7] = c[3] + offset;

    LineSegment* edges = new LineSegment[12];

    edges[0] = LineSegment(c[0], c[1]);
    edges[1] = LineSegment(c[1], c[2]);
    edges[2] = LineSegment(c[2], c[3]);
    edges[3] = LineSegment(c[3], c[0]);

    edges[4] = LineSegment(c[4], c[5]);
    edges[5] = LineSegment(c[5], c[6]);
    edges[6] = LineSegment(c[6], c[7]);
    edges[7] = LineSegment(c[7], c[4]);

    edges[8] = LineSegment(c[0], c[4]);
    edges[9] = LineSegment(c[1], c[5]);
    edges[10] = LineSegment(c[2], c[6]);
    edges[11] = LineSegment(c[3], c[7]);

    return edges;
}
//---------------------------------------------------------------------------------------

PointTriangle* Box::getFaceTriangles() const
{
    Point3f c[8];

    //XY-plane through origin
    c[0] = m_origin;
    c[1] = m_origin + (m_xAxis * m_width);
    c[2] = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height);
    c[3] = m_origin + (m_yAxis * m_height);

    //Plane parallel to XY-plane through origin at offset
    Vector3f offset = m_zAxis * m_depth;

    c[4] = c[0] + offset;
    c[5] = c[1] + offset;
    c[6] = c[2] + offset;
    c[7] = c[3] + offset;

    PointTriangle* tris = new PointTriangle[12];

    //Face 1 - 0, 1, 2, 3
    tris[0] = PointTriangle(c[0], c[1], c[2]);
    tris[1] = PointTriangle(c[2], c[3], c[0]);

    //Face 2 - 4, 5, 6, 7
    tris[2] = PointTriangle(c[6], c[5], c[4]);
    tris[3] = PointTriangle(c[4], c[7], c[6]);

    //Face 3 - 0, 1, 5, 4
    tris[4] = PointTriangle(c[5], c[1], c[0]);
    tris[5] = PointTriangle(c[0], c[4], c[5]);

    //Face 4 - 1, 2, 6, 5
    tris[6] = PointTriangle(c[6], c[2], c[1]);
    tris[7] = PointTriangle(c[1], c[5], c[6]);

    //Face 5 - 2, 3, 7, 6
    tris[8] = PointTriangle(c[7], c[3], c[2]);
    tris[9] = PointTriangle(c[2], c[6], c[7]);

    //Face 6 - 3, 0, 4, 7
    tris[10] = PointTriangle(c[4], c[0], c[3]);
    tris[11] = PointTriangle(c[3], c[7], c[4]);

    return tris;
}
//---------------------------------------------------------------------------------------

//! The normals of these planes pointing outwards of the box

Plane* Sangfroid::Maths::Box::getPlanes() const
{
    Plane* planes = new Plane[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);

    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    return planes;
}
//---------------------------------------------------------------------------------------

bool Box::isIntersecting(const Plane* pPlane) const
{
    if (!pPlane)
    {
        return false;
    }
    LineSegment* pEdges = getEdges();
    if (!pEdges)
    {
        return false;
    }
    bool bStatus = false;
    for (AIndex16 i = 0; i < 12; i++)
    {
        bStatus = pEdges->isIntersecting(pPlane);
        if (bStatus)
        {
            break;
        }
    }
    SAFE_ARRAY_DELETE(pEdges);
    return bStatus;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* Box::getIntersections(const Plane* pPlane) const
{
    if (!pPlane)
    {
        return NULL;
    }
    LineSegment* pEdges = getEdges();
    if (!pEdges)
    {
        return NULL;
    }

    std::vector<SPointIntersection> hits;
    Point3f hitPoint;

    for (AIndex16 i = 0; i < 12; i++)
    {
        if (pEdges->isIntersecting(pPlane, hitPoint))
        {
            SPointIntersection spi;
            spi.m_hitPrimitiveIndex = i;
            spi.m_intersectionPoint = hitPoint;
            hits.push_back(spi);
        }
    }
    SAFE_ARRAY_DELETE(pEdges);

    AUInt32 intersectionCount = (AUInt32)hits.size();
    if (intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool Box::isIntersecting(const Line* pLine) const
{
    if (!pLine)
    {
        return false;
    }

    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    Point3f intersectionPoint;
    AReal32 tolernace = (AReal32)(5.0 * TOLERANCE_32);

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pLine->isIntersecting(&planes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, tolernace))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* Box::getIntersections(const Line* pLine) const
{
    if (!pLine)
    {
        return NULL;
    }

    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    Point3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pLine->isIntersecting(&planes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32)hits.size();
    if (intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool Box::isIntersecting(const LineSegment* pLineSegment) const
{
    if (!pLineSegment)
    {
        return false;
    }

    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    Point3f intersectionPoint;
    AReal32 tolernace = (AReal32)(5.0 * TOLERANCE_32);

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pLineSegment->isIntersecting(&planes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, tolernace))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* Box::getIntersections(const LineSegment* pLineSegment) const
{
    if (!pLineSegment)
    {
        return NULL;
    }

    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    Point3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pLineSegment->isIntersecting(&planes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32)hits.size();
    if (intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool Box::isIntersecting(const Ray* pRay) const
{
    if (!pRay)
    {
        return false;
    }

    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    Point3f intersectionPoint;
    AReal32 tolernace = (AReal32)(5.0 * TOLERANCE_32);

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pRay->isIntersecting(&planes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, tolernace))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* Box::getIntersections(const Ray* pRay) const
{
    if (!pRay)
    {
        return NULL;
    }

    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    Point3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for (AInt16 i = 0; i < 6; i++)
    {
        if (pRay->isIntersecting(&planes[i], intersectionPoint))
        {
            if (isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32)hits.size();
    if (intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool Box::isInside(const Point3f& point, const AReal32& tolerance) const
{
    Plane planes[6];
    Point3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);

    planes[0] = Plane(m_origin, -m_xAxis);
    planes[1] = Plane(m_origin, -m_yAxis);
    planes[2] = Plane(m_origin, -m_zAxis);
    planes[3] = Plane(opCorner, m_xAxis);
    planes[4] = Plane(opCorner, m_yAxis);
    planes[5] = Plane(opCorner, m_zAxis);

    for (int i = 0; i < 6; i++)
    {
        if (EPlaneSideBack != planes[i].getQualification(point, tolerance))
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

Box* Box::transform(const Matrix4f& matrix) const
{
    Point3f boxOrigin = matrix.transform(m_origin);
    Vector3f xAxis = matrix.rotate(m_xAxis);
    Vector3f yAxis = matrix.rotate(m_yAxis);
    return new Box(boxOrigin, xAxis, yAxis, m_width, m_height, m_depth);
}
//---------------------------------------------------------------------------------------

//Assignment operator

Box& Box::operator = (const Box& box)
{
    m_origin = box.m_origin;
    m_xAxis = box.m_xAxis;
    m_yAxis = box.m_yAxis;
    m_width = box.m_width;
    m_height = box.m_height;
    m_depth = box.m_depth;

    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool Box::operator == (const Box& box) const
{
    if ((m_origin == box.m_origin) &&
        (m_xAxis == box.m_xAxis) &&
        (m_yAxis == box.m_yAxis) &&
        EQ_32(m_width, box.m_width) &&
        EQ_32(m_height, box.m_height) &&
        EQ_32(m_depth, box.m_depth))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Box::operator != (const Box& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

