
//---------------------------------------------------------------------------------------

#include "Converter.h"

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Point2f Converter::convertTo2f(const Point2i& point)
{
    return Point2f((AReal32)point.x, (AReal32)point.y);
}
//---------------------------------------------------------------------------------------

Point2i Converter::convertTo2i(const Point2f& point)
{
    return Point2i((AInt32)point.x, (AInt32)point.y);
}
//---------------------------------------------------------------------------------------

Point2f Converter::convertTo2f(const Point3f& point)
{
    return Point2f((AReal32)point.x, (AReal32)point.y);
}
//---------------------------------------------------------------------------------------

Point2i Converter::convertTo2i(const Point3f& point)
{
    return Point2i((AInt32)point.x, (AInt32)point.y);
}
//---------------------------------------------------------------------------------------

Point3f Converter::convertTo3f(const Point2f& point)
{
    return Point3f((AReal32)point.x, (AReal32)point.y, 0);
}
//---------------------------------------------------------------------------------------

Point3f Converter::convertTo3f(const Point2i& point)
{
    return Point3f((AReal32)point.x, (AReal32)point.y, 0);
}
//---------------------------------------------------------------------------------------

std::vector<Point2f> Converter::convertTo2f(const std::vector<Point2i>& points)
{
    std::vector<Point2f> out;
    AUInt32 count = points.size();

    if (count > 0)
    {
        out.resize(count);
        for (AUInt32 i = 0; i < count; i++)
        {
            out[i] = Point2f((AReal32)points[i].x, (AReal32)points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<Point2f> Converter::convertTo2f(const std::vector<Point3f>& points)
{
    std::vector<Point2f> out;
    AUInt32 count = points.size();

    if (count > 0)
    {
        out.resize(count);
        for (AUInt32 i = 0; i < count; i++)
        {
            out[i] = Point2f((AReal32)points[i].x, (AReal32)points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<Point2i> Converter::convertTo2i(const std::vector<Point2f>& points)
{
    std::vector<Point2i> out;
    AUInt32 count = points.size();

    if (count > 0)
    {
        out.resize(count);
        for (AUInt32 i = 0; i < count; i++)
        {
            out[i] = Point2i((AInt32)points[i].x, (AInt32)points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<Point2i> Converter::convertTo2i(const std::vector<Point3f>& points)
{
    std::vector<Point2i> out;
    AUInt32 count = points.size();

    if (count > 0)
    {
        out.resize(count);
        for (AUInt32 i = 0; i < count; i++)
        {
            out[i] = Point2i((AInt32)points[i].x, (AInt32)points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<Point3f> Converter::convertTo3f(const std::vector<Point2f>& points)
{
    std::vector<Point3f> out;
    AUInt32 count = points.size();

    if (count > 0)
    {
        out.resize(count);
        for (AUInt32 i = 0; i < count; i++)
        {
            out[i] = Point3f((AReal32)points[i].x, (AReal32)points[i].y, 0);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<Point3f> Converter::convertTo3f(const std::vector<Point2i>& points)
{
    std::vector<Point3f> out;
    AUInt32 count = points.size();

    if (count > 0)
    {
        out.resize(count);
        for (AUInt32 i = 0; i < count; i++)
        {
            out[i] = Point3f((AReal32)points[i].x, (AReal32)points[i].y, 0);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------



