
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "Vector3f.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Vector3f::Vector3f()
{
    x = 0.0f;
    y = 0.0f;
    z = 1.0f;
}
//---------------------------------------------------------------------------------------

Vector3f::Vector3f(const AReal32 v[3])
{
    x = v[0];
    y = v[1];
    z = v[2];
}
//---------------------------------------------------------------------------------------

Vector3f::Vector3f(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

Vector3f::Vector3f(const Vector3f& v)
{
    x = v.x;
    y = v.y;
    z = v.z;
}
//---------------------------------------------------------------------------------------

void Vector3f::set(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

void Vector3f::set(const Vector3f& v)
{
    x = v.x;
    y = v.y;
    z = v.z;
}
//---------------------------------------------------------------------------------------

void Vector3f::normalize()
{
    AReal32 mag = sqrt((x*x) + (y*y) + (z*z));
    if (!EQ_32(mag, 0.0f))
    {
        x /= mag;
        y /= mag;
        z /= mag;
    }
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::unitVec() const
{
    Vector3f v;
    AReal32 mag = sqrt((x*x) + (y*y) + (z*z));
    if (!EQ_32(mag, 0.0f))
    {
        v.x = x / mag;
        v.y = y / mag;
        v.z = z / mag;
    }
    return v;
}
//---------------------------------------------------------------------------------------

AReal32 Vector3f::magnitude() const
{
    return sqrt((x*x) + (y*y) + (z*z));
}
//---------------------------------------------------------------------------------------

AReal32 Vector3f::length() const
{
    return sqrt((x*x) + (y*y) + (z*z));
}
//---------------------------------------------------------------------------------------

AReal32 Vector3f::squareLength() const
{
    return ((x*x) + (y*y) + (z*z));
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::cross(const Vector3f& v) const
{
    //   x    y    z
    // v.x  v.y  v.z
    Vector3f c;
    c.x = (y * v.z) - (z * v.y);
    c.y = (z * v.x) - (x * v.z);
    c.z = (x * v.y) - (y * v.x);
    return c;
}
//---------------------------------------------------------------------------------------

AReal32 Vector3f::dot(const Vector3f& v) const
{
    return (x * v.x) + (y * v.y) + (z * v.z);
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::reverse() const
{
    return Vector3f(-x, -y, -z);
}
//---------------------------------------------------------------------------------------

bool Vector3f::isNull() const
{
    if (EQ_32(x, 0.0f) && EQ_32(y, 0.0f) && EQ_32(z, 0.0f))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

//operators

AReal32& Vector3f::operator [] (const AIndex32& index)
{
    if (0 == index) return x;
    if (1 == index) return y;
    if (2 == index) return z;
    throw "Vector3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AReal32& Vector3f::operator [] (const AIndex32& index) const
{
    if (0 == index) return x;
    if (1 == index) return y;
    if (2 == index) return z;
    throw "Vector3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

Vector3f& Vector3f::operator = (const Vector3f& v)
{
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
}
//---------------------------------------------------------------------------------------

bool Vector3f::operator == (const Vector3f& v) const
{
    if (EQ_32(x, v.x) && EQ_32(y, v.y) && EQ_32(z, v.z))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Vector3f::operator != (const Vector3f& v) const
{
    if ((!EQ_32(x, v.x)) || (!EQ_32(y, v.y)) || (!EQ_32(z, v.z)))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Vector3f::operator < (const Vector3f& v) const
{
    if (EQ_32(x, v.x) && EQ_32(y, v.y) && EQ_32(z, v.z))
    {
        return false;
    }
    if (x < v.x)      return true;
    else if (x > v.x) return false;
    else if (y < v.y) return true;
    else if (y > v.y) return false;
    else return (z < v.z);
}
//---------------------------------------------------------------------------------------

bool Vector3f::operator > (const Vector3f& v) const
{
    if ((!(*this < v)) && (*this != v))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::operator + (const Vector3f& v) const
{
    return Vector3f(x + v.x, y + v.y, z + v.z);
}
//---------------------------------------------------------------------------------------

Vector3f& Vector3f::operator += (const Vector3f& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::operator - (const Vector3f& v) const
{
    return Vector3f(x - v.x, y - v.y, z - v.z);
}
//---------------------------------------------------------------------------------------

Vector3f& Vector3f::operator -= (const Vector3f& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::operator * (const AReal32& scalar) const
{
    return Vector3f(x * scalar, y * scalar, z * scalar);
}
//---------------------------------------------------------------------------------------

Vector3f& Vector3f::operator *= (const AReal32& scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::operator / (const AReal32& scalar) const
{
    return Vector3f(x / scalar, y / scalar, z / scalar);
}
//---------------------------------------------------------------------------------------

Vector3f& Vector3f::operator /= (const AReal32& scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

AReal32 Vector3f::angleDeg(const Vector3f& v) const
{
    Vector3f v1 = *this;
    Vector3f v2 = v;

    v1.normalize();
    v2.normalize();

    AReal32 d = v1.dot(v2);

    if (d < -1.0f)   d = -1.0f;
    if (d > 1.0f)   d = 1.0f;

    AReal32 rad = acos(d);

    return (AReal32)RAD_TO_DEG64(rad);
}
//---------------------------------------------------------------------------------------

AReal32 Vector3f::angleRad(const Vector3f& v) const
{
    Vector3f v1 = *this;
    Vector3f v2 = v;

    v1.normalize();
    v2.normalize();

    AReal32 d = v1.dot(v2);

    if (d < -1.0f)   d = -1.0f;
    if (d > 1.0f)   d = 1.0f;

    return acos(d);
}
//---------------------------------------------------------------------------------------

Vector3f Vector3f::operator - () const
{
    return Vector3f(-x, -y, -z);
}
//---------------------------------------------------------------------------------------

