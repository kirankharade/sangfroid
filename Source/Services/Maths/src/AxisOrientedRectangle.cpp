
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "AxisOrientedRectangle.h"
#include "Box.h"
#include "Line.h"
#include "LineSegment2D.h"
#include "PointTriangle.h"
#include "IntersectionInfo.h"
#include "Converter.h"
#include <vector>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

AxisOrientedRectangle::AxisOrientedRectangle()
    : m_minPoint(ORIGIN)
    , m_maxPoint(ORIGIN)
{
}
//---------------------------------------------------------------------------------------

AxisOrientedRectangle::AxisOrientedRectangle(const Point2f& minPoint, const Point2f& maxPoint)
    : m_minPoint(minPoint)
    , m_maxPoint(maxPoint)
{
    validate();
}
//---------------------------------------------------------------------------------------

AxisOrientedRectangle::AxisOrientedRectangle(const AxisOrientedRectangle& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
}
//---------------------------------------------------------------------------------------

AxisOrientedRectangle::~AxisOrientedRectangle()
{
}
//---------------------------------------------------------------------------------------

void AxisOrientedRectangle::setMinCorner(const Point2f& minPoint)
{
    m_minPoint = minPoint;
    validate();
}
//---------------------------------------------------------------------------------------

void AxisOrientedRectangle::setMaxCorner(const Point2f& maxPoint)
{
    m_maxPoint = maxPoint;
    validate();
}
//---------------------------------------------------------------------------------------

Point2f AxisOrientedRectangle::getMinCorner() const
{
    return m_minPoint;
}
//---------------------------------------------------------------------------------------

Point2f AxisOrientedRectangle::getMaxCorner() const
{
    return m_maxPoint;
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedRectangle::getWidth() const
{
    return (m_maxPoint.x - m_minPoint.x);
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedRectangle::getHeight() const
{
    return (m_maxPoint.y - m_minPoint.y);
}
//---------------------------------------------------------------------------------------

Point2f AxisOrientedRectangle::getCentre() const
{
    return ((m_minPoint + m_maxPoint) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 AxisOrientedRectangle::getSurfaceArea() const
{
    AReal32 w = m_maxPoint.x - m_minPoint.x;
    AReal32 h = m_maxPoint.y - m_minPoint.y;
    return (w*h);
}
//---------------------------------------------------------------------------------------

Point2f* AxisOrientedRectangle::getCorners() const
{
    Point2f* pCorners = new Point2f[4];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;

    pCorners[0] = Point2f(x1, y1);
    pCorners[1] = Point2f(x2, y1);
    pCorners[2] = Point2f(x2, y2);
    pCorners[3] = Point2f(x1, y2);

    return pCorners;
}
//---------------------------------------------------------------------------------------

LineSegment2D* AxisOrientedRectangle::getEdges() const
{
    LineSegment2D* pEdges = new LineSegment2D[4];

    Point2f pts[8];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;

    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;


    pts[0] = Point2f(x1, y1);
    pts[1] = Point2f(x2, y1);
    pts[2] = Point2f(x2, y2);
    pts[3] = Point2f(x1, y2);

    pEdges[0] = LineSegment2D(pts[0], pts[1]);
    pEdges[1] = LineSegment2D(pts[1], pts[2]);
    pEdges[2] = LineSegment2D(pts[2], pts[3]);
    pEdges[3] = LineSegment2D(pts[3], pts[0]);

    return pEdges;
}
//---------------------------------------------------------------------------------------

Sangfroid::Maths::PointTriangle* AxisOrientedRectangle::getTriangles() const
{
    Point3f c[4];

    //XY-plane through origin
    c[0] = Converter::convertTo3f(m_minPoint);
    c[1] = Converter::convertTo3f(m_minPoint + (X_AXIS * getWidth()));
    c[2] = Converter::convertTo3f(m_minPoint + (X_AXIS * getWidth()) + (Y_AXIS * getHeight()));
    c[3] = Converter::convertTo3f(m_minPoint + (Y_AXIS * getHeight()));

    PointTriangle* tris = new PointTriangle[2];

    //Face 1 - 0, 1, 2, 3
    tris[0] = PointTriangle(Converter::convertTo3f(c[0]), Converter::convertTo3f(c[1]), Converter::convertTo3f(c[2]));
    tris[1] = PointTriangle(Converter::convertTo3f(c[2]), Converter::convertTo3f(c[3]), Converter::convertTo3f(c[0]));

    return tris;
}
//---------------------------------------------------------------------------------------


bool AxisOrientedRectangle::isInside(const Point2f& point, const AReal32 tolerance) const
{
    AReal32 x = point.x;
    AReal32 y = point.y;

    if (GTEQT(x, m_minPoint.x, tolerance) && LTEQT(x, m_maxPoint.x, tolerance) &&
        GTEQT(y, m_minPoint.y, tolerance) && LTEQT(y, m_maxPoint.y, tolerance))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

//Assignment operator

AxisOrientedRectangle& AxisOrientedRectangle::operator = (const AxisOrientedRectangle& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool AxisOrientedRectangle::operator == (const AxisOrientedRectangle& box) const
{
    if ((m_minPoint == box.m_minPoint) && (m_maxPoint == box.m_maxPoint))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool AxisOrientedRectangle::operator != (const AxisOrientedRectangle& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

bool AxisOrientedRectangle::operator < (const AxisOrientedRectangle& box) const
{
    if (m_minPoint < box.m_minPoint)      return true;
    if (m_minPoint > box.m_minPoint)      return false;
    if (m_maxPoint < box.m_maxPoint)      return true;
    if (m_maxPoint > box.m_maxPoint)      return false;
    return false;
}
//---------------------------------------------------------------------------------------

void AxisOrientedRectangle::validate()
{
    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;

    if (x1 > x2)
    {
        m_minPoint.x = x2;
        m_maxPoint.x = x1;
    }
    if (y1 > y2)
    {
        m_minPoint.y = y2;
        m_maxPoint.y = y1;
    }
}
//---------------------------------------------------------------------------------------

//bool AxisOrientedRectangle::isIntersecting(const Line* pLine) const
//{
//    if(!pLine)
//    {
//        return false;
//    }
//    Plane facePlanes[6]; //6 planes for 6 faces of the box
//    getFacePlanes((Plane*&) facePlanes);
//
//    Point2f intersectionPoint;
//
//    for(AInt16 i = 0; i < 6; i++)
//    {
//        if(pLine->isIntersecting(&facePlanes[i], intersectionPoint))
//        {
//            if(isInside(intersectionPoint, TOLERANCE_32))
//            {
//                return true;
//            }
//        }
//    }
//    return false;
//}
////---------------------------------------------------------------------------------------
//
//CPointIntersectionInfo* AxisOrientedRectangle::getIntersections(const Line* pLine) const
//{
//    if(!pLine)
//    {
//        return NULL;
//    }
//    Plane facePlanes[6]; //6 planes for 6 faces of the box
//    getFacePlanes((Plane*&) facePlanes);
//
//    Point2f intersectionPoint;
//    std::vector<SPointIntersection> hits;
//
//    for(AInt16 i = 0; i < 6; i++)
//    {
//        if(pLine->isIntersecting(&facePlanes[i], intersectionPoint))
//        {
//            if(isInside(intersectionPoint, TOLERANCE_32))
//            {
//                SPointIntersection spi;
//                spi.m_hitPrimitiveIndex = i;
//                spi.m_intersectionPoint = intersectionPoint;
//                hits.push_back(spi);
//            }
//        }
//    }
//    AUInt32 intersectionCount = (AUInt32) hits.size();
//    if(intersectionCount)
//    {
//        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
//        return pInfo;
//    }
//    return NULL;
//}
//---------------------------------------------------------------------------------------

