#include "Plane.h"
#include "MathUtils.h"
#include "MathsSettings.h"
#include "Line.h"
#include "Ray.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Plane::Plane()
    : m_point(Point3f())
    , m_normal(Vector3f(0, 0, 1))
{
}
//---------------------------------------------------------------------------------------

Plane::Plane(const Point3f& point, const Vector3f& normal)
    : m_point(point)
    , m_normal(normal)
{
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

Plane::Plane(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D)
    : m_point(Point3f())
    , m_normal(Vector3f(0, 0, 1))
{
    m_normal.x = A;
    m_normal.y = B;
    m_normal.z = C;
    m_normal.normalize();

    Vector3f v = m_normal * (-D);
    m_point.x = v.x;
    m_point.y = v.y;
    m_point.z = v.z;
}
//---------------------------------------------------------------------------------------

Plane::Plane(const Point3f& p1, const Point3f& p2, const Point3f& p3)
{
    m_point = p1;
    m_normal = (p2 - p1).cross((p3 - p1));
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

Plane::Plane(const Plane& plane)
{
    m_point = plane.m_point;
    m_normal = plane.m_normal;
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

Plane::~Plane()
{
}
//---------------------------------------------------------------------------------------

void Plane::set(const Point3f& point, const Vector3f& normal)
{
    m_point = point;
    m_normal = normal;
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

void Plane::set(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D)
{
    m_normal.x = A;
    m_normal.y = B;
    m_normal.z = C;
    m_normal.normalize();

    Vector3f v = m_normal * (-D);
    m_point.x = v.x;
    m_point.y = v.y;
    m_point.z = v.z;
}
//---------------------------------------------------------------------------------------

void Plane::set(const Point3f& p1, const Point3f& p2, const Point3f& p3)
{
    m_point = p1;
    m_normal = (p2 - p1).cross((p3 - p1));
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

Point3f Plane::getPoint() const
{
    return m_point;
}
//---------------------------------------------------------------------------------------

Vector3f Plane::getNormal() const
{
    return m_normal;
}
//---------------------------------------------------------------------------------------

void Plane::getCoefficients(AReal32& A, AReal32& B, AReal32& C, AReal32& D)
{
    A = m_normal.x;
    B = m_normal.y;
    C = m_normal.z;
    D = -((m_point.x * A) + (m_point.y * B) + (m_point.z * C));
}
//---------------------------------------------------------------------------------------

AReal32 Plane::getCoefficientD() const
{
    return -((m_point.x * m_normal.x) + (m_point.y * m_normal.y) + (m_point.z * m_normal.z));
}
//---------------------------------------------------------------------------------------

bool Plane::isIntersecting(const Line*& pLine) const
{
    return pLine->isIntersecting(this);
}
//---------------------------------------------------------------------------------------

bool Plane::isIntersecting(const Line*& pLine, Point3f& intersectionPoint) const
{
    return pLine->isIntersecting(this, intersectionPoint);
}
//---------------------------------------------------------------------------------------

Point3f Plane::closestPoint(const Point3f& point) const
{
    AReal32 distFromPlane = m_normal.dot(point - m_point);
    Point3f pt = point + (m_normal * (-distFromPlane));
    return pt;
}
//---------------------------------------------------------------------------------------

AReal32 Plane::distFromPoint(const Point3f& point) const
{
    return m_normal.dot(point - m_point);
}
//---------------------------------------------------------------------------------------

bool Plane::isParallel(const Plane& plane) const
{
    Vector3f thisNormal = m_normal;
    Vector3f otherNormal = plane.getNormal();
    thisNormal.normalize();
    otherNormal.normalize();

    return EQ_32(thisNormal.dot(otherNormal), 1.0f);
}
//---------------------------------------------------------------------------------------

EPlaneSideQualification Plane::getQualification(const Point3f& point, const AReal32 tolerance) const
{
    AReal32 dot = (point - m_point).dot(m_normal);

    if (EQT(dot, 0.0f, tolerance))
    {
        return EPlaneSideOnPlane;
    }
    if (dot < 0)
    {
        return EPlaneSideBack;
    }
    return EPlaneSideFront;
}
//---------------------------------------------------------------------------------------

bool Plane::Intersection(const Plane& plane1, const Plane& plane2, Point3f& intersectionPoint) const
{
    Point3f linePoint;
    Vector3f lineDir;
    bool bStatus = Intersection(plane1, linePoint, lineDir);
    if (false == bStatus)
    {
        return false;
    }
    Line line(linePoint, lineDir);
    return line.isIntersecting(&plane2, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool Plane::Intersection(const Plane& plane, Point3f& intersectionPoint, Vector3f& intersectionlineDir) const
{
    const float u = m_normal.magnitude();           //u should be equal to 1.0
    const float dot = m_normal.dot(plane.m_normal);
    const float up = plane.m_normal.magnitude();    //up should be equal to 1.0
    const double det = (u * up) - (dot * dot);
    if (fabs(det) < TOLERANCE_64)
    {
        return false;
    }
    float r = getCoefficientD();
    float rp = plane.getCoefficientD();
    const float idet = (float)1.0 / (float)det;

    const float a = (up * (-r) + dot * (rp)) * idet;
    const float b = (u * (-rp) + dot *  (r)) * idet;

    intersectionlineDir = m_normal.cross(plane.m_normal);

    Vector3f v(m_normal * a + plane.m_normal * b);

    intersectionPoint.x = v.x;
    intersectionPoint.y = v.y;
    intersectionPoint.z = v.z;

    return true;
}
//---------------------------------------------------------------------------------------

//void Plane::getSquarePlaneCorners(const Point3f& ptProjected, const AReal32& squareSize, Point3f*& corners) const
//{
//
//}
////---------------------------------------------------------------------------------------
//
//void Plane::getSquarePlaneCorners(const Vector3f& yAxisProjected, const AReal32& squareSize, Point3f*& corners) const
//{
//
//}
////---------------------------------------------------------------------------------------

//TO DO:

//bool Plane::isIntersecting(const Box* pBox) const
//{
//
//}
//---------------------------------------------------------------------------------------

//bool Plane::isIntersecting(const Box* pBox, IntersectionList*& list) const
//{
//
//}
//---------------------------------------------------------------------------------------

//Assignment operator

Plane& Plane::operator = (const Plane& plane)
{
    m_normal = plane.m_normal;
    m_point = plane.m_point;
    m_normal.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool Plane::operator == (const Plane& plane) const
{
    Vector3f normal = m_normal;
    normal.normalize();

    Vector3f otherNormal = plane.getNormal();
    otherNormal.normalize();

    if (normal == otherNormal)
    {
        if (EPlaneSideOnPlane == getQualification(plane.getPoint(), TOLERANCE_32))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Plane::operator != (const Plane& plane) const
{
    return !(*this == plane);
}
//---------------------------------------------------------------------------------------

bool Plane::operator < (const Plane& plane) const
{
    if (*this == plane)
    {
        return false;
    }

    Vector3f normal = m_normal;
    normal.normalize();

    Vector3f otherNormal = plane.getNormal();
    otherNormal.normalize();

    return (normal < otherNormal);
}
//---------------------------------------------------------------------------------------
