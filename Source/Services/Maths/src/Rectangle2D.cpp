
//---------------------------------------------------------------------------------------

//#include "MathIncludes.h"
#include "Rectangle2D.h"
#include "MathsMiscUtils.h"
#include "MathsSettings.h"
#include "Converter.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Rectangle2D::Rectangle2D()
{
    init();
}
//---------------------------------------------------------------------------------------

Rectangle2D::Rectangle2D(const AReal32& left, const AReal32& right, const AReal32& top, const AReal32& bottom)
{
    set(left, right, top, bottom);
    m_zAxisRotationDeg = 0.0f;
}
//---------------------------------------------------------------------------------------

Rectangle2D::Rectangle2D(const Point2f& reference, const AReal32& width, const AReal32& height)
{
    set(reference, width, height);
    m_zAxisRotationDeg = 0.0f;
}
//---------------------------------------------------------------------------------------

Rectangle2D::Rectangle2D(const AInt32& left, const AInt32& right, const AInt32& top, const AInt32& bottom)
{
    set(left, right, top, bottom);
    m_zAxisRotationDeg = 0.0f;
}
//---------------------------------------------------------------------------------------

Rectangle2D::Rectangle2D(const Point2i& reference, const AInt32& width, const AInt32& height)
{
    set(reference, width, height);
    m_zAxisRotationDeg = 0.0f;
}
//---------------------------------------------------------------------------------------

Rectangle2D::Rectangle2D(const Rectangle2D& rect)
{
    m_left = rect.m_left;
    m_right = rect.m_right;
    m_top = rect.m_top;
    m_bottom = rect.m_bottom;

    m_zAxisRotationDeg = rect.m_zAxisRotationDeg;

    checkValidity();
}
//---------------------------------------------------------------------------------------

Rectangle2D::~Rectangle2D()
{
}
//---------------------------------------------------------------------------------------

void Rectangle2D::set(const AReal32& left, const AReal32& right, const AReal32& top, const AReal32& bottom)
{
    m_left = left;
    m_right = right;
    m_top = top;
    m_bottom = bottom;

    checkValidity();
}
//---------------------------------------------------------------------------------------

void Rectangle2D::set(const Point2f& reference, const AReal32& width, const AReal32& height)
{
    if (EScreenConventionPosYAxisUpNegative == CSettings::YAxis2DConvention())
    {
        m_left = reference.x;
        m_top = reference.y;
        m_right = m_left + ABS(width);
        m_bottom = m_top + ABS(height);
    }
    else
    {
        m_left = reference.x;
        m_bottom = reference.y;
        m_right = m_left + ABS(width);
        m_top = m_bottom + ABS(height);
    }

    checkValidity();
}
//---------------------------------------------------------------------------------------

void Rectangle2D::set(const AInt32& left, const AInt32& right, const AInt32& top, const AInt32& bottom)
{
    m_left = (AReal32)left;
    m_right = (AReal32)right;
    m_top = (AReal32)top;
    m_bottom = (AReal32)bottom;

    checkValidity();
}
//---------------------------------------------------------------------------------------

void Rectangle2D::set(const Point2i& reference, const AInt32& width, const AInt32& height)
{
    set(Converter::convertTo2f(reference), (AReal32)width, (AReal32)height);
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getLeft() const
{
    return m_left;
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getRight() const
{
    return m_right;
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getTop() const
{
    return m_top;
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getBottom() const
{
    return m_bottom;
}
//---------------------------------------------------------------------------------------

Point2f Rectangle2D::getLeftTop() const
{
    return Point2f(m_left, m_top);
}
//---------------------------------------------------------------------------------------

Point2f Rectangle2D::getLeftBottom() const
{
    return Point2f(m_left, m_bottom);
}
//---------------------------------------------------------------------------------------

Point2f Rectangle2D::getRightBottom() const
{
    return Point2f(m_right, m_bottom);
}
//---------------------------------------------------------------------------------------

Point2f Rectangle2D::getRightTop() const
{
    return Point2f(m_right, m_top);
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getArea() const
{
    AReal32 w = std::fabs(m_right - m_left);
    AReal32 h = std::fabs(m_bottom - m_top);
    return (w*h);
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getPerimeter() const
{
    AReal32 w = ABS(m_right - m_left);
    AReal32 h = ABS(m_bottom - m_top);
    return (2 * (w + h));
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getWidth() const
{
    return ABS(m_right - m_left);
}
//---------------------------------------------------------------------------------------

void Rectangle2D::setWidth(const AReal32& width)
{
    m_right = m_left + ABS(width);
    checkValidity();
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getHeight() const
{
    return ABS(m_bottom - m_top);
}
//---------------------------------------------------------------------------------------

void Rectangle2D::setHeight(const AReal32& height)
{
    if (EScreenConventionPosYAxisUpNegative == CSettings::YAxis2DConvention())
    {
        m_bottom = m_top + ABS(height);
    }
    else
    {
        m_top = m_bottom + ABS(height);
    }
    checkValidity();
}
//---------------------------------------------------------------------------------------

AReal32 Rectangle2D::getZAxisRotation() const
{
    return m_zAxisRotationDeg;
}
//---------------------------------------------------------------------------------------

void Rectangle2D::setZAxisRotation(const AReal32& zRotationDeg)
{
    AReal32 angle = zRotationDeg;
    while (angle < 0)
    {
        angle += 360.0f;
    }
    while (angle > (360.0f + TOLERANCE_32))
    {
        angle -= 360.0f;
    }
    m_zAxisRotationDeg = angle;
}
//---------------------------------------------------------------------------------------

bool Rectangle2D::isInside(const Point2i& point) const
{
    if (EScreenConventionPosYAxisUpNegative == CSettings::YAxis2DConvention())
    {
        if (((AReal32)point.x >= m_left) && ((AReal32)point.x <= m_right) &&
            ((AReal32)point.y >= m_top) && ((AReal32)point.y <= m_bottom))
        {
            return true;
        }
    }
    else
    {
        if (((AReal32)point.x >= m_left) && ((AReal32)point.x <= m_right) &&
            ((AReal32)point.y >= m_bottom) && ((AReal32)point.y <= m_top))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Rectangle2D::isInside(const Point2f& point) const
{
    if (EScreenConventionPosYAxisUpNegative == CSettings::YAxis2DConvention())
    {
        if ((point.x >= m_left) && (point.x <= m_right) &&
            (point.y >= m_top) && (point.y <= m_bottom))
        {
            return true;
        }
    }
    else
    {
        if ((point.x >= m_left) && (point.x <= m_right) &&
            (point.y >= m_bottom) && (point.y <= m_top))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

//Assignment operator

Rectangle2D& Rectangle2D::operator = (const Rectangle2D& rect)
{
    m_left = rect.m_left;
    m_right = rect.m_right;
    m_top = rect.m_top;
    m_bottom = rect.m_bottom;
    m_zAxisRotationDeg = rect.m_zAxisRotationDeg;

    checkValidity();

    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool Rectangle2D::operator == (const Rectangle2D& rect) const
{
    if (EQ_32(m_left, rect.m_left) &&
        EQ_32(m_right, rect.m_right) &&
        EQ_32(m_top, rect.m_top) &&
        EQ_32(m_bottom, rect.m_bottom) &&
        EQ_32(m_zAxisRotationDeg, rect.m_zAxisRotationDeg))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Rectangle2D::operator != (const Rectangle2D& rect) const
{
    return !(*this == rect);
}
//---------------------------------------------------------------------------------------

//Private methods

void Rectangle2D::init()
{
    m_left = 0.0f;
    m_right = 0.0f;
    m_top = 0.0f;
    m_bottom = 0.0f;
    m_zAxisRotationDeg = 0.0f;
}
//---------------------------------------------------------------------------------------

void Rectangle2D::checkValidity()
{
    if (m_left > m_right)
    {
        Swap(m_left, m_right);
    }

    if (EScreenConventionPosYAxisUpNegative == CSettings::YAxis2DConvention())
    {
        if (m_top > m_bottom)
        {
            Swap(m_top, m_bottom);
        }
    }
    else
    {
        if (m_top < m_bottom)
        {
            Swap(m_top, m_bottom);
        }
    }
}
//---------------------------------------------------------------------------------------
