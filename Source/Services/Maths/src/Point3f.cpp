
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "Point3f.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Point3f::Point3f()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
}
//---------------------------------------------------------------------------------------

Point3f::Point3f(const AReal32 v[3])
{
    x = v[0];
    y = v[1];
    z = v[2];
}
//---------------------------------------------------------------------------------------

Point3f::Point3f(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

Point3f::Point3f(const Point3f& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
}
//---------------------------------------------------------------------------------------

void Point3f::set(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

void Point3f::set(const Point3f& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
}
//---------------------------------------------------------------------------------------

Vector3f Point3f::positionVector() const
{
    return Vector3f(x, y, z);
}
//---------------------------------------------------------------------------------------

AReal32 Point3f::dist(const Point3f& point) const
{
    return sqrt(((x - point.x)*(x - point.x)) + ((y - point.y)*(y - point.y)) + ((z - point.z)*(z - point.z)));
}
//---------------------------------------------------------------------------------------

Vector3f Point3f::vectorTo(const Point3f& point) const
{
    return Vector3f(point.x - x, point.y - y, point.z - z);
}
//---------------------------------------------------------------------------------------

Vector3f Point3f::vectorFrom(const Point3f& point) const
{
    return Vector3f(x - point.x, y - point.y, z - point.z);
}
//---------------------------------------------------------------------------------------

Point3f Point3f::offsetAlongUnitDir(const Vector3f& dir, const AReal32& offsetDist) const
{
    Vector3f v = dir;
    v.normalize();
    return Point3f((x + (v.x * offsetDist)), (y + (v.y * offsetDist)), (z + (v.z * offsetDist)));
}
//---------------------------------------------------------------------------------------

bool Point3f::AreSame(const Point3f& other, const double& tolerance) const
{
    if (
        (abs(x - other.x) < tolerance) &&
        (abs(y - other.y) < tolerance) &&
        (abs(z - other.z) < tolerance))
    {
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------------------

//operators

AReal32& Point3f::operator [] (const AIndex32& index)
{
    if (0 == index) return x;
    if (1 == index) return y;
    if (2 == index) return z;
    throw "Point3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AReal32& Point3f::operator [] (const AIndex32& index) const
{
    if (0 == index) return x;
    if (1 == index) return y;
    if (2 == index) return z;
    throw "Point3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

Point3f& Point3f::operator = (const Point3f& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
    return *this;
}
//---------------------------------------------------------------------------------------

bool Point3f::operator == (const Point3f& point) const
{
    if (EQ_32(x, point.x) && EQ_32(y, point.y) && EQ_32(z, point.z))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Point3f::operator != (const Point3f& point) const
{
    if ((!EQ_32(x, point.x)) || (!EQ_32(y, point.y)) || (!EQ_32(z, point.z)))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Point3f::operator < (const Point3f& point) const
{
    if (EQ_32(x, point.x) && EQ_32(y, point.y) && EQ_32(z, point.z))
    {
        return false;
    }
    if (x < point.x)      return true;
    else if (x > point.x) return false;
    else if (y < point.y) return true;
    else if (y > point.y) return false;
    else return (z < point.z);
}
//---------------------------------------------------------------------------------------

bool Point3f::operator > (const Point3f& point) const
{
    if ((!(*this < point)) && (*this != point))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

Vector3f Point3f::operator - (const Point3f& point) const
{
    return Vector3f(x - point.x, y - point.y, z - point.z);
}
//---------------------------------------------------------------------------------------

Point3f Point3f::operator + (const Point3f& point) const
{
    return Point3f(x + point.x, y + point.y, z + point.z);
}
//---------------------------------------------------------------------------------------

Point3f Point3f::operator - (const Vector3f& vec) const
{
    return Point3f(x - vec.x, y - vec.y, z - vec.z);
}
//---------------------------------------------------------------------------------------

Point3f Point3f::operator + (const Vector3f& vec) const
{
    return Point3f(x + vec.x, y + vec.y, z + vec.z);
}
//---------------------------------------------------------------------------------------

Point3f& Point3f::operator += (const Vector3f& vec)
{
    x += vec.x;
    y += vec.y;
    z += vec.z;
    return *this;
}
//---------------------------------------------------------------------------------------

Point3f& Point3f::operator -= (const Vector3f& vec)
{
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
    return *this;
}
//---------------------------------------------------------------------------------------

Point3f& Point3f::operator += (const Point3f& point)
{
    x += point.x;
    y += point.y;
    z += point.z;
    return *this;
}
//---------------------------------------------------------------------------------------

Point3f Point3f::operator * (const AReal32& scalar) const
{
    return Point3f(x * scalar, y * scalar, z * scalar);
}
//---------------------------------------------------------------------------------------

Point3f& Point3f::operator *= (const AReal32& scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

Point3f Point3f::operator / (const AReal32& scalar) const
{
    return Point3f(x / scalar, y / scalar, z / scalar);
}
//---------------------------------------------------------------------------------------

Point3f& Point3f::operator /= (const AReal32& scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

Point3f Point3f::operator - () const
{
    return Point3f(-x, -y, -z);
}
//---------------------------------------------------------------------------------------

