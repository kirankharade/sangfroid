
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "Point2i.h"
#include <cfloat>
#include <cmath>

using namespace Sangfroid;
using namespace Sangfroid::Maths;

//---------------------------------------------------------------------------------------

Point2i::Point2i()
{
    x = 0;
    y = 0;
}
//---------------------------------------------------------------------------------------

Point2i::Point2i(const AInt32 v[2])
{
    x = v[0];
    y = v[1];
}
//---------------------------------------------------------------------------------------

Point2i::Point2i(const AInt32& a, const AInt32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

Point2i::Point2i(const Point2i& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

void Point2i::set(const AInt32& a, const AInt32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

void Point2i::set(const Point2i& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

AReal32 Point2i::dist(const Point2i& point) const
{
    return sqrt((AReal32)(((x - point.x)*(x - point.x)) + ((y - point.y)*(y - point.y))));
}
//---------------------------------------------------------------------------------------

//operators

AInt32& Point2i::operator [] (const AIndex32& index)
{
    if (0 == index) return x;
    if (1 == index) return y;
    throw "Point2i::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AInt32& Point2i::operator [] (const AIndex32& index) const
{
    if (0 == index) return x;
    if (1 == index) return y;
    throw "Point2i::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

Point2i& Point2i::operator = (const Point2i& point)
{
    x = point.x;
    y = point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

Point2i& Point2i::operator = (const Point3f& point)
{
    x = (AInt32)point.x;
    y = (AInt32)point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

bool Point2i::operator == (const Point2i& point) const
{
    if ((x == point.x) && (y == point.y))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Point2i::operator != (const Point2i& point) const
{
    if ((x != point.x) && (y != point.y))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool Point2i::operator < (const Point2i& point) const
{
    if ((x == point.x) && (y == point.y))
    {
        return false;
    }
    if (x < point.x)      return true;
    else if (x > point.x) return false;
    else return (y < point.y);
}
//---------------------------------------------------------------------------------------

Point2i Point2i::operator * (const AReal32& scalar) const
{
    return Point2i((AInt32)(x * scalar), (AInt32)(y * scalar));
}
//---------------------------------------------------------------------------------------

Point2i& Point2i::operator *= (const AReal32& scalar)
{
    x = (AInt32)(x * scalar);
    y = (AInt32)(y * scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

Point2i Point2i::operator / (const AReal32& scalar) const
{
    return Point2i((AInt32)(x / scalar), (AInt32)(y / scalar));
}
//---------------------------------------------------------------------------------------

Point2i& Point2i::operator /= (const AReal32& scalar)
{
    x = (AInt32)(x / scalar);
    y = (AInt32)(y / scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

Point2i Point2i::operator - () const
{
    return Point2i(-x, -y);
}
//---------------------------------------------------------------------------------------

