/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "MathsStructs.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

class AN_MATHS_API ConvexHull2D
{
private:

    CPoint2fList                    m_inputPoints;
    CPoint2fList                    m_hullPoints;
    Point2f                        m_centroid;
    SCoordinateExtents              m_extents;

public:

    ConvexHull2D();

    ConvexHull2D(const std::vector<Point2f>& points);

    ConvexHull2D(const ConvexHull2D& hull);

    virtual ~ConvexHull2D();

    void addPoint(const Point2f& point);

    void addPoints(const std::vector<Point2f>& points);

    const std::vector<Point2f>& getInputPoints() const;

    const std::vector<Point2f>& getHullPoints() const;

    bool isPointOnHull(const Point2f& point) const;

    const Point2f& getCentroid() const;

    const SCoordinateExtents& getExtents() const;

    //Assignment operator

    ConvexHull2D& operator = (const ConvexHull2D& hull);

private:

    void generate();

    void generateHalfHull(std::vector<Point2f>& inputPoints);

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
