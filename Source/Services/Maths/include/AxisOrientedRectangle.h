/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "Vector3f.h"
#include "Matrix4f.h"
#include "PointTriangle.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Line;
class LineSegment2D;
class CPointIntersectionInfo;
//class PointTriangle;
//---------------------------------------------------------------------------------------

class AN_MATHS_API AxisOrientedRectangle
{
protected:

    Point2f    m_minPoint;
    Point2f    m_maxPoint;

public:

    AxisOrientedRectangle();

    AxisOrientedRectangle(const Point2f& minPoint, const Point2f& maxPoint);

    AxisOrientedRectangle(const AxisOrientedRectangle& box);

    virtual ~AxisOrientedRectangle();

    void setMinCorner(const Point2f& minPoint);

    void setMaxCorner(const Point2f& maxPoint);

    Point2f getMinCorner() const;

    Point2f getMaxCorner() const;

    AReal32 getWidth() const;

    AReal32 getHeight() const;

    Point2f getCentre() const;

    AReal32 getSurfaceArea() const;

    Point2f* getCorners() const;

    LineSegment2D* getEdges() const;

    PointTriangle* getTriangles() const;

    //bool isIntersecting(const Line* pLine) const;

    //CPointIntersectionInfo* getIntersections(const Line* pLine) const;

    bool isInside(const Point2f& point, const AReal32 tolerance = TOLERANCE_32) const;

    //Assignment operator

    virtual AxisOrientedRectangle& operator = (const AxisOrientedRectangle& box);

    //Comparison Operators

    virtual bool operator == (const AxisOrientedRectangle& box) const;

    virtual bool operator != (const AxisOrientedRectangle& box) const;

    virtual bool operator < (const AxisOrientedRectangle& box) const;

protected:

    void validate();

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
