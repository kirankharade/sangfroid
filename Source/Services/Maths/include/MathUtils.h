/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "MathUtils.h"
#include "MathsStructs.h"
#include <vector>

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class AN_MATHS_API MathUtils
{
public:

    static AReal32 getCornerAngleDeg(const Point2f& pa, const Point2f& pb, const Point2f& pc);

    static AReal32 getCornerAngleDeg(const Point3f& pa, const Point3f& pb, const Point3f& pc);

    static void sortAscending(std::vector<Point2f>& points);

    static void sortAscending(std::vector<Point3f>& points);

    static void sortDescending(std::vector<Point2f>& points);

    static void sortDescending(std::vector<Point3f>& points);

    static Point2f getCentroid(const std::vector<Point2f>& points);

    static Point3f getCentroid(const std::vector<Point3f>& points);

    static SCoordinateExtents getExtents(const std::vector<Point2f>& points);

    static SCoordinateExtents getExtents(const std::vector<Point3f>& points);

    static AReal32 GetMax(AReal32 a, AReal32 b, AReal32 c);

    static AReal32 GetMin(AReal32 a, AReal32 b, AReal32 c);

    static std::tuple<Point3f, AReal32> NearestPoint(const Point3f& p, const std::vector<Point3f>& points);

    static Sangfroid::Maths::Point3f Project(
        const Sangfroid::Maths::Point3f &point,
        const Sangfroid::Maths::Point3f &planePt,
        const Sangfroid::Maths::Vector3f &planeNorm);

    static Sangfroid::Maths::Point3f ProjectOnLine(
        const Sangfroid::Maths::Point3f &linePt,
        const Sangfroid::Maths::Vector3f &lineDir,
        const Sangfroid::Maths::Point3f &ptToBeProjected);

    static double GetProjectedDistFromLine(
        const Sangfroid::Maths::Point3f &linePt,
        const Sangfroid::Maths::Vector3f &lineDir,
        const Sangfroid::Maths::Point3f &fromPoint);

    static Sangfroid::Maths::Point3f ProjectOnLine(
        const Sangfroid::Maths::Point3f &p1,
        const Sangfroid::Maths::Point3f &p2,
        const Sangfroid::Maths::Point3f &p);

    static double Distance_PointToPlane(
        const Sangfroid::Maths::Point3f& point,
        const Sangfroid::Maths::Point3f& planePoint,
        const Sangfroid::Maths::Vector3f& planeNormal);

    static double DistanceSigned_PointToPlane(
        const Sangfroid::Maths::Point3f& point,
        const Sangfroid::Maths::Point3f& planePoint,
        const Sangfroid::Maths::Vector3f& planeNormal);

    static double Distance_PointToLine(
        const Sangfroid::Maths::Point3f& lineStart,
        const Sangfroid::Maths::Vector3f& dir,
        const Sangfroid::Maths::Point3f& point);

    static float Distance_PointToTriangle(
        const Sangfroid::Maths::Point3f& p,
        const Sangfroid::Maths::Point3f& t0,
        const Sangfroid::Maths::Point3f& t1,
        const Sangfroid::Maths::Point3f& t2,
        const Sangfroid::Maths::Vector3f& normal);

    static Sangfroid::Maths::Point3f Centroid(
        const Sangfroid::Maths::Point3f &p0,
        const Sangfroid::Maths::Point3f &p1,
        const Sangfroid::Maths::Point3f &p2);

    static Sangfroid::Maths::Point3f Centroid(
        const std::vector<Sangfroid::Maths::Point3f> &points);

    static Sangfroid::Maths::Point3f MidPoint(
        const Sangfroid::Maths::Point3f &p0,
        const Sangfroid::Maths::Point3f &p1);

    static Sangfroid::Maths::Vector3f NormalFrom3Points(
        const Sangfroid::Maths::Point3f& a,
        const Sangfroid::Maths::Point3f& b,
        const Sangfroid::Maths::Point3f& c);

    static bool InTriangle(
        const Sangfroid::Maths::Point3f& p,
        const Sangfroid::Maths::Point3f& trianglePt0,
        const Sangfroid::Maths::Point3f& trianglePt1,
        const Sangfroid::Maths::Point3f& trianglePt2);

	static bool LineSegmentPlaneIntersection(
		const Sangfroid::Maths::Point3f& startPoint,
		const Sangfroid::Maths::Point3f& endPoint,
		const Sangfroid::Maths::Point3f& planePoint,
		const Sangfroid::Maths::Vector3f& planeNormal,
		Sangfroid::Maths::Point3f& intersection,
		bool & segmentLiesInPlane,
		const float proximityTolerance,
		const float parametricTolerance = 0.0001);
};

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
