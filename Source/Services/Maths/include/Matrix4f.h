/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Vector3f.h"
#include "Point3f.h"
#include "MathsEnums.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class AN_MATHS_API Matrix4f
{
public:

    union
    {
        AReal32 m[16];

        struct
        {
            AReal32 e[4][4];
        };
    };

private:

    Vector3f m_scale;

    bool m_bAbsoluteScaleRecoverable;

public:

    Matrix4f(const ABool& bSetIdentity = true);

    Matrix4f(const AReal32* v, const bool bScaleRecoverable = true);

    Matrix4f(const Matrix4f& matrix);

    void set(const AReal32* v, const bool bScaleRecoverable = true);

    void set(const Matrix4f& matrix);

    void setIdentity();

    void setZero();

    bool isIdentity() const;

    bool isZero() const;

    const AReal32* ptr() const;

    AReal32* ptr();

    Matrix4f inverse() const;

    Matrix4f transpose() const;

    AReal32 determinent() const;

    //Rotations

    void resetRotation();

    Matrix4f getRotationMatrix() const;

    Vector3f getRotationDeg() const;

    void setRotationDeg(const AReal32& dx, const AReal32& dy, const AReal32& dz);

    void setRotationDeg(const Vector3f& d);

    void setXRotationDeg(const AReal32& dx);

    void setYRotationDeg(const AReal32& dy);

    void setZRotationDeg(const AReal32& dz);

    //Scaling

    void resetScaling();

    Vector3f getScaleValues();

    void setScaleValues(const AReal32& sx, const AReal32& sy, const AReal32& sz);

    void setScaleValues(const Vector3f& s);

    void scaleMatrix(const AReal32& sx, const AReal32& sy, const AReal32& sz);

    void scaleMatrix(const Vector3f& s);

    void scaleMatrixX(const AReal32& sx);

    void scaleMatrixY(const AReal32& sy);

    void scaleMatrixZ(const AReal32& sz);

    //Translation

    void resetTranslation();

    Vector3f getTranslation() const;

    void setTranslation(const AReal32& tx, const AReal32& ty, const AReal32& tz);

    void setTranslation(const Vector3f& t);

    void setXTranslation(const AReal32& tx);

    void setYTranslation(const AReal32& ty);

    void setZTranslation(const AReal32& tz);

    void addTranslation(const AReal32& tx, const AReal32& ty, const AReal32& tz);

    void addTranslation(const Vector3f& t);

    //Rotation about arbritrary axis

    Matrix4f  RotateDegAboutVectorThroughOrigin(const Vector3f& vec, const float& angleDeg);

    Matrix4f  RotateDegAboutVectorThroughPoint(const Vector3f& vec, const Point3f& point, const float& angleDeg);

    //Camera related transformation matrix

    Matrix4f perspectiveProjectionFieldOfViewRHS(const AReal32& fovDeg, const AReal32& aspectRatio, const AReal32& near, const AReal32& far);

    Matrix4f perspectiveProjectionFieldOfViewLHS(const AReal32& fovDeg, const AReal32& aspectRatio, const AReal32& near, const AReal32& far);

    Matrix4f perspectiveProjectionLHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

    Matrix4f perspectiveProjectionRHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

    Matrix4f orthographicProjectionLHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

    Matrix4f orthographicProjectionRHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

    Matrix4f viewMatrixLHS(const Point3f& camPosition, const Point3f& camTarget, const Vector3f& upVector);

    Matrix4f viewMatrixRHS(const Point3f& camPosition, const Point3f& camTarget, const Vector3f& upVector);

    Matrix4f cameraLookAtRHS(const Point3f& camPosition, const Point3f& camTarget, const Vector3f& upVector);

    Matrix4f cameraLookAtLHS(const Point3f& camPosition, const Point3f& camTarget, const Vector3f& upVector);

    const Matrix4f& cameraLookInDirection(const Point3f& camPosition, const Vector3f& camDirection, const Vector3f& upVector);

    //Transformations for vector alignments

    Matrix4f  AlignVecToPositiveZ(const Vector3f& vec);

    Matrix4f  RotateVecToPlaneXZ(const Vector3f& vec);

    //Transformation methods

    //Only rotates the vector. Does not apply scaling.
    Vector3f rotate(const Vector3f& vec) const;

    //Only rotates the point. Does not apply scaling.
    Point3f rotate(const Point3f& point) const;

    Vector3f rotateAndScale(const Vector3f& vec) const;

    Point3f rotateAndScale(const Point3f& point) const;

    Point3f translate(const Point3f& point) const;

    Point3f transform(const Point3f& point) const;

    //Operators

    AReal32& operator () (const AIndex32& rowIndex, const AIndex32& colIndex);

    const AReal32& operator () (const AIndex32& rowIndex, const AIndex32& colIndex) const;

    //Index Operators

    AReal32& operator [] (const AIndex32& index);

    const AReal32& operator [] (const AIndex32& index) const;

    //assignment operator

    Matrix4f& operator = (const Matrix4f& matrix);

    //Comparison Operators

    bool operator == (const Matrix4f& matrix) const;

    bool operator != (const Matrix4f& matrix) const;

    bool operator < (const Matrix4f& matrix) const;

    //Summation and subtraction operators

    Matrix4f operator + (const Matrix4f& matrix) const;

    Matrix4f& operator += (const Matrix4f& matrix);

    Matrix4f operator - (const Matrix4f& matrix) const;

    Matrix4f& operator -= (const Matrix4f& matrix);

    Matrix4f operator * (const Matrix4f& matrix) const;

    Matrix4f& operator *= (const Matrix4f& matrix);

    Vector3f  operator* (const Vector3f& vec) const;

    Point3f  operator* (const Point3f& point) const;

    Matrix4f operator * (const AReal32& scalar) const;

    Matrix4f& operator *= (const AReal32& scalar);

    Matrix4f operator / (const AReal32& scalar) const;

    Matrix4f& operator /= (const AReal32& scalar);

    Matrix4f operator - () const;

protected:

    void add(const AReal32* ptr);

    void subtract(const AReal32* ptr);

    Vector3f recoverScale() const;
};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
