/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Plane;
//---------------------------------------------------------------------------------------

class AN_MATHS_API Ray
{
protected:

    Point3f    m_point;
    Vector3f   m_direction;

public:

    Ray();

    Ray(const Point3f& point, const Vector3f& direction);

    Ray(const Point3f& firstPoint, const Point3f& secondPoint);

    Ray(const Ray& ray);

    virtual ~Ray();

    void set(const Point3f& point, const Vector3f& direction);

    void set(const Point3f& firstPoint, const Point3f& secondPoint);

    Point3f getPoint() const;

    Vector3f getDirection() const;

    bool isIntersecting(const Plane* pPlane) const;

    bool isIntersecting(const Plane* pPlane, Point3f& intersectionPoint) const;

    bool passesThrough(const Point3f& point, const AReal32 tolerance = TOLERANCE_32) const;

    Point3f closestPoint(const Point3f& point) const;

    AReal32 distFromPoint(const Point3f& point) const;

    //Assignment operator

    Ray& operator = (const Ray& ray);

    //Comparison Operators

    bool operator == (const Ray& ray) const;

    bool operator != (const Ray& ray) const;

    bool operator < (const Ray& ray) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
