/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Vector3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

#define ORIGIN Point3f(0, 0, 0)

//---------------------------------------------------------------------------------------

class AN_MATHS_API Point3f
{
public:

    union
    {
        AReal32 v[3];

        struct
        {
            AReal32 x;
            AReal32 y;
            AReal32 z;
        };
    };


public:

    Point3f();

    Point3f(const AReal32 v[3]);

    Point3f(const AReal32& a, const AReal32& b, const AReal32& c);

    Point3f(const Point3f& point);

    void set(const AReal32& a, const AReal32& b, const AReal32& c);

    void set(const Point3f& point);

    Vector3f positionVector() const;

    AReal32 dist(const Point3f& point) const;

    Vector3f vectorTo(const Point3f& point) const;

    Vector3f vectorFrom(const Point3f& point) const;

    Point3f offsetAlongUnitDir(const Vector3f& dir, const AReal32& offsetDist) const;

    bool AreSame(const Point3f& other, const double& tolerance) const;

    //Index Operators

    AReal32& operator [] (const AIndex32& i);

    const AReal32& operator [] (const AIndex32& i) const;

    //assignment operator

    Point3f& operator = (const Point3f& point);

    //Comparison Operators

    bool operator == (const Point3f& point) const;

    bool operator != (const Point3f& point) const;

    bool operator < (const Point3f& point) const;

    bool operator > (const Point3f& point) const;

    //Summation and subtraction operators

    Vector3f operator - (const Point3f& point) const;

    Point3f operator + (const Point3f& point) const;

    Point3f operator - (const Vector3f& vec) const;

    Point3f operator + (const Vector3f& vec) const;

    Point3f& operator += (const Vector3f& vec);

    Point3f& operator -= (const Vector3f& vec);

    Point3f& operator += (const Point3f& point);

    //Operators with scalar arguments

    Point3f operator * (const AReal32& scalar) const;

    Point3f& operator *= (const AReal32& scalar);

    Point3f operator / (const AReal32& scalar) const;

    Point3f& operator /= (const AReal32& scalar);

    Point3f operator - () const;

};
//---------------------------------------------------------------------------------------
typedef AN_MATHS_API std::vector<Point3f>   CPoint3fList;
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
