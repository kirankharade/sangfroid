/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "Point3f.h"
#include "Converter.h"
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

struct AN_MATHS_API SCoordinateExtents
{
    Point3f    minPoint;
    Point3f    maxPoint;

    SCoordinateExtents()
    {
        minPoint = Point3f();
        maxPoint = Point3f();
    }

    SCoordinateExtents(const Point3f& minPt, const Point3f& maxPt)
    {
        minPoint = minPt;
        maxPoint = maxPt;
    }

    SCoordinateExtents(const Point2f& minPt, const Point2f& maxPt)
    {
        minPoint = Converter::convertTo3f(minPt);
        maxPoint = Converter::convertTo3f(maxPt);
    }
};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
