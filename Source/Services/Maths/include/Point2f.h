/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"
#include "Point2i.h"
#include <vector>
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class AN_MATHS_API Point2f
{
public:

    union
    {
        AReal32 v[2];

        struct
        {
            AReal32 x;
            AReal32 y;
        };
    };


public:

    Point2f();

    Point2f(const AReal32 v[2]);

    Point2f(const AReal32& a, const AReal32& b);

    Point2f(const Point2f& point);

    Point2f(const Point2i& point);

    Point2f(const Point3f& point);

    void set(const AReal32& a, const AReal32& b);

    void set(const Point2f& point);

    AReal32 dist(const Point2f& point) const;

    //Index Operators

    AReal32& operator [] (const AIndex32& i);

    const AReal32& operator [] (const AIndex32& i) const;

    //Summation and subtraction operators

    Vector3f operator - (const Point2f& point) const;

    Point2f operator + (const Point2f& point) const;

    Point2f operator + (const Vector3f& vec) const;

    Point2f& operator += (const Point2f& point);

    Point2f& operator += (const Vector3f& vec);

    Point2f& operator -= (const Vector3f& vec);

    //assignment operator

    Point2f& operator = (const Point2f& point);

    Point2f& operator = (const Point3f& point);

    Point2f& operator = (const Point2i& point);

    //Comparison Operators

    bool operator == (const Point2f& point) const;

    bool operator != (const Point2f& point) const;

    bool operator < (const Point2f& point) const;

    bool operator > (const Point2f& point) const;

    //Operators with scalar arguments

    Point2f operator * (const AReal32& scalar) const;

    Point2f& operator *= (const AReal32& scalar);

    Point2f operator / (const AReal32& scalar) const;

    Point2f& operator /= (const AReal32& scalar);

    Point2f operator - () const;

};
//---------------------------------------------------------------------------------------
//Exports specifiers for STL containers

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
template class AN_MATHS_API std::allocator<Point2f>;
template class AN_MATHS_API std::vector<Point2f>;
#endif

typedef std::vector<Point2f> CPoint2fList;
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
