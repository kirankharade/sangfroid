/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class AN_MATHS_API Point2i
{
public:

    union
    {
        AInt32 v[2];

        struct
        {
            AInt32 x;
            AInt32 y;
        };
    };


public:

    Point2i();

    Point2i(const AInt32 v[2]);

    Point2i(const AInt32& a, const AInt32& b);

    Point2i(const Point2i& point);

    void set(const AInt32& a, const AInt32& b);

    void set(const Point2i& point);

    AReal32 dist(const Point2i& point) const;

    //Index Operators

    AInt32& operator [] (const AIndex32& i);

    const AInt32& operator [] (const AIndex32& i) const;

    //assignment operator

    Point2i& operator = (const Point2i& point);

    Point2i& operator = (const Point3f& point);

    //Comparison Operators

    bool operator == (const Point2i& point) const;

    bool operator != (const Point2i& point) const;

    bool operator < (const Point2i& point) const;

    //Operators with scalar arguments

    Point2i operator * (const AReal32& scalar) const;

    Point2i& operator *= (const AReal32& scalar);

    Point2i operator / (const AReal32& scalar) const;

    Point2i& operator /= (const AReal32& scalar);

    Point2i operator - () const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
