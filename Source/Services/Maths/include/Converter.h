/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "Point3f.h"
#include "Ray.h"
#include "LineSegment.h"
#include "Plane.h"
#include "PointTriangle.h"
#include <cfloat>
#include <cmath>
#include <vector>

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

class AN_MATHS_API Converter
{
public:

    static Point2f convertTo2f(const Point2i& point);

    static Point2f convertTo2f(const Point3f& point);

    static Point2i convertTo2i(const Point2f& point);

    static Point2i convertTo2i(const Point3f& point);

    static Point3f convertTo3f(const Point2f& point);

    static Point3f convertTo3f(const Point2i& point);

    static std::vector<Point2f> convertTo2f(const std::vector<Point2i>& points);

    static std::vector<Point2f> convertTo2f(const std::vector<Point3f>& points);

    static std::vector<Point2i> convertTo2i(const std::vector<Point2f>& points);

    static std::vector<Point2i> convertTo2i(const std::vector<Point3f>& points);

    static std::vector<Point3f> convertTo3f(const std::vector<Point2f>& points);

    static std::vector<Point3f> convertTo3f(const std::vector<Point2i>& points);

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
