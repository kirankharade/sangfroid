/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class LineSegment;
class Line;
//---------------------------------------------------------------------------------------

class AN_MATHS_API PointTriangle
{
public:

    Point3f Vertices[3];

public:

    PointTriangle();

    PointTriangle(const Point3f v[3]);

    PointTriangle(const Point3f& a, const Point3f& b, const Point3f& c);

    PointTriangle(const PointTriangle& triangle);

    void set(const Point3f& a, const Point3f& b, const Point3f& c);

    void set(const PointTriangle& triangle);

    Vector3f normal() const;

    AReal32 area() const;

    Point3f circumCenter() const;

    void reverseOrientation();

    bool isInside(const Point3f& point) const;

    bool isIntersecting(const LineSegment* lineSegment) const;

    bool isIntersecting(const Line* line) const;

    LineSegment edge(const AIndex32& i) const;

    //Index Operators

    Point3f& operator [] (const AIndex16& i);

    const Point3f& operator [] (const AIndex32& i) const;

    //assignment operator

    PointTriangle& operator = (const PointTriangle& triangle);

    //Comparison Operators

    bool operator == (const PointTriangle& triangle) const;

    bool operator != (const PointTriangle& triangle) const;
};
//---------------------------------------------------------------------------------------
//Exports specifiers for STL containers

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
template class AN_MATHS_API std::allocator<PointTriangle>;
template class AN_MATHS_API std::vector<PointTriangle>;
#endif

typedef std::vector<PointTriangle> PointTriangleList;
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
