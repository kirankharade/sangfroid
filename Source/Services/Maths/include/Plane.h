/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "MathsEnums.h"
#include "Point3f.h"
#include "Vector3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Line;
//IntersectionList;
//---------------------------------------------------------------------------------------

class AN_MATHS_API Plane
{
public:

    Point3f    m_point;
    Vector3f   m_normal;

public:

    Plane();

    Plane(const Point3f& point, const Vector3f& normal);

    Plane(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D);

    Plane(const Point3f& p1, const Point3f& p2, const Point3f& p3);

    Plane(const Plane& plane);

    virtual ~Plane();

    void set(const Point3f& point, const Vector3f& normal);

    void set(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D);

    void set(const Point3f& p1, const Point3f& p2, const Point3f& p3);

    Point3f getPoint() const;

    Vector3f getNormal() const;

    void getCoefficients(AReal32& A, AReal32& B, AReal32& C, AReal32& D);

    AReal32 getCoefficientD() const;

    bool isIntersecting(const Line*& pLine) const;

    bool isIntersecting(const Line*& pLine, Point3f& intersectionPoint) const;

    Point3f closestPoint(const Point3f& point) const;

    AReal32 distFromPoint(const Point3f& point) const;

    bool isParallel(const Plane& plane) const;

    EPlaneSideQualification getQualification(const Point3f& point, const AReal32 tolerance = TOLERANCE_32) const;

    bool Intersection(const Plane& plane1, const Plane& plane2, Point3f& intersectionPoint) const;

    bool Intersection(const Plane& plane, Point3f& intersectionPoint, Vector3f& intersectionlineDir) const;

    //TO DO:

    //void getSquarePlaneCorners(const Point3f& ptProjected, const AReal32& squareSize, Point3f*& corners) const;

    //void getSquarePlaneCorners(const Vector3f& yAxisProjected, const AReal32& squareSize, Point3f*& corners) const;

    //bool isIntersecting(const Box* pBox) const;

    //bool isIntersecting(const Box* pBox, IntersectionList*& list) const;

    //Assignment operator

    Plane& operator = (const Plane& plane);

    //Comparison Operators

    bool operator == (const Plane& plane) const;

    bool operator != (const Plane& plane) const;

    bool operator < (const Plane& plane) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
