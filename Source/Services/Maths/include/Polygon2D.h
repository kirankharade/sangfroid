/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "PointTriangle.h"
#include "ConvexHull2D.h"
#include "LineSegment2D.h"
#include <vector>

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

class AN_MATHS_API Polygon2D
{

public:

    enum BoundaryQualification
    {
        InsideBoundary,
        InsideHullBoundary,
        OutsideBoundary,
        OutsideHullBoundary,
        OnBoundary,
        OnHullBoundary,
        UndefinedBoundaryQualification
    };

private:

    CPoint2fList             m_inputPoints;
    ConvexHull2D             m_convexHull;
    PointTriangleList        m_interiorTriangles;
    PointTriangleList        m_exteriorTriangles;
    CLineSegment2DList       m_validEdges;
    CLineSegment2DList       m_allEdges;
    bool                     m_bIsSelfIntersecting;
    bool                     m_bIsConcave;
    Point2f                  m_centroidPoint;

    static const AReal32     ms_exteriorContainerScale;

public:

    Polygon2D();

    Polygon2D(const std::vector<Point2f>& points);

    Polygon2D(const std::vector<Point2i>& points);

    Polygon2D(const Polygon2D& polygon);

    virtual ~Polygon2D();

    void set(const std::vector<Point2f>& points);

    void add(const std::vector<Point2f>& points);

    void add(const Point2f& point);

    void remove(const Point2f& point);

    void removeAt(const AIndex32& index);

    bool isSelfIntersecting() const;

    bool isConcave() const;

    bool isInside(const Point2i& point) const;

    bool isInside(const Point2f& point) const;

    bool isInside(const LineSegment2D& lineSegment) const;

    bool isPolygonOnBothSidesOfSegment(const LineSegment2D& lineSegment) const;

    bool isIntersectingEdges(const LineSegment2D& lineSegment) const;

    Polygon2D::BoundaryQualification qualifyAgainstBoundary(const Point2f& point) const;

    Polygon2D::BoundaryQualification qualifyAgainstBoundary(const LineSegment2D& point) const;

    Polygon2D::BoundaryQualification qualifyAgainstBoundary(const PointTriangle& point) const;

    AReal32 getArea() const;

    AReal32 getPerimeter() const;

    Point2f getCentroid() const;

    AUInt32 getPointCount() const;

    const std::vector<Point2f>& getPoints() const;

    const ConvexHull2D& getConvexHull() const;

    const std::vector<LineSegment2D>& getAllEdges() const;

    const std::vector<PointTriangle>& getInteriorTriangles() const;

    std::vector<PointTriangle> getExteriorTriangles(const AReal32& containerScale) const;

    Polygon2D getScaled(const AReal32& scaleX, const AReal32& scaleY) const;

    Polygon2D getRotated(const Point2f& rotationCentre, const AReal32& angleDeg) const;

    Polygon2D getTranslated(const AReal32& dx, const AReal32& dy) const;

    //Assignment operator

    Polygon2D& operator = (const Polygon2D& polygon);

    //Comparison Operators

    bool operator == (const Polygon2D& rect) const;

    bool operator != (const Polygon2D& rect) const;

private:

    void update();

    void init();

    void checkIfSelfIntersecting();

    void checkIfConcave();

    void generateAllEdges();

    void findTriangles();

    void findExteriorTrianglesForConcaveCase();

    void findExteriorTrianglesForConvexCase();

    void findInteriorTrianglesForConcaveCase();

    void findInteriorTrianglesForConvexCase();

    void copy(const Polygon2D& polygon);

    bool isSegmentCreatingTwoValidChildrenPolygons(const LineSegment2D& lineSegment) const;

    bool isSegmentCreatingTwoValidChildrenPolygons(const AIndex32& ev1, const AIndex32& ev2) const;

    void addTrianglesExteriorToConvexHull();

    void getTrianglesExteriorToConvexHull(const AReal32& outerBoundaryOffset, std::vector<PointTriangle>& triangles) const;

    void findInnerTrianglesBySubDivision(std::vector<PointTriangle>& triangles);

    bool getValidDivider(AIndex32& idx1, AIndex32& idx2) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
