/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "Matrix4f.h"
#include <vector>

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Plane;
class Line;
class Ray;
class LineSegment;
class PointTriangle;
class CPointIntersectionInfo;
//---------------------------------------------------------------------------------------

class AN_MATHS_API Box
{
protected:

    Point3f    m_origin;
    Vector3f   m_xAxis;
    Vector3f   m_yAxis;
    Vector3f   m_zAxis;
    AReal32     m_width;
    AReal32     m_height;
    AReal32     m_depth;

public:

    Box();

    Box(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection,
        const AReal32& width, const AReal32& height, const AReal32& depth);

    Box(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);

    Box(const AReal32& width, const AReal32& height, const AReal32& depth);

    Box(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection, const AReal32& size);

    Box(const Point3f& origin, const AReal32& size);

    Box(const AReal32& size);

    Box(const Box& box);

    virtual ~Box();

    void set(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection,
        const AReal32& width, const AReal32& height, const AReal32& depth);

    void set(const Point3f& origin, const Vector3f& xDirection, const Vector3f& yDirection, const AReal32& size);

    void setOrigin(const Point3f& origin);

    void setDirections(const Vector3f& xDirection, const Vector3f& yDirection);

    void setXDirection(const Vector3f& xDirection);

    void setYDirection(const Vector3f& yDirection);

    void setSize(const AReal32& size);

    void setWidth(const AReal32& width);

    void setHeight(const AReal32& height);

    void setDepth(const AReal32& width);

    Point3f getOrigin() const;

    Vector3f getXDirection() const;

    Vector3f getYDirection() const;

    AReal32 getWidth() const;

    AReal32 getHeight() const;

    AReal32 getDepth() const;

    Point3f getCentre() const;

    AReal32 getVolume() const;

    AReal32 getSurfaceArea() const;

    Point3f* getCorners() const;

    LineSegment* getEdges() const;

    PointTriangle* getFaceTriangles() const;

    Plane* getPlanes() const;

    virtual bool isIntersecting(const Plane* pPlane) const;

    virtual CPointIntersectionInfo* getIntersections(const Plane* pPlane) const;

    virtual bool isIntersecting(const Line* pLine) const;

    virtual CPointIntersectionInfo* getIntersections(const Line* pLine) const;

    virtual bool isIntersecting(const LineSegment* pLineSegment) const;

    virtual CPointIntersectionInfo* getIntersections(const LineSegment* pLineSegment) const;

    virtual bool isIntersecting(const Ray* pRay) const;

    virtual CPointIntersectionInfo* getIntersections(const Ray* pRay) const;

    virtual bool isInside(const Point3f& point, const AReal32& tolerance = TOLERANCE_32) const;

    Box* transform(const Matrix4f& matrix) const;

    //Assignment operator

    Box& operator = (const Box& box);

    //Comparison Operators

    bool operator == (const Box& box) const;

    bool operator != (const Box& box) const;

    bool operator < (const Box& box) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
