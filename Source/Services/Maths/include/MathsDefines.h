/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsNamespaces.h"
#include <limits.h>
#include <float.h>

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//------------//
// Math Types
//------------//

#define TOLERANCE_32            0.000001f
#define TOLERANCE_64            0.00000001
#define STD_TOLERANCE			0.000001
#define TOLERANCE				0.0001f
#define TOLERANCE_SQUARED		0.00000001

#define PI_32                   3.14159265359f
#define PI_64                   3.1415926535897932384626433832795028841971693993751
#define PI						3.1415926535897932384626433832795

#define PI_32_RECIPROCAL        0.31830988618f
#define PI_64_RECIPROCAL        0.31830988618379067153776752674508

#define PI_32_HALF              1.57079632679f
#define PI_64_HALF              1.5707963267948966192313216916395

#define PI_32_QUARTER           0.78539816340f
#define PI_64_QUARTER           0.78539816339744830961566084581975

#define PI_32_TWICE             6.28318530718f
#define PI_64_TWICE             6.283185307179586476925286766558

#define SQRT_2_64               1.4142135623730950488016887242097
#define SQRT_3_64               1.7320508075688772935274463415059

#define SQRT_2_64_RECIPROCAL    0.70710678118654752440084436210485
#define SQRT_3_64_RECIPROCAL    0.57735026918962576450914878050196

#define MAX_REAL32              FLT_MAX
#define MAX_REAL64              DBL_MAX

#define DEG_TO_RAD64(x)         (0.017453292519943295769236907684883 * (x))
#define RAD_TO_DEG64(x)         (57.295779513082320876798154814114 * (x))
#define DEG2RAD(x)				(0.01745329251994329577 * (x))
#define RAD2DEG(x)				(57.2957795130823208768 * (x))

//---------------------------------------------------------------------------------------

//-------------//
// Math Macros
//-------------//

#define MAX(a, b)				(((a) > (b)) ? (a) : (b))
#define MIN(a, b)				(((a) < (b)) ? (a) : (b))

#define ABS(a)                  (((a) < 0) ? -(a) : (a))

#define ISZERO_32(x)            (fabs(x) <= TOLERANCE_32)
#define ISZERO_64(x)            (fabs(x) <= TOLERANCE_64)

#define ISNOTZERO_32(x)         (fabs(x) > TOLERANCE_32)
#define ISNOTZERO_64(x)         (fabs(x) > TOLERANCE_64)

#define EQ_32(x, y)             (fabs(x - y) < TOLERANCE_32)
#define GTEQ_32(x, y)           ((x >= y) || (EQ_32(x, y)))
#define LTEQ_32(x, y)           ((x <= y) || (EQ_32(x, y)))
#define LT_32(x, y)             (x < (y - TOLERANCE_32))
#define GT_32(x, y)             (x > (y + TOLERANCE_32))

#define EQ_64(x, y)             (fabs(x - y) < TOLERANCE_64)
#define GTEQ_64(x, y)           ((x >= y) || (EQ_64(x, y)))
#define LTEQ_64(x, y)           ((x <= y) || (EQ_64(x, y)))
#define LT_64(x, y)             (x < (y - TOLERANCE_64))
#define GT_64(x, y)             (x > (y + TOLERANCE_64))

#define EQT(x, y, tolerance)    (fabs(x - y) < tolerance)
#define GTEQT(x, y, tolerance)  ((x >= y) || (EQT(x, y, tolerance)))
#define LTEQT(x, y, tolerance)  ((x <= y) || (EQT(x, y, tolerance)))
#define LTT(x, y, tolerance)    (x < (y - tolerance))
#define GTT(x, y, tolerance)    (x > (y + tolerance))

//---------------------------------------------------------------------------------------

template <typename T>
inline T MAX3(T a, T b, T c)
{
    T x = MAX(a, b);
    return MAX(x, c);
}

template <typename T>
inline T MIN3(T a, T b, T c)
{
    T x = MIN(a, b);
    return MIN(x, c);
}

template <typename T>
inline T MAX4(T a, T b, T c, T d)
{
    T x = MAX(a, b);
    T y = MAX(c, d);
    return MAX(x, y);
}

template <typename T>
inline T MIN4(T a, T b, T c, T d)
{
    T x = MIN(a, b);
    T y = MIN(c, d);
    return MIN(x, y);
}

template <typename T>
inline T CLAMP(T val, T low, T high)
{
    return MIN(MAX(val, low), high);
}

template <typename T>
inline int ROUND(T val)
{
    return (int)(val + (T)0.5);
}

inline double ToDegrees(double radians)
{
    return RAD2DEG(radians);
}

inline double ToRadians(double degrees)
{
    return DEG2RAD(degrees);
}

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
