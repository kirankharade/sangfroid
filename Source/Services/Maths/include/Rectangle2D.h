/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2i.h"
#include "Point2f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------
//Forward declarations
class CRectangle2Di;
//---------------------------------------------------------------------------------------

class AN_MATHS_API Rectangle2D
{
private:

    AReal32    m_left;
    AReal32    m_right;
    AReal32    m_top;
    AReal32    m_bottom;

    AReal32   m_zAxisRotationDeg;

public:

    Rectangle2D();

    Rectangle2D(const AReal32& left, const AReal32& right, const AReal32& top, const AReal32& bottom);

    Rectangle2D(const AInt32& left, const AInt32& right, const AInt32& top, const AInt32& bottom);

    Rectangle2D(const Point2f& reference, const AReal32& width, const AReal32& height);

    Rectangle2D(const Point2i& reference, const AInt32& width, const AInt32& height);

    Rectangle2D(const Rectangle2D& rect);

    virtual ~Rectangle2D();

    void set(const AReal32& left, const AReal32& right, const AReal32& top, const AReal32& bottom);

    void set(const Point2f& reference, const AReal32& width, const AReal32& height);

    void set(const AInt32& left, const AInt32& right, const AInt32& top, const AInt32& bottom);

    void set(const Point2i& reference, const AInt32& width, const AInt32& height);

    AReal32 getLeft() const;

    AReal32 getRight() const;

    AReal32 getTop() const;

    AReal32 getBottom() const;

    Point2f getLeftTop() const;

    Point2f getLeftBottom() const;

    Point2f getRightBottom() const;

    Point2f getRightTop() const;

    AReal32 getArea() const;

    AReal32 getPerimeter() const;

    AReal32 getWidth() const;

    void setWidth(const AReal32& width);

    AReal32 getHeight() const;

    void setHeight(const AReal32& height);

    AReal32 getZAxisRotation() const;

    void setZAxisRotation(const AReal32& zRotationDeg);

    bool isInside(const Point2i& point) const;

    bool isInside(const Point2f& point) const;


    //Assignment operator

    Rectangle2D& operator = (const Rectangle2D& rect);

    //Comparison Operators

    bool operator == (const Rectangle2D& rect) const;

    bool operator != (const Rectangle2D& rect) const;

private:

    void init();

    void checkValidity();

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
