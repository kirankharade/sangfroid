/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point2f.h"
#include "Vector3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class Line;
class LineSegment;
class Ray;
//---------------------------------------------------------------------------------------

class AN_MATHS_API LineSegment2D
{
public:

    enum IntersectionClassification
    {
        Intersecting,
        IntersectingAtEnds,
        CoIncident,
        NonCoIncidentParallel,
        NonIntersecting,
        undefinedIntersection
    };

protected:

    Point2f    m_firstPoint;
    Point2f    m_secondPoint;

public:

    LineSegment2D();

    LineSegment2D(const Point2f& firstPoint, const Vector3f& direction, const AReal32& length);

    LineSegment2D(const Point2f& firstPoint, const Point2f& secondPoint);

    LineSegment2D(const LineSegment2D& lineSegment);

    LineSegment2D(const LineSegment& lineSegment);

    virtual ~LineSegment2D();

    void set(const Point2f& firstPoint, const Vector3f& direction, const AReal32& length);

    virtual void set(const Point2f& firstPoint, const Point2f& secondPoint);

    Point2f getFirstPoint() const;

    Point2f getSecondPoint() const;

    Vector3f getDirection() const;

    AReal32 getLength() const;

    IntersectionClassification classifyByIntersection(const LineSegment2D* lineSegment, Point2f& intersectionPoint) const;

    IntersectionClassification classifyByIntersection(const LineSegment2D* lineSegment) const;

    IntersectionClassification classifyByIntersection(const Line* line, Point2f& intersectionPoint) const;

    IntersectionClassification classifyByIntersection(const Line* line) const;

    IntersectionClassification classifyByIntersection(const Ray* ray, Point2f& intersectionPoint) const;

    IntersectionClassification classifyByIntersection(const Ray* ray) const;

    bool passesThrough(const Point2f& point, const AReal32 tolerance = TOLERANCE_32) const;

    Point2f closestPoint(const Point2f& point) const;

    AReal32 distFromPoint(const Point2f& point) const;

    //Assignment operator

    LineSegment2D& operator = (const LineSegment2D& lineSegment);

    //Comparison Operators

    bool operator == (const LineSegment2D& lineSegment) const;

    bool operator != (const LineSegment2D& lineSegment) const;

    bool operator < (const LineSegment2D& lineSegment) const;

};
//---------------------------------------------------------------------------------------
//Exports specifiers for STL containers

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
template class AN_MATHS_API std::allocator<LineSegment2D>;
template class AN_MATHS_API std::vector<LineSegment2D>;
#endif

typedef std::vector<LineSegment2D> CLineSegment2DList;
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
