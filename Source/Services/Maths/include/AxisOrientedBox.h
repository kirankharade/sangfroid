/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "Matrix4f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Plane;
class Line;
class Ray;
class LineSegment;
class Box;
class CPointIntersectionInfo;
class PointTriangle;
//---------------------------------------------------------------------------------------

class AN_MATHS_API AxisOrientedBox
{
protected:

    Point3f    m_minPoint;
    Point3f    m_maxPoint;

public:

    AxisOrientedBox();

    AxisOrientedBox(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);

    AxisOrientedBox(const Point3f& minPoint, const Point3f& maxPoint);

    AxisOrientedBox(const Point3f& origin, const AReal32& size);

    AxisOrientedBox(const AxisOrientedBox& box);

    virtual ~AxisOrientedBox();

    void set(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);

    void set(const Point3f& origin, const AReal32& size);

    void setMinCorner(const Point3f& minPoint);

    void setMaxCorner(const Point3f& maxPoint);

    Point3f getMinCorner() const;

    Point3f getMaxCorner() const;

    bool IsValid() const;

    AReal32 getWidth() const;

    AReal32 getHeight() const;

    AReal32 getDepth() const;

    Point3f getCentre() const;

    AReal32 getVolume() const;

    AReal32 getSurfaceArea() const;

    Point3f* getCorners() const;

    LineSegment* getEdges() const;

    PointTriangle* getFaceTriangles() const;

    Plane* getPlanes() const;

    bool isIntersecting(const Plane* pPlane) const;

    CPointIntersectionInfo* getIntersections(const Plane* pPlane) const;

    bool isIntersecting(const Line* pLine) const;

    CPointIntersectionInfo* getIntersections(const Line* pLine) const;

    bool isInside(const Point3f& point, const AReal32 tolerance = TOLERANCE_32) const;

    Box* transform(const Matrix4f& matrix) const;

    //Assignment operator

    AxisOrientedBox& operator = (const AxisOrientedBox& box);

    //Comparison Operators

    bool operator == (const AxisOrientedBox& box) const;

    bool operator != (const AxisOrientedBox& box) const;

    bool operator < (const AxisOrientedBox& box) const;

    //Addition operators

    AxisOrientedBox operator + (const AxisOrientedBox& box) const;

    AxisOrientedBox& operator += (const AxisOrientedBox& box);

    static AxisOrientedBox CalculateBox(const Maths::Point3f* points, const AInt32& pointCount);

    static AxisOrientedBox CalculateBox(const Maths::CPoint3fList points);

protected:

    void validate();

    void getFacePlanes(Plane*& facePlanes) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
