/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

//---------------------------------------------------------------------------------------
#include "MathsIncludes.h"
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

enum AN_MATHS_API ECoordSystemHandedness
{
    ECoordSystemRightHanded = 0,
    ECoordSystemLeftHanded
};

enum AN_MATHS_API EScreenConventionPosYAxis
{
    EScreenConventionPosYAxisUpPositive = 0,
    EScreenConventionPosYAxisUpNegative
};

enum AN_MATHS_API EPlaneSideQualification
{
    EPlaneSideFront = 0,
    EPlaneSideOnPlane,
    EPlaneSideBack,
};

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
