/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include <vector>
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

#define X_AXIS Vector3f(1, 0, 0)
#define Y_AXIS Vector3f(0, 1, 0)
#define Z_AXIS Vector3f(0, 0, 1)  

//---------------------------------------------------------------------------------------

class AN_MATHS_API Vector3f
{
public:

    union
    {
        AReal32 v[3];

        struct
        {
            AReal32 x;
            AReal32 y;
            AReal32 z;
        };
        struct
        {
            AReal32 i;
            AReal32 j;
            AReal32 k;
        };
    };

public:

    Vector3f();

    Vector3f(const AReal32 v[3]);

    Vector3f(const AReal32& a, const AReal32& b, const AReal32& c);

    Vector3f(const Vector3f& vec);

    void set(const AReal32& a, const AReal32& b, const AReal32& c);

    void set(const Vector3f& vec);

    void normalize();

    Vector3f unitVec() const;

    AReal32 magnitude() const;

    AReal32 length() const;

    AReal32 squareLength() const;

    Vector3f cross(const Vector3f& v) const;

    AReal32 dot(const Vector3f& v) const;

    AReal32 angleDeg(const Vector3f& v) const;

    AReal32 angleRad(const Vector3f& v) const;

    Vector3f reverse() const;

    bool isNull() const;

    //Index Operators

    AReal32& operator [] (const AIndex32& index);

    const AReal32& operator [] (const AIndex32& index) const;

    //assignment operator

    Vector3f& operator = (const Vector3f& v);

    //Comparison Operators

    bool operator == (const Vector3f& v) const;

    bool operator != (const Vector3f& v) const;

    bool operator < (const Vector3f& v) const;

    bool operator > (const Vector3f& v) const;

    //Summation and subtraction operators

    Vector3f operator + (const Vector3f& v) const;

    Vector3f& operator += (const Vector3f& v);

    Vector3f operator - (const Vector3f& v) const;

    Vector3f& operator -= (const Vector3f& v);

    //Operators with scalar arguments

    Vector3f operator * (const AReal32& scalar) const;

    Vector3f& operator *= (const AReal32& scalar);

    Vector3f operator / (const AReal32& scalar) const;

    Vector3f& operator /= (const AReal32& scalar);

    Vector3f operator - () const;

};
//---------------------------------------------------------------------------------------
typedef AN_MATHS_API std::vector<Vector3f>   CVector3fList;
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
