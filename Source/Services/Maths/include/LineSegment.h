/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "Point3f.h"
#include "Vector3f.h"

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Plane;
//---------------------------------------------------------------------------------------

class AN_MATHS_API LineSegment
{
protected:

    Point3f    m_firstPoint;
    Point3f    m_secondPoint;

public:

    LineSegment();

    LineSegment(const Point3f& firstPoint, const Vector3f& direction, const AReal32& length);

    LineSegment(const Point3f& firstPoint, const Point3f& secondPoint);

    LineSegment(const LineSegment& lineSegment);

    virtual ~LineSegment();

    void set(const Point3f& firstPoint, const Vector3f& direction, const AReal32& length);

    virtual void set(const Point3f& firstPoint, const Point3f& secondPoint);

    Point3f getFirstPoint() const;

    Point3f getSecondPoint() const;

    Vector3f getDirection() const;

    AReal32 getLength() const;

    bool isIntersecting(const Plane* pPlane) const;

    bool isIntersecting(const Plane* pPlane, Point3f& intersectionPoint) const;

    bool passesThrough(const Point3f& point, const AReal32 tolerance = TOLERANCE_32) const;

    Point3f closestPoint(const Point3f& point) const;

    AReal32 distFromPoint(const Point3f& point) const;

    //Assignment operator

    LineSegment& operator = (const LineSegment& lineSegment);

    //Comparison Operators

    bool operator == (const LineSegment& lineSegment) const;

    bool operator != (const LineSegment& lineSegment) const;

    bool operator < (const LineSegment& lineSegment) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
