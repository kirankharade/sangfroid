/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsEnums.h"
#include "Point3f.h"
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class IIntersectionInfo
{
public:

    virtual AInt32 getIntersectionCount() const = 0;

    virtual ~IIntersectionInfo() {}
};
//---------------------------------------------------------------------------------------

class CIntersectionInfo : public IIntersectionInfo
{
public:

    CIntersectionInfo();

    virtual ~CIntersectionInfo() {}

    AInt32 getIntersectionCount() const;

protected:

    AInt32 m_intersectionCount;
};
//---------------------------------------------------------------------------------------

struct SPointIntersection
{
    AInt32      m_hitPrimitiveIndex;
    Point3f    m_intersectionPoint;
};
//---------------------------------------------------------------------------------------

class CPointIntersectionInfo : public CIntersectionInfo
{
public:

    CPointIntersectionInfo();

    virtual ~CPointIntersectionInfo() {}

    CPointIntersectionInfo(const AIndex32& intersectionCount, const SPointIntersection* pIntersections);

    const SPointIntersection* getIntersections() const;

    AIndex32 getHitPrimitiveIndex(const AIndex32& index) const;

    Point3f getIntersectionPoint(const AIndex32& index) const;

    void setIntersections(const AIndex32& intersectionCount, const SPointIntersection* pIntersections);

protected:

    SPointIntersection* m_pIntersections;
};
//---------------------------------------------------------------------------------------

SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
