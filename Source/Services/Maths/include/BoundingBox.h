/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "MathsIncludes.h"
#include "AxisOrientedBox.h"
#include <vector>

//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_START
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class Plane;
class Line;
class Ray;
//---------------------------------------------------------------------------------------

class AN_MATHS_API BoundingBox : public AxisOrientedBox
{
protected:

    bool    m_bIsInitialized;

public:

    BoundingBox();

    BoundingBox(const Point3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);

    BoundingBox(const Point3f& minPoint, const Point3f& maxPoint);

    BoundingBox(const std::vector<Point3f>& points);

    BoundingBox(const Point3f* points, const AUInt32 numPoints);

    BoundingBox(const Point3f& origin, const AReal32& size);

    BoundingBox(const BoundingBox& box);

    virtual ~BoundingBox();

    void Add(const std::vector<Point3f>& points);

    void Add(const Point3f* points, const AUInt32 numPoints);

    void Add(const Point3f& point);

    //Assignment operator

    BoundingBox& operator = (const BoundingBox& box);

    //Addition Operators

    BoundingBox operator + (const BoundingBox& box);

    BoundingBox& operator += (const BoundingBox& box);

    //Comparison Operators

    bool operator == (const BoundingBox& box) const;

    bool operator != (const BoundingBox& box) const;

    bool operator < (const BoundingBox& box) const;

};
//---------------------------------------------------------------------------------------
SANGFROID_MATHS_NAMESPACE_END
//---------------------------------------------------------------------------------------
