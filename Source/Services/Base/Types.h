/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "Namespaces.h"

//---------------------------------------------------------------------------------------
SANGFROID_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
// Base Types
//---------------------------------------------------------------------------------------

typedef int                 AInt32;
typedef unsigned int        AUInt32;

typedef long long           AInt64;
typedef unsigned long long  AUInt64;

typedef short               AInt16;
typedef unsigned short      AUInt16;

typedef char                AInt8;
typedef unsigned char       AUInt8;

typedef char                AByte;
typedef unsigned char       AUByte;

typedef float               AReal32;
typedef double              AReal64;

typedef char                AChar;
typedef wchar_t             AWChar;

typedef void                AVoid;
typedef void*               AVoidPtr;

typedef bool                ABool;

typedef short               AIndex16;
typedef int                 AIndex32;
typedef long long           AIndex64;

//---------------------------------------------------------------------------------------
SANGFROID_END_NAMESPACE
//---------------------------------------------------------------------------------------
