﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "BaseIncludes.h"
#include <vector>

using namespace std;

namespace Sangfroid
{

	/*-----------------------------------------------------------------------------------*/
	// Vector of vectors with minimum interfaces
	/*-----------------------------------------------------------------------------------*/

	template<typename T>
	class UltraVector
	{
	protected:

		std::vector<std::vector<T>> m_list;
		unsigned int m_count;
		unsigned int m_granularity;

	public:

		UltraVector(unsigned int granularity)
			: m_count(0)
			, m_granularity(granularity)
		{
			std::vector<T> v;
			m_list.push_back(v);
		}

		UltraVector()
			: m_count(0)
			, m_granularity(500000)
		{
			std::vector<T> v;
			m_list.push_back(v);
		}

		virtual ~UltraVector()
		{
			clear();
		}

		void clear()
		{
			for (std::vector<std::vector<T>>::iterator it = m_list.begin(); it != m_list.end(); it++)
			{
				(*it).clear();
			}
			m_list.clear();
		}

		void push_back(T item)
		{
			size_t size = m_list.size();
			if (size == 0)
			{
				m_list.push_back(std::vector<T>());
				size = m_list.size();
			}
			std::vector<T>& subList = m_list[size - 1];

			if (subList.size() >= m_granularity)
			{
				std::vector<T> newSubList;
				m_list.push_back(newSubList);
				m_list[size].push_back(item);
			}
			else
			{
				m_list[size - 1].push_back(item);
			}
			m_count++;
		}

		unsigned int size() const
		{
			return m_count;
		}

		const T& operator[](unsigned int index) const
		{
			try
			{
				//unsigned int sum = 0;
				//unsigned int count = 0;
				//for(std::vector<std::vector<T>>::const_iterator it = m_list.begin(); it != m_list.end(); it++)
				//{
				//    unsigned int sz = (*it).size();
				//    sum += sz;
				//    if(sum > index)
				//    {
				//        sum -= sz;
				//        unsigned int listIndex = index - sum;
				//        const T& t = (*it)[listIndex];
				//        break;
				//    }
				//    count++;
				//}

				unsigned int listIndex = index / m_granularity;
				unsigned int elemIndex = index - (listIndex * m_granularity);
				return (m_list[listIndex])[elemIndex];
			}
			catch (...)
			{
				throw std::exception("Invalid Index");
			}
			return *((*(m_list.begin())).begin());
		}

		T& operator[](unsigned int index)
		{
			try
			{
				unsigned int listIndex = index / m_granularity;
				unsigned int elemIndex = index - (listIndex * m_granularity);
				return (m_list[listIndex])[elemIndex];
			}
			catch (...)
			{
				throw std::exception("Invalid Index");
			}

			return *((*(m_list.begin())).begin());
		}

	};
	/*-----------------------------------------------------------------------------------*/

}
