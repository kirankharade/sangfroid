#include <iostream>
#include <fstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <fstream>
#include <cerrno>
#include <algorithm>
#include <iterator>
#include <cstring>

#include "FileUtilities.h"
#include "StringUtilities.h"
#include "dirent.h"

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

using namespace std;
using namespace Sangfroid::GenericUtils;

/*-----------------------------------------------------------------------------------*/

std::string FileUtilities::DirectoryFromFullFilePath(const std::string& fname)
{
	size_t pos = fname.find_last_of("\\/");
	return (std::string::npos == pos)
		? ""
		: fname.substr(0, pos);
}
/*-----------------------------------------------------------------------------------*/

std::string FileUtilities::FileNameFromFilePath(const std::string& path)
{
	auto filename = path;
	const size_t last_slash_idx = filename.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		filename.erase(0, last_slash_idx + 1);
	}
	return filename;
}
/*-----------------------------------------------------------------------------------*/

std::string FileUtilities::FileNameFromFilePath_WithoutExt(const std::string& path)
{
	auto filename = path;
	const size_t last_slash_idx = filename.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		filename.erase(0, last_slash_idx + 1);
	}
	// Remove extension if present.
	const size_t period_idx = filename.rfind('.');
	if (std::string::npos != period_idx)
	{
		filename.erase(period_idx);
	}
	return filename;
}
/*-----------------------------------------------------------------------------------*/

std::vector<std::wstring> FileUtilities::ListDirs_WideChar(const std::wstring& parentDir, const std::wstring& nameContains)
{
	struct wdirent *entry = nullptr;
	std::vector<std::wstring> directories;
	WDIR* d = wopendir(parentDir.c_str());
	if (d = nullptr)
	{
		while ((entry = wreaddir(d)) != NULL)
		{
			if (entry->d_type == DT_DIR)
			{
				std::wstring name = std::wstring(entry->d_name);
				if (name.find(nameContains) != std::wstring::npos)
				{
					directories.push_back(name);
				}
			}
		}
		wclosedir(d);
	}
	return directories;
}
/*-----------------------------------------------------------------------------------*/

std::string FileUtilities::CurrentDirectory()
{
	char currentPath[FILENAME_MAX];

	if (!GetCurrentDir(currentPath, sizeof(currentPath)))
	{
		return std::string("");
	}
	return std::string(currentPath);
}
/*-----------------------------------------------------------------------------------*/

std::vector<std::string> FileUtilities::ListDirs(const std::string& parentDir, const std::string& nameContains)
{
	struct dirent *entry = nullptr;
	std::vector<std::string> directories;
	DIR* d = opendir(parentDir.c_str());
	if (d = nullptr)
	{
		while ((entry = readdir(d)) != NULL)
		{
			if (entry->d_type == DT_DIR)
			{
				std::string name = std::string(entry->d_name);
				if (name.find(nameContains) != std::string::npos)
				{
					directories.push_back(name);
				}
			}
		}
		closedir(d);
	}
	return directories;
}
/*-----------------------------------------------------------------------------------*/

std::string FileUtilities::FullPath(const std::string& relativePath)
{
	if (relativePath.length() == 0)
	{
		return std::string();
	}
	char full[_MAX_PATH];

	char* fp = _fullpath(full, relativePath.c_str(), _MAX_PATH);

	if (fp != NULL)
	{
		return std::string(fp);
	}
	return std::string();
}
/*-----------------------------------------------------------------------------------*/

bool FileUtilities::CheckFileExists(const char* filename)
{
	struct stat buffer;
	return (stat(filename, &buffer) == 0);
}
/*-----------------------------------------------------------------------------------*/

bool FileUtilities::RemoveFile(const char* filename)
{
	if (FileUtilities::CheckFileExists(filename))
		return std::remove(filename) == 0;
	return false;
}
/*-----------------------------------------------------------------------------------*/

void FileUtilities::FileCopy(const std::string& from, const std::string& to)
{
	std::ifstream is(from, ios::in | ios::binary);
	std::ofstream os(to, ios::out | ios::binary);

	std::copy(std::istream_iterator<unsigned char>(is),
		std::istream_iterator<unsigned char>(),
		std::ostream_iterator<unsigned char>(os));

	is.close();
	os.close();
}
/*-----------------------------------------------------------------------------------*/

std::string FileUtilities::GetUniqueTimestampedFile(
	const wchar_t* directory,
	const char* filePrefix,
	const char* extension)
{
	const std::wstring currentTimeWString = StringUtilities::GetCurrentTimeString_WideChar();
	const std::wstring currentTimeStdString(currentTimeWString.c_str());
	const std::wstring wFilePrefix = StringUtilities::ConvertToWideChar(filePrefix);
	const std::wstring wExtension = StringUtilities::ConvertToWideChar(extension);

	const std::wstring logFilename = std::wstring(directory) + wFilePrefix + currentTimeStdString + wExtension;

	return std::string(StringUtilities::ConvertToChar(logFilename.c_str()));
}
/*-----------------------------------------------------------------------------------*/

void FileUtilities::CreateFolder(const std::string& directory)
{
	BOOL status = CreateDirectory(directory.c_str(), NULL);
	auto errorCode = GetLastError();
	if (FALSE == status)
	{
		throw std::exception("Unable to create TestOutput directory");
	}
}
/*-----------------------------------------------------------------------------------*/

bool FileUtilities::CompareFileTexts(const char* fileA, const char* fileB)
{
	if (!FileUtilities::CheckFileExists(fileA) || !FileUtilities::CheckFileExists(fileB))
	{
		throw new std::invalid_argument("One or both input log files not found");
	}

	const char *bufferA = FileUtilities::GetBuffer(fileA);
	auto linesA = StringUtilities::ConvertBufferToLines(bufferA);
	delete bufferA;

	const char *bufferB = FileUtilities::GetBuffer(fileB);
	auto linesB = StringUtilities::ConvertBufferToLines(bufferB);
	delete bufferB;

	if (linesA.size() != linesB.size())
	{
		//Logger::WriteMessage("FileUtilities::CompareFileTexts() - File sizes unequal");
		return false;
	}

	for (unsigned int ii = 0; ii < linesA.size(); ++ii)
	{
		if (linesA[ii] != linesB[ii])
		{
			const std::string eMsg = std::string("FileUtilities::CompareFileTexts() - Mismatch at line ") + std::to_string(ii);
			//Logger::WriteMessage(eMsg.c_str());
			return false;
		}
	}

	return true;
}
/*-----------------------------------------------------------------------------------*/

const char* FileUtilities::GetBuffer(const char* filePath)
{
	ifstream file(filePath);

	std::stringstream buffer;
	buffer << file.rdbuf();
	file.close();
	_flushall();
	std::string str = buffer.str();
	char* txt = new char[str.size() + 1];
	strcpy(txt, str.c_str());

	return txt;
}
/*-----------------------------------------------------------------------------------*/

const char* FileUtilities::GetBuffer(const std::string& filePath)
{
	return GetBuffer(filePath.c_str());
}
/*-----------------------------------------------------------------------------------*/

