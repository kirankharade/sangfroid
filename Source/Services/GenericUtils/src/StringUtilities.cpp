#include <iostream>
#include <fstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <fstream>
#include <cerrno>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <chrono>
#include <ctime>
#include <iosfwd>

#include "StringUtilities.h"
#include "dirent.h"

using namespace std;
using namespace Sangfroid::GenericUtils;

/*-----------------------------------------------------------------------------------*/

const wchar_t* StringUtilities::ConvertToWideChar(const char* str)
{
	const size_t cSize = strlen(str) + 1;
	wchar_t* convertedStr = new wchar_t[cSize];
	mbstowcs(convertedStr, str, cSize);
	return convertedStr;
}
/*-----------------------------------------------------------------------------------*/

const char* StringUtilities::ConvertToChar(const wchar_t* str)
{
	const size_t cSize = wcslen(str) + 1;
	char* convertedStr = new char[cSize];
	wcstombs(convertedStr, str, cSize);
	return convertedStr;
}
/*-----------------------------------------------------------------------------------*/

template <typename T>
std::wstring StringUtilities::to_string_with_width_WideChar(const T a_value, const int n)
{
	std::wostringstream out;
	out.width(n);
	out.fill(L'0');
	out << a_value;
	return out.str();
}
/*-----------------------------------------------------------------------------------*/

template <typename T>
std::string StringUtilities::to_string_with_width(const T a_value, const int n)
{
	std::ostringstream out;
	out.width(n);
	out.fill('0');
	out << a_value;
	return out.str();
}
/*-----------------------------------------------------------------------------------*/

template <typename T>
T StringUtilities::from_string(const std::string& s)
{
	std::istringstream i(s);
	T x;
	if (!(i >> x))
		return 0;
	return x;
}
/*-----------------------------------------------------------------------------------*/

template <typename T>
T StringUtilities::from_string_WideChar(const std::wstring& s)
{
	std::wistringstream i(s);
	T x;
	if (!(i >> x))
		return 0;
	return x;
}
/*-----------------------------------------------------------------------------------*/

std::wstring StringUtilities::GetCurrentTimeString_WideChar()
{
	//Please, remember this will be UK time, not Indian time...:-)
	//Timezone not applied and not necessary
	//...................
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	time_t nowt = std::chrono::system_clock::to_time_t(now);
	tm *t = gmtime(&nowt);
	std::wstring str = to_string_with_width_WideChar(std::to_wstring(1900 + t->tm_year), 4) + L"_" +
		to_string_with_width_WideChar(std::to_wstring(1 + t->tm_mon), 2) + L"_" +
		to_string_with_width_WideChar(std::to_wstring(t->tm_mday), 2) + L"_" +
		to_string_with_width_WideChar(std::to_wstring(t->tm_hour), 2) +
		to_string_with_width_WideChar(std::to_wstring(t->tm_min), 2) +
		to_string_with_width_WideChar(std::to_wstring(t->tm_sec), 2);
	return str;
}
/*-----------------------------------------------------------------------------------*/

std::string StringUtilities::GetCurrentTimeString()
{
	//Please, remember this will be UK time, not Indian time...:-)
	//Timezone not applied and not necessary
	//...................
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	time_t nowt = std::chrono::system_clock::to_time_t(now);
	tm *t = gmtime(&nowt);
	std::string str = to_string_with_width(std::to_string(1900 + t->tm_year), 4) + "_" +
		to_string_with_width(std::to_string(1 + t->tm_mon), 2) + "_" +
		to_string_with_width(std::to_string(t->tm_mday), 2) + "_" +
		to_string_with_width(std::to_string(t->tm_hour), 2) +
		to_string_with_width(std::to_string(t->tm_min), 2) +
		to_string_with_width(std::to_string(t->tm_sec), 2);
	return str;
}
/*-----------------------------------------------------------------------------------*/

std::vector<std::string> StringUtilities::ConvertBufferToLines(const char* buffer)
{
	std::vector<std::string> lines;
	const char* tempPtr = buffer;
	std::string line;

	size_t len = strlen(buffer);
	size_t count = 0;

	while ((tempPtr != '\0') && (tempPtr != nullptr) && (count <= len))
	{
		char c = *tempPtr;
		if (c == '\n')
		{
			lines.push_back(line);
			line.clear();
		}
		else
		{
			line += c;
		}
		tempPtr++;
		count++;
	}
	return lines;
}
/*-----------------------------------------------------------------------------------*/

std::string StringUtilities::ReplaceSubstring(std::string source, std::string search, std::string replace)
{
	while (source.find(search) != std::string::npos)
	{
		auto position = source.find(search);
		source.replace(source.begin() + position, source.begin() + position + search.size(), replace);
	}
	return source;
}
/*-----------------------------------------------------------------------------------*/


