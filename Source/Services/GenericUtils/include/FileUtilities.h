/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include <vector>
#include "GenericUtilsIncludes.h"

//---------------------------------------------------------------------------------------
GENERIC_UTILS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class GENERIC_UTILS_IMP_EXP FileUtilities
{
public:

	//File / directory utilities

	static std::string DirectoryFromFullFilePath(const std::string& path);

	static std::string FileNameFromFilePath(const std::string& path);

	static std::string FileNameFromFilePath_WithoutExt(const std::string& path);

	static void FileCopy(const std::string& from, const std::string& to);

	static bool RemoveFile(const char* filename);

	static bool CheckFileExists(const char* filename);

	static const char* GetBuffer(const char* filePath);

	static const char* GetBuffer(const std::string& filePath);

	static std::string FullPath(const std::string& relativePath);

	static void CreateFolder(const std::string& directory);

	static std::string CurrentDirectory();

	static std::vector<std::string> ListDirs(const std::string& parentDir, const std::string& nameContains);

	static std::vector<std::wstring> ListDirs_WideChar(const std::wstring& parentDir, const std::wstring& nameContains);

	static bool CompareFileTexts(const char* fileA, const char* fileB);

	static std::string GetUniqueTimestampedFile(
		const wchar_t* directory,
		const char* filePrefix,
		const char* extension = "");

};
//---------------------------------------------------------------------------------------

GENERIC_UTILS_NAMESPACE_END

