/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include <vector>
#include <algorithm>

#include "GenericUtilsIncludes.h"

//---------------------------------------------------------------------------------------
GENERIC_UTILS_NAMESPACE_START
//---------------------------------------------------------------------------------------

class GENERIC_UTILS_IMP_EXP StringUtilities
{
public:
	////////////////////////////////////////////////////////////////////////
	//String / time utilities

	static std::vector<std::string> ConvertBufferToLines(const char* buffer);

	static const wchar_t* ConvertToWideChar(const char* str);

	static const char* ConvertToChar(const wchar_t* str);

	static std::wstring GetCurrentTimeString_WideChar();

	static std::string GetCurrentTimeString();

	static std::string ReplaceSubstring(std::string source, std::string search, std::string replace);
	template <typename T>
	static std::string to_string_with_width(const T a_value, const int n = 6);

	template <typename T>
	static std::wstring to_string_with_width_WideChar(const T a_value, const int n = 6);

	template <typename T>
	static T from_string(const std::string& s);

	template <typename T>
	static T from_string_WideChar(const std::wstring& s);

	static inline bool EndsWith(std::string const & value, std::string const & ending)
	{
		if (ending.size() > value.size()) return false;
		return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
	}

	static inline bool StartsWith(std::string const & value, std::string const & starting)
	{
		if (starting.size() > value.size()) return false;
		return std::equal(starting.begin(), starting.end(), value.begin());
	}

};
//---------------------------------------------------------------------------------------
GENERIC_UTILS_NAMESPACE_END
//---------------------------------------------------------------------------------------
