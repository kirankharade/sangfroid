﻿#pragma once

#include <iostream>
#include <fstream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointCloudImpl.h"
#include "PointBuffer.h"
#include "NormalBuffer.h"
#include "Isosurfacer.h"
#include "CustomCurve.h"
#include "CustomCloudMesher.h"

using namespace std;
using namespace Sangfroid::PointMatters;


/*-----------------------------------------------------------------------------------*/

PointCloudImpl::PointCloudImpl(PointBuffer* posBuffer, NormalBuffer* normalBuffer)
   :m_posBuffer(posBuffer), m_normalBuffer(normalBuffer)
{
}
/*-----------------------------------------------------------------------------------*/

PointCloudImpl::~PointCloudImpl()
{
}
/*-----------------------------------------------------------------------------------*/

PointBuffer* PointCloudImpl::Positions()
{
   return m_posBuffer;
}
/*-----------------------------------------------------------------------------------*/

NormalBuffer* PointCloudImpl::Normals()
{
   return m_normalBuffer;
}
/*-----------------------------------------------------------------------------------*/

TriangleMesh* PointCloudImpl::Isosurfaces(const char* stlFileName)
{
    auto size = this->m_posBuffer->Min().dist(this->m_posBuffer->Max()) / 1000;
   return Isosurfacer::Process(this->Positions(), size, stlFileName);
}
/*-----------------------------------------------------------------------------------*/

TriangleMesh* PointCloudImpl::ToMesh(const char* stlFileName, bool generateMissingCurves)
{
	auto curves = CustomCloudMesher::CloudToZCurves(this->Positions());
	std::vector<CustomCurve*> dCurves;
	for (auto& c : curves)
	{
		dCurves.push_back((CustomCurve*)c);
	}

	return CustomCloudMesher::Mesh(dCurves, stlFileName, generateMissingCurves);
}
/*-----------------------------------------------------------------------------------*/

std::vector<DiscreteCurve*> PointCloudImpl::CloudToZCurves()
{
	auto curves = CustomCloudMesher::CloudToZCurves(this->Positions());
	return curves;
}
/*-----------------------------------------------------------------------------------*/

PointOctree* PointCloudImpl::Octree()
{
   if (!m_pOctree)
   {
      fillUpOctree();
   }
   return m_pOctree;
}
/*-----------------------------------------------------------------------------------*/

void PointCloudImpl::fillUpOctree()
{
   if (!m_pOctree)
   {
      int treeDepth = PointOctree::CalculateTreeDepth(m_posBuffer->PointCount());
      m_pOctree = new PointOctree(m_posBuffer, treeDepth, true);
   }
}

/*-----------------------------------------------------------------------------------*/

void PointCloudImpl::Save(const char* filename)
{
	std::ofstream fs;
	fs.open(filename, std::ios::out);
   auto buffer = this->m_posBuffer;
   auto numPoints = buffer->PointCount();
	for (int i = 0; i < numPoints; i++)
	{
		auto pt = buffer->PointAt(i);
		fs << pt.x << "\t" << pt.y << "\t" << pt.z << endl;
	}
	fs.close();
}
/*-----------------------------------------------------------------------------------*/
