﻿#include <iostream>
#include "CloudVolume.h"
#include "Containers/UltraVector.h"
#include <vector>

using namespace std;
using namespace Sangfroid::PointMatters;
/*-----------------------------------------------------------------------------------*/

CloudVolume::CloudVolume()
   :VtxUnifier(nullptr), EdgeIntersectionsUnifier(nullptr)
{
}
/*-----------------------------------------------------------------------------------*/

void CloudVolume::PopulateMap(const std::vector<Sangfroid::Maths::Point3f>& points)
{
   for (auto& pt : points)
   {
      int i = (int) std::floor((pt.x - Min.x) / Spacing[0]);
      int j = (int) std::floor((pt.y - Min.y) / Spacing[1]);
      int k = (int) std::floor((pt.z - Min.z) / Spacing[2]);

      auto& itr = CellMap.find(IndexTriplet(i, j, k));

      if (itr != CellMap.end())
      {
         auto& v = itr->second;
         v.push_back(pt);
      }
      else
      {
         std::vector<Sangfroid::Maths::Point3f> v;
         v.push_back(pt);
         CellMap.insert(std::make_pair(IndexTriplet(i, j, k), v));
      }
   }
}
/*-----------------------------------------------------------------------------------*/

CloudVolume* CloudVolume::GenerateCloudVolume(
   const Sangfroid::Maths::Point3f& min,
   const Sangfroid::Maths::Point3f& max,
   const float spacing,
   const std::vector<Sangfroid::Maths::Point3f>& points)
{
   auto cv = new CloudVolume();

   cv->Min = Point3f(min.x - spacing, min.y - spacing, min.z - spacing);
   cv->Max = Point3f(max.x + spacing, max.y + spacing, max.z + spacing);

   float xrange = (cv->Max.x - cv->Min.x);
   float yrange = (cv->Max.y - cv->Min.y);
   float zrange = (cv->Max.z - cv->Min.z);

   cv->Nsteps[0] = (int)(xrange / spacing);
   cv->Nsteps[1] = (int)(yrange / spacing);
   cv->Nsteps[2] = (int)(zrange / spacing);

   cv->Spacing[0] = xrange / (float)(cv->Nsteps[0]);
   cv->Spacing[1] = yrange / (float)(cv->Nsteps[1]);
   cv->Spacing[2] = zrange / (float)(cv->Nsteps[2]);

   cv->PopulateMap(points);

   cv->ConvertTripletsToCells();

   return cv;
}
/*-----------------------------------------------------------------------------------*/

void getCellCornerPoints(const CloudVolume* cv, const IndexTriplet& t, Sangfroid::Maths::Point3f* corners)
{
   float dx = cv->Spacing[0];
   float dy = cv->Spacing[1];
   float dz = cv->Spacing[2];

   corners[0].x = cv->Min.x + (dx * t.i);
   corners[0].y = cv->Min.y + (dy * t.j);
   corners[0].z = cv->Min.z + (dz * t.k);

   corners[1].x = cv->Min.x + (dx * (t.i + 1));
   corners[1].y = cv->Min.y + (dy * t.j);
   corners[1].z = cv->Min.z + (dz * t.k);

   corners[2].x = cv->Min.x + (dx * (t.i + 1));
   corners[2].y = cv->Min.y + (dy * (t.j + 1));
   corners[2].z = cv->Min.z + (dz * t.k);

   corners[3].x = cv->Min.x + (dx * t.i);
   corners[3].y = cv->Min.y + (dy * (t.j + 1));
   corners[3].z = cv->Min.z + (dz * t.k);

   corners[4].x = cv->Min.x + (dx * t.i);
   corners[4].y = cv->Min.y + (dy * t.j);
   corners[4].z = cv->Min.z + (dz * (t.k + 1));

   corners[5].x = cv->Min.x + (dx * (t.i + 1));
   corners[5].y = cv->Min.y + (dy * t.j);
   corners[5].z = cv->Min.z + (dz * (t.k + 1));

   corners[6].x = cv->Min.x + (dx * (t.i + 1));
   corners[6].y = cv->Min.y + (dy * (t.j + 1));
   corners[6].z = cv->Min.z + (dz * (t.k + 1));

   corners[7].x = cv->Min.x + (dx * t.i);
   corners[7].y = cv->Min.y + (dy * (t.j + 1));
   corners[7].z = cv->Min.z + (dz * (t.k + 1));
}
/*-----------------------------------------------------------------------------------*/

void CloudVolume::ConvertTripletsToCells()
{
   if (VtxUnifier == nullptr)
   {
      auto expansion = 2.0f;
      Point3f min(Min.x - expansion, Min.y - expansion, Min.z - expansion);
      Point3f max(Max.x + expansion, Max.y + expansion, Max.z + expansion);
      VtxUnifier = new PointUnifier(min, max);
   }

   int count = 0;

   for (auto itr = CellMap.begin(); itr != CellMap.end(); itr++)
   {
	  count++;

	  if (count == 30000)
	  {
		  break;
	  }
      GridCell gc;

      auto& triplet = itr->first;
      auto& containedPoints = itr->second;

      gc.Triplet = triplet;

      Point3f corners[8];
      getCellCornerPoints(this, triplet, corners);
      for (int ic = 0; ic < 8; ic++)
      {
         gc.CornerIndices[ic] = VtxUnifier->Add(corners[ic]);
      }
      generateCellEdges(gc);

      for (size_t ip = 0; ip < containedPoints.size(); ip++)
      {
         auto& pt = containedPoints[ip];
         auto ptIndex = VtxUnifier->Add(pt);
         gc.ContainedPoints.push_back(ptIndex);
      }

      GridCells.push_back(gc);
   }
}
/*-----------------------------------------------------------------------------------*/

void CloudVolume::generateCellEdges(GridCell& gc)
{
   gc.Edges[0] = addToEdgeMap(GridEdge(gc.CornerIndices[0], gc.CornerIndices[1]));
   gc.Edges[1] = addToEdgeMap(GridEdge(gc.CornerIndices[1], gc.CornerIndices[2]));
   gc.Edges[2] = addToEdgeMap(GridEdge(gc.CornerIndices[2], gc.CornerIndices[3]));
   gc.Edges[3] = addToEdgeMap(GridEdge(gc.CornerIndices[3], gc.CornerIndices[0]));

   gc.Edges[4] = addToEdgeMap(GridEdge(gc.CornerIndices[4], gc.CornerIndices[5]));
   gc.Edges[5] = addToEdgeMap(GridEdge(gc.CornerIndices[5], gc.CornerIndices[6]));
   gc.Edges[6] = addToEdgeMap(GridEdge(gc.CornerIndices[6], gc.CornerIndices[7]));
   gc.Edges[7] = addToEdgeMap(GridEdge(gc.CornerIndices[7], gc.CornerIndices[4]));

   gc.Edges[8] = addToEdgeMap(GridEdge(gc.CornerIndices[0], gc.CornerIndices[4]));
   gc.Edges[9] = addToEdgeMap(GridEdge(gc.CornerIndices[1], gc.CornerIndices[5]));
   gc.Edges[10] = addToEdgeMap(GridEdge(gc.CornerIndices[2], gc.CornerIndices[6]));
   gc.Edges[11] = addToEdgeMap(GridEdge(gc.CornerIndices[3], gc.CornerIndices[7]));
}
/*-----------------------------------------------------------------------------------*/

int CloudVolume::addToEdgeMap(const GridEdge& ge)
{
   auto& itr = EdgeMap.find(ge);
   if (itr != EdgeMap.end())
   {
      return itr->second;
   }
   else
   {
      auto index = (int) EdgeMap.size();
      EdgeMap.insert(std::make_pair(ge, index));
      RevEdgeMap.insert(std::make_pair(index, ge));
      return index;
   }
}
/*-----------------------------------------------------------------------------------*/


