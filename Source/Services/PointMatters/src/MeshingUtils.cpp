﻿#include "MeshingUtils.h"
#include "PointCloud.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include "MathUtils.h"
#include "PointTriangle.h"
#include "DiscreteCurve.h"
#include "GeoElementsUtils.h"

#include <map>
#include <algorithm>
#include "FileUtilities.h"
#include <fstream>
#include <vector>
#include <vector>
#include <cmath>
#include <string> 

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::PointMatters;
using namespace Sangfroid::GenericUtils;


#pragma warning(disable:4244 4996)

/*-----------------------------------------------------------------------------------*/

UltraVector<PointTriangle> MeshingUtils::ConstructMesh(const std::vector<std::vector<Point3f*>>& intersectionGridCurves)
{
    UltraVector<PointTriangle> triangles;
    for (size_t ip = 0; ip < intersectionGridCurves.size() - 1; ip++)
    {
        auto ca = intersectionGridCurves[ip];
        auto cb = intersectionGridCurves[ip + 1];

        auto pointCount = ca.size();
        for (size_t j = 0; j < pointCount - 1; j++)
        {
            auto a = ca[j];
            auto b = ca[j + 1];
            auto c = cb[j + 1];
            if (a != nullptr && b != nullptr && c != nullptr)
            {
                PointTriangle t;
                t.Vertices[0] = *a;
                t.Vertices[1] = *b;
                t.Vertices[2] = *c;
                triangles.push_back(t);
            }

            a = ca[j];
            b = cb[j + 1];
            c = cb[j];
            if (a != nullptr && b != nullptr && c != nullptr)
            {
                PointTriangle t;
                t.Vertices[0] = *a;
                t.Vertices[1] = *b;
                t.Vertices[2] = *c;
                triangles.push_back(t);
            }
        }
    }
    return triangles;
}

std::vector<std::vector<Point3f*>> MeshingUtils::GenerateMissingAreas(std::vector<std::vector<Point3f*>>& intersectionGridCurves)
{
    if (intersectionGridCurves.size() > 2)
    {
        for (size_t ip = 1; ip < intersectionGridCurves.size() - 1; ip++)
        {
            auto& cx = intersectionGridCurves[ip];
            auto& cp = intersectionGridCurves[ip - 1];
            auto& cn = intersectionGridCurves[ip + 1];

            auto pointCount = cx.size();
            for (size_t j = 0; j < pointCount; j++)
            {
                auto& a = cx[j];
                auto& b = cp[j];
                auto& c = cn[j];
                if (a == nullptr && b != nullptr && c != nullptr)
                {
                    auto x = (b->x + c->x) / 2.0;
                    auto y = (b->y + c->y) / 2.0;
                    auto z = (b->z + c->z) / 2.0;

                    a = new Point3f(x, y, z);
                }
            }
        }
    }
    return intersectionGridCurves;
}

std::vector<std::vector<Point3f*>> MeshingUtils::FindPlaneCurveIntersections(const std::vector<DiscreteCurve*>& curves, const std::vector<std::tuple<Point3f, Vector3f>>& planes)
{
    std::vector<std::vector<Point3f*>> intersectionGridCurves;
    bool segmentLiesInPlane = false;
	const float proximityTolerance = 0.0001f;
	const float parametricTolerance = 0.008f;

    for (size_t i = 0; i < curves.size(); i++)
    {
        auto c = curves[i];
        auto points = c->Points();
        std::vector<Point3f*> intersections;

        for (size_t j = 0; j < planes.size(); j++)
        {
            auto t = planes[j];
            auto planePoint = std::get<0>(t);
            auto planeNormal = std::get<1>(t);
            Point3f* intersectionPoint = nullptr;

            for (size_t k = 0; k < points.size() - 1; k++)
            {
                auto a = points[k];
                auto b = points[k + 1];
                Point3f tempIntersection;
                auto intersecting = MathUtils::LineSegmentPlaneIntersection(a, b, planePoint, planeNormal,
                    tempIntersection, segmentLiesInPlane, proximityTolerance, parametricTolerance);

                if (intersecting)
                {
                    intersectionPoint = new Point3f(tempIntersection);
                    break;
                }
            }

            intersections.push_back(intersectionPoint);
        }
        intersectionGridCurves.push_back(intersections);
    }
    return intersectionGridCurves;
}

std::vector<std::tuple<Point3f, Vector3f>> MeshingUtils::GetIntersectionPlanes(std::vector<CustomCurve*> curves)
{
    double thetaStart = curves[0]->StartAngle();
    double thetaEnd = curves[0]->EndAngle();

    for (size_t i = 1; i < curves.size(); i++)
    {
        auto& c = curves[i];
        auto sd = c->StartDir();
        auto ed = c->EndDir();

        auto sa = c->StartAngle();
        auto ea = c->EndAngle();

        if (fabs(sa) > 361.0 || fabs(ea) > 361.0)
        {
            continue;
        }

        if (sa < thetaStart)
        {
            thetaStart = sa;
        }

        if (ea > thetaEnd)
        {
            thetaEnd = ea;
        }
    }

    thetaStart -= 2.0;
    thetaEnd += 2.0;

	return GetIntersectionPlanes(thetaStart, thetaEnd, 74);
}

std::vector<std::tuple<Point3f, Vector3f>> MeshingUtils::GetIntersectionPlanes(const double thetaStart, const double thetaEnd, const int stepsCount)
{
	std::vector<std::tuple<Point3f, Vector3f>> planes;
	auto step = (thetaEnd - thetaStart) / stepsCount;
	auto r = 100.0;

	for (int i = 0; i < stepsCount + 1; i++)
	{
		auto theta = thetaStart + (step * i);
		theta = ToRadians(theta);

		auto x = r * cos(theta);
		auto y = r * sin(theta);
		auto z = 0;

		Point3f pt(x, y, z);

		Vector3f va(0, 0, 1);
		Vector3f vb(x, y, z);

		auto dir = va.cross(vb);
		dir = dir.unitVec();

		planes.push_back(std::make_tuple(pt, dir));
	}

	return planes;
}

std::tuple<Point3f, Vector3f> MeshingUtils::GetIntersectionPlane(const double angle)
{
	auto r = 100.0;
	auto theta = ToRadians(angle);

	auto x = r * cos(theta);
	auto y = r * sin(theta);
	auto z = 0;

	Point3f pt(x, y, z);

	Vector3f va(0, 0, 1);
	Vector3f vb(x, y, z);

	auto dir = va.cross(vb);
	dir = dir.unitVec();

	return std::make_tuple(pt, dir);
}

bool MeshingUtils::IsCurveCCW(const DiscreteCurve* c)
{
    auto thisPoints = c->Points();
    Point3f origin(0, 0, thisPoints[0].z);
    auto vec1 = (thisPoints[1] - thisPoints[0]).unitVec();
    auto vec2 = (thisPoints[0] - origin).unitVec();
    auto cross = vec2.cross(vec1);
    auto dot = Vector3f(0, 0, 1).dot(cross);
    if (dot > 0)
    {
        return true;
    }
    return false;
}

bool MeshingUtils::IsAngleWithinRange(double start, double end, double value)
{
    auto ret = false;
    if (start > end)
    {
        if (value >= start || value <= end)
            return true;
    }
    else
    {
        if (value >= start && value <= end)
            return true;
    }
    return false;
}

/*-----------------------------------------------------------------------------------*/

