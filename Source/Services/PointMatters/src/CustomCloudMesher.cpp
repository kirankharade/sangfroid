﻿#include "CustomCloudMesher.h"
#include "MeshingUtils.h"
#include "PointCloud.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include "MathUtils.h"
#include "PointTriangle.h"
#include "CustomCurve.h"
#include "GeoElementsUtils.h"

#include <map>
#include <algorithm>
#include "FileUtilities.h"
#include <fstream>
#include <vector>
#include <vector>
#include <cmath>
#include <string> 

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::PointMatters;
using namespace Sangfroid::GenericUtils;


#pragma warning(disable:4244 4996)

/*-----------------------------------------------------------------------------------*/

std::vector<DiscreteCurve*> CustomCloudMesher::CloudToZCurves(PointBuffer* buffer)
{
    std::vector<DiscreteCurve*> zCurves;

    auto points = PointBuffer::PointsFromBuffer(buffer);
    if (points.size() == 0)
    {
        return zCurves;
    }

    float lastZ, zDiff = FLT_MAX;

    float zThreshold = 0.008f;
    float p2pThreshold = 3.0f;
    double vecDistThreshold = 0.001;

    int shortCurvesDueToDist = 0;
    int shortCurvesDueToZLevel = 0;
    int shortCurvesDueToDeviation = 0;

    std::vector<Point3f> curvePoints;
    curvePoints.push_back(points[0]);
    Point3f pt, lastPoint;

    for (size_t i = 1; i < points.size(); i++)
    {
        pt = points[i];
        lastPoint = points[i - 1];
        lastZ = lastPoint.z;
        zDiff = fabs(pt.z - lastZ);
        auto distP2P = pt.dist(lastPoint);

        double deviationFromDirection = DBL_MAX;
        if (curvePoints.size() >= 2)
        {
            auto size = curvePoints.size();
            auto lineStart = curvePoints[size - 2];
            auto lineEnd = curvePoints[size - 1];
            auto line = (lineEnd - lineStart).unitVec();
            deviationFromDirection = MathUtils::GetProjectedDistFromLine(lineStart, line, pt);
        }

        if (zDiff > zThreshold  && curvePoints.size() > 0)
        {
            zCurves.push_back(new DiscreteCurve(curvePoints));
            if (curvePoints.size() < 3)
            {
                shortCurvesDueToZLevel++;
            }
            curvePoints = std::vector<Point3f>();
            curvePoints.push_back(pt);
            continue;
        }

        if (deviationFromDirection < vecDistThreshold && distP2P < 3.0)
        {
            curvePoints.push_back(pt);
            continue;
        }
        if (deviationFromDirection < vecDistThreshold && distP2P > 3.0)
        {
            shortCurvesDueToDeviation++;
        }
        if (distP2P > p2pThreshold)
        {
            if (curvePoints.size() < 3)
            {
                shortCurvesDueToDist++;
            }
            zCurves.push_back(new DiscreteCurve(curvePoints));
            curvePoints = std::vector<Point3f>();
            curvePoints.push_back(pt);
            continue;
        }
        curvePoints.push_back(pt);
    }
    if (curvePoints.size() > 0)
    {
        zCurves.push_back(new DiscreteCurve(curvePoints));
    }

    //AddCurvesRenderGroup(zCurves, "Z-curves");
    //AddCurvesRenderGroup(zCurves, "Z-points", false);

    return zCurves;
}

TriangleMesh* CustomCloudMesher::Mesh(std::vector<CustomCurve*> curves, const char* stlFileName, bool generateMissingCurves)
{
    auto planes = MeshingUtils::GetIntersectionPlanes(curves);
	std::vector<DiscreteCurve*> dCurves;
	for (auto& c : curves)
	{
		dCurves.push_back((DiscreteCurve*) c);
	}
    auto intersectionGridCurves = MeshingUtils::FindPlaneCurveIntersections(dCurves, planes);
    //AddIntersectionPointsToRenderGroup(intersectionGridCurves);
    if (generateMissingCurves)
    {
        intersectionGridCurves = MeshingUtils::GenerateMissingAreas(intersectionGridCurves);
    }
    auto triangles = MeshingUtils::ConstructMesh(intersectionGridCurves);
    GeoElementsUtils::WriteTriangleListToSTL(triangles, stlFileName);
    return nullptr;
}

/*-----------------------------------------------------------------------------------*/

