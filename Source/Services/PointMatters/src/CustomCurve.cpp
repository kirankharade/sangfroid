﻿#pragma once

#include <iostream>
#include <vector>
#include "CustomCurve.h"
#include "MathsDefines.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::Maths;
using namespace Sangfroid::PointMatters;

/*-----------------------------------------------------------------------------------*/

CustomCurve::CustomCurve(const std::vector<Sangfroid::Maths::Point3f>& points)
	:DiscreteCurve(points)
{
	if (m_points.size() > 0)
	{
		m_avgZ = 0;
		m_avgR = 0;
		for (auto p : m_points)
		{
			m_avgZ += p.z;
			m_avgR += sqrtf((p.x * p.x) + (p.y * p.y));
		}
		m_avgZ /= m_points.size();
		m_avgR /= m_points.size();
	}

	if (m_points.size() > 2)
	{
		auto lastIndex = m_points.size() - 1;
		m_startDir = m_points[1] - m_points[0];
		m_endDir = m_points[m_points.size() - 1] - m_points[m_points.size() - 2];
		m_startAngle = ToDegrees(atan2f(m_points[0].y, m_points[0].x));
		m_endAngle = ToDegrees(atan2f(m_points[lastIndex].y, m_points[lastIndex].x));
		if (m_startAngle < 0) m_startAngle += 360.0;
		if (m_endAngle < 0) m_endAngle += 360.0;
	}
}

/*-----------------------------------------------------------------------------------*/
