﻿#include "KLMesher.h"
#include "KLCurve.h"
#include "MeshingUtils.h"
#include "PointCloud.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include "MathUtils.h"
#include "PointTriangle.h"
#include "DiscreteCurve.h"
#include "GeoElementsUtils.h"
#include "StringUtilities.h"

#include <map>
#include <algorithm>
#include "FileUtilities.h"
#include <fstream>
#include <vector>
#include <vector>
#include <cmath>
#include <string> 

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::PointMatters;
using namespace Sangfroid::GenericUtils;

#pragma warning(disable:4244 4996)

/*-----------------------------------------------------------------------------------*/
static const int SECTOR_COUNT = 100;
static const int DIVISIONS_PER_SECTOR = 36;
static const double ANGLE_TOLERANCE = 0.0001;
static const double PROXIMITY_TOLERANCE = 0.00001;

static Sectors _sectors;
static Curves _allCurves;

/*-----------------------------------------------------------------------------------*/

static void ExtendCurvesToSectorBoundaries()
{
	KLCurve* prevCurve = nullptr;
	KLCurve* thisCurve = nullptr;
	KLCurve* nextCurve = nullptr;
	bool segmentLiesInPlane = false;
	Point3f intersection;

	for (size_t i = 0; i < _allCurves.size(); i++)
	{
		prevCurve = (i == 0) ? nullptr : _allCurves[i - 1];
		thisCurve = _allCurves[i];
		nextCurve = (i >= (int) _allCurves.size() - 1) ? nullptr : _allCurves[i + 1];
		auto thisSector = _sectors[thisCurve->SectorIndex];

		if (prevCurve != nullptr)
		{
			auto prevSector = _sectors[prevCurve->SectorIndex];

			if(!EQT(prevSector->_endAngle, thisSector->_startAngle, ANGLE_TOLERANCE) &&
			   !EQT(fabs(prevSector->_endAngle - thisSector->_startAngle), 360.0, ANGLE_TOLERANCE))
			{
				throw "Unmatching sector angles";
			}
			auto prevCurveEndPoint = prevCurve->LastPoint();
			auto thisCurveStartPoint = thisCurve->FirstPoint();

			auto plane = MeshingUtils::GetIntersectionPlane(thisSector->_startAngle);

			if (MathUtils::LineSegmentPlaneIntersection(
				prevCurveEndPoint,
				thisCurveStartPoint,
				std::get<0>(plane),
				std::get<1>(plane),
				intersection,
				segmentLiesInPlane,
				PROXIMITY_TOLERANCE,
				0.008f))
			{
				if (!thisCurveStartPoint.AreSame(intersection, PROXIMITY_TOLERANCE))
				{
					thisCurve->InsertAtStart(intersection);
				}
			}
		}

		if (nextCurve != nullptr)
		{
			auto nextSector = _sectors[nextCurve->SectorIndex];
			if (!EQT(thisSector->_endAngle, nextSector->_startAngle, ANGLE_TOLERANCE) &&
				!EQT(fabs(thisSector->_endAngle - nextSector->_startAngle), 360.0, ANGLE_TOLERANCE))
			{
				throw "Unmatching sector angles";
			}

			auto thisCurveEndPoint = thisCurve->LastPoint();
			auto nextCurveStartPoint = nextCurve->FirstPoint();

			auto plane = MeshingUtils::GetIntersectionPlane(thisSector->_endAngle);

			if (MathUtils::LineSegmentPlaneIntersection(
				thisCurveEndPoint,
				nextCurveStartPoint,
				std::get<0>(plane),
				std::get<1>(plane),
				intersection,
				segmentLiesInPlane,
				PROXIMITY_TOLERANCE, 
				0.008f))
			{
				if (!thisCurveEndPoint.AreSame(intersection, PROXIMITY_TOLERANCE))
				{
					thisCurve->InsertAtEnd(intersection);
				}
			}
		}
	}

}

static void SegregateCloudToSectoredCurves(PointBuffer* buffer)
{
	auto points = PointBuffer::PointsFromBuffer(buffer);
	if (points.size() == 0)
	{
		return;
	}

	_sectors.clear();

	auto step = 360.0 / SECTOR_COUNT;
	for (int is = 0; is < SECTOR_COUNT; is++)
	{
		auto s = new Sector();
		s->_sectorIndex = is;
		s->_startAngle = is * step;
		s->_endAngle = (is + 1) * step;
		_sectors.push_back(s);
	}

	int currSectorIndex = -1;
	std::vector<Point3f> curvePoints;

	for (size_t i = 0; i < points.size(); i++)
	{
		auto pt = points[i];

		auto theta = ToDegrees(atan2f(pt.y, pt.x));
		if (theta < -ANGLE_TOLERANCE) theta += 360.0;

		auto sectorIndex = (int)std::floor(theta / step);

		if (currSectorIndex != sectorIndex)
		{
			if (curvePoints.size() > 0)
			{
				auto curve = new KLCurve(curvePoints);
				curve->SectorIndex = currSectorIndex;
				curve->Index = _allCurves.size();
				_sectors[currSectorIndex]->_curves.push_back(curve);
				_allCurves.push_back(curve);
			}
			curvePoints = std::vector<Point3f>();
			curvePoints.push_back(pt);
			currSectorIndex = sectorIndex;
			continue;
		}
		else
		{
			curvePoints.push_back(pt);
		}
	}

	ExtendCurvesToSectorBoundaries();
}
/*-----------------------------------------------------------------------------------*/

UltraVector<PointTriangle> GenerateSectorMesh(Sector* sector)
{
	auto planes = MeshingUtils::GetIntersectionPlanes(sector->_startAngle, sector->_endAngle, DIVISIONS_PER_SECTOR);
	std::vector<DiscreteCurve*> dCurves;
	for (auto& c : sector->_curves)
	{
		dCurves.push_back(c);
	}
	auto intersectionGridCurves = MeshingUtils::FindPlaneCurveIntersections(dCurves, planes);
	return MeshingUtils::ConstructMesh(intersectionGridCurves);
}

void KLMesher::Mesh(PointBuffer* buffer, const char* outputFile)
{
	SegregateCloudToSectoredCurves(buffer);
	UltraVector<PointTriangle> combined;

	for (int i = 0; i < SECTOR_COUNT; i++)
	{
		auto meshTriangles = GenerateSectorMesh(_sectors[i]);
		for (size_t j = 0; j < meshTriangles.size(); j++)
		{
			combined.push_back(meshTriangles[j]);
		}
	}
	GeoElementsUtils::WriteTriangleListToSTL(combined, std::string(outputFile));
}

/*-----------------------------------------------------------------------------------*/

