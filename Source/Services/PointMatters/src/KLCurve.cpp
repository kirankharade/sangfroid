﻿#pragma once

#include <iostream>
#include <vector>
#include "KLCurve.h"
#include "MathsDefines.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::Maths;
using namespace Sangfroid::PointMatters;

/*-----------------------------------------------------------------------------------*/

KLCurve::KLCurve(const std::vector<Sangfroid::Maths::Point3f>& points)
	:DiscreteCurve(points), Index(-1), SectorIndex(-1)
{
}

/*-----------------------------------------------------------------------------------*/
