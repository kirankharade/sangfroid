bool CustomCloudMesher::AreCurvesOverlapping(const DiscreteCurve* c, const DiscreteCurve* other)
{
    double minDistance = DBL_MAX;
    auto otherPoints = other->Points();
    auto thisPoints = c->Points();
    double overlappingThreshold = 0.007;
    auto cmax = c->Max();
    auto cmin = c->Min();
    auto omax = other->Max();
    auto omin = other->Min();

    if (cmax.x < omin.x) return false;
    if (cmax.y < omin.y) return false;
    if (cmax.z < omin.z) return false;
    if (cmin.x > omax.x) return false;
    if (cmin.y > omax.y) return false;
    if (cmin.z > omax.z) return false;

    auto anglesOverlapping = AreAnglesOverlapping(c, other);

    auto zDiff = fabs(c->AverageZ() - other->AverageZ());
    if (zDiff < 0.01 && anglesOverlapping)
    {
        return true;
    }

    //auto closeEncounters = 0;

    //for (int i = 1; i < otherPoints.size(); i++)
    //{
    //	auto& s = otherPoints[i - 1];
    //	auto& e = otherPoints[i];
    //	for (int j = 0; j < thisPoints.size(); j++)
    //	{
    //		auto& p = thisPoints[j];
    //		auto d = GML::Distance::PointToLineSeg(p, s, e);
    //		if (d < overlappingThreshold)
    //		{
    //			closeEncounters++;
    //			if (closeEncounters > 2)
    //			{
    //				return true;
    //			}
    //		}
    //	}
    //}
    return false;
}

std::vector<DiscreteCurve*> CustomCloudMesher::MergeCoincidentZCurves(std::vector<DiscreteCurve*> curves)
{
    auto minZ = DBL_MAX;
    auto maxZ = -DBL_MAX;
    auto minR = DBL_MAX;
    auto maxR = -DBL_MAX;

    ofstream f("C:/Temp/z-levels.txt");
    double lastZ = 0;

    for (auto& c : curves)
    {
        auto z = c->AverageZ();
        if (c->AverageZ() < minZ)	minZ = c->AverageZ();
        if (c->AverageZ() > maxZ)	maxZ = c->AverageZ();

        auto diff = z - lastZ;
        auto str = std::to_string(z) + "     " + std::to_string(diff) + "\n";
        lastZ = z;

        f << str;
    }
    f.close();

    auto span = (maxZ - minZ);
    minZ -= 0.01 * span;
    maxZ += 0.01 * span;

    auto zSteps = 100;
    auto step = (maxZ - minZ) / zSteps;

    std::map<int, std::vector<DiscreteCurve*>> cmap;
    std::map<int, int> revCmap;

    std::vector<DiscreteCurve*> mergedCurves;

    for (int i = 0; i < curves.size(); i++)
    {
        auto&c = curves[i];
        auto zOffset = c->AverageZ() - minZ;
        auto slotIndex = (int)floor(zOffset / step);
        auto itr = cmap.find(slotIndex);
        if (itr == cmap.end())
        {
            std::vector<DiscreteCurve*> tcurves;
            tcurves.push_back(c);
            cmap.insert(std::make_pair(slotIndex, tcurves));
        }
        else
        {
            itr->second.push_back(c);
        }
        revCmap.insert(std::make_pair(i, slotIndex));
    }

    for (int i = 0; i < curves.size(); i++)
    {
        auto&c = curves[i];
        if (c->Merged == true)
        {
            continue;
        }
        std::vector<DiscreteCurve*> curvesTomatch;
        auto slotIndex = revCmap[i];

        curvesTomatch.insert(curvesTomatch.end(), cmap[slotIndex].begin(), cmap[slotIndex].end());

        if (slotIndex == 0)
        {
            curvesTomatch.insert(curvesTomatch.end(), cmap[slotIndex + 1].begin(), cmap[slotIndex + 1].end());
        }
        else if (slotIndex == zSteps - 1)
        {
            curvesTomatch.insert(curvesTomatch.end(), cmap[slotIndex - 1].begin(), cmap[slotIndex - 1].end());
        }
        else
        {
            curvesTomatch.insert(curvesTomatch.end(), cmap[slotIndex + 1].begin(), cmap[slotIndex + 1].end());
            curvesTomatch.insert(curvesTomatch.end(), cmap[slotIndex - 1].begin(), cmap[slotIndex - 1].end());
        }

        std::vector<DiscreteCurve*> curvesToMerge;
        for (int j = 0; j < curvesTomatch.size(); j++)
        {
            auto& otherCurve = curvesTomatch[j];
            if (c == otherCurve)
            {
                continue;
            }

            if (otherCurve->Merged)
            {
                continue;
            }
            auto overlapping = AreCurvesOverlapping(c, otherCurve);
            if (overlapping)
            {
                curvesToMerge.push_back(otherCurve);
            }
        }

        auto mergedCurve = Merge(c, curvesToMerge);

        for (int k = 0; k < curvesToMerge.size(); k++)
        {
            curvesToMerge[k]->Merged = true;
        }
        c->Merged = true;

        mergedCurves.push_back(mergedCurve);
    }

    //AddCurvesRenderGroup(mergedCurves, "Merged-curves");

    return mergedCurves;
}

DiscreteCurve* CustomCloudMesher::Merge(DiscreteCurve* c, std::vector<DiscreteCurve*> curvesToMerge)
{
    if (curvesToMerge.size() == 0)
    {
        return c;
    }
    std::vector<Point3f> allPoints;
    auto& points = c->Points();
    allPoints.insert(allPoints.end(), points.begin(), points.end());
    for (int i = 0; i < curvesToMerge.size(); i++)
    {
        auto& oc = curvesToMerge[i];
        auto& ocPoints = oc->Points();
        allPoints.insert(allPoints.end(), ocPoints.begin(), ocPoints.end());
    }

    std::map<double, Point3f, std::less<double>> anglePoints;

    for (int j = 0; j < allPoints.size(); j++)
    {
        auto& p = allPoints[j];
        auto ar = atan2f(p.y, p.x);
        anglePoints.insert(std::make_pair(ar, p));
    }
    std::vector<Point3f> outPoints;
    for (auto itr = anglePoints.begin(); itr != anglePoints.end(); itr++)
    {
        outPoints.push_back(itr->second);
    }
    return new DiscreteCurve(outPoints);
}

std::vector<DiscreteCurve*> CustomCloudMesher::Smooth(std::vector<DiscreteCurve*> curves)
{
    std::vector<DiscreteCurve*> smoothenedCurves;
    for (int i = 0; i < curves.size(); i++)
    {
        auto curve = Smooth(curves[i]);
        smoothenedCurves.push_back(curve);
    }

    //AddCurvesRenderGroup(smoothenedCurves, "Smoothened-curves");

    return smoothenedCurves;
}

DiscreteCurve* CustomCloudMesher::Smooth(DiscreteCurve* c)
{
    std::vector<Point3f> smoothened;
    auto& points = c->Points();
    //smoothened.reserve(points.size());
    //memcpy(&(smoothened[0]), &(points[0]), points.size() * sizeof(Point3f));

    struct PointRadius
    {
        double radius;
        Point3f point;
        double angle;
        PointRadius(double r, double a, const Point3f& p)
            :radius(r), angle(a), point(p) {}
    };

    std::vector<PointRadius> rps;
    //std::map<double, Point3f, std::less<double>> radiusPointMap;

    for (int j = 0; j < points.size(); j++)
    {
        auto& p = points[j];
        auto a = atan2f(p.y, p.x);
        auto r = sqrt(p.x * p.x + p.y * p.y);
        rps.push_back(PointRadius(r, a, p));
    }

    for (int j = 1; j < rps.size() - 1; j++)
    {
        auto& p = rps[j];
        auto& pa = rps[j - 1];
        auto& pz = rps[j + 1];

        auto forwardDeviation = fabs(pa.radius - p.radius);
        auto backwardDeviation = fabs(pz.radius - p.radius);

        auto radialDeviationThreshold = 0.008;

        if (forwardDeviation > radialDeviationThreshold &&
            backwardDeviation > radialDeviationThreshold)
        {
            //auto averageR = (pa.radius + p.radius + pz.radius) / 3.0;
            p.point.x = (pa.point.x + pz.point.x) / 2.0;
            p.point.y = (pa.point.y + pz.point.y) / 2.0;
        }
    }

    std::vector<Point3f> outPoints;
    for (int j = 0; j < rps.size(); j++)
    {
        outPoints.push_back(rps[j].point);
    }
    return new DiscreteCurve(outPoints);
}

//Region separation

void CustomCloudMesher::SeparateAngularSectorRegions(std::vector<DiscreteCurve*> zCurves, int divisions, double angleOffsets)
{
    auto offset = angleOffsets;

    auto step = 360.0 / divisions;

    std::map<int, std::vector<DiscreteCurve*>> xCurves;
    for (auto& c : zCurves)
    {
        auto amax = -DBL_MAX;
        auto amin = DBL_MAX;
        auto points = c->Points();
        for (int j = 0; j < points.size(); j++)
        {
            auto& p = points[j];
            auto ar = atan2f(p.y, p.x);
            ar = ToDegrees(ar);
            if (ar < 0)
                ar += 360.0;
            ar += offset;
            if (ar > amax) amax = ar;
            if (ar < amin) amin = ar;
        }

        auto simax = (int)floor(amax / step);
        auto simin = (int)floor(amin / step);

        if (simax == simin)
        {
            auto itr = xCurves.find(simax);
            if (itr != xCurves.end())
            {
                itr->second.push_back(c);
            }
            else
            {
                std::vector<DiscreteCurve*> cs;
                cs.push_back(c);
                xCurves.insert(std::make_pair(simax, cs));
            }
        }
        else
        {
            auto dif = fabs(amax - amin);
            auto threshold = 360.0 - (2.0 * step);
            if (dif > threshold)
            {
                auto itr = xCurves.find(0);
                if (itr != xCurves.end())
                {
                    itr->second.push_back(c);
                }
                else
                {
                    std::vector<DiscreteCurve*> cs;
                    cs.push_back(c);
                    xCurves.insert(std::make_pair(0, cs));
                }
            }
        }

    }

    for (auto itr = xCurves.begin(); itr != xCurves.end(); itr++)
    {
        auto index = itr->first;
        auto curves = itr->second;
        if (curves.size() == 0)
        {
            continue;
        }

        string filename = "C:/Temp/extractedRegion_" + std::to_string(index) + ".xyz";
        ofstream f(filename);
        for (int i = 0; i < curves.size(); i++)
        {
            auto points = curves[i]->Points();
            for (int j = 0; j < points.size(); j++)
            {
                auto& p = points[j];
                f << p.x << "    " << p.y << "    " << p.z << endl;
            }
        }
        f.close();
    }
}

/*-----------------------------------------------------------------------------------*/

//Currently unused

TriangleMesh* CustomCloudMesher::Mesh(PointBuffer* buffer, const char* stlFileName)
{
    auto loc = FileUtilities::DirectoryFromFullFilePath(stlFileName);
    auto points = PointBuffer::PointsFromBuffer(buffer);

    //auto minSpacing = PoiseUtils::CalculateMinSpacing(points);
    //auto spacing = (float) (5.0f * minSpacing);
    //std::sort(points.begin(), points.end(), [](const Sangfroid::Maths::Point3f& lhs, const Sangfroid::Maths::Point3f& rhs) { return lhs.z < rhs.z; });
    //string file = "C:/Temp/Z-sorted.xyz";
    //std::ofstream fs;
    //fs.open(file.c_str(), std::ios::out);
    //auto numPoints = (int)points.size();
    //for (int ic = 0; ic < numPoints; ic++)
    //{
    //	auto& pt = points[ic];
    //	fs << pt.x << "\t" << pt.y << "\t" << pt.z << endl;
    //}
    //fs.close();

    std::vector<DiscreteCurve> zCurves;
    float currZ = FLT_MAX;
    float threshold = 0.005;

    std::vector<Point3f> curvePoints;

    for (int i = 0; i < points.size(); i++)
    {
        auto& pt = points[i];
        if (fabs(pt.z - currZ) > threshold && curvePoints.size() > 0)
        {
            zCurves.push_back(DiscreteCurve(curvePoints));
            curvePoints = std::vector<Point3f>();
            curvePoints.push_back(pt);
            currZ = pt.z;
            continue;
        }
        currZ = pt.z;
        curvePoints.push_back(pt);
    }
    return nullptr;
}

bool CustomCloudMesher::AreAnglesOverlapping(const DiscreteCurve* c, const DiscreteCurve* other)
{
    auto otherPoints = other->Points();
    auto thisPoints = c->Points();

    auto thisPointCount = thisPoints.size();
    auto otherPointCount = otherPoints.size();

    auto as = fmod(ToDegrees(atan2f(thisPoints[0].y, thisPoints[0].x)), 360.0);
    auto ae = fmod(ToDegrees(atan2f(thisPoints[thisPointCount - 1].y, thisPoints[thisPointCount - 1].x)), 360.0);
    auto bs = fmod(ToDegrees(atan2f(otherPoints[0].y, otherPoints[0].x)), 360.0);
    auto be = fmod(ToDegrees(atan2f(otherPoints[otherPointCount - 1].y, otherPoints[otherPointCount - 1].x)), 360.0);
    if (as < 0) as += 360.0;
    if (ae < 0) ae += 360.0;
    if (bs < 0) bs += 360.0;
    if (be < 0) be += 360.0;

    if (IsAngleWithinRange(as, ae, bs) || IsAngleWithinRange(bs, be, as))
        return true;

    return false;
}

/*-----------------------------------------------------------------------------------*/

std::vector<DiscreteCurve*> CustomCloudMesher::CloudToRCurves(PointBuffer * buffer)
{
    std::vector<DiscreteCurve*> rCurves;

    auto points = PointBuffer::PointsFromBuffer(buffer);
    if (points.size() == 0)
    {
        return rCurves;
    }

    float lastR, rDiff = FLT_MAX;

    float rThreshold = 0.018;
    float p2pThreshold = 3.0;

    std::vector<Point3f> curvePoints;
    curvePoints.push_back(points[0]);
    Point3f pt, lastPoint;

    for (int i = 1; i < points.size(); i++)
    {
        pt = points[i];
        lastPoint = points[i - 1];
        lastR = sqrtf((lastPoint.x * lastPoint.x) + (lastPoint.y * lastPoint.y));
        auto thisR = sqrtf((pt.x * pt.x) + (pt.y * pt.y));
        rDiff = fabs(thisR - lastR);
        auto distP2P = pt.dist(lastPoint);

        if (rDiff > rThreshold  && curvePoints.size() > 0)
        {
            rCurves.push_back(new DiscreteCurve(curvePoints));
            curvePoints = std::vector<Point3f>();
            curvePoints.push_back(pt);
            continue;
        }
        if (distP2P > p2pThreshold)
        {
            rCurves.push_back(new DiscreteCurve(curvePoints));
            curvePoints = std::vector<Point3f>();
            curvePoints.push_back(pt);
            continue;
        }
        curvePoints.push_back(pt);
    }
    if (curvePoints.size() > 0)
    {
        rCurves.push_back(new DiscreteCurve(curvePoints));
    }

    //AddCurvesRenderGroup(rCurves, "R-curves");
    //AddCurvesRenderGroup(rCurves, "R-points", false);

    return rCurves;
}

static std::vector<std::tuple<Point3f, Vector3f>> GetIntersectionPlanesForRCurves(std::vector<DiscreteCurve*> curves)
{
    std::vector<std::tuple<Point3f, Vector3f>> planes;
    auto nSteps = 500;
    auto step = 360.0 / nSteps;
    auto r = 100.0;

    for (int i = 0; i < nSteps; i++)
    {
        auto theta = step * i;
        theta = ToRadians(theta);

        auto x = r * cos(theta);
        auto y = r * sin(theta);
        auto z = 0;

        Point3f pt(x, y, z);

        Vector3f va(0, 0, 1);
        Vector3f vb(x, y, z);

        auto dir = va.cross(vb);
        dir = dir.unitVec();

        planes.push_back(std::make_tuple(pt, dir));
    }

    return planes;
}

