﻿#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointCloud.h"
#include "PointCloudImpl.h"
#include "PointBuffer.h"
#include "NormalBuffer.h"
#include <tuple>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::PointMatters;

/*-----------------------------------------------------------------------------------*/

PointCloud::PointCloud(Sangfroid::GeometryStructures::PointBuffer* posBuffer, 
    Sangfroid::GeometryStructures::NormalBuffer* normalBuffer)
   :m_pImpl(nullptr)
{
   m_pImpl = new PointCloudImpl(posBuffer, normalBuffer);
}

PointCloud::~PointCloud()
{
   if (m_pImpl != nullptr)
   {
      delete m_pImpl;
   }
}

TriangleMesh* PointCloud::ToMesh(const char* outDirectoryPath, bool generateMissingCurves)
{
	return ((PointCloudImpl*)m_pImpl)->ToMesh(outDirectoryPath, generateMissingCurves);
}

std::vector<TriangleMesh*> PointCloud::ToMesh(std::vector<PointCloud*> clouds, const char* outDirectoryPath, bool generateMissingCurves)
{
	std::vector<TriangleMesh*> meshes;
	for (size_t i = 0; i < clouds.size(); i++)
	{
		auto cloud = (PointCloudImpl*) clouds[i]->m_pImpl;
		auto mesh = cloud->ToMesh(outDirectoryPath, generateMissingCurves);
		meshes.push_back(mesh);
	}
	return meshes;
}

std::tuple<DiscreteCurve*, int> PointCloud::CloudToZCurves()
{
	auto curves = ((PointCloudImpl*)m_pImpl)->CloudToZCurves();
	auto outCurves = new DiscreteCurve[curves.size()];
	for (size_t i = 0; i < curves.size(); i++)
	{
		outCurves[i] = *(curves[i]);
	}
	return std::make_tuple(outCurves, (int) curves.size());
}

//void PointCloud::SeparateAngularSectorRegions(int divisions, double angleOffsets)
//{
//	((PointCloudImpl*)m_pImpl)->SeparateAngularSectorRegions(divisions, angleOffsets);
//}
//
//std::tuple<DiscreteCurve*, int> PointCloud::CloudToRCurves()
//{
//	auto curves = ((PointCloudImpl*)m_pImpl)->CloudToRCurves();
//	auto outCurves = new DiscreteCurve[curves.size()];
//	for (int i = 0; i < curves.size(); i++)
//	{
//		outCurves[i] = *(curves[i]);
//	}
//	return std::make_tuple(outCurves, (int)curves.size());
//}

PointBuffer* PointCloud::Positions()
{
   return ((PointCloudImpl*)m_pImpl)->Positions();
}

NormalBuffer* PointCloud::Normals()
{
   return ((PointCloudImpl*)m_pImpl)->Normals();
}

void* PointCloud::Implementation()
{
   return m_pImpl;
}

void PointCloud::Save(const char* filename)
{
	return ((PointCloudImpl*)m_pImpl)->Save(filename);
}
/*-----------------------------------------------------------------------------------*/
