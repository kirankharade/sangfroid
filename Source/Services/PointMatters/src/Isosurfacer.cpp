﻿#include "Isosurfacer.h"

#include "MathUtils.h"
#include "FileUtilities.h"
#include "GeoElementsUtils.h"
#include "SurfaceExtractor.h"
#include "PointCloud.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include <map>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::PointMatters;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::GenericUtils;


#pragma warning(disable:4244 4996)

/*-----------------------------------------------------------------------------------*/

double CalculateMinSpacing(const std::vector<Point3f>& points)
{
    auto numPoints = points.size();

    auto p1 = points[0];
    auto p2 = points[numPoints - 1];
    auto p3 = points[(size_t)numPoints / 2];

    auto d =
        get<1>(MathUtils::NearestPoint(p1, points)) +
        get<1>(MathUtils::NearestPoint(p2, points)) +
        get<1>(MathUtils::NearestPoint(p3, points));

    d /= 3.0;

    return d;
}
/*-----------------------------------------------------------------------------------*/

TriangleMesh* Isosurfacer::Process(PointBuffer* buffer, float gridCellSize, const char* stlFileName)
{
   auto points = PointBuffer::PointsFromBuffer(buffer);
   auto spacing = gridCellSize;
   std::vector<Point3f> xPoints;
   for (size_t i = 0; i < points.size(); i++)
   {
       xPoints.push_back(points[i]);
   }
   auto cv = CloudVolume::GenerateCloudVolume(buffer->Min(), buffer->Max(), spacing, xPoints);

   UltraVector<PointTriangle> triangles;
   SurfaceExtractor::March(cv, triangles);

   delete cv;

    if (nullptr != stlFileName)
    {
       GeoElementsUtils::WriteTriangleListToSTL(triangles, std::string(stlFileName));
    }

    return nullptr;
}
/*-----------------------------------------------------------------------------------*/

