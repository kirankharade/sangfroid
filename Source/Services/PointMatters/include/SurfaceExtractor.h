﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "Point3f.h"
#include "Containers/UltraVector.h"
#include "CloudVolume.h"
#include "PointTriangle.h"
#include <vector>
#include <map>

POINT_MATTERS_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/

class SurfaceExtractor
{

public:

   struct Cell
   {
      Sangfroid::Maths::Point3f corners[8];
      float values[8];
   };

public:

   static void March(const CloudVolume* volume, UltraVector<Sangfroid::Maths::PointTriangle>& triangleList);

private:

   static int ProcessCell(const SurfaceExtractor::Cell& cell, float isoValue, Sangfroid::Maths::PointTriangle*& triangles);

   static Sangfroid::Maths::Point3f Interpolate(const float isoValue,
      const Sangfroid::Maths::Point3f& p1,
      const Sangfroid::Maths::Point3f& p2,
      const float val1, const float val2);

   static int m_sEdgeTable[256];
   static int m_sTriangleTable[256][16];

};
/*-----------------------------------------------------------------------------------*/
POINT_MATTERS_NAMESPACE_END
/*-----------------------------------------------------------------------------------*/
