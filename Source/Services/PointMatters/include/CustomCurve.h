﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "DiscreteCurve.h"

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

struct CustomCurve : DiscreteCurve
{
    public:

		CustomCurve() {}

		CustomCurve(const std::vector<Sangfroid::Maths::Point3f>& points);

		virtual ~CustomCurve() {}

		double AverageZ() const { return m_avgZ; }

		double AverageR() const { return m_avgR; }

		Vector3f StartDir() const { return m_startDir; }

		Vector3f EndDir() const { return m_endDir; }

		double StartAngle() const { return m_startAngle; }

		double EndAngle() const { return m_endAngle; }


protected:

	double			m_avgZ;
	double			m_avgR;
	Vector3f		m_startDir;
	Vector3f		m_endDir;
	double			m_startAngle;
	double			m_endAngle;
};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END

