﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointBuffer.h"
#include "NormalBuffer.h"
#include "TriangleMesh.h"
#include "DiscreteCurve.h"

using namespace std;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class POINT_MATTERS_IMP_EXP PointCloud
{
private:

   void*  	m_pImpl;

public:

   PointCloud(Sangfroid::GeometryStructures::PointBuffer* posBuffer, Sangfroid::GeometryStructures::NormalBuffer* normalBuffer = nullptr);

   ~PointCloud();

   Sangfroid::GeometryStructures::TriangleMesh* ToMesh(const char* outDirectoryPath, bool generateMissingCurves);

   static std::vector<Sangfroid::GeometryStructures::TriangleMesh*> ToMesh(std::vector<PointCloud*> clouds, const char* outDirectoryPath, bool generateMissingCurves);

   std::tuple<Sangfroid::GeometryStructures::DiscreteCurve*, int> CloudToZCurves();

   Sangfroid::GeometryStructures::PointBuffer* Positions();

   Sangfroid::GeometryStructures::NormalBuffer* Normals();

   void* Implementation();

   void Save(const char* filename);
};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END

