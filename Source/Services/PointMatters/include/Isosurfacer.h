﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointCloud.h"
#include "PointBuffer.h"
#include "TriangleMesh.h"

using namespace std;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class Isosurfacer
{
    public:

       static Sangfroid::GeometryStructures::TriangleMesh* Process(Sangfroid::GeometryStructures::PointBuffer* buffer, float gridCellSize, const char* stlFileName = nullptr);

};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END

