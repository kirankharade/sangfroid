﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointCloud.h"
#include "TriangleMesh.h"
#include "PointTriangle.h"
#include "CloudVolume.h"
#include "CustomCurve.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class MeshingUtils
{
    public:

		static UltraVector<PointTriangle> ConstructMesh(const std::vector<std::vector<Point3f*>>& intersectionGridCurves);
		static std::vector<std::vector<Point3f*>> GenerateMissingAreas(std::vector<std::vector<Point3f*>>& intersectionGridCurves);
		static std::vector<std::vector<Point3f*>> FindPlaneCurveIntersections(const std::vector<DiscreteCurve*>& curves, const std::vector<std::tuple<Point3f, Vector3f>>& planes);
		static std::vector<std::tuple<Point3f, Vector3f>> GetIntersectionPlanes(std::vector<CustomCurve*> curves);
		static std::vector<std::tuple<Point3f, Vector3f>> GetIntersectionPlanes(const double thetaStart, const double thetaEnd, const int stepsCount);
		static std::tuple<Point3f, Vector3f> GetIntersectionPlane(const double theta);
		static bool IsCurveCCW(const DiscreteCurve* c);
		static bool IsAngleWithinRange(double start, double end, double value);
};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END

