/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "PointMattersNamespace.h"

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

#define SAFE_DELETE(x) if (x!= NULL) { delete x; x = NULL; }
#define SAFE_ARRAY_DELETE(x) if (x != NULL) 	{ delete[] x; x = NULL; } 
#define STD_TOLERANCE 0.000001

/*-----------------------------------------------------------------------------------*/

//--------------------------------------------------------
//Definitions, local static helpers and template functions
//--------------------------------------------------------

#define TOLERANCE 0.0001f
#define TOLERANCE_SQUARED 0.00000001

#define DEG2RAD(x) (0.01745329251994329577 * (x))
#define RAD2DEG(x) (57.2957795130823208768 * (x))

#define EQF(x, y)                 (fabs(x - y) <= TOLERANCE)
#define GTEQF(x, y)               ((x >= y) || (EQF(x, y)))
#define LTEQF(x, y)               ((x <= y) || (EQF(x, y)))
#define EQTF(x, y, tolerance)     (fabs(x - y) <= tolerance)
#define GTEQTF(x, y, tolerance)   ((x >= y) || (EQTF(x, y, tolerance)))
#define LTEQTF(x, y, tolerance)   ((x <= y) || (EQTF(x, y, tolerance)))

template <typename T>
inline T MAX3(T a, T b, T c)
{
   T x = MAX(a, b);
   return MAX(x, c);
}

template <typename T>
inline T MIN3(T a, T b, T c)
{
   T x = MIN(a, b);
   return MIN(x, c);
}

template <typename T>
inline T MAX4(T a, T b, T c, T d)
{
   T x = MAX(a, b);
   T y = MAX(c, d);
   return MAX(x, y);
}

template <typename T>
inline T MIN4(T a, T b, T c, T d)
{
   T x = MIN(a, b);
   T y = MIN(c, d);
   return MIN(x, y);
}

template <typename T>
inline T CLAMP(T val, T low, T high)
{
   return MIN(MAX(val, low), high);
}

template <typename T>
inline int ROUND(T val)
{
   return (int)(val + (T)0.5);
}

inline double ToDegrees(double radians)
{
   return RAD2DEG(radians);
}

inline double ToRadians(double degrees)
{
   return DEG2RAD(degrees);
}

POINT_MATTERS_NAMESPACE_END


