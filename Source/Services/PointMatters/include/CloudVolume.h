﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "Containers/UltraVector.h"
#include "SpacePartitioning\PointUnifier.h"
#include <vector>
#include <map>

using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

POINT_MATTERS_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/

struct CellIntersectionPair
{
   int CellIndex;
   int IntersectionPointIndex;
};
/*-----------------------------------------------------------------------------------*/

struct GridEdge
{
   int v0;
   int v1;
   std::vector<CellIntersectionPair> EdgeIntersections;
   Sangfroid::Maths::Point3f MeanIntersectionPoint;
   bool MeanIntsecCalculated;

   GridEdge(int a, int b)
      :v0(a), v1(b), MeanIntsecCalculated(false)
   {
      if (v0 > v1)
      {
         auto temp = v0;
         v0 = v1;
         v1 = temp;
      }
   }

   GridEdge()
      :v0(-1), v1(-1), MeanIntsecCalculated(false)
   {
   }

   bool operator< (const GridEdge& other) const
   {
      if (v0 < other.v0) return true;
      if (v0 > other.v0) return false;
      if (v1 < other.v1) return true;
      if (v1 > other.v1) return false;

      return false;
   }
};
/*-----------------------------------------------------------------------------------*/

struct IndexTriplet
{
   int i;
   int j;
   int k;

   IndexTriplet(int a, int b, int c)
      :i(a), j(b), k(c)
   {
   }

   bool operator< (const IndexTriplet& other) const
   {
      if (i < other.i) return true;
      if (i > other.i) return false;
      if (j < other.j) return true;
      if (j > other.j) return false;
      if (k < other.k) return true;
      if (k > other.k) return false;

      return false;
   }
};
/*-----------------------------------------------------------------------------------*/

struct GridCell
{
   IndexTriplet               Triplet;
   int                        CornerIndices[8];
   int                        Edges[12];
   std::vector<int>           ContainedPoints;
   Sangfroid::Maths::Point3f    FittedPlanePoint;
   Sangfroid::Maths::Vector3f   FittedPlaneNormal;
   float                      FittingVariance;
   std::vector<int>           Intersections;

   GridCell()
      :Triplet(-1, -1, -1)
   {
      for (auto e : Edges) e = -1;
      for (auto c : CornerIndices) c = -1;
   }
};

/*-----------------------------------------------------------------------------------*/
typedef std::map<IndexTriplet, std::vector<Sangfroid::Maths::Point3f>> IndexTripletPointsMap;
typedef std::map<GridEdge, int> GridEdgeMap;
typedef std::map<int, GridEdge> ReverseGridEdgeMap;
/*-----------------------------------------------------------------------------------*/

struct CloudVolume
{
   Sangfroid::Maths::Point3f Min;
   Sangfroid::Maths::Point3f Max;
   float                   Spacing[3];
   int                     Nsteps[3];
   UltraVector<GridCell>   GridCells;
   IndexTripletPointsMap   CellMap;
   PointUnifier*           VtxUnifier;
   GridEdgeMap             EdgeMap;
   ReverseGridEdgeMap      RevEdgeMap;
   UltraVector<Sangfroid::Maths::Point3f> UnifiedVertices;

   PointUnifier*          EdgeIntersectionsUnifier;
   UltraVector<Sangfroid::Maths::Point3f> EdgeIntersections;

   //Methods

   CloudVolume();

   void PopulateMap(const std::vector<Sangfroid::Maths::Point3f>& points);

   static CloudVolume* GenerateCloudVolume(
            const Sangfroid::Maths::Point3f& min,
            const Sangfroid::Maths::Point3f& max,
            const float spacing,
            const std::vector<Sangfroid::Maths::Point3f>& points);

   void ConvertTripletsToCells();

   void generateCellEdges(GridCell& gc);

   int addToEdgeMap(const GridEdge& ge);

};
/*-----------------------------------------------------------------------------------*/
POINT_MATTERS_NAMESPACE_END
/*-----------------------------------------------------------------------------------*/


