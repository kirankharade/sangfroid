﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointCloud.h"
#include "TriangleMesh.h"
#include "CustomCurve.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class CustomCloudMesher
{
    public:

		static std::vector<DiscreteCurve*> CloudToZCurves(PointBuffer * buffer);

		static TriangleMesh* Mesh(std::vector<CustomCurve*> curves, const char* stlFileName, bool generateMissingCurves);

	private:

};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END

