﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointCloud.h"
#include "TriangleMesh.h"
#include "CloudVolume.h"
#include "KLCurve.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/
typedef std::vector<KLCurve*> Curves;

struct Sector
{
	int _sectorIndex;
	Curves _curves;
	Curves _intersectionCurves;
	double _startAngle;
	double _endAngle;

	Sector()
	{
		_sectorIndex = -1;
		_startAngle = 0;
		_endAngle = 0;
	}
};
/*-----------------------------------------------------------------------------------*/
typedef std::vector<Sector*> Sectors;
/*-----------------------------------------------------------------------------------*/

class POINT_MATTERS_IMP_EXP KLMesher
{
    public:

		static void Mesh(PointBuffer* buffer, const char* outputFile);

};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END
