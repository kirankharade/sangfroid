/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

/*-----------------------------------------------------------------------------------*/

#ifdef POINT_MATTERS_EXPORTS
#define POINT_MATTERS_IMP_EXP __declspec(dllexport)
#else
#define POINT_MATTERS_IMP_EXP __declspec(dllimport)
#endif

/*-----------------------------------------------------------------------------------*/

