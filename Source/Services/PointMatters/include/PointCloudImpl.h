﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "PointMattersNamespace.h"
#include "PointMattersExportsDefs.h"
#include "PointBuffer.h"
#include "PointCloud.h"
#include "TriangleMesh.h"
#include "SpacePartitioning\PointOctree.h"
#include "DiscreteCurve.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;

POINT_MATTERS_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class PointCloudImpl
{
    public:

       PointCloudImpl(Sangfroid::GeometryStructures::PointBuffer* posBuffer, 
           Sangfroid::GeometryStructures::NormalBuffer* normalBuffer);

       ~PointCloudImpl();

       Sangfroid::GeometryStructures::TriangleMesh* Isosurfaces(const char* stlFileName = nullptr);

       Sangfroid::GeometryStructures::TriangleMesh* ToMesh(const char* stlFileName, bool generateMissingCurves);

	   std::vector<Sangfroid::GeometryStructures::DiscreteCurve*> CloudToZCurves();

       Sangfroid::GeometryStructures::PointBuffer* Positions();

       Sangfroid::GeometryStructures::NormalBuffer* Normals();

       Sangfroid::GeometryStructures::PointOctree* Octree();

	   void Save(const char* filename);

private:

   void fillUpOctree();

private:

    Sangfroid::GeometryStructures::PointBuffer*  m_posBuffer;
    Sangfroid::GeometryStructures::NormalBuffer* m_normalBuffer;
    Sangfroid::GeometryStructures::PointOctree*  m_pOctree;
};
/*-----------------------------------------------------------------------------------*/

POINT_MATTERS_NAMESPACE_END

