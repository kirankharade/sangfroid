﻿#pragma once

#include <iostream>
#include <fstream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "TriangleMeshImpl.h"
#include "IndexedTriangle.h"
#include "PointBuffer.h"
#include "PointTriangleOctree.h"
#include "PointTriangleOctreeBox.h"
#include "TriangleEdge.h"
#include "MathsDefines.h"
#include "MathUtils.h"
#include <vector>
#include <algorithm>
#include <functional>
#include <set>
#include <map>

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::Maths;

/*-----------------------------------------------------------------------------------*/
int TriangleMeshImpl::s_meshCount = 0;
/*-----------------------------------------------------------------------------------*/

TriangleMeshImpl::TriangleMeshImpl( PointBuffer* posBuffer, 
                                    NormalBuffer* normalBuffer, 
                                    IndexBuffer* indices)
:m_posBuffer(posBuffer),
   m_normalBuffer(normalBuffer),
   m_indexBuffer(indices),
   m_pOctree(nullptr),
   m_numTriangles(0),
   m_Id(s_meshCount++),
   m_boundaryPatch(nullptr)
{
   init();
}
/*-----------------------------------------------------------------------------------*/

TriangleMeshImpl::~TriangleMeshImpl()
{
}
/*-----------------------------------------------------------------------------------*/

int TriangleMeshImpl::Id() const
{
   return m_Id;
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::init()
{
   if (!m_indexBuffer)
   {
      m_numTriangles = m_posBuffer->PointCount() / 3;
      generateIndices();
   }
   else
   {
      m_numTriangles = m_indexBuffer->TriangleCount();
   }
   if (!m_normalBuffer)
   {
      generateNormals();
   }
}
/*-----------------------------------------------------------------------------------*/

PointBuffer* TriangleMeshImpl::Points()
{
   return m_posBuffer;
}
/*-----------------------------------------------------------------------------------*/

NormalBuffer* TriangleMeshImpl::Normals()
{
	return m_normalBuffer;
}
/*-----------------------------------------------------------------------------------*/

IndexBuffer* TriangleMeshImpl::Indices()
{
	return m_indexBuffer;
}
/*-----------------------------------------------------------------------------------*/

int TriangleMeshImpl::TriangleCount() const
{
   return m_numTriangles;
}
/*-----------------------------------------------------------------------------------*/

std::vector<Sangfroid::Maths::Point3f> TriangleMeshImpl::FaceEdgePoints() const
{
   return m_edgePoints;
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::fillUpOctree()
{
   if (!m_pOctree)
   {
      int treeDepth = PointOctree::CalculateTreeDepth(m_posBuffer->PointCount());
      m_pOctree = new PointTriangleOctree(m_posBuffer, treeDepth, true);
   }
}
/*-----------------------------------------------------------------------------------*/

//void TriangleMeshImpl::generateTriangleList()
//{
//   m_triangleList.clear();
//
//   if (m_indexBuffer && m_indexBuffer->Data())
//   {
//      for (int it = 0; it < m_numTriangles; it++)
//      {
//         Triangle t(m_indexBuffer->i(it), m_indexBuffer->j(it), m_indexBuffer->k(it));
//         m_triangleList.push_back(t);
//      }
//   }
//}
///*-----------------------------------------------------------------------------------*/

IndexedTriangle* TriangleMeshImpl::TriangleAt(const int index)
{
   if (index >= (int)m_numTriangles || 
       index < 0 || 
       m_triangleList.size() != m_numTriangles)
   {
      throw "TriangleMesh: Index out of range.";
   }
   return m_triangleList[index];
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::generateIndices()
{
   if (m_indexBuffer)
   {
      delete m_indexBuffer;
   }
   m_indexBuffer = new IndexBuffer(nullptr, m_numTriangles);
   m_indexBuffer->Data() = new int[3 * m_numTriangles];
   auto numPoints = m_posBuffer->PointCount();
   for (int i = 0; i < numPoints; i++)
   {
      m_indexBuffer->Data()[i] = i;
   }
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::generateNormals()
{
   if (m_normalBuffer)
   {
      delete m_normalBuffer;
   }
   m_normalBuffer = new NormalBuffer(nullptr, m_numTriangles);
   m_normalBuffer->Data() = new float[3 * m_numTriangles];

   if (m_indexBuffer && m_indexBuffer->Data() != nullptr)
   {
      auto nptr = m_normalBuffer->Data();
      for (int it = 0; it < m_numTriangles; it++)
      {
         auto ib = m_indexBuffer;
         auto v0 = m_posBuffer->PointAt((int)ib->i(it));
         auto v1 = m_posBuffer->PointAt((int)ib->j(it));
         auto v2 = m_posBuffer->PointAt((int)ib->k(it));

         Vector3f vec1 = (v1 - v0);
         Vector3f vec2 = (v2 - v1);
         auto n = (vec1.cross(vec2)).unitVec();

         nptr[(it * 3) + 0] = n.i;
         nptr[(it * 3) + 1] = n.j;
         nptr[(it * 3) + 2] = n.k;
      }
   }
   else
   {
      auto nptr = m_normalBuffer->Data();
      for (int it = 0; it < m_numTriangles; it++)
      {
         auto v0 = m_posBuffer->PointAt((int)((it * 3) + 0));
         auto v1 = m_posBuffer->PointAt((int)((it * 3) + 1));
         auto v2 = m_posBuffer->PointAt((int)((it * 3) + 2));

         Vector3f vec1 = (v1 - v0);
         Vector3f vec2 = (v2 - v1);
         auto n = (vec1.cross(vec2)).unitVec();

         nptr[(it * 3) + 0] = n.i;
         nptr[(it * 3) + 1] = n.j;
         nptr[(it * 3) + 2] = n.k;
      }
   }
}
/*-----------------------------------------------------------------------------------*/

static bool arePointsSame(const PointBuffer* buffer, const int& i, const int& j)
{
   auto pi = buffer->PointAt(i);
   auto pj = buffer->PointAt(j);
   const float tolerance = 0.00001f;

   if ((abs(pi.x - pj.x) < tolerance) &&
      (abs(pi.y - pj.y) < tolerance) &&
      (abs(pi.z - pj.z) < tolerance))
   {
      return true;
   }

   return false;
}
/*-----------------------------------------------------------------------------------*/

int* TriangleMeshImpl::getUniqueVertexIndices()
{
   if (!m_pOctree)
   {
      fillUpOctree();
   }

   auto vertexCount = m_posBuffer->PointCount();
   auto uniqueVtxIdxBuffer = new int[vertexCount];

   for (int i = 0; i < vertexCount; i++)
   {
      uniqueVtxIdxBuffer[i] = i;
   }

   auto ptr = &uniqueVtxIdxBuffer[0];

   for (int i = 0; i < vertexCount; i++)
   {
      auto box = m_pOctree->BoxForPointIndex(i);
      if (box == nullptr)
      {
         continue;
      }
      const std::vector<int>& list = box->GetPoints();
      auto count = (int)list.size();
      for (int j = 0; j < count; j++)
      {
         auto index = list[j];

         if ((i != index) && (arePointsSame(m_posBuffer, i, index)))
         {
            if (ptr[index] > ptr[i])
            {
               ptr[index] = ptr[i];
            }
         }
      }
   }

   return uniqueVtxIdxBuffer;
}
/*-----------------------------------------------------------------------------------*/

std::vector<TriangleEdge> TriangleMeshImpl::extractEdges(const float angleDeg)
{
   if (m_boundaryPatch)
   {
      delete m_boundaryPatch;
   }
   m_boundaryPatch = new MeshBoundaryPatch();

   m_edgePoints.clear();

   if (!m_indexBuffer)  generateIndices();
   if (!m_normalBuffer) generateNormals();

   if (m_uniqueIndexBuffer != nullptr)
   {
      delete m_uniqueIndexBuffer;
      m_uniqueIndexBuffer = nullptr;
   }
   m_uniqueIndexBuffer = getUniqueVertexIndices();

   m_revEdgeMap.clear();
   m_edgeMap.clear();

   int currEdgeIndex = 0;
   int iv0 = 0, iv1 = 0, iv2 = 0;
   int offSet0 = 0, offSet1 = 0;

   auto& currIdxPtr = m_indexBuffer->Data();
   auto uniqueIdxPtr = &(m_uniqueIndexBuffer[0]);
   auto vertexCount = m_posBuffer->PointCount();

   m_triangleList.clear();

   for (int it = 0; it < m_numTriangles; it++)
   {
      auto idx0 = m_indexBuffer->i(it);
      auto idx1 = m_indexBuffer->j(it);
      auto idx2 = m_indexBuffer->k(it);

      iv0 = uniqueIdxPtr[idx0];
      iv1 = uniqueIdxPtr[idx1];
      iv2 = uniqueIdxPtr[idx2];

      if (iv0 >= (int)vertexCount || iv1 >= (int)vertexCount || iv2 >= (int)vertexCount)
      {
         continue;
      }

      int edge0Idx = TriangleEdge::AddToHalfEdgeMap(m_edgeMap, m_revEdgeMap, TriangleEdge(iv0, iv1), it, currEdgeIndex);
      int edge1Idx = TriangleEdge::AddToHalfEdgeMap(m_edgeMap, m_revEdgeMap, TriangleEdge(iv1, iv2), it, currEdgeIndex);
      int edge2Idx = TriangleEdge::AddToHalfEdgeMap(m_edgeMap, m_revEdgeMap, TriangleEdge(iv2, iv0), it, currEdgeIndex);

      IndexedTriangle* t = new IndexedTriangle(iv0, iv1, iv2);
      t->ea = edge0Idx;
      t->eb = edge1Idx;
      t->ec = edge2Idx;

      m_triangleList.push_back(t);
   }

   //Separation now starts...

   int totalEdgeCount = (int)m_revEdgeMap.size();
   float dot = 0, angle = 0;
   float thresholdRad = (float) DEG2RAD(angleDeg);

   std::vector<TriangleEdge> faceSeparatingEdges;
   std::set<int> boundaryTriangles;

   for (int i = 0; i < totalEdgeCount; i++)
   {
      TriangleEdge& e = m_revEdgeMap[i];

      if (e.ta > -1 && e.tb > -1)
      {
         Vector3f& na = m_normalBuffer->NormalAt(e.ta);
         Vector3f& nb = m_normalBuffer->NormalAt(e.tb);
         na.normalize();
         nb.normalize();

         dot = (na.i * nb.i) + (na.j * nb.j) + (na.k * nb.k);
         if (dot < -1.0f) dot = -1.0f;
         if (dot > 1.0f) dot = 1.0f;
         angle = acos(dot);

         if (angle > thresholdRad)
         {
            auto edPta = m_posBuffer->PointAt(e.va);
            auto edPtb = m_posBuffer->PointAt(e.vb);

            m_edgePoints.push_back(edPta);
            m_edgePoints.push_back(edPtb);

            e.faceBoundaryEdge = true;

            faceSeparatingEdges.push_back(e);

            boundaryTriangles.insert(e.ta);
            boundaryTriangles.insert(e.tb);
         }
      }
      else
      {
         //This is when the edge is shared by only one triangle.
         //i.e. The boundary of the open surface.
         if (e.va > -1 && e.vb > -1)
         {
            auto edPta = m_posBuffer->PointAt(e.va);
            auto edPtb = m_posBuffer->PointAt(e.vb);

            m_edgePoints.push_back(edPta);
            m_edgePoints.push_back(edPtb);

            e.faceBoundaryEdge = true;

            faceSeparatingEdges.push_back(e);
         }

         if (e.ta == -1)
         {
            boundaryTriangles.insert(e.tb);
         }
         if (e.tb == -1)
         {
            boundaryTriangles.insert(e.ta);
         }
      }
   }

   saveEdges(m_edgePoints);

   m_boundaryPatch->BoundaryEdges = faceSeparatingEdges;
   m_boundaryPatch->BoundaryTriangles.clear();
   for (auto itr = boundaryTriangles.begin(); itr != boundaryTriangles.end(); itr++)
   {
      auto triIndex = *itr;
      auto t = m_triangleList[triIndex];
      t->IsTriangleOnEdge = true;
      m_boundaryPatch->BoundaryTriangles.push_back(t);
   }
   m_boundaryPatch->ParentMesh = this;

   return faceSeparatingEdges;
}
/*-----------------------------------------------------------------------------------*/

TriangleEdgeList TriangleMeshImpl::extractOuterBoundary()
{
   if (m_boundaryPatch)
   {
      delete m_boundaryPatch;
   }
   m_boundaryPatch = new MeshBoundaryPatch();

   m_edgePoints.clear();

   if (!m_indexBuffer)  generateIndices();
   if (!m_normalBuffer) generateNormals();

   if (m_uniqueIndexBuffer != nullptr)
   {
      delete m_uniqueIndexBuffer;
      m_uniqueIndexBuffer = nullptr;
   }
   m_uniqueIndexBuffer = getUniqueVertexIndices();

   m_revEdgeMap.clear();
   m_edgeMap.clear();

   int currEdgeIndex = 0;
   int iv0 = 0, iv1 = 0, iv2 = 0;
   int offSet0 = 0, offSet1 = 0;

   auto& currIdxPtr = m_indexBuffer->Data();
   auto uniqueIdxPtr = &(m_uniqueIndexBuffer[0]);
   auto vertexCount = m_posBuffer->PointCount();

   m_triangleList.clear();

   for (int it = 0; it < m_numTriangles; it++)
   {
      auto idx0 = m_indexBuffer->i(it);
      auto idx1 = m_indexBuffer->j(it);
      auto idx2 = m_indexBuffer->k(it);

      iv0 = uniqueIdxPtr[idx0];
      iv1 = uniqueIdxPtr[idx1];
      iv2 = uniqueIdxPtr[idx2];

      if (iv0 >= (int)vertexCount || iv1 >= (int)vertexCount || iv2 >= (int)vertexCount)
      {
         continue;
      }

      int edge0Idx = TriangleEdge::AddToHalfEdgeMap(m_edgeMap, m_revEdgeMap, TriangleEdge(iv0, iv1), it, currEdgeIndex);
      int edge1Idx = TriangleEdge::AddToHalfEdgeMap(m_edgeMap, m_revEdgeMap, TriangleEdge(iv1, iv2), it, currEdgeIndex);
      int edge2Idx = TriangleEdge::AddToHalfEdgeMap(m_edgeMap, m_revEdgeMap, TriangleEdge(iv2, iv0), it, currEdgeIndex);

      IndexedTriangle* t = new IndexedTriangle(iv0, iv1, iv2);
      t->ea = edge0Idx;
      t->eb = edge1Idx;
      t->ec = edge2Idx;

      m_triangleList.push_back(t);
   }

   int totalEdgeCount = (int)m_revEdgeMap.size();

   TriangleEdgeList faceSeparatingEdges;
   std::set<int> boundaryTriangles;

   for (int i = 0; i < totalEdgeCount; i++)
   {
      TriangleEdge& e = m_revEdgeMap[i];

      if (e.ta == -1 || e.tb == -1)
      {
         //This is when the edge is shared by only one triangle.
         //i.e. The boundary of the open surface.
         if (e.va > -1 && e.vb > -1)
         {
            auto edPta = m_posBuffer->PointAt(e.va);
            auto edPtb = m_posBuffer->PointAt(e.vb);

            m_edgePoints.push_back(edPta);
            m_edgePoints.push_back(edPtb);

            e.faceBoundaryEdge = true;

            faceSeparatingEdges.push_back(e);
         }

         if (e.ta == -1)
         {
            boundaryTriangles.insert(e.tb);
         }
         if (e.tb == -1)
         {
            boundaryTriangles.insert(e.ta);
         }
      }
   }

   saveEdges(m_edgePoints);

   m_boundaryPatch->BoundaryEdges = faceSeparatingEdges;
   m_boundaryPatch->BoundaryTriangles.clear();
   for (auto itr = boundaryTriangles.begin(); itr != boundaryTriangles.end(); itr++)
   {
      auto triIndex = *itr;
      auto t = m_triangleList[triIndex];
      t->IsTriangleOnEdge = true;
      m_boundaryPatch->BoundaryTriangles.push_back(t);
   }
   m_boundaryPatch->ParentMesh = this;

   return faceSeparatingEdges;
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::expandEdgeConnections(TriangleEdge& edge, IndexedTriangleList& currTriangleList)
{
   if (edge.faceBoundaryEdge)
   {
      return;
   }

   auto& ta = m_triangleList[edge.ta];
   if (!ta->IsFacePartitioned)
   {
      expandTriangleConnectivity(ta, currTriangleList);
   }

   auto& tb = m_triangleList[edge.tb];
   if (!tb->IsFacePartitioned)
   {
      expandTriangleConnectivity(tb, currTriangleList);
   }
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::expandTriangleConnectivity(IndexedTriangle* t, IndexedTriangleList& currTriangleList)
{
   t->IsFacePartitioned = true;

   //Add current triangle...
   currTriangleList.push_back(t);

   auto& ea = m_revEdgeMap[t->ea];
   expandEdgeConnections(ea, currTriangleList);

   auto& eb = m_revEdgeMap[t->eb];
   expandEdgeConnections(eb, currTriangleList);

   auto& ec = m_revEdgeMap[t->ec];
   expandEdgeConnections(ec, currTriangleList);

}
/*-----------------------------------------------------------------------------------*/

std::vector<IndexedTriangleList> TriangleMeshImpl::separateIntoFaces(const TriangleEdgeList& faceSeparatingEdges)
{
   std::vector<IndexedTriangleList> separatedTriangulations;

   IndexedTriangleList currFaceTriangulation;

   for (auto& t : m_triangleList)
   {
      t->IsFacePartitioned = false;
   }

   for (size_t it = 0; it < m_triangleList.size(); it++)
   {
      auto& t = m_triangleList[it];

      t->ID = it;

      if (t->IsFacePartitioned)
      {
         continue;
      }

      //Not partitioned yet, this will be new seed triangle for a new face connectivity expansion
      if (currFaceTriangulation.size() != 0)
      {
         separatedTriangulations.push_back(currFaceTriangulation);
         currFaceTriangulation = IndexedTriangleList();
      }
      expandTriangleConnectivity(t, currFaceTriangulation);
   }

   m_separatedTriangulations = separatedTriangulations;

   return separatedTriangulations;
}
/*-----------------------------------------------------------------------------------*/

void TriangleMeshImpl::saveEdges(const std::vector<Point3f>& points)
{
   std::ofstream fs;
   fs.open("C:/FeatureSeparation/faceEdges.txt", std::ios::out);
   for (size_t i = 0; i < points.size(); i+=2)
   {
      auto& pt0 = points[i];
      auto& pt1 = points[i+1];

      fs << pt0.x << "\t" << pt0.y << "\t" << pt0.z << endl;
      fs << pt1.x << "\t" << pt1.y << "\t" << pt1.z << endl;
   }
   fs.close();
}
/*-----------------------------------------------------------------------------------*/

std::vector<TriangleMesh*> TriangleMeshImpl::DivideIntoFaceTriangulations(const float angleDeg)
{
   m_faceBoundingEdges = extractEdges(angleDeg);

   auto separatedTriangulations = separateIntoFaces(m_faceBoundingEdges);

   std::vector<TriangleMesh*> triangleMeshes;
   for (auto& st : separatedTriangulations)
   {
      auto mesh = convertTriangleListToMesh(st);
      triangleMeshes.push_back(mesh);
   }

   m_triangleMeshes = triangleMeshes;

   return triangleMeshes;
}
/*-----------------------------------------------------------------------------------*/

//PointOctree* TriangleMeshImpl::populateOctreeWithTriangles()
//{
//	auto pOctree = ((PointCloudImpl*)m_associatedCloud->Implementation())->Octree();
//   pOctree->SetTriangleCount(m_numTriangles);
//
//	for (int it = 0; it < m_numTriangles; it++)
//	{
//		auto iv0 = m_indexBuffer->i(it);
//		auto iv1 = m_indexBuffer->j(it);
//		auto iv2 = m_indexBuffer->k(it);
//
//		auto v0 = this->m_posBuffer->PointAt(iv0);
//		auto v1 = this->m_posBuffer->PointAt(iv1);
//		auto v2 = this->m_posBuffer->PointAt(iv2);
//
//		auto centroid = MathUtils::Centroid(v0, v1, v2);
//
//		pOctree->AddTriangle(centroid, it);
//	}
//   return pOctree;
//}
/*-----------------------------------------------------------------------------------*/

static std::vector<IndexedTriangleList> filterFaceMeshes(const std::vector<IndexedTriangleList>& inMeshes)
{
   std::vector<IndexedTriangleList> filteredMeshes;
	for (size_t im = 0; im < inMeshes.size(); im++)
	{
		auto& mesh = inMeshes[im];
		if (mesh.size() < 20)
		{
			continue;
		}
		filteredMeshes.push_back(mesh);
	}

   for (size_t im = 0; im < filteredMeshes.size(); im++)
   {
      auto& mesh = inMeshes[im];

      for (size_t it = 0; it < mesh.size(); it++)
      {
         auto& t = mesh[it];
         t->MeshIndex = (int)im;
      }
   }

	return filteredMeshes;
}

static const PointTriangleOctreeBox* parentBoxAtLevel(const PointTriangleOctreeBox* box, int level)
{
   auto tempBox = box;
   auto levelCount = 0;
   while (tempBox->GetParent() != nullptr && levelCount < level)
   {
      tempBox = (const PointTriangleOctreeBox*) ((const PointTriangleOctreeBox*)tempBox)->GetParent();
      levelCount++;
   }
   return tempBox;
}

static void triangleContained(const PointTriangleOctreeBox* box, std::vector<int>& triangles)
{
   if (box == nullptr)
   {
      return;
   }
   if (box->IsLeafBox())
   {
      auto& ts = box->GetTriangles();
      triangles.insert(triangles.begin(), ts.begin(), ts.end());
   }
   else
   {
      auto children = box->GetChildren();
      for (int i = 0; i < 8; i++)
      {
         const PointTriangleOctreeBox* tmpBox = (const PointTriangleOctreeBox*) children[i];
         triangleContained(tmpBox, triangles);
      }
   }
}

static std::vector<int> getNearbyTriangles(const PointTriangleOctreeBox* box)
{
   std::vector<int> triIndices;
   if (box == nullptr)
   {
      return triIndices;
   }

   auto upperBox = parentBoxAtLevel(box, 3);
   triangleContained(upperBox, triIndices);
   return triIndices;
}

static float NearestDistanceFromTriangle_Approximate(
   const Sangfroid::Maths::Point3f& pt,
   const Sangfroid::Maths::Point3f& v0,
   const Sangfroid::Maths::Point3f& v1,
   const Sangfroid::Maths::Point3f& v2)
{
   float minDist = FLT_MAX;

   auto centroid = MathUtils::Centroid(v0, v1, v2);
   float d = pt.dist(centroid);
   if (d < minDist)      minDist = d;

   d = pt.dist(v0);
   if (d < minDist)      minDist = d;

   d = pt.dist(v1);
   if (d < minDist)      minDist = d;

   d = pt.dist(v2);
   if (d < minDist)      minDist = d;

   auto m0 = MathUtils::MidPoint(v0, v1);
   d = pt.dist(m0);
   if (d < minDist)      minDist = d;

   auto m1 = MathUtils::MidPoint(v1, v2);
   d = pt.dist(m1);
   if (d < minDist)      minDist = d;

   auto m2 = MathUtils::MidPoint(v2, v0);
   d = pt.dist(m2);
   if (d < minDist)      minDist = d;

   return d;
}

IndexedTriangle* TriangleMeshImpl::getNearestTriangle(
   const Sangfroid::Maths::Point3f& pt,
   const std::vector<int>& nearbyTriangleIndices,
   Sangfroid::Maths::Vector3f& nearestTriangleNormal)
{
   IndexedTriangle* nearestTriangle = nullptr;
   nearestTriangleNormal = Vector3f(0, 0, 0);

   float PROXIMITY_THRESHOLD = 0.5f;

   float minDist = FLT_MAX;
   for (size_t it = 0; it < nearbyTriangleIndices.size(); it++)
   {
      auto triIndex = nearbyTriangleIndices[it];

      auto& t = m_triangleList[triIndex];

      //get rid of the triangles which are near the edge of the mesh
      if (t->IsTriangleOnEdge)
      {
         continue;
      }

      auto v0 = this->m_posBuffer->PointAt(t->i);
      auto v1 = this->m_posBuffer->PointAt(t->j);
      auto v2 = this->m_posBuffer->PointAt(t->k);

      auto n = MathUtils::NormalFrom3Points(v0, v1, v2);
      auto dist = NearestDistanceFromTriangle_Approximate(pt, v0, v1, v2);

      if (dist > PROXIMITY_THRESHOLD)
      {
         continue;
      }

      if (dist < minDist)
      {
         minDist = dist;
         nearestTriangle = t;
         nearestTriangleNormal = n;
      }
   }
   return nearestTriangle;
}

struct PointIndexNormal
{
   int PointIndex;
   Vector3f PointNormal;

   PointIndexNormal(int idx, Vector3f normal)
      :PointIndex(idx), PointNormal(normal)
   {
   }
};

//typedef std::vector<PointIndexNormal> PointNormalList;
//
//std::vector<PointCloud*> TriangleMeshImpl::SeparateCloudRegions(const float nearnessThreshold)
//{
//   std::map<int, PointCloud*, std::greater<int>> clouds;
//
//	auto octree = populateOctreeWithTriangles();
//   auto filteredMeshes = filterFaceMeshes(m_separatedTriangulations);
//
//   //Generate a list of pointlists each of which will represent a separated cloud.
//   std::vector<PointNormalList> separatedPointCloudList;
//   separatedPointCloudList.resize(filteredMeshes.size());
//
//   //Now for each point in the original cloud,...try find the nearby triangles...
//
//   auto origCloudBuffer = m_associatedCloud->Positions();
//   auto origPointCount = origCloudBuffer->PointCount();
//   auto pOctree = ((PointCloudImpl*)m_associatedCloud->Implementation())->Octree();
//
//   for (int i = 0; i < origPointCount; i++)
//   {
//      auto box = pOctree->BoxForPointIndex(i);
//      auto nearbyTriangles = getNearbyTriangles(box);
//      if (nearbyTriangles.size() == 0)
//      {
//         continue;
//      }
//      auto pt = origCloudBuffer->PointAt(i);
//      Vector3f nearestTriangleNormal;
//      auto nearestTriangle = getNearestTriangle(pt, nearbyTriangles, nearestTriangleNormal);
//      if (nearestTriangle == nullptr)
//      {
//         continue;
//      }
//      auto meshIndex = nearestTriangle->MeshIndex;
//      if (meshIndex < separatedPointCloudList.size())
//      {
//         PointIndexNormal pin(i, nearestTriangleNormal);
//         separatedPointCloudList[meshIndex].push_back(pin);
//      }
//   }
//
//   for (auto& pc : separatedPointCloudList)
//   {
//      auto pointCount = (int) pc.size();
//      auto coords = new float[3 * pointCount];
//      auto normals = new float[3 * pointCount];
//
//      for (int ip = 0; ip < pointCount; ip++)
//      {
//         auto pointIndex = pc[ip].PointIndex;
//         auto pointNormal = pc[ip].PointNormal;
//
//         auto pt = origCloudBuffer->PointAt(pointIndex);
//         coords[(3 * ip) + 0] = pt.x;
//         coords[(3 * ip) + 1] = pt.y;
//         coords[(3 * ip) + 2] = pt.z;
//
//         normals[(3 * ip) + 0] = pointNormal.i;
//         normals[(3 * ip) + 1] = pointNormal.j;
//         normals[(3 * ip) + 2] = pointNormal.k;
//      }
//
//      auto ptBuffer = new PointBuffer(coords, (int) pointCount);
//      auto nmBuffer = new NormalBuffer(normals, (int) pointCount);
//      auto pc = new PointCloud(ptBuffer, nmBuffer);
//
//      clouds.insert(std::make_pair(pointCount, pc));
//   }
//
//   std::vector<PointCloud*> separated;
//   for (auto& itr = clouds.begin(); itr != clouds.end(); itr++)
//   {
//      separated.push_back(itr->second);
//   }
//
//   return separated;
//}
/*-----------------------------------------------------------------------------------*/

TriangleMesh* TriangleMeshImpl::convertTriangleListToMesh(const IndexedTriangleList& triangleList)
{
   TriangleMesh* mesh = nullptr;

   auto triangleCount = triangleList.size();
   auto posBuffer = m_posBuffer;
   auto  normalBuffer = m_normalBuffer;
   auto indexBuffer = new IndexBuffer(nullptr, (int) triangleCount);
   indexBuffer->Data() = new int[3 * triangleCount];
   auto idxPtr = indexBuffer->Data();

   //This new triangulation will share the same position and normal buffer but a different index buffer
   for (size_t it = 0; it < triangleCount; it++)
   {
      auto& t = triangleList[it];

      idxPtr[(3 * it) + 0] = t->i;
      idxPtr[(3 * it) + 1] = t->j;
      idxPtr[(3 * it) + 2] = t->k;
   }

   mesh = new TriangleMesh(m_posBuffer, m_normalBuffer, indexBuffer);

   return mesh;
}
/*-----------------------------------------------------------------------------------*/

TriangleMesh* TriangleMeshImpl::RemoveBigTriangles(float thresholdArea, bool unifyVertices)
{
   std::vector<int> trianglesToBeRetained;
   std::set<int> leftVertices;

   for (int it = 0; it < m_numTriangles; it++)
   {
      auto iv0 = m_indexBuffer->i(it);
      auto iv1 = m_indexBuffer->j(it);
      auto iv2 = m_indexBuffer->k(it);

      auto v0 = this->m_posBuffer->PointAt(iv0);
      auto v1 = this->m_posBuffer->PointAt(iv1);
      auto v2 = this->m_posBuffer->PointAt(iv2);

      auto area = 0.5f * ((v1 - v0).cross(v2 - v1)).length();
      auto sideA = (v1 - v0).length();
      auto sideB = (v2 - v1).length();
      auto sideC = (v0 - v2).length();

      auto largestSide = (sideA > sideB) ? sideA : sideB;
      largestSide = (largestSide > sideC) ? largestSide : sideC;
      
      if (area < thresholdArea && largestSide < 0.5)
      {
         trianglesToBeRetained.push_back(it);
         if (unifyVertices)
         {
            leftVertices.insert(iv0);
            leftVertices.insert(iv1);
            leftVertices.insert(iv2);
         }
      }
   }

   auto newTriangleCount = trianglesToBeRetained.size();
   float* newPosBuffer = nullptr;
   int* newIndexBuffer = nullptr;

   if (unifyVertices)
   {
      //These are the total vetices which are left after triangle deletion...
      //So allocate them some buffer...
      auto leftVertexCount = leftVertices.size();
      newPosBuffer = new float[leftVertexCount * 3];

      //now map old indices to new indices
      std::map<int, int> old2NewIndexMap;
      auto newIndexCount = 0;
      for (auto& itr = leftVertices.cbegin(); itr != leftVertices.cend(); itr++)
      {
         int oldIndex = *itr;
         old2NewIndexMap.insert(std::make_pair(oldIndex, newIndexCount));

         //Also fill up the position buffer using new indices...
         auto pt = m_posBuffer->PointAt(oldIndex);
         newPosBuffer[(3 * newIndexCount) + 0] = pt.x;
         newPosBuffer[(3 * newIndexCount) + 1] = pt.y;
         newPosBuffer[(3 * newIndexCount) + 2] = pt.z;

         newIndexCount++;
      }

      //Allocate the index buffer for the left triangles
      newIndexBuffer = new int[newTriangleCount * 3];

      //Now iterate over the retained triangles, and convert 
      //their old indices into corresponding new indices
      for (size_t it = 0; it < trianglesToBeRetained.size(); it++)
      {
         auto tIndex = trianglesToBeRetained[it];
         auto iv0 = m_indexBuffer->i(tIndex);
         auto iv1 = m_indexBuffer->j(tIndex);
         auto iv2 = m_indexBuffer->k(tIndex);

         auto iv0New = old2NewIndexMap[iv0];
         auto iv1New = old2NewIndexMap[iv1];
         auto iv2New = old2NewIndexMap[iv2];

         newIndexBuffer[(3 * it) + 0] = iv0New;
         newIndexBuffer[(3 * it) + 1] = iv1New;
         newIndexBuffer[(3 * it) + 2] = iv2New;
      }
      old2NewIndexMap.clear();
      leftVertices.clear();
   }
   else
   {
      newPosBuffer = new float[newTriangleCount * 9];
      newIndexBuffer = new int[newTriangleCount * 3];

      for (size_t it = 0; it < trianglesToBeRetained.size(); it++)
      {
         auto tIndex = trianglesToBeRetained[it];

         auto iv0 = m_indexBuffer->i(tIndex);
         auto iv1 = m_indexBuffer->j(tIndex);
         auto iv2 = m_indexBuffer->k(tIndex);

         auto pt0 = m_posBuffer->PointAt(iv0);
         auto pt1 = m_posBuffer->PointAt(iv1);
         auto pt2 = m_posBuffer->PointAt(iv2);

         auto posOffset = 9 * it;

         //Position buffer
         newPosBuffer[posOffset + 0] = pt0.x;
         newPosBuffer[posOffset + 1] = pt0.y;
         newPosBuffer[posOffset + 2] = pt0.z;

         newPosBuffer[posOffset + 3] = pt1.x;
         newPosBuffer[posOffset + 4] = pt1.y;
         newPosBuffer[posOffset + 5] = pt1.z;

         newPosBuffer[posOffset + 6] = pt2.x;
         newPosBuffer[posOffset + 7] = pt2.y;
         newPosBuffer[posOffset + 8] = pt2.z;

         //Index buffer
         auto indexffset = 3 * it;
         newIndexBuffer[indexffset + 0] = indexffset + 0;
         newIndexBuffer[indexffset + 1] = indexffset + 1;
         newIndexBuffer[indexffset + 2] = indexffset + 2;
      }
   }

   trianglesToBeRetained.clear();

   auto newPositions = new PointBuffer(newPosBuffer, (int) (newTriangleCount * 3));
   auto newIndices = new IndexBuffer(newIndexBuffer, (int) (newTriangleCount));

   TriangleMesh* newMesh = new TriangleMesh(newPositions, nullptr, newIndices);

   return newMesh;
}
/*-----------------------------------------------------------------------------------*/

bool TriangleMeshImpl::WriteToSTL(const char* stlFilePath)
{
   if (m_posBuffer == nullptr || stlFilePath == nullptr || m_numTriangles < 0)
   {
      return false;
   }

   try
   {
      if (!m_indexBuffer)  generateIndices();
      if (!m_normalBuffer) generateNormals();

      int numIndices = 3 * m_numTriangles;

      FILE* pFile = std::fopen(stlFilePath, "wb");
      if (!pFile)
      {
         throw "The STL file cannot be written.";
      }

      fprintf(pFile, "%-80.80s", "STL");

      fwrite(&m_numTriangles, sizeof(int), 1, pFile);

      short filler = 0;
      float n[3];

      for (int it = 0; it < (int) m_numTriangles; it++)
      {
         auto iv0 = m_indexBuffer->i(it);
         auto iv1 = m_indexBuffer->j(it);
         auto iv2 = m_indexBuffer->k(it);

         auto v0 = m_posBuffer->PointAt(iv0);
         auto v1 = m_posBuffer->PointAt(iv1);
         auto v2 = m_posBuffer->PointAt(iv2);

         Vector3f v = (v1 - v0).cross(v2 - v1);
         Vector3f nm = v.unitVec();
         n[0] = nm.i;
         n[1] = nm.j;
         n[2] = nm.k;

         fwrite(&n[0], sizeof(float), 3, pFile);

         fwrite(&v0, sizeof(float), 3, pFile);
         fwrite(&v1, sizeof(float), 3, pFile);
         fwrite(&v2, sizeof(float), 3, pFile);

         fwrite(&filler, sizeof(short), 1, pFile);
      }

      std::fflush(pFile);
      std::fclose(pFile);
   }
   catch (...)
   {
      return false;
   }
   return true;
}
/*-----------------------------------------------------------------------------------*/

UltraVector<Sangfroid::Maths::PointTriangle> TriangleMeshImpl::ToTriangleList() const
{
	UltraVector<Sangfroid::Maths::PointTriangle> triangleList;

	for (int it = 0; it < (int)m_numTriangles; it++)
	{
		auto iv0 = m_indexBuffer->i(it);
		auto iv1 = m_indexBuffer->j(it);
		auto iv2 = m_indexBuffer->k(it);

		auto v0 = m_posBuffer->PointAt(iv0);
		auto v1 = m_posBuffer->PointAt(iv1);
		auto v2 = m_posBuffer->PointAt(iv2);

		triangleList.push_back(Sangfroid::Maths::PointTriangle(v0, v1, v2));
	}

	return triangleList;
}
/*-----------------------------------------------------------------------------------*/

static std::vector<Point3f> getSampledPoints(const float stepSize, const Point3f& v0, const Point3f& v1)
{
   auto vecLength = (v1 - v0).length();
   auto edgeDir = (v1 - v0).unitVec();
   auto numSteps = (int)( vecLength / stepSize);
   auto stepLength = vecLength;
   if (numSteps > 0)
   {
      stepLength = (float)(vecLength / numSteps);
   }
   std::vector<Point3f> sampledPoints;
   for (int i = 1; i < numSteps; i++)
   {
      auto v = v0 + (edgeDir * stepLength * (float) i);
      sampledPoints.push_back(v);
   }
   sampledPoints.push_back(v1);
   return sampledPoints;
}
/*-----------------------------------------------------------------------------------*/

//void TriangleMeshImpl::GenerateCloudByFaceSampling(const char* filename, int level, Sangfroid::Maths::CTransform4d& transform)
//{
//   auto min = this->m_posBuffer->Min();
//   auto max = this->m_posBuffer->Max();
//   auto diagLength = (max - min).Mag();
//   auto stepSize = diagLength / (float)level;
//
//   std::ofstream fs;
//   fs.open(filename, std::ios::out);
//
//   for (int it = 0; it < m_numTriangles; it++)
//   {
//      auto iv0 = m_indexBuffer->i(it);
//      auto iv1 = m_indexBuffer->j(it);
//      auto iv2 = m_indexBuffer->k(it);
//
//      auto v0 = m_posBuffer->PointAt(iv0);
//      auto v1 = m_posBuffer->PointAt(iv1);
//      auto v2 = m_posBuffer->PointAt(iv2);
//
//      v0 = v0.GetTransformed(transform);
//      v1 = v1.GetTransformed(transform);
//      v2 = v2.GetTransformed(transform);
//
//      fs << v0.x << "\t" << v0.y << "\t" << v0.z << endl;
//
//      auto sampledEdgePoints = getSampledPoints(stepSize, v1, v2);
//
//      for (auto& spa: sampledEdgePoints)
//      {
//         auto sampled = getSampledPoints(stepSize, v0, spa);
//         fs << spa.x << "\t" << spa.y << "\t" << spa.z << endl;
//
//         for (auto& spb : sampled)
//         {
//            fs << spb.x << "\t" << spb.y << "\t" << spb.z << endl;
//         }
//      }
//   }
//   fs.close();
//}
/*-----------------------------------------------------------------------------------*/

TriangleMesh* TriangleMeshImpl::BoundaryTriangles()
{
   IndexedTriangleList b;

   for (size_t it = 0; it < m_triangleList.size(); it++)
   {
      auto& t = m_triangleList[it];
      //get rid of the triangles which are near the edge of the mesh
      if (t->IsTriangleOnEdge)
      {
         b.push_back(t);
      }
   }

   auto mesh = convertTriangleListToMesh(b);
   return mesh;
}
/*-----------------------------------------------------------------------------------*/


