﻿#pragma once

#include <iostream>
#include <vector>
#include "DiscreteCurve.h"
#include "MathsDefines.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::Maths;

/*-----------------------------------------------------------------------------------*/

DiscreteCurve::DiscreteCurve(const std::vector<Sangfroid::Maths::Point3f>& points)
	:m_points(points),
	m_calculatedBounds(false)
{
}

DiscreteCurve::~DiscreteCurve() 
{
}

void DiscreteCurve::CalculateBounds()
{
	if (!m_calculatedBounds)
	{
		Sangfroid::Maths::Point3f min(FLT_MAX, FLT_MAX, FLT_MAX);
		Sangfroid::Maths::Point3f max(-FLT_MAX, -FLT_MAX, -FLT_MAX);

		for (size_t i = 0; i < m_points.size(); i++)
		{
			auto& pt = m_points[i];

			auto x = pt.x;
			auto y = pt.y;
			auto z = pt.z;

			if (x < min.x)	min.x = x;
			if (y < min.y)	min.y = y;
			if (z < min.z)	min.z = z;

			if (x >= max.x)	max.x = x;
			if (y >= max.y)	max.y = y;
			if (z >= max.z)	max.z = z;
		}

		m_min = min;
		m_max = max;

		m_calculatedBounds = true;
	}
}


const std::vector<Sangfroid::Maths::Point3f>& DiscreteCurve::Points() const
{
   return m_points;
}

Sangfroid::Maths::Point3f DiscreteCurve::PointAt(const int& index) const
{
   if (index >= (int) m_points.size() || index < 0)
   {
      throw "DiscreteCurve: Point-index out of range...";
   }
   return m_points[index];
}

Sangfroid::Maths::Point3f DiscreteCurve::LastPoint() const
{
	if (m_points.size() <= 0)
	{
		throw "DiscreteCurve: Empty curve. Cannot find last point...";
	}
	return m_points[m_points.size() - 1];
}

Sangfroid::Maths::Point3f DiscreteCurve::FirstPoint() const
{
	if (m_points.size() == 0)
	{
		throw "DiscreteCurve: Empty curve. Cannot find first point...";
	}
	return m_points[0];

}

void DiscreteCurve::InsertAtStart(const Sangfroid::Maths::Point3f& point)
{
	m_points.insert(m_points.begin(), point);
}

void DiscreteCurve::InsertAtEnd(const Sangfroid::Maths::Point3f& point)
{
	m_points.push_back(point);
}

Sangfroid::Maths::Point3f DiscreteCurve::Min()
{
	CalculateBounds();
   return m_min;
}

Sangfroid::Maths::Point3f DiscreteCurve::Max()
{
   CalculateBounds();
   return m_max;
}

/*-----------------------------------------------------------------------------------*/
