﻿#pragma once

#include <iostream>
#include "PointBuffer.h"

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::Maths;


/*-----------------------------------------------------------------------------------*/

PointBuffer::PointBuffer(float* coords, int pointCount)
   :m_data(coords), 
   m_numPoints(pointCount),
   m_calculatedBounds(false)
{

}

PointBuffer::~PointBuffer() 
{
}

int PointBuffer::PointCount() const
{
   return m_numPoints;
}

float* PointBuffer::Data() const
{
   return m_data;
}

Sangfroid::Maths::Point3f PointBuffer::PointAt(const int& index) const
{
   if (index >= m_numPoints)
   {
      throw "PointBuffer: Index out of range...";
   }
   auto idx0 = (3 * index) + 0;
   auto idx1 = (3 * index) + 1;
   auto idx2 = (3 * index) + 2;

   return Sangfroid::Maths::Point3f(m_data[idx0], m_data[idx1], m_data[idx2]);
}

Sangfroid::Maths::Point3f PointBuffer::Min()
{
   calculate();
   return m_min;
}

Sangfroid::Maths::Point3f PointBuffer::Max()
{
   calculate();
   return m_max;
}

void PointBuffer::calculate()
{
   if (!m_calculatedBounds)
   {
      Sangfroid::Maths::Point3f min(FLT_MAX, FLT_MAX, FLT_MAX);
      Sangfroid::Maths::Point3f max(-FLT_MAX, -FLT_MAX, -FLT_MAX);

      for (int i = 0; i < m_numPoints; i++)
      {
         auto x = m_data[(3 * i) + 0];
         auto y = m_data[(3 * i) + 1];
         auto z = m_data[(3 * i) + 2];

         if (x < min.x)	min.x = x;
         if (y < min.y)	min.y = y;
         if (z < min.z)	min.z = z;

         if (x >= max.x)	max.x = x;
         if (y >= max.y)	max.y = y;
         if (z >= max.z)	max.z = z;
      }

      m_min = min;
      m_max = max;

      m_calculatedBounds = true;
   }
}

PointBuffer* PointBuffer::DeepCopy() const
{
   float* newData = new float[this->m_numPoints * 3];
   memcpy(&newData[0], &(this->m_data[0]), m_numPoints * 3 * sizeof(float));
   PointBuffer* newBuffer = new PointBuffer(newData, this->m_numPoints);
   return newBuffer;
}

UltraVector<Point3f> PointBuffer::PointsFromBuffer(PointBuffer* buffer)
{
	UltraVector<Point3f> points;
	auto data = buffer->Data();

	for (int i = 0; i < buffer->PointCount(); i++)
	{
		int ix = (3 * i) + 0;
		int iy = (3 * i) + 1;
		int iz = (3 * i) + 2;

		points.push_back(Point3f(data[ix], data[iy], data[iz]));
	}
	return points;
}
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
