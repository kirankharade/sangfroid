﻿#include "GeoElementsUtils.h"
#include "PointTriangle.h"
#include "IndexBuffer.h"
#include "NormalBuffer.h"
#include "PointBuffer.h"

#include <map>

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

//---------------------------------------------------------------------------------------
#pragma warning(disable:4244 4996)
//---------------------------------------------------------------------------------------

void GeoElementsUtils::WriteTriangleListToSTL(
	const UltraVector<PointTriangle>& triangleList, 
	const std::string& filename)
{
   try
   {
      FILE* pFile = std::fopen(filename.c_str(), "wb");
      if (!pFile)
      {
         throw "The STL file cannot be written.";
      }

      int triangleCount = triangleList.size();
      int vertexCount = 3 * triangleCount;
      int numIndices = 3 * triangleCount;

      //write 80 byte header
      fprintf(pFile, "%-80.80s", "STL");

      //write the facet count
      fwrite(&triangleCount, sizeof(int), 1, pFile);

      short spacer = 0;
      float va[3], vb[3], vc[3];
      float n[3];

      //write 50 byte blocks per triangle
      for (int i = 0; i < triangleCount; i++)
      {
         const PointTriangle& t = triangleList[i];

         va[0] = (double)(t.Vertices[0].x);
         va[1] = (double)(t.Vertices[0].y);
         va[2] = (double)(t.Vertices[0].z);

         vb[0] = (double)(t.Vertices[1].x);
         vb[1] = (double)(t.Vertices[1].y);
         vb[2] = (double)(t.Vertices[1].z);

         vc[0] = (double)(t.Vertices[2].x);
         vc[1] = (double)(t.Vertices[2].y);
         vc[2] = (double)(t.Vertices[2].z);

         Sangfroid::Maths::Vector3f v = (t.Vertices[1] - t.Vertices[0]).cross((t.Vertices[2] - t.Vertices[1]));
         Sangfroid::Maths::Vector3f nm = v.unitVec();
         n[0] = nm.i;
         n[1] = nm.j;
         n[2] = nm.k;

         //write normal (12 bytes)
         fwrite(&n[0], sizeof(float), 3, pFile);

         //write vertices (36 bytes)
         fwrite(&va[0], sizeof(float), 3, pFile);
         fwrite(&vb[0], sizeof(float), 3, pFile);
         fwrite(&vc[0], sizeof(float), 3, pFile);

         //write 2 byte spacer
         fwrite(&spacer, sizeof(short), 1, pFile);
      }

      std::fflush(pFile);
      std::fclose(pFile);
   }
   catch (...)
   {
      throw "Failed to write STL file.";
   }
}
/*-----------------------------------------------------------------------------------*/

UltraVector<PointTriangle> GeoElementsUtils::GetTriangles(const std::vector<TriangleMesh*> meshes)
{
	UltraVector<PointTriangle> triangles;

	try
	{
		for (size_t i = 0; i < meshes.size(); i++)
		{
			auto mesh = meshes[i];
			auto points = mesh->Points();
			auto indices = mesh->Indices();
			auto normals = mesh->Normals();

			if (indices == nullptr || normals == nullptr)
			{
				continue;
			}

			int numTriangles = mesh->TriangleCount();
			for (int it = 0; it < (int)numTriangles; it++)
			{
				auto iv0 = indices->i(it);
				auto iv1 = indices->j(it);
				auto iv2 = indices->k(it);

				auto v0 = points->PointAt(iv0);
				auto v1 = points->PointAt(iv1);
				auto v2 = points->PointAt(iv2);

				PointTriangle t(v0, v1, v2);
				triangles.push_back(t);
			}
		}
	}
	catch (...)
	{
		throw "Cannot generate triangle list.";
	}

	return triangles;
}
/*-----------------------------------------------------------------------------------*/
