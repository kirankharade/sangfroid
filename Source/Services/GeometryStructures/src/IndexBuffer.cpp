﻿#pragma once

#include <iostream>
#include "IndexBuffer.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;

/*-----------------------------------------------------------------------------------*/

IndexBuffer::IndexBuffer(int* indices, int triCount)
   :m_data(indices), m_numTriangles(triCount)
{

}
//---------------------------------------------------------------------------------------

IndexBuffer::~IndexBuffer() 
{
}
//---------------------------------------------------------------------------------------

int*& IndexBuffer::Data()
{
   return m_data;
}
//---------------------------------------------------------------------------------------

int* IndexBuffer::Data() const
{
   return m_data;
}
//---------------------------------------------------------------------------------------

int IndexBuffer::TriangleCount() const
{
   return m_numTriangles;
}
//---------------------------------------------------------------------------------------

int IndexBuffer::i(const int& triIndex) const
{
   if (triIndex >= m_numTriangles)
   {
      throw "IndexBuffer: Index out of range...";
   }
   return m_data[(3 * triIndex) + 0];
}
//---------------------------------------------------------------------------------------

int IndexBuffer::j(const int& triIndex) const
{
   if (triIndex >= m_numTriangles)
   {
      throw "IndexBuffer: Index out of range...";
   }
   return m_data[(3 * triIndex) + 1];
}
//---------------------------------------------------------------------------------------

int IndexBuffer::k(const int& triIndex) const
{
   if (triIndex >= m_numTriangles)
   {
      throw "IndexBuffer: Index out of range...";
   }
   return m_data[(3 * triIndex) + 2];
}

/*-----------------------------------------------------------------------------------*/
