﻿#pragma once

#include <iostream>
#include "NormalBuffer.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;
using namespace Sangfroid::Maths;

/*-----------------------------------------------------------------------------------*/

NormalBuffer::NormalBuffer(float* coords, int normalCount)
    :m_data(coords), m_numNormals(normalCount)
{
}

NormalBuffer::~NormalBuffer() {}

int NormalBuffer::Count() const
{
    return m_numNormals;
}

float*& NormalBuffer::Data()
{
    return m_data;
}

const float* NormalBuffer::Data() const
{
    return m_data;
}

Sangfroid::Maths::Vector3f NormalBuffer::NormalAt(const int& index) const
{
    if (index >= m_numNormals)
    {
        throw "NormalBuffer: Index out of range...";
    }
    auto idx0 = (3 * index) + 0;
    auto idx1 = (3 * index) + 1;
    auto idx2 = (3 * index) + 2;

    return Sangfroid::Maths::Vector3f(m_data[idx0], m_data[idx1], m_data[idx2]);
}
/*-----------------------------------------------------------------------------------*/
