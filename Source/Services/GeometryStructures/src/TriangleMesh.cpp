﻿#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "TriangleMesh.h"
#include "TriangleMeshImpl.h"
#include "PointBuffer.h"
#include "MathsDefines.h"
#include <vector>

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

/*-----------------------------------------------------------------------------------*/

TriangleMesh::TriangleMesh(
   PointBuffer* posBuffer, 
   NormalBuffer* normalBuffer, 
   IndexBuffer* indices)
:m_pImpl(nullptr)
{
   m_pImpl = new TriangleMeshImpl(posBuffer, normalBuffer, indices);
}

TriangleMesh::~TriangleMesh()
{
   delete m_pImpl;
}

PointBuffer* TriangleMesh::Points()
{
   return ((TriangleMeshImpl*)m_pImpl)->Points();
}

NormalBuffer* TriangleMesh::Normals()
{
   return ((TriangleMeshImpl*)m_pImpl)->Normals();
}

IndexBuffer* TriangleMesh::Indices()
{
   return ((TriangleMeshImpl*)m_pImpl)->Indices();
}

int TriangleMesh::Id() const
{
   return ((TriangleMeshImpl*)m_pImpl)->Id();
}

int TriangleMesh::TriangleCount() const
{
   return ((TriangleMeshImpl*)m_pImpl)->TriangleCount();
}

TriangleMesh* TriangleMesh::RemoveBigTriangles(float thresholdArea, bool unifyVertices)
{
   return ((TriangleMeshImpl*)m_pImpl)->RemoveBigTriangles(thresholdArea, unifyVertices);
}

bool TriangleMesh::WriteToSTL(const char* stlFilePath)
{
   return ((TriangleMeshImpl*)m_pImpl)->WriteToSTL(stlFilePath);
}

UltraVector<Sangfroid::Maths::PointTriangle> TriangleMesh::ToTriangleList() const
{
	return ((TriangleMeshImpl*)m_pImpl)->ToTriangleList();
}

TriangleMesh** TriangleMesh::DivideIntoFaceTriangulations(float angleDeg, int& meshCount)
{
   auto meshList = ((TriangleMeshImpl*)m_pImpl)->DivideIntoFaceTriangulations(angleDeg);
   meshCount = (int) meshList.size();

   auto meshes = new TriangleMesh*[meshCount];
   for (int i = 0; i < meshCount; i++)
   {
      meshes[i] = meshList[i];
   }
   return meshes;
}

Sangfroid::Maths::Point3f* TriangleMesh::FaceEdgePoints(int& numPoints) const
{
   auto pts = ((TriangleMeshImpl*)m_pImpl)->FaceEdgePoints();
   numPoints = (int) pts.size();

   Point3f* points = new Point3f[numPoints];
   for (int i = 0; i < numPoints; i++)
   {
      points[i] = pts[i];
   }
   return points;
}

TriangleMesh* TriangleMesh::BoundaryTriangles()
{
   return ((TriangleMeshImpl*)m_pImpl)->BoundaryTriangles();

}
/*-----------------------------------------------------------------------------------*/




