#include <cmath>
#include "BaseOctreeBox.h"
#include "BaseOctree.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;


BaseOctreeBox::BaseOctreeBox()
{
    InitialiseMembers();
}

BaseOctreeBox::BaseOctreeBox(BaseOctree* pTree,
                            BaseOctreeBox* parent,
                            const int depthLevel,
                            const Point3f& min,
                            const Point3f& max,
                            const bool bLazyExpansion)
    :m_pTree(pTree),
    m_pParent(parent),
    m_depthLevel(depthLevel),
    m_min(min),
    m_max(max),
    m_bLazyExpansion(bLazyExpansion)
{
    m_ppChildren = nullptr;

    if (!m_bLazyExpansion)
    {
        if (pTree)
        {
            if (m_depthLevel < pTree->GetTreeDepth())
            {
                CreateChildren(pTree);
            }
        }
    }
}

void BaseOctreeBox::InitialiseMembers()
{
    m_ppChildren = nullptr;
    m_pParent = nullptr;
    m_min = Sangfroid::Maths::Point3f();
    m_max = Sangfroid::Maths::Point3f();
    m_bLazyExpansion = false;
}

void BaseOctreeBox::CleanUp()
{
    m_pParent = NULL;
    if (NULL != m_ppChildren)
    {
        for (int i = 0; i < 8; i++)
        {
            SAFE_DELETE(m_ppChildren[i]);
        }
        SAFE_ARRAY_DELETE(m_ppChildren);
    }
    m_min = Sangfroid::Maths::Point3f();
    m_max = Sangfroid::Maths::Point3f();
}

BaseOctreeBox::~BaseOctreeBox()
{
    CleanUp();
}

BaseOctree* BaseOctreeBox::Tree() const
{
    return m_pTree;
}

BaseOctreeBox** BaseOctreeBox::GetChildren() const
{
    return m_ppChildren;
}

BaseOctreeBox* BaseOctreeBox::GetParent() const
{
    return m_pParent;
}

bool BaseOctreeBox::IsRootBox() const
{
    if (m_pParent == NULL)
    {
        return true;
    }
    return false;
}

bool BaseOctreeBox::IsLeafBox() const
{
    if (m_ppChildren)
    {
        return false;
    }
    return true;
}

bool BaseOctreeBox::IsInside(const Point3f& pt) const
{

    // Because the point passed in here is upcast from the 
    // interleaved data the equality comparison
    // must be compared with a tolerance.

    //if((pt.x < m_max.x || abs(pt.x - m_max.x) < STD_TOLERANCE) && 
    //   (pt.x > m_min.x || abs(pt.x - m_min.x) < STD_TOLERANCE) && 
    //   (pt.y < m_max.y || abs(pt.y - m_max.y) < STD_TOLERANCE) &&
    //   (pt.y > m_min.y || abs(pt.y - m_min.y) < STD_TOLERANCE) && 
    //   (pt.z < m_max.z || abs(pt.z - m_max.z) < STD_TOLERANCE) &&
    //   (pt.z > m_min.z || abs(pt.z - m_min.z) < STD_TOLERANCE))

    if (pt.x <= m_max.x && pt.x >= m_min.x &&
        pt.y <= m_max.y && pt.y >= m_min.y &&
        pt.z <= m_max.z && pt.z >= m_min.z)
    {
        return true;
    }
    return false;
}

const BaseOctreeBox* BaseOctreeBox::BoxForPoint(const Point3f& pt) const
{
    if (m_ppChildren)
    {
        for (int i = 0; i < 8; i++)
        {
            if (m_ppChildren[i]->IsInside(pt))
            {
                return m_ppChildren[i]->BoxForPoint(pt);
            }
        }
    }
    else
    {
        if (IsInside(pt))
            return this;
    }
    return NULL;
}

void BaseOctreeBox::CreateChildren(BaseOctree* pTree)
{
    if (!pTree)
    {
        return;
    }
    m_ppChildren = new BaseOctreeBox*[8];

    Point3f mid = Point3f((float)0.5 * (m_min.x + m_max.x),
        (float)0.5 * (m_min.y + m_max.y),
        (float)0.5 * (m_min.z + m_max.z));
    Point3f min(m_min);
    Point3f max(m_max);

    Point3f maxCorner[8], minCorner[8];

    int childDepthLevel = m_depthLevel + 1;

    minCorner[0] = Point3f(min.x, min.y, min.z);
    minCorner[1] = Point3f(min.x, mid.y, min.z);
    minCorner[2] = Point3f(min.x, mid.y, mid.z);
    minCorner[3] = Point3f(min.x, min.y, mid.z);
    minCorner[4] = Point3f(mid.x, min.y, min.z);
    minCorner[5] = Point3f(mid.x, mid.y, min.z);
    minCorner[6] = Point3f(mid.x, mid.y, mid.z);
    minCorner[7] = Point3f(mid.x, min.y, mid.z);

    maxCorner[0] = Point3f(mid.x, mid.y, mid.z);
    maxCorner[1] = Point3f(mid.x, max.y, mid.z);
    maxCorner[2] = Point3f(mid.x, max.y, max.z);
    maxCorner[3] = Point3f(mid.x, mid.y, max.z);
    maxCorner[4] = Point3f(max.x, mid.y, mid.z);
    maxCorner[5] = Point3f(max.x, max.y, mid.z);
    maxCorner[6] = Point3f(max.x, max.y, max.z);
    maxCorner[7] = Point3f(max.x, mid.y, max.z);

    m_ppChildren[0] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[0], maxCorner[0], m_bLazyExpansion);
    m_ppChildren[1] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[1], maxCorner[1], m_bLazyExpansion);
    m_ppChildren[2] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[2], maxCorner[2], m_bLazyExpansion);
    m_ppChildren[3] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[3], maxCorner[3], m_bLazyExpansion);
    m_ppChildren[4] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[4], maxCorner[4], m_bLazyExpansion);
    m_ppChildren[5] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[5], maxCorner[5], m_bLazyExpansion);
    m_ppChildren[6] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[6], maxCorner[6], m_bLazyExpansion);
    m_ppChildren[7] = new BaseOctreeBox(pTree, this, childDepthLevel, minCorner[7], maxCorner[7], m_bLazyExpansion);
}
