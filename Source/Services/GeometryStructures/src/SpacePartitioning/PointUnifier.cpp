#include <cmath>
#include "PointUnifier.h"
#include "PointUnifierOctree.h"
#include "Point3f.h"
#include <vector>

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

PointUnifier::PointUnifier(PointBuffer* buffer)
   :m_pOctree(nullptr)
{
   m_pOctree = new PointUnifierOctree(buffer, 4, false);
}

PointUnifier::PointUnifier(
   const Point3f& min,
   const Point3f& max)
   : m_pOctree(nullptr)
{
   m_pOctree = new PointUnifierOctree(min, max, 4, false);
}

PointUnifier::~PointUnifier()
{
   delete m_pOctree;
}

int PointUnifier::Add(const Sangfroid::Maths::Point3f& pt)
{
   return m_pOctree->Add(pt);
}

const UltraVector<Sangfroid::Maths::Point3f>& PointUnifier::Vertices() const
{
   return m_pOctree->Vertices();
}

PointBuffer* PointUnifier::VerticesAsBuffer() const
{
   return m_pOctree->UnifiedPointBuffer();
}


