#include <cmath>
#include "PointOctree.h"
#include "PointOctreeBox.h"
#include "PointBuffer.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

PointOctree::PointOctree()
{
    m_min = Point3f();
    m_max = Point3f();
    m_treeDepth = 0;
    m_bLazyExpansion = false;
    m_pRoot = nullptr;
}

PointOctree::PointOctree(const Point3f& min,
                        const Point3f& max, 
                        const int treeDepth, 
                        const bool bLazyExpansion)
                        :BaseOctree(min, max, treeDepth, bLazyExpansion)
{
   if (m_pRoot != nullptr)
   {
      delete m_pRoot;
   }
   m_pRoot = new PointOctreeBox(this, nullptr, 0, m_min, m_max, m_bLazyExpansion);
}

PointOctree::PointOctree(PointBuffer* posBuffer, const int treeDepth, const bool bLazyExpansion)
   :BaseOctree()
{
   m_min = posBuffer->Min();
   m_max = posBuffer->Max();
   m_treeDepth = treeDepth;
   m_bLazyExpansion = bLazyExpansion;
   m_pRoot = new PointOctreeBox(this, NULL, 0, m_min, m_max, m_bLazyExpansion);

   addPointBuffer(posBuffer);
}

PointOctree::~PointOctree(void)
{
}

void PointOctree::addPointBuffer(PointBuffer* buffer)
{
   auto numPoints = buffer->PointCount();
   m_index2BoxList.clear();
   m_index2BoxList.resize(numPoints);
   
   for (int i = 0; i < numPoints; i++)
   {
      auto pt = buffer->PointAt(i);
      Add(pt, i);
   }
}

PointOctreeBox* PointOctree::Add(const Point3f& pt, const int index)
{
   if(m_pRoot)
   {
      auto box = ((PointOctreeBox*) m_pRoot)->Add(this, pt, index);
      m_index2BoxList[index] = box;
      return box;
   }
   return NULL;
}

const PointOctreeBox* PointOctree::BoxForPointIndex(const int index) const
{
   if (index >= (int) m_index2BoxList.size() || index < 0)
   {
      throw "PointOctree: Index out of range.";
   }
   return m_index2BoxList[index];
}

