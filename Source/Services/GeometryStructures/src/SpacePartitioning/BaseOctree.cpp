#include <cmath>
#include "BaseOctreeBox.h"
#include "BaseOctree.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

BaseOctree::BaseOctree()
{
   InitialiseMembers();
}

BaseOctree::BaseOctree(const Point3f& min,
                        const Point3f& max, 
                        const int treeDepth, 
                        const bool bLazyExpansion)
    :m_min(min), m_max(max), m_treeDepth(treeDepth), m_bLazyExpansion(bLazyExpansion)
{
   m_pRoot = new BaseOctreeBox(this, nullptr, 0, m_min, m_max, m_bLazyExpansion);
}

BaseOctree::~BaseOctree()
{
   CleanUp();
   if (nullptr != m_pRoot)
   {
	   delete m_pRoot;
	   m_pRoot = nullptr;
   }
}

void BaseOctree::InitialiseMembers()
{
   m_pRoot = nullptr;
   m_treeDepth = 0;
   m_min = Sangfroid::Maths::Point3f();
   m_max = Sangfroid::Maths::Point3f();
   m_bLazyExpansion = false;
}

void BaseOctree::CleanUp()
{
   SAFE_DELETE(m_pRoot);

   m_treeDepth = 0;
   m_min = Sangfroid::Maths::Point3f();
   m_max = Sangfroid::Maths::Point3f();
}

int BaseOctree::GetTreeDepth() const
{
   return m_treeDepth;
}

Point3f BaseOctree::GetMin() const
{
   return m_min;
}

Point3f BaseOctree::Getmax() const
{
   return m_max;
}

int BaseOctree::CalculateTreeDepth(const int vertexCount)
{
   int treeDepth = 1;
   int temp = (int)vertexCount;
   while (temp > 1)
   {
      treeDepth++;
      temp = (int)(temp / 10.0f);
   }
   if (treeDepth < 3)
   {
      treeDepth = 3;
   }
   return treeDepth;
}

const BaseOctreeBox* BaseOctree::BoxForPoint(const Point3f& pt) const
{
   if (m_pRoot)
   {
      return m_pRoot->BoxForPoint(pt);
   }
   return nullptr;
}

