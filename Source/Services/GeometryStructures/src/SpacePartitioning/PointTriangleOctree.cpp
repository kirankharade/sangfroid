#include <cmath>
#include "PointTriangleOctree.h"
#include "PointTriangleOctreeBox.h"
#include "PointBuffer.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

PointTriangleOctree::PointTriangleOctree(const Point3f& min,
    const Point3f& max,
    const int treeDepth,
    const bool bLazyExpansion)
    :PointOctree(min, max, treeDepth, bLazyExpansion)
{
    if (m_pRoot != nullptr)
    {
        delete m_pRoot;
    }
    m_pRoot = new PointTriangleOctreeBox(this, NULL, 0, m_min, m_max, m_bLazyExpansion);
}

PointTriangleOctree::PointTriangleOctree(PointBuffer* posBuffer, const int treeDepth, const bool bLazyExpansion)
    :PointOctree()
{
    m_min = posBuffer->Min();
    m_max = posBuffer->Max();
    m_treeDepth = treeDepth;
    m_bLazyExpansion = bLazyExpansion;
    m_pRoot = new PointTriangleOctreeBox(this, NULL, 0, m_min, m_max, m_bLazyExpansion);

    addPointBuffer(posBuffer);
}

PointTriangleOctree::~PointTriangleOctree(void)
{
}

PointTriangleOctreeBox* PointTriangleOctree::AddTriangle(const Sangfroid::Maths::Point3f& centroid, const int index)
{
    if (m_pRoot)
    {
        auto box = ((PointTriangleOctreeBox*)m_pRoot)->AddTriangle(this, centroid, index);
        m_triangleIndex2BoxList[index] = box;
        return box;
    }
    return NULL;
}

const PointTriangleOctreeBox* PointTriangleOctree::BoxForTriangleIndex(const int index) const
{
    if (index >= (int) m_triangleIndex2BoxList.size() || index < 0)
    {
        throw "PointOctree: Index out of range.";
    }
    return m_triangleIndex2BoxList[index];
}

void PointTriangleOctree::SetTriangleCount(int triangleCount)
{
    m_triangleIndex2BoxList.clear();
    m_triangleIndex2BoxList.resize(triangleCount);
}

