#include <cmath>
#include "PointUnifierOctree.h"
#include "PointUnifierOctreeBox.h"
#include "PointBuffer.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

/* ---------- PointUnifierOctreeBox Methods -------------*/

PointUnifierOctreeBox::PointUnifierOctreeBox(PointUnifierOctree* pTree,
                     PointUnifierOctreeBox* parent,
                     const int depthLevel, 
                     const Point3f& min, 
                     const Point3f& max,
                     const bool bLazyExpansion)
                     :BaseOctreeBox(pTree, parent, depthLevel, min, max, bLazyExpansion)
{
   m_uniqueVertexIndexList.clear();

   if (m_bLazyExpansion == false)
   {
      if (pTree)
      {
         if (m_depthLevel < pTree->GetTreeDepth())
         {
            CreateChildren(pTree);
         }
      }
   }
}

void PointUnifierOctreeBox::InitialiseMembers()
{
   BaseOctreeBox::InitialiseMembers();
   m_uniqueVertexIndexList.clear();
}

void PointUnifierOctreeBox::CleanUp()
{
    BaseOctreeBox::CleanUp();

   m_uniqueVertexIndexList.clear();
}

PointUnifierOctreeBox::~PointUnifierOctreeBox()
{
   m_uniqueVertexIndexList.clear();
}

const std::vector<int>& PointUnifierOctreeBox::GetPoints() const
{
   return m_uniqueVertexIndexList;
}

static int findPoint(PointUnifierOctree* octree, const std::vector<int>& indices, const Sangfroid::Maths::Point3f& pt)
{
	auto proximityTolerance = 0.002;

   for (auto idx : indices)
   {
      auto vp = octree->PointAt(idx);

      if (vp.AreSame(pt, proximityTolerance))
      {
         return idx;
      }
   }
   return -1;
}

int PointUnifierOctreeBox::insertToLeafBox(const Sangfroid::Maths::Point3f& pt)
{
   //This is leaf node
   auto index = -1;
   auto ptCountInCell = m_uniqueVertexIndexList.size();
   if (ptCountInCell > 0)
   {
      index = findPoint(((PointUnifierOctree*)m_pTree), m_uniqueVertexIndexList, pt);
   }
   if (index == -1)
   {
      ((PointUnifierOctree*)m_pTree)->m_points.push_back(pt);
      index = ((PointUnifierOctree*)m_pTree)->m_points.size() - 1;
      m_uniqueVertexIndexList.push_back(index);
      return index;
   }
   return index;
}

int PointUnifierOctreeBox::Add(const Sangfroid::Maths::Point3f& pt)
{
   if(IsInside(pt))
   {
      if(m_ppChildren)
      {
         for(int i = 0; i < 8; i++)
         {
            if(m_ppChildren[i]->IsInside(pt))
            {
               return ((PointUnifierOctreeBox*)m_ppChildren[i])->Add(pt);
            }
         }
      }
      else
      {
         if(m_bLazyExpansion)
         {
            if(m_depthLevel < m_pTree->GetTreeDepth())
            {
               CreateChildren(m_pTree);

               for(int i = 0; i < 8; i++)
               {
                  if(m_ppChildren[i]->IsInside(pt))
                  {
                     return ((PointUnifierOctreeBox*)m_ppChildren[i])->Add(pt);
                  }
               }
            }
            else
            {
               //This is leaf node
               return insertToLeafBox(pt);
            }
         }
         else
         {
            //This is leaf node
            return insertToLeafBox(pt);
         }
      }
   }
   return -1;
}

void PointUnifierOctreeBox::CreateChildren(BaseOctree* pTree)
{
   if(!pTree)
   {
      return;
   }
   m_ppChildren = (BaseOctreeBox**) new PointUnifierOctreeBox*[8];
   
   Point3f mid = Point3f((float)0.5 * (m_min.x + m_max.x),
                           (float)0.5 * (m_min.y + m_max.y),
                           (float)0.5 * (m_min.z + m_max.z));
   Point3f min = m_min;
   Point3f max = m_max;

   Point3f maxCorner[8], minCorner[8];

   int childDepthLevel = m_depthLevel + 1;

   minCorner[0] = Point3f(min.x, min.y, min.z);
   minCorner[1] = Point3f(min.x, mid.y, min.z);
   minCorner[2] = Point3f(min.x, mid.y, mid.z);
   minCorner[3] = Point3f(min.x, min.y, mid.z);
   minCorner[4] = Point3f(mid.x, min.y, min.z);
   minCorner[5] = Point3f(mid.x, mid.y, min.z);
   minCorner[6] = Point3f(mid.x, mid.y, mid.z);
   minCorner[7] = Point3f(mid.x, min.y, mid.z);

   maxCorner[0] = Point3f(mid.x, mid.y, mid.z);
   maxCorner[1] = Point3f(mid.x, max.y, mid.z);
   maxCorner[2] = Point3f(mid.x, max.y, max.z);
   maxCorner[3] = Point3f(mid.x, mid.y, max.z);
   maxCorner[4] = Point3f(max.x, mid.y, mid.z);
   maxCorner[5] = Point3f(max.x, max.y, mid.z);
   maxCorner[6] = Point3f(max.x, max.y, max.z);
   maxCorner[7] = Point3f(max.x, mid.y, max.z);

   m_ppChildren[0] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[0], maxCorner[0], m_bLazyExpansion);
   m_ppChildren[1] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[1], maxCorner[1], m_bLazyExpansion);
   m_ppChildren[2] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[2], maxCorner[2], m_bLazyExpansion);
   m_ppChildren[3] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[3], maxCorner[3], m_bLazyExpansion);
   m_ppChildren[4] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[4], maxCorner[4], m_bLazyExpansion);
   m_ppChildren[5] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[5], maxCorner[5], m_bLazyExpansion);
   m_ppChildren[6] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[6], maxCorner[6], m_bLazyExpansion);
   m_ppChildren[7] = new PointUnifierOctreeBox((PointUnifierOctree*) pTree, this, childDepthLevel, minCorner[7], maxCorner[7], m_bLazyExpansion);
}

