#include <cmath>
#include "PointOctreeBox.h"
#include "PointOctree.h"
#include "PointBuffer.h"
#include <vector>

using namespace std;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

PointOctreeBox::PointOctreeBox(PointOctree* pTree,
    PointOctreeBox* parent,
    const int depthLevel,
    const Point3f& min,
    const Point3f& max,
    const bool bLazyExpansion)
    :BaseOctreeBox(pTree, parent, depthLevel, min, max, bLazyExpansion)
{
    m_pointIndexList.clear();
}

void PointOctreeBox::InitialiseMembers()
{
    BaseOctreeBox::InitialiseMembers();
    m_pointIndexList.clear();
}

void PointOctreeBox::CleanUp()
{
    BaseOctreeBox::CleanUp();

    m_pointIndexList.clear();
}

PointOctreeBox::~PointOctreeBox()
{
    m_pointIndexList.clear();
}

const std::vector<int>& PointOctreeBox::GetPoints() const
{
    return m_pointIndexList;
}

PointOctreeBox* PointOctreeBox::Add(PointOctree* pTree,
    const Point3f& pt,
    const int index)
{
    if (IsInside(pt))
    {
        if (m_ppChildren)
        {
            for (int i = 0; i < 8; i++)
            {
                if (m_ppChildren[i]->IsInside(pt))
                {
                    PointOctreeBox* pBox = ((PointOctreeBox*)m_ppChildren[i])->Add(pTree, pt, index);
                    if (pBox != NULL)
                    {
                        return pBox;
                    }
                }
            }
        }
        else
        {
            if (m_bLazyExpansion)
            {
                if (m_depthLevel < pTree->GetTreeDepth())
                {
                    CreateChildren(pTree);

                    for (int i = 0; i < 8; i++)
                    {
                        if (m_ppChildren[i]->IsInside(pt))
                        {
                            PointOctreeBox* pBox = ((PointOctreeBox*)m_ppChildren[i])->Add(pTree, pt, index);
                            if (pBox != NULL)
                            {
                                return pBox;
                            }
                        }
                    }
                }
                else
                {
                    //This is leaf node
                    m_pointIndexList.push_back(index);
                    return this;
                }
            }
            else
            {
                //This is leaf node...
                m_pointIndexList.push_back(index);
                return this;
            }
        }
    }
    return NULL;
}

void PointOctreeBox::CreateChildren(BaseOctree* pTree)
{
    if (!pTree)
    {
        return;
    }
    m_ppChildren = (BaseOctreeBox**) new PointOctreeBox*[8];

    Point3f mid = Point3f((float)0.5 * (m_min.x + m_max.x),
        (float)0.5 * (m_min.y + m_max.y),
        (float)0.5 * (m_min.z + m_max.z));
    Point3f min = m_min;
    Point3f max = m_max;

    Point3f maxCorner[8], minCorner[8];

    int childDepthLevel = m_depthLevel + 1;

    minCorner[0] = Point3f(min.x, min.y, min.z);
    minCorner[1] = Point3f(min.x, mid.y, min.z);
    minCorner[2] = Point3f(min.x, mid.y, mid.z);
    minCorner[3] = Point3f(min.x, min.y, mid.z);
    minCorner[4] = Point3f(mid.x, min.y, min.z);
    minCorner[5] = Point3f(mid.x, mid.y, min.z);
    minCorner[6] = Point3f(mid.x, mid.y, mid.z);
    minCorner[7] = Point3f(mid.x, min.y, mid.z);

    maxCorner[0] = Point3f(mid.x, mid.y, mid.z);
    maxCorner[1] = Point3f(mid.x, max.y, mid.z);
    maxCorner[2] = Point3f(mid.x, max.y, max.z);
    maxCorner[3] = Point3f(mid.x, mid.y, max.z);
    maxCorner[4] = Point3f(max.x, mid.y, mid.z);
    maxCorner[5] = Point3f(max.x, max.y, mid.z);
    maxCorner[6] = Point3f(max.x, max.y, max.z);
    maxCorner[7] = Point3f(max.x, mid.y, max.z);

    m_ppChildren[0] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[0], maxCorner[0], m_bLazyExpansion);
    m_ppChildren[1] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[1], maxCorner[1], m_bLazyExpansion);
    m_ppChildren[2] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[2], maxCorner[2], m_bLazyExpansion);
    m_ppChildren[3] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[3], maxCorner[3], m_bLazyExpansion);
    m_ppChildren[4] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[4], maxCorner[4], m_bLazyExpansion);
    m_ppChildren[5] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[5], maxCorner[5], m_bLazyExpansion);
    m_ppChildren[6] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[6], maxCorner[6], m_bLazyExpansion);
    m_ppChildren[7] = new PointOctreeBox((PointOctree*)pTree, this, childDepthLevel, minCorner[7], maxCorner[7], m_bLazyExpansion);
}
