#include <cmath>
#include "PointUnifierOctree.h"
#include "PointUnifierOctreeBox.h"
#include "PointBuffer.h"
#include <vector>

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::GeometryStructures;

PointUnifierOctree::PointUnifierOctree(const Point3f& min,
                        const Point3f& max, 
                        const int treeDepth, 
                        const bool bLazyExpansion)
                        :BaseOctree()
{
   m_min = min;
   m_max = max;
   m_treeDepth = treeDepth;
   m_bLazyExpansion = bLazyExpansion;

   if (m_pRoot != nullptr)
   {
      delete m_pRoot;
   }
   m_pRoot = new PointUnifierOctreeBox(this, nullptr, 0, m_min, m_max, bLazyExpansion);
}

PointUnifierOctree::PointUnifierOctree(PointBuffer* posBuffer, const int treeDepth, const bool bLazyExpansion)
   :BaseOctree()
{
   m_min = posBuffer->Min();
   m_max = posBuffer->Max();
   m_treeDepth = treeDepth;
   m_bLazyExpansion = bLazyExpansion;
   m_pRoot = new PointUnifierOctreeBox(this, nullptr, 0, m_min, m_max, m_bLazyExpansion);

   addPointBuffer(posBuffer);
}

PointUnifierOctree::~PointUnifierOctree(void)
{
   m_points.clear();
}

std::vector<int> PointUnifierOctree::addPointBuffer(PointBuffer* buffer)
{
   auto numPoints = (int) buffer->PointCount();
   std::vector<int> newIndices;
   newIndices.resize(numPoints);

   for (int i = 0; i < numPoints; i++)
   {
      auto pt = buffer->PointAt(i);
      newIndices[i] = Add(pt);
   }
   return newIndices;
}

int PointUnifierOctree::Add(const Point3f& pt)
{
   if(m_pRoot)
   {
      return ((PointUnifierOctreeBox*)m_pRoot)->Add(pt);
   }
   return -1;
}

PointBuffer* PointUnifierOctree::UnifiedPointBuffer() const
{
   auto pointCount = (int) m_points.size();
   auto posData = new float[pointCount * 3];
   for (int ip = 0; ip < pointCount; ip++)
   {
      auto& pt = m_points[ip];
      posData[(3 * ip) + 0] = pt.x;
      posData[(3 * ip) + 1] = pt.y;
      posData[(3 * ip) + 2] = pt.z;
   }
   auto pb = new PointBuffer(posData, pointCount);
   return pb;
}

const UltraVector<Sangfroid::Maths::Point3f>& PointUnifierOctree::Vertices()
{
   return m_points;
}

const Sangfroid::Maths::Point3f& PointUnifierOctree::PointAt(const int index) const
{
   if (index < 0 || index >= (int) m_points.size())
   {
      throw "PointUnifierOctree: Index out of range.";
   }
   return m_points[index];
}
