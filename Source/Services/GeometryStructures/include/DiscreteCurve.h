/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "Point3f.h"
#include <vector>

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

struct GEOMETRY_STRUCTURES_IMP_EXP DiscreteCurve
{

public:

	DiscreteCurve() {}

	DiscreteCurve(const std::vector<Sangfroid::Maths::Point3f>& points);

	~DiscreteCurve();

	const std::vector<Sangfroid::Maths::Point3f>& Points() const;

	Sangfroid::Maths::Point3f PointAt(const int& index) const;

	Sangfroid::Maths::Point3f LastPoint() const;

	Sangfroid::Maths::Point3f FirstPoint() const;

	void InsertAtStart(const Sangfroid::Maths::Point3f& point);

	void InsertAtEnd(const Sangfroid::Maths::Point3f& point);

	Sangfroid::Maths::Point3f Min();

	Sangfroid::Maths::Point3f Max();

protected:

	void CalculateBounds();

protected:

	Sangfroid::Maths::Point3f					m_min;
	Sangfroid::Maths::Point3f					m_max;
	std::vector<Sangfroid::Maths::Point3f>		m_points;
	bool										m_calculatedBounds;

};
/*-----------------------------------------------------------------------------------*/

typedef std::vector<DiscreteCurve*> DiscreteCurveList;

/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

