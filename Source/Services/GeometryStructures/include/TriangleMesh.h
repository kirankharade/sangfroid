﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "PointBuffer.h"
#include "NormalBuffer.h"
#include "IndexBuffer.h"
#include "PointTriangle.h"
#include "SpacePArtitioning\PointOctree.h"

using namespace std;

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP TriangleMesh
{
    public:

       TriangleMesh(PointBuffer* posBuffer, NormalBuffer* normalBuffer, IndexBuffer* indices);

       ~TriangleMesh();

       PointBuffer* Points();
       NormalBuffer* Normals();
       IndexBuffer* Indices();

       int Id() const;

       int TriangleCount() const;

       TriangleMesh* RemoveBigTriangles(float thresholdArea, bool unifyVertices);

       TriangleMesh** DivideIntoFaceTriangulations(float angleDeg, int& meshCount);

       Sangfroid::Maths::Point3f* FaceEdgePoints(int& numEdges) const;

       bool WriteToSTL(const char* stlFilePath);

       TriangleMesh* BoundaryTriangles();

	   UltraVector<Sangfroid::Maths::PointTriangle> ToTriangleList() const;

private:

   void* m_pImpl;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

