﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once
#pragma warning( disable: 4251 )

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "Point3f.h"
#include "Vector3f.h"
#include "Containers/UltraVector.h"

using namespace std;

/*-----------------------------------------------------------------------------------*/
GEOMETRY_STRUCTURES_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointBuffer
{
    public:

       PointBuffer(float* coords, int pointCount);

       ~PointBuffer();

       int PointCount() const;

       float* Data() const;

       Sangfroid::Maths::Point3f PointAt(const int& index) const;

       Sangfroid::Maths::Point3f Min();

       Sangfroid::Maths::Point3f Max();

       PointBuffer* DeepCopy() const;

	   static UltraVector<Sangfroid::Maths::Point3f> PointsFromBuffer(PointBuffer* buffer);

protected:

   void calculate();

protected:

   float*								m_data;
   int									m_numPoints;
   Sangfroid::Maths::Point3f			m_min;
   Sangfroid::Maths::Point3f			m_max;
   bool									m_calculatedBounds;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

