﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"

using namespace std;

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP IndexBuffer
{
public:

   IndexBuffer(int* indices, int triCount);

   ~IndexBuffer();

   int TriangleCount() const;

   int*& Data();

   int* Data() const;

   int i(const int& triIndex) const;

   int j(const int& triIndex) const;

   int k(const int& triIndex) const;

public:

   int*		m_data;
   int		m_numTriangles;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

