﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "PointTriangle.h"
#include "TriangleMesh.h"
#include "Containers/UltraVector.h"

using namespace std;

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP GeoElementsUtils
{
    public:

		static void WriteTriangleListToSTL(
			const UltraVector<Sangfroid::Maths::PointTriangle>& triangleList,
			const std::string& filename);

		static UltraVector<Sangfroid::Maths::PointTriangle> GetTriangles(const std::vector<Sangfroid::GeometryStructures::TriangleMesh*> meshes);

};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END
