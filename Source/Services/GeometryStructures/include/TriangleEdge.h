/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include <map>
#include <vector>

using namespace std;

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

struct TriangleEdge
{
   //! Vertex Indices
   int va;
   int vb;

   //! Sharing Triangle indices
   int ta;
   int tb;

   //!Whether this is face-boundary edge
   bool faceBoundaryEdge;

   //! Default constructor
   TriangleEdge()
   {
      va = -1;
      vb = -1;
      ta = -1;
      tb = -1;
      faceBoundaryEdge = false;
   }

   //! Constructor
   TriangleEdge(const int& v1, const int& v2)
   {
      va = (v1 < v2)? v1 : v2;
      vb = (v1 > v2)? v1 : v2;
      ta = -1;
      tb = -1;
      faceBoundaryEdge = false;
   }

   //! Less Operator for key comparison
   bool operator < (const TriangleEdge& other) const
   {
      if(va < other.va) return true;
      if(va > other.va) return false;
      if(vb < other.vb) return true;
      if(vb < other.vb) return false;
      return false;
   }

   typedef std::map<TriangleEdge, int, std::less<TriangleEdge>> EdgeMap;

   typedef std::map<int, TriangleEdge> ReverseEdgeMap;

   static int AddToHalfEdgeMap(EdgeMap& edgeMap, ReverseEdgeMap& revEdgeMap, TriangleEdge edge, const int triangleIndex, int& currEdgeIndex)
   {
      EdgeMap::iterator itr = edgeMap.find(edge);
      EdgeMap::iterator tmpItr = edgeMap.end();
      if (itr != tmpItr)
      {
         //If an edge is found...
         int edgeIndex = itr->second;
         ReverseEdgeMap::iterator revItr = revEdgeMap.find(edgeIndex);
         (revItr->second).tb = triangleIndex;
         return edgeIndex;
      }
      else
      {
         //We need to add the edge to maps
         edge.ta = triangleIndex;
         edgeMap.insert(std::pair <TriangleEdge, int>(edge, currEdgeIndex));
         revEdgeMap.insert(std::pair <int, TriangleEdge>(currEdgeIndex, edge));
         currEdgeIndex++;
         return (currEdgeIndex - 1);
      }
   }
};

/*-----------------------------------------------------------------------------------*/

typedef vector<TriangleEdge> TriangleEdgeList;

/*-----------------------------------------------------------------------------------*/


GEOMETRY_STRUCTURES_NAMESPACE_END

