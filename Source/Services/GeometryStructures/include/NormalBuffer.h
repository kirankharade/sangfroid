﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "Vector3f.h"

using namespace std;

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP NormalBuffer
{
public:

    NormalBuffer(float* coords, int normalCount);

    ~NormalBuffer();

    int Count() const;

    float*& Data();

    const float* Data() const;

    Sangfroid::Maths::Vector3f NormalAt(const int& index) const;

public:

    float*		m_data;
    int			m_numNormals;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

