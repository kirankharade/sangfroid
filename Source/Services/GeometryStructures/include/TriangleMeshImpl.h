﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "PointBuffer.h"
#include "NormalBuffer.h"
#include "IndexBuffer.h"
#include "PointTriangleOctree.h"
#include "TriangleMesh.h"
#include "TriangleEdge.h"
#include "IndexedTriangle.h"
#include "PointTriangle.h"

using namespace std;

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/
struct MeshBoundaryPatch;
/*-----------------------------------------------------------------------------------*/

class TriangleMeshImpl
{

public:

    TriangleMeshImpl(PointBuffer* posBuffer, NormalBuffer* normalBuffer, IndexBuffer* indices);

    ~TriangleMeshImpl();

    PointBuffer* Points();
    NormalBuffer* Normals();
    IndexBuffer* Indices();

    int Id() const;

    int TriangleCount() const;

    std::vector<TriangleMesh*> DivideIntoFaceTriangulations(const float angleDeg);

    IndexedTriangle* TriangleAt(const int index);

    //Returns new triangulation object with triangles removed 
    TriangleMesh* RemoveBigTriangles(float thresholdArea, bool unifyVertices);

    std::vector<Sangfroid::Maths::Point3f> FaceEdgePoints() const;

    bool WriteToSTL(const char* stlFilePath);

    TriangleMesh* BoundaryTriangles();

	UltraVector<Sangfroid::Maths::PointTriangle> ToTriangleList() const;

private:

    void init();

    void generateIndices();

    void generateNormals();

    void fillUpOctree();

    TriangleEdgeList extractEdges(const float angleDeg);

    TriangleEdgeList extractOuterBoundary();

    int* getUniqueVertexIndices();

    void orderEdgePoints();

    std::vector<IndexedTriangleList> separateIntoFaces(const TriangleEdgeList& faceSeparatingEdges);

    void expandTriangleConnectivity(IndexedTriangle* seedTriangle, IndexedTriangleList& currTriangleList);

    void expandEdgeConnections(TriangleEdge& t, IndexedTriangleList& currTriangleList);

    TriangleMesh* convertTriangleListToMesh(const IndexedTriangleList& triangleList);

    void saveEdges(const std::vector<Sangfroid::Maths::Point3f>& edgePoints);

    PointOctree* populateOctreeWithTriangles();

    IndexedTriangle* getNearestTriangle(const Sangfroid::Maths::Point3f& pt,
        const std::vector<int>& nearbyTriangleIndices,
        Sangfroid::Maths::Vector3f& nearestTriangleNormal);

private:

    int                      m_Id;
    PointBuffer*             m_posBuffer;
    NormalBuffer*	         m_normalBuffer;
    IndexBuffer*		     m_indexBuffer;
    PointTriangleOctree*     m_pOctree;
    int                      m_numTriangles;

    std::vector<Sangfroid::Maths::Point3f>     m_edgePoints;
    TriangleEdge::EdgeMap                       m_edgeMap;
    TriangleEdge::ReverseEdgeMap                m_revEdgeMap;
    IndexedTriangleList                         m_triangleList;
    std::vector<TriangleMesh*>			        m_triangleMeshes;
    std::vector<IndexedTriangleList>            m_separatedTriangulations;
    int*		                                m_uniqueIndexBuffer;
    TriangleEdgeList                            m_faceBoundingEdges;
    MeshBoundaryPatch*                          m_boundaryPatch;

    static int s_meshCount;
};
/*-----------------------------------------------------------------------------------*/

struct MeshBoundaryPatch
{
    TriangleMeshImpl*        ParentMesh;
    TriangleEdgeList         BoundaryEdges;
    IndexedTriangleList      BoundaryTriangles;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

