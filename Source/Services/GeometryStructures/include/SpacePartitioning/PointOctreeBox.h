/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "BaseOctreeBox.h"
#include "BaseOctree.h"
#include "Point3f.h"
#include "Vector3f.h"
#include <vector>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/
GEOMETRY_STRUCTURES_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/
class PointOctree;
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointOctreeBox : public BaseOctreeBox
{

protected:

   std::vector<int> m_pointIndexList;

protected:

   void InitialiseMembers();
   void CleanUp();

public:

    PointOctreeBox(PointOctree* pTree,
             PointOctreeBox* parent,
             const int depthLevel,
             const Sangfroid::Maths::Point3f& min,
             const Sangfroid::Maths::Point3f& max,
             const bool bLazyExpansion);

   virtual ~PointOctreeBox();

   const std::vector<int>& GetPoints() const;

   PointOctreeBox* Add(PointOctree* pOctree,
               const Sangfroid::Maths::Point3f& pt,
               const int index);

protected:

   virtual void CreateChildren(BaseOctree* pTree) override;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END
