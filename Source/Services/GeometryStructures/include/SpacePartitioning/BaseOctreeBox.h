/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "Point3f.h"
#include "Vector3f.h"
#include <vector>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_START

class BaseOctree;

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP BaseOctreeBox
{

protected:
   
   BaseOctree*                         m_pTree;
   BaseOctreeBox*                      m_pParent;
   BaseOctreeBox**                     m_ppChildren;
   Sangfroid::Maths::Point3f          m_min;
   Sangfroid::Maths::Point3f          m_max;
   int                                 m_depthLevel;
   bool                                m_bLazyExpansion;

   void InitialiseMembers();

   void CleanUp();

public:

    BaseOctreeBox();

    BaseOctreeBox(BaseOctree* pTree,
        BaseOctreeBox* parent,
             const int depthLevel, 
             const Sangfroid::Maths::Point3f& min, 
             const Sangfroid::Maths::Point3f& max, 
             const bool bLazyExpansion);

   virtual ~BaseOctreeBox();

   BaseOctreeBox** GetChildren() const;

   BaseOctreeBox* GetParent() const;

   BaseOctree* Tree() const;

   bool IsRootBox() const;

   bool IsLeafBox() const;

   bool IsInside(const Sangfroid::Maths::Point3f& pt) const;

   const BaseOctreeBox* BoxForPoint(const Sangfroid::Maths::Point3f& pt) const;

protected:

   virtual void CreateChildren(BaseOctree* pTree);
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

