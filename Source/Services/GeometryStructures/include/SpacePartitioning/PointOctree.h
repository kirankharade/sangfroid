/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsExportDefs.h"
#include "MathsIncludes.h"
#include "BaseOctree.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include <vector>
#include <map>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/
class PointBuffer;
class PointOctreeBox;
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointOctree : public BaseOctree
{
protected:

    std::vector<PointOctreeBox*> m_index2BoxList;

protected:

    void addPointBuffer(PointBuffer* buffer);

public:

    PointOctree();

    PointOctree(const Sangfroid::Maths::Point3f& min, const Sangfroid::Maths::Point3f& max, const int treeDepth, const bool bLazyExpansion);

    PointOctree(PointBuffer* posBuffer, const int treeDepth, const bool bLazyExpansion);

    virtual ~PointOctree(void);

    PointOctreeBox* Add(const Sangfroid::Maths::Point3f& pt, const int index);

    const PointOctreeBox* BoxForPointIndex(const int index) const;

};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END
