/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsExportDefs.h"
#include "MathsDefines.h"
#include "BaseOctree.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include <vector>
#include <map>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/
GEOMETRY_STRUCTURES_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointUnifierOctree : public BaseOctree
{

protected:

   std::vector<int> addPointBuffer(PointBuffer* buffer);

public:

   PointUnifierOctree(
      const Sangfroid::Maths::Point3f& min, 
      const Sangfroid::Maths::Point3f& max, 
      const int treeDepth, 
      const bool bLazyExpansion);

   PointUnifierOctree(
      PointBuffer* posBuffer, 
      const int treeDepth, 
      const bool bLazyExpansion);
   
   virtual ~PointUnifierOctree();
	
   int Add(const Sangfroid::Maths::Point3f& pt);

   PointBuffer* UnifiedPointBuffer() const;

   const UltraVector<Sangfroid::Maths::Point3f>& Vertices();

   const Sangfroid::Maths::Point3f& PointAt(const int index) const;

protected:

   friend class PointUnifierOctreeBox;

   UltraVector<Sangfroid::Maths::Point3f> m_points;

};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

