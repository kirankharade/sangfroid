/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsExportDefs.h"
#include "MathsIncludes.h"
#include "PointOctree.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include <vector>
#include <map>
/*-----------------------------------------------------------------------------------*/

#pragma warning(disable : 4505)

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/
class PointTriangleOctreeBox;
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointTriangleOctree : public PointOctree
{
protected:

    std::vector<PointTriangleOctreeBox*>		    m_triangleIndex2BoxList;

public:

    PointTriangleOctree(const Sangfroid::Maths::Point3f& min, const Sangfroid::Maths::Point3f& max, const int treeDepth, const bool bLazyExpansion);

    PointTriangleOctree(PointBuffer* posBuffer, const int treeDepth, const bool bLazyExpansion);

    virtual ~PointTriangleOctree(void);

    PointTriangleOctreeBox* AddTriangle(const Sangfroid::Maths::Point3f& centroid, const int index);

    const PointTriangleOctreeBox* BoxForTriangleIndex(const int index) const;

    void SetTriangleCount(int triangleCount);

};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END
