/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsExportDefs.h"
#include "MathsDefines.h"
#include "BaseOctreeBox.h"
#include "BaseOctree.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"
#include <vector>
#include <map>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/
class PointUnifierOctree;
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointUnifierOctreeBox : public BaseOctreeBox
{
public:

   PointUnifierOctreeBox(PointUnifierOctree* pTree,
             PointUnifierOctreeBox* parent,
             const int depthLevel, 
             const Sangfroid::Maths::Point3f& min, 
             const Sangfroid::Maths::Point3f& max, 
             const bool bLazyExpansion);

   virtual ~PointUnifierOctreeBox();

   const std::vector<int>& GetPoints() const;

   int Add(const Sangfroid::Maths::Point3f& pt);

protected:

   int insertToLeafBox(const Sangfroid::Maths::Point3f& pt);

   virtual void CreateChildren(BaseOctree* pTree) override;

   void InitialiseMembers();

   void CleanUp();

protected:

   std::vector<int>     m_uniqueVertexIndexList;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

