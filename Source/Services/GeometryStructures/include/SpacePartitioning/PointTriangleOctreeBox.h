/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsNamespace.h"
#include "GeoElementsExportDefs.h"
#include "PointTriangleOctree.h"
#include "PointOctreeBox.h"
#include <vector>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointTriangleOctreeBox : public PointOctreeBox
{

protected:

   std::vector<int>     m_triangleIndexList;

protected:

   void InitialiseMembers();
   void CleanUp();

public:

    PointTriangleOctreeBox(PointTriangleOctree* pTree,
            PointTriangleOctreeBox* parent,
             const int depthLevel,
             const Sangfroid::Maths::Point3f& min,
             const Sangfroid::Maths::Point3f& max,
             const bool bLazyExpansion);

   virtual ~PointTriangleOctreeBox();

   const std::vector<int>& GetTriangles() const;

   PointTriangleOctreeBox* AddTriangle(PointTriangleOctree* pOctree, const Sangfroid::Maths::Point3f& centroid, const int index);

protected:

   virtual void CreateChildren(BaseOctree* pTree) override;
};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END
