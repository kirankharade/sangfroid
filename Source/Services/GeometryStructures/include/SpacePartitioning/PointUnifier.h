﻿/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsExportDefs.h"
#include "GeoElementsNamespace.h"
#include "PointUnifierOctree.h"
#include "PointBuffer.h"
#include "Containers/UltraVector.h"

#include <iostream>

/*-----------------------------------------------------------------------------------*/
GEOMETRY_STRUCTURES_NAMESPACE_START
/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP PointUnifier
{
private:

    public:

       PointUnifier(PointBuffer* buffer);

       PointUnifier(
          const Sangfroid::Maths::Point3f& min,
          const Sangfroid::Maths::Point3f& max);

       ~PointUnifier();

       int Add(const Sangfroid::Maths::Point3f& pt);

       const UltraVector<Sangfroid::Maths::Point3f>& Vertices() const;

       PointBuffer* VerticesAsBuffer() const;

private:

   PointUnifierOctree* m_pOctree;

};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

