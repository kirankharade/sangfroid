/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include "GeoElementsExportDefs.h"
#include "BaseOctreeBox.h"
#include "Point3f.h"
#include "Vector3f.h"
#include <vector>
#include <map>
/*-----------------------------------------------------------------------------------*/
#pragma warning(disable : 4505)
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

class GEOMETRY_STRUCTURES_IMP_EXP BaseOctree
{
protected:

    BaseOctreeBox*                  m_pRoot;
    int                             m_treeDepth;
    Sangfroid::Maths::Point3f      m_min;
    Sangfroid::Maths::Point3f      m_max;
    bool                            m_bLazyExpansion;

private:

    void InitialiseMembers();

    void CleanUp();

public:

    BaseOctree();

    BaseOctree(
        const Sangfroid::Maths::Point3f& min,
        const Sangfroid::Maths::Point3f& max,
        const int treeDepth,
        const bool bLazyExpansion);

    virtual ~BaseOctree();

    int GetTreeDepth() const;

    Sangfroid::Maths::Point3f GetMin() const;
    Sangfroid::Maths::Point3f Getmax() const;

    const BaseOctreeBox* BoxForPoint(const Sangfroid::Maths::Point3f& pt) const;

    static int CalculateTreeDepth(const int vertexCount);

};
/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

