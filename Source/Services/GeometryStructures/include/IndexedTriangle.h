/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "GeoElementsNamespace.h"
#include "TriangleEdge.h"
#include <vector>

GEOMETRY_STRUCTURES_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

struct IndexedTriangle
{
   int ID;
   int MeshIndex;

   //! Vertex Indices
   int i;
   int j;
   int k;

   //! Triangle edge indices
   int ea;
   int eb;
   int ec;

   bool IsFacePartitioned;
   bool IsTriangleOnEdge;

   //! Default constructor
   IndexedTriangle()
   {
      ID = -1;
      MeshIndex = -1;

      i = -1;
      j = -1;
      k = -1;

      ea = -1;
      eb = -1;
      ec = -1;

      IsFacePartitioned = false;
      IsTriangleOnEdge = false;
   }

   //! Constructor
   IndexedTriangle(const int a, const int b, const int c)
   {
      ID = -1;
      MeshIndex = -1;

      i = a;
      j = b;
      k = c;

      ea = -1;
      eb = -1;
      ec = -1;

      IsFacePartitioned = false;
      IsTriangleOnEdge = false;
   }

   IndexedTriangle(const IndexedTriangle& other)
   {
      ID = other.ID;
      MeshIndex = other.MeshIndex;

      i = other.i;
      j = other.j;
      k = other.k;

      ea = other.ea;
      eb = other.eb;
      ec = other.ec;

      IsFacePartitioned = other.IsFacePartitioned;
      IsTriangleOnEdge = other.IsTriangleOnEdge;
   }

   IndexedTriangle& operator = (const IndexedTriangle& other)
   {
      ID = other.ID;
      MeshIndex = other.MeshIndex;

      i = other.i;
      j = other.j;
      k = other.k;

      ea = other.ea;
      eb = other.eb;
      ec = other.ec;

      IsFacePartitioned = other.IsFacePartitioned;
      IsTriangleOnEdge = other.IsTriangleOnEdge;

      return *this;
   }

};
/*-----------------------------------------------------------------------------------*/

typedef std::vector<IndexedTriangle*> IndexedTriangleList;

/*-----------------------------------------------------------------------------------*/

GEOMETRY_STRUCTURES_NAMESPACE_END

