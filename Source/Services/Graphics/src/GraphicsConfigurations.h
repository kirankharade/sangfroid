//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_GRAPHICS_CONFIGURATIONS_H__
#define __AN_GRAPHICS_CONFIGURATIONS_H__
//---------------------------------------------------------------------------------------

#include <stdio.h> 

//-------------------------------------------------------------------------
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
    #define _AN_BUILD_GRAPHICS_FOR_WINDOWS_
#endif
//-------------------------------------------------------------------------

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_
    /////////////////////////////////////////
    #ifdef AN_GRAPHICS_STATIC_LIBRARY
        #define AN_GRAPHICS_API
    #else
        #ifdef AN_GRAPHICS_EXPORT_API
            #define AN_GRAPHICS_API __declspec(dllexport)
        #else
            #define AN_GRAPHICS_API __declspec(dllimport)
        #endif
    #endif
    
    #ifdef _AN_GRAPHICS_STDCALL_SUPPORTED_
        #define AN_GRAPHICS_CALL_CONVENTION __stdcall
    #else
        #define AN_GRAPHICS_CALL_CONVENTION __cdecl
    #endif
    
    
    //Warnings for deprecation disabled...
    #if defined(_MSC_VER) && (_MSC_VER >= 1400)
        #pragma warning( disable: 4996)
        #pragma warning( disable: 4800)
        #define _CRT_SECURE_NO_DEPRECATE 1
        #define _CRT_NONSTDC_NO_DEPRECATE 1
    #endif
        
    /////////////////////////////////////////
#else

    // Forced exports in gcc for shared libs
    #if (__GNUC__ >= 4) && defined(AN_GRAPHICS_EXPORT_API) && !defined(AN_GRAPHICS_STATIC_LIBRARY) 
        #define AN_GRAPHICS __attribute__ ((visibility("default")))
    #else
        #define AN_GRAPHICS_API
    #endif

        #define AN_GRAPHICS_CALL_CONVENTION
        
#endif
//-------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#endif //__AN_GRAPHICS_CONFIGURATIONS_H__
//---------------------------------------------------------------------------------------
