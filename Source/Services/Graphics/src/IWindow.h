//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_WINDOW_H__
#define __I_WINDOW_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "SGLContextParams.h"
#include "SEvent.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
class IGraphicsContext;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IWindow
{

protected:

	IWindow(){};

public:

    IWindow(const SGLContextParams& contextParams, WindowID existingWindowID = nullptr) {}

	IWindow(const SGLContextParams& contextParams, 
                    WindowID parentID, 
                    int xPosition, int yPosition, 
                    int width, int height)  {}

	virtual ~IWindow(){};

	virtual IViewport* Viewport() const = 0;
	virtual void SetViewport(IViewport* pViewPort) = 0;

	virtual WindowID Id() const = 0;

	virtual WindowID ParentWindowID() const = 0;
	virtual void SetParentWindow(WindowID parentWindowID) = 0;

	virtual void SetVisible(const bool bIsvisible) = 0;
	virtual bool IsVisible() const = 0;

	virtual int Height() const = 0;
	virtual int Width() const = 0;
	virtual void SetSize(const int width, const int height) = 0;

	virtual int XPosition() const = 0;
	virtual int YPosition() const = 0;
	virtual void SetPosition(const int xPosition, const int yPosition) = 0;

	virtual void SetFocussed() = 0;
	virtual bool IsFocussed() const = 0;

    virtual void MakeCurrent() = 0;
    virtual void SwapBuffers() = 0;

protected:

	virtual void OnPaint() = 0;

    virtual void OnLeftButtonDoubleClick(const SEvent& ev) = 0;
	virtual void OnLeftButtonUp(const SEvent& ev) = 0;
	virtual void OnLeftButtonDown(const SEvent& ev) = 0;

	virtual void OnRightButtonDoubleClick(const SEvent& ev) = 0;
	virtual void OnRightButtonDown(const SEvent& ev) = 0;
	virtual void OnRightButtonUp(const SEvent& ev) = 0;

	virtual void OnMiddleButtonDoubleClick(const SEvent& ev) = 0;
	virtual void OnMiddleButtonDown(const SEvent& ev) = 0;
	virtual void OnMiddleButtonUp(const SEvent& ev) = 0;

	virtual void OnMouseMove(const SEvent& ev) = 0;
	virtual void OnMouseWheel(const SEvent& ev) = 0;
	virtual void OnMouseHover(const SEvent& ev) = 0;
	virtual void OnMouseLeave(const SEvent& ev) = 0;
	virtual void OnMouseEnter(const SEvent& ev) = 0;

	virtual void OnKey(const SEvent& ev) = 0;

	virtual void OnSize(const SEvent& ev) = 0;


};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__I_WINDOW_H__
//---------------------------------------------------------------------------------------
