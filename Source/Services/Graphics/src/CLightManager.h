//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LIGHT_MANAGER_H__
#define __C_LIGHT_MANAGER_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "GraphicsSTLExports.h"
#include "../Maths/CPoint3f.h"
#include "CColor.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
class CLight;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CLightManager
{

public:

    CLightManager(IViewport* parentViewport);

    virtual ~CLightManager();

    AUInt16 MaxLightCount() const;

    CLight* AddLight();
	
    CLight* AddLight(const CColor& ambient, const CColor& diffuse, 
					const CColor& specular, const Maths::CPoint3f& position, 
					const Maths::CVector3f& direction = Maths::CVector3f(0, 0, -1));

    CLight* AddSimilarLight(const CLight*& light);

    bool Remove(const AUInt16 lightIndex);

    bool Remove(CLight* light);

	CLight* GetLight(const AInt16 index);

	CLight* GetLight(const std::string& name);

    AUInt16 CurrentCount() const;

    bool EnableLight(CLight* light);

    bool DisableLight(CLight* light);

    bool EnableLight(const std::string& name);

    bool DisableLight(const std::string& name);

    bool EnableLight(const AInt16 index);

    bool DisableLight(const AInt16 index);

    bool EnableAll();

    bool DisableAll();

	bool IsEnabled(const CLight* pLight) const;

	void EnableLighting();

	void DisableLighting();

private:

    void EmptyList();

	AInt32 Add(CLight* light);

	bool CanAddLight() const;

	AInt32 GetAvailableIndex() const;

private:

    IViewport*      m_pViewport;

    void*           m_pListImpl;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LIGHT_MANAGER_H__
//---------------------------------------------------------------------------------------
