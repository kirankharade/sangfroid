//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CColor.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CColor::CColor()
{
    AUByte red = 155;
    AUByte green = 155;
    AUByte blue = 155;
    AUByte alpha = 255;

    m_color = ((((red << 16) | (green << 8)) | blue) | (alpha << 24));
}
//---------------------------------------------------------------------------------------

CColor::~CColor()
{
}
//---------------------------------------------------------------------------------------

CColor::CColor(const AReal32 red, const AReal32 green, const AReal32 blue, const AReal32 alpha)
{
    AUByte ubRed = (AUByte) (255 * red);
    AUByte ubGreen = (AUByte) (255 * green);
    AUByte ubBlue = (AUByte) (255 * blue);
    AUByte ubAlpha = (AUByte) (255 * alpha);

    m_color = ((((ubRed << 16) | (ubGreen << 8)) | ubBlue) | (ubAlpha << 24));
}
//---------------------------------------------------------------------------------------

CColor::CColor(const AUByte red, const AUByte green, const AUByte blue, const AUByte alpha)
{
    m_color = ((((red << 16) | (green << 8)) | blue) | (alpha << 24));
}
//---------------------------------------------------------------------------------------

CColor::CColor(const EPredefinedColor predefColor, const AUByte alpha)
{
    m_color = (AUInt32) predefColor;
    SetAlpha(AReal32 (alpha/255.0f));
}
//---------------------------------------------------------------------------------------

CColor::CColor(const CColor& color)
{
    m_color = color.m_color;
}
//---------------------------------------------------------------------------------------

CColor::CColor(const AUInt32 color)
{
    m_color = color;
}
//---------------------------------------------------------------------------------------

CColor::CColor(const CColor& color, const AUByte alpha)
{
    m_color = color.m_color;
    SetAlpha((AReal32) alpha/255.0f);
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Red() const
{
    return ((AReal32) ((m_color >> 16) & 255) / (AReal32) 255.0);
}
//---------------------------------------------------------------------------------------

void CColor::SetRed(const AReal32 red)
{
    AUByte r = (AUByte) red * 255;
    m_color = ((r & 255) << 16) | (m_color & 0xFF00FFFF);
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Green() const
{
    return ((AReal32) ((m_color >> 8) & 255) / (AReal32) 255.0);
}
//---------------------------------------------------------------------------------------

void CColor::SetGreen(const AReal32 green)
{
    AUByte g = (AUByte) green * 255;
    m_color = ((g & 255) << 8) | (m_color & 0xFFFF00FF);
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Blue() const
{
    return ((AReal32) (m_color & 255) / (AReal32) 255.0);
}
//---------------------------------------------------------------------------------------

void CColor::SetBlue(const AReal32 blue)
{
    AUByte b = (AUByte) blue * 255;
    m_color = (b & 255) | (m_color & 0xFFFFFF00);
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Alpha() const
{
    return ((AReal32) ((m_color >> 24) & 255) / (AReal32) 255.0);
}
//---------------------------------------------------------------------------------------

void CColor::SetAlpha(const AReal32 alpha)
{
    AUByte a = (AUByte) alpha * 255;
    m_color = ((a & 255) << 24) | (m_color & 0x00FFFFFF);
}
//---------------------------------------------------------------------------------------

AUByte CColor::RedAsByte() const
{
    return (AUByte) ((m_color >> 16) & 255);
}
//---------------------------------------------------------------------------------------

void CColor::SetRedAsByte(const AUByte r)
{
    m_color = ((r & 255) << 16) | (m_color & 0xFF00FFFF);
}
//---------------------------------------------------------------------------------------

AUByte CColor::GreenAsByte() const
{
    return (AUByte) ((m_color >> 8) & 255);
}
//---------------------------------------------------------------------------------------

void CColor::SetGreenAsByte(const AUByte g)
{
    m_color = ((g & 255) << 8) | (m_color & 0xFFFF00FF);
}
//---------------------------------------------------------------------------------------

AUByte CColor::BlueAsByte() const
{
    return (AUByte) (m_color & 255);
}
//---------------------------------------------------------------------------------------

void CColor::SetBlueAsByte(const AUByte b)
{
    m_color = (b & 255) | (m_color & 0xFFFFFF00);
}
//---------------------------------------------------------------------------------------

AUByte CColor::AlphaAsByte() const
{
    return (AUByte) ((m_color >> 24) & 255);
}
//---------------------------------------------------------------------------------------

void CColor::SetAlphaAsByte(const AUByte a)
{
    m_color = ((a & 255) << 24) | (m_color & 0x00FFFFFF);
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Hue() const
{
    AReal32 red = Red();
    AReal32 green = Green();
    AReal32 blue = Blue();
    AReal32 hue = 0.0;

    AReal32 max = CUtils::GetMax(red, green, blue);
    AReal32 min = CUtils::GetMin(red, green, blue);
    AReal32 rangeInv = (AReal32) (1.0 / (max - min));

    if(max == min)
    {
        return hue;
    }

    if(red == max)
    {
        hue = rangeInv * (green - blue);
    }
    else if(green == max)
    {
        hue = (AReal32) (2.0 + (rangeInv * (blue - red)));
    }
    else if(blue == max)
    {
        hue = (AReal32) (4.0 + (rangeInv * (red - green)));
    }

    hue *= 60.0;
    if(hue < 0)
    {
        hue += 360.0;
    }
    return hue;
}
//---------------------------------------------------------------------------------------

void CColor::SetHue(const AReal32 hue)
{
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Saturation() const
{
    AReal32 red = Red();
    AReal32 green = Green();
    AReal32 blue = Blue();
    AReal32 max = CUtils::GetMax(red, green, blue);
    AReal32 min = CUtils::GetMin(red, green, blue);
    AReal32 range = max - min;

    if(max == min)
    {
        return 0;
    }

    AReal32 temp = (AReal32) (0.5 * (max + min));
    if(temp <= 0.5)
    {
        return (AReal32) (range / (max + min));
    }
    return (AReal32) (range / ((2.0 - max) - min));
}
//---------------------------------------------------------------------------------------

void CColor::SetSaturation(const AReal32 saturation)
{
}
//---------------------------------------------------------------------------------------

AReal32 CColor::Luminance() const
{
    AReal32 red = Red();
    AReal32 green = Green();
    AReal32 blue = Blue();
    AReal32 max = CUtils::GetMax(red, green, blue);
    AReal32 min = CUtils::GetMin(red, green, blue);

    return (AReal32) (0.5 * (max + min));
}
//---------------------------------------------------------------------------------------

void CColor::SetLuminance(const AReal32 luminance)
{
}
//---------------------------------------------------------------------------------------

CColor CColor::Opaque() const
{
    CColor temp(m_color);
    temp.SetAlpha(1.0);
    return temp;
}
//---------------------------------------------------------------------------------------

SColorRGBAf CColor::ToRGBAf() const
{
    SColorRGBAf c;
	c.r = ((AReal32) ((m_color >> 16) & 255) / (AReal32) 255.0);
	c.g = ((AReal32) ((m_color >> 8) & 255) / (AReal32) 255.0);
	c.b = ((AReal32) (m_color & 255) / (AReal32) 255.0);
	c.a = ((AReal32) ((m_color >> 24) & 255) / (AReal32) 255.0);
    return c;
}
//---------------------------------------------------------------------------------------

SColorARGBf CColor::ToARGBf() const
{
    SColorARGBf c;
	c.r = ((AReal32) ((m_color >> 16) & 255) / (AReal32) 255.0);
	c.g = ((AReal32) ((m_color >> 8) & 255) / (AReal32) 255.0);
	c.b = ((AReal32) (m_color & 255) / (AReal32) 255.0);
	c.a = ((AReal32) ((m_color >> 24) & 255) / (AReal32) 255.0);
    return c;
}
//---------------------------------------------------------------------------------------

SColorRGBAf* CColor::ToRGBAf(const CColor* colors, const unsigned int count)
{
    if(colors == nullptr)
    {
        return nullptr;
    }
    SColorRGBAf* c = new SColorRGBAf[count];
    for(unsigned int i = 0; i < count; i++)
    {
        c[i] = colors[i].ToRGBAf();
    }
    return c;
}
//---------------------------------------------------------------------------------------

SColorRGBAf* CColor::ToRGBAf(const std::vector<CColor> colors)
{
    unsigned int count = colors.size();
    SColorRGBAf* c = new SColorRGBAf[count];
    for(unsigned int i = 0; i < count; i++)
    {
        c[i] = colors[i].ToRGBAf();
    }
    return c;
}
//---------------------------------------------------------------------------------------

SColorARGBf* CColor::ToARGBf(const CColor* colors, const unsigned int count)
{
    if(colors == nullptr)
    {
        return nullptr;
    }
    SColorARGBf* c = new SColorARGBf[count];
    for(unsigned int i = 0; i < count; i++)
    {
        c[i] = colors[i].ToARGBf();
    }
    return c;
}
//---------------------------------------------------------------------------------------

SColorARGBf* CColor::ToARGBf(const std::vector<CColor> colors)
{
    unsigned int count = colors.size();
    SColorARGBf* c = new SColorARGBf[count];
    for(unsigned int i = 0; i < count; i++)
    {
        c[i] = colors[i].ToARGBf();
    }
    return c;
}
//---------------------------------------------------------------------------------------


CColor& CColor::operator = (const CColor& color)
{
    m_color = color.m_color;
    return *this;
}
//---------------------------------------------------------------------------------------

bool CColor::operator == (const CColor& color) const
{
    return (m_color == color.m_color);
}
//---------------------------------------------------------------------------------------

bool CColor::operator != (const CColor& color) const
{
    return (m_color != color.m_color);
}
//---------------------------------------------------------------------------------------

CColor CColor::Average(const CColor& colorA, const CColor& colorB)
{
    AReal32 r = 0.5f * (colorA.Red()   + colorB.Red());
    AReal32 g = 0.5f * (colorA.Green() + colorB.Green());
    AReal32 b = 0.5f * (colorA.Blue()  + colorB.Blue());
    AReal32 a = 0.5f * (colorA.Alpha() + colorB.Alpha());

    return CColor(r, g, b, a);
}
//---------------------------------------------------------------------------------------

CColor CColor::Average(const CColor& colorA, const CColor& colorB,
                       const CColor& colorC, const CColor& colorD)
{
    AReal32 r = 0.25f * (colorA.Red()   + colorB.Red()   + colorC.Red()   + colorD.Red()  );
    AReal32 g = 0.25f * (colorA.Green() + colorB.Green() + colorC.Green() + colorD.Green());
    AReal32 b = 0.25f * (colorA.Blue()  + colorB.Blue()  + colorC.Blue()  + colorD.Blue() );
    AReal32 a = 0.25f * (colorA.Alpha() + colorB.Alpha() + colorC.Alpha() + colorD.Alpha());

    return CColor(r, g, b, a);
}
//---------------------------------------------------------------------------------------


