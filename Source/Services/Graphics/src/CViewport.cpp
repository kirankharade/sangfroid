//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/MathsDefines.h"
#include "CViewport.h"
#include "IEventProcessor.h"
#include "CEventProcessor.h"
#include "CLayer3D.h"
#include "CLayer2D.h"
#include "CBackGround.h"
#include "CCamera.h"
#include "CLightManager.h"
#include "SEvent.h"
#include "CGraphicsUtils.h"
#include "IFactory.h"
#include "IGraphics.h"
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CViewport* CViewport::New(IWindow* parentWindow)
{
    CViewport* pVP = new CViewport(parentWindow);
    pVP->Initialize();
    return pVP;
}
//---------------------------------------------------------------------------------------

CViewport::CViewport(IWindow* parentWindow)
: IViewport(),
  m_parentWindow(parentWindow),
  m_pBackground(nullptr),
  m_p2DBackgroundLayer(nullptr),
  m_p2DForegroundLayer(nullptr),
  m_p3DLayer(nullptr),
  m_pEventProcessor(nullptr),
  m_currSceneRotationCentre(ORIGIN),
  m_bIsVisible(true),
  m_pLightManager(nullptr),
  m_id(-1),
  m_name("Viewport")
{
}
//---------------------------------------------------------------------------------------

CViewport::~CViewport()
{
    CleanUp();
}
//---------------------------------------------------------------------------------------

void CViewport::Initialize()
{
    if(m_parentWindow)
    {
        m_parentWindow->SetViewport(this);
    }

    m_pBackground = new CBackGround(this);
    m_p2DBackgroundLayer = new CLayer2D(this);
    m_p2DForegroundLayer = new CLayer2D(this);
    m_p3DLayer = new CLayer3D(this);

    m_pEventProcessor = new CEventProcessor(this);
	m_pLightManager = new CLightManager(this);

    m_id = CGraphicsUtils::GetUniqueObjectID();
}
//---------------------------------------------------------------------------------------

const std::string& CViewport::Name() const
{
    return m_name;
}
//---------------------------------------------------------------------------------------

void CViewport::SetName(const std::string& name)
{
    m_name = name;
}
//---------------------------------------------------------------------------------------

void CViewport::CleanUp()
{
}
//---------------------------------------------------------------------------------------

void CViewport::Update()
{
}
//---------------------------------------------------------------------------------------

long CViewport::Id() const
{
    return m_id;
}
//---------------------------------------------------------------------------------------

IWindow* CViewport::ParentWindow() const
{
    return m_parentWindow;
}
//---------------------------------------------------------------------------------------

void CViewport::SetParentWindow(IWindow* window)
{
    m_parentWindow = window;
    if(m_parentWindow)
    {
        m_parentWindow->SetViewport(this);
    }
}
//---------------------------------------------------------------------------------------

CLayer2D* CViewport::LayerForeground2D() const
{
    return m_p2DForegroundLayer;
}
//---------------------------------------------------------------------------------------

CLayer2D* CViewport::LayerBackground2D() const
{
    return m_p2DBackgroundLayer;
}
//---------------------------------------------------------------------------------------

CLayer3D* CViewport::Layer3D() const
{
    return m_p3DLayer;
}
//---------------------------------------------------------------------------------------

CBackGround* CViewport::BackGround() const
{
    return m_pBackground;
}
//---------------------------------------------------------------------------------------

void CViewport::SetBackGround(CBackGround* bg)
{
    if(bg != nullptr)
    {
        m_pBackground = bg;
    }
}
//---------------------------------------------------------------------------------------

CLightManager* CViewport::LightManager() const
{
	return m_pLightManager;
}
//---------------------------------------------------------------------------------------

bool CViewport::Visible() const
{
    if(m_bIsVisible)
    {
        if(m_parentWindow)
        {
            return (m_parentWindow->IsVisible() && m_bIsVisible);
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

void  CViewport::SetVisible(const bool bIsVisible)
{
    m_bIsVisible = bIsVisible;
    Update();
}
//---------------------------------------------------------------------------------------

bool CViewport::Enabled(const EViewportAction action) const
{
    return false;
}
//---------------------------------------------------------------------------------------

void CViewport::SetEnabled(const EViewportAction action, const bool bEnabled)
{
}
//---------------------------------------------------------------------------------------

void CViewport::SetCurrentRotationCentre(const Maths::CPoint3f& centre)
{
    m_currSceneRotationCentre = centre;
    Update();
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f CViewport::CurrentRotationCentre() const
{
    return m_currSceneRotationCentre;
}
//---------------------------------------------------------------------------------------

void CViewport::ResetCurrentRotationCentre()
{
    //or get from scene
    m_currSceneRotationCentre = ORIGIN;
    Update();
}
//---------------------------------------------------------------------------------------

int  CViewport::Height() const
{
    if(m_parentWindow)
    {
        return m_parentWindow->Height();
    }
    return 0;
}
//---------------------------------------------------------------------------------------

int  CViewport::Width() const
{
    if(m_parentWindow)
    {
        return m_parentWindow->Width();
    }
    return 0;
}
//---------------------------------------------------------------------------------------

void  CViewport::SetSize(const int width, const int height)
{
    if(m_parentWindow)
    {
        m_parentWindow->SetSize(width, height);
        Resize();
    }
}
//---------------------------------------------------------------------------------------

IEventProcessor*  CViewport::EventProcessor() const
{
    return m_pEventProcessor;
}
//---------------------------------------------------------------------------------------

void CViewport::Render()
{
    int height = Height();
    int width = Width();
    if(height < 3 || width < 3)
    {
        return;
    }

    if(nullptr == m_pBackground)
    {
        m_pBackground = new CBackGround(this);
    }

    //Put a mutex lock guard here...No need to unlock.
    //Will automatically unlock when lock goes out of scope.
    //std::lock_guard<std::mutex> lock(m_renderMutex);  

    if( Visible() )
    {
        m_parentWindow->MakeCurrent();

        BeginScene();

        if(m_pBackground)
        {
            m_pBackground->Render();
        }

        if(m_p2DBackgroundLayer)
        {
            m_p2DBackgroundLayer->Render();
        }
        if(m_p3DLayer)
        {
            IGraphics::Gfx()->DefaultSetUp(this);
            m_p3DLayer->Render();
        }
        if(m_p2DForegroundLayer)
        {
            m_p2DForegroundLayer->Render();
        }

        EndScene();
        if(m_parentWindow)
        {
            m_parentWindow->SwapBuffers();
        }
    }
}
//---------------------------------------------------------------------------------------

void CViewport::BeginScene()
{
    IGraphics* g = IGraphics::Gfx();
    if(g)
    {
        g->SetViewport(this);
    }
}
//---------------------------------------------------------------------------------------

void CViewport::EndScene()
{
}
//---------------------------------------------------------------------------------------

void CViewport::Resize()
{
    if(!m_parentWindow)
    {
        return;
    }
    int height = Height();
    int width  = Width();

    if( ! (height > 0 && width > 0) )
    {
        return;
    }

    m_parentWindow->MakeCurrent();

    //resize background and layers
    if(m_pBackground)
    {
        m_pBackground->Resize(width, height);
    }
    if(m_p2DBackgroundLayer)
    {
        m_p2DBackgroundLayer->Resize(width, height);
    }
    if(m_p3DLayer)
    {
        m_p3DLayer->Resize(width, height);
    }
    if(m_p2DForegroundLayer)
    {
        m_p2DForegroundLayer->Resize(width, height);
    }

}
//---------------------------------------------------------------------------------------

void  CViewport::FitView(const bool bUpdate)
{
}
//---------------------------------------------------------------------------------------

void  CViewport::FitView()
{
}
//---------------------------------------------------------------------------------------

void  CViewport::ResetView()
{
    //May be ask camera to reset view
}
//---------------------------------------------------------------------------------------

void  CViewport::Invalidate()
{
    Render();
}
//---------------------------------------------------------------------------------------

void CViewport::UpdateListeners()
{
    //NotifyListeners(MiscUtils::eLE_VIEW_TRANSFORMATION_UPDATE);
}
//---------------------------------------------------------------------------------------

