//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_UNIQUE_ITEM_MAP_H__
#define __C_UNIQUE_ITEM_MAP_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include <map>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
class CUniqueItemMap
{

public:

    CUniqueItemMap();

    virtual ~CUniqueItemMap();

    bool add(TKey key, TValue value);

    bool remove(TKey key);

    TValue find(const TKey& key) const;

    bool exists(const TKey& key) const;

    unsigned int size() const;

    void clear();

    TValue operator [] (const TKey& key);

    TValue GetByIndex(const AUInt32& index);
	
private:

    std::map<TKey, TValue>*   m_items;
};
//---------------------------------------------------------------------------------------
//Implementation
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
CUniqueItemMap<TKey, TValue>::CUniqueItemMap()
    :m_items(nullptr)
{
   m_items = new std::map<TKey, TValue>();
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
CUniqueItemMap<TKey, TValue>::~CUniqueItemMap()
{
   m_items->clear();
   SAFE_CLEANUP(m_items);
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
bool CUniqueItemMap<TKey, TValue>::add(TKey key, TValue value)
{
    std::map<TKey, TValue>::iterator itr = m_items->find(key);
    if(itr == m_items->end())
    {
		m_items->insert(std::make_pair(key, value));
		return true;
	}
    return false;
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
bool CUniqueItemMap<TKey, TValue>::remove(TKey key)
{
    std::map<TKey, TValue>::iterator itr = m_items->find(key);
    if(itr != m_items->end())
    {
        m_items->erase(itr);
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
void CUniqueItemMap<TKey, TValue>::clear()
{
    m_items->clear();
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
TValue CUniqueItemMap<TKey, TValue>::find(const TKey& key) const
{
    std::map<TKey, TValue>::iterator itr = m_items->find(key);
    if(itr != m_items->end())
    {
		return itr->second;
    }
    return (TValue) nullptr;
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
bool CUniqueItemMap<TKey, TValue>::exists(const TKey& key) const
{
    std::map<TKey, TValue>::iterator itr = m_items->find(key);
    if(itr != m_items->end())
    {
		return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
unsigned int CUniqueItemMap<TKey, TValue>::size() const
{
    return m_items->size();
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
TValue CUniqueItemMap<TKey, TValue>::GetByIndex(const AUInt32& index)
{
    std::map<TKey, TValue>::iterator itr = m_items->begin();
	int count = 0;
    while(itr != m_items->end())
    {
		if(count == index)
		{
			return itr->second;
		}
		count++;
    }
    return (TValue) nullptr;
}
//---------------------------------------------------------------------------------------

template<typename TKey, typename TValue>
TValue CUniqueItemMap<TKey, TValue>::operator [] (const TKey& key)
{
    std::map<TKey, TValue>::iterator itr = m_items->find(key);
    if(itr != m_items->end())
    {
		return itr->second;
    }
    return (TValue) nullptr;
}
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_UNIQUE_ITEM_MAP_H__
//---------------------------------------------------------------------------------------
