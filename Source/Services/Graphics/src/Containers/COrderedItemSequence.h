//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_ORDERED_ITEM_SEQUENCE_H__
#define __C_ORDERED_ITEM_SEQUENCE_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include <vector>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

template<typename T>
class COrderedItemSequence
{

public:

    COrderedItemSequence();
    virtual ~COrderedItemSequence();

    bool addAtIndex(T item, const int index);
    bool addAtFront(T item);
    bool addAtBack(T item);

    bool remove(T item);

    void setToFront(T item);
    void setToBack(T item);

    bool forward(T item, const unsigned int& step);
    bool backward(T item, const unsigned int& step);

    int index(T item) const;
    bool find(T item) const;

    unsigned int size() const;

    T operator [] (const unsigned int& i);

private:

    std::vector<T>*   m_items;
};
//---------------------------------------------------------------------------------------
//Implementation
//---------------------------------------------------------------------------------------

template<typename T>
COrderedItemSequence<T>::COrderedItemSequence()
    :m_items(nullptr)
{
   m_items = new std::vector<T>();
}
//---------------------------------------------------------------------------------------

template<typename T>
COrderedItemSequence<T>::~COrderedItemSequence()
{
   m_items->clear();
   SAFE_CLEANUP(m_items);
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::addAtIndex(T item, const int index)
{
    if(index < 0)
    {
        return false;
    }
    if(find(item))
    {
        //Already exists
        return false;
    }
    if((int) (m_items->size() - 1) < index)
    {
        //Add at end
        m_items->push_back(item);
        return true;
    }
    std::vector<T>::iterator itr = m_items->begin();
    m_items->insert(itr + index, item);
    return true;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::addAtFront(T item)
{
    if(find(item))
    {
        //Already exists
        return false;
    }
    m_items->insert(m_items->begin(), item);
    return true;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::addAtBack(T item)
{
    if(find(item))
    {
        //Already exists
        return false;
    }
    m_items->push_back(item);
    return true;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::remove(T item)
{
    std::vector<T>::iterator itr = m_items->begin();
    while(itr != m_items->end())
    {
        if(*itr == item)
        {
            m_items->erase(itr);
            return true;
        }
        itr++;
    }
    return false;
}
//---------------------------------------------------------------------------------------

template<typename T>
void COrderedItemSequence<T>::setToFront(T item)
{
    if(!find(item))
    {
        m_items->insert(m_items->begin(), item);
        return;
    }
    remove(item);
    m_items->insert(m_items->begin(), item);
}
//---------------------------------------------------------------------------------------

template<typename T>
void COrderedItemSequence<T>::setToBack(T item)
{
    if(!find(item))
    {
        m_items->push_back(item);
        return;
    }
    remove(item);
    m_items->push_back(item);
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::forward(T item, const unsigned int& step)
{
    int idx = index(item);
    if(idx == -1)
    {
        //Doesn't exist
        return false;
    }
    idx -= step;
    remove(item);
    if(idx < 0)
    {
        m_items->insert(m_items->begin(), item);
        return true;
    }
    m_items->insert(m_items->begin() + idx, item);
    return true;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::backward(T item, const unsigned int& step)
{
    int idx = index(item);
    if(idx == -1)
    {
        //Doesn't exist
        return false;
    }
    idx += step;
    remove(item);
    if(idx >= ((int) m_items->size()))
    {
        m_items->push_back(item);
        return true;
    }
    m_items->insert(m_items->begin() + idx, item);
    return true;
}
//---------------------------------------------------------------------------------------

template<typename T>
int COrderedItemSequence<T>::index(T item) const
{
    std::vector<T>::iterator itr = m_items->begin();
    int index = 0;
    while(itr != m_items->end())
    {
        if(*itr == item)
        {
            return index;
        }
        itr++;
        index++;
    }
    return -1;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool COrderedItemSequence<T>::find(T item) const
{
    std::vector<T>::iterator itr = m_items->begin();
    while(itr != m_items->end())
    {
        if(*itr == item)
        {
            return true;
        }
        itr++;
    }
    return false;
}
//---------------------------------------------------------------------------------------

template<typename T>
unsigned int COrderedItemSequence<T>::size() const
{
    return m_items->size();
}
//---------------------------------------------------------------------------------------

template<typename T>
T COrderedItemSequence<T>::operator [] (const unsigned int& i)
{
    if((i < 0) || (i >= m_items->size()))
    {
        return (T) nullptr;
    }
    return m_items->operator[](i);
}
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_ORDERED_ITEM_SEQUENCE_H__
//---------------------------------------------------------------------------------------
