//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_UNIQUE_ITEM_LIST_H__
#define __C_UNIQUE_ITEM_LIST_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include <vector>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

template<typename T>
class CUniqueItemList
{

public:

    CUniqueItemList();

    virtual ~CUniqueItemList();

    bool add(T item);

    bool remove(T item);

    int index(T item) const;

    bool find(T item) const;

    unsigned int size() const;

    void clear();

    T operator [] (const unsigned int& i);

private:

    std::vector<T>*   m_items;
};
//---------------------------------------------------------------------------------------
//Implementation
//---------------------------------------------------------------------------------------

template<typename T>
CUniqueItemList<T>::CUniqueItemList()
    :m_items(nullptr)
{
   m_items = new std::vector<T>();
}
//---------------------------------------------------------------------------------------

template<typename T>
CUniqueItemList<T>::~CUniqueItemList()
{
   m_items->clear();
   SAFE_CLEANUP(m_items);
}
//---------------------------------------------------------------------------------------

template<typename T>
bool CUniqueItemList<T>::add(T item)
{
    if(find(item))
    {
        //Already exists
        return false;
    }
    m_items->push_back(item);
    return true;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool CUniqueItemList<T>::remove(T item)
{
    std::vector<T>::iterator itr = m_items->begin();
    while(itr != m_items->end())
    {
        if(*itr == item)
        {
            m_items->erase(itr);
            return true;
        }
        itr++;
    }
    return false;
}
//---------------------------------------------------------------------------------------

template<typename T>
int CUniqueItemList<T>::index(T item) const
{
    std::vector<T>::iterator itr = m_items->begin();
    int index = 0;
    while(itr != m_items->end())
    {
        if(*itr == item)
        {
            return index;
        }
        itr++;
        index++;
    }
    return -1;
}
//---------------------------------------------------------------------------------------

template<typename T>
bool CUniqueItemList<T>::find(T item) const
{
    std::vector<T>::iterator itr = m_items->begin();
    while(itr != m_items->end())
    {
        if(*itr == item)
        {
            return true;
        }
        itr++;
    }
    return false;
}
//---------------------------------------------------------------------------------------

template<typename T>
unsigned int CUniqueItemList<T>::size() const
{
    return m_items->size();
}
//---------------------------------------------------------------------------------------

template<typename T>
void CUniqueItemList<T>::clear()
{
    return m_items->clear();
}
//---------------------------------------------------------------------------------------

template<typename T>
T CUniqueItemList<T>::operator [] (const unsigned int& i)
{
    if((i < 0) || (i >= m_items->size()))
    {
        return (T) nullptr;
    }
    return m_items->operator[](i);
}
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_UNIQUE_ITEM_LIST_H__
//---------------------------------------------------------------------------------------
