//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "CBackGround.h"
#include "Defaults.h"
#include "Dependencies/glew/GL/glew.h"
#include "IFactory.h"
#include "IGraphics.h"
#include "IDrawManager.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CBackGround::CBackGround(IViewport* parentViewport)
    : m_pParentViewport(parentViewport)
{
    m_colors[0] = Defaults::BackGroundColor;
    m_colors[1] = Defaults::BackGroundColor;
    m_colors[2] = Defaults::BackGroundColor;
    m_colors[3] = Defaults::BackGroundColor;
    m_colors[4] = Defaults::BackGroundColor;

    m_type = EBackGroundType_Colored;
    m_colorType = EBackGroundColorType_SingleColor;
    m_textureLayoutType = EBackGroundTextureType_Scaled;
}
//---------------------------------------------------------------------------------------

CBackGround::~CBackGround()
{
    m_pParentViewport = nullptr;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetColors(const CColor& color)
{
    m_colors[0] = color;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetColors(const CColor& color1,
                            const CColor& color2)
{
    m_colors[0] = color1;
    m_colors[1] = color2;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetColors(const CColor& color1,
                            const CColor& color2,
                            const CColor& color3)
{
    m_colors[0] = color1;
    m_colors[1] = color2;
    m_colors[2] = color3;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetColors(const CColor& color1, //topLeftColor
                            const CColor& color2, //topRightColor
                            const CColor& color3, //bottomRightColor
                            const CColor& color4) //bottomLeftColor
{
    m_colors[0] = color1;
    m_colors[1] = color2;
    m_colors[2] = color3;
    m_colors[3] = color4;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetColors(const CColor& color1, //topLeftColor
                            const CColor& color2, //topRightColor
                            const CColor& color3, //bottomRightColor
                            const CColor& color4, //bottomLeftColor
                            const CColor& centreColor)
{
    m_colors[0] = color1;
    m_colors[1] = color2;
    m_colors[2] = color3;
    m_colors[3] = color4;
    m_colors[4] = centreColor;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetType(EBackGroundType type)
{
    m_type = type;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetColoringType(EBackGroundColorType type)
{
    m_colorType = type;
}
//---------------------------------------------------------------------------------------

void CBackGround::SetTextureLayoutType(EBackGroundTextureType type)
{
    m_textureLayoutType = type;
}
//--------------------------------------------------------------------------------------

EBackGroundType CBackGround::Type() const
{
    return m_type;
}
//---------------------------------------------------------------------------------------

EBackGroundColorType CBackGround::ColoringType() const
{
    return m_colorType;
}
//---------------------------------------------------------------------------------------

EBackGroundTextureType CBackGround::TextureLayoutType() const
{
    return m_textureLayoutType;
}
//---------------------------------------------------------------------------------------

CColor* CBackGround::Colors() const
{
    return (CColor*) m_colors;
}
//---------------------------------------------------------------------------------------

void CBackGround::Render()
{
    IGraphics* g = IGraphics::Gfx();
    if(g)
    {
        IDrawManager* dm = g->DrawManager();
        if(dm)
        {
            dm->Draw(this);
        }
    }
}
//---------------------------------------------------------------------------------------

IViewport* CBackGround::Viewport() const
{
    return m_pParentViewport;
}
//---------------------------------------------------------------------------------------

void CBackGround::Resize(const AUInt32 width, const AUInt32 height)
{
    //currently does nothing
}
//---------------------------------------------------------------------------------------
