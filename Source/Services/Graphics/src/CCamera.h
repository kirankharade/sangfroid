//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_CAMERA_H__
#define __C_CAMERA_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CMatrix4f.h"
#include "../Maths/CPlane.h"
#include "../Maths/MathsEnums.h"
#include "IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class IWindow;
class IViewport;
class IScene;
class CCameraFrustum;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CCamera
{

public:

    CCamera(IViewport* viewport);

    virtual ~CCamera();

    Maths::ECoordSystemHandedness CoordSystemConvention() const;

    void SetLeftHanded();

    void SetRightHanded();

    bool IsLeftHanded() const;

    bool IsRightHanded() const;

    void SetPerspectiveProjection();

    void SetOrthographicsProjection();

    bool IsPerspectiveProjection() const;

    bool IsOrthographicsProjection() const;

    const Maths::CMatrix4f&ProjectionMatrix() const;

    void SetProjectionMatrix(const Maths::CMatrix4f& projection, bool isOrthogonal = true);

    const Maths::CMatrix4f& ViewMatrix() const;

    void SetViewMatrix(const Maths::CMatrix4f& view);

    void SetPositionTargetAndUpVector(const Maths::CPoint3f& pos, const Maths::CPoint3f& target, const Maths::CVector3f& up);

    const Maths::CPoint3f& Position() const;

    void SetPosition(const Maths::CPoint3f& pos);

    const Maths::CPoint3f& Target() const;

    void SetTarget(const Maths::CPoint3f& pos);

    const Maths::CVector3f& UpVector() const;

    void SetUpVector(const Maths::CVector3f& pos);

    Maths::CVector3f ViewDirection() const;

    const Maths::CVector3f HorizontalDirection() const;

    float Near() const;

    void SetNear(const float near);

    float Far() const;

    void SetFar(const float far);

    float AspectRatio() const;

    void SetAspectRatio(const float ar);

    void SetAspectRatio(const int screenHeight, const int screenWidth);

    float FieldOfViewAngle() const;

    void SetFieldOfViewAngle(const float fov);

    float ViewHeight() const;

    void SetViewHeight(const float height, const bool useAspectRatio = false);

    void SetNearFarAndViewHeight(const float near, const float far, const float viewHeight);

    int ViewportHeight() const;

    int ViewportWidth() const;

    int ViewportXPosition() const;

    int ViewportYPosition() const;

    const CCameraFrustum* ViewFrustum() const;

    void SetViewAffectorMatrix(const Maths::CMatrix4f& affector);

    const Maths::CMatrix4f& ViewAffectorMatrix() const;

    virtual void RecalculateProjectionMatrix();

    virtual void RecalculateViewMatrix();

    virtual void RecalculateAspectRatio(const int screenHeight, const int screenWidth);

    virtual void FitToScene(const Maths::CPoint3f& sceneCentre, const float sceneBoundRadius, bool animate = false);

    virtual void FitToSceneWithPresetView(const Maths::CPoint3f& sceneCentre, const float sceneBoundRadius, const EPresetViewType viewType, bool animate = false);

    void InitializeForScene(const Maths::CPoint3f& sceneCentre, const float sceneBoundRadius);

    void SetFitViewMargin(const float viewMargin = 1.05f);

    void Animate(const Maths::CPoint3f& finalTargetPos,
                const Maths::CPoint3f& finalCamPos,
                const Maths::CVector3f& finalUpVect, 
                const float& finalViewHeight );

    virtual void SetForRendering();

protected:

    void Initialize();

    void SanitizeUpVector();

protected:

    IViewport*          m_pViewport;
    bool                m_bOrthogonal;
    Maths::CPoint3f     m_position;
    Maths::CPoint3f     m_target;
    Maths::CVector3f    m_upVector;
    float               m_near;
    float               m_far;
    float               m_fovRad;	     //Field of view angle, in radians. (perspective)

    float               m_sceneHeight;   //Orthographic view (Top - Bottom)
    float               m_sceneWidth;    //Orthographic view (Right - Left)
    float               m_aspectRatio;

    CCameraFrustum*     m_pViewFrustum;
    Maths::CMatrix4f    m_affectorMatrix;
    float               m_fitViewMargin;

    Maths::ECoordSystemHandedness m_handedness;

    friend class CCameraUtils;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_CAMERA_H__
//---------------------------------------------------------------------------------------
