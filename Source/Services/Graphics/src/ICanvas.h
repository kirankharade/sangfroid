//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include <string>
//---------------------------------------------------------------------------------------
#ifndef __I_CANVAS_H__
#define __I_CANVAS_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API ICanvas
{

public:
    ICanvas() {}
    virtual ~ICanvas() {}

    virtual const std::string& Name() const = 0;
    virtual void SetName(const std::string& name) = 0;

    virtual void Render() = 0;
    virtual void Resize() = 0;

    virtual int Height() const = 0;
    virtual int Width() const = 0;
    virtual void SetSize(const int width, const int height) = 0;

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_CANVAS_H__
//---------------------------------------------------------------------------------------

