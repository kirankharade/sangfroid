//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_2DTEXTURE_H__
#define __C_2DTEXTURE_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsDefines.h"
#include "../GraphicsSTLExports.h"
#include "../CColor.h"
#include "ITexture.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class CImage;
class IDrawable;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C2DTexture : public ITexture
{

public:

    C2DTexture(const AUInt32 width, const AUInt32 height, const AUByte* imageData, AUInt16 bitsPerPixel);

    C2DTexture(const CImage* image);

    C2DTexture(const std::string& imageFilename);

    C2DTexture(const AUInt32 width, const AUInt32 height, const CColor& color);

    C2DTexture(const AUInt32 width, const AUInt32 height, const CColor& color, AUInt16 bitsPerPixel);

    C2DTexture(const C2DTexture& texture);

    virtual ~C2DTexture();

    virtual ETextureType Type();

protected:

    void CopyImage(const CImage* image);

    friend class IDrawable;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_2DTEXTURE_H__
//---------------------------------------------------------------------------------------
