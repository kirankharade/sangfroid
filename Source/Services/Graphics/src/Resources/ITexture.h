//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_TEXTURE_H__
#define __I_TEXTURE_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsDefines.h"
#include "../GraphicsSTLExports.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IDrawable;
class IGraphics;
class ITextureManager;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API ITexture
{

public:

    ITexture();

    ITexture(const AUInt32 width, const AUInt32 height, const AUInt32 depth, const AUByte* imageData, AUInt16 bitsPerPixel);

    ITexture(const ITexture& texture);

    virtual ~ITexture();

    virtual ETextureType Type() = 0;

    virtual AUInt32 Width() const;

    virtual AUInt32 Depth() const;

    virtual AUInt32 Height() const;

    virtual const AUByte* Data() const;

    virtual AUByte* Data();

    virtual bool IsDirty();

    virtual void SetDirty(bool bDirty);

protected:

    virtual const AInt32 Id() const;

    virtual const AInt32 OpenGLId() const;

    virtual void SetId(const AUInt32 id);

    virtual void SetOpenGLId(const AUInt32 openGLId);

    void Allocate();

    void SetColor(const CColor& color);

    void Copy(const AUByte* data);

protected:

    AUInt32             m_width;
    AUInt32             m_height;
    AUInt32             m_depth;
    AUByte*             m_pData;
    bool                m_bIsDirty;
    AUInt16             m_bitsPerPixel;
    AInt32              m_textureId;
    AInt32              m_openGLId;
    AUInt32             m_byteCount;

    friend class IDrawable;
    friend class IGraphics;
    friend class ITextureManager;

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_TEXTURE_H__
//---------------------------------------------------------------------------------------
