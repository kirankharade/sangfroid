//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_IMAGE_H__
#define __C_IMAGE_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsDefines.h"
#include "../GraphicsSTLExports.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

enum EPixelFormat
{
    EPixelFormat_R8G8B8A8,
    EPixelFormat_B8G8R8A8,
    EPixelFormat_R8G8B8,
    EPixelFormat_B8G8R8,
    EPixelFormat_Unknown
};
//---------------------------------------------------------------------------------------

enum EImageOriginType
{
    EImageOriginType_BottomLeft,
    EImageOriginType_TopLeft
};
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CImage
{

public:

    CImage(const AUInt32 width, const AUInt32 height, const AUInt32 depth, 
           const AUByte* imageData, EPixelFormat pixelFormat, 
           EImageOriginType originType);

    CImage(const AUInt32 width, const AUInt32 height, const AUInt32 depth, 
           const CColor& color, EPixelFormat pixelFormat, 
           EImageOriginType originType);

    CImage(const AUInt32 width, const AUInt32 height, const AUInt32 depth, 
           EPixelFormat pixelFormat, EImageOriginType originType);

    CImage(const AUInt32 width, const AUInt32 height, EPixelFormat pixelFormat, EImageOriginType originType);

    CImage(const std::string& fileName);

    CImage(const CImage& image);

    virtual ~CImage();

    virtual AUInt32 Width() const;

    virtual AUInt32 Depth() const;

    virtual AUInt32 Height() const;

    virtual const AUByte* Data() const;

    virtual AUByte* Data();

    EPixelFormat PixelFormat() const;

    EImageOriginType OriginType() const;

    virtual bool IsDirty() const;

    virtual void SetDirty(bool bDirty);

protected:

    void Allocate();

    void SetColor(const CColor& color);

    void Copy(const AUByte* data);

protected:

    AUInt32             m_width;
    AUInt32             m_height;
    AUInt32             m_depth;
    AUByte*             m_pData;
    EPixelFormat        m_pixelformat;
    bool                m_bIsDirty;
    EImageOriginType    m_originType;
    AUInt32             m_byteCount;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_IMAGE_H__
//---------------------------------------------------------------------------------------
