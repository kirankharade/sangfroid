//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "ITexture.h"
#include "../../Base/Namespaces.h"
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

ITexture::ITexture(const AUInt32 width, const AUInt32 height, const AUInt32 depth, const AUByte* imageData, AUInt16 bitsPerPixel)
    : m_width(width)
    , m_height(height)
    , m_depth(depth)
    , m_pData(nullptr)
    , m_bIsDirty(true)
    , m_bitsPerPixel(bitsPerPixel)
    , m_textureId(-1)
    , m_openGLId(-1)
    , m_byteCount(0)
{
    Allocate();
    Copy(imageData);
}
//---------------------------------------------------------------------------------------

ITexture::ITexture()
    : m_width(0)
    , m_height(0)
    , m_depth(1)
    , m_pData(nullptr)
    , m_bIsDirty(true)
    , m_bitsPerPixel(0)
    , m_textureId(-1)
    , m_openGLId(-1)
    , m_byteCount(0)
{
}
//---------------------------------------------------------------------------------------

ITexture::ITexture(const ITexture& texture)
    : m_width(texture.m_width)
    , m_height(texture.m_height)
    , m_depth(texture.m_depth)
    , m_pData(nullptr)
    , m_bIsDirty(true)
    , m_bitsPerPixel(texture.m_bitsPerPixel)
    , m_textureId(-1)
    , m_openGLId(-1)
    , m_byteCount(texture.m_byteCount)
{
    Allocate();
    Copy(texture.Data());
}
//---------------------------------------------------------------------------------------

ITexture::~ITexture()
{
    SAFE_CLEANUP(m_pData);
}
//---------------------------------------------------------------------------------------

AUInt32 ITexture::Width() const
{
    return m_width;
}
//---------------------------------------------------------------------------------------

AUInt32 ITexture::Depth() const
{
    return m_depth;
}
//---------------------------------------------------------------------------------------

AUInt32 ITexture::Height() const
{
    return m_height;
}
//---------------------------------------------------------------------------------------

const AUByte* ITexture::Data() const
{
    return m_pData;
}
//---------------------------------------------------------------------------------------

AUByte* ITexture::Data()
{
    return m_pData;
}
//---------------------------------------------------------------------------------------

bool ITexture::IsDirty()
{
    return m_bIsDirty;
}
//---------------------------------------------------------------------------------------

void ITexture::SetDirty(bool bDirty)
{
    m_bIsDirty = bDirty;
}
//---------------------------------------------------------------------------------------

const AInt32 ITexture::Id() const
{
    return m_textureId;
}
//---------------------------------------------------------------------------------------

const AInt32 ITexture::OpenGLId() const
{
    return m_openGLId;
}
//---------------------------------------------------------------------------------------

void ITexture::SetId(const AUInt32 id)
{
    m_textureId = (AUInt32) id;
}
//---------------------------------------------------------------------------------------

void ITexture::SetOpenGLId(const AUInt32 openGLId)
{
    m_openGLId = (AInt32) openGLId;
}
//---------------------------------------------------------------------------------------

void ITexture::Allocate()
{
    unsigned int numPixels = m_width * m_height * m_depth;
    if(numPixels == 0)
    {
        return;
    }
    unsigned int numBytesPerPixel = m_bitsPerPixel / 8;
    m_byteCount = numBytesPerPixel * numPixels;

    SAFE_CLEANUP(m_pData);

    m_pData = new AUByte[m_byteCount];
}
//---------------------------------------------------------------------------------------

void ITexture::SetColor(const CColor& color)
{
    if(nullptr == m_pData)
    {
        return;
    }

    AUByte r = color.RedAsByte();
    AUByte g = color.GreenAsByte();
    AUByte b = color.BlueAsByte();
    AUByte a = color.AlphaAsByte();

    unsigned int numPixels = m_width * m_height * m_depth;

    if(m_bitsPerPixel == 32)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 4 * i;
            m_pData[offset + 0] = r;
            m_pData[offset + 1] = g;
            m_pData[offset + 2] = b;
            m_pData[offset + 3] = a;
        }
    }
    if(m_bitsPerPixel == 24)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 3 * i;
            m_pData[offset + 0] = r;
            m_pData[offset + 1] = g;
            m_pData[offset + 2] = b;
        }
    }
    else
    {
        //KK: Can also use memset...
        for(unsigned int i = 0; i < m_byteCount; i++)
        {
            m_pData[i] = 0;
        }
    }
}
//---------------------------------------------------------------------------------------

void ITexture::Copy(const AUByte* data)
{
    if(nullptr == data)
    {
        return;
    }
    try
    {
        memcpy(m_pData, data, m_byteCount);
    }
    catch(...)
    {
        //Throw some error
    }
}
//---------------------------------------------------------------------------------------

