//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "C2DTexture.h"
#include "CImage.h"
#include "../../Base/Namespaces.h"
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

C2DTexture::C2DTexture(const AUInt32 width, const AUInt32 height, 
                       const AUByte* imageData, AUInt16 bitsPerPixel)
    :ITexture(width, height, 1, imageData, bitsPerPixel)
{
}
//---------------------------------------------------------------------------------------

C2DTexture::C2DTexture(const CImage* image)
    :ITexture()
{
    CopyImage(image);
}
//---------------------------------------------------------------------------------------

C2DTexture::C2DTexture(const std::string& imageFilename)
    :ITexture()
{
    CImage* image = new CImage(imageFilename);
    CopyImage(image);
}
//---------------------------------------------------------------------------------------

C2DTexture::C2DTexture(const AUInt32 width, const AUInt32 height, const CColor& color)
    :ITexture(width, height, 1, nullptr, 32)
{
    Allocate();
    SetColor(color);
}
//---------------------------------------------------------------------------------------

C2DTexture::C2DTexture(const AUInt32 width, const AUInt32 height, 
                       const CColor& color, AUInt16 bitsPerPixel)
    :ITexture(width, height, 1, nullptr, bitsPerPixel)
{
    Allocate();
    SetColor(color);
}
//---------------------------------------------------------------------------------------

C2DTexture::C2DTexture(const C2DTexture& texture)
    :ITexture(texture)
{
}
//---------------------------------------------------------------------------------------

C2DTexture::~C2DTexture()
{
}
//---------------------------------------------------------------------------------------

ETextureType C2DTexture::Type()
{
    return ETexture_2D;
}
//---------------------------------------------------------------------------------------

void C2DTexture::CopyImage(const CImage* image)
{
    if(nullptr == image)
    {
        throw "Invalid image object.";
    }
    const AUByte* data = image->Data();
    if(nullptr == data)
    {
        throw "Invalid image data.";
    }

    m_width = image->Width();
    m_height = image->Height();
    m_depth = image->Depth();

    EPixelFormat pixelformat = image->PixelFormat();
    EImageOriginType originType = image->OriginType();

    unsigned int numPixels = m_width * m_height * m_depth;

    if(EPixelFormat_B8G8R8 == pixelformat || EPixelFormat_R8G8B8 == pixelformat)
    {
        m_bitsPerPixel = 24;
    }
    else
    {
        m_bitsPerPixel = 32;
    }

    Allocate();

    if(pixelformat == EPixelFormat_R8G8B8A8)
    {
        Copy(data);
    }
    else if(pixelformat == EPixelFormat_R8G8B8)
    {
        Copy(data);
    }
    else if(pixelformat == EPixelFormat_B8G8R8A8)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 4 * i;
            m_pData[offset + 0] = data[offset + 2];
            m_pData[offset + 1] = data[offset + 1];
            m_pData[offset + 2] = data[offset + 0];
            m_pData[offset + 3] = data[offset + 3];
        }
    }
    else if(pixelformat == EPixelFormat_B8G8R8)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 3 * i;
            m_pData[offset + 0] = data[offset + 2];
            m_pData[offset + 1] = data[offset + 1];
            m_pData[offset + 2] = data[offset + 0];
        }
    }
    else
    {
        //Treat as EPixelFormat_R8G8B8A8
        Copy(data);
    }

    if(originType == EImageOriginType_TopLeft)
    {
        //If not in openGL format, convert to OpenGL format
        //TO DO:
    }
}
//---------------------------------------------------------------------------------------
