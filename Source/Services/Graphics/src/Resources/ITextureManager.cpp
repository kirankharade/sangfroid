//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../IGraphics.h"
#include "ITexture.h"
#include "ITextureManager.h"
#include "../../Base/Namespaces.h"
#include <map>
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------
typedef std::map<AUInt32 /*openGlId*/, ITexture*>   TextureMap;
typedef TextureMap::iterator   TextureMapIterator;
typedef TextureMap::const_iterator   TextureMapConstIterator;

//---------------------------------------------------------------------------------------

ITextureManager::ITextureManager(IGraphics* graphics)
    :m_pGraphics(graphics)
{
    m_pTextureCollection = new TextureMap();
}
//---------------------------------------------------------------------------------------

ITextureManager::~ITextureManager()
{
    //DeleteAllTextures(); Let derived classes call it from their destructors first
    ((TextureMap*)m_pTextureCollection)->clear();
    SAFE_CLEANUP(m_pTextureCollection);
}
//---------------------------------------------------------------------------------------

IGraphics* ITextureManager::Graphics() const
{
    return m_pGraphics;
}
//---------------------------------------------------------------------------------------

bool ITextureManager::Add(ITexture* texture)
{
    //Derived classes will implement this and actually add the texture and then call this method
    AInt32 id = texture->OpenGLId();
    if(id != -1 && !Exists(texture))
    {
        ((TextureMap*) m_pTextureCollection)->insert(std::make_pair(id, texture));
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool ITextureManager::Exists(const AUInt32 openGLId) const
{
    TextureMap* map = (TextureMap*) m_pTextureCollection;
    TextureMapConstIterator itr = map->find(openGLId);
    if(itr != map->end())
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool ITextureManager::Exists(ITexture* texture) const
{
    TextureMap* map = (TextureMap*) m_pTextureCollection;
    for(TextureMapConstIterator itr = map->begin(); itr != map->end(); itr++)
    {
        if(texture == itr->second)
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

void ITextureManager::Remove(ITexture* texture, const bool deleteData)
{
    TextureMap* map = (TextureMap*) m_pTextureCollection;
    for(TextureMapIterator itr = map->begin(); itr != map->end(); itr++)
    {
        if(texture == itr->second)
        {
            RemoveFromGraphics(itr->first);
            map->erase(itr);
            if(deleteData)
            {
                delete texture;
                texture = nullptr;
            }
            return;
        }
    }
}
//---------------------------------------------------------------------------------------

void ITextureManager::Remove(const AUInt32 openGLId, const bool deleteData)
{
    TextureMap* map = (TextureMap*) m_pTextureCollection;
    TextureMapIterator itr = map->find(openGLId);
    if(itr != map->end())
    {
        RemoveFromGraphics(itr->first);
        ITexture* texture = itr->second;
        map->erase(itr);
        if(deleteData)
        {
            delete texture;
            texture = nullptr;
        }
        return;
    }
}
//---------------------------------------------------------------------------------------

AUInt32 ITextureManager::Count() const
{
    TextureMap* map = (TextureMap*) m_pTextureCollection;
    return map->size();
}
//---------------------------------------------------------------------------------------

void ITextureManager::DeleteAllTextures()
{
    TextureMap* map = (TextureMap*) m_pTextureCollection;
    for(TextureMapIterator itr = map->begin(); itr != map->end(); itr++)
    {
        //first remove the graphics definition
        RemoveFromGraphics(itr->first);

        ITexture* texture = itr->second;

        //remove from map
        map->erase(itr);

        //remove the texture object
        delete texture;
    }
}
//---------------------------------------------------------------------------------------

void ITextureManager::SetOpenGLId(ITexture* texture, AUInt32 oglId)
{
    if(texture)
    {
        texture->SetOpenGLId(oglId);
    }
}
//---------------------------------------------------------------------------------------

AUInt32 ITextureManager::GetOpenGLId(ITexture* texture)
{
    return texture->OpenGLId();
}
//---------------------------------------------------------------------------------------

