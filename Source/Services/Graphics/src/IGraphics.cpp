//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "IGraphics.h"
#include "IFactory.h"
#include "Renderers/CFixedFunctionGraphics.h"

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_
#include "WindowInterfaces/Win32/CWin32Factory.h"
#endif

//---------------------------------------------------------------------------------------
using namespace An;
using namespace An::Graphics;
//---------------------------------------------------------------------------------------
static IFactory* s_factoryInstance = nullptr;
IGraphics* IGraphics::m_spGraphics = nullptr;
EWindowingSystemType IGraphics::m_sWindowingSystem = EWindowingSystemType_Win32;
EOpenGLEngineType IGraphics::m_sEngineType = EOpenGLEngineType_FixedFunction;
bool IGraphics::m_sInitialized = false;
//---------------------------------------------------------------------------------------

void IGraphics::InitializeGraphics(const EWindowingSystemType windowingSystemType, const EOpenGLEngineType engineType)
{
    if(!m_sInitialized)
    {
        m_sWindowingSystem = windowingSystemType;
        m_sEngineType = engineType;

        GenerateFactory(nullptr, windowingSystemType);
        GenerateGraphics(engineType);
    }
    else
    {
        throw std::exception("Graphics already initialized...");
    }
}
//---------------------------------------------------------------------------------------

void IGraphics::InitializeGraphics(WindowID windowID, 
                                   const EWindowingSystemType windowingSystemType, 
                                   const EOpenGLEngineType engineType)
{
    if(!m_sInitialized)
    {
        m_sWindowingSystem = windowingSystemType;
        m_sEngineType = engineType;

        GenerateFactory(windowID, windowingSystemType);
        GenerateGraphics(engineType);
    }
    else
    {
        throw std::exception("Graphics already initialized...");
    }
}
//---------------------------------------------------------------------------------------

void IGraphics::InitializeGraphics(WindowID windowID, SGLContextParams& contextParams, 
                                   const EWindowingSystemType windowingSystemType, 
                                   const EOpenGLEngineType engineType)
{
    if(!m_sInitialized)
    {
        m_sWindowingSystem = windowingSystemType;
        m_sEngineType = engineType;

        GenerateFactory(windowID, contextParams, windowingSystemType);
        GenerateGraphics(engineType);
    }
    else
    {
        throw std::exception("Graphics already initialized...");
    }
}
//---------------------------------------------------------------------------------------

void IGraphics::GenerateFactory(WindowID windowID, const EWindowingSystemType windowingSystemType)
{
    //Create the instance of the factory based on the initialization parameters ...
    switch(windowingSystemType)
    {
        case EWindowingSystemType_Win32:
            {
                s_factoryInstance = new CWin32Factory();
                if(nullptr != windowID)
                {
                    ((CWin32Factory*) s_factoryInstance)->Initialize(windowID);
                }
                break;
            }
        case EWindowingSystemType_XWindowsLinux:
            //s_factoryInstance = new CWin32Factory(windowingSystemType, engineType);
            break;
        case EWindowingSystemType_MacOS:
            break;
        case EWindowingSystemType_Android:
            break;
        case EWindowingSystemType_iOS:
            break;
        default:
            break;
    }

}
//---------------------------------------------------------------------------------------

void IGraphics::GenerateFactory(WindowID windowID, SGLContextParams& contextParams, 
                                const EWindowingSystemType windowingSystemType)
{
    //Create the instance of the factory based on the initialization parameters ...
    switch(windowingSystemType)
    {
        case EWindowingSystemType_Win32:
            {
                s_factoryInstance = new CWin32Factory();
                if(nullptr != windowID)
                {
                    ((CWin32Factory*) s_factoryInstance)->Initialize(windowID, contextParams);
                }
                break;
            }
        case EWindowingSystemType_XWindowsLinux:
            //s_factoryInstance = new CWin32Factory(windowingSystemType, engineType);
            break;
        case EWindowingSystemType_MacOS:
            break;
        case EWindowingSystemType_Android:
            break;
        case EWindowingSystemType_iOS:
            break;
        default:
            break;
    }

}
//---------------------------------------------------------------------------------------

void IGraphics::GenerateGraphics(const EOpenGLEngineType engineType)
{
    //Create the instance of the factory based on the initialization parameters ...

    if(m_spGraphics == nullptr)
    {
        switch(engineType)
        {
        case EOpenGLEngineType_FixedFunction:
            {
                m_spGraphics = new CFixedFunctionGraphics();
                break;
            }
        case EOpenGLEngineType_Core_3:
            {
                m_spGraphics = nullptr;
                break;
            }
        case EOpenGLEngineType_Core_4:
            {
                m_spGraphics = nullptr;
                break;
            }
        case EOpenGLEngineType_ES:
            {
                m_spGraphics = nullptr;
                break;
            }
        default:
            {
                m_spGraphics = new CFixedFunctionGraphics();
                break;
            }
        }
    }
}
//---------------------------------------------------------------------------------------

IFactory* IGraphics::Factory()
{
    return s_factoryInstance;
}
//---------------------------------------------------------------------------------------

IGraphics* IGraphics::Gfx()
{
    return m_spGraphics;
}
//---------------------------------------------------------------------------------------
