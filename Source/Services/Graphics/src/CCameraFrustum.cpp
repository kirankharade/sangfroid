//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "CCameraFrustum.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CCameraFrustum::CCameraFrustum(const CCameraFrustum& other)
{
	m_cameraPosition=other.m_cameraPosition;
	for(int i = 0; i < 6; ++i)
    {
		m_planes[i] = other.m_planes[i];
    }
	m_boundingBox=other.m_boundingBox;
    m_viewMatrix = other.m_viewMatrix;
    m_projMatrix = other.m_projMatrix;
}
//---------------------------------------------------------------------------------------

//CCameraFrustum::CCameraFrustum(const Maths::CMatrix4f& mat)
//{
//	SetFrom ( mat );
//}
////---------------------------------------------------------------------------------------

Maths::CPoint3f CCameraFrustum::GetFarLeftUp() const
{
	Maths::CPoint3f p;
	m_planes[CCameraFrustum::EFrustumPlane_Far].Intersection(m_planes[CCameraFrustum::EFrustumPlane_Top], m_planes[CCameraFrustum::EFrustumPlane_Left], p);
	return p;
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f CCameraFrustum::GetFarLeftDown() const
{
	Maths::CPoint3f p;
	m_planes[CCameraFrustum::EFrustumPlane_Far].Intersection(m_planes[CCameraFrustum::EFrustumPlane_Bottom], m_planes[CCameraFrustum::EFrustumPlane_Left], p);
	return p;
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f CCameraFrustum::GetFarRightUp() const
{
	Maths::CPoint3f p;
	m_planes[CCameraFrustum::EFrustumPlane_Far].Intersection(m_planes[CCameraFrustum::EFrustumPlane_Top], m_planes[CCameraFrustum::EFrustumPlane_Right], p);
	return p;
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f CCameraFrustum::GetFarRightDown() const
{
	Maths::CPoint3f p;
	m_planes[CCameraFrustum::EFrustumPlane_Far].Intersection(m_planes[CCameraFrustum::EFrustumPlane_Bottom], m_planes[CCameraFrustum::EFrustumPlane_Right], p);
	return p;
}
//---------------------------------------------------------------------------------------

const Maths::CBoundingBox CCameraFrustum::GetBoundingBox() const
{
	return m_boundingBox;
}
//---------------------------------------------------------------------------------------

void CCameraFrustum::RecalculateBoundingBox()
{
    m_boundingBox = CBoundingBox();
	m_boundingBox.Add(m_cameraPosition);
	m_boundingBox.Add(GetFarLeftUp());
	m_boundingBox.Add(GetFarRightUp());
	m_boundingBox.Add(GetFarLeftDown());
	m_boundingBox.Add(GetFarRightDown());
}
//---------------------------------------------------------------------------------------

//void CCameraFrustum::SetFrom(const Maths::CMatrix4f& mat)
//{
//	m_planes[EFrustumPlane_Left].getNormal().x = mat[3 ] + mat[0];
//	m_planes[EFrustumPlane_Left].m_normal.y = mat[7 ] + mat[4];
//	m_planes[EFrustumPlane_Left].m_normal.z = mat[11] + mat[8];
//	m_planes[EFrustumPlane_Left].D =        mat[15] + mat[12];
//
//	m_planes[EFrustumPlane_Right].m_normal.x = mat[3 ] - mat[0];
//	m_planes[EFrustumPlane_Right].m_normal.y = mat[7 ] - mat[4];
//	m_planes[EFrustumPlane_Right].m_normal.z = mat[11] - mat[8];
//	m_planes[EFrustumPlane_Right].D =        mat[15] - mat[12];
//
//	m_planes[EFrustumPlane_Top].m_normal.x = mat[3 ] - mat[1];
//	m_planes[EFrustumPlane_Top].m_normal.y = mat[7 ] - mat[5];
//	m_planes[EFrustumPlane_Top].m_normal.z = mat[11] - mat[9];
//	m_planes[EFrustumPlane_Top].D =        mat[15] - mat[13];
//
//	m_planes[EFrustumPlane_Bottom].m_normal.x = mat[3 ] + mat[1];
//	m_planes[EFrustumPlane_Bottom].m_normal.y = mat[7 ] + mat[5];
//	m_planes[EFrustumPlane_Bottom].m_normal.z = mat[11] + mat[9];
//	m_planes[EFrustumPlane_Bottom].D =        mat[15] + mat[13];
//
//	m_planes[EFrustumPlane_Far].m_normal.x = mat[3 ] - mat[2];
//	m_planes[EFrustumPlane_Far].m_normal.y = mat[7 ] - mat[6];
//	m_planes[EFrustumPlane_Far].m_normal.z = mat[11] - mat[10];
//	m_planes[EFrustumPlane_Far].D =        mat[15] - mat[14];
//
//	m_planes[EFrustumPlane_Near].m_normal.x = mat[2];
//	m_planes[EFrustumPlane_Near].m_normal.y = mat[6];
//	m_planes[EFrustumPlane_Near].m_normal.z = mat[10];
//	m_planes[EFrustumPlane_Near].D =        mat[14];
//
//	// normalize normals
//	for (int i=0; i != 6; ++i)
//	{
//		float fVal = m_planes[i].m_normal.SquareLength();		
//		const float len = -Maths::Sqrt(1.f/fVal);
//		m_planes[i].m_normal *= len;
//		m_planes[i].D *= len;
//	}
//
//	RecalculateBoundingBox();
//}
//---------------------------------------------------------------------------------------

Maths::CMatrix4f& CCameraFrustum::ViewMatrix()
{
    return m_viewMatrix;
}
//---------------------------------------------------------------------------------------

const Maths::CMatrix4f& CCameraFrustum::ViewMatrix() const
{
    return m_viewMatrix;
}
//---------------------------------------------------------------------------------------

Maths::CMatrix4f& CCameraFrustum::ProjectionMatrix()
{
    return m_projMatrix;
}
//---------------------------------------------------------------------------------------

const Maths::CMatrix4f& CCameraFrustum::ProjectionMatrix() const
{
    return m_projMatrix;
}
//---------------------------------------------------------------------------------------
