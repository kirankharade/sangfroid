//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LAYER_SEQUENCE_H__
#define __C_LAYER_SEQUENCE_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include <string>

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class ILayer;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CLayerSequence
{

public:

    CLayerSequence ();
    virtual ~CLayerSequence();

    bool addAtIndex(ILayer* pLayer, const int index);
    bool addAtFront(ILayer* pLayer);
    bool addAtBack(ILayer* pLayer);

    bool remove(ILayer* pLayer);

    void setToFront(ILayer* pLayer);
    void setToBack(ILayer* pLayer);

    bool forward(ILayer* pLayer, const unsigned int& step);
    bool backward(ILayer* pLayer, const unsigned int& step);

    int index(ILayer* pLayer) const;
    bool find(ILayer* pLayer) const;

    unsigned int size() const;

    ILayer* operator [] (const unsigned int& i);
    ILayer* getByName(const std::string& name);

private:

    void*   m_pImpl;
};
//-----------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LAYER_SEQUENCE_H__
//---------------------------------------------------------------------------------------
