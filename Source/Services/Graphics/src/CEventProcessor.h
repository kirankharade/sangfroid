//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "IEventProcessor.h"
#include "SEvent.h"
//---------------------------------------------------------------------------------------
#ifndef __C_EVENT_PROCESSOR_H__
#define __C_EVENT_PROCESSOR_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class CViewport;

//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CEventProcessor : public IEventProcessor
{

public:
	CEventProcessor(CViewport* viewport);
	virtual ~CEventProcessor() {}

	virtual void OnEvent(const SEvent& ev);

    virtual void OnPaint();

    virtual void OnLeftButtonDoubleClick(const SEvent& ev);
    virtual void OnLeftButtonUp(const SEvent& ev);
    virtual void OnLeftButtonDown(const SEvent& ev);

    virtual void OnRightButtonDoubleClick(const SEvent& ev);
    virtual void OnRightButtonDown(const SEvent& ev);
    virtual void OnRightButtonUp(const SEvent& ev);

    virtual void OnMiddleButtonDoubleClick(const SEvent& ev);
    virtual void OnMiddleButtonDown(const SEvent& ev);
    virtual void OnMiddleButtonUp(const SEvent& ev);

    virtual void OnMouseMove(const SEvent& ev);
    virtual void OnMouseWheel(const SEvent& ev);
    virtual void OnMouseHover(const SEvent& ev);
    virtual void OnMouseLeave(const SEvent& ev);
    virtual void OnMouseEnter(const SEvent& ev);

    virtual void OnKey(const SEvent& ev);

    virtual void OnSize(const SEvent& ev);

protected:

    CViewport* m_pViewport;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_EVENT_PROCESSOR_H__
//---------------------------------------------------------------------------------------

