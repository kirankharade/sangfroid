//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "CLayerSequence.h"
#include "Containers/COrderedItemSequence.h"
#include "ILayer.h"
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;

typedef COrderedItemSequence<ILayer*>  CLayerSequenceImpl;
//---------------------------------------------------------------------------------------

CLayerSequence::CLayerSequence()
    :m_pImpl(nullptr)
{
    m_pImpl = new CLayerSequenceImpl();
}
//---------------------------------------------------------------------------------------

CLayerSequence::~CLayerSequence()
{
    SAFE_CLEANUP(m_pImpl);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::addAtIndex(ILayer* pLayer, const int index)
{
    return ((CLayerSequenceImpl*) m_pImpl)->addAtIndex(pLayer, index);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::addAtFront(ILayer* pLayer)
{
    return ((CLayerSequenceImpl*) m_pImpl)->addAtFront(pLayer);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::addAtBack(ILayer* pLayer)
{
    return ((CLayerSequenceImpl*) m_pImpl)->addAtBack(pLayer);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::remove(ILayer* pLayer)
{
    return ((CLayerSequenceImpl*) m_pImpl)->remove(pLayer);
}
//---------------------------------------------------------------------------------------

void CLayerSequence::setToFront(ILayer* pLayer)
{
    ((CLayerSequenceImpl*) m_pImpl)->setToFront(pLayer);
}
//---------------------------------------------------------------------------------------

void CLayerSequence::setToBack(ILayer* pLayer)
{
    ((CLayerSequenceImpl*) m_pImpl)->setToBack(pLayer);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::forward(ILayer* pLayer, const unsigned int& step)
{
    return ((CLayerSequenceImpl*) m_pImpl)->forward(pLayer, step);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::backward(ILayer* pLayer, const unsigned int& step)
{
    return ((CLayerSequenceImpl*) m_pImpl)->backward(pLayer, step);
}
//---------------------------------------------------------------------------------------

int CLayerSequence::index(ILayer* pLayer) const
{
    return ((CLayerSequenceImpl*) m_pImpl)->index(pLayer);
}
//---------------------------------------------------------------------------------------

bool CLayerSequence::find(ILayer* pLayer) const
{
    return ((CLayerSequenceImpl*) m_pImpl)->find(pLayer);
}
//---------------------------------------------------------------------------------------

unsigned int CLayerSequence::size() const
{
    return ((CLayerSequenceImpl*) m_pImpl)->size();
}
//---------------------------------------------------------------------------------------

ILayer* CLayerSequence::operator [] (const unsigned int& i)
{
    return ((CLayerSequenceImpl*) m_pImpl)->operator[](i);
}
//---------------------------------------------------------------------------------------

ILayer* CLayerSequence::getByName(const std::string& name)
{
    int count = ((CLayerSequenceImpl*) m_pImpl)->size();
    for(int i = 0; i < count; i++)
    {
        ILayer* pLayer = ((CLayerSequenceImpl*) m_pImpl)->operator[](i);
        if(pLayer && pLayer->Name() == name)
        {
            return pLayer;
        }
    }
    return nullptr;
}
//---------------------------------------------------------------------------------------

