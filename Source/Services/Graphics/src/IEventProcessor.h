//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_EVENT_PROCESSOR_H__
#define __I_EVENT_PROCESSOR_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "ICanvas.h"
#include "IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IEventProcessor
{

public:
    virtual ~IEventProcessor() {}

    virtual void OnEvent(const SEvent& ev) = 0;


	virtual void OnPaint() = 0;

    virtual void OnLeftButtonDoubleClick(const SEvent& ev) = 0;
	virtual void OnLeftButtonUp(const SEvent& ev) = 0;
	virtual void OnLeftButtonDown(const SEvent& ev) = 0;

	virtual void OnRightButtonDoubleClick(const SEvent& ev) = 0;
	virtual void OnRightButtonDown(const SEvent& ev) = 0;
	virtual void OnRightButtonUp(const SEvent& ev) = 0;

	virtual void OnMiddleButtonDoubleClick(const SEvent& ev) = 0;
	virtual void OnMiddleButtonDown(const SEvent& ev) = 0;
	virtual void OnMiddleButtonUp(const SEvent& ev) = 0;

	virtual void OnMouseMove(const SEvent& ev) = 0;
	virtual void OnMouseWheel(const SEvent& ev) = 0;
	virtual void OnMouseHover(const SEvent& ev) = 0;
	virtual void OnMouseLeave(const SEvent& ev) = 0;
	virtual void OnMouseEnter(const SEvent& ev) = 0;

	virtual void OnKey(const SEvent& ev) = 0;

	virtual void OnSize(const SEvent& ev) = 0;

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_EVENT_PROCESSOR_H__
//---------------------------------------------------------------------------------------

