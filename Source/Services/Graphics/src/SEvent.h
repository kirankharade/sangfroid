//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
//---------------------------------------------------------------------------------------
#ifndef __S_EVENT_H__
#define __S_EVENT_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

enum AN_GRAPHICS_API EEventType
{
	EEventType_Mouse,
	EEventType_KeyInput,
	EEventType_Paint,
	EEventType_User,
	EEventType_Resize,
	EEventType_MouseMovementOnSelection,
	EEventType_Selection,
	EEventType_Log,
};
//---------------------------------------------------------------------------------------

enum AN_GRAPHICS_API EMouseEventType
{
    EMouseEvent_LeftButtonDown = 0,
    EMouseEvent_RightButtonDown,
    EMouseEvent_MiddleButtonDown,
    EMouseEvent_LeftButtonUp,
    EMouseEvent_RightButtonUp,
    EMouseEvent_MiddleButtonUp,
    EMouseEvent_MouseMove,
    EMouseEvent_MouseWheel,
    EMouseEvent_LeftButtonDoubleClick,
    EMouseEvent_RightButtonDoubleClick,
    EMouseEvent_MiddleButtonDoubleClick,
    EMouseEvent_Hover,
    EMouseEvent_Enter,
    EMouseEvent_Leave
};
//---------------------------------------------------------------------------------------

enum AN_GRAPHICS_API EMouseButtonStateType
{
	EMouseState_Left = 0x01,
	EMouseState_Right = 0x02,
	EMouseState_Middle = 0x04,
	EMouseState_Force32Bit = 0x7fffffff
};
//---------------------------------------------------------------------------------------

enum AN_GRAPHICS_API EKeyCodeType
{
	EKeyCodeType_LBUTTON          = 0x01,  // Left mouse button
	EKeyCodeType_RBUTTON          = 0x02,  // Right mouse button
	EKeyCodeType_CANCEL           = 0x03,  // Control-break processing
	EKeyCodeType_MBUTTON          = 0x04,  // Middle mouse button (three-button mouse)
	EKeyCodeType_XBUTTON1         = 0x05,  // Windows 2000/XP: X1 mouse button
	EKeyCodeType_XBUTTON2         = 0x06,  // Windows 2000/XP: X2 mouse button
	EKeyCodeType_BACK             = 0x08,  // BACKSPACE 
	EKeyCodeType_TAB              = 0x09,  // TAB 
	EKeyCodeType_CLEAR            = 0x0C,  // CLEAR 
	EKeyCodeType_RETURN           = 0x0D,  // ENTER 
	EKeyCodeType_SHIFT            = 0x10,  // SHIFT 
	EKeyCodeType_CONTROL          = 0x11,  // CTRL 
	EKeyCodeType_MENU             = 0x12,  // ALT 
	EKeyCodeType_PAUSE            = 0x13,  // PAUSE 
	EKeyCodeType_CAPITAL          = 0x14,  // CAPS LOCK 
	EKeyCodeType_KANA             = 0x15,  // IME Kana mode
	EKeyCodeType_HANGUEL          = 0x15,  // IME Hanguel mode (maintained for compatibility use KEY_HANGUL)
	EKeyCodeType_HANGUL           = 0x15,  // IME Hangul mode
	EKeyCodeType_JUNJA            = 0x17,  // IME Junja mode
	EKeyCodeType_FINAL            = 0x18,  // IME final mode
	EKeyCodeType_HANJA            = 0x19,  // IME Hanja mode
	EKeyCodeType_KANJI            = 0x19,  // IME Kanji mode
	EKeyCodeType_ESCAPE           = 0x1B,  // ESC 
	EKeyCodeType_CONVERT          = 0x1C,  // IME convert
	EKeyCodeType_NONCONVERT       = 0x1D,  // IME nonconvert
	EKeyCodeType_ACCEPT           = 0x1E,  // IME accept
	EKeyCodeType_MODECHANGE       = 0x1F,  // IME mode change request
	EKeyCodeType_SPACE            = 0x20,  // SPACEBAR
	EKeyCodeType_PRIOR            = 0x21,  // PAGE UP 
	EKeyCodeType_NEXT             = 0x22,  // PAGE DOWN 
	EKeyCodeType_END              = 0x23,  // END 
	EKeyCodeType_HOME             = 0x24,  // HOME 
	EKeyCodeType_LEFT             = 0x25,  // LEFT ARROW 
	EKeyCodeType_UP               = 0x26,  // UP ARROW 
	EKeyCodeType_RIGHT            = 0x27,  // RIGHT ARROW 
	EKeyCodeType_DOWN             = 0x28,  // DOWN ARROW 
	EKeyCodeType_SELECT           = 0x29,  // SELECT 
	EKeyCodeType_PRINT            = 0x2A,  // PRINT 
	EKeyCodeType_EXECUT           = 0x2B,  // EXECUTE 
	EKeyCodeType_SNAPSHOT         = 0x2C,  // PRINT SCREEN 
	EKeyCodeType_INSERT           = 0x2D,  // INS 
	EKeyCodeType_DELETE           = 0x2E,  // DEL 
	EKeyCodeType_HELP             = 0x2F,  // HELP 
	EKeyCodeType_KEY_0            = 0x30,  // 0 
	EKeyCodeType_KEY_1            = 0x31,  // 1 
	EKeyCodeType_KEY_2            = 0x32,  // 2 
	EKeyCodeType_KEY_3            = 0x33,  // 3 
	EKeyCodeType_KEY_4            = 0x34,  // 4 
	EKeyCodeType_KEY_5            = 0x35,  // 5 
	EKeyCodeType_KEY_6            = 0x36,  // 6 
	EKeyCodeType_KEY_7            = 0x37,  // 7 
	EKeyCodeType_KEY_8            = 0x38,  // 8 
	EKeyCodeType_KEY_9            = 0x39,  // 9 
	EKeyCodeType_KEY_A            = 0x41,  // A 
	EKeyCodeType_KEY_B            = 0x42,  // B 
	EKeyCodeType_KEY_C            = 0x43,  // C 
	EKeyCodeType_KEY_D            = 0x44,  // D 
	EKeyCodeType_KEY_E            = 0x45,  // E 
	EKeyCodeType_KEY_F            = 0x46,  // F 
	EKeyCodeType_KEY_G            = 0x47,  // G 
	EKeyCodeType_KEY_H            = 0x48,  // H 
	EKeyCodeType_KEY_I            = 0x49,  // I 
	EKeyCodeType_KEY_J            = 0x4A,  // J 
	EKeyCodeType_KEY_K            = 0x4B,  // K 
	EKeyCodeType_KEY_L            = 0x4C,  // L 
	EKeyCodeType_KEY_M            = 0x4D,  // M 
	EKeyCodeType_KEY_N            = 0x4E,  // N 
	EKeyCodeType_KEY_O            = 0x4F,  // O 
	EKeyCodeType_KEY_P            = 0x50,  // P 
	EKeyCodeType_KEY_Q            = 0x51,  // Q 
	EKeyCodeType_KEY_R            = 0x52,  // R 
	EKeyCodeType_KEY_S            = 0x53,  // S 
	EKeyCodeType_KEY_T            = 0x54,  // T 
	EKeyCodeType_KEY_U            = 0x55,  // U 
	EKeyCodeType_KEY_V            = 0x56,  // V 
	EKeyCodeType_KEY_W            = 0x57,  // W 
	EKeyCodeType_KEY_X            = 0x58,  // X 
	EKeyCodeType_KEY_Y            = 0x59,  // Y 
	EKeyCodeType_KEY_Z            = 0x5A,  // Z 
	EKeyCodeType_LWIN             = 0x5B,  // Left Windows key (Microsoft� Natural� keyboard)
	EKeyCodeType_RWIN             = 0x5C,  // Right Windows key (Natural keyboard)
	EKeyCodeType_APPS             = 0x5D,  // Applications key (Natural keyboard)
	EKeyCodeType_SLEEP            = 0x5F,  // Computer Sleep 
	EKeyCodeType_NUMPAD0          = 0x60,  // Numeric keypad 0 
	EKeyCodeType_NUMPAD1          = 0x61,  // Numeric keypad 1 
	EKeyCodeType_NUMPAD2          = 0x62,  // Numeric keypad 2 
	EKeyCodeType_NUMPAD3          = 0x63,  // Numeric keypad 3 
	EKeyCodeType_NUMPAD4          = 0x64,  // Numeric keypad 4 
	EKeyCodeType_NUMPAD5          = 0x65,  // Numeric keypad 5 
	EKeyCodeType_NUMPAD6          = 0x66,  // Numeric keypad 6 
	EKeyCodeType_NUMPAD7          = 0x67,  // Numeric keypad 7 
	EKeyCodeType_NUMPAD8          = 0x68,  // Numeric keypad 8 
	EKeyCodeType_NUMPAD9          = 0x69,  // Numeric keypad 9 
	EKeyCodeType_MULTIPLY         = 0x6A,  // Multiply 
	EKeyCodeType_ADD              = 0x6B,  // Add 
	EKeyCodeType_SEPARATOR        = 0x6C,  // Separator 
	EKeyCodeType_SUBTRACT         = 0x6D,  // Subtract 
	EKeyCodeType_DECIMAL          = 0x6E,  // Decimal 
	EKeyCodeType_DIVIDE           = 0x6F,  // Divide 
	EKeyCodeType_F1               = 0x70,  // F1 
	EKeyCodeType_F2               = 0x71,  // F2 
	EKeyCodeType_F3               = 0x72,  // F3 
	EKeyCodeType_F4               = 0x73,  // F4 
	EKeyCodeType_F5               = 0x74,  // F5 
	EKeyCodeType_F6               = 0x75,  // F6 
	EKeyCodeType_F7               = 0x76,  // F7 
	EKeyCodeType_F8               = 0x77,  // F8 
	EKeyCodeType_F9               = 0x78,  // F9 
	EKeyCodeType_F10              = 0x79,  // F10 
	EKeyCodeType_F11              = 0x7A,  // F11 
	EKeyCodeType_F12              = 0x7B,  // F12 
	EKeyCodeType_F13              = 0x7C,  // F13 
	EKeyCodeType_F14              = 0x7D,  // F14 
	EKeyCodeType_F15              = 0x7E,  // F15 
	EKeyCodeType_F16              = 0x7F,  // F16 
	EKeyCodeType_F17              = 0x80,  // F17 
	EKeyCodeType_F18              = 0x81,  // F18 
	EKeyCodeType_F19              = 0x82,  // F19 
	EKeyCodeType_F20              = 0x83,  // F20 
	EKeyCodeType_F21              = 0x84,  // F21 
	EKeyCodeType_F22              = 0x85,  // F22 
	EKeyCodeType_F23              = 0x86,  // F23 
	EKeyCodeType_F24              = 0x87,  // F24 
	EKeyCodeType_NUMLOCK          = 0x90,  // NUM LOCK 
	EKeyCodeType_SCROLL           = 0x91,  // SCROLL LOCK 
	EKeyCodeType_LSHIFT           = 0xA0,  // Left SHIFT 
	EKeyCodeType_RSHIFT           = 0xA1,  // Right SHIFT 
	EKeyCodeType_LCONTROL         = 0xA2,  // Left CONTROL 
	EKeyCodeType_RCONTROL         = 0xA3,  // Right CONTROL 
	EKeyCodeType_LMENU            = 0xA4,  // Left MENU 
	EKeyCodeType_RMENU            = 0xA5,  // Right MENU 
	EKeyCodeType_PLUS             = 0xBB,  // Plus Key   (+)
	EKeyCodeType_COMMA            = 0xBC,  // Comma Key  (,)
	EKeyCodeType_MINUS            = 0xBD,  // Minus Key  (-)
	EKeyCodeType_PERIOD           = 0xBE,  // Period Key (.)
	EKeyCodeType_ATTN             = 0xF6,  // Attn 
	EKeyCodeType_CRSEL            = 0xF7,  // CrSel 
	EKeyCodeType_EXSEL            = 0xF8,  // ExSel 
	EKeyCodeType_EREOF            = 0xF9,  // Erase EOF 
	EKeyCodeType_PLAY             = 0xFA,  // Play 
	EKeyCodeType_ZOOM             = 0xFB,  // Zoom 
	EKeyCodeType_PA1              = 0xFD,  // PA1 
	EKeyCodeType_OEM_CLEAR        = 0xFE,   // Clear 
};
//---------------------------------------------------------------------------------------

enum AN_GRAPHICS_API EResizeEventType
{
    EResizeEvent_Maximized = 0,
    EResizeEvent_Minimized,
    EResizeEvent_Restored,
    EResizeEvent_MaxHide,
    EResizeEvent_MaxShow
};
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SMouseEvent
{
	AInt32 x;
	AInt32 y;
	AReal32 wheelDelta; // Mouse wheel delta
	bool shift:1;
	bool control:1;
	AUInt32 state;
	EMouseEventType type;

	bool isLeftButtonPressed() const { return 0 != ( state & EMouseState_Left ); }
	bool isRightButtonPressed() const { return 0 != ( state & EMouseState_Right ); }
	bool isMiddleButtonPressed() const { return 0 != ( state & EMouseState_Middle ); }
};
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SKeyEvent
{
	EKeyCodeType key;
	bool pressedDown:1;
	bool shift:1;
	bool control:1;
	wchar_t keyboardChar;
};
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SResizeEvent
{
	int Width;
	int Height;
	EResizeEventType type;
};
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SEvent
{
	EEventType type;
	union
	{
		struct SMouseEvent MouseEvent;
		struct SKeyEvent KeyEvent;
        struct SResizeEvent ResizeEvent;
	};
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __S_EVENT_H__
//---------------------------------------------------------------------------------------

