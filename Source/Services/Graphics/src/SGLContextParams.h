//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __S_GL_CONTEXT_PARAMS_H__
#define __S_GL_CONTEXT_PARAMS_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "SGLContextParams.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SGLContextParams
{
    SGLContextParams()
        :  m_openGLEngineType(EOpenGLEngineType_FixedFunction),
        m_Bits(32),
        m_ZBufferBits(16),
        m_bIsFullscreen(false),
        m_bIsUsingStencilbuffer(true),
        m_bIsUsingAlphaChannel(false),
        m_bIsDoubleBuffered(true),
        m_bIsVerticalSyncEnabled(false),
        m_bIsAntiAliased(false)
   {
   }

   SGLContextParams(const SGLContextParams& other)
   {   
       *this = other;
   }

   SGLContextParams& operator=(const SGLContextParams& other)
   {
      m_openGLEngineType         = other.m_openGLEngineType;
      m_Bits                     = other.m_Bits;
      m_ZBufferBits              = other.m_ZBufferBits;
      m_bIsFullscreen            = other.m_bIsFullscreen;
      m_bIsUsingStencilbuffer    = other.m_bIsUsingStencilbuffer;
      m_bIsUsingAlphaChannel     = other.m_bIsUsingAlphaChannel;
      m_bIsDoubleBuffered        = other.m_bIsDoubleBuffered;
      m_bIsVerticalSyncEnabled   = other.m_bIsVerticalSyncEnabled;
      m_bIsAntiAliased           = other.m_bIsAntiAliased;
      return *this;
   }

   EOpenGLEngineType    m_openGLEngineType;
   unsigned char        m_Bits;
   unsigned char        m_ZBufferBits;
   bool                 m_bIsFullscreen;
   bool                 m_bIsUsingStencilbuffer;
   bool                 m_bIsUsingAlphaChannel;
   bool                 m_bIsDoubleBuffered;
   bool                 m_bIsVerticalSyncEnabled;
   bool                 m_bIsAntiAliased;
};
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __S_GL_CONTEXT_PARAMS_H__
//---------------------------------------------------------------------------------------

