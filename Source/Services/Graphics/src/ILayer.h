//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_LAYER_H__
#define __I_LAYER_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "GraphicsSTLExports.h"
#include "CEventEmitter.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
class ISceneNode;
class IScene;
template class AN_GRAPHICS_API std::vector<An::Graphics::ISceneNode*>;
template class AN_GRAPHICS_API std::allocator<An::Graphics::ISceneNode*>;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API ILayer : public CEventEmitter
{

protected:

    ILayer(IViewport* viewport);

public:

    virtual ~ILayer();

    std::string Name() const;

    AIndex64 Id() const;

    virtual void Render() = 0;

    virtual void Resize(const AUInt32 width, const AUInt32 height) = 0;

    IScene* Scene();

    void SetScene(IScene* scene);

protected:

    virtual void PreRenderOperations();

    virtual void PostRenderOperations();

    const std::vector<ISceneNode*>& GetRenderableNodes(ESceneNodeType nodeType) const;

    void RegisterNodeToRender(ISceneNode* node, ESceneNodeType nodeType);

protected:

    IViewport*      m_pViewport;
    AIndex64        m_id;
    std::string     m_name;
    void*           m_pRenderRegister;
    IScene*         m_pScene;

    friend class ISceneNode;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__I_LAYER_H__
//---------------------------------------------------------------------------------------
