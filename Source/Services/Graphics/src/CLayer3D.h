//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LAYER_3D_H__
#define __C_LAYER_3D_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "GraphicsSTLExports.h"
#include "ILayer.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class CCamera;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CLayer3D: public ILayer
{

public:

    CLayer3D(IViewport* viewport);

    virtual ~CLayer3D();

    virtual void Render();

    virtual void Resize(const AUInt32 width, const AUInt32 height);

    virtual CCamera* Camera() const;

    virtual void FitToScene();

    virtual void InitializeForScene();

protected:

    virtual void PreRenderOperations();

    virtual void PostRenderOperations();

    void SetSceneLightingAndMaterial();

    void SetCameraForRendering();

protected:

    CCamera*        m_pCamera;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LAYER_3D_H__
//---------------------------------------------------------------------------------------
