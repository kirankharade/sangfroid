//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CIOSTL.h"
#include "../GraphicsIncludes.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CVector3f.h"
#include "../Scene/C3DMesh.h"
#include "CStringUtils.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <vector>

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------
#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_
#pragma warning(disable : 4996)
#endif

//---------------------------------------------------------------------------------------

C3DMesh* CIOSTL::Read(const std::string& file)
{
   if(!IsValid(file))
   {
      return nullptr;
   }

   C3DMesh* pMesh = nullptr;

   if(IsBinary(file))
   {
      return ReadBinarySTL(file);
   }
   return ReadAsciiSTL(file);
}
//---------------------------------------------------------------------------------------

bool CIOSTL::Write(const std::string& file, const C3DMesh& mesh, bool bWriteBinary)
{
   if(bWriteBinary)
   {
      return WriteBinary(file, mesh);
   }
   return WriteASCII(file, mesh);
}
//---------------------------------------------------------------------------------------

bool CIOSTL::WriteBinary(const std::string& file, const C3DMesh& mesh)
{
   FILE* pFile = std::fopen(file.c_str(), "wb");
   if(!pFile)
   {
      return false;
   }

   const CPoint3f* vertices = mesh.Vertices();
   const CVector3f* normals = mesh.Normals();
   const unsigned int* indices = mesh.Indices();
   const int numIndices = mesh.IndexCount();
   const int numTriangles = numIndices / 3;

   string meshName = mesh.Name();
   if(meshName.size() == 0)
   {
      meshName = "Mesh";
   }

   fprintf(pFile, "%-80.80s", meshName.c_str());
   fwrite(&numTriangles, sizeof(int), 1, pFile);

   int idx0, idx1, idx2;
   short spacer = 0;
   float n[3]  = {0, 0, 0};
   float v1[3] = {0, 0, 0};
   float v2[3] = {0, 0, 0};
   float v3[3] = {0, 0, 0};

   CVector3f normal;
   CPoint3f va, vb, vc;

   //write 50 byte blocks per triangle
   for (int t = 0; t < numTriangles; t++)
   {
      idx0 = indices[(t * 3) + 0];
      idx1 = indices[(t * 3) + 1];
      idx2 = indices[(t * 3) + 2];
      
      normal = normals[t];
      n[0] = normal.x;
      n[1] = normal.y;
      n[2] = normal.z;

      va = vertices[idx0];
      vb = vertices[idx1];
      vc = vertices[idx2];

      v1[0] = va.x;
      v1[1] = va.y;
      v1[2] = va.z;

      v2[0] = vb.x;
      v2[1] = vb.y;
      v2[2] = vb.z;

      v3[0] = vc.x;
      v3[1] = vc.y;
      v3[2] = vc.z;

      fwrite(&n[0], sizeof(float), 3, pFile);

      //write vertices (36 bytes)
      fwrite(&v1[0], sizeof(float), 3, pFile);
      fwrite(&v2[0], sizeof(float), 3, pFile);
      fwrite(&v3[0], sizeof(float), 3, pFile);

      //write 2 byte spacer
      fwrite(&spacer, sizeof(short), 1, pFile);
   }

   std::fflush(pFile);
   std::fclose(pFile);
   return true;
}
//---------------------------------------------------------------------------------------

bool CIOSTL::WriteASCII(const std::string& file, const C3DMesh& mesh)
{
   FILE* pFile = std::fopen(file.c_str(), "w");
   if(!pFile)
   {
      return false;
   }

   const CPoint3f* vertices = mesh.Vertices();
   const CVector3f* normals = mesh.Normals();
   const unsigned int* indices = mesh.Indices();
   const int numIndices = mesh.IndexCount();
   const int numTriangles = numIndices / 3;

   string meshName = mesh.Name();
   if(meshName.size() == 0)
   {
      meshName = "Mesh";
   }
   string title = "solid ";
   title += meshName;

   fputs(title.c_str(), pFile);

   int idx0, idx1, idx2;

   CVector3f n;
   CPoint3f va, vb, vc;

   //write 50 byte blocks per triangle
   for (int t = 0; t < numTriangles; t++)
   {
      idx0 = indices[(t * 3) + 0];
      idx1 = indices[(t * 3) + 1];
      idx2 = indices[(t * 3) + 2];
      
      n = normals[t];
      va = vertices[idx0];
      vb = vertices[idx1];
      vc = vertices[idx2];

      fprintf(pFile, "facet normal %f %f %f\r\n", n.x, n.y, n.z);
      fputs("outer loop\r\n", pFile);
      fprintf(pFile, "vertex %f %f %f\r\n", va.x, va.y, va.z);
      fprintf(pFile, "vertex %f %f %f\r\n", vb.x, vb.y, vb.z);
      fprintf(pFile, "vertex %f %f %f\r\n", vc.x, vc.y, vc.z);
      fputs("endloop\r\n", pFile);
      fputs("endfacet\r\n", pFile);
   }

   fputs("endsolid\r\n", pFile);
   std::fflush(pFile);
   std::fclose(pFile);
   return true;
}
//---------------------------------------------------------------------------------------

bool CIOSTL::Write(const std::string& file, const C3DMesh* mesh, bool bWriteBinary)
{
   return Write(file, *mesh, bWriteBinary);
}
//---------------------------------------------------------------------------------------

C3DMesh* CIOSTL::ReadAsciiSTL(const std::string& file)
{
   const unsigned int MAX_LINE_BUFFER = 1000;
   char str[MAX_LINE_BUFFER], tmpStr[MAX_LINE_BUFFER];

   std::string cstr = "";
   float nx = 0, ny = 0, nz = 0;
   float x = 0, y = 0, z = 0;

   FILE* fp = std::fopen(file.c_str(), "r");
   if(!fp)
   {
      return NULL;
   }

   std::vector<CPoint3f> vertices;
   std::vector<CVector3f> normals;
   std::vector<CColor> colors;
   std::vector<CPoint2f> texCoords;
   std::vector<int> indices;

   int numIndices = 0;

   while(NULL != fgets(str, 1000, fp))
   {
      cstr = str;
      if(true == CStringUtils::StartsWith(cstr, "endSolid", true, true))
      {
         break;
      }
      if(true == CStringUtils::StartsWith(cstr, "outer", true, true) ||
         true == CStringUtils::StartsWith(cstr, "endloop", true, true))
      {
         continue;
      }

      if(true == CStringUtils::StartsWith(cstr, "facet", true, true))
      {
         sscanf(str, "%s %s %f %f %f", &tmpStr, &tmpStr, &nx, &ny, &nz);
         normals.push_back(CVector3f(nx, ny, nz));

         fgets(str, MAX_LINE_BUFFER, fp);
         fgets(str, MAX_LINE_BUFFER, fp);
         sscanf(str, "%s %f %f %f", &tmpStr, &x, &y, &z);
         vertices.push_back(CPoint3f(x,y,z));

         fgets(str, MAX_LINE_BUFFER, fp);
         sscanf(str, "%s %f %f %f", &tmpStr, &x, &y, &z);
         vertices.push_back(CPoint3f(x,y,z));

         fgets(str, MAX_LINE_BUFFER, fp);
         sscanf(str, "%s %f %f %f", &tmpStr, &x, &y, &z);
         vertices.push_back(CPoint3f(x,y,z));

         indices.push_back(numIndices); numIndices++;
         indices.push_back(numIndices); numIndices++;
         indices.push_back(numIndices); numIndices++;
      }
   }
   fclose(fp);

   C3DMesh* pMesh = new C3DMesh(vertices, normals, colors, texCoords, indices);

   return pMesh;
}
//---------------------------------------------------------------------------------------

C3DMesh* CIOSTL::ReadBinarySTL(const std::string& file)
{
   unsigned int numTriangles = 0;
   unsigned char headerSection[81];
   const int TRIANGLE_SIZE = 50; // 50 bytes is the size for each triangle.
   const int HEADER_SIZE = 80; // 80 bytes is the size for STL header section.

   long fileSize = FileSize(file);

   FILE* fp = std::fopen(file.c_str(), "rb");
   fread(headerSection, 1, HEADER_SIZE, fp); // Read the header, 80 bytes
   fread(&numTriangles, 1,  4, fp);          // 4 bytes- triangle count. Read the triangle count, not reliable.

   // Obtaining the count from the file length
   numTriangles = (unsigned int) ((fileSize - (HEADER_SIZE + 4)) / TRIANGLE_SIZE);

   std::vector<CPoint3f> vertices;
   std::vector<CVector3f> normals;
   std::vector<CColor> colors;
   std::vector<CPoint2f> texCoords;
   std::vector<int> indices;

   vertices.resize(numTriangles * 3);
   normals.resize(numTriangles);
   indices.resize(numTriangles * 3);

   STriangle t;
   int vertexIdx = 0;

   for(unsigned int i = 0; i < numTriangles; ++i)
   {
      if(fread(&t, 50, 1, fp) > 0)
      {
         normals[i] = CVector3f(t.n[0], t.n[1], t.n[2]);

         vertices[vertexIdx + 0] = CPoint3f(t.v[0][0], t.v[0][1], t.v[0][2]);
         vertices[vertexIdx + 1] = CPoint3f(t.v[1][0], t.v[1][1], t.v[1][2]);
         vertices[vertexIdx + 2] = CPoint3f(t.v[2][0], t.v[2][1], t.v[2][2]);

         indices[vertexIdx + 0] = vertexIdx + 0;
         indices[vertexIdx + 1] = vertexIdx + 1;
         indices[vertexIdx + 2] = vertexIdx + 2;

         vertexIdx += 3;
      }
   }

   fclose(fp);

   C3DMesh* pMesh = new C3DMesh(vertices, normals, colors, texCoords, indices);

   return pMesh;
}
//---------------------------------------------------------------------------------------

bool CIOSTL::IsValid(const std::string& file)
{
   struct stat fileStats;
   if(stat(file.c_str(), &fileStats) != 0)
   {
      return false;
   }

   long size = fileStats.st_size; //bytes
   if(size < 134) // 80 (header) + 4 + 50 (space occupied by at least one triangle)
   {
      return false;
   }

   if(! CStringUtils::EndsWith(file, ".stl", true, true))
   {
      return false;
   }
   return true;
}
//---------------------------------------------------------------------------------------

bool CIOSTL::IsBinary(const std::string& file)
{
   if(file.length() == 0)
   {
      return false;
   }

   const unsigned int BUFFER_LENGTH = 1024;
   const unsigned int TEMP_BUFFER_LENGTH = BUFFER_LENGTH - 84;

   unsigned char buf[BUFFER_LENGTH], tempbuf[TEMP_BUFFER_LENGTH];

   FILE* fp = std::fopen(file.c_str(), "rb");
   fread(buf, 1, BUFFER_LENGTH, fp);

   //Skip first 84 bytes while searching, because that is where header
   //and number for triangles count are located in binary file.
   unsigned int count = 0;
   for(unsigned int i = 84; i < BUFFER_LENGTH; i++, count++)
   {
      tempbuf[count] = CStringUtils::ToUpper(buf[i]);
   }

   // Binary STL should not contain string 'facet' after header section.
   string cstr = "facet";

   for(unsigned int i = 0; i < TEMP_BUFFER_LENGTH; i++)
   {
      bool bFound = false;
      if(CStringUtils::ToUpper(tempbuf[i]) == CStringUtils::ToUpper(cstr[0]))
      {
         for(unsigned int j = 1; j < cstr.length(); j++)
         {
            if((i + j) >= TEMP_BUFFER_LENGTH)
            {
               // Not found...
               fclose(fp);
               return true;
            }
            if(CStringUtils::ToUpper(tempbuf[i+j]) != CStringUtils::ToUpper(cstr[j]))
            {
               //continue the outer loop while searching...
               bFound = false;
               break;
            }
            else
            {
               bFound = true;
            }
         }
      }
      if(bFound == true)
      {
         // 'facet' string found in the buffer,... it is ASCII STL.
         fclose(fp);
         return false;
      }
   }
   fclose(fp);

   return true;
}
//---------------------------------------------------------------------------------------

long CIOSTL::FileSize(const std::string& file)
{
   struct stat fileStats;
   if( stat(file.c_str(), &fileStats) != 0 )
   {
      return 0;
   }

   return (long) fileStats.st_size;
}
//---------------------------------------------------------------------------------------

