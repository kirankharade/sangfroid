//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_IO_STL_H__
#define __C_IO_STL_H__
//---------------------------------------------------------------------------------------
#include <string>
#include "../GraphicsIncludes.h"
#include "../GraphicsSTLExports.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class C3DMesh;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CIOSTL
{

public:

   static C3DMesh* Read(const std::string& file);
   
   static bool Write(const std::string& file, const C3DMesh& mesh, bool bWriteBinary = true);

   static bool Write(const std::string& file, const C3DMesh* mesh, bool bWriteBinary = true);

private:

   static C3DMesh* ReadAsciiSTL(const std::string& file);

   static C3DMesh* ReadBinarySTL(const std::string& file);

   static bool IsValid(const std::string& file);

   static bool IsBinary(const std::string& file);

   static long FileSize(const std::string& file);

   static bool WriteBinary(const std::string& file, const C3DMesh& mesh);

   static bool WriteASCII(const std::string& file, const C3DMesh& mesh);


   struct STriangle
   {
      float n[3];
      float v[3][3];
      unsigned short attribute;
   };


};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_IO_STL_H__
//---------------------------------------------------------------------------------------

