//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "../GraphicsIncludes.h"
#include <string>
#include "CStringUtils.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------

char CStringUtils::ToUpper(const char& c)
{
   char ch = c;
   if( (c >= 65) && (ch <= 90) )
   {
      ch += 32;
   }
   return ch;
}
//---------------------------------------------------------------------------------------

char CStringUtils::ToLower(const char& c)
{
   char ch = c;
   if( (c > 32) && (ch <= 64) )
   {
      ch -= 32;
   }
   return ch;
}
//---------------------------------------------------------------------------------------

bool CStringUtils::StartsWith(const std::string& src, const std::string& search, 
                              const bool& bIgnoreCase, const bool& bIgnoreStartingSpaces)
{
   if( search.length() > src.length() )
   {
      return false;
   }

   string temp;
   int loc = 0;

   if( bIgnoreStartingSpaces )
   {
      while( src[loc] == ' ' || src[loc] == '\t' )
      {
         loc++;
      }
   }

   if(bIgnoreCase)
   {
      for( unsigned int i = 0; i < search.length(); i++ )
      {
         if( CStringUtils::ToUpper(src[loc + i]) != CStringUtils::ToUpper(search[i]) )
         {
            return false;
         }
      }
   }
   else
   {
      for( unsigned int  i = 0; i < search.length(); i++ )
      {
         if( src[loc + i] != search[i] )
         {
            return false;
         }
      }
   }
   return true;
}
//---------------------------------------------------------------------------------------

bool CStringUtils::EndsWith(const std::string& src, const std::string& search, 
                            const bool& bIgnoreCase, const bool& bIgnoreEndingSpaces)
{
   if( search.length() > src.length() )
   {
      return false;
   }

   unsigned int srcSize = (unsigned int)src.length();
   unsigned int searchSize = (unsigned int)search.length();
   string temp;
   unsigned int loc = srcSize - 1;

   if( bIgnoreEndingSpaces )
   {
      while( src[loc] == ' ' || src[loc] == '\t' )
      {
         loc--;
      }
   }

   if(bIgnoreCase)
   {
      for(int i = (searchSize - 1); i >= 0; i--)
      {
         const char srcChar = CStringUtils::ToUpper(src[loc - i]);
         const char searhChar = CStringUtils::ToUpper(search[(searchSize - 1) - i]);
         if( CStringUtils::ToUpper(srcChar) != CStringUtils::ToUpper(searhChar) )
         {
            return false;
         }
      }
   }
   else
   {
      for(int i = searchSize - 1; i >= 0; i-- )
      {
         if( src[loc - i] != search[(searchSize - 1) - i] )
         {
            return false;
         }
      }
   }
   return true;
}
//---------------------------------------------------------------------------------------

bool CStringUtils::Contains(const std::string& src, const std::string& search)
{
   unsigned int srcSize = (unsigned int)src.length();
   unsigned int searchSize = (unsigned int)search.length();

   for(unsigned int i = 0; i < srcSize; i++)
   {
      bool bFound = false;
      if( CStringUtils::ToUpper(src[i]) == CStringUtils::ToUpper(search[0]) )
      {
         for(unsigned int j = 1; j < searchSize; j++)
         {
            if( (i + j) >= srcSize )
            {
               // Not found...
               return false;
            }
            if( CStringUtils::ToUpper(src[i+j]) != CStringUtils::ToUpper(search[j]) )
            {
               //continue the outer loop while searching...
               bFound = false;
               break;
            }
            else
            {
               bFound = true;
            }
         }
      }
      if(bFound == true)
      {
         return true;
      }
   }
   return false;
}
//---------------------------------------------------------------------------------------

void CStringUtils::ToWideCharArray(const char*& s, wchar_t*& ws, const unsigned int& size)
{
   ws = new wchar_t [size+1];
   for(unsigned int i = 0; i < size; i++)
   {
      ws[i] = s[i];
   }
   ws[size] = '\0';
}
//---------------------------------------------------------------------------------------

std::wstring CStringUtils::ToWideString(const std::string& s)
{
   unsigned int size = s.length();
   std::wstring wstr;
   wstr.reserve(size);

   for(unsigned int i = 0; i < size; i++)
   {
      wstr[i] = s[i];
   }

   return wstr;
}
//---------------------------------------------------------------------------------------

void CStringUtils::ToWideString(const std::string& s, std::wstring& ws)
{
   unsigned int size = s.length();
   ws.clear();
   ws.reserve(size);

   for(unsigned int i = 0; i < size; i++)
   {
      ws[i] = s[i];
   }
}
//---------------------------------------------------------------------------------------

std::string CStringUtils::ToString(const AIndex64& num)
{
    char str[100];
    sprintf(str, "%ld", num);
    return std::string(str);
}
//---------------------------------------------------------------------------------------
