//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LIGHT_H__
#define __C_LIGHT_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "GraphicsSTLExports.h"
#include "CColor.h"
#include "../Maths/CPoint3f.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class CLightManager;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CLight
{

protected:

    CLight();

    CLight(const CColor& ambient, const CColor& diffuse, const CColor& specular, 
           const Maths::CPoint3f& position, 
           const Maths::CVector3f& direction = Maths::CVector3f(0, 0, -1));

    CLight(const CLight& light);

    virtual ~CLight();

public:

    std::string Name() const;
    void SetName(const std::string& name);

    ELightType Type() const;
    void SetType(const ELightType& type);

    CColor Ambient() const;
    void SetAmbient(const CColor& ambient);

    CColor Diffuse() const;
    void SetDiffuse(const CColor& diffuse);

    CColor Specular() const;
    void SetSpecular(const CColor& specular);

    Maths::CPoint3f Position() const;
    void SetPosition(const Maths::CPoint3f& position);

    Maths::CVector3f SpotDirection() const;
    void SetSpotDirection(const Maths::CVector3f& direction);

    AReal32 SpotCutOffAngleDeg() const;
    void SetSpotCutOffAngleDeg(const AReal32 angle);

    AReal32 SpotExponent() const;
    void SetSpotExponent(const AReal32 exponent);

    AReal32 ConstantAttenuation() const;
    void SetConstantAttenuation(const AReal32 constantAttenuation);

    AReal32 LinearAttenuation() const;
    void SetLinearAttenuation(const AReal32 linearAttenuation);

    AReal32 QuadraticAttenuation() const;
    void SetQuadraticAttenuation(const AReal32 quadraticAttenuation);

    void Enable();

    void Disable();

    bool IsEnabled() const;

    AInt16 Index() const;

    CLight& operator = (const CLight& light);

    bool operator == (const CLight& light) const;
    bool operator != (const CLight& light) const;

private:

    void SetIndex(const AInt16& index);

    void SetManager(CLightManager* manager);

private:

    CColor  m_ambient;
    CColor  m_diffuse;
    CColor  m_specular;

    Maths::CPoint3f     m_position;
    Maths::CVector3f    m_spotDirection;

    AReal32  m_spotCutOffAngle;
    AReal32  m_spotExponent;
    AReal32  m_constantAttenuation;
    AReal32  m_linearAttenuation;
    AReal32  m_quadraticAttenuation;

    AInt16          m_index;
    std::string     m_name;
    CLightManager*  m_pManager;
    ELightType      m_type;

    friend class IGraphics;
    friend class CLightManager;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LIGHT_H__
//---------------------------------------------------------------------------------------
