//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_GRAPHICS_CONTEXT_H__
#define __C_GRAPHICS_CONTEXT_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "IGraphicsContext.h"
#include "SGLContextParams.h"
#include "IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class IWindow;

//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CGraphicsContext: public IGraphicsContext
{

protected:

    CGraphicsContext();

    CGraphicsContext(const SGLContextParams& contextParams);

    CGraphicsContext(IWindow* window);

    CGraphicsContext(const SGLContextParams& contextParams, IWindow* window);

    virtual ~CGraphicsContext();

public:

    virtual void MakeCurrent();

    virtual void SwapBuffers();

    const SGLContextParams* ContextParams() const;

protected:

    SGLContextParams    m_glContextParams;
    IWindow*            m_window;
    bool                m_bSelfCreatedWindow;
    int                 m_majorOpenGLVersion;
    int                 m_minorOpenGLVersion;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_GRAPHICS_CONTEXT_H__
//---------------------------------------------------------------------------------------

