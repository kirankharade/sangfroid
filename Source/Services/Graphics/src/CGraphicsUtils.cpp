//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/MathsDefines.h"
#include "CGraphicsUtils.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------
long CGraphicsUtils::s_uniqueID = 0;
//---------------------------------------------------------------------------------------

long CGraphicsUtils::GetUniqueObjectID()
{
    s_uniqueID++;
    return s_uniqueID;
}
//---------------------------------------------------------------------------------------
