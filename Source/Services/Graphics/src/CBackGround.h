//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_BACK_GROUND_H__
#define __C_BACK_GROUND_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "CColor.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CBackGround
{

public:

    CBackGround(IViewport* parentViewport);

    virtual ~CBackGround();

    void SetColors(const CColor& color);

    void SetColors(const CColor& color1,
                   const CColor& color2);

    void SetColors(const CColor& color1,
                   const CColor& color2,
                   const CColor& color3);

    void SetColors(const CColor& color1,
                   const CColor& color2,
                   const CColor& color3,
                   const CColor& color4);

    void SetColors(const CColor& color1, //bottomLeftColor
                   const CColor& color2, //bottomRightColor
                   const CColor& color3, //topRightColor
                   const CColor& color4, //topLeftColor 
                   const CColor& centreColor);

    //void SetTexture(CTexture* texture);

    void SetType(EBackGroundType type);

    void SetColoringType(EBackGroundColorType type);

    void SetTextureLayoutType(EBackGroundTextureType type);

    EBackGroundType Type() const;

    EBackGroundColorType ColoringType() const;

    EBackGroundTextureType TextureLayoutType() const;

    CColor* Colors() const;

    IViewport* Viewport() const;

    void Render();

    virtual void Resize(const AUInt32 width, const AUInt32 height);

protected:

    CColor                      m_colors[5];
    EBackGroundType             m_type;
    EBackGroundColorType        m_colorType;
    EBackGroundTextureType      m_textureLayoutType;
    IViewport*                  m_pParentViewport;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_BACK_GROUND_H__
//---------------------------------------------------------------------------------------
