//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_DRAW_MANAGER_H__
#define __I_DRAW_MANAGER_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "CColor.h"
#include "Defaults.h"
#include "../Maths/CPoint2f.h"
#include "../Maths/CPoint3f.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IGraphics;
class I2DGeometry;
class I3DGeometry;
class CBackGround;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IDrawManager
{

public:

    IDrawManager(IGraphics* graphics) {}

    virtual ~IDrawManager() {}

    virtual IGraphics* Graphics() = 0;

    virtual void Draw(const CBackGround* background) = 0;

    virtual void Draw(I2DGeometry* geometry) const = 0;

    virtual void Draw(I3DGeometry* geometry) const = 0;

    virtual void DrawPoint(const Maths::CPoint3f& point, const CColor& color = Defaults::Color, const AReal32& size = Defaults::PointSize) const = 0;

    virtual void DrawPoint(const Maths::CPoint2f& point, const CColor& color = Defaults::Color, const AReal32& size = Defaults::PointSize) const = 0;

    virtual void DrawLine(const Maths::CPoint3f& point1, const Maths::CPoint3f& point2, const CColor& color = Defaults::Color, const AReal32& thickness = Defaults::LineWidth) const = 0;

    virtual void DrawLine(const Maths::CPoint2f& point1, const Maths::CPoint2f& point2, const CColor& color = Defaults::Color, const AReal32& thickness = Defaults::LineWidth) const = 0;

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_DRAW_MANAGER_H__
//---------------------------------------------------------------------------------------

