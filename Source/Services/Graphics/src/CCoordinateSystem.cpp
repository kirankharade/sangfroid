//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "CCoordinateSystem.h"
#include "CGraphicsUtils.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CCoordinateSystem::CCoordinateSystem(CCoordinateSystem* parent)
: m_id(CGraphicsUtils::GetUniqueObjectID()),
  m_name(nullptr),
  m_parent(parent),
  m_origin(ORIGIN),
  m_xRotationDeg(0),
  m_yRotationDeg(0),
  m_zRotationDeg(0),
  m_relativeTransform(CMatrix4f(true)),
  m_absoluteTransform(CMatrix4f(true))
{
}
//---------------------------------------------------------------------------------------

CCoordinateSystem::~CCoordinateSystem()
{
    if(m_name != nullptr)
    {
        SAFE_CLEANUP(m_name);
    }
}
//---------------------------------------------------------------------------------------

long CCoordinateSystem::Id() const
{
    return m_id;
}
//---------------------------------------------------------------------------------------

const char* CCoordinateSystem::Name() const
{
    return m_name;
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetName(const char* name)
{
    if(m_name != nullptr)
    {
        SAFE_CLEANUP(m_name);
    }
    if(name != nullptr)
    {
        int length = strlen(name);
        m_name = new char[length + 1];
        strcpy(m_name, name);
    }
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetParent(CCoordinateSystem* parent)
{
    if(m_parent != parent)
    {
        m_parent = parent;
        Update();
    }
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetAbsoluteTransform(const Maths::CMatrix4f& m)
{
    m_absoluteTransform = m;
    //If absolute matrix is directly set, set parent to nullptr 
    //and set all the relative parameters to their initialization value.
    m_parent = nullptr;
	m_origin = ORIGIN;
	m_xRotationDeg = 0;
	m_yRotationDeg = 0;
	m_zRotationDeg = 0;
    m_relativeTransform.setIdentity();
}
//---------------------------------------------------------------------------------------

Maths::CMatrix4f CCoordinateSystem::RelativeTransform() const
{
    return m_relativeTransform;
}
//---------------------------------------------------------------------------------------

Maths::CMatrix4f CCoordinateSystem::AbsoluteTransform() const
{
    return m_absoluteTransform;
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetRelativeOrigin(const Maths::CPoint3f o)
{
    m_origin = o;
    Update();
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetRelativeRotationDegX(const float angle)
{
    m_xRotationDeg = angle;
    Update();
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetRelativeRotationDegY(const float angle)
{
    m_yRotationDeg = angle;
    Update();
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::SetRelativeRotationDegZ(const float angle)
{
    m_zRotationDeg = angle;
    Update();
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::Update()
{
    CalculateRelativeMatrix();

    if(m_parent)
    {
        //KK: Please check the multiplication order...
        m_absoluteTransform = RelativeTransform() * m_parent->RelativeTransform();
    }
    else
    {
        //Global cs
        m_absoluteTransform = RelativeTransform();
    }
}
//---------------------------------------------------------------------------------------

void CCoordinateSystem::CalculateRelativeMatrix() 
{
    m_relativeTransform.setIdentity();
    m_relativeTransform.setTranslation(m_origin.x, m_origin.y, m_origin.z);
	m_relativeTransform.setRotationDeg(m_xRotationDeg, m_yRotationDeg, m_zRotationDeg);
}
//---------------------------------------------------------------------------------------

