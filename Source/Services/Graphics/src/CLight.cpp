//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CLight.h"
#include "CLightManager.h"
#include "CColor.h"
#include "Defaults.h"
#include "IFactory.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CLight::CLight()
: m_ambient(Defaults::LightAmbient),
  m_position(Defaults::LightPosition),
  m_spotDirection(Defaults::LightSpotDirection),
  m_spotCutOffAngle(Defaults::LightSpotCutOffAngle),
  m_spotExponent(Defaults::LightSpotExponent),
  m_constantAttenuation(Defaults::LightConstantAttenuation),
  m_linearAttenuation(Defaults::LightLinearAttenuation),
  m_quadraticAttenuation(Defaults::LightQuadraticAttenuation),
  m_index(-1),
  m_name(""),
  m_type(ELightType_GeneralDirectionalLight)
{
}
//---------------------------------------------------------------------------------------

CLight::CLight(const CColor& ambient, const CColor& diffuse, const CColor& specular, 
        const Maths::CPoint3f& position, const Maths::CVector3f& direction)
: m_ambient(ambient),
  m_diffuse(diffuse),
  m_specular(specular),
  m_position(position),
  m_spotDirection(direction),
  m_spotCutOffAngle(Defaults::LightSpotCutOffAngle),
  m_spotExponent(Defaults::LightSpotExponent),
  m_constantAttenuation(Defaults::LightConstantAttenuation),
  m_linearAttenuation(Defaults::LightLinearAttenuation),
  m_quadraticAttenuation(Defaults::LightQuadraticAttenuation),
  m_index(-1),
  m_name(""),
  m_type(ELightType_GeneralDirectionalLight)
{
}
//---------------------------------------------------------------------------------------

CLight::CLight(const CLight& light)
: m_ambient(light.m_ambient),
  m_diffuse(light.m_diffuse),
  m_specular(light.m_specular),
  m_position(light.m_position),
  m_spotDirection(light.m_spotDirection),
  m_spotCutOffAngle(light.m_spotCutOffAngle),
  m_spotExponent(light.m_spotExponent),
  m_constantAttenuation(light.m_constantAttenuation),
  m_linearAttenuation(light.m_linearAttenuation),
  m_quadraticAttenuation(light.m_quadraticAttenuation),
  m_index(-1),
  m_name(""),
  m_type(ELightType_GeneralDirectionalLight)
{
}
//---------------------------------------------------------------------------------------

CLight::~CLight()
{
}
//---------------------------------------------------------------------------------------

std::string CLight::Name() const
{
    return m_name;
}
//---------------------------------------------------------------------------------------

void CLight::SetName(const std::string& name)
{
    m_name = name;
}
//---------------------------------------------------------------------------------------

ELightType CLight::Type() const
{
    return m_type;
}
//---------------------------------------------------------------------------------------

void CLight::SetType(const ELightType& type)
{
    m_type = type;
}
//---------------------------------------------------------------------------------------

CColor CLight::Ambient() const
{
    return m_ambient;
}
//---------------------------------------------------------------------------------------

void CLight::SetAmbient(const CColor& ambient)
{
    m_ambient = ambient;
}
//---------------------------------------------------------------------------------------

CColor CLight::Diffuse() const
{
    return m_diffuse;
}
//---------------------------------------------------------------------------------------

void CLight::SetDiffuse(const CColor& diffuse)
{
    m_diffuse = diffuse;
}
//---------------------------------------------------------------------------------------

CColor CLight::Specular() const
{
    return m_specular;
}
//---------------------------------------------------------------------------------------

void CLight::SetSpecular(const CColor& specular)
{
    m_specular = specular;
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f CLight::Position() const
{
    return m_position;
}
//---------------------------------------------------------------------------------------

void CLight::SetPosition(const Maths::CPoint3f& position)
{
    m_position = position;
}
//---------------------------------------------------------------------------------------

Maths::CVector3f CLight::SpotDirection() const
{
    return m_spotDirection;
}
//---------------------------------------------------------------------------------------

void CLight::SetSpotDirection(const Maths::CVector3f& direction)
{
    m_spotDirection = direction;
}
//---------------------------------------------------------------------------------------

AReal32 CLight::SpotCutOffAngleDeg() const
{
    return m_spotCutOffAngle;
}
//---------------------------------------------------------------------------------------

void CLight::SetSpotCutOffAngleDeg(const AReal32 angle)
{
    m_spotCutOffAngle = angle;
}
//---------------------------------------------------------------------------------------

AReal32 CLight::SpotExponent() const
{
    return m_spotExponent;
}
//---------------------------------------------------------------------------------------

void CLight::SetSpotExponent(const AReal32 exponent)
{
    m_spotExponent = exponent;
}
//---------------------------------------------------------------------------------------

AReal32 CLight::ConstantAttenuation() const
{
    return m_constantAttenuation;
}
//---------------------------------------------------------------------------------------

void CLight::SetConstantAttenuation(const AReal32 constantAttenuation)
{
    m_constantAttenuation = constantAttenuation;
}
//---------------------------------------------------------------------------------------

AReal32 CLight::LinearAttenuation() const
{
    return m_linearAttenuation;
}
//---------------------------------------------------------------------------------------

void CLight::SetLinearAttenuation(const AReal32 linearAttenuation)
{
    m_linearAttenuation = linearAttenuation;
}
//---------------------------------------------------------------------------------------

AReal32 CLight::QuadraticAttenuation() const
{
    return m_quadraticAttenuation;
}
//---------------------------------------------------------------------------------------

void CLight::SetQuadraticAttenuation(const AReal32 quadraticAttenuation)
{
    m_quadraticAttenuation = quadraticAttenuation;
}
//---------------------------------------------------------------------------------------

void CLight::Enable()
{
	if(m_pManager)
	{
		m_pManager->EnableLight(this);
	}
}
//---------------------------------------------------------------------------------------

void CLight::Disable()
{
	if(m_pManager)
	{
		m_pManager->DisableLight(this);
	}
}
//---------------------------------------------------------------------------------------

bool CLight::IsEnabled() const
{
	if(m_pManager)
	{
		m_pManager->IsEnabled(this);
	}
    return false;
}
//---------------------------------------------------------------------------------------

AInt16 CLight::Index() const
{
    return m_index;
}
//---------------------------------------------------------------------------------------

void CLight::SetIndex(const AInt16& index)
{
    m_index = index;
}
//---------------------------------------------------------------------------------------

void CLight::SetManager(CLightManager* manager)
{
	m_pManager = manager;
}
//---------------------------------------------------------------------------------------

CLight& CLight::operator = (const CLight& light)
{
    m_ambient = light.m_ambient;
    m_diffuse = light.m_diffuse;
    m_specular = light.m_specular;
    m_position = light.m_position;
    m_spotDirection = light.m_spotDirection;
    m_spotCutOffAngle = light.m_spotCutOffAngle;
    m_spotExponent = light.m_spotExponent;
    m_constantAttenuation = light.m_constantAttenuation;
    m_linearAttenuation = light.m_linearAttenuation;
    m_quadraticAttenuation = light.m_quadraticAttenuation;

    return *this;
}
//---------------------------------------------------------------------------------------

bool CLight::operator == (const CLight& light) const
{

    return (
         (m_ambient == light.m_ambient) &&
         (m_diffuse == light.m_diffuse) &&
         (m_specular == light.m_specular) &&
         (m_position == light.m_position) &&
         (m_spotDirection == light.m_spotDirection) &&
         (fabs(m_spotCutOffAngle - light.m_spotCutOffAngle) < 1.0f) &&
         (fabs(m_spotExponent - light.m_spotExponent) < 0.001f) &&
         (fabs(m_constantAttenuation - light.m_constantAttenuation) < 0.0001f) &&
         (fabs(m_linearAttenuation - light.m_linearAttenuation) < 0.0001f) &&
         (fabs(m_quadraticAttenuation - light.m_quadraticAttenuation) < 0.0001f)
        );
}
//---------------------------------------------------------------------------------------

bool CLight::operator != (const CLight& light) const
{
    return !(*this == light);
}
//---------------------------------------------------------------------------------------

