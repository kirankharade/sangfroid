//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../IDrawManager.h"
#include "../Dependencies/glew/GL/glew.h"
//---------------------------------------------------------------------------------------
#ifndef __C_FIXED_FUNCTION_DRAW_MANAGER_H__
#define __C_FIXED_FUNCTION_DRAW_MANAGER_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IGraphics;
class I2DGeometry;
class I3DGeometry;
class CBackGround;
class C3DMesh;
class CFixedFunctionBackgroundPainter;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CFixedFunctionDrawManager : public IDrawManager
{

public:

    CFixedFunctionDrawManager(IGraphics* graphics);

    virtual ~CFixedFunctionDrawManager();

    virtual IGraphics* Graphics();

    virtual void Draw(const CBackGround* background);

    virtual void Draw(I2DGeometry* geometry) const;

    virtual void Draw(I3DGeometry* geometry) const;

    virtual void DrawPoint(const Maths::CPoint3f& point, const CColor& color = Defaults::Color, const AReal32& size = Defaults::PointSize) const;

    virtual void DrawPoint(const Maths::CPoint2f& point, const CColor& color = Defaults::Color, const AReal32& size = Defaults::PointSize) const;

    virtual void DrawLine(const Maths::CPoint3f& point1, const Maths::CPoint3f& point2, const CColor& color = Defaults::Color, const AReal32& thickness = Defaults::LineWidth) const;

    virtual void DrawLine(const Maths::CPoint2f& point1, const Maths::CPoint2f& point2, const CColor& color = Defaults::Color, const AReal32& thickness = Defaults::LineWidth) const;

protected:

    void ApplyOrthographic2DProjection(const int width, const int height) const;

    void ResetLastProjection() const;

    void Draw3DMesh(C3DMesh* mesh) const;

    GLboolean* DisableClipping() const;

    void EnableClipping(GLboolean* planesStatus, const bool deallocate) const;

protected:

    IGraphics*                          m_pGraphics;
    CFixedFunctionBackgroundPainter*    m_pBGPainter;

    friend class CFixedFunctionBackgroundPainter;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_FIXED_FUNCTION_DRAW_MANAGER_H__
//---------------------------------------------------------------------------------------

