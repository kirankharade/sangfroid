//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../IDrawManager.h"
#include "../Resources/ITextureManager.h"
#include "../Dependencies/glew/GL/glew.h"
//---------------------------------------------------------------------------------------
#ifndef __C_FIXED_FUNCTION_TEXTURE_MANAGER_H__
#define __C_FIXED_FUNCTION_TEXTURE_MANAGER_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IGraphics;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CFixedFunctionTextureManager : public ITextureManager
{

public:

    CFixedFunctionTextureManager(IGraphics* graphics);

    virtual ~CFixedFunctionTextureManager();

    virtual bool Add(ITexture* texture);

    virtual void Update(ITexture* texture);

protected:

    void RemoveFromGraphics(const AUInt32 openGLId);

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_FIXED_FUNCTION_TEXTURE_MANAGER_H__
//---------------------------------------------------------------------------------------

