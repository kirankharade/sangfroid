//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CFixedFunctionGraphics.h"
#include "CFixedFunctionDrawManager.h"
#include "CFixedFunctionBackgroundPainter.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CPoint2f.h"
#include "../CBackGround.h"
#include "../Scene/I3DGeometry.h"
#include "../Scene/C3DMesh.h"
#include "../CBackGround.h"
#include "../IViewport.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CFixedFunctionDrawManager::CFixedFunctionDrawManager(IGraphics* graphics)
    : IDrawManager(graphics)
      ,m_pGraphics(graphics)
      ,m_pBGPainter(nullptr)
{
}
//---------------------------------------------------------------------------------------

CFixedFunctionDrawManager::~CFixedFunctionDrawManager()
{
    m_pGraphics = nullptr;
}
//---------------------------------------------------------------------------------------

IGraphics* CFixedFunctionDrawManager::Graphics()
{
    return m_pGraphics;
}
//---------------------------------------------------------------------------------------


void CFixedFunctionDrawManager::Draw(const CBackGround* pBackGround)
{
    if(nullptr == m_pBGPainter)
    {
        m_pBGPainter = new CFixedFunctionBackgroundPainter(m_pGraphics, this);
    }
    m_pBGPainter->Draw(pBackGround);
}
//---------------------------------------------------------------------------------------

GLboolean* CFixedFunctionDrawManager::DisableClipping() const
{
    GLboolean* s = new GLboolean[GL_MAX_CLIP_PLANES];
    for(GLint i = 0; i < GL_MAX_CLIP_PLANES; i++)
    {
        s[0] = glIsEnabled(GL_CLIP_PLANE0 + i);
        glDisable(GL_CLIP_PLANE0 + i);
    }
    return s;
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::EnableClipping(GLboolean* planesStatus, const bool deallocate) const
{
    for(GLint i = 0; i < GL_MAX_CLIP_PLANES; i++)
    {
        if(planesStatus[i] == GL_TRUE)
        {
            glEnable(GL_CLIP_PLANE0 + i);
        }
    }
    if(deallocate)
    {
        SAFE_ARRAY_CLEANUP(planesStatus);
    }
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::ApplyOrthographic2DProjection(const int width, const int height) const
{
   glMatrixMode(GL_PROJECTION);
   glPushMatrix();
   glLoadIdentity();
   gluOrtho2D(0,(GLsizei) width, (GLsizei) height, 0);

   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glLoadIdentity();
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::ResetLastProjection() const
{
   glMatrixMode(GL_PROJECTION);
   glPopMatrix();

   glMatrixMode(GL_MODELVIEW);
   glPopMatrix();
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::Draw(I2DGeometry* geometry) const
{
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::Draw3DMesh(C3DMesh* mesh) const
{
    const CPoint3f* vertices = mesh->Vertices();
    const unsigned int vertexCount = mesh->VertexCount();
    if(vertexCount == 0)
    {
        return;
    }
    /////////////////////////////////////////////////////////////////////////////

    GLfloat mp[16], mv[16];

    glGetFloatv(GL_MODELVIEW_MATRIX, mv);
    glGetFloatv(GL_PROJECTION_MATRIX, mp);

    GLenum err = glGetError();

    //Apply material if set
    //{
        //GLfloat white[4] = {1.0f, 1.0f, 1.0f, 1.0f};
        //glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
        //glMaterialfv(GL_BACK, GL_DIFFUSE, white);
        //glMaterialfv(GL_FRONT, GL_AMBIENT, white);
        //glMaterialfv(GL_BACK, GL_AMBIENT, white);
    //}

    glEnable(GL_LIGHTING);

    //err = glGetError();
    /////////////////////////////////////////////////////////////////////////////
   
   const CVector3f* normals = mesh->Normals();
   const unsigned int* indices = mesh->Indices();
   const int numIndices = mesh->IndexCount();
   const int numTriangles = numIndices / 3;

   const CPoint3f centre = mesh->Bounds().getCentre();
   const CAxisOrientedBox box = mesh->Bounds();

    const AReal32* colors = mesh->Colors();
    const CPoint2f* texCoords = mesh->TexCoords();
    unsigned int indexCount = mesh->IndexCount();
    bool useVertexColors = mesh->UseVertexColors() && mesh->ColorCount() > 0;

    //const int textureId = mesh->TextureId();
    //const bool useTextures = textureId > 0 && 
    //                         mesh->TexturingEnabled() &&
    //                         texCoords.size() > 0;

    const int textureId = -1;
    const bool useTextures = false;

    float* vertexPointer = (float*) vertices;
    float* normalPointer = (float*) normals;
    float* colorPointer = (float*) colors;
    float* texCoordsPointer = (float*) texCoords;
    unsigned int* indexPointer = (unsigned int*) indices;

    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_LIGHTING_BIT);
    {
        err = glGetError();

        glEnable(GL_LIGHTING);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, 0, vertexPointer);

        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, normalPointer);

        err = glGetError();

        if(useVertexColors)
        {
            glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
            glEnable(GL_COLOR_MATERIAL);
            glEnableClientState(GL_COLOR_ARRAY);
            glColorPointer(4, GL_FLOAT, 0, colorPointer);
        }
        else
        {
            glEnable(GL_LIGHTING);
            glDisable(GL_COLOR_MATERIAL);

        }
        err = glGetError();

        //if(useTextures)
        //{
        //    glEnable(GL_TEXTURE_2D);

        //    if(m_pGraphics->TextureManager()->IsDirty(textureId))
        //    {
        //        m_pGraphics->TextureManager()->Update(textureId);
        //    }
        //    m_pGraphics->TextureManager()->Bind(textureId);
        //    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        //    glTexCoordPointer(2, GL_FLOAT, 0, texCoordsPointer);
        //}

        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, indexPointer);

        err = glGetError();

        if(useTextures)
        {
            glDisable(GL_TEXTURE_2D);
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        }

        if(useVertexColors)
        {
            glDisableClientState(GL_COLOR_ARRAY);
        }
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);

    }
    glPopAttrib();
    err = glGetError();
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::Draw(I3DGeometry* geometry) const
{
    if(!geometry)
    {
        return;
    }

    EDrawableType type = geometry->Type();
    
    if(type == EDrawableType_Mesh3D)
    {
        C3DMesh* mesh = dynamic_cast<C3DMesh*> (geometry);
        if(nullptr != mesh)
        {
            Draw3DMesh(mesh);
        }
    }
    else if(type == EDrawableType_Text3D)
    {
    }
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::DrawPoint(const CPoint3f& point, const CColor& color, const AReal32& size) const
{
    bool isLightEnabled = glIsEnabled(GL_LIGHTING);

    glDisable(GL_LIGHTING);

    glPointSize(size);

    SColorRGBAf c = color.ToRGBAf();

    glBegin(GL_POINTS);

        glColor4f(c.r, c.g, c.b, c.a);
        
        glVertex3f(point.x, point.y, point.z);

    glEnd();

    if(isLightEnabled)
    {
        glEnable(GL_LIGHTING);
    }
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::DrawPoint(const CPoint2f& point, const CColor& color, const AReal32& size) const
{
    bool isLightEnabled = glIsEnabled(GL_LIGHTING);

    glDisable(GL_LIGHTING);

    glPointSize(size);

    SColorRGBAf c = color.ToRGBAf();

    glBegin(GL_POINTS);

        glColor4f(c.r, c.g, c.b, c.a);
        
        glVertex2f(point.x, point.y);

    glEnd();

    if(isLightEnabled)
    {
        glEnable(GL_LIGHTING);
    }
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::DrawLine(const CPoint3f& point1, const CPoint3f& point2, const CColor& color, const AReal32& thickness) const
{
    bool isLightEnabled = glIsEnabled(GL_LIGHTING);

    glDisable(GL_LIGHTING);

    glLineWidth(thickness);

    SColorRGBAf c = color.ToRGBAf();

    glBegin(GL_LINES);

        glColor4f(c.r, c.g, c.b, c.a);
        
        glVertex3f(point1.x, point1.y, point1.z);
        glVertex3f(point2.x, point2.y, point2.z);

    glEnd();

    if(isLightEnabled)
    {
        glEnable(GL_LIGHTING);
    }
}
//---------------------------------------------------------------------------------------

void CFixedFunctionDrawManager::DrawLine(const CPoint2f& point1, const CPoint2f& point2, const CColor& color, const AReal32& thickness) const
{
    bool isLightEnabled = glIsEnabled(GL_LIGHTING);

    glDisable(GL_LIGHTING);

    glLineWidth(thickness);

    SColorRGBAf c = color.ToRGBAf();

    glBegin(GL_LINES);

        glColor4f(c.r, c.g, c.b, c.a);
        
        glVertex2f(point1.x, point1.y);
        glVertex2f(point2.x, point2.y);

    glEnd();

    if(isLightEnabled)
    {
        glEnable(GL_LIGHTING);
    }
}
//---------------------------------------------------------------------------------------

