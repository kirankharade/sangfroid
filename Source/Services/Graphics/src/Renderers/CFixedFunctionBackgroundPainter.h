//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../IGraphics.h"
#include "../Dependencies/glew/GL/glew.h"
//---------------------------------------------------------------------------------------
#ifndef __C_FIXED_FUNCTION_BACKGROUND_PAINTER_H__
#define __C_FIXED_FUNCTION_BACKGROUND_PAINTER_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IGraphics;
class CFixedFunctionDrawManager;
class CBackGround;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CFixedFunctionBackgroundPainter
{

public:

    CFixedFunctionBackgroundPainter(IGraphics* graphics, CFixedFunctionDrawManager* drawManager);

    virtual ~CFixedFunctionBackgroundPainter();

    virtual void Draw(const CBackGround* background) const;

protected:

    void DrawSingleColoredBackGround(const CColor& color) const;

    void DrawColoredBackGround(const CBackGround* pBackGround) const;

    void DrawTexturedBackGround(const CBackGround* pBackGround) const;

    void DrawColoredTextureBlendedBackGround(const CBackGround* pBackGround) const;

    virtual void SetUp() const;

protected:

    IGraphics*                      m_pGraphics;
    CFixedFunctionDrawManager*      m_pDrawManager;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_FIXED_FUNCTION_BACKGROUND_PAINTER_H__
//---------------------------------------------------------------------------------------

