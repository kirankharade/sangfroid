//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CFixedFunctionGraphics.h"
#include "CFixedFunctionTextureManager.h"
#include "../Resources/ITexture.h"
#include "../Resources/ITextureManager.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CPoint2f.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CFixedFunctionTextureManager::CFixedFunctionTextureManager(IGraphics* graphics)
    : ITextureManager(graphics)
{
}
//---------------------------------------------------------------------------------------

CFixedFunctionTextureManager::~CFixedFunctionTextureManager()
{
    ITextureManager::DeleteAllTextures();
}
//---------------------------------------------------------------------------------------

bool CFixedFunctionTextureManager::Add(ITexture* texture)
{
    if(nullptr == texture)
    {
        return false;
    }
    if(Exists(texture))
    {
        return true;
    }

    GLuint oglId = 0;
    glGenTextures(1, &oglId);

    GLint w = texture->Width();
    GLint h = texture->Height();
    const GLvoid* data = texture->Data();

    glBindTexture(GL_TEXTURE_2D, oglId);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,
                             0,
                       GL_RGBA,
                             w,
                             h,
                             0,
                   GL_BGRA_EXT,
              GL_UNSIGNED_BYTE,
                         data);

    glBindTexture(GL_TEXTURE_2D, oglId);

    SetOpenGLId(texture, oglId);

    return ITextureManager::Add(texture);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionTextureManager::Update(ITexture* texture)
{
    if(nullptr == texture)
    {
        return;
    }
    if(!Exists(texture))
    {
        Add(texture);
        return;
    }

    GLuint oglId = GetOpenGLId(texture);
    if(false == glIsTexture(oglId))
    {
        glGenTextures(1, &oglId);
        SetOpenGLId(texture, oglId);
    }

    GLint w = texture->Width();
    GLint h = texture->Height();
    const GLvoid* data = texture->Data();

    glBindTexture(GL_TEXTURE_2D, oglId);

    glTexSubImage2D(GL_TEXTURE_2D,
                               0,
                               0,
                               0,
                               w,
                               h,
                     GL_BGRA_EXT,
                GL_UNSIGNED_BYTE,
                            data);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionTextureManager::RemoveFromGraphics(const AUInt32 openGLId)
{
    GLuint* ids = new GLuint[1];
    ids[0] = openGLId;
    glDeleteTextures(1, ids);
}
//---------------------------------------------------------------------------------------
