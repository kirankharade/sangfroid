//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CFixedFunctionGraphics.h"
#include "CFixedFunctionDrawManager.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "../CLight.h"
#include "../CMaterial.h"
#include "../CLightManager.h"
#include "../CViewport.h"
#include "../Dependencies/glew/GL/glew.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CFixedFunctionGraphics::CFixedFunctionGraphics()
    : IGraphics()
      , m_pDrawManager(nullptr)
      , m_pFrontFaceMaterial(new CMaterial())
      , m_pBackFaceMaterial(new CMaterial())
      , m_cameraViewMatrix(CMatrix4f())
      , m_projectionMatrix(CMatrix4f())
      , m_modelWorldMatrix(CMatrix4f())
{
    m_pDrawManager = new CFixedFunctionDrawManager(this);
}
//---------------------------------------------------------------------------------------

CFixedFunctionGraphics::~CFixedFunctionGraphics()
{
    SAFE_CLEANUP(m_pDrawManager);
}
//---------------------------------------------------------------------------------------

EOpenGLEngineType CFixedFunctionGraphics::Type() const
{ 
    return EOpenGLEngineType_FixedFunction;
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::DefaultSetUp(IViewport* pViewport)
{
    GLenum err = glGetError();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);

	glEnable(GL_NORMALIZE);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
    glDisable(GL_CULL_FACE);

	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);

    err = glGetError();

	DefaultLightAndMaterial(pViewport);

    err = glGetError();
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::DefaultLightAndMaterial(IViewport* pViewport)
{
    ApplyMaterialToFrontFace(&(Defaults::FrontMaterial));
	ApplyMaterialToBackFace(&(Defaults::BackMaterial));

    SetAmbientLight(CColor(0.2f, 0.2f, 0.2f));

	CLightManager* lm = pViewport->LightManager();

	CLight* l = lm->AddLight();
    if(l)
    {
	    l->SetAmbient(Defaults::LightAmbient);
	    l->SetDiffuse(Defaults::LightDiffuse);
	    l->SetSpecular(Defaults::LightSpecular);
	    l->SetSpotDirection(Defaults::LightSpotDirection);
	    l->SetType(ELightType_GeneralDirectionalLight);
        SetLight(l);
	    l->Enable();
        EnableLighting();
	    AInt16 index = l->Index();
    }
}
//---------------------------------------------------------------------------------------

IDrawManager* CFixedFunctionGraphics::DrawManager()
{
    return m_pDrawManager;
}
//---------------------------------------------------------------------------------------

AUInt16 CFixedFunctionGraphics::MaxLightCount() const
{
	return (AUInt16) GL_MAX_LIGHTS;
}
//---------------------------------------------------------------------------------------

bool CFixedFunctionGraphics::SetLight(CLight* light)
{
    GLenum err = glGetError();

	if(light == nullptr)
	{
		return false;
	}
	AInt16 index = light->Index();
	if(index < 0 || index >= GL_MAX_LIGHTS)
	{
		return false;
	}
	GLenum lightIndex = GL_LIGHT0 + index;

	bool bIsEnabled = glIsEnabled(lightIndex);
	glDisable(lightIndex);

	SColorRGBAf a = (light->Ambient()).ToRGBAf();
	SColorRGBAf d = (light->Diffuse()).ToRGBAf();
	SColorRGBAf s = (light->Specular()).ToRGBAf();
	CPoint3f pos = light->Position();
	CVector3f vec = light->SpotDirection();

	GLfloat constantAttenuation = light->ConstantAttenuation();
	GLfloat linearAttenuation = light->LinearAttenuation();
	GLfloat quadraticAttenuation = light->QuadraticAttenuation();

	GLfloat ambient[] = { a.r, a.g, a.b, a.a };
	GLfloat diffuse[] = { d.r, d.g, d.b, d.a };
	GLfloat specular[] = { s.r, s.r, s.b, s.a };
	GLfloat position[] = { pos.x, pos.y, pos.z, 1.0f };
	GLfloat direction[] = { -vec.x, -vec.y, -vec.z, 0.0f };
	GLfloat spotDirection[] = { -vec.x, -vec.y, -vec.z};
	//GLfloat direction[] = { vec.x, vec.y, vec.z };

	glLightfv(lightIndex, GL_AMBIENT, ambient);
	glLightfv(lightIndex, GL_DIFFUSE, diffuse);
	glLightfv(lightIndex, GL_SPECULAR, specular);

	glLightfv(lightIndex, GL_CONSTANT_ATTENUATION, &constantAttenuation);
	glLightfv(lightIndex, GL_LINEAR_ATTENUATION, &linearAttenuation);
	glLightfv(lightIndex, GL_QUADRATIC_ATTENUATION, &quadraticAttenuation);

	ELightType type = light->Type();

	if(ELightType_GeneralDirectionalLight == type)
	{
		glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 0);
		glLightfv(lightIndex, GL_POSITION, direction);
		glLightf(lightIndex, GL_SPOT_EXPONENT, 0.0f);
		glLightf(lightIndex, GL_SPOT_CUTOFF, 180.0f);
	}
	else if(ELightType_SpotDirectionalLight == type)
	{
		glLightfv(lightIndex, GL_POSITION, position);
		glLightfv(lightIndex, GL_SPOT_DIRECTION, spotDirection);
		glLightf(lightIndex, GL_SPOT_EXPONENT, light->SpotExponent());
		glLightf(lightIndex, GL_SPOT_CUTOFF, light->SpotCutOffAngleDeg());
	}
	else if(ELightType_PointLight == type)
	{
		glLightfv(lightIndex, GL_POSITION, position);
		glLightf(lightIndex, GL_SPOT_EXPONENT, 0.0f);
		glLightf(lightIndex, GL_SPOT_CUTOFF, 180.0f);
	}

	if(bIsEnabled)
	{
		glEnable(lightIndex);
	}
    
    err = glGetError();

	return true;
}
//---------------------------------------------------------------------------------------

bool CFixedFunctionGraphics::UnsetLight(CLight* light)
{
	if(light == nullptr)
	{
		return false;
	}
	AInt16 index = light->Index();
	if(index < 0 || index >= GL_MAX_LIGHTS)
	{
		return false;
	}
	GLenum lightIndex = GL_LIGHT0 + index;

	glDisable(lightIndex);

	return true;
}
//---------------------------------------------------------------------------------------

bool CFixedFunctionGraphics::EnableLight(CLight* light)
{
	if(light == nullptr)
	{
		return false;
	}
	AInt16 index = light->Index();
	if(index < 0 || index >= GL_MAX_LIGHTS)
	{
		return false;
	}
	GLenum lightIndex = GL_LIGHT0 + index;
	glEnable(lightIndex);
	return true;
}
//---------------------------------------------------------------------------------------

bool CFixedFunctionGraphics::DisableLight(CLight* light)
{
	if(light == nullptr)
	{
		return false;
	}
	AInt16 index = light->Index();
	if(index < 0 || index >= GL_MAX_LIGHTS)
	{
		return false;
	}
	GLenum lightIndex = GL_LIGHT0 + index;
	glDisable(lightIndex);
	return true;
}
//---------------------------------------------------------------------------------------

bool CFixedFunctionGraphics::IsLightEnabled(const AInt16 index) const
{
	if(index < 0 || index >= GL_MAX_LIGHTS)
	{
		return false;
	}
	GLenum lightIndex = GL_LIGHT0 + index;
	return (bool) glIsEnabled(lightIndex);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::EnableLighting()
{
	glEnable(GL_LIGHTING);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::DisableLighting()
{
	glDisable(GL_LIGHTING);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::ApplyMaterialToFrontFace(const CMaterial* material)
{
	if(!material)
	{
		return;
	}
    GLenum err = glGetError();
	SColorRGBAf a = (material->Ambient()).ToRGBAf();
	SColorRGBAf d = (material->Diffuse()).ToRGBAf();
	SColorRGBAf s = (material->Specular()).ToRGBAf();
	SColorRGBAf e = (material->Emissive()).ToRGBAf();
	AReal32 shininess = material->Shininess();

	GLfloat ambient[] = { a.r, a.g, a.b, a.a };
	GLfloat diffuse[] = { d.r, d.g, d.b, d.a };
	GLfloat specular[] = { s.r, s.g, s.b, s.a };
	GLfloat emissive[] = { e.r, e.g, e.b, e.a };

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_EMISSION, emissive);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);

    err = glGetError();
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::ApplyMaterialToBackFace(const CMaterial* material)
{
	if(!material)
	{
		return;
	}
    GLenum err = glGetError();

	SColorRGBAf a = (material->Ambient()).ToRGBAf();
	SColorRGBAf d = (material->Diffuse()).ToRGBAf();
	SColorRGBAf s = (material->Specular()).ToRGBAf();
	SColorRGBAf e = (material->Emissive()).ToRGBAf();
	AReal32 shininess = material->Shininess();

	GLfloat ambient[] = { a.r, a.g, a.b, a.a };
	GLfloat diffuse[] = { d.r, d.g, d.b, d.a };
	GLfloat specular[] = { s.r, s.g, s.b, s.a };
	GLfloat emissive[] = { e.r, e.g, e.b, e.a };

	glMaterialfv(GL_BACK, GL_AMBIENT, ambient);
	glMaterialfv(GL_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_BACK, GL_SPECULAR, specular);
	glMaterialfv(GL_BACK, GL_EMISSION, emissive);
	glMaterialf(GL_BACK, GL_SHININESS, shininess);
    err = glGetError();
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::ApplyTransformation(const ETransformationType type, const Maths::CMatrix4f& matrix)
{
    CMatrix4f mat = matrix;
    mat = mat.transpose();
    GLenum err = glGetError();

    switch(type)
    {
        case ETransformationType_CameraView:
        case ETransformationType_ModelWorld:
            {
                if(type == ETransformationType_CameraView)
                {
                    m_cameraViewMatrix = mat;
                }
                else if(type == ETransformationType_ModelWorld)
                {
                    m_modelWorldMatrix = mat;
                }
                glMatrixMode(GL_MODELVIEW);
                glLoadMatrixf(m_cameraViewMatrix.m);
                //UpdateClipPlanes();
                glMultMatrixf(m_modelWorldMatrix.m);
                break;
            }
        case ETransformationType_Projection:
            {
                m_projectionMatrix = mat;
                glMatrixMode(GL_PROJECTION);
                glLoadMatrixf(m_projectionMatrix.m);
                break;
            }
        default:
            break;
    }
    err = glGetError();

    GLfloat mp[16], mv[16];

    glGetFloatv(GL_MODELVIEW_MATRIX, mv);
    glGetFloatv(GL_PROJECTION_MATRIX, mp);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::SetViewport(const IViewport* viewport)
{
    if(viewport)
    {
        int w = viewport->Width();
        int h = viewport->Height();
        glViewport(0, 0, w, h);
    } 
}
//---------------------------------------------------------------------------------------

void CFixedFunctionGraphics::SetAmbientLight(const CColor& ambientColor)
{
    GLenum err = glGetError();
    SColorRGBAf c = ambientColor.ToRGBAf();
	GLfloat data[4] = {c.r, c.g, c.b, c.a};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, data);
    err = glGetError();
}
//---------------------------------------------------------------------------------------
