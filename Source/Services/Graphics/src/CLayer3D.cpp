//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CGraphicsUtils.h"
#include "FileUtils/CStringUtils.h"
#include "CLayer3D.h"
#include "Scene/ISceneNode.h"
#include "Scene/C3DScene.h"
#include "CCamera.h"
#include "IGraphics.h"
#include "IFactory.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CLayer3D::CLayer3D(IViewport* viewport)
    : ILayer(viewport)
    , m_pCamera(nullptr)
{
    m_name = std::string("3DLayer-") + CStringUtils::ToString(m_id);
    m_pCamera = new CCamera(viewport);
}
//---------------------------------------------------------------------------------------

CLayer3D::~CLayer3D()
{
    SAFE_CLEANUP(m_pCamera);
}
//---------------------------------------------------------------------------------------

void CLayer3D::Render()
{
    PreRenderOperations();
    SetCameraForRendering();

    const std::vector<ISceneNode*>& opaqueNodes = GetRenderableNodes(ESceneNodeType_Solid);
    unsigned int opaqueNodeCount = opaqueNodes.size();
    for(unsigned int i = 0; i < opaqueNodeCount; i++)
    {
        ISceneNode* node = opaqueNodes[i];
        node->Render();
    }

    const std::vector<ISceneNode*>& transparentNodes = GetRenderableNodes(ESceneNodeType_Transparent);
    unsigned int transparentNodeCount = transparentNodes.size();
    for(unsigned int i = 0; i < transparentNodeCount; i++)
    {
        ISceneNode* node = transparentNodes[i];
        node->Render();
    }

    const std::vector<ISceneNode*>& textNodes = GetRenderableNodes(ESceneNodeType_Text);
    unsigned int textNodeCount = textNodes.size();
    for(unsigned int i = 0; i < textNodeCount; i++)
    {
        ISceneNode* node = textNodes[i];
        node->Render();
    }

    PostRenderOperations();
}
//---------------------------------------------------------------------------------------

CCamera* CLayer3D::Camera() const
{
    return m_pCamera;
}
//---------------------------------------------------------------------------------------

void CLayer3D::Resize(const AUInt32 width, const AUInt32 height)
{
    if(m_pCamera)
    {
        m_pCamera->SetAspectRatio(height, width);
    }
}
//---------------------------------------------------------------------------------------

void CLayer3D::SetCameraForRendering()
{
    CMatrix4f projectionMatrix = m_pCamera->ProjectionMatrix();
    CMatrix4f modelviewMatrix = m_pCamera->ViewMatrix();
    IGraphics* g = IGraphics::Gfx();
    g->ApplyTransformation(ETransformationType_Projection, projectionMatrix);
    g->ApplyTransformation(ETransformationType_CameraView, modelviewMatrix);
}
//---------------------------------------------------------------------------------------

void CLayer3D::SetSceneLightingAndMaterial()
{
    C3DScene* scene = dynamic_cast<C3DScene*> (m_pScene);
    if(!scene)
    {
        return;
    }

    IGraphics* g = IGraphics::Gfx();
    if(g)
    {
        const CMaterial& mat = scene->SceneDefaultMaterial();
        const CColor& ambientColor = scene->SceneAmbientColor();

        g->ApplyMaterialToFrontFace(&mat);
        g->SetAmbientLight(ambientColor);
    }
}
//---------------------------------------------------------------------------------------

void CLayer3D::PreRenderOperations()
{
    ILayer::PreRenderOperations();

}
//---------------------------------------------------------------------------------------

void CLayer3D::PostRenderOperations()
{
    ILayer::PostRenderOperations();
}
//---------------------------------------------------------------------------------------

void CLayer3D::FitToScene()
{
    if(!m_pScene || !m_pCamera)
    {
        return;
    }

    C3DScene* scene = dynamic_cast<C3DScene*> (m_pScene);
    CAxisOrientedBox bbox = scene->Bounds();
    AReal32 sceneRadius = bbox.getMinCorner().dist(bbox.getMaxCorner());
    m_pCamera->FitToScene(bbox.getCentre(), sceneRadius);
}
//---------------------------------------------------------------------------------------

void CLayer3D::InitializeForScene()
{
    if(!m_pScene || !m_pCamera)
    {
        return;
    }

    C3DScene* scene = dynamic_cast<C3DScene*> (m_pScene);
    CAxisOrientedBox bbox = scene->Bounds();
    AReal32 sceneRadius = bbox.getMinCorner().dist(bbox.getMaxCorner());
    m_pCamera->InitializeForScene(bbox.getCentre(), sceneRadius);
}
//---------------------------------------------------------------------------------------
