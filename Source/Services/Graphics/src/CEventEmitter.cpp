//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CEventEmitter.h"
#include "CEventListener.h"
#include <map>
#include <vector>

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------
typedef std::map<const std::string, std::vector<CEventListener*> > EventListenerMap;
typedef std::map<const std::string, std::vector<CEventListener*> >::iterator EventListenerMapItr;
typedef std::map<const std::string, std::vector<CEventListener*> >::const_iterator EventListenerMapConstItr;
//---------------------------------------------------------------------------------------

CEventEmitter::CEventEmitter()
	:m_pEventListenerMap(nullptr)
{
	m_pEventListenerMap = new EventListenerMap();
}
//---------------------------------------------------------------------------------------

CEventEmitter::~CEventEmitter()
{
	if(m_pEventListenerMap)
	{
		SAFE_CLEANUP(m_pEventListenerMap);
	}
}
//---------------------------------------------------------------------------------------

static void RemoveFromList(std::vector<CEventListener*>& list, CEventListener* listener)
{
	std::vector<CEventListener*>::iterator itr = list.begin();
	for(; itr != list.end(); itr++)
	{
		if(*itr == listener)
		{
			list.erase(itr);
			return;
		}
	}
}
//---------------------------------------------------------------------------------------

static bool Contains(const std::vector<CEventListener*>& list, CEventListener* listener)
{
	std::vector<CEventListener*>::const_iterator itr = list.begin();
	for(; itr != list.end(); itr++)
	{
		if(*itr == listener)
		{
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------------------

void CEventEmitter::Register(CEventListener *listener, const std::string & eventName)
{
	if(nullptr == listener)
	{
		return;
	}
	EventListenerMap* m = (EventListenerMap*) m_pEventListenerMap;
	EventListenerMapItr itr = m->find(eventName);

	if(itr != m->end())
	{
		if(!Contains((itr->second), listener))
		{
			(itr->second).push_back(listener);
		}
	}
	else
	{
		std::vector<CEventListener*> list;
		list.push_back(listener);
		m->insert(std::make_pair(eventName, list));
	}
}
//---------------------------------------------------------------------------------------

void CEventEmitter::UnRegister(CEventListener *listener, const std::string & eventName)
{
	EventListenerMap* m = (EventListenerMap*) m_pEventListenerMap;

	EventListenerMapItr itr = m->find(eventName);
	if(itr == m->end())
	{
		return;
	}
	RemoveFromList(itr->second, listener);
}
//---------------------------------------------------------------------------------------

void CEventEmitter::Emit(const std::string & eventName, void* userData)
{
	EventListenerMap* m = (EventListenerMap*) m_pEventListenerMap;

	EventListenerMapItr mitr = m->find(eventName);
	if(mitr == m->end())
	{
		return;
	}

	std::vector<CEventListener*>& list = mitr->second;
	std::vector<CEventListener*>::iterator vitr = list.begin();

	for(; vitr != list.end(); vitr++)
	{
		CEventListener* listener = *vitr;
		if(nullptr != listener)
		{
			listener->Listen(this, eventName, userData);
		}
	}
}
//---------------------------------------------------------------------------------------

