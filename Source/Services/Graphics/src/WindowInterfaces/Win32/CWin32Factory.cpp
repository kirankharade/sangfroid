//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CWin32Factory.h"
#include "CWin32Window.h"
#include "CWin32GLInitializer.h"
#include "../../GraphicsIncludes.h"
#include "../../../Base/Namespaces.h"
#include "../../IEventProcessor.h"
#include "../../IViewport.h"
#include "../../SEvent.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_

//---------------------------------------------------------------------------------------

CWin32Factory::CWin32Factory()
{
}
//---------------------------------------------------------------------------------------

CWin32Factory::~CWin32Factory()
{
}
//---------------------------------------------------------------------------------------

void CWin32Factory::Initialize(WindowID windowID)
{
    auto hDC = GetDC((HWND) windowID);
    auto hglrc = CWin32GLInitializer::Context(hDC);
}
//---------------------------------------------------------------------------------------

void CWin32Factory::Initialize(WindowID windowID, SGLContextParams& contextParams)
{
    auto hDC = GetDC((HWND) windowID);
    auto hglrc = CWin32GLInitializer::Context(hDC, contextParams);
}
//---------------------------------------------------------------------------------------

//IWindow* CWin32Factory::Window(const SGLContextParams& contextParams, 
//                               WindowID existingWindowID) const
//{
//    return new CWin32Window(contextParams, existingWindowID);
//}
////---------------------------------------------------------------------------------------
//
//IWindow* CWin32Factory::Window(const SGLContextParams& contextParams, WindowID parentID, 
//                               int xPosition, int yPosition, int width, int height) const
//{
//    return new CWin32Window(contextParams, parentID, xPosition, yPosition, width, height);
//}
////---------------------------------------------------------------------------------------
//
IWindow* CWin32Factory::Window(const SGLContextParams& contextParams, int width, int height) const
{
    return new CWin32Window(contextParams, nullptr);
}
////---------------------------------------------------------------------------------------

IWindow* CWin32Factory::Window(const SGLContextParams& contextParams, WindowID windowID) const
{
    return new CWin32Window(contextParams, windowID);
}
//---------------------------------------------------------------------------------------

IWindow* CWin32Factory::Window(WindowID windowID) const
{
    return new CWin32Window(windowID);
}
//---------------------------------------------------------------------------------------


#endif //_AN_BUILD_GRAPHICS_FOR_WINDOWS_
