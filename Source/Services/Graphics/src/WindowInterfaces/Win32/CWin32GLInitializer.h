//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_WIN32_GL_INITIALIZER_H__
#define __I_WIN32_GL_INITIALIZER_H__
//---------------------------------------------------------------------------------------
#include <Windows.h>
#include "../../GraphicsIncludes.h"
#include "../../GraphicsDefines.h"
#include "../../SGLContextParams.h"
#include "../../IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
//---------------------------------------------------------------------------------------

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_

class AN_GRAPHICS_API CWin32GLInitializer
{

private:

    CWin32GLInitializer();

    CWin32GLInitializer(const CWin32GLInitializer & );

    void operator=(const CWin32GLInitializer & );

    ~CWin32GLInitializer();

    static void CreateContext(HDC hDC, SGLContextParams& contextParams);

    static const SGLContextParams& ContextParams();

public:

    static HGLRC Context(HDC hDC, SGLContextParams& contextParams);

    static HGLRC Context(HDC hDC);

private:

    static HGLRC            m_hGLRC;
    static int              m_majorOpenGLVersion;
    static int              m_minorOpenGLVersion;
    static SGLContextParams m_contextParams;
};

#endif //_AN_BUILD_GRAPHICS_FOR_WINDOWS_

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__I_WIN32_GL_INITIALIZER_H__
//---------------------------------------------------------------------------------------
