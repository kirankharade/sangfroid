//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CWin32GLInitializer.h"
#include "../../GraphicsIncludes.h"
#include "../../../Base/Namespaces.h"
#include "../../IEventProcessor.h"
#include "../../IViewport.h"
#include "../../SEvent.h"
#include "../../Dependencies/glew/GL/glew.h"
#include "../../Dependencies/glew/GL/wglew.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_

//---------------------------------------------------------------------------------------
HGLRC CWin32GLInitializer::m_hGLRC = nullptr;
int CWin32GLInitializer::m_majorOpenGLVersion = 2;
int CWin32GLInitializer::m_minorOpenGLVersion = 0;
SGLContextParams CWin32GLInitializer::m_contextParams = SGLContextParams();
//---------------------------------------------------------------------------------------

CWin32GLInitializer::CWin32GLInitializer()
{
}
//---------------------------------------------------------------------------------------

CWin32GLInitializer::~CWin32GLInitializer()
{
    BOOL status = wglDeleteContext(m_hGLRC);
    if(status != TRUE)
    {
        throw std::exception("Error while cleaning up rendering context.");
    }
}
//---------------------------------------------------------------------------------------

//To be called first time only during the OpenGL initialization.

HGLRC CWin32GLInitializer::Context(HDC hDC, SGLContextParams& contextParams)
{
    if(nullptr == m_hGLRC)
    {
        CreateContext(hDC, contextParams);
    }
    return m_hGLRC;
}
//---------------------------------------------------------------------------------------

//To be called for every new window initialization for window.

HGLRC CWin32GLInitializer::Context(HDC hDC)
{
    if(nullptr == m_hGLRC)
    {
        CreateContext(hDC, m_contextParams);
    }
    return m_hGLRC;
}
//---------------------------------------------------------------------------------------

const SGLContextParams& CWin32GLInitializer::ContextParams()
{
    return m_contextParams;
}
//---------------------------------------------------------------------------------------

void CWin32GLInitializer::CreateContext(HDC hDC, SGLContextParams& contextParams)
{

    PIXELFORMATDESCRIPTOR pfd;
    memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW; //This context draws to window
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 16; //32
    pfd.cDepthBits = 24; //32
    pfd.iLayerType = PFD_MAIN_PLANE;

    int nPixelFormat = ChoosePixelFormat(hDC, &pfd);
    if (nPixelFormat == 0)
    {
        throw std::exception("Cannot create OpenGL context.");
    }

    bool status = SetPixelFormat(hDC, nPixelFormat, &pfd);
    if (!status)
    {
        throw std::exception("Cannot create OpenGL context.");
    }

    //First create the temporory opengl context using default basic OpenGL 2.1 fixed function context
    //This will be used to intialize glew...
    HGLRC tmpContext = wglCreateContext(hDC);
    wglMakeCurrent(hDC, tmpContext);

    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        //God may help us!
        throw std::exception("Cannot create OpenGL context.");
    }

    //First we will try to get OpenGL 4 context...
    int settings40[] = 
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
        WGL_CONTEXT_MINOR_VERSION_ARB, 0,
        WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, // Set our OpenGL context to be forward compatible
        0
    };

    int settings32[] = 
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
        WGL_CONTEXT_MINOR_VERSION_ARB, 2,
        WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
        0
    };

    if (wglewIsSupported("WGL_ARB_create_context") == 1)
    { 
        if(contextParams.m_openGLEngineType == EOpenGLEngineType_Core_4)
        {
            m_hGLRC = wglCreateContextAttribsARB(hDC, NULL, settings40);
            if(m_hGLRC == nullptr)
            {
                m_hGLRC = wglCreateContextAttribsARB(hDC, NULL, settings32);
                contextParams.m_openGLEngineType = EOpenGLEngineType_Core_3;
            }
            wglMakeCurrent(NULL, NULL);
            wglDeleteContext(tmpContext);
            wglMakeCurrent(hDC, m_hGLRC);
        }
        else if(contextParams.m_openGLEngineType == EOpenGLEngineType_Core_3)
        {
            m_hGLRC = wglCreateContextAttribsARB(hDC, NULL, settings32);
            if(m_hGLRC == nullptr)
            {
                m_hGLRC = tmpContext;
                contextParams.m_openGLEngineType = EOpenGLEngineType_FixedFunction;
            }
            else
            {
                wglMakeCurrent(NULL, NULL);
                wglDeleteContext(tmpContext);
                wglMakeCurrent(hDC, m_hGLRC);
            }
        }
        else
        {
            m_hGLRC = tmpContext;
            contextParams.m_openGLEngineType = EOpenGLEngineType_FixedFunction;
        }
    }
    else
    {
        m_hGLRC = tmpContext;
        contextParams.m_openGLEngineType = EOpenGLEngineType_FixedFunction;
    }

    //Get back the versions finally set...
    int glVersion[2] = {-1, -1};
    glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
    glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);

    m_majorOpenGLVersion = glVersion[0];
    m_minorOpenGLVersion = glVersion[1];

    m_contextParams = contextParams;
}
//---------------------------------------------------------------------------------------

#endif //_AN_BUILD_GRAPHICS_FOR_WINDOWS_
