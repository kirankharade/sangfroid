//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_WIN32_WINDOW_H__
#define __I_WIN32_WINDOW_H__
//---------------------------------------------------------------------------------------
#include <Windows.h>
#include "../../GraphicsIncludes.h"
#include "../../GraphicsDefines.h"
#include "../../SGLContextParams.h"
#include "../../IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
//---------------------------------------------------------------------------------------

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_

class AN_GRAPHICS_API CWin32Window : public IWindow
{

public:

    CWin32Window(const SGLContextParams& contextParams, WindowID existingWindowID = NULL);

    CWin32Window(WindowID existingWindowID);

    CWin32Window(const SGLContextParams& contextParams);

    virtual ~CWin32Window();

    virtual IViewport* Viewport() const;
    virtual void SetViewport(IViewport* pViewPort);

    virtual WindowID Id() const;

    virtual WindowID ParentWindowID() const;
    virtual void SetParentWindow(WindowID parentWindowID);

    virtual void SetVisible(const bool bVisible);
    virtual bool IsVisible() const;

    virtual int Height() const;
    virtual int Width() const;
    virtual void SetSize(const int width, const int height);

    virtual int XPosition() const;
    virtual int YPosition() const;
    virtual void SetPosition(const int xPosition, const int yPosition);

    virtual void SetFocussed();
    virtual bool IsFocussed() const;

    void MakeCurrent();
    void SwapBuffers();

    void MoveWindow(int xPosition, int yPosition, int width, int height, bool bRepaint = true);
    void SetWindowPosition(WindowID insertAfter, int xPosition, int yPosition, int width, int height, UINT nFlags);

    virtual WindowID CreateEx(DWORD dwExStyle, 
                              char* lpszClassName,
                              char* lpszWindowName,
                              DWORD dwStyle,
                              const RECT& rect,
                              HWND hParentWnd,
                              UINT nID);

protected:

    virtual void Init();

    virtual void OnPaint();

    virtual void OnLeftButtonDoubleClick(const SEvent& ev);
    virtual void OnLeftButtonUp(const SEvent& ev);
    virtual void OnLeftButtonDown(const SEvent& ev);

    virtual void OnRightButtonDoubleClick(const SEvent& ev);
    virtual void OnRightButtonDown(const SEvent& ev);
    virtual void OnRightButtonUp(const SEvent& ev);

    virtual void OnMiddleButtonDoubleClick(const SEvent& ev);
    virtual void OnMiddleButtonDown(const SEvent& ev);
    virtual void OnMiddleButtonUp(const SEvent& ev);

    virtual void OnMouseMove(const SEvent& ev);
    virtual void OnMouseWheel(const SEvent& ev);
    virtual void OnMouseHover(const SEvent& ev);
    virtual void OnMouseLeave(const SEvent& ev);
    virtual void OnMouseEnter(const SEvent& ev);

    virtual void OnKey(const SEvent& ev);

    virtual void OnSize(const SEvent& ev);

    virtual LRESULT WinProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
    LRESULT OnEraseBackGround(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

    bool CanTrackMouse() const;

private:
    static LRESULT CALLBACK Wnd32Proc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
    char* RegisterWindowClass(UINT nClassStyle,HCURSOR hCursor, HBRUSH hbrBackground, HICON hIcon);

protected:
    WindowID                m_hWnd;
    WindowID                m_hParentWnd;
    IViewport*              m_pViewport;
    bool                    m_bShouldTrackMouse;
    IGraphicsContext*       m_pGraphicsContext;
    SGLContextParams        m_contextParams;
    HDC                     m_hDC;
    WNDPROC                 m_originalWinproc;
    HGLRC                   m_HGLRC;
};

#endif //_AN_BUILD_GRAPHICS_FOR_WINDOWS_

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__I_WIN32_WINDOW_H__
//---------------------------------------------------------------------------------------
