//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_WIN32_FACTORY_H__
#define __I_WIN32_FACTORY_H__
//---------------------------------------------------------------------------------------
#include "../../GraphicsIncludes.h"
#include "../../GraphicsDefines.h"
#include "../../SGLContextParams.h"
#include "../../IFactory.h"
#include "../../IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IViewport;
//---------------------------------------------------------------------------------------

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_

class CWin32Factory : public IFactory
{

public:

    CWin32Factory();

    virtual ~CWin32Factory();

    virtual IWindow* Window(const SGLContextParams& contextParams, WindowID windowID) const;

    virtual IWindow* Window(WindowID windowID) const;

	IWindow* Window(const SGLContextParams& contextParams, int width, int height) const;

    void Initialize(WindowID windowID);

    void Initialize(WindowID windowID, SGLContextParams& contextParams);

};

#endif //_AN_BUILD_GRAPHICS_FOR_WINDOWS_

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__I_WIN32_FACTORY_H__
//---------------------------------------------------------------------------------------
