//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CWin32Window.h"
#include "CWin32GLInitializer.h"
#include "../../GraphicsIncludes.h"
#include "../../../Base/Namespaces.h"
#include "../../IEventProcessor.h"
#include "../../IViewport.h"
#include "../../SEvent.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

#ifdef _AN_BUILD_GRAPHICS_FOR_WINDOWS_
//---------------------------------------------------------------------------------------
static int s_windowCounter = 0;
static char s_className[255] = {0};
static HWND s_capturedWindowHandle = nullptr;
//---------------------------------------------------------------------------------------

CWin32Window::CWin32Window(const SGLContextParams& contextParams, WindowID existingWindowID)
: m_contextParams(contextParams),
  m_hWnd((HWND) existingWindowID), 
  m_hParentWnd(nullptr), 
  m_pViewport(nullptr), 
  m_bShouldTrackMouse(false),
  m_pGraphicsContext(nullptr),
  m_hDC(nullptr),
  m_HGLRC(nullptr)
{
    if(m_hWnd == nullptr)
    {
		RECT rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = 500;
		rect.bottom = 400;

		HINSTANCE hInstance = GetModuleHandle(nullptr);
		//DWORD dwStyle = (HWND) m_hParentWnd ? WS_CHILD | WS_VISIBLE  : WS_OVERLAPPED | WS_VISIBLE;
		DWORD dwStyle = (HWND)m_hParentWnd ? WS_CHILD : WS_OVERLAPPED;
		m_hWnd = CreateEx(0, RegisterWindowClass(CS_DBLCLKS, nullptr, nullptr, nullptr), nullptr, dwStyle, rect, (HWND)m_hParentWnd, 0);
	}
    Init();
}
//---------------------------------------------------------------------------------------

CWin32Window::CWin32Window(WindowID existingWindowID)
: m_contextParams(SGLContextParams()),
  m_hWnd((HWND) existingWindowID), 
  m_hParentWnd(nullptr), 
  m_pViewport(nullptr), 
  m_bShouldTrackMouse(false),
  m_pGraphicsContext(nullptr),
  m_hDC(nullptr),
  m_HGLRC(nullptr)
{
    if(m_hWnd == nullptr)
    {
        throw std::exception("Invalid window handle provided.");
    }
    Init();
}
//---------------------------------------------------------------------------------------

CWin32Window::CWin32Window(const SGLContextParams& contextParams)
: m_contextParams(contextParams),
  m_hWnd((HWND) nullptr), 
  m_hParentWnd(nullptr), 
  m_pViewport(nullptr), 
  m_bShouldTrackMouse(false),
  m_pGraphicsContext(nullptr)
{
    if(m_hWnd == nullptr)
    {
        RECT rect;
        rect.left = 0;
        rect.top = 0;
        rect.right = 500;
        rect.bottom = 400;

        HINSTANCE hInstance = GetModuleHandle(nullptr);
        //DWORD dwStyle = (HWND) m_hParentWnd ? WS_CHILD | WS_VISIBLE  : WS_OVERLAPPED | WS_VISIBLE;
        DWORD dwStyle = (HWND) m_hParentWnd ? WS_CHILD  : WS_OVERLAPPED;
        m_hWnd = CreateEx(0,RegisterWindowClass(CS_DBLCLKS, nullptr, nullptr, nullptr), nullptr, dwStyle ,rect, (HWND) m_hParentWnd,0);
    }
    Init();
}
//---------------------------------------------------------------------------------------

void CWin32Window::Init()
{
    //Store this pointer as user data with m_hWnd
    SetWindowLongPtr((HWND) m_hWnd, GWLP_USERDATA, (LONG_PTR)this);

    m_originalWinproc = (WNDPROC)SetWindowLongPtr((HWND) m_hWnd, GWLP_WNDPROC, (LONG_PTR) Wnd32Proc);

    m_hDC = GetDC((HWND) m_hWnd);

    m_HGLRC = CWin32GLInitializer::Context(m_hDC, m_contextParams);

    if(m_HGLRC == nullptr)
    {
        throw std::exception("Cannot create render context.");
    }

    MakeCurrent();
}
//---------------------------------------------------------------------------------------

CWin32Window::~CWin32Window()
{
    //Only for the created windows by CWin32Window
    //::DestroyWindow((HWND) m_hWnd);

   if (m_hDC && !ReleaseDC((HWND) m_hWnd, m_hDC))
   {
       throw std::exception("Error in releasing window resources.");
   }

   //Reset the winproc
   SetWindowLongPtr((HWND) m_hWnd, GWLP_WNDPROC, (LONG_PTR) m_originalWinproc);

   m_hDC = nullptr;
}
//---------------------------------------------------------------------------------------

IViewport* CWin32Window::Viewport() const
{
    return m_pViewport;
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetViewport(IViewport* pViewport)
{
    m_pViewport = pViewport;
}
//---------------------------------------------------------------------------------------

WindowID CWin32Window::Id() const
{
    return m_hWnd;
}
//---------------------------------------------------------------------------------------

WindowID CWin32Window::ParentWindowID() const
{
    return m_hParentWnd;
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetParentWindow(WindowID parentWindowID)
{
    HWND hParentWnd = reinterpret_cast<HWND>(parentWindowID);
    m_hParentWnd = hParentWnd;
    ::SetParent((HWND) m_hWnd, hParentWnd);
    RECT r;
    ::GetClientRect(hParentWnd, &r);
    ::MoveWindow((HWND) m_hWnd, r.left, r.top, (r.right - r.left), (r.bottom - r.top), 1);
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetVisible(const bool bVisible)
{
    int nCmdShow = bVisible ? SW_SHOW : SW_HIDE;
    ::ShowWindow((HWND) m_hWnd, nCmdShow);
    ::UpdateWindow((HWND) m_hWnd);
}
//---------------------------------------------------------------------------------------

bool CWin32Window::IsVisible() const
{
    return ((bool) ::IsWindowVisible((HWND) m_hWnd));
}
//---------------------------------------------------------------------------------------

int CWin32Window::Height() const
{
    RECT r;
    GetClientRect((HWND) (HWND) m_hWnd, &r);
    return (r.bottom - r.top);
}
//---------------------------------------------------------------------------------------

int CWin32Window::Width() const
{
    RECT r;
    GetClientRect((HWND) (HWND) m_hWnd, &r);
    return (r.right - r.left);
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetSize(const int width, const int height)
{
    RECT r;
    ::GetClientRect((HWND) m_hWnd, &r);
    ::MoveWindow((HWND) m_hWnd, r.left, r.top, width, height, 1);
}
//---------------------------------------------------------------------------------------

int CWin32Window::XPosition() const
{
    RECT r;
    ::GetClientRect((HWND) m_hWnd, &r);
    return r.left;
}
//---------------------------------------------------------------------------------------

int CWin32Window::YPosition() const
{
    RECT r;
    ::GetClientRect((HWND) m_hWnd, &r);
    return r.top;
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetPosition(const int xPosition, const int yPosition)
{
    RECT r;
    ::GetClientRect((HWND) m_hWnd, &r);
    int width = r.bottom - r.top;
    int height = r.right - r.left;
    ::MoveWindow((HWND) m_hWnd, r.left, r.top, width, height, 1);
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetFocussed()
{
    ::SetFocus((HWND) m_hWnd);
}
//---------------------------------------------------------------------------------------

bool CWin32Window::IsFocussed() const
{
    HWND hWnd = ::GetFocus();
    return (hWnd == (HWND) m_hWnd);
}
//---------------------------------------------------------------------------------------

void CWin32Window::MoveWindow(int xPosition, int yPosition, int width, int height, bool bRepaint)
{
    ::MoveWindow((HWND) m_hWnd, xPosition, yPosition, width, height, bRepaint ? 1 : 0);
}
//---------------------------------------------------------------------------------------

void CWin32Window::SetWindowPosition(WindowID insertAfter, int xPosition, int yPosition, int width, int height, UINT nFlags)
{
    ::SetWindowPos((HWND) m_hWnd, (HWND) insertAfter, xPosition, yPosition, width, height, nFlags);
}
//---------------------------------------------------------------------------------------

bool CWin32Window::CanTrackMouse() const
{ 
    return m_pViewport != NULL; 
}

void CWin32Window::MakeCurrent()
{
    wglMakeCurrent(m_hDC, m_HGLRC);
}
//---------------------------------------------------------------------------------------

void CWin32Window::SwapBuffers()
{
    ::SwapBuffers(m_hDC);
}
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
//Protected methods
//---------------------------------------------------------------------------------------

void CWin32Window::OnPaint()
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        SEvent sEvent;
        sEvent.type = EEventType_Paint;
        processor->OnEvent(sEvent);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnRightButtonDoubleClick(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnRightButtonDown(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnRightButtonUp(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMiddleButtonDoubleClick(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMiddleButtonDown(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMiddleButtonUp(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnLeftButtonDoubleClick(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnLeftButtonUp(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnLeftButtonDown(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMouseMove(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMouseWheel(const SEvent& ev)
{
    bool bResult = false;
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMouseHover(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMouseLeave(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnMouseEnter(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnKey(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }

    if(::IsWindow((HWND) m_hParentWnd))
    {
        EKeyCodeType key = ev.KeyEvent.key;
        if(ev.KeyEvent.control) key = (EKeyCodeType)(key | EKeyCodeType_CONTROL);
        if(ev.KeyEvent.shift) key = (EKeyCodeType)(key | EKeyCodeType_SHIFT);

        if(ev.KeyEvent.pressedDown)
            ::SendMessage((HWND) m_hParentWnd, WM_KEYDOWN, ev.KeyEvent.key, 0);
    }
}
//---------------------------------------------------------------------------------------

void CWin32Window::OnSize(const SEvent& ev)
{
    IEventProcessor* processor = nullptr;
    if(m_pViewport)
    {
        processor = m_pViewport->EventProcessor();
    }
    if(processor)
    {
        processor->OnEvent(ev);
    }
}
//---------------------------------------------------------------------------------------

#define WM_MOUSEENTER (WM_APP + 1)
//---------------------------------------------------------------------------------------
#define EVENT_ID_MOUSE_POLLING          50
#define MOUSE_POLLING_DELAY             150
//---------------------------------------------------------------------------------------

LRESULT CWin32Window::WinProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL 0x020A
#endif
#ifndef WHEEL_DELTA
#define WHEEL_DELTA 120
#endif

    SEvent ev;
    ev.type = EEventType_Mouse;
    ev.MouseEvent.x = (short)LOWORD(lParam);
    ev.MouseEvent.y = (short)HIWORD(lParam);
    ev.MouseEvent.shift = ((LOWORD(wParam) & MK_SHIFT) != 0);
    ev.MouseEvent.control = ((LOWORD(wParam) & MK_CONTROL) != 0);
    ev.MouseEvent.state = wParam & ( MK_LBUTTON | MK_RBUTTON);

    if (wParam & MK_MBUTTON)
    {
        ev.MouseEvent.state |= EMouseState_Middle;
    }

    ev.MouseEvent.wheelDelta = 0.0f;

    switch(iMsg)
    {
    case WM_PAINT :
        {
            PAINTSTRUCT ps;
            BeginPaint(hWnd, &ps);
            OnPaint();
            EndPaint(hWnd, &ps);
        }
        break;

    case WM_SIZE:
        {
            ev.type = EEventType_Resize;
            ev.ResizeEvent.Width = (short)LOWORD(lParam);
            ev.ResizeEvent.Height = (short)HIWORD(lParam);

            if((LOWORD(wParam) & SIZE_MAXHIDE) != 0)
            {
                ev.ResizeEvent.type = EResizeEvent_MaxHide;
            }
            else if((LOWORD(wParam) & SIZE_MAXIMIZED) != 0)
            {
                ev.ResizeEvent.type = EResizeEvent_Maximized;
            }
            else if((LOWORD(wParam) & SIZE_MAXSHOW) != 0)
            {
                ev.ResizeEvent.type = EResizeEvent_MaxShow;
            }
            else if((LOWORD(wParam) & SIZE_MINIMIZED) != 0)
            {
                ev.ResizeEvent.type = EResizeEvent_Minimized;
            }
            else if((LOWORD(wParam) & SIZE_RESTORED) != 0)
            {
                ev.ResizeEvent.type = EResizeEvent_Restored;
            }

            OnSize(ev);
        }
        break;
    case WM_RBUTTONDOWN:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_RightButtonDown;
        OnRightButtonDown(ev);
        break;
    case WM_RBUTTONUP:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_RightButtonUp;
        OnRightButtonUp(ev);
        break;
    case WM_RBUTTONDBLCLK:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_RightButtonDoubleClick;
        OnRightButtonDoubleClick(ev);
        break;
    case WM_MBUTTONDOWN:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_MiddleButtonDown;
        OnMiddleButtonDown(ev);
        break;
    case WM_MBUTTONUP:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_MiddleButtonUp;
        OnMiddleButtonUp(ev);
        break;
    case WM_MBUTTONDBLCLK:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_MiddleButtonDoubleClick;
        OnMiddleButtonDoubleClick(ev);
        break;
    case WM_LBUTTONDOWN:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_LeftButtonDown;
        OnLeftButtonDown(ev);
        break;
    case WM_LBUTTONUP:
        ev.type = EEventType_Mouse;
        ev.MouseEvent.type = EMouseEvent_LeftButtonUp;
        OnLeftButtonUp(ev);
        break;
    case WM_LBUTTONDBLCLK:
        ev.MouseEvent.type = EMouseEvent_LeftButtonDoubleClick;
        ev.type = EEventType_Mouse;
        OnLeftButtonDoubleClick(ev);
        break;
    case WM_MOUSEMOVE:
        {
            ev.type = EEventType_Mouse;
            ev.MouseEvent.type = EMouseEvent_MouseMove;
            OnMouseMove(ev);
            SetFocus((HWND) m_hWnd);

            if(!CanTrackMouse()) break;

            m_bShouldTrackMouse = true;
            RECT rc;
            POINT pt;
            ::GetClientRect((HWND) m_hWnd,&rc);
            pt.x = ev.MouseEvent.x;
            pt.y = ev.MouseEvent.y;

            if(PtInRect(&rc,pt))
            {
                SetTimer((HWND) m_hWnd,EVENT_ID_MOUSE_POLLING,MOUSE_POLLING_DELAY,NULL);
                if((HWND) m_hWnd != s_capturedWindowHandle)
                {
                    s_capturedWindowHandle = (HWND) m_hWnd;
                    PostMessage((HWND) m_hWnd,WM_MOUSEENTER,0,MAKELONG(pt.x,pt.y));
                }
                break;
            }

            KillTimer((HWND) m_hWnd,EVENT_ID_MOUSE_POLLING);
            PostMessage((HWND) m_hWnd,WM_MOUSELEAVE,0,MAKELONG(pt.x,pt.y));
        }
        break;
    case WM_TIMER:
        {
            if(!CanTrackMouse()) break;

            if(m_bShouldTrackMouse)
            {
                RECT rc;
                POINT pt;
                ::GetClientRect((HWND) m_hWnd,&rc);
                GetCursorPos(&pt);
                MapWindowPoints(NULL, (HWND) m_hWnd, &pt, 1 );
                if(PtInRect(&rc,pt))
                {
                    PostMessage((HWND) m_hWnd,WM_MOUSEHOVER,0,MAKELONG(pt.x,pt.y));
                    m_bShouldTrackMouse = false;
                    break;
                }

                KillTimer((HWND) m_hWnd,EVENT_ID_MOUSE_POLLING);
                PostMessage((HWND) m_hWnd,WM_MOUSELEAVE,0,MAKELONG(pt.x,pt.y));
                m_bShouldTrackMouse = false;
            }
        }
        break;

    case WM_MOUSEHOVER:
        {
            BYTE allKeys[256];
            GetKeyboardState(allKeys);
            ev.MouseEvent.shift = ((allKeys[VK_SHIFT] & 0x80)!=0);
            ev.MouseEvent.control = ((allKeys[VK_CONTROL] & 0x80)!=0);
            if(GetAsyncKeyState(VK_LBUTTON))
                ev.MouseEvent.state += EMouseState_Left;
            if(GetAsyncKeyState(VK_RBUTTON))
                ev.MouseEvent.state += EMouseState_Right;
            if(GetAsyncKeyState(VK_MBUTTON))
                ev.MouseEvent.state += EMouseState_Middle;

            ev.type = EEventType_Mouse;
            ev.MouseEvent.type = EMouseEvent_Hover;
            OnMouseHover(ev);
        }
        break;
    case WM_MOUSELEAVE:
        {
            BYTE allKeys[256];
            GetKeyboardState(allKeys);
            ev.MouseEvent.shift = ((allKeys[VK_SHIFT] & 0x80)!=0);
            ev.MouseEvent.control = ((allKeys[VK_CONTROL] & 0x80)!=0);
            if(GetAsyncKeyState(VK_LBUTTON))
                ev.MouseEvent.state += EMouseState_Left;
            if(GetAsyncKeyState(VK_RBUTTON))
                ev.MouseEvent.state += EMouseState_Right;
            if(GetAsyncKeyState(VK_MBUTTON))
                ev.MouseEvent.state += EMouseState_Middle;

            ev.type = EEventType_Mouse;
            ev.MouseEvent.type = EMouseEvent_Leave;

            ReleaseCapture();
            s_capturedWindowHandle = NULL;
            ::SetFocus(NULL);

            OnMouseLeave(ev);
        }
        break;
    case WM_MOUSEWHEEL:
        {
            POINT p;
            p.x = 0; p.y = 0;
            ClientToScreen(hWnd, &p);
            ev.MouseEvent.x -= p.x;
            ev.MouseEvent.y -= p.y;
            ev.MouseEvent.wheelDelta = ((float)((short)HIWORD(wParam))) / (float)WHEEL_DELTA;
            ev.type = EEventType_Mouse;
            ev.MouseEvent.type = EMouseEvent_MouseWheel;
            OnMouseWheel(ev);
        }
        break;
    case WM_MOUSEENTER:
        {
            //Reference: For Mouse enter and leave 
            //1. http://www.codeproject.com/KB/tips/gbTestSDI.aspx
            //2. http://www.codeproject.com/KB/GDI/mousecapture.aspx

            BYTE allKeys[256];
            GetKeyboardState(allKeys);
            ev.MouseEvent.shift = ((allKeys[VK_SHIFT] & 0x80)!=0);
            ev.MouseEvent.control = ((allKeys[VK_CONTROL] & 0x80)!=0);
            if(GetAsyncKeyState(VK_LBUTTON))
                ev.MouseEvent.state += EMouseState_Left;
            if(GetAsyncKeyState(VK_RBUTTON))
                ev.MouseEvent.state += EMouseState_Right;
            if(GetAsyncKeyState(VK_MBUTTON))
                ev.MouseEvent.state += EMouseState_Middle;

            ev.type = EEventType_Mouse;
            ev.MouseEvent.type = EMouseEvent_Enter;
            OnMouseEnter(ev);
        }
        break;
    case WM_SYSKEYDOWN:
    case WM_SYSKEYUP:
    case WM_KEYDOWN:
    case WM_KEYUP:
        {
            HKL KEYBOARD_INPUT_HKL=0;
            UINT KEYBOARD_INPUT_CODEPAGE = 1252;
            BYTE allKeys[256];

            ev.type = EEventType_KeyInput;
            ev.KeyEvent.key = (EKeyCodeType)wParam;
            ev.KeyEvent.pressedDown = (iMsg==WM_KEYDOWN || iMsg == WM_SYSKEYDOWN);

            const UINT MY_MAPVK_VSC_TO_VK_EX = 3;
            if ( ev.KeyEvent.key == EKeyCodeType_SHIFT )
            {
                ev.KeyEvent.key = (EKeyCodeType)MapVirtualKey( ((lParam>>16) & 255), MY_MAPVK_VSC_TO_VK_EX );
            }
            if ( ev.KeyEvent.key == EKeyCodeType_CONTROL )
            {
                ev.KeyEvent.key = (EKeyCodeType)MapVirtualKey( ((lParam>>16) & 255), MY_MAPVK_VSC_TO_VK_EX );
                if (lParam & 0x1000000)
                    ev.KeyEvent.key = EKeyCodeType_RCONTROL;
            }
            if ( ev.KeyEvent.key == EKeyCodeType_MENU )
            {
                ev.KeyEvent.key = (EKeyCodeType)MapVirtualKey( ((lParam>>16) & 255), MY_MAPVK_VSC_TO_VK_EX );
                if (lParam & 0x1000000)
                    ev.KeyEvent.key = EKeyCodeType_RMENU;
            }

            GetKeyboardState(allKeys);

            ev.KeyEvent.shift = ((allKeys[VK_SHIFT] & 0x80)!=0);
            ev.KeyEvent.control = ((allKeys[VK_CONTROL] & 0x80)!=0);

            WORD keyChars[2];
            UINT scanCode = HIWORD(lParam);
            int conversionResult = ToAsciiEx(wParam,scanCode,allKeys,keyChars,0,KEYBOARD_INPUT_HKL);
            if (conversionResult == 1)
            {
                WORD unicodeChar;
                MultiByteToWideChar(
                    KEYBOARD_INPUT_CODEPAGE,
                    MB_PRECOMPOSED,
                    (LPCSTR)keyChars,
                    sizeof(keyChars),
                    (WCHAR*)&unicodeChar,
                    1 );
                ev.KeyEvent.keyboardChar = unicodeChar; 
            }
            else
                ev.KeyEvent.keyboardChar = 0;

            // allow composing characters like '@' with Alt Gr on non-US keyboards
            if ((allKeys[VK_MENU] & 0x80) != 0)
                ev.KeyEvent.control = 0;

            OnKey(ev);
        }
        break;
    case WM_ERASEBKGND:
        {
            return OnEraseBackGround(hWnd, iMsg, wParam, lParam);
        }
    }

    return DefWindowProc(hWnd, iMsg, wParam, lParam);
}
//---------------------------------------------------------------------------------------

LRESULT CWin32Window::OnEraseBackGround(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	return 0L;
}
//---------------------------------------------------------------------------------------
//Private methods
//---------------------------------------------------------------------------------------

LRESULT CALLBACK CWin32Window::Wnd32Proc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    //if (iMsg == WM_NCCREATE)
    //{
    //    //Get window handle from lpCreateParams which is generally set in CreateWindow
    //    SetWindowLong(hWnd, GWL_USERDATA, (long)((LPCREATESTRUCT(lParam))->lpCreateParams));
    //}

    //Get window handle from user data
    CWin32Window* pWnd = (CWin32Window *)GetWindowLong(hWnd, GWL_USERDATA);

    if (pWnd)
        return pWnd->WinProc(hWnd, iMsg, wParam, lParam);
    else
        return DefWindowProc(hWnd, iMsg, wParam, lParam);
}
//---------------------------------------------------------------------------------------

char* CWin32Window::RegisterWindowClass(UINT nClassStyle,
                                        HCURSOR hCursor,
                                        HBRUSH hbrBackground,
                                        HICON hIcon)
{
    sprintf_s(s_className, "CWin32Window_%d",s_windowCounter);
    s_windowCounter++;

    HINSTANCE hInstance = GetModuleHandle(0);
    WNDCLASS    wndclass;
    wndclass.style          = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc    = CWin32Window::Wnd32Proc;
    wndclass.cbClsExtra     = 0;
    wndclass.cbWndExtra     = 0;
    wndclass.hInstance      = hInstance;
    wndclass.hIcon          = hIcon ? hIcon : LoadIcon(hInstance, LPCSTR(s_className));
    wndclass.hCursor        = hCursor ? hCursor : LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground  = hbrBackground ? hbrBackground : (HBRUSH)(COLOR_WINDOW+1);
    wndclass.lpszMenuName   = LPCSTR(s_className);
    wndclass.lpszClassName  = LPCSTR(s_className);
    RegisterClass(&wndclass);
    return s_className;
}
//---------------------------------------------------------------------------------------

WindowID CWin32Window::CreateEx(DWORD dwExStyle, 
                            char* lpszClassName,
                            char* lpszWindowName, 
                            DWORD dwStyle, 
                            const RECT& rect,
                            HWND hParentWnd, 
                            UINT nID)
{

    HINSTANCE hInstance = GetModuleHandle(NULL);

    int width = rect.right - rect.left;
    int height = rect.bottom - rect.top;

    HWND hWnd = CreateWindowEx(dwExStyle, LPCSTR(lpszClassName), LPCSTR(lpszWindowName),
        dwStyle, rect.left, rect.top, width, height, hParentWnd, NULL, hInstance, (void *)this);

    return hWnd;
}
//---------------------------------------------------------------------------------------

//
//CWin32Window::CWin32Window(const SGLContextParams& contextParams, 
//                            WindowID parentID, 
//                            int xPosition, int yPosition, 
//                            int width, int height)
//: m_contextParams(contextParams),
//  m_hWnd(nullptr), 
//  m_hParentWnd(parentID), 
//  m_pViewport(NULL), 
//  m_bShouldTrackMouse(false),
//  m_pGraphicsContext(nullptr)
//{
//    RECT rect;
//    rect.left   = xPosition;
//    rect.top    = yPosition;
//    rect.right  = xPosition + width;
//    rect.bottom = yPosition + height;
//
//    HINSTANCE hInstance = GetModuleHandle(NULL);
//    if(parentID != NULL)
//    {
//        ::GetClientRect((HWND) m_hParentWnd, &rect);
//    }
//
//    DWORD dwStyle = (HWND) m_hParentWnd ? WS_CHILD | WS_BORDER | WS_VISIBLE : WS_POPUP | WS_VISIBLE;
//    m_hWnd = CreateEx(WS_EX_TOPMOST,RegisterWindowClass(CS_DBLCLKS, NULL, NULL, NULL), NULL, dwStyle ,rect, (HWND) m_hParentWnd, 0);
//}
////---------------------------------------------------------------------------------------
//
//CWin32Window::CWin32Window(const SGLContextParams& contextParams, int width, int height)
//: m_contextParams(contextParams),
//  m_hWnd(nullptr), 
//  m_hParentWnd(nullptr), 
//  m_pViewport(NULL), 
//  m_bShouldTrackMouse(false),
//  m_pGraphicsContext(nullptr)
//{
//    RECT rect;
//    rect.left   = 0;
//    rect.top    = 0;
//    rect.right  = 0 + width;
//    rect.bottom = 0 + height;
//
//    HINSTANCE hInstance = GetModuleHandle(NULL);
//
//    DWORD dwStyle = (HWND) m_hParentWnd ? WS_CHILD | WS_BORDER | WS_VISIBLE : WS_POPUP | WS_VISIBLE | WS_OVERLAPPEDWINDOW;
//    m_hWnd = CreateEx(0, RegisterWindowClass(CS_DBLCLKS, NULL, NULL, NULL), NULL, dwStyle ,rect, (HWND) m_hParentWnd, 0);
//}
////---------------------------------------------------------------------------------------

#endif //_AN_BUILD_GRAPHICS_FOR_WINDOWS_
