//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_CAMERA_FRUSTUM_H__
#define __C_CAMERA_FRUSTUM_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CMatrix4f.h"
#include "../Maths/CPlane.h"
#include "../Maths/CBoundingBox.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class IScene;
class CCamera;
//---------------------------------------------------------------------------------------

class CCameraFrustum
{

public:

    CCameraFrustum() {}

	CCameraFrustum(const CCameraFrustum& other);

	void Transform(const Maths::CMatrix4f& mat);

	Maths::CPoint3f GetFarLeftUp() const;

	Maths::CPoint3f GetFarLeftDown() const;

	Maths::CPoint3f GetFarRightUp() const;

	Maths::CPoint3f GetFarRightDown() const;

	const Maths::CBoundingBox GetBoundingBox() const;

	void RecalculateBoundingBox();

	Maths::CMatrix4f& ViewMatrix();

	const Maths::CMatrix4f& ViewMatrix() const;

	Maths::CMatrix4f& ProjectionMatrix();

	const Maths::CMatrix4f& ProjectionMatrix() const;

	enum EFrustumPlane
	{
		EFrustumPlane_Far,
		EFrustumPlane_Near,
		EFrustumPlane_Left,
		EFrustumPlane_Right,
		EFrustumPlane_Bottom,
		EFrustumPlane_Top
	};

private:

	Maths::CPoint3f         m_cameraPosition;
	Maths::CPlane           m_planes[6];
	Maths::CBoundingBox     m_boundingBox;
	Maths::CMatrix4f        m_viewMatrix;
	Maths::CMatrix4f        m_projMatrix;

    friend class CCamera;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_CAMERA_FRUSTUM_H__
//---------------------------------------------------------------------------------------
