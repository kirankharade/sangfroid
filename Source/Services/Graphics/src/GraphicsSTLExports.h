//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_GRAPHICS_STL_EXPORTS_H__
#define __AN_GRAPHICS_STL_EXPORTS_H__
//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "../Maths/CVector3f.h"
#include "../Maths/CPoint3f.h"
#include "CColor.h"
#include <string>
#include <vector>

template class AN_GRAPHICS_API std::allocator<char>;
template class AN_GRAPHICS_API std::basic_string<char>;
template class AN_GRAPHICS_API std::allocator<wchar_t>;
template class AN_GRAPHICS_API std::basic_string<wchar_t>;

template class AN_GRAPHICS_API std::allocator<An::Maths::CPoint3f>;
template class AN_GRAPHICS_API std::vector<An::Maths::CPoint3f>;
template class AN_GRAPHICS_API std::allocator<An::Maths::CVector3f>;
template class AN_GRAPHICS_API std::vector<An::Maths::CVector3f>;

template class AN_GRAPHICS_API std::allocator<int>;
template class AN_GRAPHICS_API std::vector<int>;

template class AN_GRAPHICS_API std::allocator<An::Graphics::CColor>;
template class AN_GRAPHICS_API std::vector<An::Graphics::CColor>;

//---------------------------------------------------------------------------------------
#endif //__AN_MATH_STL_EXPORTS_H__
//---------------------------------------------------------------------------------------
