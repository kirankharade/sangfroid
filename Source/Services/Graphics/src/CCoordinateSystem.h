//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_COORDINATE_SYSTEM_H__
#define __C_COORDINATE_SYSTEM_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CMatrix4f.h"
#include "../Maths/CPlane.h"
#include "../Maths/MathsEnums.h"
#include "IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class IWindow;
class CViewport;
class IScene;
class CCameraFrustum;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CCoordinateSystem
{

public:

    CCoordinateSystem(CCoordinateSystem* parent);

    virtual ~CCoordinateSystem();

    long Id() const;
    const char* Name() const;
    void SetName(const char* name);

    void SetParent(CCoordinateSystem* parent);

    void SetAbsoluteTransform(const Maths::CMatrix4f& m);

    virtual Maths::CMatrix4f RelativeTransform() const;
    virtual Maths::CMatrix4f AbsoluteTransform() const;

    void SetRelativeOrigin(const Maths::CPoint3f o);
    void SetRelativeRotationDegX(const float angle);
    void SetRelativeRotationDegY(const float angle);
    void SetRelativeRotationDegZ(const float angle);

protected:

    void Update();
    void CalculateRelativeMatrix();

protected:

    long                            m_id;
    char*                           m_name;
    CCoordinateSystem*              m_parent;
    Maths::CPoint3f                 m_origin;
    float                           m_xRotationDeg;
    float                           m_yRotationDeg;
    float                           m_zRotationDeg;
    Maths::CMatrix4f                m_relativeTransform;
    Maths::CMatrix4f                m_absoluteTransform;

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_COORDINATE_SYSTEM_H__
//---------------------------------------------------------------------------------------
