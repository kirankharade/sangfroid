//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "C3DGeometrySceneNode.h"
#include "C3DScene.h"
#include "IScene.h"
#include "I3DGeometry.h"
#include "../ILayer.h"
#include "../IGraphics.h"
#include "../IFactory.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------
typedef std::vector<I3DGeometry*> Geometry3DList;
typedef std::vector<I3DGeometry*>::iterator Geometry3DListItr;
//---------------------------------------------------------------------------------------

C3DGeometrySceneNode::C3DGeometrySceneNode(C3DSceneNode* parentNode, C3DScene* scene)
    : C3DSceneNode(parentNode, scene)
    ,m_pDrawables(nullptr)
{
    m_pDrawables = new std::vector<I3DGeometry*>();
}
//---------------------------------------------------------------------------------------

C3DGeometrySceneNode::~C3DGeometrySceneNode()
{
    SAFE_CLEANUP(m_pDrawables);
}
//---------------------------------------------------------------------------------------

void C3DGeometrySceneNode::Render()
{
    C3DSceneNode::Render();

    IGraphics* g = IGraphics::Gfx();
    if(g)
    {
        CMatrix4f absMat = AbsoluteTransformation();
        g->ApplyTransformation(ETransformationType_ModelWorld, absMat);

        CMaterial* material = GetMaterial();
        g->ApplyMaterialToFrontFace(material);

        Geometry3DList* list = (Geometry3DList*) m_pDrawables;

        for(unsigned int i = 0; i < list->size(); i++)
        {
            IDrawable* drawable = (*list)[i];
            if(nullptr != drawable)
            {
                drawable->Render();
            }
        }
    }
}
//---------------------------------------------------------------------------------------

bool C3DGeometrySceneNode::AddDrawable(I3DGeometry* drawable)
{
    if(!drawable)
    {
        return false;
    }

    Geometry3DList* list = (Geometry3DList*) m_pDrawables;

    for(unsigned int i = 0; i < list->size(); i++)
    {
        if(drawable == (*list)[i])
        {
            //Already exists, don't add
            return false;
        }
    }

    list->push_back(drawable);

    UpdateBounds();

    if(m_pScene)
    {
        m_pScene->SetDirty();
    }

    return true;
}
//---------------------------------------------------------------------------------------

bool C3DGeometrySceneNode::RemoveDrawable(I3DGeometry* drawable)
{
    if(!drawable)
    {
        return false;
    }

    Geometry3DList* list = (Geometry3DList*) m_pDrawables;

    for(Geometry3DListItr itr = list->begin(); itr != list->end(); itr++)
    {
        if(drawable == (*itr))
        {
            list->erase(itr);
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CMaterial* C3DGeometrySceneNode::GetMaterial()
{
    CMaterial* m = Material();
    if(nullptr == m)
    {
        ISceneNode* node = this;
        while(nullptr == m && nullptr != node)
        {
            ISceneNode* parent = node->Parent();
            if(parent != nullptr)
            {
                m = parent->Material();
                if(nullptr == m)
                {
                    node = parent;
                }
            }
        }
        if(nullptr == m)
        {
            m = new CMaterial();
        }
    }
    return m;
}
//---------------------------------------------------------------------------------------

void C3DGeometrySceneNode::UpdateBounds()
{
    Geometry3DList* list = (Geometry3DList*) m_pDrawables;

    if(list->size() > 0)
    {
        CAxisOrientedBox bb = ((*list)[0])->Bounds();
        if(! bb.IsValid())
        {
            ;
            //throw some error
        }
        for(unsigned int i = 1; i < list->size(); i++)
        {
            CAxisOrientedBox bbox = ((*list)[i])->Bounds();
            if(bbox.IsValid())
            {
                bb += bbox;
            }
        }
        m_boundingBox = bb;
    }
}
//---------------------------------------------------------------------------------------

