//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_3D_SCENE_NODE_H__
#define __C_3D_SCENE_NODE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "ISceneNode.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CMatrix4f.h"
#include "../../Maths/CAxisOrientedBox.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class C3DScene;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C3DSceneNode : public ISceneNode
{

public:

    C3DSceneNode(C3DSceneNode* parentNode, C3DScene* scene);

    virtual ~C3DSceneNode();

    virtual bool AddChild(ISceneNode* pChildNode);

    virtual void Render();

    virtual void RegisterForRendering(ILayer* pLayer);

    Maths::CPoint3f Centre() const;

    const Maths::CAxisOrientedBox& Bounds();

    const Maths::CMatrix4f& AbsoluteTransformation() const;

    const Maths::CMatrix4f& RelativeTransformation() const;

    void SetRelativeTransformation(const Maths::CMatrix4f& mat);

protected:

    void CalculateAbsoluteTransformation();

    void UpdateBounds();

protected:

    Maths::CPoint3f             m_centre;
    Maths::CAxisOrientedBox     m_boundingBox;
    Maths::CMatrix4f            m_relativeMat;
    Maths::CMatrix4f            m_absoluteMat;

    friend class IScene;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_3D_SCENE_NODE_H__
//---------------------------------------------------------------------------------------

