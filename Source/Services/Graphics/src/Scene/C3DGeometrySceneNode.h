//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_3D_GEOMETRY_SCENE_NODE_H__
#define __C_3D_GEOMETRY_SCENE_NODE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "C3DSceneNode.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CMatrix4f.h"
#include "../../Maths/CAxisOrientedBox.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class C3DScene;
class I3DGeometry;
class CMaterial;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C3DGeometrySceneNode : public C3DSceneNode
{

public:

    C3DGeometrySceneNode(C3DSceneNode* parentNode, C3DScene* scene);

    virtual ~C3DGeometrySceneNode();

    virtual void Render();

    bool AddDrawable(I3DGeometry* drawable);

    bool RemoveDrawable(I3DGeometry* drawable);

protected:

    CMaterial* GetMaterial();

    void UpdateBounds();

protected:

    void*       m_pDrawables;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_3D_GEOMETRY_SCENE_NODE_H__
//---------------------------------------------------------------------------------------
