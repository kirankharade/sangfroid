//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "C3DMesh.h"
#include "../IFactory.h"
#include "../IGraphics.h"
#include "../IDrawManager.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

C3DMesh::C3DMesh()
    : I3DGeometry()
    ,m_pVertices(nullptr)
    ,m_pTexCoords(nullptr)
    ,m_pNormals(nullptr)
    ,m_pIndices(nullptr)
    ,m_pColorPointer(nullptr)
    ,m_indexCount(0)
    ,m_vertexCount(0)
    ,m_texCoordsCount(0)
    ,m_colorCount(0)
    ,m_normalCount(0)
    ,m_bounds(CAxisOrientedBox())
{
    CleanUp();
    m_name = "";
}
//---------------------------------------------------------------------------------------

C3DMesh::C3DMesh(const std::vector<Maths::CPoint3f>& vertices,
        const std::vector<Maths::CVector3f>& normals,
        const std::vector<CColor>& colors,
        const std::vector<Maths::CPoint2f>& texCoords,
        const std::vector<AInt32>& indices)
    : I3DGeometry()
    ,m_pVertices(nullptr)
    ,m_pTexCoords(nullptr)
    ,m_pNormals(nullptr)
    ,m_pIndices(nullptr)
    ,m_pColorPointer(nullptr)
    ,m_indexCount(indices.size())
    ,m_vertexCount(vertices.size())
    ,m_texCoordsCount(texCoords.size())
    ,m_colorCount(colors.size())
    ,m_normalCount(normals.size())
    ,m_bounds(CAxisOrientedBox())
{
    if(m_vertexCount > 0)
    {
        m_pVertices     = new CPoint3f[m_vertexCount];
        for(unsigned int i = 0; i < m_vertexCount; i++)
        {
            m_pVertices[i] = vertices[i];
        }
    }
    if(m_normalCount > 0)
    {
        m_pNormals      = new CVector3f[m_normalCount];
        for(unsigned int i = 0; i < m_normalCount; i++)
        {
            m_pNormals[i] = normals[i];
        }
    }
    if(m_texCoordsCount > 0)
    {
        m_pTexCoords    = new CPoint2f[m_texCoordsCount];
        for(unsigned int i = 0; i < m_texCoordsCount; i++)
        {
            m_pTexCoords[i] = texCoords[i];
        }
    }
    if(m_indexCount > 0)
    {
        m_pIndices      = new AUInt32[m_indexCount];
        for(unsigned int i = 0; i < m_indexCount; i++)
        {
            m_pIndices[i] = indices[i];
        }
    }
    if(m_colorCount > 0)
    {
        m_pColorPointer = new AReal32[4 * m_colorCount];
        m_pColorPointer = (AReal32*) CColor::ToRGBAf(colors);
    }
    m_bounds = CAxisOrientedBox::CalculateBox(m_pVertices, m_vertexCount);
}
//---------------------------------------------------------------------------------------

C3DMesh::C3DMesh(Maths::CPoint3f* vertices, const AUInt32 vertexCount,
        Maths::CVector3f* normals, const AUInt32 normalCount,
        CColor* colors, const AUInt32 colorCount,
        Maths::CPoint2f* texCoords, const AUInt32 texCoordsCount,
        int* indices, const AUInt32 indexCount)
    : I3DGeometry()
    ,m_pVertices(nullptr)
    ,m_pTexCoords(nullptr)
    ,m_pNormals(nullptr)
    ,m_pIndices(nullptr)
    ,m_pColorPointer(nullptr)
    ,m_indexCount(indexCount)
    ,m_vertexCount(vertexCount)
    ,m_texCoordsCount(texCoordsCount)
    ,m_colorCount(colorCount)
    ,m_normalCount(normalCount)
    ,m_bounds(CAxisOrientedBox())
{
    if(m_vertexCount > 0)
    {
        m_pVertices     = new CPoint3f[m_vertexCount];
        memcpy(&m_pVertices[0], &vertices[0], m_vertexCount * sizeof(CPoint3f));
    }
    if(m_normalCount > 0)
    {
        m_pNormals      = new CVector3f[m_normalCount];
        memcpy(&m_pNormals[0], &normals[0], m_normalCount * sizeof(CVector3f));
    }
    if(m_texCoordsCount > 0)
    {
        m_pTexCoords    = new CPoint2f[m_texCoordsCount];
        memcpy(&m_pTexCoords[0], &texCoords[0], m_texCoordsCount * sizeof(CPoint2f));
    }
    if(m_indexCount > 0)
    {
        m_pIndices      = new AUInt32[m_indexCount];
        memcpy(&m_pIndices[0], &indices[0], m_indexCount * sizeof(AInt32));
    }
    if(m_colorCount > 0)
    {
        //m_pColorPointer = new AReal32[4 * m_colorCount];
        m_pColorPointer = (AReal32*) CColor::ToRGBAf(&colors[0], m_colorCount);
    }
    m_bounds = CAxisOrientedBox::CalculateBox(m_pVertices, m_vertexCount);
}
//---------------------------------------------------------------------------------------

C3DMesh::~C3DMesh()
{
    CleanUp();
}
//---------------------------------------------------------------------------------------

void C3DMesh::CleanUp()
{
    SAFE_CLEANUP(m_pVertices);
    SAFE_CLEANUP(m_pNormals);
    SAFE_CLEANUP(m_pTexCoords);
    SAFE_CLEANUP(m_pColorPointer);
    SAFE_CLEANUP(m_pIndices);
}
//---------------------------------------------------------------------------------------

EDrawableType C3DMesh::Type() const
{ 
    return EDrawableType_Mesh3D; 
}
//---------------------------------------------------------------------------------------

const Maths::CPoint3f* C3DMesh::Vertices() const 
{ 
    return m_pVertices; 
}
//---------------------------------------------------------------------------------------

const Maths::CPoint2f* C3DMesh::TexCoords() const
{ 
    return m_pTexCoords; 
}
//---------------------------------------------------------------------------------------

const AReal32* C3DMesh::Colors() const
{ 
    return m_pColorPointer; 
}
//---------------------------------------------------------------------------------------

const Maths::CVector3f* C3DMesh::Normals() const 
{ 
    return m_pNormals; 
}
//---------------------------------------------------------------------------------------

const unsigned int* C3DMesh::Indices() const 
{ 
    return m_pIndices; 
}
//---------------------------------------------------------------------------------------

AUInt32 C3DMesh::TriangleCount() const 
{ 
    return m_indexCount / 3; 
}
//---------------------------------------------------------------------------------------

AUInt32 C3DMesh::VertexCount() const 
{ 
    return m_vertexCount; 
}
//---------------------------------------------------------------------------------------

AUInt32 C3DMesh::TexCoordsCount() const 
{ 
    return m_texCoordsCount; 
}
//---------------------------------------------------------------------------------------

AUInt32 C3DMesh::NormalCount() const 
{ 
    return m_normalCount; 
}
//---------------------------------------------------------------------------------------

AUInt32 C3DMesh::ColorCount() const 
{ 
    return m_colorCount; 
}
//---------------------------------------------------------------------------------------

AUInt32 C3DMesh::IndexCount() const 
{ 
    return m_indexCount; 
}
//---------------------------------------------------------------------------------------

void C3DMesh::Render() 
{
    IGraphics* g = IGraphics::Gfx();
    if(g)
    {
        IDrawManager* dm = g->DrawManager();
        if(dm)
        {
            dm->Draw(this);
        }
    }
}
//---------------------------------------------------------------------------------------

const Maths::CAxisOrientedBox& C3DMesh::Bounds() const
{
    return m_bounds;
}
//---------------------------------------------------------------------------------------
