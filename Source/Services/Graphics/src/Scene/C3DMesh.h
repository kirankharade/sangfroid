//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_3D_MESH_H__
#define __C_3D_MESH_H__
//---------------------------------------------------------------------------------------
#include "I3DGeometry.h"
#include "../GraphicsIncludes.h"
#include "../GraphicsSTLExports.h"
#include "../../Maths/CAxisOrientedBox.h"
#include "../../Maths/CPoint2f.h"
#include "../../Maths/CVector3f.h"
#include "../CColor.h"
#include <vector>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IGraphics;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C3DMesh : public I3DGeometry
{

public:

   C3DMesh();

   C3DMesh(const std::vector<Maths::CPoint3f>& vertices,
           const std::vector<Maths::CVector3f>& normals,
           const std::vector<CColor>& colors,
           const std::vector<Maths::CPoint2f>& texCoords,
           const std::vector<AInt32>& indices);

   C3DMesh(Maths::CPoint3f* vertices, const AUInt32 vertexCount,
           Maths::CVector3f* normals, const AUInt32 normalCount,
           CColor* colors, const AUInt32 colorCount,
           Maths::CPoint2f* texCoords, const AUInt32 texCoordsCount,
           AInt32* indices, const AUInt32 indexCount);

   virtual ~C3DMesh();

   virtual EDrawableType Type() const;

   const Maths::CPoint3f* Vertices() const;

   const Maths::CPoint2f* TexCoords() const;

   const AReal32* Colors() const;

   const Maths::CVector3f* Normals() const;

   const unsigned int* Indices() const;

   AUInt32 TriangleCount() const;

   AUInt32 VertexCount() const;

   AUInt32 TexCoordsCount() const;

   AUInt32 NormalCount() const;

   AUInt32 ColorCount() const;

   AUInt32 IndexCount() const;

   void Render();

   const Maths::CAxisOrientedBox& Bounds() const;

protected:

    void CleanUp();

private:

   Maths::CPoint3f*             m_pVertices;
   Maths::CPoint2f*             m_pTexCoords;
   Maths::CVector3f*            m_pNormals;
   AUInt32*                     m_pIndices;
   AReal32*                     m_pColorPointer;
   Maths::CAxisOrientedBox      m_bounds;

   AUInt32                      m_vertexCount;
   AUInt32                      m_texCoordsCount;
   AUInt32                      m_normalCount;
   AUInt32                      m_colorCount;
   AUInt32                      m_indexCount;

   friend class IGraphics;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_3D_MESH_H__
//---------------------------------------------------------------------------------------


