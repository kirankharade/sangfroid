//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_GEOMETRY_3D_H__
#define __I_GEOMETRY_3D_H__
//---------------------------------------------------------------------------------------
#include "IDrawable.h"
#include "../GraphicsIncludes.h"
//#include "../../Maths/CPoint3f.h"
//#include "../../Maths/CVector3f.h"
#include "../../Maths/CAxisOrientedBox.h"
#include "../GraphicsSTLExports.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API I3DGeometry : public IDrawable
{

public:

   I3DGeometry()
   {
      m_name = "3DGeometry";
      m_bUseVertexColors = false;
   }

   virtual ~I3DGeometry()
   {
   }

   virtual const Maths::CAxisOrientedBox& Bounds() const = 0;

   bool UseVertexColors() const { return m_bUseVertexColors; }

   void SetUseVertexColors(const bool& useVertexColors) { m_bUseVertexColors = useVertexColors; }

protected:

    bool    m_bUseVertexColors;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __I_GEOMETRY_3D_H__
//---------------------------------------------------------------------------------------

