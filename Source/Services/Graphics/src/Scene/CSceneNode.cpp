//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "CSceneNode.h"
#include "IScene.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CSceneNode::CSceneNode(ISceneNode* parentNode, IScene* scene)
    : ISceneNode(parentNode, scene)
{
}
//---------------------------------------------------------------------------------------

CSceneNode::~CSceneNode()
{
}
//---------------------------------------------------------------------------------------

void CSceneNode::Render()
{
}
//---------------------------------------------------------------------------------------

void CSceneNode::RegisterForRendering()
{
    ISceneNode::RegisterForRendering();
}
//---------------------------------------------------------------------------------------
