//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "CRootSceneNode.h"
#include "IScene.h"
#include "../ILayer.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CRootSceneNode::CRootSceneNode(IScene* scene)
    : ISceneNode(nullptr, scene)
{
}
//---------------------------------------------------------------------------------------

CRootSceneNode::~CRootSceneNode()
{
}
//---------------------------------------------------------------------------------------

void CRootSceneNode::Render()
{
}
//---------------------------------------------------------------------------------------

void CRootSceneNode::RegisterForRendering(ILayer* pLayer)
{
    ISceneNode::RegisterForRendering(pLayer);
}
//---------------------------------------------------------------------------------------
