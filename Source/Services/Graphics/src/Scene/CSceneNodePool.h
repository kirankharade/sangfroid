//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_SCENENODE_POOL_H__
#define __C_SCENENODE_POOL_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class ISceneNode;
class IScene;
//---------------------------------------------------------------------------------------

class CSceneNodePool
{

public:

    CSceneNodePool(IScene* scene);

    virtual ~CSceneNodePool();

    bool Add(ISceneNode* pChildNode);

    bool Detach(ISceneNode* pChildNode);

    bool Detach(const std::string& name);

    bool Detach(const AIndex64& id);

    ISceneNode* Search(const std::string& name);

    ISceneNode* Search(const AIndex64& id);

    bool Exists(const std::string& name) const;

    bool Exists(const AIndex64& id) const;

protected:

    IScene*                     m_pScene;
    void*                       m_pNameNodeMap;
    void*                       m_pIdNodeMap;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_SCENENODE_POOL_H__
//---------------------------------------------------------------------------------------

