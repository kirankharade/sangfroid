//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_3D_SCENE_H__
#define __C_3D_SCENE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "ISceneNode.h"
#include "IScene.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CAxisOrientedBox.h"
#include "../CColor.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C3DScene : public IScene
{

public:

    static C3DScene* Create();

    virtual ~C3DScene();

    Maths::CPoint3f Centre() const;

    const Maths::CAxisOrientedBox& Bounds();

    const CColor& SceneAmbientColor() const;

    void SetAmbientLight(const CColor& light);

    const CMaterial& SceneDefaultMaterial() const;

    void SetSceneDefaultMaterial(const CMaterial& material);

protected:

    C3DScene();

    virtual void CheckInitialization();

protected:

    Maths::CPoint3f             m_centre;
    Maths::CAxisOrientedBox     m_boundingBox;
    CColor                      m_sceneAmbientColor;
    CMaterial                   m_sceneMaterial;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_3D_SCENE_H__
//---------------------------------------------------------------------------------------

