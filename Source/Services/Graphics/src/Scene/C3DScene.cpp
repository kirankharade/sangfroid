//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "C3DScene.h"
#include "IScene.h"
#include "../CCamera.h"
#include "../ILayer.h"
#include "../Defaults.h"
#include "CSceneNodePool.h"
#include "C3DSceneNode.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

C3DScene* C3DScene::Create()
{
    C3DScene* scene = new C3DScene();
    scene->CheckInitialization();
    return scene;
}
//---------------------------------------------------------------------------------------

C3DScene::C3DScene()
    : IScene()
     ,m_centre(CPoint3f())
     ,m_boundingBox(CAxisOrientedBox())
     ,m_sceneAmbientColor(Defaults::LightAmbient)
     ,m_sceneMaterial(CMaterial())
{
}
//---------------------------------------------------------------------------------------

C3DScene::~C3DScene()
{
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f C3DScene::Centre() const
{
    return m_centre;
}
//---------------------------------------------------------------------------------------

const Maths::CAxisOrientedBox& C3DScene::Bounds()
{
    C3DSceneNode* baseNode = dynamic_cast<C3DSceneNode*> (m_pBaseNode);
    if(baseNode)
    {
        m_boundingBox = baseNode->Bounds();
    }
    return m_boundingBox;
}
//---------------------------------------------------------------------------------------

const CColor& C3DScene::SceneAmbientColor() const
{
    return m_sceneAmbientColor;
}
//---------------------------------------------------------------------------------------

void C3DScene::SetAmbientLight(const CColor& light)
{
    m_sceneAmbientColor = light;
}
//---------------------------------------------------------------------------------------

const CMaterial& C3DScene::SceneDefaultMaterial() const
{
    return m_sceneMaterial;
}
//---------------------------------------------------------------------------------------

void C3DScene::SetSceneDefaultMaterial(const CMaterial& material)
{
    m_sceneMaterial = material;
}
//---------------------------------------------------------------------------------------

void C3DScene::CheckInitialization()
{
    if(!m_bInitialized)
    {
        m_pNodePool = new CSceneNodePool(this);
        m_pBaseNode = new C3DSceneNode(nullptr, this);
        m_bInitialized = true;
    }
}
//---------------------------------------------------------------------------------------

