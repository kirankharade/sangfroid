//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "ISceneNode.h"
#include "CSceneNodeCollection.h"
#include <vector>
#include <string>

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------
typedef vector<ISceneNode*, std::allocator<ISceneNode*>> NodeList;
typedef vector<ISceneNode*, std::allocator<ISceneNode*>>::iterator NodeListIterator;
typedef vector<ISceneNode*, std::allocator<ISceneNode*>>::const_iterator NodeListConstIterator;
//---------------------------------------------------------------------------------------

CSceneNodeCollection::CSceneNodeCollection ()
    : m_pImpl(nullptr)
{
    m_pImpl = new vector<ISceneNode*, std::allocator<ISceneNode*>>();
}
//---------------------------------------------------------------------------------------

CSceneNodeCollection::~CSceneNodeCollection()
{
    SAFE_CLEANUP(m_pImpl);
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::Insert(ISceneNode* pNode, const unsigned int index)
{
    if(!pNode)
    {
        return false;
    }
    if(Contains(pNode))
    {
        DetachFromCollection(pNode);
    }
    NodeList* list = ((NodeList*) m_pImpl);
    unsigned int count = 0;
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(count != index)
        {
            count++;
            continue;
        }
        list->insert(itr, pNode);

        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::PushFront(ISceneNode* pNode)
{
    if(!pNode)
    {
        return false;
    }
    if(Contains(pNode))
    {
        DetachFromCollection(pNode);
    }

    NodeList* list = ((NodeList*) m_pImpl);
    list->insert(list->begin(), pNode);

    return true;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::PushBack(ISceneNode* pNode)
{
    if(!pNode)
    {
        return false;
    }
    if(Contains(pNode))
    {
        DetachFromCollection(pNode);
    }

    NodeList* list = ((NodeList*) m_pImpl);
    list->push_back(pNode);

    return true;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::DetachFromCollection(ISceneNode* pNode)
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(pNode == *itr)
        {
            list->erase(itr);
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::DetachFromCollection(const AIndex64& id)
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(*itr)
        {
            if(id == (*itr)->Id())
            {
                list->erase(itr);
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::DetachFromCollection(const std::string& name)
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(*itr)
        {
            if(name == (*itr)->Name())
            {
                list->erase(itr);
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

int CSceneNodeCollection::Index(ISceneNode* pNode) const
{
    int count = 0;
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(pNode == *itr)
        {
            return count;
        }
        count++;
    }
    return -1;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::Contains(ISceneNode* pNode) const
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(pNode == *itr)
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::Contains(const std::string& name) const
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(*itr)
        {
            if(name == (*itr)->Name())
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodeCollection::Contains(const AIndex64& id) const
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(*itr)
        {
            if(id == (*itr)->Id())
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

unsigned int CSceneNodeCollection::Count() const
{
    NodeList* list = ((NodeList*) m_pImpl);
    return list->size();
}
//---------------------------------------------------------------------------------------

ISceneNode* CSceneNodeCollection::operator [] (const unsigned int& index)
{
    NodeList* list = ((NodeList*) m_pImpl);
    if(index >= list->size())
    {
        return nullptr;
    }
    return list->at(index);
}
//---------------------------------------------------------------------------------------

const ISceneNode* CSceneNodeCollection::operator [] (const unsigned int& index) const
{
    NodeList* list = ((NodeList*) m_pImpl);
    if(index >= list->size())
    {
        return nullptr;
    }
    return list->at(index);
}
//---------------------------------------------------------------------------------------

ISceneNode* CSceneNodeCollection::Node(const std::string& name)
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(name == (*itr)->Name())
        {
            return *itr;
        }
    }
    return nullptr;
}
//---------------------------------------------------------------------------------------

ISceneNode* CSceneNodeCollection::Node(const AIndex64& id)
{
    NodeList* list = ((NodeList*) m_pImpl);
    for(NodeListIterator itr = list->begin(); itr != list->end(); itr++)
    {
        if(id == (*itr)->Id())
        {
            return *itr;
        }
    }
    return nullptr;
}
//---------------------------------------------------------------------------------------

