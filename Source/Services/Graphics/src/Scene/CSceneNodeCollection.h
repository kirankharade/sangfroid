//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_SCENENODE_COLLECTION_H__
#define __C_SCENENODE_COLLECTION_H__
//---------------------------------------------------------------------------------------

#include "../GraphicsIncludes.h"
#include "../GraphicsDefines.h"
#include <string>

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class ISceneNode;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CSceneNodeCollection
{

public:

    CSceneNodeCollection ();
    virtual ~CSceneNodeCollection();

    bool Insert(ISceneNode* pNode, const unsigned int index);
    bool PushFront(ISceneNode* pNode);
    bool PushBack(ISceneNode* pNode);

    bool DetachFromCollection(ISceneNode* pNode);
    bool DetachFromCollection(const AIndex64& id);
    bool DetachFromCollection(const std::string& name);

    int Index(ISceneNode* pNode) const;
    bool Contains(ISceneNode* pNode) const;
    bool Contains(const std::string& name) const;
    bool Contains(const AIndex64& id) const;

    unsigned int Count() const;

    ISceneNode* operator [] (const unsigned int& index);
    const ISceneNode* operator [] (const unsigned int& index) const;
    ISceneNode* Node(const std::string& name);
    ISceneNode* Node(const AIndex64& id);

private:

    void*   m_pImpl;
};
//-----------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_SCENENODE_COLLECTION_H__
//---------------------------------------------------------------------------------------
