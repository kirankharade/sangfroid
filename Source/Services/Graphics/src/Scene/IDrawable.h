//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_DRAWABLE_H__
#define __I_DRAWABLE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsSTLExports.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IDrawable
{

public:

    IDrawable()
    {
        m_name = "Drawable";
        m_id = CGraphicsUtils::GetUniqueObjectID();
    }

    virtual ~IDrawable()
    {
    }

    virtual EDrawableType Type() const = 0;

    std::string& Name() { return m_name; }

    const std::string& Name() const { return m_name; }

    AIndex64 Id() const { return m_id; }

    virtual void Render() = 0;

protected:

    std::string             m_name;
    AIndex64                m_id;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __I_DRAWABLE_H__
//---------------------------------------------------------------------------------------

