﻿//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_G_TRIANGLE3_H__
#define __C_G_TRIANGLE3_H__
//---------------------------------------------------------------------------------------

#include <cmath>
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CVector3f.h"
#include "../GraphicsIncludes.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CGTriangle3
{
public:

    Maths::CPoint3f p1;
    Maths::CPoint3f p2;
    Maths::CPoint3f p3;

    CGTriangle3()
    {

    }

    CGTriangle3(Maths::CPoint3f a, Maths::CPoint3f b, Maths::CPoint3f c)
    {
        p1 = a;
        p2 = b;
        p3 = c;
    }

    Maths::CVector3f Normal()
    {
        Maths::CVector3f v1(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
        Maths::CVector3f v2(p3.x - p2.x, p3.y - p2.y, p3.z - p2.z);
        Maths::CVector3f normal = v1.cross(v2);
        normal.normalize();
        return normal;
    }

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_G_TRIANGLE3_H__
//---------------------------------------------------------------------------------------
