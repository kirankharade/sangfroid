//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_2D_SCENE_H__
#define __C_2D_SCENE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../../Maths/CPoint2f.h"
#include "../../Maths/CAxisOrientedRectangle.h"
#include "ISceneNode.h"
#include "IScene.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C2DScene : public IScene
{

public:

    static C2DScene* Create();

    virtual ~C2DScene();

    Maths::CPoint2f Centre() const;

    const Maths::CAxisOrientedRectangle& Bounds() const;

protected:

    C2DScene();

protected:

    Maths::CPoint2f                  m_centre;
    Maths::CAxisOrientedRectangle    m_boundingBox;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_2D_SCENE_H__
//---------------------------------------------------------------------------------------

