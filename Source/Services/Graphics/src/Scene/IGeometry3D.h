//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_GEOMETRY_3D_H__
#define __I_GEOMETRY_3D_H__
//---------------------------------------------------------------------------------------
#include "IDrawable.h"
#include "../GraphicsIncludes.h"
//#include "../../Maths/CPoint3f.h"
//#include "../../Maths/CVector3f.h"
#include "../../Maths/CAxisOrientedBox.h"
#include "../GraphicsSTLExports.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IGeometry3D : public IDrawable
{

public:

   IGeometry3D()
   {
      m_name = "3DGeometry";
   }

   virtual ~IGeometry3D()
   {
   }

   //virtual Maths::CAxisOrientedBox* BoundingBox() = 0;

protected:


};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __I_GEOMETRY_3D_H__
//---------------------------------------------------------------------------------------

