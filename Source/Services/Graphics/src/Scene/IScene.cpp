//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "IScene.h"
#include "ISceneNode.h"
#include "CRootSceneNode.h"
#include "CSceneNodeCollection.h"
#include "CSceneNodePool.h"
#include "../FileUtils/CStringUtils.h"
#include <map>
#include <string>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------

IScene::IScene()
    :m_name("Scene")
    ,m_id(CGraphicsUtils::GetUniqueObjectID())
    ,m_pBaseNode(nullptr)
    ,m_pNodePool(nullptr)
    ,m_bInitialized(false)
    ,m_bIsDirty(false)
{
    m_name = std::string("Scene_") + CStringUtils::ToString(m_id);
}
//---------------------------------------------------------------------------------------

IScene::~IScene()
{
    SAFE_CLEANUP(m_pBaseNode);
    SAFE_CLEANUP(m_pNodePool);
}
//---------------------------------------------------------------------------------------

const ISceneNode* IScene::BaseNode() const
{
    return m_pBaseNode;
}
//---------------------------------------------------------------------------------------

ISceneNode* IScene::BaseNode()
{
    return m_pBaseNode;
}
//---------------------------------------------------------------------------------------

void IScene::CheckInitialization()
{
    if(!m_bInitialized)
    {
        m_pNodePool = new CSceneNodePool(this);
        m_pBaseNode = new CRootSceneNode(this);
        m_bInitialized = true;
    }
}
//---------------------------------------------------------------------------------------

void IScene::AddToNodePool(ISceneNode* node)
{
    CheckInitialization();

    if(!node)
    {
        return;
    }

    if(m_pNodePool)
    {
        m_bIsDirty =true;
        ((CSceneNodePool*)m_pNodePool)->Add(node);
    }
}
//---------------------------------------------------------------------------------------

bool IScene::RemoveFromScene(ISceneNode* node)
{
    CheckInitialization();
    if(node)
    {
        node->SetScene(nullptr);
        m_bIsDirty = true;
        return ((CSceneNodePool*) m_pNodePool)->Detach(node);
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool IScene::ExistsInScene(const std::string& name) const
{
    if(!m_bInitialized)
    {
        return false;
    }
    return ((CSceneNodePool*) m_pNodePool)->Exists(name);
}
//---------------------------------------------------------------------------------------

bool IScene::ExistsInScene(const AIndex64& id) const
{
    if(!m_bInitialized)
    {
        return false;
    }
    return ((CSceneNodePool*) m_pNodePool)->Exists(id);
}
//---------------------------------------------------------------------------------------

ISceneNode* IScene::SearchInScene(const std::string& name) const
{
    if(!m_bInitialized)
    {
        return nullptr;
    }
    return ((CSceneNodePool*) m_pNodePool)->Search(name);
}
//---------------------------------------------------------------------------------------

ISceneNode* IScene::SearchInScene(const AIndex64& id) const
{
    if(!m_bInitialized)
    {
        return nullptr;
    }
    return ((CSceneNodePool*) m_pNodePool)->Search(id);
}
//---------------------------------------------------------------------------------------

const std::string& IScene::Name() const
{ 
    return m_name; 
}
//---------------------------------------------------------------------------------------

void IScene::SetName(const std::string& name)
{
    CheckInitialization();
    m_name = name; 
}
//---------------------------------------------------------------------------------------

AIndex64 IScene::Id() const 
{ 
    return m_id;
}
//---------------------------------------------------------------------------------------

void IScene::RegisterToRender(ILayer* layer)
{
    if(m_pBaseNode)
    {
        m_pBaseNode->RegisterForRendering(layer);
    }
}
//---------------------------------------------------------------------------------------

void IScene::SetDirty()
{
    m_bIsDirty = true;
}
//---------------------------------------------------------------------------------------

bool IScene::IsDirty() const
{
    return m_bIsDirty;
}
//---------------------------------------------------------------------------------------
