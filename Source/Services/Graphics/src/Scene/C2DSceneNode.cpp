//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "C2DSceneNode.h"
#include "C2DScene.h"
#include "IScene.h"
#include "../ILayer.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

C2DSceneNode::C2DSceneNode(C2DSceneNode* parentNode, C2DScene* scene)
    : ISceneNode(parentNode, scene)
{
}
//---------------------------------------------------------------------------------------

C2DSceneNode::~C2DSceneNode()
{
}
//---------------------------------------------------------------------------------------

void C2DSceneNode::Render()
{
}
//---------------------------------------------------------------------------------------

void C2DSceneNode::RegisterForRendering(ILayer* pLayer)
{
    ISceneNode::RegisterForRendering(pLayer);
}
//---------------------------------------------------------------------------------------

Maths::CPoint2f C2DSceneNode::Centre() const
{
    return m_centre;
}
//---------------------------------------------------------------------------------------

const Maths::CAxisOrientedRectangle& C2DSceneNode::Bounds() const
{
    return m_boundingBox;
}
//---------------------------------------------------------------------------------------


