//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_SCENE_H__
#define __I_SCENE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsSTLExports.h"
#include "IDrawable.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class ISceneNode;
class CSceneNodeCollection;
class ILayer;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IScene
{

protected:

    IScene();

public:

    virtual ~IScene();

    const ISceneNode* BaseNode() const;

    ISceneNode* BaseNode();

    ISceneNode* SearchInScene(const std::string& name) const;

    ISceneNode* SearchInScene(const AIndex64& id) const;

    bool ExistsInScene(const std::string& nodeName) const;

    bool ExistsInScene(const AIndex64& nodeId) const;

    const std::string& Name() const;

    void SetName(const std::string& name);

    AIndex64 Id() const;

    void SetDirty();

    bool IsDirty() const;

private:

    bool RemoveFromScene(ISceneNode* childNode);

protected:

    virtual void CheckInitialization();

    void AddToNodePool(ISceneNode* node);

    void RegisterToRender(ILayer* layer);

protected:

    ISceneNode*                 m_pBaseNode;
    void*                       m_pNodePool;
    std::string                 m_name;
    AIndex64                    m_id;
    bool                        m_bInitialized;
    bool                        m_bIsDirty;

    friend class ISceneNode;
    friend class ILayer;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __I_SCENE_H__
//---------------------------------------------------------------------------------------

