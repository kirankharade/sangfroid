//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "../FileUtils/CStringUtils.h"
#include "ISceneNode.h"
#include "IScene.h"
#include "CSceneNodeCollection.h"
#include "../CMaterial.h"
#include "../ILayer.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

ISceneNode::ISceneNode(ISceneNode* parentNode, IScene* scene)
    :m_name("SceneNode")
    ,m_id(CGraphicsUtils::GetUniqueObjectID())
    ,m_pNodeCollection(nullptr)
    ,m_pScene(scene)
    ,m_pParent(parentNode)
    ,m_bIsVisible(true)
    ,m_pMaterial(nullptr)
{
    m_pNodeCollection = new CSceneNodeCollection();
    m_pMaterial = new CMaterial();
    m_name = std::string("SceneNode_") + CStringUtils::ToString(m_id);
}
//---------------------------------------------------------------------------------------

ISceneNode::~ISceneNode()
{
    SAFE_CLEANUP(m_pNodeCollection);
}
//---------------------------------------------------------------------------------------

IScene* ISceneNode::Scene() const
{
    return m_pScene;
}
//---------------------------------------------------------------------------------------

ISceneNode* ISceneNode::Parent() const
{
    return m_pParent;
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetParent(ISceneNode* node, ISceneNode* parent)
{
    if(node)
    {
        node->m_pParent = parent;
    }
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetScene(IScene* scene)
{
    m_pScene = scene;
}
//---------------------------------------------------------------------------------------

AUInt32 ISceneNode::ChildrenCount() const
{
    if(m_pNodeCollection)
    {
        return m_pNodeCollection->Count();
    }
    return 0;
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetSceneAffiliation(IScene* scene)
{
    if(m_pScene != scene)
    {
        if(m_pParent)
        {
            m_pParent->DetachChild(this);
        }
        if(m_pScene)
        {
            m_pScene->RemoveFromScene(this);
        }
        m_pScene = scene;
        if(m_pScene)
        {
            m_pScene->AddToNodePool(this);
        }
    }
    //Do the same for children nodes...
    if(m_pNodeCollection)
    {
        AUInt32 childrenCount = m_pNodeCollection->Count();
        for(AUInt32 i = 0; i < childrenCount; i++)
        {
            ISceneNode* node = (*m_pNodeCollection)[i];
            if(node)
            {
                node->SetSceneAffiliation(scene, false);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetSceneAffiliation(IScene* scene, const bool detachFromParent)
{
    if(m_pScene != scene)
    {
        if(m_pParent && detachFromParent)
        {
            m_pParent->DetachChild(this);
        }
        if(m_pScene)
        {
            m_pScene->RemoveFromScene(this);
        }
        m_pScene = scene;
        if(m_pScene)
        {
            m_pScene->AddToNodePool(this);
        }
    }
    //Do the same for children nodes...
    if(m_pNodeCollection)
    {
        AUInt32 childrenCount = m_pNodeCollection->Count();
        for(AUInt32 i = 0; i < childrenCount; i++)
        {
            ISceneNode* node = (*m_pNodeCollection)[i];
            if(node)
            {
                node->SetSceneAffiliation(scene, detachFromParent);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void ISceneNode::RemoveFromScene()
{
    if(m_pScene)
    {
        if(m_pParent)
        {
            m_pParent->DetachChild(this);
        }
        m_pScene->RemoveFromScene(this);
        m_pScene = nullptr;
    }

    //Do the same for children nodes...
    if(m_pNodeCollection)
    {
        AUInt32 childrenCount = m_pNodeCollection->Count();
        for(AUInt32 i = 0; i < childrenCount; i++)
        {
            ISceneNode* node = (*m_pNodeCollection)[i];
            if(node)
            {
                node->RemoveFromScene(false);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void ISceneNode::RemoveFromScene(const bool detachFromParent)
{
    if(m_pScene)
    {
        if(m_pParent && detachFromParent)
        {
            m_pParent->DetachChild(this);
        }
        m_pScene->RemoveFromScene(this);
        m_pScene = nullptr;
    }

    //Do the same for children nodes...
    if(m_pNodeCollection)
    {
        AUInt32 childrenCount = m_pNodeCollection->Count();
        for(AUInt32 i = 0; i < childrenCount; i++)
        {
            ISceneNode* node = (*m_pNodeCollection)[i];
            if(node)
            {
                node->RemoveFromScene(detachFromParent);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

bool ISceneNode::AddChild(ISceneNode* pChildNode)
{
    if(nullptr == pChildNode)
    {
        return false;
    }

    pChildNode->SetSceneAffiliation(m_pScene);

    if(this != pChildNode->Parent())
    {
        ISceneNode* pParent = pChildNode->Parent();
        if(pParent)
        {
            pParent->DetachChild(pChildNode);
        }
    }
    ISceneNode::SetParent(pChildNode, this);

    return m_pNodeCollection->PushBack(pChildNode);
}
//---------------------------------------------------------------------------------------

bool ISceneNode::DetachChild(ISceneNode* node)
{
    if(nullptr == node)
    {
        return false;
    }
    AIndex64 id = node->Id();
    if(m_pNodeCollection->Contains(id))
    {
        SetParent(node, nullptr);
        return m_pNodeCollection->DetachFromCollection(node);
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool ISceneNode::DetachChild(const std::string& nodeName)
{
    ISceneNode* node = m_pScene->SearchInScene(nodeName);
    if(node == nullptr)
    {
        return false;
    }

    if(m_pNodeCollection->Contains(node->Id()))
    {
        SetParent(node, nullptr);
        return m_pNodeCollection->DetachFromCollection(node);
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool ISceneNode::DetachChild(const AIndex64& nodeId)
{
    ISceneNode* node = m_pScene->SearchInScene(nodeId);
    if(node == nullptr)
    {
        return false;
    }

    if(m_pNodeCollection->Contains(node->Id()))
    {
        SetParent(node, nullptr);
        return m_pNodeCollection->DetachFromCollection(node);
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool ISceneNode::IsChildNode(const std::string& name) const
{
    return m_pNodeCollection->Contains(name);
}
//---------------------------------------------------------------------------------------

bool ISceneNode::IsChildNode(const AIndex64& nodeId) const
{
    return m_pNodeCollection->Contains(nodeId);
}
//---------------------------------------------------------------------------------------

ISceneNode* ISceneNode::Child(const std::string& name)
{
    return m_pNodeCollection->Node(name);
}
//---------------------------------------------------------------------------------------

ISceneNode* ISceneNode::Child(const AIndex64& id)
{
    return m_pNodeCollection->Node(id);
}
//---------------------------------------------------------------------------------------

bool ISceneNode::IsDescendentNode(const std::string& name) const
{
    if(IsChildNode(name))
    {
        return true;
    }
    for(unsigned int i = 0; i < m_pNodeCollection->Count(); i++)
    {
        if(true == (*m_pNodeCollection)[i]->IsDescendentNode(name))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool ISceneNode::IsDescendentNode(const AIndex64& id) const
{
    if(IsChildNode(id))
    {
        return true;
    }
    for(unsigned int i = 0; i < m_pNodeCollection->Count(); i++)
    {
        if(true == (*m_pNodeCollection)[i]->IsDescendentNode(id))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CSceneNodeCollection*& ISceneNode::Children()
{
    return m_pNodeCollection;
}
//---------------------------------------------------------------------------------------

const CSceneNodeCollection* ISceneNode::Children() const
{
    return m_pNodeCollection;
}
//---------------------------------------------------------------------------------------

const std::string& ISceneNode::Name() const
{ 
    return m_name; 
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetName(const std::string& name)
{ 
    m_name = name; 
}
//---------------------------------------------------------------------------------------

AIndex64 ISceneNode::Id() const 
{ 
    return m_id;
}
//---------------------------------------------------------------------------------------

CMaterial* ISceneNode::Material()
{ 
    return m_pMaterial;
}
//---------------------------------------------------------------------------------------

const CMaterial* ISceneNode::Material() const
{ 
    return m_pMaterial;
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetMaterial(CMaterial*& material)
{ 
    m_pMaterial = material;
}
//---------------------------------------------------------------------------------------

bool ISceneNode::Visible() const
{ 
    if(m_pParent)
    {
        if(!m_pParent->Visible())
        {
            return false;
        }
    }
    return m_bIsVisible;
}
//---------------------------------------------------------------------------------------

void ISceneNode::SetVisible(const bool isVisible)
{ 
    m_bIsVisible = isVisible;
}
//---------------------------------------------------------------------------------------

void ISceneNode::RegisterForRendering(ILayer* pLayer)
{ 
    bool bIsVisible = Visible();
    if(bIsVisible)
    {
        RegisterSelf(pLayer);

        if(m_pNodeCollection)
        {
            AUInt32 count = m_pNodeCollection->Count();
            for(AUInt32 i = 0; i < count; i++)
            {
                (*m_pNodeCollection)[i]->RegisterForRendering(pLayer);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void ISceneNode::RegisterSelf(ILayer* pLayer)
{
    if(pLayer)
    {
        pLayer->RegisterNodeToRender(this, ESceneNodeType_Solid); //TODO: remove this hardcoding for node type
    }
}
//---------------------------------------------------------------------------------------
