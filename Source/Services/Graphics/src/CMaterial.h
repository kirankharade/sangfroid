//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_MATERIAL_H__
#define __C_MATERIAL_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "GraphicsEnums.h"
#include "CColor.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CMaterial
{

public:

    CMaterial();
    CMaterial(const CColor& ambient, const CColor& diffuse, const CColor& emissive, const CColor& specular, const AReal32& shininess);
    CMaterial(const CColor& diffuse);
    CMaterial(const CMaterial& material);
    virtual ~CMaterial( );

    CColor Ambient() const;
    void SetAmbient(const CColor& ambient);
    CColor Diffuse() const;
    void SetDiffuse(const CColor& diffuse);
    CColor Specular() const;
    void SetSpecular(const CColor& specular);
    CColor Emissive() const;
    void SetEmissive(const CColor& emissive);

    AReal32 Shininess() const;
    void SetShininess(const AReal32 shininess);

    CMaterial& operator = (const CMaterial& material);
    bool operator == (const CMaterial& material) const;
    bool operator != (const CMaterial& material) const;

private:

    CColor  m_ambient;
    CColor  m_diffuse;
    CColor  m_emissive;
    CColor  m_specular;
    AReal32  m_shininess;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_MATERIAL_H__
//---------------------------------------------------------------------------------------
