//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __GRAPHICS_PREDEFINED_EVENTS_H__
#define __GRAPHICS_PREDEFINED_EVENTS_H__
//---------------------------------------------------------------------------------------
#include "GraphicsDefines.h"
#include "GraphicsSTLExports.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

const std::string Event_PreSceneRender = "Event_PreSceneRender";
const std::string Event_PostSceneRender = "Event_PreSceneRender";

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__GRAPHICS_PREDEFINED_EVENTS_H__
//---------------------------------------------------------------------------------------
