//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_COLOR_H__
#define __C_COLOR_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "GraphicsEnums.h"
#include <string>
#include <vector>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SColorRGBAf
{
    AReal32 r;
    AReal32 g;
    AReal32 b;
    AReal32 a;
};
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API SColorARGBf
{
    AReal32 a;
    AReal32 r;
    AReal32 g;
    AReal32 b;
};
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CColor
{

public:

    CColor();
    CColor(const AReal32 red, const AReal32 green, const AReal32 blue, const AReal32 alpha = 1.0);
    CColor(const AUByte red, const AUByte green, const AUByte blue, const AUByte alpha = 255);
    CColor(const EPredefinedColor predefColor, const AUByte alpha = 255);
    CColor(const CColor& color);
    CColor(const AUInt32 color);
    CColor(const CColor& color, const AUByte alpha);
    virtual ~CColor( );

    AReal32 Red() const;
    void SetRed(const AReal32 red);

    AReal32 Green() const;
    void SetGreen(const AReal32 green);

    AReal32 Blue() const;
    void SetBlue(const AReal32 blue);

    AReal32 Alpha() const;
    void SetAlpha(const AReal32 alpha);

    AUByte RedAsByte() const;
    void SetRedAsByte(const AUByte red);

    AUByte GreenAsByte() const;
    void SetGreenAsByte(const AUByte green);

    AUByte BlueAsByte() const;
    void SetBlueAsByte(const AUByte blue);

    AUByte AlphaAsByte() const;
    void SetAlphaAsByte(const AUByte alpha);

    AReal32 Hue() const;
    void SetHue(const AReal32 hue);

    AReal32 Saturation() const;
    void SetSaturation(const AReal32 saturation);

    AReal32 Luminance() const;
    void SetLuminance(const AReal32 luminance);

    SColorRGBAf ToRGBAf() const;
    SColorARGBf ToARGBf() const;

    CColor Opaque() const;

    CColor& operator = (const CColor& color);
    bool operator == (const CColor& color) const;
    bool operator != (const CColor& color) const;

    static SColorRGBAf* ToRGBAf(const CColor* colors, const unsigned int count);
    static SColorARGBf* ToARGBf(const CColor* colors, const unsigned int count);
    static SColorRGBAf* CColor::ToRGBAf(const std::vector<CColor> colors);
    static SColorARGBf* CColor::ToARGBf(const std::vector<CColor> colors);

    static CColor Average(const CColor& colorA, const CColor& colorB);
    static CColor Average(const CColor& colorA, const CColor& colorB,
                          const CColor& colorC, const CColor& colorD);

private:

    AUInt32  m_color;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_COLOR_H__
//---------------------------------------------------------------------------------------
