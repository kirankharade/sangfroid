//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_VIEWPORT_H__
#define __I_VIEWPORT_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CPoint3f.h"
#include "GraphicsDefines.h"
#include "ICanvas.h"
#include "IWindow.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

enum EViewportAction
{
	EViewportAction_Zoom,
	EViewportAction_ZoomWindow,
	EViewportAction_TransformAlongScreenAxes,
	EViewportAction_TransformAlongSceneAxes,
	EViewportAction_TransformationHints
    // and many more
};
//---------------------------------------------------------------------------------------

class ILayer;
class IEventProcessor;
class CLayer2D;
class CLayer3D;
class IGraphicsContext;
class CBackGround;
class CCamera;
class CLightManager;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IViewport : public ICanvas
{

public:

    virtual ~IViewport() {}

	virtual long Id() const = 0;
    virtual IWindow* ParentWindow() const = 0;
    virtual void SetParentWindow(IWindow*) = 0;

    virtual CLayer2D* LayerForeground2D() const = 0;
    virtual CLayer2D* LayerBackground2D() const = 0;
    virtual CLayer3D* Layer3D() const = 0;

    virtual CBackGround* BackGround() const = 0;
    virtual void SetBackGround(CBackGround* bg) = 0;

	virtual CLightManager* LightManager() const = 0;

    virtual bool Visible() const = 0;
    virtual void SetVisible(const bool bIsVisible) = 0;

    virtual bool Enabled(const EViewportAction action) const = 0;
    virtual void SetEnabled(const EViewportAction action, const bool bEnabled) = 0;

    virtual void SetCurrentRotationCentre(const An::Maths::CPoint3f& centre) = 0;
    virtual An::Maths::CPoint3f CurrentRotationCentre() const = 0;
    virtual void ResetCurrentRotationCentre() = 0;

    virtual IEventProcessor* EventProcessor() const = 0;

    virtual void FitView() = 0;
    virtual void FitView(const bool bUpdate) = 0;
    virtual void ResetView() = 0;
    virtual void Invalidate() = 0;

protected:
    
    IViewport() {}

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_VIEWPORT_H__
//---------------------------------------------------------------------------------------

