//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_VIEWPORT_H__
#define __C_VIEWPORT_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "../Maths/CPoint3f.h"
#include "IViewport.h"
#include "GraphicsSTLExports.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class ILayer;
class IViewport;
class IWindow;
class IEventProcessor;
class CLayer3D;
class CLayer2D;
class IGraphicsContext;
class CBackGround;
class CCamera;
class CLightManager;

//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CViewport : public IViewport
{

public:

    static CViewport* New(IWindow* parentWindow);

	virtual ~CViewport();

	virtual long Id() const;
	virtual const std::string& Name() const;
	virtual void SetName(const std::string& name);

    virtual IWindow* ParentWindow() const;
    virtual void SetParentWindow(IWindow*);

    virtual CLayer2D* LayerForeground2D() const;
    virtual CLayer2D* LayerBackground2D() const;
    virtual CLayer3D* Layer3D() const;

    virtual CBackGround* BackGround() const;
    virtual void SetBackGround(CBackGround* bg);

	virtual CLightManager* LightManager() const;

    virtual bool Visible() const;
    virtual void SetVisible(const bool bIsVisible);

    virtual bool Enabled(const EViewportAction action) const;
    virtual void SetEnabled(const EViewportAction action, const bool bEnabled);

    virtual void SetCurrentRotationCentre(const Maths::CPoint3f& centre);
    virtual Maths::CPoint3f CurrentRotationCentre() const;
    virtual void ResetCurrentRotationCentre();

	virtual int Height() const;
	virtual int Width() const;
	virtual void SetSize(const int width, const int height);

    virtual IEventProcessor* EventProcessor() const;

    virtual void Render();
	virtual void Resize();
    virtual void FitView();
    virtual void FitView(const bool bUpdate);
    virtual void ResetView();
    virtual void Invalidate();

protected: 
    CViewport(IWindow* parentWindow);
    void Initialize();
    void CleanUp();
    void Update();
    void UpdateListeners();
	void BeginScene();
	void EndScene();

protected:
    IWindow*                m_parentWindow;
    CBackGround*            m_pBackground;
    CLayer2D*               m_p2DBackgroundLayer;
    CLayer2D*               m_p2DForegroundLayer;
    CLayer3D*               m_p3DLayer;
	IEventProcessor*        m_pEventProcessor;
	Maths::CPoint3f         m_currSceneRotationCentre;
	CLightManager*			m_pLightManager;
	bool                    m_bIsVisible;
	long                    m_id;
    std::string             m_name;
    //std::mutex            m_renderMutex;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_VIEWPORT_H__

