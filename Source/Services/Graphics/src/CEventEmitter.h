//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_EVENT_EMITTER_H__
#define __C_EVENT_EMITTER_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include <string>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class CEventListener;
//-----------------------------------------------------------------------------

class AN_GRAPHICS_API CEventEmitter
{

public:

	CEventEmitter();
	virtual ~CEventEmitter();

	void Register(CEventListener* listener, const std::string& eventName);
	void UnRegister(CEventListener* listener, const std::string& eventName);
	void Emit(const std::string & eventName, void* userData);

protected:

	void* m_pEventListenerMap;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_EVENT_EMITTER_H__
//---------------------------------------------------------------------------------------
