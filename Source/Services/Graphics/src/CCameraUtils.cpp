//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/MathsEnums.h"
#include "CCamera.h"
#include "CViewport.h"
#include "CCameraUtils.h"
#include "CCameraFrustum.h"
#include "CCoordinateSystem.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

float CCameraUtils::CalculateSceneExtent(CCamera* cam, 
                                        const CPoint3f& sceneCentre, 
                                        const float sceneBoundRadius, 
                                        const bool bAdjustToAspectRatio)
{
	float sceneExtent = ( 2.0f * cam->m_fitViewMargin * sceneBoundRadius);
	if(bAdjustToAspectRatio)
	{
		float minSide = (float) ( cam->ViewportHeight() > cam->ViewportWidth() ? cam->ViewportWidth() : cam->ViewportHeight() );

		float factor = 1;
		if(minSide > 0)
			factor = ( (float) cam->ViewportHeight() ) / minSide;

		sceneExtent *= factor;
	}
	return sceneExtent;
}
//---------------------------------------------------------------------------------------

void CCameraUtils::StandardViewParameters(const EPresetViewType viewType,
										const float viewExtent, 
                                        const CPoint3f& target, 
										CPoint3f& pos, 
                                        CVector3f& up)
{
	pos = target;

	float d = 10.0f * viewExtent;

	switch(viewType)
	{
	case EPresetView_Front:
		pos.z += d; 
		up = Y_AXIS;
		break;

	case EPresetView_Back:
		pos.z -= d; 
		up = Y_AXIS;
		break;

	case EPresetView_Left:
		pos.x -= d; 
		up = Y_AXIS;
		break;

	case EPresetView_Right:
		pos.x += d; 
		up = Y_AXIS;
		break;

	case EPresetView_Top:
		pos.y -= d; 
		up = Z_AXIS;
		break;

	case EPresetView_Bottom:
		pos.y += d; 
		up = Z_AXIS;
		break;

	case EPresetView_Isometric:
	case EPresetView_Undefined:
	default:
		pos.x -= d; 
		pos.y += d; 
		pos.z -= d; 
		up = Y_AXIS;
		break;
	}
}
//---------------------------------------------------------------------------------------

float CCameraUtils::GetCameraResetParameters(CCamera* cam, 
                                    const CPoint3f& sceneCentre, 
                                    const float sceneBoundRadius,
                                    const EPresetViewType viewType,
								    CCoordinateSystem* pCoordSystem, 
                                    CPoint3f& target, 
								    CPoint3f& pos,
                                    CVector3f& up)
{
	float extent = CalculateSceneExtent(cam, sceneCentre, sceneBoundRadius, true);
	target = sceneCentre;
	StandardViewParameters(viewType, extent, target, pos, up);
	if(pCoordSystem)
	{
		CMatrix4f csMat = pCoordSystem->AbsoluteTransform();
        pos = csMat.transform(pos);
        up = csMat.rotate(up);
	}
	return extent;
}
//---------------------------------------------------------------------------------------

CPoint3f CCameraUtils::ModelToScreenCoords(CCamera* cam, const CPoint3f& point)
{
    CMatrix4f modelView = cam->ViewMatrix();
    CMatrix4f projection = cam->ProjectionMatrix();
    int viewportX = cam->ViewportXPosition();
    int viewportY = cam->ViewportYPosition();
    int viewportWidth = cam->ViewportWidth();
    int viewportHeight = cam->ViewportHeight();

    CPoint3f outPoint;

    if(!Project(point, modelView, projection, viewportX, viewportY, viewportWidth, viewportHeight, outPoint))
    {
        throw "Cannot project point.";
    }

    return outPoint;
}
//---------------------------------------------------------------------------------------

CPoint3f CCameraUtils::ScreenToModelCoords(CCamera* cam, const CPoint3f& point)
{
    CMatrix4f modelView = cam->ViewMatrix();
    CMatrix4f projection = cam->ProjectionMatrix();
    int viewportX = cam->ViewportXPosition();
    int viewportY = cam->ViewportYPosition();
    int viewportWidth = cam->ViewportWidth();
    int viewportHeight = cam->ViewportHeight();

    CPoint3f outPoint;

    if(!UnProject(point, modelView, projection, viewportX, viewportY, viewportWidth, viewportHeight, outPoint))
    {
        throw "Cannot un-project point.";
    }

    return outPoint;
}
//---------------------------------------------------------------------------------------

bool CCameraUtils::UnProject(const CPoint3f& point, 
                            const CMatrix4f& modelView, 
                            const CMatrix4f& projection, 
                            const int viewportX, 
                            const int viewportY, 
                            const int viewportWidth, 
                            const int viewportHeight,
                            Maths::CPoint3f& outPoint)
{
    CMatrix4f mat = (projection * modelView).inverse();

    float p[4];
    float q[4];

    p[0] = point.x;
    p[1] = viewportHeight - point.y; //Only true for microsoft windows
    p[2] = point.z;
    p[3] = 1.0f;

    //Map input point to viewport
    p[0] = (p[0] - viewportX) / viewportWidth;
    p[1] = (p[1] - viewportY) / viewportHeight;

    p[0] = (p[0] * 2) - 1;
    p[1] = (p[1] * 2) - 1;
    p[2] = (p[2] * 2) - 1;

    //Apply matrix
    for(int i = 0; i < 4; i++)
    {
        q[i] = (p[0] * mat.e[i][0]) + (p[1] * mat.e[i][1]) + (p[2] * mat.e[i][2]) + (p[3] * mat.e[i][3]);
    }

    float w = q[3];

    if (EQ_32(w, 0))
    {
        return false;
    }

    //Normalize
    outPoint.x =  q[0] / w;
    outPoint.x =  q[1] / w;
    outPoint.x =  q[2] / w;

    return true;
}
//---------------------------------------------------------------------------------------

bool CCameraUtils::Project(const CPoint3f& point, 
                            const CMatrix4f& modelView, 
                            const CMatrix4f& projection, 
                            const int viewportX, 
                            const int viewportY, 
                            const int viewportWidth, 
                            const int viewportHeight,
                            Maths::CPoint3f& outPoint)
{
    CMatrix4f mat = projection * modelView;

    float p[4];
    float q[4];

    p[0] = point.x;
    p[1] = point.y;
    p[2] = point.z;
    p[3] = 1.0f;

    //aply matrix
    for(int i = 0; i < 4; i++)
    {
        q[i] = (p[0] * mat.e[i][0]) + (p[1] * mat.e[i][1]) + (p[2] * mat.e[i][2]) + (p[3] * mat.e[i][3]);
    }

    float w = q[3];

    if(EQ_32(w, 0.0))
    {
        return false;
    }

    //Normalize
    q[0] /= w;
    q[1] /= w;
    q[2] /= w;

    q[0] = (q[0] * 0.5f) + 0.5f;
    q[1] = (q[1] * 0.5f) + 0.5f;
    q[2] = (q[2] * 0.5f) + 0.5f;

    q[0] = (q[0] * viewportWidth) + viewportX;
    q[1] = (q[1] * viewportHeight) + viewportY;

    outPoint.x =  q[0];
    outPoint.x =  viewportHeight - q[1]; //Only true for microsoft windows
    outPoint.x =  q[2];

    return true;
}
//---------------------------------------------------------------------------------------

float CCameraUtils::GetProjectedTranslationAlongVector(CCamera* cam, 
                                               const CPoint3f& dirBasePt, 
											   const CPoint3f& dirEndPt, 
											   const CPoint3f& lastMousePosition,
											   const CPoint3f& currMousePosition)
{
	float worldD = (dirEndPt - dirBasePt).magnitude();
	CPoint3f screenDirBasePoint = ModelToScreenCoords(cam, dirBasePt);
	CPoint3f screenDirEndPoint = ModelToScreenCoords(cam, dirEndPt);
	CVector3f screenDirVec = screenDirEndPoint - screenDirBasePoint;
	float screenD = screenDirVec.magnitude();

	CVector3f screenMovementVec = currMousePosition - lastMousePosition;
	float screenMovementDist = screenMovementVec.magnitude();

	//Find the angle and direction between 2 screen vectors
	float dir = 1.0f;

	screenMovementVec.normalize();
	screenDirVec.normalize();


    float angleDeg = screenMovementVec.angleDeg(screenDirVec);

	// flip direction
	if(angleDeg > 120.0f)dir = -dir;

	float angleRad = (float) (DEG_TO_RAD64(angleDeg));

	//conversion factor for screen movement to world
	float dotProduct = screenMovementVec.dot(screenDirVec);
	float conversionFactor = fabs(dotProduct) * (worldD / screenD) * screenMovementDist;
	return dir * conversionFactor* fabs( cos( angleRad ) );
}
//---------------------------------------------------------------------------------------

//CPoint3f CCameraUtils::ProjectedMousePositionOnPlane(CCamera* cam,
//                                    const int mousePosX, 
//                                    const int mousePosY, 
//                                    const Maths::CPlane& plane)
//{
//    ILayer* pLayer = dynamic_cast<ILayer*> ( m_pParentLayer );
//    IViewPort* pViewPort = pLayer->GetViewport();
//
//    Maths::Vector2D<int> mousePosition;
//    mousePosition.X = input.Point.X;
//    mousePosition.Y = input.Point.Y;
//    ICollisionPerformer* collMan = pViewPort->GetVisible3DLayer()->GetCollisionPerformer();
//    CoreMaths::CLine3f rayLine = collMan->GetRayFromScreenCoordinates(eSL_USER_SPECIFIED_SELECTION,mousePosition,  
//        pViewPort->GetVisible3DLayer());
//
//    CPoint3f point;
//    plane.GetIntersectionWithLine(rayLine.m_Start, rayLine.GetVector(), point);
//    return point;
//}
//---------------------------------------------------------------------------------------

