/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once

#include <iostream>
#include "IOExports.h"
#include "IONamespace.h"
#include "IDataStream.h"

using namespace std;

IO_NAMESPACE_START
//---------------------------------------------------------------------

class IO_IMP_EXP Reader
{

private:

   void*  	m_pImpl;

public:

   Reader();

   ~Reader();

	IDataStream* Load(const char* file, const char* fileFormat);
};
//---------------------------------------------------------------------
IO_NAMESPACE_END

