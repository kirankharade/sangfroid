/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once
#pragma warning( disable : 4996 )
#include <iostream>
#include <sstream>
#include "IDataStream.h"
#include "TriangleMesh.h"

using namespace std;

IO_NAMESPACE_START

typedef unsigned char byte;

//---------------------------------------------------------------------

class STLDataStreamImpl
{

private:
   std::ifstream					   m_inFileStream;
	std::string 					   m_fileName; 

public:

   STLDataStreamImpl(const std::string& file);
   virtual ~STLDataStreamImpl();

   Sangfroid::GeometryStructures::TriangleMesh* Mesh();

private:

    Sangfroid::GeometryStructures::TriangleMesh* read(const byte* pByteStream, int streamLength);

};

//---------------------------------------------------------------------
IO_NAMESPACE_END


