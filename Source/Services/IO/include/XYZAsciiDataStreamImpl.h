/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma once
#pragma warning( disable : 4996 )
#include <iostream>
#include <fstream>
#include "IPointDataStream.h"
#include "Point3f.h"

using namespace std;

IO_NAMESPACE_START
//---------------------------------------------------------------------

class XYZAsciiDataStreamImpl
{

private:
    Sangfroid::Maths::Point3f	m_min;
    Sangfroid::Maths::Point3f	m_max;
    int					        m_numPoints;
    std::ifstream				m_inFileStream;
    std::string 				m_fileName;

public:

    XYZAsciiDataStreamImpl(const std::string& file);
    virtual ~XYZAsciiDataStreamImpl();

    const Sangfroid::Maths::Point3f& Min();
    const Sangfroid::Maths::Point3f& Max();

    int PointCount() const;

    Sangfroid::Maths::Point3f First();
    Sangfroid::Maths::Point3f Next();

private:

    void reset();
    void calculate();
    bool readNextPoint(double& x, double& y, double& z);
};

//---------------------------------------------------------------------
IO_NAMESPACE_END
