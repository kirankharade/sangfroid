/*-----------------------------------------------------------------------------------------*
* SANGFROID FRAMEWORK                                                                     *
* Copyright(C) [2016-2018] Kiran Amrut Kharade. All Rights Reserved.                      *
* This source code and the information herein is a property of Kiran Amrut Kharade. It is *
* strictly forebidden to duplicate, reproduce or disseminate in part or full without a    *
* prior written permission of Kiran Amrut Kharade.                                        *
* @Author Kiran A. Kharade <KiranAKharade@gmail.com>                                      *
*-----------------------------------------------------------------------------------------*/

#pragma warning( disable : 4996 )
#pragma once

#include <iostream>
#include "Point3f.h"
#include "IOExports.h"
#include "IONamespace.h"

#include "IDataStream.h"

using namespace std;

IO_NAMESPACE_START
//---------------------------------------------------------------------

class IO_IMP_EXP IPointDataStream : public IDataStream
{

public:

   IPointDataStream() {}
   virtual ~IPointDataStream() {}

	virtual Sangfroid::Maths::Point3f Min() const = 0;
	virtual Sangfroid::Maths::Point3f Max() const = 0;

	virtual int PointCount() const = 0;

	virtual Sangfroid::Maths::Point3f First() = 0;
	virtual Sangfroid::Maths::Point3f Next() = 0;
};

//---------------------------------------------------------------------
IO_NAMESPACE_END

