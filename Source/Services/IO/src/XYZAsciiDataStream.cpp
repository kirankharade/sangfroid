#pragma once
#pragma warning( disable : 4996 )
#include <iostream>
#include <fstream>

#include "XYZAsciiDataStream.h"
#include "XYZAsciiDataStreamImpl.h"

using namespace std;
using namespace Sangfroid::IO;
using namespace Sangfroid::Maths;

/*-----------------------------------------------------------------------------------*/

XYZAsciiDataStream::XYZAsciiDataStream(const char* file)
    :m_pImpl(nullptr)
{
    std::string str(file);
    m_pImpl = new XYZAsciiDataStreamImpl(str);
}

XYZAsciiDataStream::~XYZAsciiDataStream()
{
    if (m_pImpl)
    {
        delete m_pImpl;
    }
}

const char* XYZAsciiDataStream::Type() const
{
    return "ASCII-cloud";
}

Sangfroid::Maths::Point3f XYZAsciiDataStream::Min() const
{
    return ((XYZAsciiDataStreamImpl*)m_pImpl)->Min();
}

Sangfroid::Maths::Point3f XYZAsciiDataStream::Max() const
{
    return ((XYZAsciiDataStreamImpl*)m_pImpl)->Max();
}

int XYZAsciiDataStream::PointCount() const
{
    return ((XYZAsciiDataStreamImpl*)m_pImpl)->PointCount();
}

Sangfroid::Maths::Point3f XYZAsciiDataStream::First()
{
    return ((XYZAsciiDataStreamImpl*)m_pImpl)->First();
}

Sangfroid::Maths::Point3f XYZAsciiDataStream::Next()
{
    return ((XYZAsciiDataStreamImpl*)m_pImpl)->Next();
}

/*-----------------------------------------------------------------------------------*/
