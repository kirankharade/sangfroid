#pragma once
#pragma warning( disable : 4996 )
#include <iostream>
#include <fstream>


#include "STLDataStream.h"
#include "STLDataStreamImpl.h"
#include <sys/stat.h>
#include <ctime>
#include "TriangleMesh.h"
#include "PointBuffer.h"

using namespace std;
using namespace Sangfroid::GeometryStructures;

IO_NAMESPACE_START

/*-----------------------------------------------------------------------------------*/

bool fileExists(const std::string& filePath)
{
   struct stat fileStats;

   auto s = stat(filePath.c_str(), &fileStats);

   // Check file existance
   if (s != 0)
   {
      return false;
   }
   return true;
}

STLDataStream::STLDataStream(const char* file)
   :m_pImpl(nullptr)
{
   std::string str(file);

   if (!fileExists(file))
   {
      throw "File does not exist.";
   }
   m_pImpl = new STLDataStreamImpl(str);
}

STLDataStream::~STLDataStream()
{
   if (m_pImpl)
   {
      delete m_pImpl;
   }
}

const char* STLDataStream::Type() const
{
   return "STL";
}

TriangleMesh* STLDataStream::Mesh()
{
   return ((STLDataStreamImpl*)m_pImpl)->Mesh();
}
/*-----------------------------------------------------------------------------------*/

IO_NAMESPACE_END
