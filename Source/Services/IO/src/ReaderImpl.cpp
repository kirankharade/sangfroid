#pragma once

#include "ReaderImpl.h"
#include "STLDataStream.h"
#include "XYZAsciiDataStream.h"
#include <algorithm>

using namespace std;
using namespace Sangfroid::IO;

/*-----------------------------------------------------------------------------------*/

ReaderImpl::ReaderImpl() 
{
}

ReaderImpl::~ReaderImpl() 
{
}

IDataStream* ReaderImpl::Load(const char* file, const char* format)
{
	std::string f = format;
	//std::transform(f.begin(), f.end(), f.begin(), ::tolower);

	if (f == "ASCII-cloud" || f == "xyz" || f == "ascii")
	{
		XYZAsciiDataStream* st = new XYZAsciiDataStream(file);
		return st;
	}
	if (f == "stl" || f == "STL")
	{
		STLDataStream* st = new STLDataStream(file);
		return st;
	}
	return nullptr;
}

/*-----------------------------------------------------------------------------------*/


