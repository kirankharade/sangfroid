#pragma warning( disable : 4996 )
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "STLDataStreamImpl.h"
#include "TriangleMesh.h"
#include "MathsIncludes.h"
#include "Vector3f.h"
#include "Point3f.h"
#include <sys/stat.h>
#include <ctime>

using namespace std;
using namespace Sangfroid;
using namespace Sangfroid::Maths;
using namespace Sangfroid::IO;
using namespace Sangfroid::GeometryStructures;

/*-----------------------------------------------------------------------------------*/

STLDataStreamImpl::STLDataStreamImpl(const std::string& file)
	:m_fileName(file)
{
}

STLDataStreamImpl::~STLDataStreamImpl()
{
	if (m_inFileStream.is_open())
	{
		m_inFileStream.close();
	}
}

int long getFileSize(const std::string& filename)
{

   struct stat fileStats;

   // Check file existance
   if (stat(filename.c_str(), &fileStats) != 0)
   {
      return 0;
   }

   // File size in bytes
   return (int long) fileStats.st_size;

}

static size_t indexOfNextCharacter(const byte* byteStream, const size_t length, const size_t startIndex)
{
   char LINE_RETURN_CHAR = '\n';
   char SPACE_CHAR = ' ';
   size_t index = startIndex;

   while (byteStream[index] == SPACE_CHAR || byteStream[index] == LINE_RETURN_CHAR)
   {
      if (index >= length)
      {
         throw "The index provided is outside the bounds of the array.";
      }

      index++;
   }

   return index;
}

TriangleMesh* loadBinarySTLIntoMesh(const byte* byteStream, int startIndex, int triangleCount)
{
   int vertexCount = triangleCount * 3;
   auto posData = new float[vertexCount * 3];
   auto normalData = new float[triangleCount * 3];

   int streamIndex = startIndex;

   float* posPtr = posData;
   float* normalPtr = normalData;

   for (int triIndex = 0; triIndex < triangleCount; ++triIndex)
   {
      //Read normal
	   memcpy(normalPtr, byteStream + streamIndex, sizeof(Vector3f)); streamIndex += sizeof(Vector3f);
	   normalPtr += 3;

      //Read vertex 0
	   memcpy(posPtr, byteStream + streamIndex, sizeof(Point3f)); streamIndex += sizeof(Point3f);
	   posPtr += 3;

      //Read vertex 1
	   memcpy(posPtr, byteStream + streamIndex, sizeof(Point3f)); streamIndex += sizeof(Point3f);
	   posPtr += 3;

      //Read vertex 2
	   memcpy(posPtr, byteStream + streamIndex, sizeof(Point3f)); streamIndex += sizeof(Point3f);
	   posPtr += 3;

      //Increment the streamIndex by 2 for the attribute
      streamIndex += 2;


   }

   auto ptbuffer = new PointBuffer(posData, vertexCount);
   auto nmbuffer = new NormalBuffer(normalData, triangleCount);

   return new TriangleMesh(ptbuffer, nmbuffer, nullptr);
}

static TriangleMesh* loadAsciiSTLIntoMesh(const byte* byteStream, unsigned int streamLength)
{
   const std::string KEYWORD_NORMAL("facet normal");
   const std::string KEYWORD_VERTEX("vertex");
   const char KEYWORD_NORMAL_FIRST_CHAR = 'f';
   const char KEYWORD_VERTEX_FIRST_CHAR = 'v';

   std::vector<Point3f> vertices;
   std::vector<Vector3f> normals;

   try
   {
      int approxSizePerTriangle = 140;
      unsigned int approxNumOfTriangles = streamLength / approxSizePerTriangle;
      vertices.reserve(approxNumOfTriangles * 3); //3 vertices per triangle
      normals.reserve(approxNumOfTriangles);
   }
   catch (...)
   {
      throw "Read STL: Insufficient system resources.";
   }

   float i = 0, j = 0, k = 0; //This must be float not double
   float x = 0, y = 0, z = 0; //This must be float not double

   char LINE_RETURN_CHAR = '\n';
   char SPACE_CHAR = ' ';

   size_t index = 0;
   std::string readData;

   try
   {
      while (index < streamLength)
      {
         char currentChar = byteStream[index];

         if (currentChar == KEYWORD_NORMAL_FIRST_CHAR)
         {
            readData = std::string((char*)(byteStream + index), KEYWORD_NORMAL.size());

            if (readData == KEYWORD_NORMAL)
            {
               index += KEYWORD_NORMAL.size();

               //Loop through until the next character appears...
               //This is required in case there is more than one space between the numbers
               size_t startIndex = index = indexOfNextCharacter(byteStream, streamLength, index);

               //Read where the 3 numbers end
               for (int numbersCount = 0; numbersCount < 3; numbersCount++)
               {
                  while (byteStream[index] != SPACE_CHAR &&
                     byteStream[index] != LINE_RETURN_CHAR)
                  {
                     index++;
                  }

                  //Loop through until the next character appears...
                  //This is required in case there is more than one space between the numbers
                  index = indexOfNextCharacter(byteStream, streamLength, index);
               }

               readData = std::string((char*)(byteStream + startIndex), (int)(index - startIndex));

               int readCount = std::sscanf(readData.c_str(), "%f %f %f", &i, &j, &k);
               if (readCount == 3)
               {
                  normals.push_back(Vector3f(i, j, k));
               }
            }
            else { index++; }
         }
         else if (currentChar == KEYWORD_VERTEX_FIRST_CHAR)
         {
            readData = std::string((char*)(byteStream + index), KEYWORD_VERTEX.size());

            if (readData == KEYWORD_VERTEX)
            {
				index += KEYWORD_VERTEX.size();

               //Loop through until the next character appears...
               //This is required in case there is more than one space between the numbers
               size_t startIndex = index = indexOfNextCharacter(byteStream, streamLength, index);

               //Read where the 3 numbers end
               for (int numbersCount = 0; numbersCount < 3; numbersCount++)
               {
                  while (byteStream[index] != SPACE_CHAR &&
                     byteStream[index] != LINE_RETURN_CHAR)
                  {
                     index++;
                  }

                  //Loop through until the next character appears...
                  //This is required in case there is more than one space between the numbers
                  index = indexOfNextCharacter(byteStream, streamLength, index);
               }

               readData = std::string((char*)(byteStream + startIndex), (int)(index - startIndex));

               int readCount = std::sscanf(readData.c_str(), "%f %f %f", &x, &y, &z);
               if (readCount == 3)
               {
                  vertices.push_back(Point3f(x, y, z));
               }
            }
            else { index++; }
         }
         else
         {
            index++;
         }
      }
   }
   catch (...)
   {
      throw "The STL file provided could not be loaded : Unable to read data in stream.";
   }

   Point3f* pVertices = NULL;
   if (vertices.size() > 0)
   {
      pVertices = &vertices[0];
   }

   Vector3f* pNormals = NULL;
   if (normals.size() > 0)
   {
      pNormals = &normals[0];
   }

   size_t vertexCount = vertices.size();
   int GvertexCount = (int)vertices.size();

   int triangleCount = (int) (vertexCount/3);
   auto posData = new float[vertexCount * 3];
   auto normalData = new float[triangleCount * 3];

   float* posPtr = posData;
   float* normalPtr = normalData;

   for (int it = 0; it < triangleCount; it++)
   {
	   auto v0 = vertices[(it * 3) + 0];
	   auto v1 = vertices[(it * 3) + 1];
	   auto v2 = vertices[(it * 3) + 2];
	   auto n = normals[it];

	   memcpy(normalPtr, &n, sizeof(Vector3f));
	   normalPtr += 3;

	   //Read vertex 0
	   memcpy(posPtr, &v0, sizeof(Point3f));
	   posPtr += 3;

	   //Read vertex 1
	   memcpy(posPtr, &v1, sizeof(Point3f));
	   posPtr += 3;

	   //Read vertex 2
	   memcpy(posPtr, &v2, sizeof(Point3f));
	   posPtr += 3;

   }

   auto ptbuffer = new PointBuffer(posData, (int) vertexCount);
   auto nmbuffer = new NormalBuffer(normalData, triangleCount);

   return new TriangleMesh(ptbuffer, nmbuffer, nullptr);
}

TriangleMesh* STLDataStreamImpl::Mesh()
{
   TriangleMesh* mesh = nullptr;
   int long fileSize = getFileSize(m_fileName);

   if (fileSize <= 0)
   {
      throw "Read STL: File is empty.";
   }

   //Try and read the file into a byte array...
   //We may not have enough memory to do this so an exception is thrown
   unsigned char* pBuffer = nullptr;
   try
   {
      pBuffer = new byte[(int)fileSize];
   }
   catch (std::bad_alloc)
   {
      throw "Read STL: Out of memory.";
   }

   FILE* fp = fopen(m_fileName.c_str(), "rb");
   fread(pBuffer, (int)fileSize, 1, fp);
   fclose(fp);

   mesh = read(pBuffer, (int)fileSize);
   delete[](pBuffer);

   return mesh;
}

static bool isBinary(const byte* byteStream, unsigned int streamLength)
{
   const int HEADER_LENGTH = 84;
   if (streamLength < HEADER_LENGTH) return false;

   const int CHECKED_CHUNK_SIZE = 256;
   const int BUFFER_LENGTH = std::min((int)streamLength, HEADER_LENGTH + CHECKED_CHUNK_SIZE);

   for (int i = HEADER_LENGTH; i < BUFFER_LENGTH; i++)
   {
      byte c = byteStream[i];

      //if it's greater than 127 then it's outside the normal ASCII range
      if (c > 127)
      {
         return true;
      }
   }
   return false;
}


TriangleMesh* STLDataStreamImpl::read(const byte* pByteStream, int streamLength)
{
   if (pByteStream == NULL || streamLength == 0)
   {
      throw "Read STL: Insufficient data supplied.";
   }

   TriangleMesh* mesh = nullptr;

   if (isBinary(pByteStream, streamLength))
   {
      //Check there is at least the header in the file
      const int HEADER_LENGTH = 80;
      const int TRIANGLE_COUNT_LENGTH = 4;

      if (streamLength < (unsigned int)(HEADER_LENGTH +
         TRIANGLE_COUNT_LENGTH))
      {
         throw "Read STL: Insufficient data supplied.";
      }

      //Check there is some triangle data in the file. If there isn't, an empty CTriangulation
      //will be returned
      int dataLength = streamLength - (HEADER_LENGTH + TRIANGLE_COUNT_LENGTH);
      if (dataLength > 0)
      {
         if (dataLength % 50 != 0)
         {
            throw "Read STL: Insufficient data supplied.";
         }

         int numTriangles = dataLength / 50;

         mesh = loadBinarySTLIntoMesh(pByteStream, streamLength - dataLength, numTriangles);
      }
   }
   else
   {
      mesh = loadAsciiSTLIntoMesh(pByteStream, streamLength);
   }

   return mesh;
}

//void CIO_STL_impl::WriteASCII(const CTriangulation& triangulation, const std::string& filename)
//{
//   try
//   {
//      FILE* pFile = CFileUtils::OpenFile(filename, CFileUtils::EFileOpenMode_WriteASCII);
//      if (!pFile)
//      {
//         throw CGLiDEException("The STL file could not be saved : Unable to create file.");
//      }
//
//      int triangleCount = triangulation.get_TriangleCount();
//
//      CBaseArray<int>* pIndices = triangulation.get_Indices();
//      CBaseArray<Point3f>* pPoints = triangulation.get_Points();
//
//      //STL uses facet normals.
//      triangulation.m_pImpl->CalculateFacetNormals();
//      CArray<Vector3f>* pNormals = triangulation.m_pImpl->m_pFacetNormals;
//
//      fputs("solid Output by GLiDE\r\n", pFile);
//
//      int idx0, idx1, idx2;
//      Point3f position;
//      Vector3f normal;
//      for (int triIndex = 0; triIndex < triangleCount; ++triIndex)
//      {
//         idx0 = (triIndex * 3) + 0;
//         idx1 = (triIndex * 3) + 1;
//         idx2 = (triIndex * 3) + 2;
//
//         if (triangulation.get_UseIndices())
//         {
//            idx0 = pIndices->operator[](idx0);
//            idx1 = pIndices->operator[](idx1);
//            idx2 = pIndices->operator[](idx2);
//         }
//
//         normal = pNormals->operator[](triIndex);
//         fprintf(pFile, "facet normal %f %f %f\r\n", normal.i, normal.j, normal.k);
//
//         fputs("outer loop\r\n", pFile);
//
//         position = pPoints->operator[](idx0);
//         fprintf(pFile, "vertex %f %f %f\r\n", position.x, position.y, position.z);
//
//         position = pPoints->operator[](idx1);
//         fprintf(pFile, "vertex %f %f %f\r\n", position.x, position.y, position.z);
//
//         position = pPoints->operator[](idx2);
//         fprintf(pFile, "vertex %f %f %f\r\n", position.x, position.y, position.z);
//
//         fputs("endloop\r\n", pFile);
//         fputs("endfacet\r\n", pFile);
//      }
//
//      fputs("endsolid\r\n", pFile);
//
//      std::fflush(pFile);
//      std::fclose(pFile);
//   }
//   catch (...)
//   {
//      throw CGLiDEException("The STL file could not be saved : An unexpected error occurred.");
//   }
//}
//

//void CIO_STL_impl::WriteBinary(const CTriangulation& triangulation, const std::string& filename)
//{
//   // Binary STL files consist of a 80 byte header line that can be interpreted as a comment string. 
//   // The following 4 bytes interpreted as a long integer give the total number of facets. 
//   // What follows is a normal and 3 vertices for each facet, 
//   // each coordinate represented as a 4 byte floating point number (12 bytes in all). 
//   // There is a 2 byte spacer between each facet. 
//   // The result is that each facet is represented by 50 bytes, 12 for the normal, 
//   // 36 for the 3 vertices, and 2 for the spacer. 
//
//   try
//   {
//      FILE* pFile = CFileUtils::OpenFile(filename, CFileUtils::EFileOpenMode_WriteBinary);
//      if (!pFile)
//      {
//         throw CGLiDEException("The STL file could not be saved : Unable to create file.");
//      }
//
//      int triangleCount = triangulation.get_TriangleCount();
//
//      CBaseArray<int>* pIndices = triangulation.get_Indices();
//      CBaseArray<Point3f>* pPoints = triangulation.get_Points();
//
//      //STL uses facet normals.
//      triangulation.m_pImpl->CalculateFacetNormals();
//      CArray<Vector3f>* pNormals = triangulation.m_pImpl->m_pFacetNormals;
//
//      //write 80 byte header
//      fprintf(pFile, "%-80.80s", "Binary STL Output by GLiDE");
//
//      //write the facet count
//      fwrite(&triangleCount, sizeof(int), 1, pFile);
//
//      int idx0, idx1, idx2;
//      short spacer = 0;
//      Point3f position;
//      Vector3f normal;
//
//      //write 50 byte blocks per triangle
//      for (int triIndex = 0; triIndex < triangleCount; ++triIndex)
//      {
//         idx0 = (triIndex * 3) + 0;
//         idx1 = (triIndex * 3) + 1;
//         idx2 = (triIndex * 3) + 2;
//
//         if (triangulation.get_UseIndices())
//         {
//            idx0 = pIndices->operator[](idx0);
//            idx1 = pIndices->operator[](idx1);
//            idx2 = pIndices->operator[](idx2);
//         }
//
//         //write normal (12 bytes)
//         normal = pNormals->operator[](triIndex);
//         fwrite(&normal, sizeof(Vector3f), 1, pFile);
//
//         //write vertices (36 bytes)
//         position = pPoints->operator[](idx0);
//         fwrite(&position, sizeof(Point3f), 1, pFile);
//
//         position = pPoints->operator[](idx1);
//         fwrite(&position, sizeof(Point3f), 1, pFile);
//
//         position = pPoints->operator[](idx2);
//         fwrite(&position, sizeof(Point3f), 1, pFile);
//
//         //write 2 byte spacer
//         fwrite(&spacer, sizeof(short), 1, pFile);
//      }
//
//      std::fflush(pFile);
//      std::fclose(pFile);
//   }
//   catch (...)
//   {
//      throw CGLiDEException("The STL file could not be saved : An unexpected error occurred.");
//   }
//}
//
//----------Private Methods----------//

