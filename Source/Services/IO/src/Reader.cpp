#pragma once

#include "Reader.h"
#include "ReaderImpl.h"
#include "IDataStream.h"

using namespace std;
using namespace Sangfroid::IO;

/*-----------------------------------------------------------------------------------*/

Reader::Reader() 
:m_pImpl(nullptr)
{
   m_pImpl = new ReaderImpl();
}

Reader::~Reader() 
{
   if (m_pImpl)
   {
      delete m_pImpl;
   }
}

IDataStream* Reader::Load(const char* file, const char* format)
{
   if (m_pImpl)
   {
      return ((ReaderImpl*)m_pImpl)->Load(file, format);
   }
   return nullptr;
}

/*-----------------------------------------------------------------------------------*/


