#pragma warning( disable : 4996 )
#pragma once

#include <iostream>
#include <fstream>
#include <vector>

#include "XYZAsciiDataStreamImpl.h"

using namespace std;
using namespace Sangfroid::IO;
using namespace Sangfroid::Maths;

/*-----------------------------------------------------------------------------------*/

XYZAsciiDataStreamImpl::XYZAsciiDataStreamImpl(const std::string& file)
    :m_fileName(file),
    m_numPoints(0)
{
    calculate();
    reset();
}

XYZAsciiDataStreamImpl::~XYZAsciiDataStreamImpl()
{
    if (m_inFileStream.is_open())
    {
        m_inFileStream.close();
    }
}

const Sangfroid::Maths::Point3f& XYZAsciiDataStreamImpl::Min()
{
    return m_min;
}

const Sangfroid::Maths::Point3f& XYZAsciiDataStreamImpl::Max()
{
    return m_max;
}

int XYZAsciiDataStreamImpl::PointCount() const
{
    return m_numPoints;
}

Sangfroid::Maths::Point3f XYZAsciiDataStreamImpl::First()
{
    reset();

    double x = 0, y = 0, z = 0;
    if (readNextPoint(x, y, z))
    {
        return Sangfroid::Maths::Point3f((float)x, (float)y, (float)z);
    }

    throw "Unable to read first point from file " + m_fileName;
}

Sangfroid::Maths::Point3f XYZAsciiDataStreamImpl::Next()
{
    double x = 0, y = 0, z = 0;
    if (readNextPoint(x, y, z))
    {
        return Sangfroid::Maths::Point3f((float)x, (float)y, (float)z);
    }
    throw "Unable to read next point from file " + m_fileName;
}

void XYZAsciiDataStreamImpl::reset()
{
    if (m_inFileStream.is_open())
    {
        m_inFileStream.close();
    }
    m_inFileStream.open(m_fileName.c_str(), std::ios::in);
}

void XYZAsciiDataStreamImpl::calculate()
{
    reset();

    Sangfroid::Maths::Point3f min(FLT_MAX, FLT_MAX, FLT_MAX);
    Sangfroid::Maths::Point3f max(-FLT_MAX, -FLT_MAX, -FLT_MAX);

    std::vector<Point3f> points;

    double x = 0, y = 0, z = 0;
    while (readNextPoint(x, y, z))
    {
        points.push_back(Point3f((float)x, (float)y, (float)z));

        if ((float)x < min.x)	min.x = (float)x;
        if ((float)y < min.y)	min.y = (float)y;
        if ((float)z < min.z)	min.z = (float)z;

        if ((float)x >= max.x)	max.x = (float)x;
        if ((float)y >= max.y)	max.y = (float)y;
        if ((float)z >= max.z)	max.z = (float)z;

        m_numPoints++;
    }

    m_min = min;
    m_max = max;
}

bool XYZAsciiDataStreamImpl::readNextPoint(double& x, double& y, double& z)
{
	char str[1024];
	m_inFileStream.getline(str, 1023);

    auto& streamx = m_inFileStream >> x;
    if (streamx.eof())
    {
        return false;
    }
    auto& streamy = m_inFileStream >> y;
    if (streamy.eof())
    {
        return false;
    }
    auto& streamz = m_inFileStream >> z;
    if (streamz.eof())
    {
        return false;
    }
    return true;
}

/*-----------------------------------------------------------------------------------*/
