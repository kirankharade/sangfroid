﻿using System.Windows.Controls;
using System.Windows.Input;

namespace Dazzle.RoutedCommands
{
    public static class RoutedCommands
    {
        public static RoutedCommand Click = new RoutedCommand("Click", typeof(Border));
    }
}
