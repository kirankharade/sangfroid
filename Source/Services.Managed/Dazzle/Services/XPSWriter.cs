﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;

namespace Dazzle
{
    public class XPSWriter
    {

        public static FixedDocument CreateFixedDocument(UIElement ctrl)
        {
            FixedDocument doc = new FixedDocument();
            doc.DocumentPaginator.PageSize = new Size(96 * 8.5, 96 * 11);
            PageContent page = new PageContent();
            FixedPage fixedPage = CreateOneFixedPage(ctrl);
            ((IAddChild)page).AddChild(fixedPage);

            doc.Pages.Add(page);
            return doc;

        }

        private static FixedPage CreateOneFixedPage(UIElement ctrl)
        {
            FixedPage page = new FixedPage();
            page.Background = Brushes.White;
            page.Width = 96 * 8.5;
            page.Height = 96 * 11;

            //Border b = new Border();
            //b.BorderThickness = new Thickness(1);
            //b.BorderBrush = Brushes.Black;
            //b.Child = ctrl;
            //FixedPage.SetLeft(b, 96 * 2);
            //FixedPage.SetTop(b, 96 * 2);

            
            try
            {
                var child = ctrl as FrameworkElement;
                if(child != null && child.Parent != null)
                {
                    var parent = LogicalTreeHelper.GetParent(child);
                    var parentAsPanel = parent as Panel;
                    if (parentAsPanel != null)
                    {
                        parentAsPanel.Children.Remove(child);
                    }
                    var parentAsContentControl = parent as ContentControl;
                    if (parentAsContentControl != null)
                    {
                        parentAsContentControl.Content = null;
                    }
                    var parentAsDecorator = parent as Decorator;
                    if (parentAsDecorator != null)
                    {
                        parentAsDecorator.Child = null;
                    }
                    page.Children.Add(child);
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            Size sz = new Size(96 * 8.5, 96 * 11);
            page.Measure(sz);
            page.Arrange(new Rect(new Point(), sz));
            page.UpdateLayout();

            return page;
        }

        public static void CreateXPSDocument(UIElement ctrl)
        {
            FixedDocument doc = CreateFixedDocument(ctrl);
            XpsDocument xpsd = new XpsDocument(@"C:\output.xps", FileAccess.ReadWrite);
            XpsDocumentWriter xw = XpsDocument.CreateXpsDocumentWriter(xpsd);
            xw.Write(doc);
            xpsd.Close();
        }
    }
}
