﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Threading;

namespace Dazzle
{
    public static class UIHelper
    {

        private static Action EmptyDelegate = delegate () { };

        public static T GetChildOfType<T>(this DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }

        public static T GetChildOfTypeRecursive<T>(this DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;

                var c = GetChildOfTypeRecursive<T>(child);
                if(c != null)
                {
                    return c;
                }
            }
            return null;
        }

        public static void ForceSyncUpdates()
        {
            Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background, new Action(delegate { }));
        }

        public static void SetExpanderVerticalLabel(Expander parent, string text, double fontSize, Color textColor, Color backgroundColor)
        {
            var button = parent.GetChildOfType<ToggleButton>();
            if (button != null)
            {
                TextBlock tb = new TextBlock();
                tb.VerticalAlignment = VerticalAlignment.Center;
                tb.HorizontalAlignment = HorizontalAlignment.Stretch;
                tb.Margin = new Thickness(0, 0, 0, 0);
                tb.FontSize = fontSize;
                tb.Text = text;
                tb.Foreground = new SolidColorBrush(textColor);
                button.Background = new SolidColorBrush(backgroundColor);
                tb.RenderTransform = new RotateTransform(-90, 0, 0);

                button.Content = tb;
            }
        }

        public static void ForceResize(FrameworkElement e)
        {
            e.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            e.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

    }
}
