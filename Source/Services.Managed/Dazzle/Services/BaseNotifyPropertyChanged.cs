﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Services.Common
{
    public class BaseNotifyPropertyChanged : INotifyPropertyChanged
    {
        #region Member variables

        public event PropertyChangedEventHandler PropertyChanged = null;

        #endregion

        #region Event handlers

        protected void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}


