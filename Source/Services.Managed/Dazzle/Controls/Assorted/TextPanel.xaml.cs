﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Assorted
{
    /// <summary>
    /// Interaction logic for TextPanel.xaml
    /// </summary>
    public partial class TextPanel : UserControl, INotifyPropertyChanged
    {
        protected string _displayText = string.Empty;
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<System.Windows.Input.MouseButtonEventArgs> OnClicked = null;

        public TextPanel()
        {
            InitializeComponent();
        }

        public string DisplayText
        {
            get
            {
                return _displayText;
            }
            set
            {
                _displayText = value;
                OnPropertyChanged("DisplayText");
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void DisplayPanelBorder_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            DisplayPanelBorder.Background = Brushes.Transparent;
        }

        private void DisplayPanelBorder_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var brush = Application.Current.Resources["BackgroundBrush_MessageLabel_Selected"];
            DisplayPanelBorder.Background = brush as SolidColorBrush;
        }

        private void DisplayPanelBorder_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            OnClicked?.Invoke(this, e);
        }
    }
}

