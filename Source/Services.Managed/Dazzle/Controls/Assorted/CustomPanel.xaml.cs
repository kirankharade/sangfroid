﻿using System.Windows;
using System.Windows.Controls;

namespace Dazzle.Controls.Assorted
{
    /// <summary>
    /// Interaction logic for CustomBorderControl.xaml
    /// </summary>
    public partial class CustomPanel : UserControl
    {
        public CustomPanel()
        {
            InitializeComponent();
        }

        public ContentControl Container
        {
            get
            {
                return this.ContainmentArea;
            }
        }
    }
}
