﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Assorted
{
    /// <summary>
    /// Interaction logic for MultiColorTextPanel.xaml
    /// </summary>
    public partial class MultiColorTextPanel : UserControl, INotifyPropertyChanged
    {
        protected string _displayText = string.Empty;
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler OnClicked = null;

        public MultiColorTextPanel()
        {
            InitializeComponent();
        }

        public void SetText(string text, Brush brush)
        {
            TextContainer.Children.Clear();
            TextContainer.Children.Add(new TextBlock() { Text = text, Foreground = brush });
        }

        public void AddText(string text, Brush brush)
        {
            TextContainer.Children.Add(new TextBlock() { Text = text, Foreground = brush });
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void DisplayPanelBorder_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            DisplayPanelBorder.Background = Brushes.Transparent;
        }

        private void DisplayPanelBorder_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var brush = Application.Current.Resources["BackgroundBrush_MessageLabel_Selected"];
            DisplayPanelBorder.Background = brush as SolidColorBrush;
        }

        private void DisplayPanelBorder_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            OnClicked?.Invoke(this, e);
        }
    }
}

