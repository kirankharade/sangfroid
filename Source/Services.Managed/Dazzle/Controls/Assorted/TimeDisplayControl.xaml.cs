﻿using System;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Dazzle.Controls.Assorted
{
    /// <summary>
    /// Interaction logic for TimeDisplayControl.xaml
    /// </summary>
    public partial class TimeDisplayControl : UserControl
    {

        Timer _timer = null;
        protected Dispatcher _uiThread = null;
        public event EventHandler OnClicked = null;

        public delegate void ForwardingDelegate(object ctrl);

        public TimeDisplayControl()
        {
            InitializeComponent();
        }

        private void TimeDisplay_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            StartDisplay(2000);
        }

        private void StartDisplay(int afterTheseManyMiliseconds)
        {
            _uiThread = Dispatcher.CurrentDispatcher;
            _timer = new Timer(this.OnDisplayDurationFinished, this, afterTheseManyMiliseconds, 1000);
            TimerDisplay.OnClicked += TimerDisplay_OnClicked;
        }

        private void TimerDisplay_OnClicked(object sender, EventArgs e)
        {
            OnClicked?.Invoke(this, null);
        }

        private void OnDisplayDurationFinished(object state)
        {
            _uiThread.BeginInvoke(DispatcherPriority.Normal, new ForwardingDelegate(UpdateTimedisplayText), state as TimeDisplayControl);
        }

        private void UpdateTimedisplayText(object ctrl)
        {
            (ctrl as TimeDisplayControl).TimerDisplay.DisplayText = DateTime.Now.ToString("dddd, dd:MM:yyyy, h:mm:ss tt");
        }

    }
}
