﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Dazzle.Controls.TextBoxes
{
    /// <summary>
    /// Interaction logic for DoubleInputTextBox.xaml
    /// </summary>
    public partial class DoubleInputTextBox : UserControl
    {
        #region Fields

        SolidColorBrush _errorBrush = new SolidColorBrush(Color.FromRgb(240, 100, 100));
        SolidColorBrush _normalBrush = new SolidColorBrush(Colors.YellowGreen);

        #endregion

        public DoubleInputTextBox()
        {
            InitializeComponent();
            DataObject.AddPastingHandler(ThisTextBox, PasteEventHandler);
        }

        #region Properties

        #region Dependency properties

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
              "Text", typeof(string), typeof(DoubleInputTextBox), new PropertyMetadata(""));

        public string Text
        {
            get { return GetValue(TextProperty) as string; }
            set { SetValue(TextProperty, value); }
        }

        #endregion

        #endregion


        private void PasteEventHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (!e.DataObject.GetDataPresent(typeof(string))) return;

            var txt = e.DataObject.GetData(typeof(string)) as string;
            double result = 0;
            var isDouble = Double.TryParse(txt, out result);
            if (!isDouble)
            {
                e.CancelCommand();
            }
        }

        private void ThisTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char decimalSeparator = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            if (!IsDigitOrDecimal(e.Text))
            {
                e.Handled = true;
            }
        }

        protected bool IsDigitOrDecimal(string str)
        {
            char decimalSeparator = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            foreach (char c in str)
            {
                if (char.IsDigit(c))
                {
                    return true;
                }
                if (c == decimalSeparator)
                {
                    return true;
                }
            }

            return false;
        }

        //private bool IsMatch(string input)
        //{
        //    return Regex.IsMatch(input, "^[a-zA-Z0-9 ]");
        //}

        private void ThisTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            double result = Double.MaxValue;
            var isDouble = Double.TryParse(ThisTextBox.Text, out result);
            if (!isDouble)
            {
                ThisTextBox.BorderBrush = _errorBrush;
            }
            else
            {
                ThisTextBox.BorderBrush = _normalBrush;
            }
        }
    }
}
