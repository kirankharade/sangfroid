﻿using System;
using System.Windows.Controls;

namespace Dazzle.Controls.TextBoxes
{
    public class CustomTextBox: TextBox
    {

        public CustomTextBox()
            :base()
        {
            Margin = new System.Windows.Thickness(5);
            TextWrapping = System.Windows.TextWrapping.Wrap;
            VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            FontFamily = new System.Windows.Media.FontFamily("Segoe UI");
            FontSize = 14;
            AcceptsReturn = true;
            Cursor = System.Windows.Input.Cursors.IBeam;
            Visibility = System.Windows.Visibility.Visible;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            //CaretIndex = Text.Length;
            ScrollToEnd();
        }

    }
}
