using System;
using System.Windows;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

//Based on and Courtsey: https://www.codeproject.com/Articles/22876/WPF-Taskbar-Notifier-A-WPF-Taskbar-Notification-Wi
//License: The Code Project Open License (CPOL) 1.02 (https://www.codeproject.com/info/cpol10.aspx)

namespace Dazzle.Controls.Notifiers
{
    public enum DisplayStates
    {
        Appearing,
        Appeared,
        Vanishing,
        Vanished
    }

    public class NotifierControlBase : Window, INotifyPropertyChanged
    {
        #region Member variables

        protected int _slideInDuration = 2500;
        protected int _slideOutDuration = 2500;
        protected int _displayDuration = 4000;

        protected DispatcherTimer _displayTimer = null;
        protected Storyboard _animationStoryboard = null; 
        protected DoubleAnimation _windowTopAnimation = null;
        protected int _offsetFromScreenRight = 0;
        protected DisplayStates _displayState;
        protected long _id = _popupCount++;

        protected double _topYWhenHidden;
        protected double _topYWhenShown;

        protected static long _popupCount = 0;
        protected static List<long> _popups = new List<long>();
        
        #endregion

        #region Events and handlers

        private EventHandler CompletelyVanished;
        private EventHandler CompletelyAppeared;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Init();

            _popups.Add(_id);

            Top = _topYWhenHidden;
            _displayState = DisplayStates.Vanished;

            // Display duration timer
            _displayTimer = new DispatcherTimer();
            _displayTimer.Interval = TimeSpan.FromMilliseconds(_displayDuration);
            _displayTimer.Tick += new EventHandler(DisplayTimer_Elapsed);

            _windowTopAnimation = new DoubleAnimation();
            Storyboard.SetTargetProperty(_windowTopAnimation, new PropertyPath(Window.TopProperty));
            _animationStoryboard = new Storyboard();
            _animationStoryboard.Children.Add(_windowTopAnimation);
            _animationStoryboard.FillBehavior = FillBehavior.Stop;

            CompletelyVanished = new EventHandler(OnCompletelyVanished);
            CompletelyAppeared = new EventHandler(OnCompletelyAppeared);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            OnTopYChnaged(e);
        }

        private void OnTopYChnaged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property.Name == "Top")
            {
                if (((double)e.NewValue != (double)e.OldValue) && ((double)e.OldValue != _topYWhenHidden))
                {
                    BringForward();
                }
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            ShowInTaskbar = false;
            base.OnInitialized(e);
        }

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public NotifierControlBase()
        {
            Loaded += new RoutedEventHandler(OnLoaded);
        }

        #region Properties

        public int SlideInDuration
        {
            get { return _slideInDuration; }
            set
            {
                _slideInDuration = value;
                RaisePropertyChanged("SlideInDuration");
            }
        }

        public int SlideOutDuration
        {
            get { return _slideOutDuration; }
            set
            {
                _slideOutDuration = value;
                RaisePropertyChanged("SlideOutDuration");
            }
        }

        public int DisplayDuration
        {
            get { return _displayDuration; }
            set
            {
                _displayDuration = value;
                if (_displayTimer != null)
                    _displayTimer.Interval = TimeSpan.FromMilliseconds(_displayDuration);
                RaisePropertyChanged("DisplayDuration");
            }
        }

        public int OffsetFromScreenRight
        {
            get { return _offsetFromScreenRight; }
            set
            {
                _offsetFromScreenRight = value;
                RaisePropertyChanged("OffsetFromScreenRight");
            }
        }

        protected DisplayStates DisplayState
        {
            get
            {
                return _displayState;
            }
            set
            {
                if (value != _displayState)
                {
                    _displayState = value;
                    OnDisplayStateChanged();
                }
            }
        }

        #endregion

        #region Public Methods

        public void Notify()
        {
            if (DisplayState == DisplayStates.Appeared)
            {
                _displayTimer.Stop();
                _displayTimer.Start();
            }
            else
            {
                if(!IsLoaded)
                {
                    this.Show();
                }
                DisplayState = DisplayStates.Appearing;
            }
        }

        public void Vanish()
        {
            DisplayState = DisplayStates.Vanished;
        }

        #endregion

        #region Private and Protected methods

        private void Init()
        {
            var workingArea = GetWorkingArea();
            var rightMostX = workingArea.Right;
            Left = rightMostX - (ActualWidth + _offsetFromScreenRight);
            _topYWhenHidden = workingArea.Bottom;

            var offset = workingArea.Height / 6.0;

            _topYWhenShown = workingArea.Bottom - offset;
            _topYWhenShown -= (80 * _popups.Count);
        }

        private Rectangle GetWorkingArea()
        {
            var rect = new Rectangle((int)Left, (int)Top, (int)ActualWidth, (int)ActualHeight);
            var workingArea = Screen.GetWorkingArea(rect);
            workingArea.Width = (int) System.Windows.SystemParameters.PrimaryScreenWidth;
            workingArea.Height = (int) System.Windows.SystemParameters.PrimaryScreenHeight;

            return workingArea;
        }

        private void BringForward()
        {
            Topmost = true;
            Topmost = false;
        }

        private void OnDisplayStateChanged()
        {
            if (_animationStoryboard == null)
                return;

            _animationStoryboard.Stop(this);
            DisconnectHandlers();

            if (_displayState != DisplayStates.Vanished)
            {
                BringForward();
            }

            if (_displayState == DisplayStates.Appeared)
            {
                Top = _topYWhenShown;
                if (!IsMouseOver)
                {
                    _displayTimer.Stop();
                    _displayTimer.Start();
                }
            }
            else if (_displayState == DisplayStates.Appearing)
            {
                Visibility = Visibility.Visible;
                BringForward();
                int duration = RemainingDuration(_slideInDuration, _topYWhenShown);
                if (duration < 1)
                {
                    DisplayState = DisplayStates.Appeared;
                    return;
                }
                Appear(duration);
            }
            else if (_displayState == DisplayStates.Vanishing)
            {
                int duration = RemainingDuration(_slideOutDuration, _topYWhenHidden);
                if (duration < 1)
                {
                    DisplayState = DisplayStates.Vanished;
                    return;
                }
                Vanish(duration);
            }
            else if (_displayState == DisplayStates.Vanished)
            {
                Top = _topYWhenHidden;
                Visibility = Visibility.Hidden;
                _popups.Remove(_id);
            }
        }

        private void Vanish(int duration)
        {
            _windowTopAnimation.To = _topYWhenHidden;
            _windowTopAnimation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, duration));

            _animationStoryboard.Completed += CompletelyVanished;
            _animationStoryboard.Begin(this, true);
        }

        private void Appear(int duration)
        {
            _windowTopAnimation.To = _topYWhenShown;
            _windowTopAnimation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, duration));

            _animationStoryboard.Completed += CompletelyAppeared;
            _animationStoryboard.Begin(this, true);
        }

        private void DisconnectHandlers()
        {
            _animationStoryboard.Completed -= CompletelyVanished;
            _animationStoryboard.Completed -= CompletelyAppeared;
        }

        private int RemainingDuration(int totalDuration, double topDestinationPosition)
        {
            if (Top == topDestinationPosition)
            {
                return 0;
            }
            double distanceRemaining = Math.Abs(Top - topDestinationPosition);
            double percentDone = distanceRemaining / ActualHeight;
            return (int)(totalDuration * percentDone);
        }

        protected virtual void OnCompletelyVanished(object sender, EventArgs e)
        {
            DisplayState = DisplayStates.Vanished;
        }

        protected virtual void OnCompletelyAppeared(object sender, EventArgs e)
        {
            DisplayState = DisplayStates.Appeared;
        }

        private void DisplayTimer_Elapsed(Object sender, EventArgs args)
        {
            _displayTimer.Stop();
            if (!IsMouseOver)
            {
                DisplayState = DisplayStates.Vanishing;
            }
        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            if (DisplayState == DisplayStates.Appeared)
            {
                _displayTimer.Stop();
            }
            else if ((DisplayState == DisplayStates.Vanished) ||
                     (DisplayState == DisplayStates.Vanishing))
            {
                DisplayState = DisplayStates.Appearing;
            }
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            if (DisplayState == DisplayStates.Appeared)
            {
                _displayTimer.Stop();
                _displayTimer.Start();
            }
            base.OnMouseEnter(e);
        }

        #endregion

    }
}



