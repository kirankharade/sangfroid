﻿using Dazzle.Appearance;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Dazzle.Controls.Notifiers
{
    /// <summary>
    /// Interaction logic for ContextErrorNotifier.xaml
    /// </summary>
    public partial class ContextErrorNotifier : Window, INotifyPropertyChanged
    {
        #region Member variables

        private string _messageText = string.Empty;
        private Action _action = null;
        private Brush _backgroundBrush = (Brush)Application.Current.Resources["Level_9_BackgroundBrush"];
        private Brush _borderBrush = new SolidColorBrush(Color.FromRgb(50, 60, 70));
        private CornerRadius _cornerRadius = new CornerRadius(1);
        private double _opacity = 0.99;
        private Brush _foregroundBrush = new SolidColorBrush(Colors.Red);
        private double _fontSize = (double)Application.Current.Resources["DefaultFontSize"];
        Timer _timer = null;
        protected Dispatcher _uiThread = null;
        protected int _displayDuration = 4000;

        public delegate void ForwardingDelegate(ContextErrorNotifier dialog);

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public ContextErrorNotifier()
        {
            InitializeComponent();
            if (_backgroundBrush == null)
            {
                _backgroundBrush = new SolidColorBrush(Color.FromRgb(23, 34, 41));
            }
            if (_fontSize < 0 || _fontSize > 25)
            {
                _fontSize = 12;
            }

            Closed += OnClosed;
            Loaded += OnLoaded;
            MouseDown += Notifier_MouseDown;
            Topmost = true;
            Focusable = false;
        }

        #region Properties

        public string MessageText
        {
            get
            {
                return _messageText;
            }
            set
            {
                _messageText = value;
                RaisePropertyChanged("MessageText");
            }
        }

        public Brush BackgroundBrush
        {
            get
            {
                return _backgroundBrush;
            }
            set
            {
                _backgroundBrush = value;
                RaisePropertyChanged("BackgroundBrush");
            }
        }

        public Brush OutlineBrush
        {
            get
            {
                return _borderBrush;
            }
            set
            {
                _borderBrush = value;
                RaisePropertyChanged("OutlineBrush");
            }
        }

        public Brush ForegroundBrush
        {
            get
            {
                return _foregroundBrush;
            }
            set
            {
                _foregroundBrush = value;
                RaisePropertyChanged("ForegroundBrush");
            }
        }

        public CornerRadius CornerRadius
        {
            get
            {
                return _cornerRadius;
            }
            set
            {
                _cornerRadius = value;
                RaisePropertyChanged("CornerRadius");
            }
        }

        public double NotifierOpacity
        {
            get
            {
                return _opacity;
            }
            set
            {
                _opacity = value;
                RaisePropertyChanged("NotifierOpacity");
            }
        }

        public double MessageFontSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                _fontSize = value;
                RaisePropertyChanged("MessageFontSize");
            }
        }

        #endregion

        #region Public methods

        static public void ShowMessage(string text, int displayDuration, Point anchorPoint, Action actionOnClicked)
        {
            ContextErrorNotifier pn = new ContextErrorNotifier();
            pn.Top = anchorPoint.Y;
            pn.Left = anchorPoint.X;
            AppearanceManager.SetAppearance(pn);
            pn.Popup(text, displayDuration, actionOnClicked);
        }

        protected void Popup(string text, int displayDuration, Action action)
        {
            MessageText = text;
            _action = action;
            _displayDuration = displayDuration;
            Focusable = false;
            Show();
        }

        #endregion

        #region Event handlers

        private void Notifier_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Close();
            _action?.Invoke();
        }

        private void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnDisplayDurationFinished(object state)
        {
            _uiThread.BeginInvoke(DispatcherPriority.Normal, new ForwardingDelegate(UpdateOpacity), state as ContextErrorNotifier);
        }

        private void OnKeyPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                this.Close();
            }
        }

        private void OnClosed(object sender, EventArgs args)
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            _uiThread = Dispatcher.CurrentDispatcher;
            _timer = new Timer(this.OnDisplayDurationFinished, this, _displayDuration, 40);
        }

        #endregion

        static private void UpdateOpacity(ContextErrorNotifier d)
        {
            var opacity = d.Opacity;
            opacity -= 0.1;
            if (opacity < 0)
            {
                d.Close();
            }
            else
            {
                d.Opacity = opacity;
            }
        }

    }
}
