using Dazzle.Appearance;
using System;
using System.Windows;
using System.Windows.Media;

namespace Dazzle.Controls.Notifiers
{
    public class PopupMessageContent
    {
        public string MessageText { get; set; }
        public Action ActionOnClicked { get; set; }
    }

    /* Usage: 
     *  Something like this with static method call (default appearance)
     *      PopupNotifier.ShowMessage(e.DisplayText, null);
     *  or
     *  by constructing an object of the notifier and customizing it...
     *  PopupNotifier pn = new PopupNotifier()
     *  {
     *      NotifierOpacity = 0.9,
     *      BackgroundBrush = new SolidColorBrush(Color.FromRgb(222, 111, 55)),
     *      MessageFontSize = 18
     *  };
     *  AppearanceManager.SetAppearance(pn);
     *  pn.Popup(e.DisplayText, null);
     *  pn.Dispose();
     *
     * */

    public partial class PopupNotifier : NotifierControlBase, IDisposable
    {
        #region Member variables

        private string _messageText = string.Empty;
        private Action _action = null;
        private Visibility _closeButtonVisibility = Visibility.Visible;
        private Brush _backgroundBrush = (Brush)Application.Current.Resources["Level_0_BackgroundBrush"];
        private Brush _borderBrush = new SolidColorBrush(Color.FromRgb(250, 235, 215));
        private CornerRadius _cornerRadius = new CornerRadius(3);
        private double _opacity = 0.7;
        private Brush _foregroundBrush = new SolidColorBrush(Color.FromRgb(154, 205, 50));
        private double _fontSize = (double) Application.Current.Resources["DefaultFontSize"];

        #endregion

        public PopupNotifier()
        {
            InitializeComponent();
            if(_backgroundBrush == null)
            {
                _backgroundBrush = new SolidColorBrush(Color.FromRgb(23, 34, 41));
            }
            if(_fontSize < 0 || _fontSize > 25)
            {
                _fontSize = 12;
            }
            CloseNotificationButton.Clicked += OnClosed;
        }

        #region Properties

        public string MessageText
        {
            get
            {
                return _messageText;
            }
            set
            {
                _messageText = value;
                RaisePropertyChanged("MessageText");
            }
        }

        public bool ShowCloseButton
        {
            get
            {
                return CloseButtonVisibility == Visibility.Visible;
            }
            set
            {
                if(value)
                {
                    CloseButtonVisibility = Visibility.Visible;
                }
                else
                {
                    CloseButtonVisibility = Visibility.Collapsed;
                }
                RaisePropertyChanged("ShowCloseButton");
            }
        }

        protected Visibility CloseButtonVisibility
        {
            get
            {
                return _closeButtonVisibility;
            }
            set
            {
                _closeButtonVisibility = value;
                RaisePropertyChanged("CloseButtonVisibility");
            }
        }

        public Brush BackgroundBrush
        {
            get
            {
                return _backgroundBrush;
            }
            set
            {
                _backgroundBrush = value;
                RaisePropertyChanged("BackgroundBrush");
            }
        }

        public Brush OutlineBrush
        {
            get
            {
                return _borderBrush;
            }
            set
            {
                _borderBrush = value;
                RaisePropertyChanged("OutlineBrush");
            }
        }

        public Brush ForegroundBrush
        {
            get
            {
                return _foregroundBrush;
            }
            set
            {
                _foregroundBrush = value;
                RaisePropertyChanged("ForegroundBrush");
            }
        }

        public CornerRadius CornerRadius
        {
            get
            {
                return _cornerRadius;
            }
            set
            {
                _cornerRadius = value;
                RaisePropertyChanged("CornerRadius");
            }
        }

        public double NotifierOpacity
        {
            get
            {
                return _opacity;
            }
            set
            {
                _opacity = value;
                RaisePropertyChanged("NotifierOpacity");
            }
        }

        public double MessageFontSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                _fontSize = value;
                RaisePropertyChanged("MessageFontSize");
            }
        }

        #endregion

        static public void ShowMessage(string text, Action actionOnClicked)
        {
            PopupNotifier pn = new PopupNotifier();
            AppearanceManager.SetAppearance(pn);
            pn.Popup(text, actionOnClicked);
            pn.Dispose();
        }

        public void Popup(string text, Action action)
        {
            DisplayState = DisplayStates.Vanished;
            MessageText = text;
            _action = action;
            Notify();
        }

        private void OnClosed(object sender, EventArgs e)
        {
            Vanish();
        }

        public void Dispose()
        {
        }

        private void Notifier_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Vanish();
            _action?.Invoke();
        }
    }
}