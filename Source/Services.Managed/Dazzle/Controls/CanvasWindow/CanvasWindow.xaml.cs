﻿using System.Windows;
using System.Windows.Controls;

namespace Dazzle.Controls.CanvasWindow
{
    /// <summary>
    /// Interaction logic for CanvasWindow.xaml
    /// </summary>
    public partial class CanvasWindow : Window
    {
        public CanvasWindow()
        {
            InitializeComponent();
        }

        public ContentControl Surface
        {
            get
            {
                return Canvas;
            }
            set
            {
                Canvas = value;
            }
        }
    }
}
