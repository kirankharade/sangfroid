﻿using Dazzle.Controls.ExpansionPanels;
using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace Dazzle.Controls.Tabs
{
    /// <summary>
    /// Interaction logic for TabberControl.xaml
    /// </summary>
    public partial class TabberControl : UserControl
    {
        private string _headerIconPath = String.Empty;
        private TabberViewModel _viewModel = null;
        public EventHandler<SelectableItemSelectionChangedEventArgs> MenuSelectionChanged = null;

        public DispatcherPriority EmptyDelegate { get; private set; }

        public TabberControl()
        {
            InitializeComponent();
            _viewModel = new TabberViewModel();
            DataContext = _viewModel;


        }
        #region Properties

        public ContentControl Pane { get { return this.InteractionArea; } }

        #endregion
        private void OnTabClicked(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void TabberControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_viewModel.SelectedIndex == -1 && _viewModel.Items.Count > 0)
            {
                _viewModel.SelectedIndex = 0;
            }
        }

        public bool AddTab(BinarySelectableItem tab)
        {
            if (tab == null ||
                tab.StoredContent == null ||
                _viewModel.GetTab(tab.Header) != null ||
                string.IsNullOrWhiteSpace(tab.Header))
            {
                return false;
            }
            return _viewModel.AddTab(tab);
        }

        public bool RemoveTab(string header)
        {
            if (string.IsNullOrWhiteSpace(header))
            {
                return false;
            }
            return _viewModel.RemoveTab(_viewModel.GetTab(header));
        }

        public bool SelectMenuItem(string header)
        {
            return _viewModel.SetTabSelected(header);
        }

        public SelectableItem SelectedTab()
        {
            var selectedIndex = _viewModel.SelectedIndex;
            if (selectedIndex < 0 || selectedIndex >= _viewModel.TabCount)
            {
                return null;
            }
            return _viewModel[selectedIndex];
        }

        public SelectableItem GetTab(string header)
        {
            return _viewModel.GetTab(header);
        }

        public void ForceResize()
        {
            //this.TabListBox.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            //this.TabListBox.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }


        #region Event handlers

        private void OnTabSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var previousTab = e.RemovedItems.Count > 0 ? e.RemovedItems[0] : null;
            if (previousTab != null)
            {
                (previousTab as SelectableItem).Selected = false;
            }
            var newTab = e.AddedItems.Count > 0 ? e.AddedItems[0] : null;
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = previousTab as SelectableItem, Current = newTab as SelectableItem };

            var ct = SelectedTab();
            if (ct != null)
            {
                ct.Selected = true;
            }
            MenuSelectionChanged?.Invoke(this, args);
        }

        #endregion


    }
}
