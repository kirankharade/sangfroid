﻿using Services.Common;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls
{
    public class SelectableItem: BaseNotifyPropertyChanged, ISerializable
    {
        protected string _header = "";
        protected ContentControl _contentControl = null;
        protected ImageSource _enabledIcon = null;
        protected ImageSource _disabledIcon = null;
        protected ImageSource _icon = null;
        protected bool _selected = false;
        protected Visibility _iconVisibility = Visibility.Visible;
        protected Visibility _headerVisibility = Visibility.Visible;
        protected double _iconHeight = 40;
        private double _itemMinWidth = 30;
        protected double _iconWidth = 40;
        protected double _itemHeight = 50;
        protected ToolTip _toolTip = new ToolTip();
        protected Brush _foregroundBrush = new SolidColorBrush(Colors.Black);

        public SelectableItem(string header, ContentControl content = null, string enabledIconPath = "", string disabledIconPath = "")
        {
            _header = header;
            _contentControl = content;

            if (!string.IsNullOrEmpty(enabledIconPath))
            {
                _enabledIcon = (ImageSource)new ImageSourceConverter().ConvertFromString(enabledIconPath);

                if (string.IsNullOrEmpty(disabledIconPath))
                    _disabledIcon = _enabledIcon;
            }

            if (!string.IsNullOrEmpty(disabledIconPath))
            {
                _disabledIcon = (ImageSource)new ImageSourceConverter().ConvertFromString(disabledIconPath);

                if (string.IsNullOrEmpty(enabledIconPath))
                    _enabledIcon = _disabledIcon;
            }

            _icon = _disabledIcon;

            ForegroundBrush = new SolidColorBrush(Colors.LightGray);
            SelectedForegroundBrush = new SolidColorBrush(Colors.Black);
            UnselectedForegroundBrush = new SolidColorBrush(Colors.LightGray);
        }

        #region Properties

        public string Header => _header;

        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                Icon = _selected ? _enabledIcon : _disabledIcon;
                ForegroundBrush = _selected ? SelectedForegroundBrush : UnselectedForegroundBrush;
            }
        }

        public ContentControl StoredContent => _contentControl;


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Header", _header, typeof(string));
            info.AddValue("Content", _contentControl, typeof(ContentControl));
        }

        public SelectableItem(SerializationInfo info, StreamingContext context)
        {
            _header = (string)info.GetValue("Header", typeof(string));
            _contentControl = (ContentControl)info.GetValue("Header", typeof(ContentControl));
        }

        public double ItemHeight
        {
            get { return _itemHeight; }
            set
            {
                if (_itemHeight != value)
                {
                    _itemHeight = value;
                    RaisePropertyChanged("ItemHeight");
                }
            }
        }

        public double ItemMinWidth
        {
            get { return _itemMinWidth; }
            set
            {
                if (_itemMinWidth != value)
                {
                    _itemMinWidth = value;
                    RaisePropertyChanged("ItemMinWidth");
                }
            }
        }

        public ImageSource Icon
        {
            get
            {
                return _icon;
            }
            private set
            {
                _icon = value;
                RaisePropertyChanged("Icon");
            }
        }

        public Visibility HeaderVisibility
        {
            get
            {
                return _headerVisibility;
            }
            set
            {
                _headerVisibility = value;
                RaisePropertyChanged("HeaderVisibility");
            }
        }
        public Visibility IconVisibility
        {
            get { return _iconVisibility; }
            set
            {
                if (_iconVisibility != value)
                {
                    _iconVisibility = value;
                    RaisePropertyChanged(nameof(IconVisibility));
                }
            }
        }

        public double IconHeight
        {
            get { return _iconHeight; }
            set
            {
                if (_iconHeight != value)
                {
                    _iconHeight = value;
                    RaisePropertyChanged(nameof(IconHeight));
                }
            }
        }

        public double IconWidth
        {
            get { return _iconWidth; }
            set
            {
                if (_iconWidth != value)
                {
                    _iconWidth = value;
                    RaisePropertyChanged(nameof(IconWidth));
                }
            }
        }

        public ToolTip TabToolTip
        {
            get
            {
                return _toolTip;
            }
            set
            {
                _toolTip = value;
                RaisePropertyChanged("TabToolTip");
            }
        }

        public Brush ForegroundBrush
        {

            get { return _foregroundBrush; }
            set
            {
                _foregroundBrush = value;
                RaisePropertyChanged("ForegroundBrush");
            }
        }

        public Brush SelectedForegroundBrush { get; set; }
        public Brush UnselectedForegroundBrush { get; set; }

        #endregion

    }
}

