﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls
{
    public class BinarySelectableItem : SelectableItem
    {
        protected bool _active = false;
        protected bool _treatAsRegularTab = false;
        protected bool _canUserChangeState = true;
        protected ImageSource _onIcon = null;
        protected ImageSource _offIcon = null;
        protected ImageSource _binaryIcon = null;

        public bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
                BinaryIcon = _active ? _onIcon : _offIcon;
            }
        }

        public bool CanUserChangeState
        {
            get { return _canUserChangeState; }
            set
            {
                _canUserChangeState = value;
                RaisePropertyChanged();
            }
        }

        public ImageSource BinaryIcon
        {
            get
            {
                return _binaryIcon;
            }

            set
            {
                _binaryIcon = value;
                RaisePropertyChanged("BinaryIcon");
            }
        }

        public Visibility ActiveIconVisibility
        {
            get
            {
                if(_treatAsRegularTab)
                {
                    return Visibility.Hidden;
                }
                else
                {
                    return Visibility.Visible;
                }
            }

        }

        public BinarySelectableItem(
            string header, 
            ContentControl content,
            string enabledIconPath,
            string disabledIconPath,
            bool treatAsRegularTab
            )
            : base(header, content, enabledIconPath, disabledIconPath)
        {

            var onIconPath = "./Views/Images/MeasurementSetup/Active_checked.png";
            var offIconPath = "./Views/Images/MeasurementSetup/Active_unchecked.png";

            _onIcon = (ImageSource)new ImageSourceConverter().ConvertFromString(onIconPath);
            _offIcon = (ImageSource)new ImageSourceConverter().ConvertFromString(offIconPath);
            _binaryIcon = _offIcon;
            _treatAsRegularTab = treatAsRegularTab;
        }


    }
}
