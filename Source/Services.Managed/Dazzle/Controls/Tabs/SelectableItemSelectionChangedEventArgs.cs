﻿namespace Dazzle.Controls.Tabs
{
    public class SelectableItemSelectionChangedEventArgs
    {
        public SelectableItem Current { get; set; }
        public SelectableItem Previous { get; set; }
    }
}
