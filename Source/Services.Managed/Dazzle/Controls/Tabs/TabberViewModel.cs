﻿using Services.Common;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Controls;

namespace Dazzle.Controls.Tabs
{
    public class TabberViewModel : BaseNotifyPropertyChanged
    {
        #region Member variables

        private ObservableCollection<BinarySelectableItem> _items = new ObservableCollection<BinarySelectableItem>();
        private int _selectedIndex = -1;

        #endregion

        #region Properties

        public ObservableCollection<BinarySelectableItem> Items => _items;

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                RaisePropertyChanged("SelectedIndex");
            }
        }

        public int TabCount
        {
            get
            {
                return _items.Count;
            }
        }

        public BinarySelectableItem this[int index]
        {
            get
            {
                if (_items.Count > index)
                {
                    return _items[index];
                }
                return null;
            }
            set
            {
                if (_items.Count > index)
                {
                    _items[index] = value;
                }
            }
        }

        #endregion

        #region Methods

        public bool SetTabSelected(string header)
        {
            var tabIndex = -1;
            for(int i = 0; i < _items.Count; i++)
            {
                if(_items[i].Header == header)
                {
                    tabIndex = i;
                    break;
                }
            }
            if(tabIndex > -1)
            {
                SelectedIndex = tabIndex;
                _items[SelectedIndex].Selected = true;
                return true;
            }
            return false;
        }

        public BinarySelectableItem GetTab(string header)
        {
            foreach (var item in _items)
            {
                if (item.Header == header)
                {
                    return item;
                }
            }
            return null;
        }

        internal bool AddTab(BinarySelectableItem tab)
        {
            if(null == GetTab(tab.Header))
            {
                _items.Add(tab);
                return true;
            }
            return false;
        }

        internal bool RemoveTab(BinarySelectableItem tab)
        {
            if (null != GetTab(tab.Header))
            {
                _items.Remove(tab);
                return true;
            }
            return false;
        }

        #endregion
    }
}


