﻿using Services.Common;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Dazzle.Controls.ExpansionPanels
{
    public class ExpansionPanelMenuViewModel : BaseExpansionPanelViewModel
    {
        #region Member variables

        private ObservableCollection<SelectableItem> _items = new ObservableCollection<SelectableItem>();
        private int _selectedTabIndex = -1;

        #endregion

        public ExpansionPanelMenuViewModel()
        {
        }

        #region Properties

        public ObservableCollection<SelectableItem> Items => _items;

        public int SelectedIndex
        {
            get { return _selectedTabIndex; }
            set
            {
                _selectedTabIndex = value;
                RaisePropertyChanged("SelectedIndex");
            }
        }

        public int ItemCount
        {
            get
            {
                return _items.Count;
            }
        }

        public SelectableItem this[int index]
        {
            get
            {
                if (_items.Count > index)
                {
                    return _items[index];
                }
                return null;
            }
            set
            {
                if (_items.Count > index)
                {
                    _items[index] = value;
                }
            }
        }

        #endregion

        #region Methods

        public bool SetItemSelected(string header)
        {
            var tabIndex = -1;
            for(int i = 0; i < _items.Count; i++)
            {
                if(_items[i].Header == header)
                {
                    tabIndex = i;
                    break;
                }
            }
            if(tabIndex > -1)
            {
                SelectedIndex = tabIndex;
                _items[SelectedIndex].Selected = true;
                return true;
            }
            return false;
        }

        public SelectableItem GetItem(string header)
        {
            foreach (var item in _items)
            {
                if (item.Header == header)
                {
                    return item;
                }
            }
            return null;
        }

        internal bool Add(SelectableItem tab)
        {
            if(null == GetItem(tab.Header))
            {
                _items.Add(tab);
                return true;
            }
            return false;
        }

        internal bool Remove(SelectableItem tab)
        {
            if (null == GetItem(tab.Header))
            {
                _items.Remove(tab);
                return true;
            }
            return false;
        }

        #endregion
    }
}

