﻿using Services.Common;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.ExpansionPanels
{
    public class BaseExpansionPanelViewModel : BaseNotifyPropertyChanged
    {
        private string _headerText = "";
        private ImageSource _headerIconSource = null;
        private ExpandDirection _expandDirection = ExpandDirection.Left;
        private Brush _backgroundBrush = new SolidColorBrush(Color.FromRgb(23, 34, 41));
        private Brush _boundaryBrush = new SolidColorBrush(Color.FromRgb(23, 34, 41));
        private double _collapsedWidth = 60;
        private double _expandedWidth = 230;

        public BaseExpansionPanelViewModel ()
        {
        }

        public string HeaderText
        {
            get { return _headerText; }
            set
            {
                _headerText = value;
                RaisePropertyChanged("HeaderText");
            }
        }

        public ImageSource HeaderIcon
        {
            get
            {
                return _headerIconSource;
            }
            set
            {
                _headerIconSource = value;
                RaisePropertyChanged("HeaderIcon");
            }
        }

        public ExpandDirection Direction
        {
            get { return _expandDirection; }
            set
            {
                _expandDirection = value;
                RaisePropertyChanged("Direction");
            }
        }

        public Brush BackgroundBrush
        {
            get
            {
                return _backgroundBrush;
            }
            set
            {
                _backgroundBrush = value;
                RaisePropertyChanged("BackgroundBrush");
            }
        }

        public Brush BoundaryBrush
        {
            get
            {
                return _boundaryBrush;
            }
            set
            {
                _boundaryBrush = value;
                RaisePropertyChanged("BoundaryBrush");
            }
        }

        public double CollapsedWidth
        {
            get
            {
                return _collapsedWidth;
            }
            set
            {
                _collapsedWidth = value;
                RaisePropertyChanged("CollapsedWidth");
            }
        }

        public double ExpandedWidth
        {
            get
            {
                return _expandedWidth;
            }
            set
            {
                _expandedWidth = value;
                RaisePropertyChanged("ExpandedWidth");
            }
        }

    }
}