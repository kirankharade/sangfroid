﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Dazzle.Controls.ExpansionPanels
{
    public partial class BinaryMenuExpander : BaseExpansionPanelMenu
    {

        public BinaryMenuExpander():base()
        {
            InitializeComponent();

            _expander = TheExpander;
            _menuListBox = TabListBox;
            _outerBorder = OuterBorder;

            TheExpander.IsExpanded = true;

            this.CommandBindings.Add(new CommandBinding(RoutedCommands.RoutedCommands.Click, OnActiveButtonClicked));
        }

        private void OnActiveButtonClicked(object sender, ExecutedRoutedEventArgs e)
        {
            var item = (e.OriginalSource as FrameworkElement).TemplatedParent;
            var tab = (item as ContentPresenter).Content as BinarySelectableItem;
            if (tab?.CanUserChangeState ?? false)
            {
                tab.Active = !tab.Active;
            }
        }
    }
}
