﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Dazzle.Controls.ExpansionPanels
{
    public partial class ExpansionPanelFlipperMenu : BaseExpansionPanelMenu
    {

        public ExpansionPanelFlipperMenu():base()
        {
            InitializeComponent();

            _expander = TheExpander;
            _menuListBox = TabListBox;

            TheExpander.IsExpanded = true;
        }

        #region Properties

        override public string HeaderText 
        {
            get { return _viewModel.HeaderText; }
            set
            {
                _viewModel.HeaderText = value;
                UIHelper.SetExpanderVerticalLabel(TheExpander, HeaderText, 22, Colors.White, Color.FromRgb(60, 70, 80));
            }
        }

        #endregion

        override protected void OnLoaded(object sender, RoutedEventArgs e)  
        {
            base.OnLoaded(sender, e);
            UIHelper.SetExpanderVerticalLabel(TheExpander, HeaderText, 22, Colors.White, Color.FromRgb(60, 70, 80));
        }
    }
}
