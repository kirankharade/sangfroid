﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.ExpansionPanels
{
    /// <summary>
    /// Interaction logic for ExpansionPanelFlipper.xaml
    /// </summary>
    public partial class ExpansionPanelFlipper : UserControl
    {
        protected string _headerIconPath = String.Empty;
        private BaseExpansionPanelViewModel  _viewModel;
        public EventHandler<RoutedEventArgs> Expanded = null;
        public EventHandler<RoutedEventArgs> Collapsed = null;


        public ExpansionPanelFlipper()
        {
            InitializeComponent();
            _viewModel = new BaseExpansionPanelViewModel ();
            DataContext = _viewModel;
            TheExpander.IsExpanded = true;
        }

        public ContentControl Container { get { return this.PaneContent; } }

        public string HeaderText
        {
            get { return _viewModel.HeaderText; }
            set
            {
                _viewModel.HeaderText = value;
                UIHelper.SetExpanderVerticalLabel(TheExpander, HeaderText, 22, Colors.White, Color.FromRgb(60, 70, 80));
            }
        }

        public string HeaderIcon
        {
            get { return _headerIconPath; }
            set
            {
                _headerIconPath = value;
                if (!string.IsNullOrEmpty(_headerIconPath))
                {
                    var src = (ImageSource)new ImageSourceConverter().ConvertFromString(_headerIconPath);
                    _viewModel.HeaderIcon = src;
                }
            }
        }

        public Brush BackgroundBrush
        {
            get
            {
                return _viewModel.BackgroundBrush;
            }
            set
            {
                _viewModel.BackgroundBrush = value;
            }
        }

        public Brush BoundaryBrush
        {
            get
            {
                return _viewModel.BoundaryBrush;
            }
            set
            {
                _viewModel.BoundaryBrush = value;
            }
        }

        public double CollapsedWidth
        {
            get
            {
                return _viewModel.CollapsedWidth;
            }
            set
            {
                _viewModel.CollapsedWidth = value;
            }
        }

        public double ExpandedWidth
        {
            get
            {
                return _viewModel.ExpandedWidth;
            }
            set
            {
                _viewModel.ExpandedWidth = value;
            }
        }

        public ExpandDirection Direction
        {
            get { return _viewModel.Direction; }
            set { _viewModel.Direction = value; }
        }

        public bool IsExpanded
        {
            get { return TheExpander.IsExpanded; }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            UIHelper.SetExpanderVerticalLabel(TheExpander, HeaderText, 22, Colors.White, Color.FromRgb(60,70,80));
        }

        private void OnHeaderMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TheExpander.IsExpanded = false;
        }

        private void OnExpanded(object sender, RoutedEventArgs e)
        {
            Expanded?.Invoke(this, e);
        }

        private void OnCollapsed(object sender, RoutedEventArgs e)
        {
            Collapsed?.Invoke(this, e);
        }

        public void Collapse()
        {
            TheExpander.IsExpanded = false;
        }

        public void Expand()
        {
            TheExpander.IsExpanded = true;
        }

        public void ForceResize()
        {
            UIHelper.ForceResize(this.PaneContent);
        }


    }
}
