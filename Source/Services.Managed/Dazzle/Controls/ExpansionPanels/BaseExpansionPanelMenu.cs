﻿using Dazzle.Controls.Tabs;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Dazzle.Controls.ExpansionPanels
{
    public class BaseExpansionPanelMenu : UserControl
    {

        #region Fields

        protected string _headerIconPath = String.Empty;
        protected ExpansionPanelMenuViewModel _viewModel = null;
        protected Expander _expander = null;
        protected ListBox _menuListBox = null;
        protected Border _outerBorder = null;

        public EventHandler<RoutedEventArgs> Expanded = null;
        public EventHandler<RoutedEventArgs> Collapsed = null;
        public EventHandler<SelectableItemSelectionChangedEventArgs> MenuSelectionChanged = null;

        #endregion

        #region Constructors

        public BaseExpansionPanelMenu()
        {
            _viewModel = new ExpansionPanelMenuViewModel();
            DataContext = _viewModel;
        }

        #endregion

        #region Properties

        virtual public string HeaderText
        {
            get { return _viewModel.HeaderText; }
            set { _viewModel.HeaderText = value; }
        }

        public string HeaderIcon
        {
            get { return _headerIconPath; }
            set
            {
                _headerIconPath = value;
                if (!string.IsNullOrEmpty(_headerIconPath))
                {
                    var src = (ImageSource)new ImageSourceConverter().ConvertFromString(_headerIconPath);
                    _viewModel.HeaderIcon = src;
                }
            }
        }

        public ExpandDirection Direction
        {
            get { return _viewModel.Direction; }
            set { _viewModel.Direction = value; }
        }

        public int SelectedIndex
        {
            get
            {
                return _viewModel.SelectedIndex;
            }
            set
            {
                _viewModel.SelectedIndex = value;
            }
        }

        public Brush BackgroundBrush
        {
            get
            {
                return _viewModel.BackgroundBrush;
            }
            set
            {
                _viewModel.BackgroundBrush = value;
            }
        }

        public Brush BoundaryBrush
        {
            get
            {
                return _viewModel.BoundaryBrush;
            }
            set
            {
                _viewModel.BoundaryBrush = value;
            }
        }

        public double CollapsedWidth
        {
            get
            {
                return _viewModel.CollapsedWidth;
            }
            set
            {
                _viewModel.CollapsedWidth = value;
            }
        }

        public double ExpandedWidth
        {
            get
            {
                return _viewModel.ExpandedWidth;
            }
            set
            {
                _viewModel.ExpandedWidth = value;
            }
        }

        #endregion

        #region Public methods

        public bool AddMenuItem(SelectableItem tab)
        {
            if (tab == null ||
                tab.StoredContent == null ||
                _viewModel.GetItem(tab.Header) != null ||
                string.IsNullOrWhiteSpace(tab.Header))
            {
                return false;
            }
            return _viewModel.Add(tab);
        }

        public bool RemoveTab(string header)
        {
            if (string.IsNullOrWhiteSpace(header))
            {
                return false;
            }
            return _viewModel.Remove(_viewModel.GetItem(header));
        }

        public bool SelectMenuItem(string header)
        {
            return _viewModel.SetItemSelected(header);
        }

        public void Collapse()
        {
            if (_expander != null)
            {
                _expander.IsExpanded = false;
            }
        }

        public void Expand()
        {
            if (_expander != null)
            {
                _expander.IsExpanded = true;
            }
        }

        public SelectableItem SelectedTab()
        {
            var selectedIndex = _viewModel.SelectedIndex;
            if (selectedIndex < 0 || selectedIndex >= _viewModel.ItemCount)
            {
                return null;
            }
            return _viewModel[selectedIndex];
        }

        public SelectableItem GetTab(string header)
        {
            return _viewModel.GetItem(header);
        }

        public void ForceResize()
        {
            if(_menuListBox != null)
            {
                UIHelper.ForceResize(_menuListBox);
            }
        }

        #endregion

        #region Event handlers

        virtual protected void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel.SelectedIndex == -1 && _viewModel.Items.Count > 0)
            {
                _viewModel.SelectedIndex = 0;
            }
        }

        protected void OnHeaderMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_expander != null)
            {
                _expander.IsExpanded = !_expander.IsExpanded;
            }
        }

        protected void OnTabSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var previousTab = e.RemovedItems.Count > 0 ? e.RemovedItems[0] : null;
            if (previousTab != null)
            {
                (previousTab as SelectableItem).Selected = false;
            }
            var newTab = e.AddedItems.Count > 0 ? e.AddedItems[0] : null;
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = previousTab as SelectableItem, Current = newTab as SelectableItem };

            var ct = SelectedTab();
            if (ct != null)
            {
                ct.Selected = true;
            }
            MenuSelectionChanged?.Invoke(this, args);
        }

        protected void OnExpanded(object sender, RoutedEventArgs e)
        {
            if (_outerBorder != null)
            {
                AnimateWidth(_outerBorder, CollapsedWidth, ExpandedWidth);
            }
            Expanded?.Invoke(this, e);
        }

        protected void OnCollapsed(object sender, RoutedEventArgs e)
        {
            if (_outerBorder != null)
            {
                AnimateWidth(_outerBorder, ExpandedWidth, CollapsedWidth);
            }
            Collapsed?.Invoke(this, e);
        }

        #endregion

        #region Protected methods

        protected void AnimateWidth(Border outerBorder, double from, double to)
        {
            var widthAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromMilliseconds(400)
            };

            Storyboard.SetTarget(widthAnimation, outerBorder);
            Storyboard.SetTargetProperty(widthAnimation, new PropertyPath(Border.WidthProperty));

            var sb = new Storyboard();
            sb.Children.Add(widthAnimation);
            sb.Begin();
        }

        #endregion

    }
}

