﻿namespace Dazzle.Controls.ExpansionPanels
{
    public partial class ExpansionPanelMenu : BaseExpansionPanelMenu
    {

        public ExpansionPanelMenu():base()
        {
            InitializeComponent();

            _expander = TheExpander;
            _menuListBox = TabListBox;
            _outerBorder = OuterBorder;

            TheExpander.IsExpanded = true;
        }

    }
}
