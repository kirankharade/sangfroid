﻿using Dazzle.Controls.Buttons;
using System.Windows.Controls;

namespace Dazzle.Controls.VerticalToolbar
{
    /// <summary>
    /// Interaction logic for VerticalToolbar.xaml
    /// </summary>
    public partial class VerticalToolbarControl : UserControl
    {
        public VerticalToolbarControl()
        {
            InitializeComponent();
        }

        public void ClearChildren()
        {
            ToolContainer.Children.Clear();
        }

        public void AddTool(ToolButton button)
        {
            ToolContainer.Children.Add(button);
        }

        public void AddTool(ToolSwitchButton button)
        {
            ToolContainer.Children.Add(button);
        }

        public void AddToolSeparator()
        {
            var separator = new VerticalToolSeparator();
            ToolContainer.Children.Add(separator);
        }
    }
}
