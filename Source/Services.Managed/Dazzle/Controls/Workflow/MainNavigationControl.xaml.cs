﻿using Dazzle.Controls.ExpansionPanels;
using Dazzle.Controls.Tabs;
using System;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Workflow
{
    public partial class MainNavigationControl : UserControl
    {
        #region Fields

        public EventHandler<SelectableItemSelectionChangedEventArgs> MenuSelectionChanged = null;
        public EventHandler MainMenuExpanded = null;
        public EventHandler MainMenuCollapsed = null;

        #endregion

        public MainNavigationControl()
        {
            InitializeComponent();

            this.DataContext = this;

            Menu.BackgroundBrush = new SolidColorBrush(Colors.Transparent);
            Menu.BoundaryBrush = new SolidColorBrush(Colors.Transparent);
            Menu.CollapsedWidth = 50.0;
            Menu.ExpandedWidth = 200.0;
            Menu.MenuSelectionChanged += OnMenuSelectionChanged;
            Menu.Expand();
        }

        #region Properties

        public ExpansionPanelMenu Menu
        {
            get
            {
                return this.TheMenu;
            }
        }

        public ContentControl InteractionPane
        {
            get
            {
                return UIContainer;
            }
        }

        #endregion Properties

        #region Event handlers

        private void OnMenuSelectionChanged(object sender, SelectableItemSelectionChangedEventArgs e)
        {
            MenuSelectionChanged?.Invoke(this, e);
        }

        #endregion

    }
}

