﻿using Dazzle.Controls.ExpansionPanels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Workflow
{
    /// <summary>
    /// Interaction logic for BasicWorkflowControl.xaml
    /// </summary>
    public partial class BasicWorkflowControl : UserControl
    {
        public BasicWorkflowControl()
        {
            InitializeComponent();

            TheExpansionPanel.Expand();
        }

        public ExpansionPanelFlipper TheExpansionPanel
        {
            get
            {
                return this.FlipperExpansionPanel;
            }

        }
        public ContentControl InteractionPane
        {
            get
            {
                return this.InteractionArea;
            }
        }

        public ContentControl ToolbarContainer
        {
            get
            {
                return this.VerticalToolbarContainer;
            }

        }

        public Brush ExpanderBackgroundBrush
        {
            get
            {
                return TheExpansionPanel.BackgroundBrush;
            }
            set
            {
                TheExpansionPanel.BackgroundBrush = value;
            }
        }

        public Brush ExpanderBoundaryBrush
        {
            get
            {
                return TheExpansionPanel.BoundaryBrush;
            }
            set
            {
                TheExpansionPanel.BoundaryBrush = value;
            }
        }


        public bool ShowVerticalToolbar
        {
            get
            {
                return VerticalToolbarContainer.Visibility == Visibility.Visible;
            }

            set
            {
                if (value == false)
                {
                    VerticalToolbarContainer.Visibility = Visibility.Collapsed;
                    VerticalToolbarGridColumn.Width = new GridLength(0);
                    VerticalToolbarGridColumn.MinWidth = 0;
                }
                else
                {
                    VerticalToolbarContainer.Visibility = Visibility.Visible;
                    VerticalToolbarGridColumn.Width = new GridLength(55);
                    VerticalToolbarGridColumn.MinWidth = 55;
                }

                UIHelper.ForceSyncUpdates();
            }
        }
    }
}
