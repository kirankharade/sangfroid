﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace Dazzle.Controls.Dialog
{
    /// <summary>
    /// Interaction logic for MessageTextBlock.xaml
    /// </summary>
    public partial class MessageTextBlock : UserControl, INotifyPropertyChanged
    {
        private string _messageText;

        public MessageTextBlock()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string MessageText
        {
            get { return _messageText; }
            set
            {
                _messageText = value;
                RaisePropertyChanged("MessageText");
            }
        }
        private void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
