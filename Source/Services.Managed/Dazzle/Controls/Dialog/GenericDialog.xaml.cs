﻿using Dazzle.Controls.Buttons;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using XUtilities.Managed;

namespace Dazzle.Controls.Dialog
{
    public enum DialogType
    {
        CriticalError,
        Error,
        Warning,
        Information,
        Query,
        Notification,
        Input
    }

    public partial class GenericDialog : Window
    {
        #region Fields

        protected string _headerText = string.Empty;
        protected string _headerIconPath = string.Empty;
        protected bool _showCloseButton = true;
        protected DialogType _flavour = DialogType.Input;
        protected ImageSource _iconImageSource = null;

        #endregion

        #region Events

        public event EventHandler Escape_KeyPressed = null;
        public event EventHandler F10_KeyPressed = null;
        public event EventHandler F1_KeyPressed = null;
        public event EventHandler Enter_KeyPressed = null;
        public event EventHandler DialogClosed = null;
        public event EventHandler DialogLoaded = null;

        #endregion

        public GenericDialog()
        {
            InitializeComponent();
            DataContext = this;
            CloseDialogButton.Clicked += OnClosed;
            Topmost = true;
        }

        private void SetHeaderStyle()
        {
            if (Flavour == DialogType.CriticalError)
            {
                (this.HeaderText as TextBlock).Foreground = this.Resources["Brush_Error"] as SolidColorBrush;
            }
            if (Flavour == DialogType.Error)
            {
                (this.HeaderText as TextBlock).Foreground = this.Resources["Brush_Error"] as SolidColorBrush;
            }
            else if (Flavour == DialogType.Warning)
            {
                (this.HeaderText as TextBlock).Foreground = this.Resources["Brush_Warning"] as SolidColorBrush;
            }
            else if (Flavour == DialogType.Information)
            {
                (this.HeaderText as TextBlock).Foreground = this.Resources["Brush_Information"] as SolidColorBrush;
            }
            else if (Flavour == DialogType.Query)
            {
                (this.HeaderText as TextBlock).Foreground = this.Resources["Brush_Query"] as SolidColorBrush;
            }
            else if (Flavour == DialogType.Notification)
            {
            }
        }

        private void OnClosed(object sender, EventArgs e)
        {
            DialogClosed?.Invoke(this, e);
        }

        #region Properties

        public string Header
        {
            get
            {
                return _headerText;
            }
            set
            {
                _headerText = value;
                this.HeaderText.Text = _headerText;
            }
        }

        public string HeaderIconPath
        {
            get
            {
                return _headerIconPath;
            }
            set
            {
                _headerIconPath = value;
                SetIcon();
            }
        }

        public bool ShowCloseButton
        {
            get
            {
                return _showCloseButton;
            }

            set
            {
                _showCloseButton = value;
            }
        }

        public UIElement BottomRegion
        {
            get
            {
                return this.BottomRowBorder;
            }
        }

        public UIElement BottomSeparator
        {
            get
            {
                return this.BottomRowSeparator;
            }
        }

        public DialogType Flavour
        {
            get
            {
                return _flavour;
            }
            set
            {
                _flavour = value;
                SetHeaderStyle();
            }
        }

        #endregion

        #region Methods

        public void AddButton(DialogButton button)
        {
            ButtonContainer.Children.Add(button);
        }

        public void ClearButtons()
        {
            ButtonContainer.Children.Clear();
        }

        private void SetIcon()
        {
            if (!string.IsNullOrEmpty(HeaderIconPath))
                _iconImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(HeaderIconPath);
            if (_iconImageSource != null)
            {
                HeaderIconImage.Source = _iconImageSource;
            }
        }

        public void SetDialogRegion(UIElement ui)
        {
            for (int i = 0; i < this.MainGrid.Children.Count; i++)
            {
                var control = this.MainGrid.Children[i];
                if (2 == Grid.GetRow(control))
                {
                    this.MainGrid.Children.Remove(ui);
                    break;
                }
            }

            Grid.SetRow(ui, 1);
            Grid.SetColumn(ui, 0);
            this.MainGrid.Children.Add(ui);
        }

        public void CollapseDialogRegion()
        {
            this.DialogRow.Height = new GridLength(0);
        }

        #endregion

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            SetIcon();
            SetHeaderStyle();

            var keyBindings = new List<KeyBinding>();
            keyBindings.Add(new KeyBinding { Key = Key.F10, Command = new RelayCommand((s) => { Close(); }) });
            this.InputBindings.AddRange(keyBindings);

            if (ShowCloseButton)
            {
                CloseDialogButton.Visibility = Visibility.Visible;
            }
            else
            {
                CloseDialogButton.Visibility = Visibility.Hidden;
            }
            DialogLoaded?.Invoke(this, EventArgs.Empty);
        }

        private void HeaderRowBorder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                DragMove();
            }
            catch (InvalidOperationException exp)
            {
            }
            catch (Exception exp)
            {
            }
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                Enter_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.Escape)
            {
                Escape_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.F1)
            {
                F1_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.F10)
            {
                F10_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.System && e.SystemKey == System.Windows.Input.Key.F10)
            {
                F10_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
