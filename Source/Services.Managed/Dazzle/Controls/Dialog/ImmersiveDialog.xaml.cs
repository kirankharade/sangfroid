﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Dazzle.Controls.Dialog
{
    /// <summary>
    /// Interaction logic for LinearProgressWindow.xaml
    /// </summary>
    public partial class ImmersiveDialog : Window, INotifyPropertyChanged
    {

        #region Member variables

        private string _messageText = string.Empty;
        public event EventHandler Clicked = null;

        private string _iconPath = string.Empty;

        Timer _timer = null;
        protected Dispatcher _uiThread = null;
        private double _windowOpacity = 0.95;
        private bool _showCloseButton = false;

        public delegate void ForwardingDelegate(ImmersiveDialog dialog);

        public event EventHandler Escape_KeyPressed = null;
        public event EventHandler F10_KeyPressed = null;
        public event EventHandler F1_KeyPressed = null;
        public event EventHandler Enter_KeyPressed = null;
        public event EventHandler DialogLoaded = null;

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public ImmersiveDialog()
        {
            InitializeComponent();
            DisplayDurationMilliseconds = -1;
            this.CloseNotificationButton.Clicked += OnWindowClosed;
        }

        #region Properties

        public double WindowOpacity
        {
            get
            {
                return _windowOpacity;
            }
            set
            {
                _windowOpacity = value;
                RaisePropertyChanged("WindowOpacity");
            }
        }

        public bool ShowCloseButton
        {
            get
            {
                return _showCloseButton;
            }
            set
            {
                _showCloseButton = value;
                CloseNotificationButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                RaisePropertyChanged("ShowCloseButton");
            }
        }

        public string MessageText
        {
            get
            {
                return _messageText;
            }
            set
            {
                _messageText = value;
                RaisePropertyChanged("MessageText");
            }
        }

        public string IconPath
        {
            get
            {
                return _iconPath;
            }
            set
            {
                _iconPath = value;
                if (!string.IsNullOrEmpty(_iconPath))
                {
                    Icon.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(_iconPath);
                }
            }
        }

        public Image DialogIcon
        {
            get
            {
                return this.Icon;
            }
        }

        public StackPanel TopPanel
        {
            get
            {
                return TopContainer;
            }
        }

        public ContentControl MiddlePanel
        {
            get
            {
                return MiddleContainer;
            }
        }

        public StackPanel ButtonPanel
        {
            get
            {
                return ButtonContainer;
            }
        }

        public int DisplayDurationMilliseconds { get; set; }

        #endregion

        private void OnMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                DragMove();
            }
            catch (InvalidOperationException exp)
            {
            }
            catch (Exception exp)
            {
            }
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnKeyPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                Enter_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.Escape)
            {
                Escape_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.F1)
            {
                F1_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.F10)
            {
                F10_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if(e.Key == System.Windows.Input.Key.System && e.SystemKey == System.Windows.Input.Key.F10)
            {
                F10_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if(DisplayDurationMilliseconds > 0 && DisplayDurationMilliseconds < 60000)
            {
                FadeAway(DisplayDurationMilliseconds);
            }
            DialogLoaded?.Invoke(this, EventArgs.Empty);
        }

        public void FadeAway(int afterTheseManyMiliseconds)
        {
            _uiThread = Dispatcher.CurrentDispatcher;
            _timer = new Timer(this.OnDisplayDurationFinished, this, afterTheseManyMiliseconds, 40);
        }

        private void OnDisplayDurationFinished(object state)
        {
            _uiThread.BeginInvoke(DispatcherPriority.Normal, new ForwardingDelegate(UpdateOpacity), state as ImmersiveDialog);
        }

        static private void UpdateOpacity(ImmersiveDialog dialog)
        {
            var opacity = dialog.Opacity;
            opacity -= 0.1;
            if (opacity < 0)
            {
                dialog.Close();
            }
            else
            {
                dialog.Opacity = opacity;
            }
        }

        private void OnClosed(object sender, EventArgs e)
        {
            if(_timer != null)
            {
                _timer.Dispose();
            }
        }
    }
}
