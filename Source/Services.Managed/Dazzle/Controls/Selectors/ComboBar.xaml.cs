﻿using Dazzle.Controls.Tabs;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Dazzle.Controls.Selectors
{
    /// <summary>
    /// Interaction logic for ComboBar.xaml
    /// </summary>
    public partial class ComboBar : UserControl
    {
        public EventHandler<SelectableItemSelectionChangedEventArgs> ItemSelectionChanged = null;
        bool _keyNavigation = false;

        public ComboBar()
        {
            InitializeComponent();
            ArrowRotationAngle = 0;
            LostFocus += OnCombobarLostFocus;
        }

        #region Properties

        public int SelectedIndex
        {
            get { return (int)this.GetValue(SelectedIndexProperty); }
            set { this.SetValue(SelectedIndexProperty, value); }
        }

        public SelectableItem SelectedItem
        {
            get { return (SelectableItem)this.GetValue(SelectedItemProperty); }
            set { this.SetValue(SelectedItemProperty, value); }
        }

        public ObservableCollection<SelectableItem> Items
        {
            get { return (ObservableCollection<SelectableItem>)this.GetValue(ItemsProperty); }
            set { this.SetValue(ItemsProperty, value); }
        }

        public PlacementMode OpeningDirection
        {
            get { return (PlacementMode)this.GetValue(OpeningDirectionProperty); }
            set { this.SetValue(OpeningDirectionProperty, value); }
        }

        public double ArrowRotationAngle
        {
            get { return (double)this.GetValue(ArrowRotationAngleProperty); }
            set { this.SetValue(ArrowRotationAngleProperty, value); }
        }

        public string SelectedItemHeader
        {
            get { return (string)this.GetValue(SelectedItemHeaderProperty); }
            set { this.SetValue(SelectedItemHeaderProperty, value); }
        }

        public bool IsOpen
        {
            get { return (bool)this.GetValue(IsOpenProperty); }
            private set { this.SetValue(IsOpenProperty, value); }
        }

        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register(
          "SelectedIndex", typeof(int), typeof(ComboBar), new PropertyMetadata(0, SelectedIndexPropertyChanged));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
          "SelectedItem", typeof(SelectableItem), typeof(ComboBar), new PropertyMetadata(null, SelectedItemPropertyChanged));

        public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register(
          "Items", typeof(ObservableCollection<SelectableItem>), typeof(ComboBar), new PropertyMetadata(new ObservableCollection<SelectableItem>()));

        public static readonly DependencyProperty SelectedItemHeaderProperty = DependencyProperty.Register(
          "SelectedItemHeader", typeof(string), typeof(ComboBar), new PropertyMetadata(""));

        public static readonly DependencyProperty OpeningDirectionProperty = DependencyProperty.Register(
          "OpeningDirection", typeof(PlacementMode), typeof(ComboBar), new PropertyMetadata(PlacementMode.Bottom));

        public static readonly DependencyProperty ArrowRotationAngleProperty = DependencyProperty.Register(
          "ArrowRotationAngle", typeof(double), typeof(ComboBar), new PropertyMetadata(0.0));

        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register(
          "IsOpen", typeof(bool), typeof(ComboBar), new PropertyMetadata(false));


        #endregion

        private static void SelectedIndexPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ComboBar).OnSelectedIndexPropertyChanged((int)e.OldValue, (int)e.NewValue);
        }

        private static void SelectedItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ComboBar).OnSelectedItemPropertyChanged((SelectableItem)e.OldValue, (SelectableItem)e.NewValue);
        }

        private void OnSelectedItemPropertyChanged(SelectableItem oldValue, SelectableItem newValue)
        {
            SelectableItem oldItem = oldValue;
            SelectableItem newItem = newValue;
            if (oldItem != null)
            {
                oldItem.Selected = false;
            }
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = oldItem, Current = newItem };
            var ct = newItem;
            if (ct != null)
            {
                ct.Selected = true;
                SelectedItemHeader = ct.Header;
            }
            else
            {
                SelectedItemHeader = "";
            }
            if(!_keyNavigation)
            {
                ThePopup.IsOpen = false;
            }
            ItemSelectionChanged?.Invoke(this, args);
        }

        private void OnSelectedIndexPropertyChanged(int oldValue, int newValue)
        {
            SelectableItem oldItem = (oldValue < Items.Count && oldValue >= 0) ? Items[oldValue] : null;
            SelectableItem newItem = (newValue < Items.Count && newValue >= 0) ? Items[newValue] : null;
            if (oldItem != null)
            {
                oldItem.Selected = false;
            }
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = oldItem, Current = newItem };
            var ct = newItem;
            if (ct != null)
            {
                ct.Selected = true;
                SelectedItemHeader = ct.Header;
            }
            else
            {
                SelectedItemHeader = "";
            }
            if (!_keyNavigation)
            {
                ThePopup.IsOpen = false;
            }
            ItemSelectionChanged?.Invoke(this, args);
        }

        public void ForceResize()
        {
            if (TabListBox != null)
            {
                UIHelper.ForceResize(TabListBox);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if(OpeningDirection == PlacementMode.Right)
            {
                ArrowRotationAngle = -90.0;
            }
            else if(OpeningDirection == PlacementMode.Top)
            {
                ArrowRotationAngle = 180.0;
            }
            else
            {
                ArrowRotationAngle = 0;
            }
            if (SelectedIndex == -1 && Items.Count > 0)
            {
                SelectedIndex = 0;
            }
        }

        private void OnCombobarLostFocus(object sender, RoutedEventArgs e)
        {
            ThePopup.IsOpen = false;
        }

        private void TheComboButton_Checked(object sender, RoutedEventArgs e)
        {
            IsOpen = true;
        }

        private void TheComboButton_Unchecked(object sender, RoutedEventArgs e)
        {
            IsOpen = false;
        }

        public void Previous()
        {
            _keyNavigation = true;

            if (SelectedIndex == 0)
            {
                SelectedIndex = Items.Count - 1;
            }
            else
            {
                SelectedIndex = SelectedIndex - 1;
            }
            SelectedItem = Items[SelectedIndex];

            _keyNavigation = false;
        }

        public void Next()
        {
            _keyNavigation = true;

            if (SelectedIndex == Items.Count - 1)
            {
                SelectedIndex = 0;
            }
            else
            {
                SelectedIndex = SelectedIndex + 1;
            }
            SelectedItem = Items[SelectedIndex];

            _keyNavigation = false;
        }

        public void SelectCurrent()
        {
            ThePopup.IsOpen = false;
        }

    }
}
