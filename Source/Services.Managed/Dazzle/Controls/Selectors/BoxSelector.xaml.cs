﻿using Dazzle.Controls.Tabs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Selectors
{
    /// <summary>
    /// Interaction logic for BoxSelector.xaml
    /// </summary>
    public partial class BoxSelector : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = null;

        public EventHandler<SelectableItemSelectionChangedEventArgs> TabSelectionChanged = null;
        private ObservableCollection<SelectableItem> _items = new ObservableCollection<SelectableItem>();
        private int _selectedIndex = -1;
        private Brush _backgroundBrush = new SolidColorBrush(Color.FromRgb(23, 34, 41));
        private Brush _boundaryBrush = new SolidColorBrush(Color.FromRgb(23, 34, 41));
        private double _selectorHeight = 30;

        public BoxSelector()
        {
            InitializeComponent();
        }

        #region Properties

        public ObservableCollection<SelectableItem> Items => _items;

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedIndex"));
            }
        }

        public int ItemCount
        {
            get
            {
                return _items.Count;
            }
        }

        public SelectableItem this[int index]
        {
            get
            {
                if (_items.Count > index)
                {
                    return _items[index];
                }
                return null;
            }
            set
            {
                if (_items.Count > index)
                {
                    _items[index] = value;
                }
            }
        }

        public Brush BackgroundBrush
        {
            get
            {
                return _backgroundBrush;
            }
            set
            {
                _backgroundBrush = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BackgroundBrush"));
            }
        }

        public Brush BoundaryBrush
        {
            get
            {
                return _boundaryBrush;
            }
            set
            {
                _boundaryBrush = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BoundaryBrush"));
            }
        }

        public double SelectorHeight
        {
            get
            {
                return _selectorHeight;
            }
            set
            {
                _selectorHeight = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectorHeight"));
            }
        }

        #endregion

        public bool Select(string header)
        {
            var tabIndex = -1;
            for (int i = 0; i < _items.Count; i++)
            {
                if (_items[i].Header == header)
                {
                    tabIndex = i;
                    break;
                }
            }
            if (tabIndex > -1)
            {
                SelectedIndex = tabIndex;
                _items[SelectedIndex].Selected = true;
                return true;
            }
            return false;
        }

        public SelectableItem GetItem(string header)
        {
            foreach (var item in _items)
            {
                if (item.Header == header)
                {
                    return item;
                }
            }
            return null;
        }

        public SelectableItem SelectedItem()
        {
            var selectedIndex = SelectedIndex;
            if (selectedIndex < 0 || selectedIndex >= ItemCount)
            {
                return null;
            }
            return this[selectedIndex];
        }

        public bool Add(SelectableItem item)
        {
            if (item == null || GetItem(item.Header) != null || string.IsNullOrWhiteSpace(item.Header))
            {
                return false;
            }
            _items.Add(item);
            return true;
        }

        public bool Remove(string header)
        {
            if (string.IsNullOrWhiteSpace(header))
            {
                return false;
            }
            var item = GetItem(header);
            if (null != item)
            {
                _items.Remove(item);
                return true;
            }
            return false;
        }

        public void Clear()
        {
            _items.Clear();
        }

        public void ForceResize()
        {
            if (TabListBox != null)
            {
                UIHelper.ForceResize(TabListBox);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (SelectedIndex == -1 && Items.Count > 0)
            {
                SelectedIndex = 0;
            }
        }

        protected void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var previousTab = e.RemovedItems.Count > 0 ? e.RemovedItems[0] : null;
            if (previousTab != null)
            {
                (previousTab as SelectableItem).Selected = false;
            }
            var newTab = e.AddedItems.Count > 0 ? e.AddedItems[0] : null;
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = previousTab as SelectableItem, Current = newTab as SelectableItem };

            var ct = SelectedItem();
            if (ct != null)
            {
                ct.Selected = true;
            }
            TabSelectionChanged?.Invoke(this, args);
        }

    }
}
