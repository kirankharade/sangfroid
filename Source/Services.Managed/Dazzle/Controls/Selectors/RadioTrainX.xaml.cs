﻿using Dazzle.Controls.Tabs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Selectors
{

    public partial class RadioTrainX : UserControl
    {
        public EventHandler<SelectableItemSelectionChangedEventArgs> ItemSelectionChanged = null;

        public RadioTrainX()
        {
            InitializeComponent();
            SelectorHeight = 25.0;
        }

        #region Properties

        public int SelectedIndex
        {
            get { return (int)this.GetValue(SelectedIndexProperty); }
            set { this.SetValue(SelectedIndexProperty, value); }
        }

        public SelectableItem SelectedItem
        {
            get { return (SelectableItem)this.GetValue(SelectedItemProperty); }
            set { this.SetValue(SelectedItemProperty, value); }
        }

        public ObservableCollection<SelectableItem> Items
        {
            get { return (ObservableCollection<SelectableItem>)this.GetValue(ItemsProperty); }
            set { this.SetValue(ItemsProperty, value); }
        }

        public double SelectorHeight
        {
            get { return (double)this.GetValue(SelectorHeightProperty); }
            set { this.SetValue(SelectorHeightProperty, value); }
        }

        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register(
          "SelectedIndex", typeof(int), typeof(RadioTrainX), new PropertyMetadata(0, SelectedIndexPropertyChanged));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
          "SelectedItem", typeof(SelectableItem), typeof(RadioTrainX), new PropertyMetadata(null, SelectedItemPropertyChanged));

        public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register(
          "Items", typeof(ObservableCollection<SelectableItem>), typeof(RadioTrainX), new PropertyMetadata(new ObservableCollection<SelectableItem>()));

        public static readonly DependencyProperty SelectorHeightProperty = DependencyProperty.Register(
          "SelectorHeight", typeof(double), typeof(RadioTrainX), new PropertyMetadata(30.0));

        #endregion

        private static void SelectedIndexPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as RadioTrainX).OnSelectedIndexPropertyChanged((int)e.OldValue, (int)e.NewValue);
        }

        private static void SelectedItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as RadioTrainX).OnSelectedItemPropertyChanged((SelectableItem)e.OldValue, (SelectableItem)e.NewValue);
        }

        private void OnSelectedItemPropertyChanged(SelectableItem oldValue, SelectableItem newValue)
        {
            SelectableItem oldItem = oldValue;
            SelectableItem newItem = newValue;
            if (oldItem != null)
            {
                oldItem.Selected = false;
            }
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = oldItem, Current = newItem };
            var ct = newItem;
            if (ct != null)
            {
                ct.Selected = true;
            }
            ItemSelectionChanged?.Invoke(this, args);
        }

        private void OnSelectedIndexPropertyChanged(int oldValue, int newValue)
        {
            SelectableItem oldItem = (oldValue < Items.Count && oldValue >= 0) ? Items[oldValue] : null;
            SelectableItem newItem = (newValue < Items.Count && newValue >= 0) ? Items[newValue] : null;
            if(oldItem != null)
            {
                oldItem.Selected = false;
            }
            var args = new SelectableItemSelectionChangedEventArgs() { Previous = oldItem, Current = newItem };
            var ct = newItem;
            if (ct != null)
            {
                ct.Selected = true;
            }
            ItemSelectionChanged?.Invoke(this, args);
        }


        public void ForceResize()
        {
            if (TabListBox != null)
            {
                UIHelper.ForceResize(TabListBox);
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (SelectedIndex == -1 && Items.Count > 0)
            {
                SelectedIndex = 0;
            }
        }

    }
}
