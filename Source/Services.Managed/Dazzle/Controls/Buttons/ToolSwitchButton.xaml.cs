﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for ToolSwitchButton.xaml
    /// </summary>
    public partial class ToolSwitchButton : UserControl
    {
        #region Member variables

        private ToggleButton _button = null;
        private Action<bool> _action = null;
        private string _tooltip = null;

        private ImageSource _checkedEnabledImageSource = null;
        private ImageSource _checkedDisabledImageSource = null;
        private ImageSource _checkedMouseOverImageSource = null;
        private ImageSource _uncheckedEnabledImageSource = null;
        private ImageSource _uncheckedDisabledImageSource = null;
        private ImageSource _uncheckedMouseOverImageSource = null;

        private string _checkedEnabledImageLocation = string.Empty;
        private string _checkedDisabledImageLocation = string.Empty;
        private string _checkedMouseOverImageLocation = string.Empty;
        private string _uncheckedEnabledImageLocation = string.Empty;
        private string _uncheckedDisabledImageLocation = string.Empty;
        private string _uncheckedMouseOverImageLocation = string.Empty;

        #endregion

        public ToolSwitchButton(
            string checkedEnabledImageLocation,
            string checkedDisabledImageLocation,
            string checkedMouseOverImageLocation,
            string uncheckedEnabledImageLocation,
            string uncheckedDisabledImageLocation,
            string uncheckedMouseOverImageLocation,
            bool isCheckedInitialState
            )
        {
            InitializeComponent();

            _button = ToolButtonElement;
            _button.IsChecked = isCheckedInitialState;

            _checkedEnabledImageLocation     = checkedEnabledImageLocation;
            _checkedDisabledImageLocation    = checkedDisabledImageLocation;
            _checkedMouseOverImageLocation   = checkedMouseOverImageLocation;
            _uncheckedEnabledImageLocation   = uncheckedEnabledImageLocation;
            _uncheckedDisabledImageLocation  = uncheckedDisabledImageLocation;
            _uncheckedMouseOverImageLocation = uncheckedMouseOverImageLocation;

            SetImageSource();
            SetButtonState();
        }

        #region Properties

        public Action<bool> Action
        {
            get
            {
                return _action;
            }

            set
            {
                _action = value;
            }
        }

        public string TooltipText
        {
            get
            {
                return _tooltip;
            }

            set
            {
                _tooltip = value;
                SetTooltip();
            }
        }

        #endregion


        private void SetImageSource()
        {
            if (!string.IsNullOrEmpty(_checkedEnabledImageLocation))
                _checkedEnabledImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_checkedEnabledImageLocation);

            if (!string.IsNullOrEmpty(_checkedDisabledImageLocation))
                _checkedDisabledImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_checkedDisabledImageLocation);

            if (!string.IsNullOrEmpty(_checkedMouseOverImageLocation))
                _checkedMouseOverImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_checkedMouseOverImageLocation);

            if (!string.IsNullOrEmpty(_uncheckedEnabledImageLocation))
                _uncheckedEnabledImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_uncheckedEnabledImageLocation);

            if (!string.IsNullOrEmpty(_uncheckedDisabledImageLocation))
                _uncheckedDisabledImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_uncheckedDisabledImageLocation);

            if (!string.IsNullOrEmpty(_uncheckedMouseOverImageLocation))
                _uncheckedMouseOverImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_uncheckedMouseOverImageLocation);

        }

        private void SetButtonState()
        {
            _button.Background = Brushes.Transparent;
            _button.BorderBrush = Brushes.Transparent;
            _button.BorderThickness = new System.Windows.Thickness(0);

            if (_button.IsChecked == true)
            {
                if(_button.IsEnabled)
                {
                    if(_button.IsMouseOver)
                    {
                        (_button.Content as Image).Source = _checkedMouseOverImageSource;
                    }
                    else
                    {
                        (_button.Content as Image).Source = _checkedEnabledImageSource;
                    }
                }
                else
                {
                    (_button.Content as Image).Source = _checkedDisabledImageSource;
                }
            }
            else
            {
                if (_button.IsEnabled)
                {
                    if (_button.IsMouseOver)
                    {
                        (_button.Content as Image).Source = _uncheckedMouseOverImageSource;
                    }
                    else
                    {
                        (_button.Content as Image).Source = _uncheckedEnabledImageSource;
                    }
                }
                else
                {
                    (_button.Content as Image).Source = _uncheckedDisabledImageSource;
                }
            }
        }

        private void SetTooltip()
        {
            if (string.IsNullOrEmpty(_tooltip))
            {
                return;
            }
            ToolTip t = new ToolTip();
            t.Content = _tooltip;
            t.HasDropShadow = true;
            _button.ToolTip = t;
        }

        #region Events

        private void ToolSwitchButton_MouseEnter(object sender, MouseEventArgs e)
        {
            SetButtonState();
        }

        private void ToolSwitchButton_MouseLeave(object sender, MouseEventArgs e)
        {
            SetButtonState();
        }

        private void ToolSwitchButton_Checked(object sender, RoutedEventArgs e)
        {
            SetButtonState();
            if(_action != null)
            {
                _action(_button.IsChecked == true);
            }
        }

        private void ToolSwitchButton_Unchecked(object sender, RoutedEventArgs e)
        {
            SetButtonState();
            if (_action != null)
            {
                _action(_button.IsChecked == true);
            }
        }

        private void ToolSwitchButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SetButtonState();
        }

        private void ToolSwitchButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SetButtonState();
        }

        private void ToolSwitchButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SetButtonState();
        }

        #endregion

    }
}



