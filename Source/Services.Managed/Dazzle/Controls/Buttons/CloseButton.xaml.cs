﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for CloseButton.xaml
    /// </summary>
    public partial class CloseButton : UserControl
    {
        private Action _action = null;
        private Brush _existingBrush = null;
        public event EventHandler Clicked = null;

        public CloseButton()
        {
            InitializeComponent();
            _existingBrush = ActionButton.Background;
        }

        public Action Action
        {
            get
            {
                return _action;
            }

            set
            {
                _action = value;
            }
        }

        private void OnClicked(object sender, RoutedEventArgs e)
        {
            Clicked?.Invoke(this, new EventArgs());
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            var brush = this.TryFindResource("BackgroundBrush_DialogButton_Focused");
            DrawingPen.Brush = brush as SolidColorBrush;
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            ActionButton.Background = _existingBrush;
            DrawingPen.Brush = new SolidColorBrush(Color.FromRgb(223, 226, 228));
        }
    }
}
