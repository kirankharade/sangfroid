﻿using System.Windows.Controls;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for VerticalToolSeparator.xaml
    /// </summary>
    public partial class VerticalToolSeparator : UserControl
    {
        public VerticalToolSeparator()
        {
            InitializeComponent();
        }
    }
}
