﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for CloseButton.xaml
    /// </summary>
    public partial class TickButton : UserControl
    {
        private Action _action = null;
        private Brush _existingBackgroundBrush = null;
        public event EventHandler Clicked = null;

        public TickButton()
        {
            InitializeComponent();
            _existingBackgroundBrush = BorderBrush_GenericButton.Background;
        }

        public Action Action
        {
            get
            {
                return _action;
            }

            set
            {
                _action = value;
            }
        }

        private void OnMouseEnter(object sender, MouseEventArgs e)
        {
            var brush = this.TryFindResource("BackgroundBrush_DialogButton_Focused");
            BorderBrush_GenericButton.Background = brush as SolidColorBrush;
            DrawingPen.Brush = new SolidColorBrush(Color.FromRgb(255, 255, 255));
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            BorderBrush_GenericButton.Background = _existingBackgroundBrush;
            DrawingPen.Brush = new SolidColorBrush(Colors.DarkGray);
        }

        private void OnClicked(object sender, RoutedEventArgs e)
        {
            Clicked?.Invoke(this, new EventArgs());
        }
    }
}
