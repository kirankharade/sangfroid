﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for OnOffSwitch.xaml
    /// </summary>
    public partial class OnOffSwitch : UserControl, INotifyPropertyChanged
    {
        private string _header = "Header";
        private Brush _foregroundBrush = new SolidColorBrush(Colors.Black);
        private Brush _onForegroundBrush = new SolidColorBrush(Colors.Black);
        private Brush _offForegroundBrush = new SolidColorBrush(Colors.White);

        public event PropertyChangedEventHandler PropertyChanged;

        public OnOffSwitch()
        {
            InitializeComponent();
            Header = OffLabel;
        }

        #region Properties

        public bool IsOn
        {
            get { return (bool)this.GetValue(IsOnProperty); }
            set { this.SetValue(IsOnProperty, value); }
        }

        public string OnLabel
        {
            get { return (string)this.GetValue(OnLabelProperty); }
            set { this.SetValue(OnLabelProperty, value); }
        }

        public string OffLabel
        {
            get { return (string)this.GetValue(OffLabelProperty); }
            set { this.SetValue(OffLabelProperty, value); }
        }

        public Visibility LabelVisibility
        {
            get { return (Visibility)this.GetValue(LabelVisibilityProperty); }
            set { this.SetValue(LabelVisibilityProperty, value); }
        }

        public ImageSource Icon
        {
            get { return (ImageSource)this.GetValue(IconProperty); }
            set { this.SetValue(IconProperty, value); }
        }

        public Visibility IconVisibility
        {
            get { return (Visibility)this.GetValue(IconVisibilityProperty); }
            set { this.SetValue(IconVisibilityProperty, value); }
        }

        public double IconHeight
        {
            get { return (double)this.GetValue(IconHeightProperty); }
            set { this.SetValue(IconHeightProperty, value); }
        }

        public double IconWidth
        {
            get { return (double)this.GetValue(IconWidthProperty); }
            set { this.SetValue(IconWidthProperty, value); }
        }

        public string Tooltip
        {
            get { return (string)this.GetValue(TooltipProperty); }
            set { this.SetValue(TooltipProperty, value); }
        }

        public static readonly DependencyProperty IsOnProperty = DependencyProperty.Register(
          "IsOn", typeof(bool), typeof(OnOffSwitch), new PropertyMetadata(false, IsOnPropertyChanged));

        public static readonly DependencyProperty OnLabelProperty = DependencyProperty.Register(
          "OnLabel", typeof(string), typeof(OnOffSwitch), new PropertyMetadata("Yes", IsOnPropertyChanged));

        public static readonly DependencyProperty OffLabelProperty = DependencyProperty.Register(
          "OffLabel", typeof(string), typeof(OnOffSwitch), new PropertyMetadata("No", IsOnPropertyChanged));

        public static readonly DependencyProperty LabelVisibilityProperty = DependencyProperty.Register(
          "LabelVisibility", typeof(Visibility), typeof(OnOffSwitch), new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
          "Icon", typeof(ImageSource), typeof(OnOffSwitch), new PropertyMetadata(null));

        public static readonly DependencyProperty IconVisibilityProperty = DependencyProperty.Register(
          "IconVisibility", typeof(Visibility), typeof(OnOffSwitch), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty IconHeightProperty = DependencyProperty.Register(
          "IconHeight", typeof(double), typeof(OnOffSwitch), new PropertyMetadata(10.0));

        public static readonly DependencyProperty IconWidthProperty = DependencyProperty.Register(
          "IconWidth", typeof(double), typeof(OnOffSwitch), new PropertyMetadata(10.0));

        //public static readonly DependencyProperty ForegroundBrushProperty = DependencyProperty.Register(
        //  "ForegroundBrush", typeof(Brush), typeof(OnOffSwitch), new PropertyMetadata(new SolidColorBrush(Colors.White)));

        public static readonly DependencyProperty TooltipProperty = DependencyProperty.Register(
          "Tooltip", typeof(string), typeof(OnOffSwitch), new PropertyMetadata(""));

        #endregion

        private static void IsOnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as OnOffSwitch).Update();
        }

        private void Update()
        {
            if(IsOn)
            {
                Header = OnLabel;
                ForegroundBrush = _onForegroundBrush;
            }
            else
            {
                Header = OffLabel;
                ForegroundBrush = _offForegroundBrush;
            }
        }

        public string Header
        {

            get { return _header; }
            set
            {
                _header = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Header"));
            }
        }

        public Brush ForegroundBrush
        {

            get { return _foregroundBrush; }
            set
            {
                _foregroundBrush = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ForegroundBrush"));
            }
        }

    }
}
