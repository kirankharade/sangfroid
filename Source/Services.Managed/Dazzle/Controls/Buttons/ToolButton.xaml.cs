﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Dazzle.Controls.Buttons
{
    public partial class ToolButton : UserControl
    {
        #region Member variables

        protected Button _button = null;
        protected Action _action = null;
        protected string _tooltip = null;

        protected ImageSource _enabledImageSource = null;
        protected ImageSource _disabledImageSource = null;
        protected ImageSource _mouseOverImageSource = null;

        protected string _enabledImageLocation = string.Empty;
        protected string _disabledImageLocation = string.Empty;
        protected string _mouseOverImageLocation = string.Empty;

        #endregion

        public ToolButton(
            string enabledImageLocation,
            string disabledImageLocation,
            string mouseOverImageLocation)
        {
            InitializeComponent();

            _button = ToolButtonElement;
            _button.IsEnabled = true;
            
            _enabledImageLocation = enabledImageLocation;
            _disabledImageLocation = disabledImageLocation;
            _mouseOverImageLocation = mouseOverImageLocation;

            SetImageSource();
            SetButtonState();
        }

        public ToolButton()
        {
            InitializeComponent();

            _button = ToolButtonElement;
            _button.IsEnabled = true;

            SetImageSource();
            SetButtonState();
        }

        public ToolButton(
            ImageSource enabledImageSource,
            ImageSource disabledImageSource,
            ImageSource mouseOverImageSource)
        {
            InitializeComponent();

            _button = ToolButtonElement;
            _button.IsEnabled = true;

            EnabledImageSource = enabledImageSource;
            DisabledImageSource = disabledImageSource;
            MouseOverImageSource = mouseOverImageSource;

            SetButtonState();
        }


        private void SetImageSource()
        {
            if (!string.IsNullOrEmpty(_enabledImageLocation))
                _enabledImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_enabledImageLocation);

            if (!string.IsNullOrEmpty(_disabledImageLocation))
                _disabledImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_disabledImageLocation);

            if (!string.IsNullOrEmpty(_mouseOverImageLocation))
                _mouseOverImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_mouseOverImageLocation);
        }

        #region Properties

        public Action Action
        {
            get
            {
                return _action;
            }

            set
            {
                _action = value;
            }
        }

        public string TooltipText
        {
            get
            {
                return _tooltip;
            }

            set
            {
                _tooltip = value;
                SetTooltip();
            }
        }

        public ImageSource EnabledImageSource
        {
            get
            {
                return _enabledImageSource;
            }

            set
            {
                _enabledImageSource = value;
                SetImageSource();
            }
        }

        public ImageSource DisabledImageSource
        {
            get
            {
                return _disabledImageSource;
            }

            set
            {
                _disabledImageSource = value;
                SetImageSource();
            }
        }

        public ImageSource MouseOverImageSource
        {
            get
            {
                return _mouseOverImageSource;
            }

            set
            {
                _mouseOverImageSource = value;
                SetImageSource();
            }
        }

        public string EnabledImageLocation
        {
            get
            {
                return _enabledImageLocation;
            }

            set
            {
                _enabledImageLocation = value;
                SetImageSource();
            }
        }

        public string DisabledImageLocation
        {
            get
            {
                return _disabledImageLocation;
            }

            set
            {
                _disabledImageLocation = value;
                SetImageSource();
            }
        }

        public string MouseOverImageLocation
        {
            get
            {
                return _mouseOverImageLocation;
            }

            set
            {
                _mouseOverImageLocation = value;
                SetImageSource();
            }
        }

        #endregion

        protected virtual void SetButtonState()
        {
            _button.Background = Brushes.Transparent;
            _button.BorderBrush = Brushes.Transparent;
            _button.BorderThickness = new System.Windows.Thickness(0);

            if (_button.IsEnabled)
            {
                if(_button.IsPressed)
                {
                    ButtonImage.Source = _mouseOverImageSource;
                }
                else if (_button.IsMouseOver)
                {
                    ButtonImage.Source = _mouseOverImageSource;
                }
                else
                {
                    ButtonImage.Source = _enabledImageSource;
                }
            }
            else
            {
                ButtonImage.Source = _disabledImageSource;
            }
        }

        private void SetTooltip()
        {
            if (string.IsNullOrEmpty(_tooltip))
            {
                return;
            }
            ToolTip t = new ToolTip();
            t.Content = _tooltip;
            t.HasDropShadow = true;
            _button.ToolTip = t;
        }

        #region Events

        protected void ToolButton_Click(object sender, RoutedEventArgs e)
        {
            SetButtonState();
            _action?.Invoke();
        }

        protected void ToolButton_MouseEnter(object sender, MouseEventArgs e)
        {
            SetButtonState();
        }

        protected void ToolButton_MouseLeave(object sender, MouseEventArgs e)
        {
            SetButtonState();
        }

        private void ToolButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SetButtonState();
        }

        protected void ToolButtonElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SetButtonState();
        }

        protected void ToolButtonElement_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SetButtonState();
        }

        #endregion

    }
}
