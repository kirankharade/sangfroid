﻿using System.ComponentModel;
using System.Windows.Controls;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for HorizontalToolSeparator.xaml
    /// </summary>
    public partial class HorizontalToolSeparator : UserControl, INotifyPropertyChanged
    {
        public HorizontalToolSeparator()
        {
            InitializeComponent();
        }

        public double SeparatorHeight
        {
            get
            {
                return SeparatorBorder.ActualHeight;
            }
            set
            {
                SeparatorBorder.Height = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SeparatorHeight"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
