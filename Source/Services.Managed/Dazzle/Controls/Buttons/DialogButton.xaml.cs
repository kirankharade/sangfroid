﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.ComponentModel;

namespace Dazzle.Controls.Buttons
{
    /// <summary>
    /// Interaction logic for DialogButton.xaml
    /// </summary>
    public partial class DialogButton : UserControl
    {
        private string _buttonText = string.Empty;
        private string _buttonIcon = string.Empty;
        private ImageSource _buttonIconImageSource = null;

        public event EventHandler Clicked = null;


        public DialogButton()
        {
            InitializeComponent();
        }

        #region Properties

        public string ButtonText
        {
            get
            {
                return _buttonText;
            }

            set
            {
                _buttonText = value;
                ButtonTextBlock.Text = _buttonText;
            }
        }

        public string ButtonIcon
        {
            get
            {
                return _buttonIcon;
            }

            set
            {
                _buttonIcon = value;

                if (!string.IsNullOrEmpty(_buttonIcon))
                    _buttonIconImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_buttonIcon);

            }
        }

        #endregion

        private void DialogBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Clicked?.Invoke(this, new EventArgs());
        }

    }
}
