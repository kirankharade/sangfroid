﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace Dazzle.Controls.Progress
{
    /// <summary>
    /// Interaction logic for SessionProgressControl.xaml
    /// </summary>
    public partial class CircularProgress : UserControl, INotifyPropertyChanged
    {
        double _percentageProgress = 0;

        public delegate void ForwardingDelegate(object state);

        public CircularProgress()
        {
            InitializeComponent();
            ProgressText.Text = GetPercentageText();
            this.ToolTip = "Overall Measurement Session Progress";
        }

        private string GetPercentageText()
        {
            return ((int)_percentageProgress).ToString() + " %";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public double PercentageProgress
        {
            get
            {
                return _percentageProgress;
            }
            set
            {
                _percentageProgress = value;
                ProgressText.Text = GetPercentageText();
                RaisePropertyChanged("PercentageProgress");
            }
        }


    }
}
