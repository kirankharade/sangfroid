﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Dazzle.Controls.Progress
{
    /// <summary>
    /// Interaction logic for LinearProgressWindow.xaml
    /// </summary>
    public partial class LinearProgressWindow : Window, INotifyPropertyChanged
    {

        #region Member variables

        private string _messageText = string.Empty;
        private string _hintText = string.Empty;
        public event EventHandler Clicked = null;
        private int _currentProgress = 0;

        Timer _timer = null;
        protected Dispatcher _uiThread = null;
        private double _windowOpacity = 0.97;

        public delegate void ForwardingDelegate(LinearProgressWindow window);

        public event EventHandler Escape_KeyPressed = null;
        public event EventHandler Enter_KeyPressed = null;

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public LinearProgressWindow()
        {
            InitializeComponent();
            this.CloseNotificationButton.Clicked += OnWindowClosed;
        }

        #region Properties

        public string MessageText
        {
            get
            {
                return _messageText;
            }
            set
            {
                _messageText = value;
                RaisePropertyChanged("MessageText");
            }
        }

        public string ClickHintText
        {
            get
            {
                return _hintText;
            }
            set
            {
                _hintText = value;
                RaisePropertyChanged("ClickHintText");
            }
        }

        public int CurrentProgress
        {
            get { return _currentProgress; }
            set
            {
                if (_currentProgress != value)
                {
                    _currentProgress = value;
                    RaisePropertyChanged("CurrentProgress");
                }
            }
        }

        public double WindowOpacity
        {
            get
            {
                return _windowOpacity;
            }
            set
            {
                _windowOpacity = value;
                RaisePropertyChanged("WindowOpacity");
            }
        }

        public Label HintLabel
        {
            get
            {
                return HintText;
            }
        }

        #endregion

        private void OnMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(ProgressGrid.Visibility == Visibility.Visible)
            {
                try
                {
                    DragMove();
                }
                catch (InvalidOperationException exp)
                {
                }
                catch (Exception exp)
                {
                }
            }
            else
            {
                Clicked?.Invoke(this, EventArgs.Empty);
            }
        }

        private void OnWindowClosed(object sender, EventArgs e)
        {
            if (_timer != null)
            {
                _timer.Dispose();
            }
            this.Close();
        }

        public void HideProgress()
        {
            ProgressGrid.Visibility = Visibility.Collapsed;
            this.CloseNotificationButton.Visibility = Visibility.Visible;
        }

        public void FadeAway(int afterTheseManyMiliseconds)
        {
            _uiThread = Dispatcher.CurrentDispatcher;
            _timer = new Timer(this.OnDisplayDurationFinished, this, afterTheseManyMiliseconds, 40);
        }

        private void OnDisplayDurationFinished(object state)
        {
            _uiThread.BeginInvoke(DispatcherPriority.Normal, new ForwardingDelegate(UpdateOpacity), state as LinearProgressWindow);
        }

        static private void UpdateOpacity(LinearProgressWindow dialog)
        {
            var opacity = dialog.Opacity;
            opacity -= 0.1;
            if (opacity < 0)
            {
                dialog.Close();
            }
            else
            {
                dialog.Opacity = opacity;
            }
        }

        private void OnKeyPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                Enter_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
            else if (e.Key == System.Windows.Input.Key.Escape)
            {
                Escape_KeyPressed?.Invoke(this, EventArgs.Empty);
            }
        }


    }
}
