﻿namespace Dazzle.Controls.Balloons
{
    public enum BalloonAnimationTypes
    {
        Unbox,
        Unfurl,
        None
    }
}
