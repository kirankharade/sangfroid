﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Windows.Forms;

namespace Dazzle.Controls.Balloons
{
    public class BalloonBase : Window, INotifyPropertyChanged
    {
        protected System.Windows.Controls.Control _hostedControl = null;
        protected double _windowOpacity = 0.9;
        protected PopupPosition _popupPosition = PopupPosition.ScreenCenter;

        protected int _firstDuration = 230;
        protected int _secondDuration = 280;
        protected int _thirdDuration = 280;

        protected double _margin = 20;
        protected double _finalWidth = 0;
        protected double _finalHeight = 0;
        protected double _finalLeft = 0;
        protected double _finalTop = 0;

        protected double _startingWidth = 5;
        protected double _startingHeight = 5;
        protected double _startingLeft = 0;
        protected double _startingTop = 0;

        protected bool _isModal = true;
        protected bool _isResizable = true;

        protected double _tickButtonSize = 40;

        protected System.Windows.Point _animStartPosition = new Point(0, 0);
        protected System.Windows.Point _animEndPosition = new Point(0, 0);

        protected Storyboard _firstAnimationStoryboard = null;
        protected Storyboard _secondAnimationStoryboard = null;
        protected Storyboard _thirdAnimationStoryboard = null;

        protected DoubleAnimation _windowTopAnimation = null;
        protected DoubleAnimation _windowLeftAnimation = null;
        protected DoubleAnimation _windowHeightAnimation = null;
        protected DoubleAnimation _windowWidthAnimation = null;

        protected Action _closeCallbackAction = null;
        protected Action _sizeChangedCallbackAction = null;
        private BalloonAnimationTypes _animationType = BalloonAnimationTypes.Unbox;

        #region Event handlers

        public EventHandler CompletelyAppeared = null;
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public BalloonBase(
            System.Windows.Point animStartPosition,
            PopupPosition popupPosition,
            double width, 
            double height, 
            bool IsModal,
            bool isResizable,
            Action closeCallback,
            Action sizeChangedCallback)
        {
            _closeCallbackAction = closeCallback;
            _sizeChangedCallbackAction = sizeChangedCallback;

            _animStartPosition = animStartPosition;
            _popupPosition = popupPosition;
            _finalHeight = height;
            _finalWidth = width;
            _isModal = IsModal;
            _isResizable = isResizable;

            ResizeMode = _isResizable ? ResizeMode.CanResize : ResizeMode.NoResize;

            Loaded += new RoutedEventHandler(OnLoaded);
        }

        #region Properties

        public bool IsModal
        {
            get { return _isModal; }

            set { _isModal = value; }
        }

        public Action CloseCallbackAction
        {
            get { return _closeCallbackAction; }

            set { _closeCallbackAction = value; }
        }

        public Action SizeChangedCallbackAction
        {
            get { return _sizeChangedCallbackAction; }

            set { _sizeChangedCallbackAction = value; }
        }

        public System.Windows.Controls.Control HostedContent
        {
            get { return _hostedControl; }
            set
            {
                _hostedControl = value;
            }
        }

        public double WindowOpacity
        {
            get { return _windowOpacity; }
            set
            {
                _windowOpacity = value;
                RaisePropertyChanged("WindowOpacity");
            }
        }

        public double TickButtonSize
        {
            get { return _tickButtonSize; }
            set
            {
                _tickButtonSize = value;
                RaisePropertyChanged("TickButtonSize");
            }
        }

        public BalloonAnimationTypes AnimationType
        {
            get { return _animationType; }
            set
            {
                _animationType = value;
                RaisePropertyChanged("AnimationType");
            }
        }

        #endregion

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            base.ResizeMode = _isResizable ? ResizeMode.CanResize : ResizeMode.NoResize;

            Init();
            InitAnimations();

            Top = _startingTop;
            Left = _startingLeft;
            Width = _startingWidth;
            Height = _startingHeight;

            _firstAnimationStoryboard.Begin(this, true);
        }

        private void OnFirstAnimationStageFinished(object sender, EventArgs e)
        {
            _secondAnimationStoryboard.Begin(this, true);
        }

        private void OnSecondAnimationStageFinished(object sender, EventArgs e)
        {
            _thirdAnimationStoryboard.Begin(this, true);
        }

        private void OnThirdAnimationStageFinished(object sender, EventArgs e)
        {
            Top = _finalTop;
            Left = _finalLeft;
            Width = _finalWidth;
            Height = _finalHeight;

            CompletelyAppeared?.Invoke(this, e);
        }

        protected void Init()
        {
            GetAnimationEndPosition();
        }

        protected void GetAnimationEndPosition()
        {
            var screenArea = GetWorkingArea();
            var halfScreenWidth = screenArea.Width / 2.0;
            var halfScreenHeight = screenArea.Height / 2.0;

            var finalHalfWidth = _finalWidth / 2.0;
            var finalHalfHeight = _finalHeight / 2.0;
            var startingHalfWidth = _startingWidth / 2.0;
            var startingHalfHeight = _startingHeight / 2.0;

            _startingLeft = _animStartPosition.X;
            _startingTop = _animStartPosition.Y;

            var margin = 5;

            if (_popupPosition == PopupPosition.ScreenCenter)
            {
                _finalLeft = halfScreenWidth - finalHalfWidth;
                _finalTop = halfScreenHeight - finalHalfHeight;
                _startingLeft = _animStartPosition.X - startingHalfWidth;
                _startingTop = _animStartPosition.Y - startingHalfHeight;
            }
            else if(_popupPosition == PopupPosition.Top)
            {
                _finalLeft = _animStartPosition.X - finalHalfWidth;
                _finalTop = _animStartPosition.Y - (_finalHeight /*+ finalHalfHeight*/ + margin);
            }
            else if(_popupPosition == PopupPosition.Bottom)
            {
                _finalLeft = _animStartPosition.X - finalHalfWidth;
                _finalTop = _animStartPosition.Y + (_finalHeight /*+ finalHalfHeight*/ + margin);
            }
            else if(_popupPosition == PopupPosition.Left)
            {
                _finalLeft = _animStartPosition.X - (_finalWidth /*+ finalHalfWidth*/ + margin);
                _finalTop = _animStartPosition.Y - finalHalfHeight;
            }
            else
            {
                _finalLeft = _animStartPosition.X + (finalHalfWidth * 0.15 + margin); /*_finalWidth * 0.5 + finalHalfWidth + */
                _finalTop = _animStartPosition.Y + _finalHeight;
            }

            //Final correction
            if (_startingLeft < _margin) _startingLeft = _margin;
            if (_startingLeft > (screenArea.Width - _margin)) _startingLeft = screenArea.Width - _margin;
            if (_startingTop < _margin) _startingTop = _margin;
            if (_startingTop > (screenArea.Height - _margin)) _startingTop = screenArea.Height - _margin;

            if (_finalLeft < _margin) _finalLeft = _margin;
            if (_finalLeft > (screenArea.Width - _margin)) _finalLeft = screenArea.Width - _margin;
            if (_finalTop < _margin) _finalTop = _margin;
            if (_finalTop > (screenArea.Height - _margin)) _finalTop = screenArea.Height - _margin;
        }

        protected void InitAnimations()
        {
            var d1 = new TimeSpan(0, 0, 0, 0, (int)_firstDuration);
            var d2 = new TimeSpan(0, 0, 0, 0, (int)_secondDuration);
            var d3 = new TimeSpan(0, 0, 0, 0, (int)_thirdDuration);

            if(_animationType == BalloonAnimationTypes.Unbox)
            {
                //First stage of animation

                _windowTopAnimation = new DoubleAnimation(_startingTop, _finalTop, d1, FillBehavior.Stop);
                Storyboard.SetTarget(_windowTopAnimation, this);
                Storyboard.SetTargetProperty(_windowTopAnimation, new PropertyPath(Window.TopProperty));

                _windowLeftAnimation = new DoubleAnimation(_startingLeft, _finalLeft, d1, FillBehavior.Stop);
                Storyboard.SetTarget(_windowLeftAnimation, this);
                Storyboard.SetTargetProperty(_windowLeftAnimation, new PropertyPath(Window.LeftProperty));

                _firstAnimationStoryboard = new Storyboard();
                _firstAnimationStoryboard.Children.Add(_windowTopAnimation);
                _firstAnimationStoryboard.Children.Add(_windowLeftAnimation);
                _firstAnimationStoryboard.FillBehavior = FillBehavior.Stop;

                //Second stage of animation

                _windowWidthAnimation = new DoubleAnimation(_startingWidth, _finalWidth, d2, FillBehavior.Stop);
                Storyboard.SetTarget(_windowWidthAnimation, this);
                Storyboard.SetTargetProperty(_windowWidthAnimation, new PropertyPath(FrameworkElement.WidthProperty));

                _secondAnimationStoryboard = new Storyboard();
                _secondAnimationStoryboard.Children.Add(_windowWidthAnimation);
                _secondAnimationStoryboard.FillBehavior = FillBehavior.Stop;

                //Third stage of animation

                _windowHeightAnimation = new DoubleAnimation(_startingHeight, _finalHeight, d3, FillBehavior.Stop);
                Storyboard.SetTarget(_windowHeightAnimation, this);
                Storyboard.SetTargetProperty(_windowHeightAnimation, new PropertyPath(FrameworkElement.HeightProperty));

                _thirdAnimationStoryboard = new Storyboard();
                _thirdAnimationStoryboard.Children.Add(_windowHeightAnimation);
                _thirdAnimationStoryboard.FillBehavior = FillBehavior.Stop;
            }
            else if(_animationType == BalloonAnimationTypes.Unfurl)
            {
                _windowTopAnimation = new DoubleAnimation(_startingTop, _finalTop, d1, FillBehavior.Stop);
                Storyboard.SetTarget(_windowTopAnimation, this);
                Storyboard.SetTargetProperty(_windowTopAnimation, new PropertyPath(Window.TopProperty));

                _windowLeftAnimation = new DoubleAnimation(_startingLeft, _finalLeft, d1, FillBehavior.Stop);
                Storyboard.SetTarget(_windowLeftAnimation, this);
                Storyboard.SetTargetProperty(_windowLeftAnimation, new PropertyPath(Window.LeftProperty));

                _firstAnimationStoryboard = new Storyboard();
                _firstAnimationStoryboard.Children.Add(_windowTopAnimation);
                _firstAnimationStoryboard.Children.Add(_windowLeftAnimation);
                _firstAnimationStoryboard.FillBehavior = FillBehavior.Stop;

                //Second stage of animation

                d2 = new TimeSpan(0, 0, 0, 0, (int)10);
                _startingWidth = _finalWidth;
                _windowWidthAnimation = new DoubleAnimation(_startingWidth, _finalWidth, d2, FillBehavior.Stop);
                Storyboard.SetTarget(_windowWidthAnimation, this);
                Storyboard.SetTargetProperty(_windowWidthAnimation, new PropertyPath(FrameworkElement.WidthProperty));

                _secondAnimationStoryboard = new Storyboard();
                _secondAnimationStoryboard.Children.Add(_windowWidthAnimation);
                _secondAnimationStoryboard.FillBehavior = FillBehavior.Stop;

                //Third stage of animation

                _windowHeightAnimation = new DoubleAnimation(_startingHeight, _finalHeight, d3, FillBehavior.Stop);
                Storyboard.SetTarget(_windowHeightAnimation, this);
                Storyboard.SetTargetProperty(_windowHeightAnimation, new PropertyPath(FrameworkElement.HeightProperty));

                _thirdAnimationStoryboard = new Storyboard();
                _thirdAnimationStoryboard.Children.Add(_windowHeightAnimation);
                _thirdAnimationStoryboard.FillBehavior = FillBehavior.Stop;
            }
            else
            {
                d1 = new TimeSpan(0, 0, 0, 0, (int) 10);
                d2 = new TimeSpan(0, 0, 0, 0, (int) 10);
                d3 = new TimeSpan(0, 0, 0, 0, (int) 10);

                _startingTop = _finalTop;
                _startingLeft = _finalLeft;
                _startingWidth = _finalWidth;
                _startingHeight = _finalHeight;

                _windowTopAnimation = new DoubleAnimation(_startingTop, _finalTop, d1, FillBehavior.Stop);
                Storyboard.SetTarget(_windowTopAnimation, this);
                Storyboard.SetTargetProperty(_windowTopAnimation, new PropertyPath(Window.TopProperty));

                _windowLeftAnimation = new DoubleAnimation(_startingLeft, _finalLeft, d1, FillBehavior.Stop);
                Storyboard.SetTarget(_windowLeftAnimation, this);
                Storyboard.SetTargetProperty(_windowLeftAnimation, new PropertyPath(Window.LeftProperty));

                _firstAnimationStoryboard = new Storyboard();
                _firstAnimationStoryboard.Children.Add(_windowTopAnimation);
                _firstAnimationStoryboard.Children.Add(_windowLeftAnimation);
                _firstAnimationStoryboard.FillBehavior = FillBehavior.Stop;

                //Second stage of animation

                d2 = new TimeSpan(0, 0, 0, 0, (int)10);
                _startingWidth = _finalWidth;
                _windowWidthAnimation = new DoubleAnimation(_startingWidth, _finalWidth, d2, FillBehavior.Stop);
                Storyboard.SetTarget(_windowWidthAnimation, this);
                Storyboard.SetTargetProperty(_windowWidthAnimation, new PropertyPath(FrameworkElement.WidthProperty));

                _secondAnimationStoryboard = new Storyboard();
                _secondAnimationStoryboard.Children.Add(_windowWidthAnimation);
                _secondAnimationStoryboard.FillBehavior = FillBehavior.Stop;

                //Third stage of animation

                _windowHeightAnimation = new DoubleAnimation(_startingHeight, _finalHeight, d3, FillBehavior.Stop);
                Storyboard.SetTarget(_windowHeightAnimation, this);
                Storyboard.SetTargetProperty(_windowHeightAnimation, new PropertyPath(FrameworkElement.HeightProperty));

                _thirdAnimationStoryboard = new Storyboard();
                _thirdAnimationStoryboard.Children.Add(_windowHeightAnimation);
                _thirdAnimationStoryboard.FillBehavior = FillBehavior.Stop;

            }

            _firstAnimationStoryboard.Completed += OnFirstAnimationStageFinished;
            _secondAnimationStoryboard.Completed += OnSecondAnimationStageFinished;
            _thirdAnimationStoryboard.Completed += OnThirdAnimationStageFinished;
        }

        private System.Drawing.Rectangle GetWorkingArea()
        {
            var rect = new System.Drawing.Rectangle((int)Left, (int)Top, (int)ActualWidth, (int)ActualHeight);
            var workingArea = Screen.GetWorkingArea(rect);
            workingArea.Width = (int)System.Windows.SystemParameters.PrimaryScreenWidth;
            workingArea.Height = (int)System.Windows.SystemParameters.PrimaryScreenHeight;

            return workingArea;
        }

        #region Public Methods

        public void Bulge()
        {
            if (!IsLoaded)
            {
                if(_isModal)
                {
                    this.ShowDialog();
                }
                else
                {
                    this.Show();
                }
            }
        }

        #endregion

        #region Private Methods

        protected override void OnInitialized(EventArgs e)
        {
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            ShowInTaskbar = false;
            Topmost = true;
            base.OnInitialized(e);
        }

        private void DisconnectHandlers()
        {
            _firstAnimationStoryboard.Completed -= OnFirstAnimationStageFinished;
            _secondAnimationStoryboard.Completed -= OnSecondAnimationStageFinished;
            _thirdAnimationStoryboard.Completed -= OnThirdAnimationStageFinished;
        }

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
