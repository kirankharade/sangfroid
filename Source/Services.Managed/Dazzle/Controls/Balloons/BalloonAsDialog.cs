﻿using Dazzle.Appearance;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Dazzle.Controls.Balloons
{
    public class BalloonAsDialog
    {
        BalloonDialog _dialog = null;

        public BalloonAsDialog()
        {
            AnimationStartPosition = new Point(0, 0);
            PopupPosition = PopupPosition.ScreenCenter;
            Width = 150;
            Height = 150;
            HostedControl = null;
            IsModal = true;
            IsResizable = true;
            CloseCallback = null;
            ResizeCallback = null;
            ShowMaximizeButton = true;
        }

        #region Proerties

        public Point AnimationStartPosition { get; set; }

        public PopupPosition PopupPosition { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public Control HostedControl { get; set; }

        public bool IsModal { get; set; }

        public bool IsResizable { get; set; }

        public Action CloseCallback { get; set; }

        public Action ResizeCallback { get; set; }

        public bool ShowMaximizeButton { get; set; }

        public bool ShowHeaderPanel { get; set; }

        #endregion

        #region Public methods

        public void Show()
        {
            _dialog = new BalloonDialog(AnimationStartPosition, PopupPosition, Width, Height, HostedControl, IsModal, IsResizable, CloseCallback, ResizeCallback);
            _dialog.AnimationType = BalloonAnimationTypes.Unfurl;
            _dialog.ShowMaximizeMinimize = ShowMaximizeButton;
            _dialog.ShowHeaderPanel = ShowHeaderPanel;
            _dialog.CompletelyAppeared += (s, e) => { _dialog.HostContainer.Content = _dialog.HostedContent; };
            AppearanceManager.SetAppearance(_dialog);
            _dialog.Bulge();
        }

        public void Close()
        {
            if(_dialog != null)
            {
                _dialog.Close();
            }
        }

        #endregion
    }

}
