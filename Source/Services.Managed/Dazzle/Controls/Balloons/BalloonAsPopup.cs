﻿using Dazzle.Appearance;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Dazzle.Controls.Balloons
{
    public class BalloonAsPopup
    {
        BalloonPopup _dialog = null;

        public BalloonAsPopup()
        {
            AnimationStartPosition = new Point(0, 0);
            PopupPosition = PopupPosition.ScreenCenter;
            Width = 150;
            Height = 150;
            HostedControl = null;
            CloseCallback = null;
            TickButtonSize = 40;
        }

        #region Proerties

        public Point AnimationStartPosition { get; set; }

        public PopupPosition PopupPosition { get; set; }

        public double Width { get; set; }

        public double Height { get; set; }

        public double TickButtonSize { get; set; }

        public Control HostedControl { get; set; }

        public Action CloseCallback { get; set; }

        public BalloonAnimationTypes AnimationType { get; set; }

        #endregion

        #region Public methods

        public void Show()
        {
            _dialog = new BalloonPopup(AnimationStartPosition, PopupPosition, Width, Height, TickButtonSize, HostedControl, CloseCallback);
            _dialog.AnimationType = AnimationType;
            _dialog.CompletelyAppeared += (s, e) => { _dialog.HostContainer.Content = _dialog.HostedContent; };
            AppearanceManager.SetAppearance(_dialog);
            _dialog.Bulge();
        }

        public void Close()
        {
            if (_dialog != null)
            {
                _dialog.Close();
            }
        }

        #endregion
    }
}
