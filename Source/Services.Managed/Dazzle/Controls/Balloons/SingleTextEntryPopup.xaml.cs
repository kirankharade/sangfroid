﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

//Usage: ()=> { SingleTextEntryPopup.Popup("", new Point(150, 150), PopupPosition.ScreenCenter); }

namespace Dazzle.Controls.Balloons
{
    public partial class SingleTextEntryPopup : UserControl, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private string _text = " ";
        private ImageSource _focussedTickIcon = null;
        private ImageSource _unfocussedTickIcon = null;
        private ImageSource _icon = null;
        private readonly Regex _regExForComparisonName = new Regex(@"^[a-zA-Z0-9\s'('')']+");
        private Brush _labelForeground = new SolidColorBrush(Colors.YellowGreen);
        private string _fieldLabel = "Label";

        public event EventHandler<string> Closed = null;
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public SingleTextEntryPopup(string defaultText)
        {
            InitializeComponent();

            DataContext = this;

            var onIconPath = "pack://application:,,,/Dazzle;component/Images/Active_checked.png";
            var offIconPath = "pack://application:,,,/Dazzle;component/Images/Active_unfocussed.png";

            _focussedTickIcon = (ImageSource)new ImageSourceConverter().ConvertFromString(onIconPath);
            _unfocussedTickIcon = (ImageSource)new ImageSourceConverter().ConvertFromString(offIconPath);

            _text = defaultText;

            AcceptIcon = _unfocussedTickIcon;
            DataObject.AddPastingHandler(TheTextBox, PasteHandler);
        }

        #region Static public methods

        static public string Popup(string defaultText, Point animStartPosition, PopupPosition popupPosition)
        {
            var control = new SingleTextEntryPopup(defaultText);

            control.VerticalAlignment = VerticalAlignment.Top;
            control.HorizontalAlignment = HorizontalAlignment.Left;
            control.HorizontalContentAlignment = HorizontalAlignment.Left;
            control.VerticalContentAlignment = VerticalAlignment.Top;

            var text = "";

            var balloon = new BalloonAsDialog()
            {
                Height = 36,
                Width = 445,
                AnimationStartPosition = animStartPosition,
                ShowHeaderPanel = false,
                ShowMaximizeButton = false,
                PopupPosition = popupPosition,
                HostedControl = control,
                IsResizable = false,
                IsModal = true,
                CloseCallback = () => { text = control.Text; },
            };
            control.Closed += (s, e) => { balloon.Close(); text = e; };
            balloon.Show();

            return text;
        }

        #endregion

        #region Properties

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
            }
        }

        public ImageSource AcceptIcon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AcceptIcon"));
            }
        }

        public Brush LabelForeground
        {
            get
            {
                return _labelForeground;
            }
            set
            {
                _labelForeground = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LabelForeground"));
            }
        }

        public string FieldLabel
        {
            get
            {
                return _fieldLabel;
            }
            set
            {
                _fieldLabel = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FieldLabel"));
            }
        }

        #endregion

        #region IDataErrorInfo methods

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;

                if (columnName != nameof(Text))
                {
                    return result;
                }

                var compName = this.Text.Trim();

                if (compName == "")
                    result = "An empty string";

                return result;
            }
        }

        #endregion

        #region Event handlers

        private void TheTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(TheTextBox.Text))
                {
                    return;
                }
                _text = TheTextBox.Text;
                Closed?.Invoke(this, _text);
            }
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            _text = TheTextBox.Text;
            Closed?.Invoke(this, _text);
        }

        private void AcceptButton_MouseEnter(object sender, MouseEventArgs e)
        {
            AcceptIcon = _focussedTickIcon;
        }

        private void AcceptButton_MouseLeave(object sender, MouseEventArgs e)
        {
            AcceptIcon = _unfocussedTickIcon;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(TheTextBox.Text))
                {
                    return;
                }
                _text = TheTextBox.Text;
                Closed?.Invoke(this, _text);
            }
        }

        private void TheTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = e.IsRepeat;
        }

        private void TheTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!_regExForComparisonName.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }

        private void PasteHandler(object sender, DataObjectPastingEventArgs e)
        {
            e.CancelCommand();
        }

        #endregion
    }
}
