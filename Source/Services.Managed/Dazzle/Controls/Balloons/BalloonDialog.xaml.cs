﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Dazzle.Controls.Balloons
{
    /// <summary>
    /// Interaction logic for BalloonDialog.xaml
    /// </summary>
    public partial class BalloonDialog : BalloonBase
    {
        private bool _showMaximizeMinimize = true;

        public BalloonDialog(
            Point animStartPosition, 
            double width, 
            double height, 
            Control control, 
            bool isModal,
            bool isResizable,
            Action closeCallback,
            Action sizeChangedCallback)
            :base(animStartPosition, PopupPosition.ScreenCenter, width, height, isModal, isResizable, closeCallback, sizeChangedCallback)
        {
            InitializeComponent();

            HostedContent = control;
            CloseButton.Clicked += OnClosed;
            MaximizeButton.Clicked += OnMaximized;
            RestoreButton.Clicked += OnRestore;
        }
        public BalloonDialog(
            Point animStartPosition,
            PopupPosition popupPosition,
            double width,
            double height,
            Control control,
            bool isModal,
            bool isResizable,
            Action closeCallback,
            Action sizeChangedCallback)
            : base(animStartPosition, popupPosition, width, height, isModal, isResizable, closeCallback, sizeChangedCallback)
        {
            InitializeComponent();

            HostedContent = control;
            CloseButton.Clicked += OnClosed;
            MaximizeButton.Clicked += OnMaximized;
            RestoreButton.Clicked += OnRestore;
        }

        #region Properties

        public bool ShowMaximizeMinimize
        {
            get { return _showMaximizeMinimize; }
            set
            {
                _showMaximizeMinimize = value;

                if(!value)
                {
                    RestoreButton.Visibility = Visibility.Collapsed;
                    MaximizeButton.Visibility = Visibility.Collapsed;
                }
                else
                {
                    if (this.WindowState == WindowState.Normal)
                    {
                        RestoreButton.Visibility = Visibility.Collapsed;
                        MaximizeButton.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        RestoreButton.Visibility = Visibility.Visible;
                        MaximizeButton.Visibility = Visibility.Collapsed;
                    }
                }
                UIHelper.ForceSyncUpdates(); ;
            }
        }

        public bool ShowHeaderPanel
        {
            get
            {
                if(HeaderRowBorder.Visibility == Visibility.Visible)
                {
                    return true;
                }
                return false;
            }
            set
            {
                HeaderRowBorder.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                if(HeaderRowBorder.Visibility != Visibility.Visible)
                {
                    HeaderRow.Height = new GridLength(0);
                }
                UIHelper.ForceSyncUpdates(); ;
            }
        }

        #endregion

        private void OnClosed(object sender, EventArgs e)
        {
            _closeCallbackAction?.Invoke();
            this.Close();
        }

        private void OnRestore(object sender, EventArgs e)
        {
            this.WindowState = WindowState.Normal;
            RestoreButton.Visibility = Visibility.Collapsed;
            MaximizeButton.Visibility = Visibility.Visible;

            _sizeChangedCallbackAction?.Invoke();
        }

        private void OnMaximized(object sender, EventArgs e)
        {
            this.WindowState = WindowState.Maximized;
            RestoreButton.Visibility = Visibility.Visible;
            MaximizeButton.Visibility = Visibility.Collapsed;

            _sizeChangedCallbackAction?.Invoke();
        }

        private void OnMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                DragMove();
            }
            catch (InvalidOperationException exp)
            {
            }
            catch (Exception exp)
            {
            }
        }

        private void OnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Escape)
            {
                this.Close();
            }
        }
    }
}
