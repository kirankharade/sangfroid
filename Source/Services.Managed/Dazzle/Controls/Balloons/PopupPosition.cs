﻿namespace Dazzle.Controls.Balloons
{
    public enum PopupPosition { Top, Bottom, Left, Right, ScreenCenter }
}
