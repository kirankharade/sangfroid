﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Dazzle.Controls.Balloons
{
    /// <summary>
    /// Interaction logic for BalloonPopup.xaml
    /// </summary>
    public partial class BalloonPopup : BalloonBase
    {
        public BalloonPopup(
            Point animStartPosition,
            PopupPosition popupPosition,
            double width,
            double height,
            double tickButtonSize,
            Control hostedControl,
            Action closeCallback)
            : base(animStartPosition, popupPosition, width, height, true, true, closeCallback, null)
        {
            InitializeComponent();

            _closeCallbackAction = closeCallback;
            HostedContent = hostedControl;
            CloseButton.Clicked += OnClosed;
            TickButtonSize = tickButtonSize;
        }

        private void OnClosed(object sender, EventArgs e)
        {
            _closeCallbackAction?.Invoke();
            UIHelper.ForceSyncUpdates();
            this.Close();
        }

        private void OuterBorder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                DragMove();
            }
            catch (InvalidOperationException exp)
            {
            }
            catch (Exception exp)
            {
            }
        }
    }
}
