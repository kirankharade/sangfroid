﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Dazzle.Appearance
{

    /// <summary>
    /// This empty wrapper class can be used to distinguish between 
    /// theme specific ResourceDictionaries and other ResourceDictionaries.
    /// </summary>
    public class ThemeResourceDictionary : ResourceDictionary
    {
    }

    /// <summary>
    /// This empty wrapper class can be used to distinguish between 
    /// skin specific ResourceDictionaries and other ResourceDictionaries.
    /// </summary>
    public class SkinResourceDictionary : ResourceDictionary
    {
    }

    public class LayoutResourceDictionary : ResourceDictionary
    {
    }

    /// <summary>
    /// This class is used to change theme and skin at run time.
    /// When change request comes (for theme or skin), this class creates a new instance of <see cref="ThemeResourceDictionary"/> 
    /// or <see cref="SkinResourceDictionary"/> as the case may be and adds it to the MergedDictionaries.
    /// At the same time it searches for other ResourceDitionaries of type <see cref="ThemeResourceDictionary"/> 
    /// or <see cref="SkinResourceDictionary"/> and removes them so that only one theme is used.
    /// </summary>
    /// 
    public class ThemeSkinLayoutSelector : DependencyObject
    {
        public static readonly DependencyProperty CurrentThemeDictionaryProperty = DependencyProperty.RegisterAttached(
            "CurrentThemeDictionary",
            typeof(Uri),
            typeof(ThemeSkinLayoutSelector),
            new UIPropertyMetadata(null, CurrentThemeDictionaryChanged));

        public static readonly DependencyProperty CurrentSkinDictionaryProperty = DependencyProperty.RegisterAttached(
            "CurrentSkinDictionary",
            typeof(Uri),
            typeof(ThemeSkinLayoutSelector),
            new UIPropertyMetadata(null, CurrentSkinDictionaryChanged));

        public static readonly DependencyProperty CurrentLayoutDictionaryProperty = DependencyProperty.RegisterAttached(
            "CurrentLayoutDictionary",
            typeof(Uri),
            typeof(ThemeSkinLayoutSelector),
            new UIPropertyMetadata(null, CurrentLayoutDictionaryChanged));

        public static Uri GetCurrentThemeDictionary(DependencyObject obj)
        {
            return (Uri)obj.GetValue(CurrentThemeDictionaryProperty);
        }

        public static void SetTheme(DependencyObject obj, Uri value)
        {
            obj.SetValue(CurrentThemeDictionaryProperty, value);
        }

        public static Uri GetCurrentSkinDictionary(DependencyObject obj)
        {
            return (Uri)obj.GetValue(CurrentSkinDictionaryProperty);
        }

        public static void SetSkin(DependencyObject obj, Uri value)
        {
            obj.SetValue(CurrentSkinDictionaryProperty, value);
        }

        public static Uri GetCurrentLayoutDictionary(DependencyObject obj)
        {
            return (Uri)obj.GetValue(CurrentLayoutDictionaryProperty);
        }

        public static void SetLayout(DependencyObject obj, Uri value)
        {
            obj.SetValue(CurrentLayoutDictionaryProperty, value);
        }

        private static void CurrentThemeDictionaryChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is FrameworkElement)
            {
                ApplyTheme(obj as FrameworkElement, GetCurrentThemeDictionary(obj));
            }
        }

        private static void CurrentSkinDictionaryChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is FrameworkElement)
            {
                ApplySkin(obj as FrameworkElement, GetCurrentSkinDictionary(obj));
            }
        }

        private static void CurrentLayoutDictionaryChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (obj is FrameworkElement)
            {
                ApplyLayout(obj as FrameworkElement, GetCurrentLayoutDictionary(obj));
            }
        }

        private static void ApplyTheme(FrameworkElement targetElement, Uri dictionaryUri)
        {
            if (targetElement == null) return;

            try
            {
                ThemeResourceDictionary themeDictionary = null;
                if (dictionaryUri != null)
                {
                    themeDictionary = new ThemeResourceDictionary();
                    themeDictionary.Source = dictionaryUri;

                    // add the new dictionary to the collection of merged dictionaries of the target object
                    targetElement.Resources.MergedDictionaries.Insert(0, themeDictionary);
                }

                // find if the target element already has a theme applied
                List<ThemeResourceDictionary> existingDictionaries =
                    (from dictionary in targetElement.Resources.MergedDictionaries.OfType<ThemeResourceDictionary>()
                     select dictionary).ToList();

                // remove the existing dictionaries
                foreach (ThemeResourceDictionary dictionary in existingDictionaries)
                {
                    if (themeDictionary == dictionary) continue;  // don't remove the newly added dictionary
                    targetElement.Resources.MergedDictionaries.Remove(dictionary);
                }
            }
            finally { }
        }

        private static void ApplySkin(FrameworkElement targetElement, Uri dictionaryUri)
        {
            if (targetElement == null) return;

            try
            {
                SkinResourceDictionary skinDictionary = null;
                if (dictionaryUri != null)
                {
                    skinDictionary = new SkinResourceDictionary();
                    skinDictionary.Source = dictionaryUri;

                    // add the new dictionary to the collection of merged dictionaries of the target object
                    targetElement.Resources.MergedDictionaries.Insert(0, skinDictionary);
                }

                // find if the target element already has a skin applied
                List<SkinResourceDictionary> existingDictionaries =
                    (from dictionary in targetElement.Resources.MergedDictionaries.OfType<SkinResourceDictionary>()
                     select dictionary).ToList();

                // remove the existing dictionaries
                foreach (SkinResourceDictionary dictionary in existingDictionaries)
                {
                    if (skinDictionary == dictionary) continue;  // don't remove the newly added dictionary
                    targetElement.Resources.MergedDictionaries.Remove(dictionary);
                }
            }
            finally { }
        }

        private static void ApplyLayout(FrameworkElement targetElement, Uri dictionaryUri)
        {
            if (targetElement == null) return;

            try
            {
                LayoutResourceDictionary d = null;
                if (dictionaryUri != null)
                {
                    d = new LayoutResourceDictionary();
                    d.Source = dictionaryUri;

                    // add the new dictionary to the collection of merged dictionaries of the target object
                    targetElement.Resources.MergedDictionaries.Insert(0, d);
                }

                // find if the target element already has a skin applied
                List<LayoutResourceDictionary> existingDictionaries =
                    (from dictionary in targetElement.Resources.MergedDictionaries.OfType<LayoutResourceDictionary>()
                     select dictionary).ToList();

                // remove the existing dictionaries
                foreach (LayoutResourceDictionary dictionary in existingDictionaries)
                {
                    if (d == dictionary) continue;  // don't remove the newly added dictionary
                    targetElement.Resources.MergedDictionaries.Remove(dictionary);
                }
            }
            finally { }
        }


    }
}


