﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Dazzle.Appearance
{
    public enum ThemeType
    {
        Default = 0,
        Chiselled
    }

    public enum SkinType
    {
        Default = 0,
        GreenOnBlack,
        Oceania,
        FiftyShadesOfGray,
        Obsidian,
        Mymy
    }

    public enum LayoutType
    {
        Default = 0, //Widescreen
        WideScreen,  //High resolution W/H > 1.25, i.e. Laptop, desktop
        SquareScreen //Low resolution, with approximately square monitor with W/H ~ 1.
    }

    public class AppearanceManager
    {
        static private Dictionary<ThemeType, Uri> _themes = new Dictionary<ThemeType, Uri>();
        static private Dictionary<SkinType, Uri> _skins = new Dictionary<SkinType, Uri>();
        static private Dictionary<LayoutType, Uri> _layouts = new Dictionary<LayoutType, Uri>();

        static void Init()
        {
            var t = UriKind.RelativeOrAbsolute;

            if (_themes.Count == 0)
            {
                _themes.Add(ThemeType.Default, new Uri("pack://application:,,,/Dazzle;component/Appearance/Themes/DefaultTheme.xaml", t));
                //_themes.Add(ThemeType.Chiselled, new Uri("pack://application:,,,/Dazzle;component/Appearance/Themes/ChiselledTheme.xaml", t));
            }
            if(_skins.Count == 0)
            { 
                _skins.Add(SkinType.Default, new Uri("pack://application:,,,/Dazzle;component/Appearance/Skins/DefaultSkin.xaml", t));
                _skins.Add(SkinType.GreenOnBlack, new Uri("pack://application:,,,/Dazzle;component/Appearance/Skins/GreenOnBlackSkin.xaml", t));
                //_skins.Add(SkinType.Oceania, new Uri("pack://application:,,,/Dazzle;component/Appearance/Skins/OceaniaSkin.xaml", t));
                //_skins.Add(SkinType.FiftyShadesOfGray, new Uri("pack://application:,,,/Dazzle;component/Appearance/Skins/FiftyShadesOfGraySkin.xaml", t));
                //_skins.Add(SkinType.Obsidian, new Uri("pack://application:,,,/Dazzle;component/Appearance/Skins/ObsidianSkin.xaml", t));
                //_skins.Add(SkinType.Mymy, new Uri("pack://application:,,,/Dazzle;component/Appearance/Skins/MymySkin.xaml", t));
            }
            if (_layouts.Count == 0)
            {
                _layouts.Add(LayoutType.Default, new Uri("pack://application:,,,/Dazzle;component/Appearance/Layouts/WideScreenLayout.xaml", t));
                _layouts.Add(LayoutType.WideScreen, new Uri("pack://application:,,,/Dazzle;component/Appearance/Layouts/WideScreenLayout.xaml", t));
                _layouts.Add(LayoutType.SquareScreen, new Uri("pack://application:,,,/Dazzle;component/Appearance/Layouts/DefaultLayout.xaml", t));
            }
        }

        public static Uri GetThemeUri(ThemeType type)
        {
            Init();
            return _themes[type];
        }

        public static Uri GetSkinUri(SkinType type)
        {
            Init();
            return _skins[type];
        }

        public static Uri GetLayoutUri(LayoutType type)
        {
            Init();
            return _layouts[type];
        }

        #region Fields

        static ThemeType _currentTheme = ThemeType.Default;
        static SkinType _currentSkin = SkinType.Default;
        static LayoutType _currentLayout = LayoutType.Default;
        static List<DependencyObject> _registeredObjects = new List<DependencyObject>();

        #endregion

        #region Properties

        public static ThemeType CurrentTheme
        {
            get
            {
                return _currentTheme;
            }

            set
            {
                _currentTheme = value;
                ApplyTheme();
            }
        }

        public static SkinType CurrentSkin
        {
            get
            {
                return _currentSkin;
            }

            set
            {
                _currentSkin = value;
                ApplySkin();
            }
        }

        public static LayoutType CurrentLayout
        {
            get
            {
                return _currentLayout;
            }

            set
            {
                _currentLayout = value;
                ApplyLayout();
            }
        }

        #endregion

        #region Private static methods

        private static void ApplyTheme()
        {
            foreach(var o in _registeredObjects)
            {
                ThemeSkinLayoutSelector.SetTheme(o, GetThemeUri(_currentTheme));
            }
        }

        private static void ApplySkin()
        {
            foreach (var o in _registeredObjects)
            {
                ThemeSkinLayoutSelector.SetSkin(o, GetSkinUri(_currentSkin));
            }
        }

        private static void ApplyLayout()
        {
            foreach (var o in _registeredObjects)
            {
                ThemeSkinLayoutSelector.SetLayout(o, GetLayoutUri(_currentLayout));
            }
        }

        #endregion

        #region Public static methods

        /// <summary>
        /// This method registers high level and persistent UI elements such as 
        /// windows for dynamic theme and skin changes. This includes main windows
        /// or other windows which are always visible when the user requests change
        /// of appearance. 
        /// There is no need to register windows which are modal 
        /// and are created and destroyed often such as dialogs and pop-ups for 
        /// which always the current theme/skin is applied during the loading call.
        /// 
        /// </summary>
        /// <param name="o">High level user interface element</param>
        /// 
        public static void RegisterUI(DependencyObject o)
        {
            if(o == null || _registeredObjects.Contains(o))
            {
                return;
            }
            _registeredObjects.Add(o);
            SetAppearance(o);
        }

        /// <summary>
        /// This method sets the appearance (theme and skin) to the UI element.
        /// </summary>
        /// <param name="o">High level user interface element</param>
        /// 
        public static void SetAppearance(DependencyObject o)
        {
            if (o == null)
            {
                return;
            }
            ThemeSkinLayoutSelector.SetSkin(o, GetSkinUri(_currentSkin));
            ThemeSkinLayoutSelector.SetTheme(o, GetThemeUri(_currentTheme));
            ThemeSkinLayoutSelector.SetLayout(o, GetLayoutUri(_currentLayout));
        }
        #endregion

    }
}

