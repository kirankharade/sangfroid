
Sangfroid Framework
-------------------

Sangfroid framework is a collection of modules dealing with Mathematics, Geometry, 3D Graphics, Statistics, Computer vision and related engineering domains. 
The core implementation is in C++ with supplementary modules in C++/CLI, C# and Python.

Versioning
----------
Semantic versioning: <Major Version>.<Minor Version>.<Patch Version>

Authors
-------
Kiran Amrut Kharade <KiranAKharade@gmail.com>

License
-------
Currently 'Sangfroid Framework' remains a closed source solution. 
The usage, modifications and duplications are prohibited without the prior permission of the author Kiran A. Kharade.

Built with
----------
The codebase has been currently tested with Visual studio 2015.
The plans are to support it on Visual studio 2017 and latest version of GCC on Ubuntu Linux.

